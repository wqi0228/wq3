/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import com.cy.work.backend.web.vo.converter.ReportPageConverter;
import com.cy.work.backend.web.vo.converter.to.ReportPage;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作報告首頁快選
 * @author brain0925_liao
 */
@Entity
@Table(name = "wr_homepage_favorite")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WRHomepageFavorite implements Serializable {


    private static final long serialVersionUID = 7457437538387037924L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "define_sid", length = 36)
    private String sid;

    @Column(name = "usersid", nullable = false)
    private Integer usersid;

    @Column(name = "report_page", nullable = true)
    @Convert(converter = ReportPageConverter.class)
    private ReportPage report_page;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;
    
    
}
