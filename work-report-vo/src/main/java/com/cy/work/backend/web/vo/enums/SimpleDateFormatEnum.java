/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

import com.cy.work.backend.web.vo.SimpleDateFormatThreadSafe;
import java.util.Locale;

/**
 * 時間轉換器類型
 *
 * @author brain0925_liao
 */
public enum SimpleDateFormatEnum {
    /** yyyy/MM/dd */
    SdfDate(new SimpleDateFormatThreadSafe("yyyy/MM/dd")),
    /** yyyy-MM-dd */
    SdfDateDash(new SimpleDateFormatThreadSafe("yyyy-MM-dd")),
    /** yyyy-MM-dd HH:mm */
    SdfDateDashTime(new SimpleDateFormatThreadSafe("yyyy-MM-dd HH:mm")),
    /** yyyy-MM-dd HH:mm:ss */
    SdfDateDashTimeSS(new SimpleDateFormatThreadSafe("yyyy-MM-dd HH:mm:ss")),
    /** yyyy-MM-dd aa hh:mm:ss */
    SdfDateDashTimeSSaa(new SimpleDateFormatThreadSafe("yyyy-MM-dd aa hh:mm:ss", Locale.US)),
    /** HH:mm */
    SdfTime(new SimpleDateFormatThreadSafe("HH:mm")),
    /** yyyy/MM/dd HH:mm */
    SdfDateTime(new SimpleDateFormatThreadSafe("yyyy/MM/dd HH:mm")),
    /** yyyy-MM-dd HH:mm:ss.SSS */
    SdfTimestamp(new SimpleDateFormatThreadSafe("yyyy-MM-dd HH:mm:ss.SSS")),
    /** yyyy/MM/dd aa hh:mm:ss */
    SdfDateTimeSSaa(new SimpleDateFormatThreadSafe("yyyy/MM/dd aa hh:mm:ss", Locale.US)),
    /** yyyy/MM/dd aa hh:mm */
    SdfDateTimeaa(new SimpleDateFormatThreadSafe("yyyy/MM/dd aa hh:mm", Locale.US)),
    /** MM/dd/yyyy HH:mm:ss aa */
    SdfDateTimeaa_b(new SimpleDateFormatThreadSafe("MM/dd/yyyy HH:mm:ss aa", Locale.US));

    private final SimpleDateFormatThreadSafe simpleDateFormat;

    SimpleDateFormatEnum(SimpleDateFormatThreadSafe value) {
        this.simpleDateFormat = value;
    }

    public SimpleDateFormatThreadSafe getValue() {
        return simpleDateFormat;
    }
}
