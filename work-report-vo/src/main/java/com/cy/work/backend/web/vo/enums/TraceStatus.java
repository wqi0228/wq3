/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 追蹤狀態類型
 * @author brain0925_liao
 */
public enum TraceStatus {
    /** 未完成 */
    UN_FINISHED("未完成"),
    /** 已完成 */
    FINISHED("已完成");

    private final String value;

    TraceStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
