/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.converter.to.RRcordTo;
import com.cy.work.backend.web.vo.converter.to.ReadRecord;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 工作報告已讀狀況Converter
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class ReadRecordConverter implements AttributeConverter<ReadRecord, String> {

    @Override
    public String convertToDatabaseColumn(ReadRecord attribute) {
        try {
            if (attribute == null || attribute.getRRecord() == null) {
                return "";
            }
//            List<RRcordTo> rRcordTo = Lists.newArrayList();
//            attribute.getRRecord().forEach(item -> {
//                RRcordTo r = new RRcordTo(item.getReader(), item.getRead().name(), item.getReadDay());
//            });
            String json = WkJsonUtils.getInstance().toJson(attribute.getRRecord()).replace("\r", "").replace("\n", "").replace(" ", "");;
            //log.info("ReadRecordConverter" + json);
            return json;
        } catch (IOException ex) {
            log.error("parser ReadRecordConverter to json fail!!" + ex.getMessage(), ex);
        }
        return "";
    }

    @Override
    public ReadRecord convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new ReadRecord();
        }
        try {
            ReadRecord to = new ReadRecord();
            List<RRcordTo> resultList = WkJsonUtils.getInstance().fromJsonToList(dbData, RRcordTo.class);
//            resultList.forEach(item -> {
//                RRecord r = new RRecord(item.getReader(), item.getReadDay(), WRReadStatus.valueOf(item.getRead()));
//                to.getRRecord().add(r);
//            });
            to.setRRecord(resultList);
            return to;
        } catch (IOException ex) {
            log.error("parser json to ReadRecord fail!!" + ex.getMessage(), ex);
            return new ReadRecord();
        }
    }
}
