/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 轉寄已未讀類型
 * @author brain0925_liao
 */
public enum ReadRecordStatus {

    /** 轉寄已讀 */
    READ("轉寄已讀"),
    /** 轉寄未讀 */
    UNREAD("轉寄未讀");
    
    private final String value;

    ReadRecordStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
