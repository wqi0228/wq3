/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author brain0925_liao
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReadReceiptsMemberTo implements Serializable {


    private static final long serialVersionUID = -3682286102623182159L;

    @JsonProperty(value = "reader")
    private String reader;

    @JsonProperty(value = "readreceipt")
    private String readreceipt;

    @JsonProperty(value = "readtime")
    private String readtime;

    @JsonProperty(value = "requiretime")
    private String requiretime;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.reader);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReadReceiptsMemberTo other = (ReadReceiptsMemberTo) obj;
        if (!Objects.equals(this.reader, other.reader)) {
            return false;
        }
        return true;
    }

}
