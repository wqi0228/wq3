/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 轉寄部門查詢欄位參數
 * @author brain0925_liao
 */
public enum WRReceviceDepTransSearchColumn implements WRColumn {

    INDEX("序", true, false, false, "5"),
    FOWARDDEP("部", true, false, false, "10"),
    FOWARDMEMBER("個", true, false, false, "10"),
    TRACE("追", true, false, false, "10"),
    SEND_DEP("寄發單位", true, true, true, "70"),
    SEND_USER("寄發者", true, true, true, "70"),
    SEND_TIME("寄發時間", true, true, true, "70"),
    RECEVICE_DEP("寄發至", true, true, true, "70"),
    TAG("標籤", true, true, true, "70"),
    THEME("主題", true, false, false, ""),
    MODIFY_TIME("異動時間", true, true, true, "70"),
    READED("是否閱讀", true, true, true, "70");

    /** 欄位名稱 */
    private final String name;
    /** 預設是否顯示 */
    private final boolean defaultShow;
    /** 是否可修改欄位寬度 */
    private final boolean canModifyWidth;
    /** 是否可修改是否顯示 */
    private final boolean canSelectItem;
    /** 預設欄位寬度 */
    private final String defaultWidth;

    WRReceviceDepTransSearchColumn(String name, boolean defaultShow, boolean canModifyWidth, boolean canSelectItem, String defaultWidth) {
        this.name = name;
        this.defaultShow = defaultShow;
        this.canModifyWidth = canModifyWidth;
        this.defaultWidth = defaultWidth;
        this.canSelectItem = canSelectItem;
    }

    @Override
    public boolean getCanSelectItem() {
        return canSelectItem;
    }

    @Override
    public boolean getDefaultShow() {
        return defaultShow;
    }

    @Override
    public boolean getCanModifyWidth() {
        return canModifyWidth;
    }

    @Override
    public String getDefaultWidth() {
        return defaultWidth;
    }

    @Override
    public String getVal() {
        return name;
    }
}
