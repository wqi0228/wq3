/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 需要額外查詢筆數的介面類型
 * @author brain0925_liao
 */
public enum WRFunItemComponentType {

    FAVORITES("收藏夾", "fun-favorites"),
    TRACE("追蹤", "fun-trace"),
    INCOME_DEPT("收部門轉發","fun-income-forwarding-department"),
    INCOME_REPORT("收呈報","fun-income-report"),
    INCOME_INSTRUCTION("收指示","fun-income-instruction"),
    INCOME_MEMBER("收個人","fun-income-personal");

    private final String val;

    private final String componentID;

    WRFunItemComponentType(String val, String componentID) {
        this.val = val;
        this.componentID = componentID;
    }

    public String getComponentID() {
        return componentID;
    }

    public String getVal() {
        return val;
    }
}
