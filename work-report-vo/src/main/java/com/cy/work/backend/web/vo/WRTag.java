/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.vo.converter.UserDepConverter;
import com.cy.work.backend.web.vo.converter.WRTagCategoryTypeConverter;
import com.cy.work.backend.web.vo.converter.to.UserDep;
import com.cy.work.backend.web.vo.enums.WRTagCategoryType;
import com.cy.work.common.vo.listener.RemoveContorlCharListener;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作報告標籤
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wr_tag")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
@EntityListeners(RemoveContorlCharListener.class) //控制字元移除器
public class WRTag implements Serializable {


    private static final long serialVersionUID = 80350745400863526L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wr_tag_sid", length = 36)
    private String sid;

    @Column(name = "tag_name", nullable = false, length = 255)
    private String tag_name;
    
     @Column(name = "compSid", nullable = false)
    private Integer compSid;

    @Column(name = "use_dep", nullable = true)
    @Convert(converter = UserDepConverter.class)
    private UserDep use_dep;

    @Column(name = "category", nullable = true)
    @Convert(converter = WRTagCategoryTypeConverter.class)
    private WRTagCategoryType category;

    @Column(name = "memo", nullable = true, length = 255)
    private String memo;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr_sid;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr_sid;

    @Column(name = "update_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;

    @Column(name = "seq", nullable = false)
    private Integer seq;

}
