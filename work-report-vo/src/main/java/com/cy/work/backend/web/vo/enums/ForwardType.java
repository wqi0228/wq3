/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 轉寄大項類型
 * @author brain0925_liao
 */
public enum ForwardType {

    /** 部門 */
    DEP("部門", 0),
    /** 個人 */
    PERSON("個人", 1);

    private final String value;

    private final Integer key;

    ForwardType(String value, Integer key) {
        this.value = value;
        this.key = key;
    }

    public Integer getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
