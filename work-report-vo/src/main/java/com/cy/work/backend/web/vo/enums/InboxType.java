/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 轉寄細項類型
 * @author brain0925_liao
 */
public enum InboxType {
    /** 收呈報 下屬寄給上層主管 */
    INCOME_REPORT("收呈報"),
    /** 收指示 上層主管寄給下屬 */
    INCOME_INSTRUCTION("收指示"),
    /** 收個人 不同組織互寄 */
    INCOME_MEMBER("收個人"),
    /** 收部門轉發 部門互寄 */
    INCOME_DEPT("收部門轉發");

    private final String value;

    InboxType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
