/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import com.cy.commons.enums.Activation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作報告收藏
 * @author brain0925_liao
 */
@Entity
@Table(name = "wr_favorite")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WRFavorite implements Serializable {

    
    private static final long serialVersionUID = 4596016030965340496L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "favorite_sid", length = 36)
    private String sid;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;
    
     @Column(name = "create_usr", nullable = false)
    private Integer create_usr;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;
    
     @Column(name = "wr_sid", nullable = false, length = 36)
    private String wr_sid;

    @Column(name = "wr_no", nullable = false, length = 21)
    private String wr_no;

}
