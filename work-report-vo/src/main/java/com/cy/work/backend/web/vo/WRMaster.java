/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import com.cy.work.backend.web.vo.converter.ReadReceiptsMemberConverter;
import com.cy.work.backend.web.vo.converter.ReadRecordConverter;
import com.cy.work.backend.web.vo.converter.WREditTypeConverter;
import com.cy.work.backend.web.vo.converter.WRStatusConverter;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMember;
import com.cy.work.backend.web.vo.converter.to.ReadRecord;
import com.cy.work.backend.web.vo.enums.WREditType;
import com.cy.work.backend.web.vo.enums.WRStatus;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.listener.RemoveContorlCharListener;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 工作報告主檔
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wr_master")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
@EntityListeners(RemoveContorlCharListener.class) //控制字元移除器
public class WRMaster implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1375869072593354833L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wr_sid", length = 36)
    private String sid;

    @Column(name = "wr_no", nullable = false, length = 255, unique = true)
    private String wr_no;

    @Column(name = "comp_sid", nullable = false)
    private Integer comp_sid;

    @Column(name = "dep_sid", nullable = false)
    private Integer dep_sid;

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;

    @Column(name = "submit_dt")
    private Date submit_dt;

    @Convert(converter = WRStatusConverter.class)
    @Column(name = "wr_status", nullable = false)
    private WRStatus wr_status;

    @Column(name = "has_attachment", nullable = false)
    private boolean has_attachment;

    @Column(name = "read_record")
    @Convert(converter = ReadRecordConverter.class)
    private ReadRecord read_record;

    @Column(name = "wr_tag_sid", nullable = false, length = 36)
    private String wr_tag_sid;

    @Convert(converter = WREditTypeConverter.class)
    @Column(name = "edit_type", nullable = false)
    private WREditType edit_type;

    @Column(name = "theme", nullable = true, length = 255)
    private String theme;

    @Column(name = "content", nullable = false, length = 255)
    private String content;

    @Convert(converter = StringBlobConverter.class)
    @Column(name = "content_css", nullable = false)
    private String content_css;

    @Column(name = "memo", nullable = true, length = 255)
    private String memo;

    @Convert(converter = StringBlobConverter.class)
    @Column(name = "memo_css", nullable = true)
    private String memo_css;

    @Column(name = "read_receipts_member", nullable = true)
    @Convert(converter = ReadReceiptsMemberConverter.class)
    private ReadReceiptsMember read_receipts_member;

    @Column(name = "last_modify_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date last_modify_dt;

    @Column(name = "lock_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lock_dt;

    @Column(name = "lock_usr", nullable = true)
    private Integer lock_usr;

    @Column(name = "last_modify_memo", nullable = true, length = 255)
    private String last_modify_memo;

    @Convert(converter = StringBlobConverter.class)
    @Column(name = "last_modify_memo_css", nullable = true)
    private String last_modify_memo_css;

}
