/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 追蹤方式類型
 * @author brain0925_liao
 */
public enum TraceType {
    /** 部門 */
    DEPARTMENT("部門"),
    /** 個人 */
    PERSONAL("個人"),
    /** 邀請其他人追蹤 */
    OTHER_PERSON("邀請其他人追蹤");

    private final String value;

    TraceType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
