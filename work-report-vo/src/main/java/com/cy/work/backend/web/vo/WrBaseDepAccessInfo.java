/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.GenericGenerator;
import org.testng.collections.Sets;

import com.cy.work.backend.web.vo.converter.LimitBaseAccessViewDepConverter;
import com.cy.work.backend.web.vo.converter.OtherBaseAccessViewDepConverter;
import com.cy.work.backend.web.vo.converter.to.DepTo;
import com.cy.work.backend.web.vo.converter.to.LimitBaseAccessViewDep;
import com.cy.work.backend.web.vo.converter.to.OtherBaseAccessViewDep;
import com.cy.work.common.utils.WkStringUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author brain0925_liao
 */
@Entity
@Table(name = "wr_base_dep_access_info")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = { "sid" })
@Data
public class WrBaseDepAccessInfo implements Serializable {

    private static final long serialVersionUID = -6036387437182941551L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "base_dep_access_sid", length = 36)
    private String sid;

    @Column(name = "login_user_dep", nullable = false)
    private Integer loginUserDepSid;

    @Column(name = "limit_base_access_view_dep")
    @Convert(converter = LimitBaseAccessViewDepConverter.class)
    private LimitBaseAccessViewDep limitBaseAccessViewDep;

    @Column(name = "other_base_access_view_dep")
    @Convert(converter = OtherBaseAccessViewDepConverter.class)
    private OtherBaseAccessViewDep otherBaseAccessViewDep;

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr_sid;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr_sid;

    @Column(name = "update_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;

    /**
     * 限制檢視部門
     * 
     * @return
     */
    public Set<Integer> getLimitBaseAccessViewDepSids() {
        if (this.limitBaseAccessViewDep == null
                || WkStringUtils.isEmpty(this.limitBaseAccessViewDep.getDepTos())) {
            return Sets.newHashSet();
        }

        Set<Integer> depSids = Sets.newHashSet();
        for (DepTo depTo : this.limitBaseAccessViewDep.getDepTos()) {
            depSids.add(depTo.getSid());
        }
        return depSids;
    }

    /**
     * 增加可閱
     * 
     * @return
     */
    public Set<Integer> getOtherBaseAccessViewDepSids() {
        if (this.otherBaseAccessViewDep == null
                || WkStringUtils.isEmpty(this.otherBaseAccessViewDep.getDepTos())) {
            return Sets.newHashSet();
        }

        Set<Integer> depSids = Sets.newHashSet();
        for (DepTo depTo : this.otherBaseAccessViewDep.getDepTos()) {
            depSids.add(depTo.getSid());
        }
        return depSids;
    }
}
