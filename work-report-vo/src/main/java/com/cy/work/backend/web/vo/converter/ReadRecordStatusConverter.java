/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.enums.ReadRecordStatus;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 轉寄已未讀Converter
 *
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class ReadRecordStatusConverter implements AttributeConverter<ReadRecordStatus, String> {

    @Override
    public String convertToDatabaseColumn(ReadRecordStatus attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.name();
    }

    @Override
    public ReadRecordStatus convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return ReadRecordStatus.valueOf(dbData);
        } catch (Exception e) {
            log.error("ReadRecordStatus Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
