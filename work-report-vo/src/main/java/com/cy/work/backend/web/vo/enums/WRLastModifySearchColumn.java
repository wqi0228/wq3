/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 最齁修改日期調整查詢欄位參數
 * @author brain0925_liao
 */
public enum WRLastModifySearchColumn implements WRColumn {

    INDEX("序", true, false, false, "5"),
    CREATE_TIME("建立日期", true, true, true, "70"),
    DEPARTMENT("部門", true, true, true, "70"),
    CREATE_USER("建立者", true, true, true, "70"),
    THEME("主題", true, false, false, ""),
    NO("單號", true, true, true, "70"),
    LASTCANMODIFY_DATE("最後可編輯時間", true, true, true, "70"),
    EDIT_TYPE("編輯權限", true, true, true, "70"),
    STATUS("狀態", true, true, true, "70"),
    MEMO("備註說明", true, true, true, "70");
    /** 欄位名稱 */
    private final String name;
    /** 預設是否顯示 */
    private final boolean defaultShow;
    /** 是否可修改欄位寬度 */
    private final boolean canModifyWidth;
    /** 是否可修改是否顯示 */
    private final boolean canSelectItem;
    /** 預設欄位寬度 */
    private final String defaultWidth;

    WRLastModifySearchColumn(String name, boolean defaultShow, boolean canModifyWidth, boolean canSelectItem, String defaultWidth) {
        this.name = name;
        this.defaultShow = defaultShow;
        this.canModifyWidth = canModifyWidth;
        this.defaultWidth = defaultWidth;
        this.canSelectItem = canSelectItem;
    }

    @Override
    public boolean getCanSelectItem() {
        return canSelectItem;
    }

    @Override
    public boolean getDefaultShow() {
        return defaultShow;
    }

    @Override
    public boolean getCanModifyWidth() {
        return canModifyWidth;
    }

    @Override
    public String getDefaultWidth() {
        return defaultWidth;
    }

    @Override
    public String getVal() {
        return name;
    }
}
