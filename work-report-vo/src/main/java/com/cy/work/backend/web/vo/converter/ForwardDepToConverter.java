/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.converter.to.DepTo;
import com.cy.work.backend.web.vo.converter.to.SpecificDepTo;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 轉寄部門Converter
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class ForwardDepToConverter implements AttributeConverter<SpecificDepTo, String> {

    @Override
    public String convertToDatabaseColumn(SpecificDepTo attribute) {
        try {
            if (attribute == null) {
                return "";
            }
            String json = WkJsonUtils.getInstance().toJson(attribute.getDepInfo());
            //log.error("json" + json);
            return json;
        } catch (IOException ex) {
            log.error("parser ForwardDepToConverter to json fail!!" + ex.getMessage(),ex);
        }
        return "";
    }

    @Override
    public SpecificDepTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new SpecificDepTo();
        }
        try {
            SpecificDepTo to = new SpecificDepTo();
            List<DepTo> resultList = WkJsonUtils.getInstance().fromJsonToList(dbData, DepTo.class);
            to.setDepInfo(resultList);
            return to;
        } catch (IOException ex) {
            log.error("parser json to GroupsTo fail!!" + ex.getMessage(),ex);
            return new SpecificDepTo();
        }
    }

}

