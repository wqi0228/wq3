package com.cy.work.backend.web.vo.converter.to;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.cy.work.backend.web.vo.enums.SqlColumnType;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomClauseVO {

    @Getter
    @Setter
    /** 標籤 */
    private String selectTag;
    
    @Getter
    @Setter
    /** 單據狀態 */
    private String selectStatus = "COMMIT";
    
    @Getter
    @Setter
    /** 主題*/
    private String title;
    
    @Getter
    @Setter
    /** 內容*/
    private String content;
    
    @Getter
    @Setter
    /** 建立人員 */
    private String personName;
    
    @Getter
    @Setter
    /** 日期撈取 */
    private SqlColumnType sqlColumnType;
   
    @Getter
    @Setter
    /** 排序方式 */
    private String order;
   
    @Getter
    @Setter
    /** 日期按鈕位址 */
    private Integer dateTypeIndex = -1;
}
