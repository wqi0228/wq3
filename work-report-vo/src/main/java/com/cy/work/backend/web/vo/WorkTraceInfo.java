/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.vo.converter.TraceStatusConverter;
import com.cy.work.backend.web.vo.converter.TraceTypeConverter;
import com.cy.work.backend.web.vo.enums.TraceStatus;
import com.cy.work.backend.web.vo.enums.TraceType;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.listener.RemoveContorlCharListener;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作報告追蹤
 * @author brain0925_liao
 */
@Entity
@Table(name = "wr_trace_info")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
@EntityListeners(RemoveContorlCharListener.class) //控制字元移除器
public class WorkTraceInfo implements Serializable {


    private static final long serialVersionUID = -1206100672836760559L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "tracesid", length = 36)
    private String sid;

    @Column(name = "trace_source_sid", nullable = false, length = 36)
    private String trace_source_sid;

    @Column(name = "trace_source_no", nullable = false, length = 36)
    private String trace_source_no;

    @Column(name = "trace_dep", nullable = true)
    private Integer trace_dep;

    @Column(name = "trace_usr", nullable = true)
    private Integer trace_usr;

    @Column(name = "trace_source_type", nullable = false, length = 45)
    private String trace_source_type;

    @Convert(converter = TraceStatusConverter.class)
    @Column(name = "trace_status", nullable = false)
    private TraceStatus trace_status;

    @Convert(converter = TraceTypeConverter.class)
    @Column(name = "trace_type", nullable = false)
    private TraceType trace_type;

    @Column(name = "trace_memo", nullable = true, length = 255)
    private String trace_memo;

    @Convert(converter = StringBlobConverter.class)
    @Column(name = "trace_memo_css", nullable = true)
    private String trace_memo_css;

    @Column(name = "trace_notice_date", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date trace_notice_date;

    @Column(name = "trace_finish_date", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date trace_finish_date;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;

   
}
