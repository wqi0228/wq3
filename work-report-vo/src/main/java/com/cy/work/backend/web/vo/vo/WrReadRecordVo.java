package com.cy.work.backend.web.vo.vo;

import java.io.Serializable;
import java.util.Date;

import com.cy.work.backend.web.vo.WrReadRecord;

import lombok.Getter;
import lombok.Setter;

public class WrReadRecordVo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3837732641942727445L;

    @Getter
    @Setter
    private String readRecordSid;

    @Getter
    @Setter
    private String readStatus;

    @Getter
    @Setter
    private boolean status;

    @Getter
    @Setter
    private String createUser;

    @Getter
    @Setter
    private Date createDate;

    @Getter
    @Setter
    private String updateUser;

    @Getter
    @Setter
    private Date updateDate;

    @Getter
    @Setter
    private String wrSid;


    public static WrReadRecordVo valueOf(WrReadRecord wrReadRecord){
        WrReadRecordVo result = new WrReadRecordVo();
        result.setReadRecordSid(wrReadRecord.getReadRecordSid());
        result.setReadStatus(wrReadRecord.getReadStatus());
        result.setStatus(wrReadRecord.isStatus());
        result.setCreateUser(wrReadRecord.getCreateUser());
        result.setCreateDate(wrReadRecord.getCreateDate());
        result.setUpdateUser(wrReadRecord.getUpdateUser());
        result.setUpdateDate(wrReadRecord.getUpdateDate());
        result.setWrSid(wrReadRecord.getWrSid());
        return result;
    }
}
