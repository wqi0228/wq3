/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 編輯權限類型
 * @author brain0925_liao
 */
public enum WREditType {
    /** 部門 */
    DEP("部門"),
    /** 個人 */
    PERSONAL("個人");

    private final String value;

    WREditType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
