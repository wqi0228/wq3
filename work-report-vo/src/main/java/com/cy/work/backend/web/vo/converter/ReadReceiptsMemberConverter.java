/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMember;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMemberTo;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 讀取回傳資訊Converter
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class ReadReceiptsMemberConverter implements AttributeConverter<ReadReceiptsMember, String> {

    @Override
    public String convertToDatabaseColumn(ReadReceiptsMember attribute) {
        try {
            if (attribute == null || attribute.getReadReceiptsMemberTo() == null) {
                return "";
            }
//            List<RRcordTo> rRcordTo = Lists.newArrayList();
//            attribute.getRRecord().forEach(item -> {
//                RRcordTo r = new RRcordTo(item.getReader(), item.getRead().name(), item.getReadDay());
//            });
            String json = WkJsonUtils.getInstance().toJson(attribute.getReadReceiptsMemberTo()).replace("\r", "").replace("\n", "");
            //log.info("ReadReceiptsMemberConverter" + json);
            return json;
        } catch (IOException ex) {
            log.error("parser ReadReceiptsMemberConverter to json fail!!" + ex.getMessage(), ex);
        }
        return "";
    }

    @Override
    public ReadReceiptsMember convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new ReadReceiptsMember();
        }
        try {
            ReadReceiptsMember to = new ReadReceiptsMember();
            List<ReadReceiptsMemberTo> resultList = WkJsonUtils.getInstance().fromJsonToList(dbData, ReadReceiptsMemberTo.class);
//            resultList.forEach(item -> {
//                RRecord r = new RRecord(item.getReader(), item.getReadDay(), WRReadStatus.valueOf(item.getRead()));
//                to.getRRecord().add(r);
//            });
            if (resultList != null) {
                to.setReadReceiptsMemberTo(resultList);
            }
            return to;
        } catch (IOException ex) {
            log.error("parser json to ReadReceiptsMember fail!!" + ex.getMessage(), ex);
            return new ReadReceiptsMember();
        }
    }
}
