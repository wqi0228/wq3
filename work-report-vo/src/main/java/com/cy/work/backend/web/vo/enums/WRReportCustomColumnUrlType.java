
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 自訂欄位URL 類型
 *
 * @author brain0925_liao
 */
public enum WRReportCustomColumnUrlType {
    /** 個人草稿查詢 */
    PERSON_DRAFT_SEARCH("work-report/search/search1.xhtml"),
    /** 工作報告查詢 */
    WORK_REPORT_SEARCH("work-report/search/search2.xhtml"),
    /** 部門草稿查詢 */
    DEP_DRAFT_SEARCH("work-report/search/search3.xhtml"),
    /** 追蹤查詢 */
    TRACE_SEARCH("work-report/search/search4.xhtml"),
    /** 收藏查詢 */
    FAVORITE_SEARCH("work-report/search/search5.xhtml"),
    /** 部門轉發查詢 */
    RECEVICE_DEP_SEARCH("work-report/search/search6.xhtml"),
    /** 寄件備份查詢 */
    SEND_SEARCH("work-report/search/search7.xhtml"),
    /** 收個人查詢 */
    INCOME_MEMBER_SEARCH("work-report/search/search8.xhtml"),
    /** 收指示查詢 */
    INCOME_INSTRUCTION_SEARCH("work-report/search/search9.xhtml"),
    /** 收呈報查詢 */
    INCOME_REPORT_SEARCH("work-report/search/search10.xhtml"),
    /** 最後可編輯時間查詢 */
    LAST_MODIFY_SEARCH("work-report/setting/setting2.xhtml"),
    /** 標籤維護查詢 */
    TAG_SEARCH("work-report/setting/setting3.xhtml");

    private final String val;

    WRReportCustomColumnUrlType(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
