/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 自訂欄位URL Converter
 * @author brain0925_liao
 */
@Slf4j
@Converter
public class WRReportCustomColumnUrlConverter implements AttributeConverter<WRReportCustomColumnUrlType, String> {

    @Override
    public String convertToDatabaseColumn(WRReportCustomColumnUrlType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.getVal();
    }

    @Override
    public WRReportCustomColumnUrlType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            for (WRReportCustomColumnUrlType x : WRReportCustomColumnUrlType.values()) {
                if (dbData.equals(x.getVal())) {
                    return x;
                }
            }
            return null;
        } catch (Exception e) {
            log.error("WRReportCustomColumnUrlType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
