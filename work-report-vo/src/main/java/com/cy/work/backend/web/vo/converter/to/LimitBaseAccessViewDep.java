/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter.to;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author brain0925_liao
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LimitBaseAccessViewDep implements Serializable {


    private static final long serialVersionUID = -3000334326058466932L;
    private List<DepTo> depTos = Lists.newArrayList();
}
