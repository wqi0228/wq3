/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter.to;

import com.cy.work.backend.web.vo.enums.WRReadStatus;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author brain0925_liao
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RRecord implements Serializable {


    private static final long serialVersionUID = 3517561003474481449L;

    private String reader;

    private String readDay;

    private WRReadStatus read;

}
