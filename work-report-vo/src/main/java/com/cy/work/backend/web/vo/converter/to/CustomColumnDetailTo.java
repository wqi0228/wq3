/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author brain0925_liao
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomColumnDetailTo implements Serializable {


    private static final long serialVersionUID = 278218851938122813L;
    /** 欄位名稱 */
    @JsonProperty(value = "name")
    private String name;
    /** 欄位寬度 */
    @JsonProperty(value = "width")
    private String width;
}