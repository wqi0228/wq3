/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.converter.to.ReportPage;
import com.cy.work.backend.web.vo.converter.to.ReportPageTo;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 快選區Converter
 *
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class ReportPageConverter implements AttributeConverter<ReportPage, String> {

    @Override
    public String convertToDatabaseColumn(ReportPage attribute) {
        try {
            if (attribute == null || attribute.getReportPageTos() == null) {
                return "";
            }
//            List<RRcordTo> rRcordTo = Lists.newArrayList();
//            attribute.getRRecord().forEach(item -> {
//                RRcordTo r = new RRcordTo(item.getReader(), item.getRead().name(), item.getReadDay());
//            });
            String json = WkJsonUtils.getInstance().toJson(attribute.getReportPageTos()).replace("\r", "").replace("\n", "");
            //log.info("ReadReceiptsMemberConverter" + json);
            return json;
        } catch (Exception ex) {
            log.error("parser ReportPageConverter to json fail!!" + ex.getMessage(), ex);
        }
        return "";
    }

    @Override
    public ReportPage convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            ReportPage to = new ReportPage();
            List<ReportPageTo> resultList = WkJsonUtils.getInstance().fromJsonToList(dbData, ReportPageTo.class);
//            resultList.forEach(item -> {
//                RRecord r = new RRecord(item.getReader(), item.getReadDay(), WRReadStatus.valueOf(item.getRead()));
//                to.getRRecord().add(r);
//            });
            if (resultList != null) {
                to.setReportPageTos(resultList);
            }
            return to;
        } catch (Exception ex) {
            log.error("parser json to ReportPageConverter fail!!" + ex.getMessage(), ex);
            return null;
        }
    }
}
