/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 讀取回條已未讀類型
 * @author brain0925_liao
 */
public enum WReadReceiptStatus {
    READ,
    UNREAD
}
