/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.vo.converter.ForwardTypeConverter;
import com.cy.work.backend.web.vo.enums.ForwardType;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.listener.RemoveContorlCharListener;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作報告轉寄Group
 * @author brain0925_liao
 */
@Entity
@Table(name = "wr_inbox_send_group")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
@EntityListeners(RemoveContorlCharListener.class) //控制字元移除器
public class WRInboxSendGroup implements Serializable {

    
    private static final long serialVersionUID = -201148761590777679L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "inbox_group_sid", length = 36)
    private String sid;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Column(name = "message", nullable = true)
    private String message;

    @Convert(converter = StringBlobConverter.class)
    @Column(name = "message_css", nullable = true)
    private String message_css;
    @Column(name = "wr_sid", nullable = false, length = 36)
    private String wr_sid;
    @Column(name = "wr_no", nullable = false, length = 21)
    private String wr_no;
    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;

    @Column(name = "send_to", nullable = false)
    private String send_to;

    @Convert(converter = ForwardTypeConverter.class)
    @Column(name = "forward_type", nullable = false)
    private ForwardType forward_type;
}
