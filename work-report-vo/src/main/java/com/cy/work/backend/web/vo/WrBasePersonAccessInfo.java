/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import com.cy.work.backend.web.vo.converter.LimitBaseAccessViewPersonConverter;
import com.cy.work.backend.web.vo.converter.OtherBaseAccessViewPersonConverter;
import com.cy.work.backend.web.vo.converter.to.DepTo;
import com.cy.work.backend.web.vo.converter.to.LimitBaseAccessViewPerson;
import com.cy.work.backend.web.vo.converter.to.OtherBaseAccessViewPerson;
import com.cy.work.backend.web.vo.converter.to.UserTo;
import com.cy.work.common.utils.WkStringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.testng.collections.Sets;

/**
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wr_base_person_access_info")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WrBasePersonAccessInfo implements Serializable {


    private static final long serialVersionUID = 1033894853548193127L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "base_person_access_sid", length = 36)
    private String sid;

    @Column(name = "login_user", nullable = false)
    private Integer loginUserSid;

    @Column(name = "limit_base_access_view_person")
    @Convert(converter = LimitBaseAccessViewPersonConverter.class)
    private LimitBaseAccessViewPerson limitBaseAccessViewPerson;
    public Set<Integer> getLimitBaseAccessViewPersonSids() {
        if (this.limitBaseAccessViewPerson == null
                || WkStringUtils.isEmpty(this.limitBaseAccessViewPerson.getUserTos())) {
            return Sets.newHashSet();
        }
        
        Set<Integer> userSids = Sets.newHashSet();
        for (UserTo userTo : this.limitBaseAccessViewPerson.getUserTos()) {
            userSids.add(userTo.getSid());
        }
        return userSids;
    }


    @Column(name = "other_base_access_view_person")
    @Convert(converter = OtherBaseAccessViewPersonConverter.class)
    private OtherBaseAccessViewPerson otherBaseAccessViewPerson;
    public Set<Integer> getOtherBaseAccessViewPersonSids() {
        if (this.otherBaseAccessViewPerson == null
                || WkStringUtils.isEmpty(this.otherBaseAccessViewPerson.getUserTos())) {
            return Sets.newHashSet();
        }

        Set<Integer> userSids = Sets.newHashSet();
        for (UserTo userTo : this.otherBaseAccessViewPerson.getUserTos()) {
            userSids.add(userTo.getSid());
        }
        return userSids;
    }

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr_sid;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr_sid;

    @Column(name = "update_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;
}
