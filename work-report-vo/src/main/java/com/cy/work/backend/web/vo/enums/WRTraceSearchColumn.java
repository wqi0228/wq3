/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 追蹤查詢欄位參數
 * @author brain0925_liao
 */
public enum WRTraceSearchColumn implements WRColumn {
    INDEX("序", true, false, false, "5"),
    FOWARDDEP("部", true, false, false, "10"),
    FOWARDMEMBER("個", true, false, false, "10"),
    TRACE("追", true, false, false, "10"),
    TRACE_TYPE("追蹤方式", true, true, true, "70"),
    TAG("標籤", true, true, true, "70"),
    TRACE_NOTIFY_TIME("追蹤日期", true, true, true, "70"),
    CREATE_USER("新增者", true, true, true, "70"),
    TRACE_STATUS("追蹤狀態", true, true, true, "70"),
    TRACE_CONTENT("追蹤事項", true, true, true, "70"),
    THEME("主題", true, false, false, ""),
    MODIFY_TIME("最後異動日期", true, true, true, "70"),
    READED("是否閱讀", true, true, true, "70"),
    TRACE_TIME("追蹤時間", true, true, true, "70");

    /** 欄位名稱 */
    private final String name;
    /** 預設是否顯示 */
    private final boolean defaultShow;
    /** 是否可修改欄位寬度 */
    private final boolean canModifyWidth;
    /** 是否可修改是否顯示 */
    private final boolean canSelectItem;
    /** 預設欄位寬度 */
    private final String defaultWidth;

    WRTraceSearchColumn(String name, boolean defaultShow, boolean canModifyWidth, boolean canSelectItem, String defaultWidth) {
        this.name = name;
        this.defaultShow = defaultShow;
        this.canModifyWidth = canModifyWidth;
        this.defaultWidth = defaultWidth;
        this.canSelectItem = canSelectItem;
    }

    @Override
    public boolean getCanSelectItem() {
        return canSelectItem;
    }

    @Override
    public boolean getDefaultShow() {
        return defaultShow;
    }

    @Override
    public boolean getCanModifyWidth() {
        return canModifyWidth;
    }

    @Override
    public String getDefaultWidth() {
        return defaultWidth;
    }

    @Override
    public String getVal() {
        return name;
    }
}
