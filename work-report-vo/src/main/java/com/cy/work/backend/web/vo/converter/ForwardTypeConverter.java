/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.enums.ForwardType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 轉寄類型Converter
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class ForwardTypeConverter implements AttributeConverter<ForwardType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(ForwardType attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.getKey();
    }

    @Override
    public ForwardType convertToEntityAttribute(Integer dbData) {
        if (dbData == null) {
            return null;
        }
        try {
            for (ForwardType ft : ForwardType.values()) {
                if (ft.getKey().equals(dbData)) {
                    return ft;
                }
            }
        } catch (Exception e) {
            log.error("ForwardType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
        return null;
    }

}
