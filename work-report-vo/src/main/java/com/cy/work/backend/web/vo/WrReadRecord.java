package com.cy.work.backend.web.vo;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "wr_read_record")
public class WrReadRecord implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1094406865752191505L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wr_read_record_sid", length = 36)
    private String readRecordSid;

    @Column(name = "read_status")
    private String readStatus;

    @Column(name = "status")
    private boolean status;

    @Column(name = "create_usr")
    private String createUser;

    @Column(name = "create_dt")
    private Date createDate;

    @Column(name = "update_usr")
    private String updateUser;

    @Column(name = "update_dt")
    private Date updateDate;

    @Column(name = "wr_sid")
    private String wrSid;

    @Column(name = "history")
    private String history;

    public WrReadRecord() {
    }

    public WrReadRecord(String readStatus, String createUser, Date createDate, String wrSid) {
        this.readStatus = readStatus;
        this.createUser = createUser;
        this.updateUser = createUser;
        this.createDate = createDate;
        this.updateDate = createDate;
        this.wrSid = wrSid;
    }
}
