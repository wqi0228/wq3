/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.converter.to.DepTo;
import com.cy.work.backend.web.vo.converter.to.LimitBaseAccessViewDep;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class LimitBaseAccessViewDepConverter implements AttributeConverter<LimitBaseAccessViewDep, String> {

    @Override
    public String convertToDatabaseColumn(LimitBaseAccessViewDep attribute) {
        try {
            if (attribute == null) {
                return "";
            }
            String json = this.toJson(attribute.getDepTos());
            return json;
        } catch (IOException ex) {
            log.error("parser GroupInfoController to json fail!!" + ex.getMessage(), ex);
        }
        return "";
    }
    
    private String toJson(Object object) throws IOException {
        ObjectWriter ow = new ObjectMapper().writer();
        return ow.writeValueAsString(object);
    }

    @Override
    public LimitBaseAccessViewDep convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            LimitBaseAccessViewDep to = new LimitBaseAccessViewDep();
            List<DepTo> resultList = WkJsonUtils.getInstance().fromJsonToList(dbData, DepTo.class);
            to.setDepTos(resultList);
            return to;
        } catch (IOException ex) {
            log.error("parser json to GroupInfoController fail!!" + ex.getMessage(), ex);
            return null;
        }
    }
}
