package com.cy.work.backend.web.vo.converter;

import java.io.IOException;

import javax.persistence.AttributeConverter;

import com.cy.work.backend.web.vo.converter.to.CustomClauseVO;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SearchInfoConverter implements AttributeConverter<CustomClauseVO, String> {

	@Override
	public String convertToDatabaseColumn(CustomClauseVO attribute) {
		if(attribute != null){
			try {
				return WkJsonUtils.getInstance().toJson(attribute);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
		}
		return null;
	}

	@Override
	public CustomClauseVO convertToEntityAttribute(String dbData) {
		if(!Strings.isNullOrEmpty(dbData)){
			try {
				return WkJsonUtils.getInstance().fromJson(dbData, CustomClauseVO.class);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
		}
		return null;
	}
}
