/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 主檔單據狀態類型
 * @author brain0925_liao
 */
public enum WRStatus {
    /** 草稿 */
    DRAFT("草稿"),
    /** 作廢 */
    INVIALD("作廢"),
     /** 刪除 */
    DELETE("刪除"),
    /** 發佈提交 */
    COMMIT("發佈提交");

    private final String val;

    WRStatus(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
