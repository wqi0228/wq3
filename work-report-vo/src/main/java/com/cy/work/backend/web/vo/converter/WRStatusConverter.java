/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.enums.WRStatus;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 工作報告單據狀態Converter
 * @author brain0925_liao
 */
@Slf4j
@Converter
public class WRStatusConverter implements AttributeConverter<WRStatus, String>{
    @Override
    public String convertToDatabaseColumn(WRStatus attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WRStatus convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WRStatus.valueOf(dbData);
        } catch (Exception e) {
            log.error("WRStatus Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
