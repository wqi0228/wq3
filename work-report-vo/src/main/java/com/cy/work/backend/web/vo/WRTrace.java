/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.vo.converter.WRTraceTypeConverter;
import com.cy.work.backend.web.vo.enums.WRTraceType;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.listener.RemoveContorlCharListener;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作報告追蹤
 * @author brain0925_liao
 */
@Entity
@Table(name = "wr_trace")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
@EntityListeners(RemoveContorlCharListener.class) //控制字元移除器
public class WRTrace implements Serializable {


    private static final long serialVersionUID = -7397776991113850842L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wr_trace_sid", length = 36)
    private String sid;

    @Column(name = "wr_sid", nullable = false, length = 36)
    private String wr_sid;

    @Column(name = "wr_no", nullable = false, length = 21)
    private String wr_no;
    
    @Convert(converter = WRTraceTypeConverter.class)
    @Column(name = "wr_trace_type", nullable = false, length = 45)
    private WRTraceType wr_trace_type;

    @Column(name = "wr_trace_content", nullable = true, length = 255)
    private String wr_trace_content;

    @Convert(converter = StringBlobConverter.class)
    @Column(name = "wr_trace_content_css", nullable = true)
    private String wr_trace_content_css;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;
    
      @Column(name = "create_usr", nullable = false)
    private Integer create_usr;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;

}
