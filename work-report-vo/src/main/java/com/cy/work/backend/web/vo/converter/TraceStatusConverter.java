/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.enums.TraceStatus;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 追蹤狀態Converter
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class TraceStatusConverter implements AttributeConverter<TraceStatus, String> {

    @Override
    public String convertToDatabaseColumn(TraceStatus attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public TraceStatus convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return TraceStatus.valueOf(dbData);
        } catch (Exception e) {
            log.error("TraceStatus Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
