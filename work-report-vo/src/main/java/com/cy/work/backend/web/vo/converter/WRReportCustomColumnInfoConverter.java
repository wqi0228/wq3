/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.converter.to.WRReportCustomColumnInfo;
import com.cy.work.backend.web.vo.converter.to.WRReportCustomColumnInfoTo;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 自訂欄位Converter
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class WRReportCustomColumnInfoConverter implements AttributeConverter<WRReportCustomColumnInfo, String> {

    @Override
    public String convertToDatabaseColumn(WRReportCustomColumnInfo attribute) {
        try {
            if (attribute == null || attribute.getWrReportCustomColumnInfoTos() == null) {
                return "";
            }
//            List<RRcordTo> rRcordTo = Lists.newArrayList();
//            attribute.getRRecord().forEach(item -> {
//                RRcordTo r = new RRcordTo(item.getReader(), item.getRead().name(), item.getReadDay());
//            });
            String json = WkJsonUtils.getInstance().toJson(attribute.getWrReportCustomColumnInfoTos()).replace("\r", "").replace("\n", "");
            //log.info("WRReportCustomColumnInfoConverter" + json);
            return json;
        } catch (IOException ex) {
            log.error("parser WRReportCustomColumnInfoConverter to json fail!!" + ex.getMessage(), ex);
        }
        return "";
    }

    @Override
    public WRReportCustomColumnInfo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new WRReportCustomColumnInfo();
        }
        try {
            WRReportCustomColumnInfo to = new WRReportCustomColumnInfo();
            List<WRReportCustomColumnInfoTo> resultList = WkJsonUtils.getInstance().fromJsonToList(dbData, WRReportCustomColumnInfoTo.class);
//            resultList.forEach(item -> {
//                RRecord r = new RRecord(item.getReader(), item.getReadDay(), WRReadStatus.valueOf(item.getRead()));
//                to.getRRecord().add(r);
//            });
            if (resultList != null) {
                to.setWrReportCustomColumnInfoTos(resultList);
            }
            return to;
        } catch (IOException ex) {
            log.error("parser json to WRReportCustomColumnInfo fail!!" + ex.getMessage(), ex);
            return new WRReportCustomColumnInfo();
        }
    }
}
