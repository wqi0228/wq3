package com.cy.work.backend.web.vo.enums;

public enum SqlColumnType {
	/** wr_tace.create_dt*/
	WT_CREATE_DATE("wt.createDate"),
	/** wr_tace.update_dt*/
	WT_UPDATE_DATE("wt.updateDate");
	
	private String value;
	
	private SqlColumnType(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
}
