/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 功能清單
 * @author brain0925_liao
 */
public enum WRFuncGroupBaseType {
     /**首頁*/
     HOME("3","首頁"),
     /**維護*/
     MAIN_TAIN("0","維護"),
     /**新增*/
     ADD("1","新增"),
     /**查詢*/
     SEARCH("4","查詢");
    
    private final String group_base_sid;
    
    private final String name;

    WRFuncGroupBaseType(String group_base_sid,String name) {
        this.group_base_sid = group_base_sid;
        this.name = name;
    }
    
    public String getKey(){
        return group_base_sid;
    }

    public String getValue() {
        return name;
    }
}
