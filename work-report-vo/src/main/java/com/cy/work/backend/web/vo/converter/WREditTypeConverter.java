/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.enums.WREditType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 編輯權限Converter
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class WREditTypeConverter  implements AttributeConverter<WREditType, String>{
    @Override
    public String convertToDatabaseColumn(WREditType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WREditType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WREditType.valueOf(dbData);
        } catch (Exception e) {
            log.error("WRStatus Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}

