/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 主檔追蹤類型
 * @author brain0925_liao
 */
public enum WRTraceType {
    /** 變更標籤 */
    MODIFY_TAG("變更標籤"),
    /** 編輯權限調整 */
    MODIFY_EDIT_TYPE("編輯權限調整"),
    /** 主題異動 */
    MODIFY_THEME("主題異動"),
    /** 內容異動 */
    MODIFY_CONTNET("內容異動"),
    /** 備註異動 */
    MODIFY_MEMO("備註異動"),
    /** 回覆 */
    REPLY("回覆"),
    /** 作廢 */
    INVAILD("作廢"),
    /** 刪除附加檔案 */
    DEL_ATTACHMENT("刪除附加檔案"),
    /** 刪除工作報告（草稿） */
    DEL_WR("刪除工作報告（草稿）"),
    /** 發佈提交 */
    COMMIT("發佈提交"),
     /** 提交 */
    SUMMIT("提交"),
    /** 新增/調整索取讀取記錄*/
    ADD_READ_RECEIPTS("新增/調整索取讀取記錄");

    private final String val;

    WRTraceType(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
