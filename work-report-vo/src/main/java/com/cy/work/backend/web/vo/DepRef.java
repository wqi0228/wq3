/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author brain0925_liao
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "dep_ref")
public class DepRef {

    @Id
    @Column(name = "PrimaryOrgSID", nullable = true)
    private Integer sid;

    @Column(name = "XS_PrimaryOrgSID", nullable = true)
    private Integer xsPrimaryOrgSID;
}
