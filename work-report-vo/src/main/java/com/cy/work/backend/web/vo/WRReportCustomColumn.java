/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo;

import com.cy.work.backend.web.vo.converter.SearchInfoConverter;
import com.cy.work.backend.web.vo.converter.WRReportCustomColumnInfoConverter;
import com.cy.work.backend.web.vo.converter.WRReportCustomColumnUrlConverter;
import com.cy.work.backend.web.vo.converter.to.CustomClauseVO;
import com.cy.work.backend.web.vo.converter.to.WRReportCustomColumnInfo;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作報告自定欄位設定值
 * @author brain0925_liao
 */
@Entity
@Table(name = "wr_report_custom_column")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WRReportCustomColumn implements Serializable {

    
    private static final long serialVersionUID = -8934411682340787931L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "custom_column_sid", length = 36)
    private String sid;

    @Convert(converter = WRReportCustomColumnUrlConverter.class)
    @Column(name = "url", nullable = false, length = 100)
    private WRReportCustomColumnUrlType url;

    @Column(name = "pagecnt", nullable = false)
    private Integer pagecnt;

    @Column(name = "type", nullable = false, length = 36)
    private String type;

    @Column(name = "usersid", nullable = false)
    private Integer usersid;

    @Column(name = "update_dt", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;

    @Convert(converter = WRReportCustomColumnInfoConverter.class)
    @Column(name = "info")
    private WRReportCustomColumnInfo info;

    @Convert(converter = SearchInfoConverter.class)
    @Column(name = "search_info")
    private CustomClauseVO customClauseVO;

}
