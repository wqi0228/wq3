/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 特殊參數
 * @author brain0925_liao
 */
public enum WorkParamEnum {
    
    BIG_BOSS_SIDS("config.big.boss.sids");
    
    private final String val;

    WorkParamEnum(String val) {
        this.val = val;
    }


    public String getVal() {
        return val;
    }
}
