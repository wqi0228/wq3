/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.enums.InboxType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 轉寄方式Converter
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class InboxTypeConverter implements AttributeConverter<InboxType, String> {

    @Override
    public String convertToDatabaseColumn(InboxType attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.name();
    }

    @Override
    public InboxType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return InboxType.valueOf(dbData);
        } catch (Exception e) {
            log.error("InboxType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
