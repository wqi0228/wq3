/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 * 僅轉寄查詢使用,已未讀類型
 * @author brain0925_liao
 */
public enum TransReadStatus {
    
    NEED_READ("需閱讀 (未&待閱讀)"),
    WAIT_READ("待閱讀"),
    NO_READ("未閱讀"),
    READED("已閱讀");
    
    private final String value;

    TransReadStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
