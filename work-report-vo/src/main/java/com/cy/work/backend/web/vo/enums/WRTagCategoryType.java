/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.enums;

/**
 *
 * @author brain0925_liao
 */
public enum WRTagCategoryType {
    /** 工作報告 */
    WORKREPORT("工作報告"),
    /** 測試 */
    TEST("測試");

    private final String val;

    WRTagCategoryType(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
