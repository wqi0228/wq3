/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.vo.converter;

import com.cy.work.backend.web.vo.enums.WRTraceType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 追蹤類型Converter
 * @author brain0925_liao
 */
@Slf4j
@Converter
public class WRTraceTypeConverter implements AttributeConverter<WRTraceType, String> {

    @Override
    public String convertToDatabaseColumn(WRTraceType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WRTraceType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WRTraceType.valueOf(dbData);
        } catch (Exception e) {
            log.error("WRTraceType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
