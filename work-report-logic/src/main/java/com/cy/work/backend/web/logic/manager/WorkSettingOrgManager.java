/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao Org物件管理器
 */
@Component
public class WorkSettingOrgManager implements Serializable {

    private static final long serialVersionUID = 7547352448381708156L;
    /** Org物件Cache管理器 */
    @Autowired
    private WkOrgCache wkOrgCache;

    /**
     * Find All Org
     *
     * @return
     */
    public List<Org> findAll() {
        return wkOrgCache.findAll();
    }

    /**
     * Find Org By SID
     *
     * @param sid
     * @return
     */
    public Org findBySid(Integer sid) {
        return wkOrgCache.findBySid(sid);
    }

    public Org findByID(String id) {
        return wkOrgCache.findById(id);
    }

    public List<Org> findChildOrgByParentSid(Integer parentSid) {
        return wkOrgCache.getChildOrgs(parentSid);
    }

    public String showParentDep(Org org) {
        if (org.getLevel() != null && (org.getLevel().equals(OrgLevel.GROUPS) || org.getLevel().equals(OrgLevel.DIVISION_LEVEL))) {
            return org.getName();
        }
        if (org.getType().equals(OrgType.HEAD_OFFICE) || org.getType().equals(OrgType.BRANCH_OFFICE)) {
            return org.getName();
        }
        return this.parseDivision(this.showParentDepInner(org));
    }

    /**
     * 顯示所有上層組織到「處」級為止
     *
     * @param org 部門
     * @return
     */
    public String showParentDepInner(Org org) {
        if (!OrgType.DEPARTMENT.equals(org.getType())) {
            return org.getName();
        }
        if (org.getParent() == null || org.getParent().getSid() == null
                || !OrgType.DEPARTMENT.equals(org.getParent().getType())) {
            return org.getName();
        }
        Org parentOrg = this.findBySid(org.getParent().getSid());
        return showParentDepInner(parentOrg) + "-" + org.getName();
    }

    private String parseDivision(String result) {
        if (!result.contains("群")) {
            return result;
        }
        String[] array = result.split("-");
        int groupCnt = 0;
        int divCnt = 0;
        for (String each : array) {
            if (each.contains("群")) {
                groupCnt++;
            }
            if (each.contains("處")) {
                divCnt++;
            }
        }
        if (groupCnt > 0 && divCnt > 0) {
            List<String> strList = Lists.newArrayList();
            int idx = 0;
            for (String each : array) {
                if (idx++ != 0) {
                    strList.add(each);
                }
            }
            return Joiner.on(" - ").join(strList);
        }
        return result;
    }

}
