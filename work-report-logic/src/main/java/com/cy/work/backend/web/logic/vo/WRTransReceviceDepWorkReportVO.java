/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.vo;

import com.cy.work.backend.web.vo.enums.WRReadStatus;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;

/**
 * 部門轉發報表轉出物件
 * @author brain0925_liao
 */
public class WRTransReceviceDepWorkReportVO implements Serializable {

    
    private static final long serialVersionUID = 8228726715342231240L;

    public WRTransReceviceDepWorkReportVO(String wr_Id, String no, boolean tofowardDep, boolean tofowardMember, boolean totrace, Integer sendUserDepSid, Integer sendUserSid, Date sendTime, Integer receviceDepSid, String wr_tag_sid, String theme, Date modifyDate, WRReadStatus wRReadStatus) {
        this.wr_Id = wr_Id;
        this.no = no;
        this.tofowardDep = tofowardDep;
        this.tofowardMember = tofowardMember;
        this.totrace = totrace;
        this.sendUserDepSid = sendUserDepSid;
        this.sendUserSid = sendUserSid;
        this.sendTime = sendTime;
        this.receviceDepSid = receviceDepSid;
        this.wr_tag_sid = wr_tag_sid;
        this.theme = theme;
        this.modifyDate = modifyDate;
        this.wRReadStatus = wRReadStatus;
    }
    /**工作報告Sid*/
    @Getter
    private final String wr_Id;
    /**工作報告單號*/
    @Getter
    private final String no;
    /**是否有轉寄部門*/
    @Getter
    private final boolean tofowardDep;
    /**是否有轉寄個人*/
    @Getter
    private final boolean tofowardMember;
    /**是否有追蹤*/
    @Getter
    private final boolean totrace;
    /**轉寄者部門Sid*/
    @Getter
    private final Integer sendUserDepSid;
    /**轉寄者Sid*/
    @Getter
    private final Integer sendUserSid;
    /**轉寄時間*/
    @Getter
    private final Date sendTime;
    /**接收部門Sid*/
    @Getter
    private final Integer receviceDepSid;
    /**標籤ID*/
    @Getter
    private final String wr_tag_sid;
    /**主題*/
    @Getter
    private final String theme;
    /**工作報告異動時間*/
    @Getter
    private final Date modifyDate;
    /**工作報告已未讀狀態*/
    @Getter
    private final WRReadStatus wRReadStatus;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.wr_Id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRTransReceviceDepWorkReportVO other = (WRTransReceviceDepWorkReportVO) obj;
        if (!Objects.equals(this.wr_Id, other.wr_Id)) {
            return false;
        }
        return true;
    }

    

   
    
    
}
