package com.cy.work.backend.web.logic.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.work.backend.web.vo.WRReportCustomColumn;
import com.cy.work.backend.web.vo.converter.to.CustomClauseVO;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;

@Component
public class WRReportCustomColumnHelper {

    @Autowired
    private WRReportCustomColumnManager wrReportCustomColumnManager;
    

    /**
     * 載入自訂預設搜尋條件
     */
    public CustomClauseVO loadCustomClauseVO(Integer userSid){
		WRReportCustomColumn entity = wrReportCustomColumnManager
				.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.WORK_REPORT_SEARCH, userSid);
    	return entity != null ? entity.getCustomClauseVO() : null;
    }
    
    /**
     * 儲存自訂預設搜尋條件
     */
    public void saveCustomClause(CustomClauseVO customClauseVO, Integer userSid){
		WRReportCustomColumn wrReportCustomColumn = wrReportCustomColumnManager
				.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.WORK_REPORT_SEARCH, userSid);
		if(wrReportCustomColumn == null){
			wrReportCustomColumn = new WRReportCustomColumn();
	    	wrReportCustomColumn.setUrl(WRReportCustomColumnUrlType.WORK_REPORT_SEARCH);
	    	wrReportCustomColumn.setPagecnt(50);
		}
		wrReportCustomColumn.setCustomClauseVO(customClauseVO);
    	wrReportCustomColumnManager.save(wrReportCustomColumn, userSid);
    }
}
