/**
 * 
 */
package com.cy.work.backend.web.logic.manager.setting6;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.testng.collections.Lists;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.work.backend.web.logic.manager.setting6.vo.Setting6VO;
import com.cy.work.backend.web.repository.NativeSqlRepository;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;

/**
 * @author allen1214_wu
 *
 */
@Service
public class Setting6Service implements Serializable {

    // ========================================================================
    //
    // ========================================================================


    private static final long serialVersionUID = 8364579677463710924L;
    @Autowired
    private NativeSqlRepository nativeSqlRepository;

    // ========================================================================
    //
    // ========================================================================
    /**
     * 準備使用記錄資料
     * 
     * @return
     */
    public List<Setting6VO> prepareUsedCount() {

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT dep_sid  AS depSid, ");
        varname1.append("       MAX(create_dt) as lastCreateDate, ");
        varname1.append("       Count(*) AS cnt ");
        varname1.append("FROM   wr_master ");
        varname1.append("WHERE  dep_sid IS NOT NULL ");
        varname1.append("GROUP  BY dep_sid");

        // ====================================
        // 查詢
        // ====================================
        List<Setting6VO> results = this.nativeSqlRepository.getResultList(
                varname1.toString(),
                null,
                Setting6VO.class);

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 整裡資料
        // ====================================
        for (Setting6VO setting6vo : results) {
            Org dep = WkOrgCache.getInstance().findBySid(setting6vo.getDepSid());
            if (dep == null) {
                setting6vo.setCompName("<span style='color:red;'>" + setting6vo.getDepSid() + "找不到資料</span>");
                setting6vo.setDepName("<span style='color:red;'>" + setting6vo.getDepSid() + "找不到資料</span>");
                continue;
            }

            setting6vo.setOrgOrder(WkOrgCache.getInstance().findOrgOrderSeq(dep));

            if (dep.getCompany() != null) {
                setting6vo.setCompName(dep.getCompany().getName());
            }

            setting6vo.setDepName(this.prepareOrgName(dep.getSid()));
            setting6vo.setLastCreateDateStr(WkDateUtils.formatDate(setting6vo.getLastCreateDate(), WkDateUtils.YYYY_MM_DD2));
        }

        // ====================================
        // 排序
        // ====================================
        return results.stream()
                .sorted(Comparator.comparing(Setting6VO::getOrgOrder))
                .collect(Collectors.toList());

    }

    private String prepareOrgName(Integer depSid) {
        // 組麵包屑樣式
        String bcreadcrumb = WkOrgUtils.prepareBreadcrumbsByDepName(depSid, OrgLevel.GROUPS, false, "~");
        // 標的單位凸顯樣式
        bcreadcrumb = WkOrgUtils.makeupDepName(bcreadcrumb, "~", "font-weight: bold;");

        if (!WkOrgUtils.isActive(depSid)) {
            bcreadcrumb = "<span class='WS1-1-2' style='text-decoration: line-through;'>(停用)&nbsp;"
                    + "<span class='WS1-1-1' style='text-decoration: none;'>" + bcreadcrumb + "</span>"
                    + "</span>";
        }

        return bcreadcrumb;
    }

}
