/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.vo;

import java.io.Serializable;

/**
 *
 * @author brain0925_liao 角色Json轉換物件
 */
public class RoleSIDVO  implements Serializable {


    private static final long serialVersionUID = -4715722295631708790L;
    /** 角色SID */
    private String roleSID;

    public RoleSIDVO() {
    }

    public RoleSIDVO(String roleSID) {
        this.roleSID = roleSID;
    }

    public String getRoleSID() {
        return roleSID;
    }

    public void setRoleSID(String roleSID) {
        this.roleSID = roleSID;
    }

}
