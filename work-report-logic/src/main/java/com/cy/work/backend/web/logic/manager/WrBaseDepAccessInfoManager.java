/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.commons.vo.Org;
import com.cy.work.backend.web.logic.cache.WrBaseDepAccessInfoCache;
import com.cy.work.backend.web.repository.WrBaseDepAccessInfoRepository;
import com.cy.work.backend.web.vo.WrBaseDepAccessInfo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 基礎單位可閱權限設定
 *
 * @author brain0925_liao
 */
@Component
public class WrBaseDepAccessInfoManager implements Serializable {


    private static final long serialVersionUID = -7077850668823194452L;

    /** 基礎單位可閱權限設定Cache */
    @Autowired
    private WrBaseDepAccessInfoCache wrBaseDepAccessInfoCache;
    /** WrBaseDepAccessInfoRepository */
    @Autowired
    private WrBaseDepAccessInfoRepository wrBaseDepAccessInfoRepository;
    /** WorkSettingOrgManager */
    @Autowired
    private WorkSettingOrgManager workSettingOrgManager;

    /**
     * 取得基礎單位可閱權限設定
     *
     * @param depSid 部門Sid
     * @return
     */
    public WrBaseDepAccessInfo getByLoginDepSid(Integer depSid) {
        return wrBaseDepAccessInfoCache.getByDepSid(depSid);
    }

    /**
     * 取得基礎單位限制部門
     *
     * @param wrBaseDepAccessInfoDepSid 部門Sid
     * @return
     */
    public List<Org> getLimitBaseAccessViewDep(Integer wrBaseDepAccessInfoDepSid) {
        WrBaseDepAccessInfo wrBaseDepAccessInfo = getByLoginDepSid(wrBaseDepAccessInfoDepSid);
        if (wrBaseDepAccessInfo == null || wrBaseDepAccessInfo.getLimitBaseAccessViewDep() == null || wrBaseDepAccessInfo.getLimitBaseAccessViewDep().getDepTos() == null) {
            return null;
        }
        List<Org> limitBaseAccessViewDeps = Lists.newArrayList();
        wrBaseDepAccessInfo.getLimitBaseAccessViewDep().getDepTos().forEach(item -> {
            limitBaseAccessViewDeps.add(workSettingOrgManager.findBySid(item.getSid()));
        });
        return limitBaseAccessViewDeps;
    }

    /**
     * 取得基礎單位增加部門
     *
     * @param wrBaseDepAccessInfoDepSid 部門Sid
     * @return
     */
    public List<Org> getOtherBaseAccessViewDep(Integer wrBaseDepAccessInfoDepSid) {
        WrBaseDepAccessInfo wrBaseDepAccessInfo = getByLoginDepSid(wrBaseDepAccessInfoDepSid);
        if (wrBaseDepAccessInfo == null || wrBaseDepAccessInfo.getOtherBaseAccessViewDep() == null || wrBaseDepAccessInfo.getOtherBaseAccessViewDep().getDepTos() == null) {
            return Lists.newArrayList();
        }
        List<Org> otherBaseAccessViewDeps = Lists.newArrayList();
        wrBaseDepAccessInfo.getOtherBaseAccessViewDep().getDepTos().forEach(item -> {
            otherBaseAccessViewDeps.add(workSettingOrgManager.findBySid(item.getSid()));
        });
        return otherBaseAccessViewDeps;
    }

    /**
     * 建立基礎單位可閱權限設定
     *
     * @param wrBaseDepAccessInfo 基礎單位可閱權限設定物件
     * @param loginUserSid 建立者Sid
     */
    public void createWrBaseDepAccessInfo(WrBaseDepAccessInfo wrBaseDepAccessInfo, Integer loginUserSid) {
        wrBaseDepAccessInfo.setCreate_dt(new Date());
        wrBaseDepAccessInfo.setCreate_usr_sid(loginUserSid);
        wrBaseDepAccessInfo = wrBaseDepAccessInfoRepository.save(wrBaseDepAccessInfo);
        wrBaseDepAccessInfoCache.updateCacheBySid(wrBaseDepAccessInfo);
    }

    /**
     * 更新基礎單位可閱權限設定
     *
     * @param wrBaseDepAccessInfo 基礎單位可閱權限設定物件
     * @param loginUserSid 更新者Sid
     */
    public void updateWrBaseDepAccessInfo(WrBaseDepAccessInfo wrBaseDepAccessInfo, Integer loginUserSid) {
        wrBaseDepAccessInfo.setUpdate_dt(new Date());
        wrBaseDepAccessInfo.setUpdate_usr_sid(loginUserSid);
        wrBaseDepAccessInfo = wrBaseDepAccessInfoRepository.save(wrBaseDepAccessInfo);
        wrBaseDepAccessInfoCache.updateCacheBySid(wrBaseDepAccessInfo);
    }

    /**
     * 找出已設定的可閱權限
     * @return
     */
    public List<WrBaseDepAccessInfo> findOtherBaseAccessViewDepNotNull() {
        return wrBaseDepAccessInfoRepository.findAllByOtherBaseAccessViewDepNotNull();
    }

    /**
     * 找出已設定的限制可閱
     * @return
     */
    public List<WrBaseDepAccessInfo> findLimitBaseAccessViewDepNotNull() {
        return wrBaseDepAccessInfoRepository.findAllByLimitBaseAccessViewDepNotNull();
    }
}
