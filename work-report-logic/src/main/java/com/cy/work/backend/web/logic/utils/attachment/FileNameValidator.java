/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.utils.attachment;

/**
 *
 * @author brain0925_liao
 */
public class FileNameValidator {

    public static final int MAX_ALLOWED_FILENAME_LENGTH = 255;

    protected static final char[] NOT_ALLOWED_CHARACTERS = {'\\', ':', '/', '*', '?', '\"', '<', '>',
        '|'};

    private String fileName;

    private boolean acceptable;

    private String validateMessage;

    public FileNameValidator(String fileName) {
        this.fileName = fileName;
        this.acceptable = validateFileName(fileName);
    }

    private boolean validateFileName(String fileName) {
        StringBuilder msg = new StringBuilder();
        boolean valid = true;

        if (fileName.length() > MAX_ALLOWED_FILENAME_LENGTH) {
            msg.append("檔名超過 ");
            msg.append(MAX_ALLOWED_FILENAME_LENGTH);
            msg.append(" 個字元！");
            valid = false;
        }

        for (char ch : NOT_ALLOWED_CHARACTERS) {
            if (fileName.indexOf(Character.getNumericValue(ch)) != -1) {
                msg.append("檔名含有特殊字元 ");
                msg.append(NOT_ALLOWED_CHARACTERS);
                valid = false;
                break;
            }
        }

        validateMessage = msg.toString();

        return valid;
    }

    public String getFileName() {
        return fileName;
    }

    public boolean isAcceptable() {
        return acceptable;
    }

    public String getValidateMessage() {
        return validateMessage;
    }

}
