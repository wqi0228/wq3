/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.cache.WRTagCache;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.vo.WRTag;
import com.cy.work.backend.web.vo.WRTrace;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.WREditType;
import com.cy.work.backend.web.vo.enums.WRTraceType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * 追蹤邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class WRTraceCustomLogicManager implements InitializingBean, Serializable {
    private static final long serialVersionUID = 5128644649694918439L;
    @Autowired
    private WRTraceManager wRTraceManager;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WRTagCache wRTagCache;

    private static WRTraceCustomLogicManager instance;

    public static WRTraceCustomLogicManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRTraceCustomLogicManager.instance = this;
    }

    /**
     * 取得追蹤By 追蹤Sid
     *
     * @param sid 追蹤Sid
     * @return
     */
    public WRTrace getWRTraceBySid(String sid) {
        return wRTraceManager.getWRTraceBySid(sid);
    }

    /**
     * 取得變更標籤追蹤物件
     *
     * @param oriTagID    原本標籤ID
     * @param targetTagID 更新後標籤ID
     * @param userSid     變更者Sid
     * @param wr_sid      工作報告Sid
     * @param wr_no       工作報告單號
     * @return
     */
    public WRTrace getWRTrace_MODIFY_TAG(String oriTagID, String targetTagID,
                                         Integer userSid, String wr_sid, String wr_no) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.MODIFY_TAG);
        WRTag oriTag = wRTagCache.findBySid(oriTagID);
        WRTag targetTag = wRTagCache.findBySid(targetTagID);
        String content = "標籤名稱由[" + oriTag.getTag_name() + "] 變更為 ["
                + targetTag.getTag_name() + "]";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

    /**
     * 取得提交追蹤物件
     *
     * @param userSid 提交者Sid
     * @param wr_sid  工作報告Sid
     * @param wr_no   工作報告單號
     * @return
     */
    public WRTrace getWRTrace_COMMIT(Integer userSid, String wr_sid, String wr_no) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.COMMIT);
        String content = workSettingUserManager.findBySID(userSid).getName()
                + "於[" + ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date()) + "] 發佈提交";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立修改主題追蹤物件
     *
     * @param oriContent    原本主題內容
     * @param targetContent 更新後主題內容
     * @param userSid       更新者Sid
     * @param wr_sid        工作報告Sid
     * @param wr_no         工作報告單號
     * @return
     */
    public WRTrace getWRTrace_MODIFY_THEME(String oriContent, String targetContent,
                                           Integer userSid, String wr_sid, String wr_no) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.MODIFY_THEME);
        String content = "主題由[" + oriContent + "] 變更為 ["
                + targetContent + "]";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立修改內容追蹤物件
     *
     * @param oriContent    原本內容
     * @param targetContent 修改後內容
     * @param userSid       修改者Sid
     * @param wr_sid        工作報告Sid
     * @param wr_no         工作報告單號
     * @return
     */
    public WRTrace getWRTrace_MODIFY_CONTNET(String oriContent, String targetContent,
                                             Integer userSid, String wr_sid, String wr_no) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.MODIFY_CONTNET);
//        String content = "變更前 [" + oriContent + "] 變更後 ["
//                + targetContent + "]";
        String content = "內容資訊異動";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立修改備註追蹤物件
     *
     * @param oriContent    原本備註內容
     * @param targetContent 修改後備註內容
     * @param userSid       修改者Sid
     * @param wr_sid        工作報告Sid
     * @param wr_no         工作報告單號
     * @return
     */
    public WRTrace getWRTrace_MODIFY_MEMO(String oriContent, String targetContent,
                                          Integer userSid, String wr_sid, String wr_no) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.MODIFY_MEMO);
//        String content = "變更前 [" + oriContent + "] 變更後 ["
//                + targetContent + "]";
        String content = "備註資訊異動";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立修改編輯權限追蹤物件
     *
     * @param oriEditTypeID    原本編輯權限ID
     * @param targetEditTypeID 修改後編輯權限ID
     * @param userSid          修改者Sid
     * @param wr_sid           工作報告Sid
     * @param wr_no            工作報告單號
     * @return
     */
    public WRTrace getWRTrace_MODIFY_EDIT_TYPE(String oriEditTypeID, String targetEditTypeID,
                                               Integer userSid, String wr_sid, String wr_no) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.MODIFY_EDIT_TYPE);
        WREditType oriWREditType = WREditType.valueOf(oriEditTypeID);
        WREditType targeWREditType = WREditType.valueOf(targetEditTypeID);

        String content = "編輯權限由 [" + oriWREditType.getValue() + "] 變更為 ["
                + targeWREditType.getValue() + "]";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立新增回覆追蹤物件
     *
     * @param content 回覆內容
     * @param userSid 回覆者Sid
     * @param wr_sid  工作報告Sid
     * @param wr_no   工作報告單號
     * @return
     */
    public WRTrace getWRTrace_REPLY(String content, Integer userSid, String wr_sid, String wr_no) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.REPLY);
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立刪除附件追蹤物件
     *
     * @param attSid        附件Sid
     * @param fileName      附件名稱
     * @param createUserSid 建立者Sid
     * @param deleteUserSid 刪除者Sid
     * @param wr_sid        工作報告Sid
     * @param wr_no         工作報告單號
     * @return
     */
    public WRTrace getWRTrace_DEL_ATTACHMENT(String attSid, String fileName, Integer createUserSid, Integer deleteUserSid, String wr_sid, String wr_no) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.DEL_ATTACHMENT);
        wrt.setStatus(Activation.ACTIVE);
        User createUser = workSettingUserManager.findBySID(createUserSid);
        User deleteUser = workSettingUserManager.findBySID(deleteUserSid);
        String content = ""
                + "檔案名稱：【" + fileName + "】\n"
                + "上傳成員：【" + ((createUser != null) ? createUser.getName() : "") + "】\n"
                + "\n"
                + "刪除來源：【工作報告 － 單號：" + wr_no + "  】\n"
                + "刪除成員：【" + ((deleteUser != null) ? deleteUser.getName() : "") + "】\n";
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立新增/調整索取讀取記錄追蹤物件
     *
     * @param wr_sid 工作報告Sid
     * @param wr_no  工作報告單號
     * @return
     */
    public WRTrace getWRTrace_ADD_READ_RECEIPTS(String wr_sid, String wr_no) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.ADD_READ_RECEIPTS);
        wrt.setStatus(Activation.ACTIVE);
        String content = "新增/調整索取讀取記錄";
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立刪除工作報告追蹤物件
     *
     * @param wr_sid        工作報告Sid
     * @param wr_no         工作報告單號
     * @param deleteUserSid 刪除者Sid
     * @return
     */
    public WRTrace getWRTrace_DEL_WR(String wr_sid, String wr_no, Integer deleteUserSid) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.DEL_WR);
        wrt.setStatus(Activation.ACTIVE);
        String content = "工作報告已被【" + ((workSettingUserManager.findBySID(deleteUserSid) != null) ? workSettingUserManager.findBySID(deleteUserSid).getName() : "") + "】執行刪除";
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立作廢工作報告追蹤物件
     *
     * @param wr_sid 工作報告Sid
     * @param wr_no  工作報告單號
     * @param reason 作廢理由
     * @return
     */
    public WRTrace getWRTrace_INVAILD(String wr_sid, String wr_no, String reason) {
        WRTrace wrt = new WRTrace();
        wrt.setWr_sid(wr_sid);
        wrt.setWr_no(wr_no);
        wrt.setWr_trace_type(WRTraceType.INVAILD);
        wrt.setStatus(Activation.ACTIVE);
        String content = "作廢原因 :" + reason;
        wrt.setWr_trace_content(content);
        wrt.setWr_trace_content_css(content);
        return wrt;
    }

}
