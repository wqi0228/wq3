/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.utils;

import com.cy.work.backend.web.vo.SimpleDateFormatThreadSafe;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class ToolsDate implements Serializable {


    private static final long serialVersionUID = -2309749796830989939L;

    public static Date transStringToDate(SimpleDateFormatThreadSafe simpleDateFormat, String str) {
        if (Strings.isNullOrEmpty(str)) {
            return null;
        }

        try {
            return simpleDateFormat.parse(str);
        } catch (Exception e) {
            log.error("transStringToDate Error", e);
        }
        return new Date();
    }

    public static String transDateToString(SimpleDateFormatThreadSafe simpleDateFormat, Date date) {
        if (date == null) {
            return "";
        }
        try {
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            log.error("transDateToString Error", e);
        }
        return "";
    }

    public static String transDateToChineseString(Date date) {
        if (date == null) {
            return "";
        }
        try {
            String dateString = transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), date);
            String[] dateArray = dateString.split("/");
            return dateArray[0] + "年" + dateArray[1] + "月" + dateArray[2] + "號";
        } catch (Exception e) {
            log.error("transDateToChineseString Error", e);
        }
        return "";

    }

}
