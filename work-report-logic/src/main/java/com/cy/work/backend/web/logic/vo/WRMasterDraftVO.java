/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;

/**
 * 工作報告草稿報表轉出物件
 *
 * @author brain0925_liao
 */
public class WRMasterDraftVO implements Serializable {

    private static final long serialVersionUID = 4898529322593986578L;

    public WRMasterDraftVO(String sid, Date create_dt, String wr_tag_sid,
            Integer dep_sid, Integer create_usr, String theme, String wr_status, Date update_dt, String wr_no) {
        this.sid = sid;
        this.create_dt = create_dt;
        this.wr_tag_sid = wr_tag_sid;
        this.dep_sid = dep_sid;
        this.create_usr = create_usr;
        this.theme = theme;
        this.wr_status = wr_status;
        this.update_dt = update_dt;
        this.wr_no = wr_no;
    }
    /** 工作報告Sid */
    @Getter
    private final String sid;
    /** 草稿建立日期 */
    @Getter
    private final Date create_dt;
    /** 標籤ID */
    @Getter
    private final String wr_tag_sid;
    /** 建立部門Sid */
    @Getter
    private final Integer dep_sid;
    /** 建立者Sid */
    @Getter
    private final Integer create_usr;
    /** 主題 */
    @Getter
    private final String theme;
    /** 單據狀態 */
    @Getter
    private final String wr_status;
    /** 草稿更新日期 */
    @Getter
    private final Date update_dt;
    /** 工作報告單號 */
    @Getter
    private final String wr_no;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRMasterDraftVO other = (WRMasterDraftVO) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }

    
}
