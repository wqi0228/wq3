/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.vo;

import com.cy.work.backend.web.vo.enums.WRReadStatus;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;

/**
 * 轉寄個人類報表轉出物件
 * @author brain0925_liao
 */
public class WRTransIncomeWorkReportVO implements Serializable {

    
    private static final long serialVersionUID = 120781888343652928L;

    public WRTransIncomeWorkReportVO(String wr_Id, String no, boolean tofowardDep, boolean tofowardMember, boolean totrace, Integer sendUserDepSid, Integer sendUserSid, Date sendTime, String wr_tag_sid, String theme, Date modifyDate, WRReadStatus wRReadStatus, String sid) {
        this.sid = sid;
        this.wr_Id = wr_Id;
        this.no = no;
        this.tofowardDep = tofowardDep;
        this.tofowardMember = tofowardMember;
        this.totrace = totrace;
        this.sendUserDepSid = sendUserDepSid;
        this.sendUserSid = sendUserSid;
        this.sendTime = sendTime;
        this.wr_tag_sid = wr_tag_sid;
        this.theme = theme;
        this.modifyDate = modifyDate;
        this.wRReadStatus = wRReadStatus;
    }
    /**轉寄細項Sid*/
    @Getter
    private final String sid;
    /**工作報告Sid*/
    @Getter
    private final String wr_Id;
    /**工作報告單號*/
    @Getter
    private final String no;
    /**是否有轉寄部門*/
    @Getter
    private final boolean tofowardDep;
    /**是否有轉寄個人*/
    @Getter
    private final boolean tofowardMember;
    /**是否有追蹤*/
    @Getter
    private final boolean totrace;
    /**轉寄者部門Sid*/
    @Getter
    private final Integer sendUserDepSid;
    /**轉寄者Sid*/
    @Getter
    private final Integer sendUserSid;
    /**轉寄時間*/
    @Getter
    private final Date sendTime;
    /**標籤Sid*/
    @Getter
    private final String wr_tag_sid;
    /**主題*/
    @Getter
    private final String theme;
    /**工作報告異動時間*/
    @Getter
    private final Date modifyDate;
    /**工作報告已未讀狀態*/
    @Getter
    private final WRReadStatus wRReadStatus;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.wr_Id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRTransIncomeWorkReportVO other = (WRTransIncomeWorkReportVO) obj;
        if (!Objects.equals(this.wr_Id, other.wr_Id)) {
            return false;
        }
        return true;
    }

}
