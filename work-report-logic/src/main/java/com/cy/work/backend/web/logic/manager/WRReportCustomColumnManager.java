/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.work.backend.web.vo.WRReportCustomColumn;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.cy.work.backend.web.repository.WRReportCustomColumnRepository;

/**
 * 自訂欄位
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRReportCustomColumnManager implements Serializable {

    
    private static final long serialVersionUID = -6748805890868383174L;
    @Autowired
    private WRReportCustomColumnRepository wrReportCustomColumnRepository;

    /**
     * 儲存自訂欄位設定
     *
     * @param wrReportCustomColumn 自訂欄位設定物件
     * @param createUserSid 儲存者Sid
     * @return
     */
    public WRReportCustomColumn save(WRReportCustomColumn wrReportCustomColumn, Integer createUserSid) {
        try {
            wrReportCustomColumn.setUsersid(createUserSid);
            wrReportCustomColumn.setType("WR");
            Date date = new Date();
            wrReportCustomColumn.setUpdate_dt(date);
            WRReportCustomColumn wr = wrReportCustomColumnRepository.save(wrReportCustomColumn);
            return wr;
        } catch (Exception e) {
            log.error("save Error : " + e.toString(), e);
        }
        return null;
    }

    /**
     * 取得自訂欄位設定 By Url 及 登入者Sid
     *
     * @param wrReportCustomColumnUrlType Url列舉
     * @param userSid 登入者Sid
     * @return
     */
    public WRReportCustomColumn getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType wrReportCustomColumnUrlType, Integer userSid) {
        try {
            List<WRReportCustomColumn> wrReportCustomColumns = wrReportCustomColumnRepository.getWRReportCustomColumnByUrlAndUserSid(wrReportCustomColumnUrlType, userSid);
            if (wrReportCustomColumns != null && wrReportCustomColumns.size() > 0) {
                return wrReportCustomColumns.get(0);
            }
        } catch (Exception e) {
            log.error("getWRReportCustomColumnByUrlAndUserSid Error ", e);
        }
        return null;
    }
}
