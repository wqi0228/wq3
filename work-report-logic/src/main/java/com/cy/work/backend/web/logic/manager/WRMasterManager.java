/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.query.WRMasterQueryService;
import com.cy.work.backend.web.logic.utils.ReqularPattenUtils;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRMasterDraftVO;
import com.cy.work.backend.web.logic.vo.WRMasterLastModifyVO;
import com.cy.work.backend.web.logic.vo.WRMasterWorkReportVO;
import com.cy.work.backend.web.repository.WRMasterRepository;
import com.cy.work.backend.web.vo.WRMaster;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMember;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMemberTo;
import com.cy.work.backend.web.vo.enums.*;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 工作報告
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRMasterManager implements Serializable {

    private static final long serialVersionUID = -8052433384316630776L;

    @Autowired
    private WRMasterRepository wRMasterRepository;
    @Autowired
    private WRMasterQueryService wRMasterQueryService;
    @Autowired
    private WorkSettingOrgManager workSettingOrgManager;
    @Autowired
    private ReqularPattenUtils reqularUtils;
    @Autowired
    private WrBasePersonAccessInfoManager wrBasePersonAccessInfoManager;
    @Autowired
    private WrBaseDepAccessInfoManager wrBaseDepAccessInfoManager;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WrReadRecordManager wrReadRecordManager;

    public List<WRMaster> getByCreateUserSid(Integer createUserSid) {
        return wRMasterRepository.findByCreateUserSid(createUserSid);
    }

    /**
     * 取得工作報告 By Sid
     *
     * @param sid 工作報告Sid
     * @return
     */
    public WRMaster findBySid(String sid) {
        return wRMasterRepository.findBySid(sid);
    }

    /**
     * 建立工作報告
     *
     * @param wrMaster      工作報告物件
     * @param createUserSid 建立者Sid
     * @return
     */
    public WRMaster create(WRMaster wrMaster, Integer createUserSid) {
        try {
            wrMaster.setCreate_usr(createUserSid);
            Date createDate = new Date();
            wrMaster.setCreate_dt(createDate);
            wrMaster.setUpdate_dt(createDate);
            WRMaster wr = wRMasterRepository.save(wrMaster);
            return wr;
        } catch (Exception e) {
            log.error("save Error : " + e.toString(), e);
        }
        return null;
    }

    /**
     * 更新工作報告 (不更新修改時間及修改者)
     *
     * @param wrMaster 工作報告物件
     * @return
     */
    public WRMaster updateOnlyWRMaster(WRMaster wrMaster) {
        try {
            WRMaster wr = wRMasterRepository.save(wrMaster);
            return wr;
        } catch (Exception e) {
            log.error("save Error : " + e.toString(), e);
        }
        return null;
    }

    public void updateReadReceiptsMember(ReadReceiptsMember readReceiptsMember, String wr_sid) {
        wRMasterRepository.updateReadReceiptsMember(readReceiptsMember, wr_sid);
    }

    /**
     * 更新工作報告(更新修改時間及修改者)
     *
     * @param wrMaster      工作報告物件
     * @param updateUserSid 更新者Sid
     * @return
     */
    public WRMaster update(WRMaster wrMaster, Integer updateUserSid) {
        try {
            wrMaster.setUpdate_usr(updateUserSid);
            wrMaster.setUpdate_dt(new Date());
            WRMaster wr = wRMasterRepository.save(wrMaster);
            return wr;
        } catch (Exception e) {
            log.error("save Error : " + e.toString(), e);
        }
        return null;
    }

    /**
     * 取得最後一筆工作報告單號 By 工作報告單據狀態及 建立時間區間
     *
     * @param start    建立起始時間
     * @param end      建立結束時間
     * @param wRStatus 工作報告單據狀態
     * @return
     */
    public String findTodayLastWRMaster(Date start, Date end, WRStatus wRStatus, Integer companySid) {
        List<String> nos = null;
        if (wRStatus.equals(WRStatus.COMMIT)) {
            nos = wRMasterRepository.findTodayLastWRMaster(start, end, companySid);
        } else if (wRStatus.equals(WRStatus.DRAFT)) {
            nos = wRMasterRepository.findTodayLastTempWRMaster(start, end, companySid);
        }
        if (CollectionUtils.isNotEmpty(nos)) {
            return nos.get(0);
        }
        return "";
    }

    /**
     * 取得建立筆數 By 工作報告單據狀態及 建立時間區間
     *
     * @param start    建立起始時間
     * @param end      建立結束時間
     * @param wRStatus 工作報告單據狀態
     * @return
     */
    public Integer findTodayWRMasterTotalSize(Date start, Date end, WRStatus wRStatus, Integer companySid) {
        try {
            if (wRStatus.equals(WRStatus.COMMIT)) {
                return wRMasterRepository.findTodayWRMasterTotalSize(start, end, companySid);
            } else if (wRStatus.equals(WRStatus.DRAFT)) {
                return wRMasterRepository.findTodayTempWRMasterTotalSize(start, end, companySid);
            }
        } catch (Exception e) {
            log.error("findTodayWRMasterTotalSize Error : " + e.toString(), e);
        }
        return 0;
    }

    public void updateLastModifyTime(String wr_sid, Date lastDate) {
        wRMasterRepository.updateLastModifyTime(lastDate, wr_sid);
    }

    public void updateLockUserAndLockDate(String wr_sid, Date lockDate, Integer lockUserSid) {
        wRMasterRepository.updateLockUserAndLockDate(lockDate, lockUserSid, wr_sid);
    }

    public void closeLockUserAndLockDate(String wr_sid, Integer closeUserSid) {
        wRMasterRepository.closeLockUserAndLockDate(null, null, wr_sid, closeUserSid);
    }

    public void updateLastModifyMemo(String wr_sid, String memo, String memoCss) {
        wRMasterRepository.updateLastModifyMemo(memo, memoCss, wr_sid);
    }

    /**
     * 取得工作報告內設定索取讀取回條的部門List By 登入者Sid
     *
     * @param searchUserSid 登入者Sid
     * @return
     */
    public List<Org> findSendReadReceiptsOrg(Integer searchUserSid) {
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT wr.dep_sid FROM wr_master wr ");
        builder.append(" WHERE 1=1 ");
        String read_receipts = "%" + "\"reader\" : \"" + reqularUtils.replaceIllegalSqlLikeStr(String.valueOf(searchUserSid)) + "\"" + "%";
        builder.append(" AND wr.read_receipts_member LIKE :read_receipts_member");
        parameters.put("read_receipts_member", read_receipts);
        builder.append(" AND wr.wr_status in (:wr_status) ");
        List<String> wrStatus = Lists.newArrayList();
        wrStatus.add(WRStatus.COMMIT.name());
        wrStatus.add(WRStatus.INVIALD.name());
        parameters.put("wr_status", wrStatus);
        List<Org> orgs = Lists.newArrayList();
        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        for (int i = 0; i < result.size(); i++) {
            try {
                Integer dep_sid = (Integer) result.get(i);
                orgs.add(workSettingOrgManager.findBySid(dep_sid));
            } catch (Exception e) {
                log.error("findWorkReportWRMaster Error", e);
            }

        }
        return orgs;
    }

    public List<String> findSendReadReceiptswrSids(Integer searchUserSid) {
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT wr.wr_sid FROM wr_master wr ");
        builder.append(" WHERE 1=1 ");
        String read_receipts = "%" + "\"reader\" : \"" + reqularUtils.replaceIllegalSqlLikeStr(String.valueOf(searchUserSid)) + "\"" + "%";
        builder.append(" AND wr.read_receipts_member LIKE :read_receipts_member");
        parameters.put("read_receipts_member", read_receipts);
        List<String> wrSids = Lists.newArrayList();
        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        for (int i = 0; i < result.size(); i++) {
            try {
                String wrSid = (String) result.get(i);
                wrSids.add(wrSid);
            } catch (Exception e) {
                log.error("findWorkReportWRMaster Error", e);
            }

        }
        return wrSids;
    }

    public List<WRMasterLastModifyVO> finaWorkReportModifyEditDte(List<Integer> depSids, String no, String createUserName, String wr_id) {
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        builder.append(" "
                + "wr.wr_sid,"
                + "wr.create_usr,"
                + "wr.create_dt,"
                + "wr.dep_sid,"
                + "wr.theme,"
                + "wr.wr_no,"
                + "wr.last_modify_dt,"
                + "wr.last_modify_memo,"
                + "wr.last_modify_memo_css,"
                + "wr.edit_type,"
                + "wr.wr_status "
                + "FROM wr_master wr ");
        builder.append(" WHERE 1=1 ");
        builder.append(" AND wr.wr_status in (:wr_status)");
        List<String> ststusStrings = Lists.newArrayList();
        ststusStrings.add(WRStatus.COMMIT.name());
        ststusStrings.add(WRStatus.INVIALD.name());
        parameters.put("wr_status", ststusStrings);
        if (depSids != null && !depSids.isEmpty()) {
            builder.append(" AND wr.dep_sid IN (:dep_sids) ");
            parameters.put("dep_sids", depSids);
        }
        if (!Strings.isNullOrEmpty(no)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(no) + "%";
            builder.append(" AND wr.wr_no LIKE :wr_no");
            parameters.put("wr_no", textNo);
        }
        if (!Strings.isNullOrEmpty(createUserName.trim())) {
            List<User> likeUserList = workSettingUserManager.likeUserByUsername(createUserName.trim());
            if (CollectionUtils.isEmpty(likeUserList)) {
                return Lists.newArrayList();
            } else {
                builder.append(" AND wr.create_usr in (:userSids)");
                parameters.put("userSids", likeUserList.stream().map(User::getSid).collect(Collectors.toList()));
            }
        }
        if (!Strings.isNullOrEmpty(wr_id)) {
            builder.append(" AND wr.wr_sid = :wr_sid ");
            parameters.put("wr_sid", wr_id);
        }
        builder.append(" ORDER BY  wr.create_dt DESC ");
        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        List<WRMasterLastModifyVO> wRMasterLastModifyVOs = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            try {
                Object[] record = (Object[]) result.get(i);
                String wr_sid = (String) record[0];
                Integer create_usr = (Integer) record[1];
                Date create_dt = (Date) record[2];
                Integer dep_sid = (Integer) record[3];
                String themeR = (String) record[4];
                String wr_no = (String) record[5];
                Date last_modify_dt = (Date) record[6];
                String last_modify_memo = (String) record[7];
                String last_modify_memo_css = "";
                if (record[8] != null) {
                    byte[] messageByte = (byte[]) record[8];
                    last_modify_memo_css = messageByte == null ? "" : new String(messageByte, "UTF-8");
                }
                String edit_typeStr = (String) record[9];
                WREditType edit_type = WREditType.valueOf(edit_typeStr);
                String wr_statusStr = (String) record[10];
                WRStatus wr_status = WRStatus.valueOf(wr_statusStr);

                WRMasterLastModifyVO wRMasterLastModifyVO = new WRMasterLastModifyVO(wr_sid, create_usr, create_dt, dep_sid, themeR, wr_no, last_modify_dt, last_modify_memo,
                        last_modify_memo_css, wr_status, edit_type);
                wRMasterLastModifyVOs.add(wRMasterLastModifyVO);
            } catch (Exception e) {
                log.error("finaWorkReportModifyEditDte Error", e);
            }
        }
        return wRMasterLastModifyVOs;
    }

    /**
     * 取得工作報告查詢
     *
     * @param depSids          介面挑選的建立部門Sid(基礎部門) List
     * @param tagID            介面挑選標籤ID
     * @param status           介面挑選單據狀態ID
     * @param theme            主題
     * @param content          內容
     * @param searchUserSid    登入者Sid
     * @param startDate        建立時間起始
     * @param endDate          建立時間結束
     * @param sid              工作報告Sid
     * @param sendRendReceipts 介面挑選的建立部門Sid(索取讀取回條部門) List
     * @param searchUserDepSid 登入者部門Sid
     * @return
     */
    public List<WRMasterWorkReportVO> findWorkReportWRMaster(
            List<Integer> depSids,
            String tagID, String status, String theme,
            String content, Integer searchUserSid, Date startDate, Date endDate, String sid,
            List<Integer> sendRendReceipts,
            Integer searchUserDepSid,
            List<Integer> otherBaseViewDepSids, String createUserName, SqlColumnType sqlColumnType, String order) {
        if ((depSids == null || depSids.isEmpty()) && (sendRendReceipts == null || sendRendReceipts.isEmpty()) && (otherBaseViewDepSids == null || otherBaseViewDepSids.isEmpty())) {
            return Lists.newArrayList();
        }
        String trace_source_type = "WR";

        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        // 查詢轉寄部門
        builder.append(" (SELECT COUNT(wiDep.inbox_sid) as transDepCount FROM wr_inbox wiDep WHERE wiDep.status = 0"
                + " AND wiDep.wr_sid = wr.wr_sid"
                + " AND wiDep.forward_type = '" + ForwardType.DEP.getKey() + "'"
                // + " AND wiDep.receive_dep = " + searchUserdepSid
                + " )");
        builder.append(",");
        // 查詢轉寄個人
        builder.append(" (SELECT COUNT(wiPerson.inbox_sid) as transPersonCount FROM wr_inbox wiPerson WHERE wiPerson.status = 0"
                + " AND wiPerson.wr_sid = wr.wr_sid"
                + " AND wiPerson.forward_type = '" + ForwardType.PERSON.getKey() + "'"
                // + " AND wiPerson.receive = " + searchUserSid + ""
                + " )");
        builder.append(",");
        // 查詢追蹤個人
        builder.append(" (SELECT COUNT(wtp.tracesid) as personTraceCount FROM  wr_trace_info wtp WHERE wtp.status = 0 "
                + " AND wtp.trace_source_sid = wr.wr_sid "
                + " AND wtp.trace_source_type = '" + trace_source_type + "'"
                + " AND wtp.trace_status in ('" + TraceStatus.UN_FINISHED.name() + "')"
                + " AND wtp.trace_type in ('" + TraceType.PERSONAL.name() + "','" + TraceType.OTHER_PERSON.name() + "')"
                + " AND wtp.trace_usr = " + searchUserSid
                + ")");
        builder.append(",");
        // 查詢追蹤部門
        builder.append(" (SELECT COUNT(wtp.tracesid) as depTraceCount FROM  wr_trace_info wtp WHERE wtp.status = 0 "
                + " AND wtp.trace_source_sid = wr.wr_sid "
                + " AND wtp.trace_source_type = '" + trace_source_type + "'"
                + " AND wtp.trace_status in ('" + TraceStatus.UN_FINISHED.name() + "')"
                + " AND wtp.trace_type in ('" + TraceType.DEPARTMENT.name() + "')"
                + " AND wtp.trace_dep = " + searchUserDepSid + ")");
        builder.append(",");
        builder.append("  wr.wr_sid,wr.create_usr,wt.createDate,wr.dep_sid,wr.wr_tag_sid,wr.theme,wr.wr_status,wt.updateDate,wr.wr_no,wr.read_receipts_member,wr.edit_type FROM wr_master wr ");
        builder.append(
                " INNER JOIN (SELECT wr_sid, wr_no, max(create_dt) as updateDate,min(create_dt) as createDate FROM wr_trace  GROUP BY wr_sid, wr_no) wt ON wr.wr_sid=wt.wr_sid AND wr.wr_no=wt.wr_no");

        builder.append(" WHERE 1=1 ");
        if (!Strings.isNullOrEmpty(tagID)) {
            builder.append(" AND wr.wr_tag_sid = :wr_tag_sid");
            parameters.put("wr_tag_sid", tagID);
        }
        if (!Strings.isNullOrEmpty(sid)) {
            builder.append(" AND wr.wr_sid = :wr_sid");
            parameters.put("wr_sid", sid);
        }
        if (!Strings.isNullOrEmpty(theme)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(theme) + "%";
            builder.append(" AND wr.theme LIKE :theme");
            parameters.put("theme", textNo);
        }
        if (!Strings.isNullOrEmpty(content)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(content) + "%";
            builder.append(" AND wr.content LIKE :content");
            parameters.put("content", textNo);
        }
        if (!Strings.isNullOrEmpty(createUserName.trim())) {
            List<User> likeUserList = workSettingUserManager.likeUserByUsername(createUserName.trim());
            if (CollectionUtils.isEmpty(likeUserList)) {
                return Lists.newArrayList();
            } else {
                builder.append(" AND wr.create_usr in (:userSids)");
                parameters.put("userSids", likeUserList.stream().map(User::getSid).collect(Collectors.toList()));
            }
        }
        boolean needToAddOR = false;
        StringBuilder depSearch = new StringBuilder();
        depSearch.append(" AND ( ");
        if (depSids != null && depSids.size() > 0) {
            // 檢測是否基礎單位成員有限制成員名單
            List<User> limitBaseUser = wrBasePersonAccessInfoManager.getLimitBaseAccessViewUser(searchUserSid);
            // 檢測是否基礎單位有限制部門名單
            List<Org> limitBaseOrgs = wrBaseDepAccessInfoManager.getLimitBaseAccessViewDep(searchUserDepSid);
            // 確認限制成員規則
            if (limitBaseUser != null && limitBaseUser.size() > 0) {
                depSearch.append("(");
                // 若為空值,且not null,該登入者限制無任何成員可檢視
                if (limitBaseUser.isEmpty()) {
                    depSearch.append(" ( 1=2 ) ");
                } else {
                    boolean limitBaseUserNeedToAddOR = false;
                    for (User ui : limitBaseUser) {
                        if (!depSids.contains(ui.getPrimaryOrg().getSid())) {
                            continue;
                        }
                        // 限制成員及部門是交集,故需確認限制成員是否有在限制部門內,才可進行
                        if (limitBaseOrgs != null) {
                            Org userDep = workSettingOrgManager.findBySid(ui.getPrimaryOrg().getSid());
                            if (!limitBaseOrgs.contains(userDep)) {
                                continue;
                            }
                        }
                        if (limitBaseUserNeedToAddOR) {
                            depSearch.append(" OR ");
                        }
                        depSearch.append(" ( wr.create_usr = '" + ui.getSid() + "' ) ");
                        limitBaseUserNeedToAddOR = true;
                    }
                }
                // 增加判斷索取讀取回條
                depSearch.append(" OR ( wr.dep_sid IN (:limitSendRendReceipts) AND wr.read_receipts_member LIKE :limitRead_receipts_member ) ");
                String read_receipts = "%" + "\"reader\" : \"" + reqularUtils.replaceIllegalSqlLikeStr(String.valueOf(searchUserSid)) + "\"" + "%";
                parameters.put("limitSendRendReceipts", depSids);
                parameters.put("limitRead_receipts_member", read_receipts);
                depSearch.append(")");
            } // 確認限制部門規則
            else if (limitBaseOrgs != null && limitBaseOrgs.size() > 0) {
                depSearch.append("(");
                // 若為空值,且not null,該登入者限制無任何部門可檢視
                if (limitBaseOrgs.isEmpty()) {
                    depSearch.append(" ( 1=2 ) ");
                } else {

                    Set<Integer> orgSids = limitBaseOrgs.stream()
                            .map(Org::getSid)
                            .collect(Collectors.toSet());

                    Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerOrgSids(searchUserSid);
                    if (WkStringUtils.notEmpty(managerOrgSids)) {
                        orgSids.addAll(managerOrgSids);
                        Set<Integer> childSids = WkOrgCache.getInstance().findAllChildSids(managerOrgSids);
                        if (WkStringUtils.notEmpty(childSids)) {
                            orgSids.addAll(childSids);
                        }
                    }

                    orgSids = orgSids.stream()
                            .filter(orgSid -> depSids.contains(orgSid))
                            .collect(Collectors.toSet());

                    depSearch.append(" ( wr.dep_sid IN (" + orgSids.stream().map(v -> v + "").collect(Collectors.joining(",")) + ") ) ");
                    // parameters.put("dep_sids", limitOrgs);
                }
                // 增加判斷索取讀取回條
                depSearch.append(" OR ( wr.dep_sid IN (:limitSendRendReceipts) AND wr.read_receipts_member LIKE :limitRead_receipts_member ) ");
                String read_receipts = "%" + "\"reader\" : \"" + reqularUtils.replaceIllegalSqlLikeStr(String.valueOf(searchUserSid)) + "\"" + "%";
                parameters.put("limitSendRendReceipts", depSids);
                parameters.put("limitRead_receipts_member", read_receipts);
                depSearch.append(")");
            } else {
                depSearch.append(" ( wr.dep_sid IN (:dep_sids) ) ");
                parameters.put("dep_sids", depSids);
            }
            needToAddOR = true;
        }
        // 確認索取回條調整
        if (sendRendReceipts != null && !sendRendReceipts.isEmpty()) {
            if (needToAddOR) {
                depSearch.append(" OR ");
            }
            depSearch.append(" ( wr.dep_sid IN (:sendRendReceipts) AND wr.read_receipts_member LIKE :read_receipts_member ) ");
            String read_receipts = "%" + "\"reader\" : \"" + reqularUtils.replaceIllegalSqlLikeStr(String.valueOf(searchUserSid)) + "\"" + "%";
            parameters.put("sendRendReceipts", sendRendReceipts);
            parameters.put("read_receipts_member", read_receipts);
            needToAddOR = true;
        }
        // 確認增加權限部門及成員
        if (otherBaseViewDepSids != null && !otherBaseViewDepSids.isEmpty()) {
            if (needToAddOR) {
                depSearch.append(" OR ");
            }
            depSearch.append("(");
            List<User> otherBaseUsers = wrBasePersonAccessInfoManager.getOtherBaseAccessViewUser(searchUserSid);
            List<Org> otherBaseOrgs = wrBaseDepAccessInfoManager.getOtherBaseAccessViewDep(searchUserDepSid);
            List<Integer> otherSid = Lists.newArrayList();
            otherBaseOrgs.forEach(item -> {
                if (otherBaseViewDepSids.contains(item.getSid())) {
                    otherSid.add(item.getSid());
                }
            });
            if (otherSid.isEmpty()) {
                depSearch.append(" ( 1=2 ) ");
            } else {
                depSearch.append(" ( wr.dep_sid IN (:otherDep_sids) ) ");
                parameters.put("otherDep_sids", otherSid);
            }

            if (!otherBaseUsers.isEmpty()) {
                for (User uo : otherBaseUsers) {
                    if (!otherBaseViewDepSids.contains(uo.getPrimaryOrg().getSid())) {
                        continue;
                    }
                    depSearch.append(" OR ");
                    depSearch.append(" ( wr.dep_sid = '" + uo.getPrimaryOrg().getSid() + "' AND wr.create_usr = '" + uo.getSid() + "' ) ");
                }
            }
            // 增加判斷索取讀取回條
            depSearch.append(" OR ( wr.dep_sid IN (:otherSendRendReceipts) AND wr.read_receipts_member LIKE :otherRead_receipts_member ) ");
            String read_receipts = "%" + "\"reader\" : \"" + reqularUtils.replaceIllegalSqlLikeStr(String.valueOf(searchUserSid)) + "\"" + "%";
            parameters.put("otherSendRendReceipts", otherBaseViewDepSids);
            parameters.put("otherRead_receipts_member", read_receipts);
            depSearch.append(")");
        }
        depSearch.append(" ) ");
        builder.append(depSearch.toString());
        builder.append(" AND wr.wr_status in (:wr_status) ");
        List<String> wrStatus = Lists.newArrayList();
        if (!Strings.isNullOrEmpty(status)) {
            wrStatus.add(status);
        } else {
            wrStatus.add(WRStatus.COMMIT.name());
            wrStatus.add(WRStatus.INVIALD.name());
        }
        parameters.put("wr_status", wrStatus);

        if (startDate != null) {
            String startDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), startDate);
            builder.append(" AND " + sqlColumnType.getValue() + " >= :start_dt ");
            parameters.put("start_dt", startDateStr + " 00:00:00");
        }
        if (endDate != null) {
            String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), endDate);
            builder.append(" AND " + sqlColumnType.getValue() + " <= :end_dt ");
            parameters.put("end_dt", endDateStr + " 23:59:59");
        }
        builder.append(" ORDER BY " + sqlColumnType.getValue() + " " + order);
        List<WRMasterWorkReportVO> wRMasterWorkReportVOs = Lists.newArrayList();
        // log.info("query sql:{}", builder.toString());

        List<WrReadRecord> userReadRecordList = wrReadRecordManager.findAllByUserSid(searchUserSid + "");
        Map<String, String> userRecordMap = userReadRecordList.stream().collect(Collectors.toMap(WrReadRecord::getWrSid, WrReadRecord::getReadStatus));
        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        for (int i = 0; i < result.size(); i++) {
            try {
                Object[] record = (Object[]) result.get(i);
                Integer transDepCount = ((BigInteger) record[0]).intValue();
                Integer transPersonCount = ((BigInteger) record[1]).intValue();
                Integer personTraceCount = ((BigInteger) record[2]).intValue();
                Integer depTraceCount = ((BigInteger) record[3]).intValue();
                String wr_sid = (String) record[4];
                Integer create_usr = (Integer) record[5];
                Date create_dt = (Date) record[6];
                Integer dep_sid = (Integer) record[7];
                String wr_tag_sid = (String) record[8];
                String themeR = (String) record[9];
                WRStatus wr_statusR = WRStatus.valueOf((String) record[10]);
                Date update_dt = (Date) record[11];
                String wr_no = (String) record[12];
                Object read_receipts_memberObject = record[13];
                WREditType wREditType = WREditType.valueOf((String) record[14]);

                WRReadStatus wRReadStatus = WRReadStatus.UNREAD;
                if (userRecordMap.get(wr_sid) != null) {
                    wRReadStatus = WRReadStatus.valueOf(userRecordMap.get(wr_sid));
                }

                List<ReadReceiptsMemberTo> wRReadReceiptsMemberResult = Lists.newArrayList();
                if (read_receipts_memberObject != null) {
                    String read_receipts_memberStr = (String) read_receipts_memberObject;
                    wRReadReceiptsMemberResult.addAll(WkJsonUtils.getInstance().fromJsonToList(read_receipts_memberStr, ReadReceiptsMemberTo.class));
                }
                boolean isReadReceiptsMemberResult = false;
                boolean readReceiptsMemberSended = false;
                List<ReadReceiptsMemberTo> readReceiptsMemberResult = wRReadReceiptsMemberResult.stream()
                        .filter(each -> each.getReader().equals(String.valueOf(searchUserSid)))
                        .collect(Collectors.toList());
                if (readReceiptsMemberResult != null && !readReceiptsMemberResult.isEmpty()) {
                    isReadReceiptsMemberResult = true;
                    if (!Strings.isNullOrEmpty(readReceiptsMemberResult.get(0).getReadreceipt())) {
                        WReadReceiptStatus wReadReceiptStatus = WReadReceiptStatus.valueOf(readReceiptsMemberResult.get(0).getReadreceipt());
                        if (WReadReceiptStatus.READ.equals(wReadReceiptStatus)) {
                            readReceiptsMemberSended = true;
                        }
                    }
                }
                boolean toTrace = false;
                if (personTraceCount > 0 || depTraceCount > 0) {
                    toTrace = true;
                }
                boolean toForwardDep = false;
                if (transDepCount > 0) {
                    toForwardDep = true;
                }
                boolean toForwardMember = false;
                if (transPersonCount > 0) {
                    toForwardMember = true;
                }
                WRMasterWorkReportVO wrd = new WRMasterWorkReportVO(wr_sid, toForwardDep, toForwardMember, toTrace,
                        create_dt, wr_tag_sid, dep_sid, create_usr, themeR,
                        wr_statusR, update_dt, wRReadStatus, wr_no, isReadReceiptsMemberResult, wREditType, readReceiptsMemberSended);
                wRMasterWorkReportVOs.add(wrd);
            } catch (Exception e) {
                log.error("findWorkReportWRMaster Error", e);
            }

        }
        return wRMasterWorkReportVOs;
    }

    /**
     * 草稿查詢(若無挑選任何部門,預設載入登入者的草稿)
     *
     * @param depSids       挑選的部門Sids
     * @param tagID         介面挑選標籤ID
     * @param sid           工作報告Sid
     * @param theme         主題
     * @param content       內容
     * @param searchUserSid 登入者Sid
     * @param createTime    草稿建立日期
     * @return
     */
    public List<WRMasterDraftVO> findDraftWRMaster(List<Integer> depSids, String tagID, String sid, String theme,
            String content, Integer searchUserSid, Date createTime) {
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT wr.wr_sid,wr.create_usr,wr.create_dt,wr.dep_sid,wr.wr_tag_sid,wr.theme,wr.wr_status,wr.update_dt,wr.wr_no FROM wr_master wr ");
        builder.append(" WHERE 1=1 ");
        if (!Strings.isNullOrEmpty(tagID)) {
            builder.append(" AND wr.wr_tag_sid = :wr_tag_sid");
            parameters.put("wr_tag_sid", tagID);
        }
        if (!Strings.isNullOrEmpty(sid)) {
            builder.append(" AND wr.wr_sid = :wr_sid");
            parameters.put("wr_sid", sid);
        }
        if (!Strings.isNullOrEmpty(theme)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(theme) + "%";
            builder.append(" AND wr.theme LIKE :theme");
            parameters.put("theme", textNo);
        }
        if (!Strings.isNullOrEmpty(content)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(content) + "%";
            builder.append(" AND wr.content LIKE :content");
            parameters.put("content", textNo);
        }
        if (depSids != null && depSids.size() > 0) {
            builder.append(" AND ( wr.dep_sid IN (:dep_sids) OR wr.create_usr = :create_usr )");
            parameters.put("dep_sids", depSids);
            parameters.put("create_usr", searchUserSid);
        } else {
            builder.append(" AND wr.create_usr = :create_usr ");
            parameters.put("create_usr", searchUserSid);
        }
        if (createTime != null) {
            String draftTimeStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), createTime);
            builder.append(" AND wr.create_dt <= :end_dt AND wr.create_dt >= :start_dt ");
            parameters.put("start_dt", draftTimeStr + " 00:00:00");
            parameters.put("end_dt", draftTimeStr + " 23:59:59");
        }

        builder.append(" AND wr.wr_status in (:wr_status) ");
        List<String> wrStatus = Lists.newArrayList();
        wrStatus.add(WRStatus.DRAFT.name());
        // wrStatus.add(WRStatus.DELETE.name());
        parameters.put("wr_status", wrStatus);
        builder.append(" ORDER BY  wr.update_dt DESC ");
        List<WRMasterDraftVO> wRMasterDraftVOs = Lists.newArrayList();
        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        for (int i = 0; i < result.size(); i++) {
            try {
                Object[] record = (Object[]) result.get(i);
                String wr_sid = (String) record[0];
                Integer create_usr = (Integer) record[1];
                Date create_dt = (Date) record[2];
                Integer dep_sid = (Integer) record[3];
                String wr_tag_sid = (String) record[4];
                String themeR = (String) record[5];
                String wr_statusR = (String) record[6];
                Date update_dt = (Date) record[7];
                String wr_no = (String) record[8];
                WRMasterDraftVO wrd = new WRMasterDraftVO(wr_sid, create_dt, wr_tag_sid, dep_sid, create_usr, themeR, wr_statusR, update_dt, wr_no);
                wRMasterDraftVOs.add(wrd);
            } catch (Exception e) {
                log.error("findDraftWRMaster Error", e);
            }

        }
        return wRMasterDraftVOs;
    }

}
