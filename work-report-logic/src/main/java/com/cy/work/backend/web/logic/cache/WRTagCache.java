/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.cache;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.repository.WRTagRepository;
import com.cy.work.backend.web.vo.WRTag;
import com.cy.work.backend.web.vo.enums.WRTagCategoryType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRTagCache implements Serializable {

    
    private static final long serialVersionUID = 642224973853959621L;
    @Autowired
    private WRTagRepository wRTagRepository;
    /** 控制啟動FLAG */
    private Boolean startController;
    /** Cache 物件 */
    private Map<String, WRTag> wRTagMap;

    public WRTagCache() {
        startController = Boolean.TRUE;
        wRTagMap = Maps.newConcurrentMap();
    }

    /** 初始化資料 */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            this.updateCache();
        }
    }

    /** 排程更新Cache資料 */
    @Scheduled(cron = "0 0/30 * * * ?")
    public void updateCache() {
        log.info("建立標籤Cache");
        try {
            Map<String, WRTag> tempWRTagMap = Maps.newConcurrentMap();
            wRTagRepository.findAll().forEach(item -> {
                if (WRTagCategoryType.WORKREPORT.equals(item.getCategory())) {
                    tempWRTagMap.put(item.getSid(), item);
                }
            });
            wRTagMap = tempWRTagMap;
        } catch (Exception e) {
            log.error("updateCache", e);
        }
        log.info("建立標籤Cache結束");
    }

    /** 更新Cache資料 */
    public void updateCacheBySid(WRTag wRTag) {
        wRTagMap.put(wRTag.getSid(), wRTag);
    }

    public WRTag findBySid(String sid) {
        return wRTagMap.get(sid);
    }

    public List<WRTag> getActiveWRTag() {
        List<WRTag> wRTags = Lists.newArrayList();
        Lists.newArrayList(wRTagMap.values()).forEach(item -> {
            if (item.getStatus().equals(Activation.ACTIVE)) {
                wRTags.add(item);
            }
        });
        return wRTags;
    }

    public List<WRTag> getWRTags() {
        return Lists.newArrayList(wRTagMap.values());
    }

}
