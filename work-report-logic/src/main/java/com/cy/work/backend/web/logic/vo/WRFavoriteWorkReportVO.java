/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;

/**
 * 收藏報表轉出物件
 *
 * @author brain0925_liao
 */
public class WRFavoriteWorkReportVO implements Serializable {

    private static final long serialVersionUID = -4938386083051937310L;

    public WRFavoriteWorkReportVO(String favorite_sid, String wr_sid, String theme, Date favoriteDate, String wr_no) {
        this.favorite_sid = favorite_sid;
        this.wr_sid = wr_sid;
        this.theme = theme;
        this.favoriteDate = favoriteDate;
        this.wr_no = wr_no;
    }
    /** 收藏Sid*/
    @Getter
    private final String favorite_sid;
    /**工作報告Sid*/
    @Getter
    private final String wr_sid;
    /**主題*/
    @Getter
    private final String theme;
    /**收藏日期*/
    @Getter
    private final Date favoriteDate;
    /** 工作報告單號 */
    @Getter
    private final String wr_no;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.favorite_sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRFavoriteWorkReportVO other = (WRFavoriteWorkReportVO) obj;
        if (!Objects.equals(this.favorite_sid, other.favorite_sid)) {
            return false;
        }
        return true;
    }
    
    

}
