/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.query;

import com.cy.commons.vo.User;
import com.cy.work.backend.web.vo.WRMaster;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Component
public class WRMasterQueryService implements QueryService<WRMaster>, Serializable {


    private static final long serialVersionUID = 4891404519262134437L;
    @PersistenceContext
    transient private EntityManager em;

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public List findWithQuery(String sql, Map<String, Object> parameters, User executor) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        List result = query.getResultList();
        return result;
    }
}
