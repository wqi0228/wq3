/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.AttachmentVO;
import com.cy.work.backend.web.repository.WRAttachmentRepository;
import com.cy.work.backend.web.vo.WRAttachment;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 工作報告附件
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRAttachmentManager implements Serializable {

    private static final long serialVersionUID = -6416465683935390710L;

    @Autowired
    private WRAttachmentRepository wRAttachmentRepository;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WorkSettingOrgManager workSettingOrgManager;

    /**
     * 取得附件 By 附件Sid
     *
     * @param att_sid
     * @return
     */
    public WRAttachment findBySid(String att_sid) {
        try {
            return wRAttachmentRepository.findOne(att_sid);
        } catch (Exception e) {
            log.error("findBySid Error : " + e.toString(), e);
            return null;
        }
    }

    /**
     * 取得資料庫所有附件資料
     *
     * @return
     */
    public List<WRAttachment> findAll() {
        try {
            return wRAttachmentRepository.findAll();
        } catch (Exception e) {
            log.error("findAll Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }

    /**
     * 取得附件List By 工作報告Sid
     *
     * @param wrSid
     * @return
     */
    public List<WRAttachment> getWRAttachmentByWRSid(String wrSid) {
        try {
            return wRAttachmentRepository.getWRAttachmentByWRSid(wrSid);
        } catch (Exception e) {
            log.error("getWRAttachmentByWRSid Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }

    /**
     * 建立附件資料
     *
     * @param wrAttachment 附件物件
     * @param userSid 建立者
     * @return
     */
    public WRAttachment create(WRAttachment wrAttachment, Integer userSid) {
        try {
            wrAttachment.setCreate_usr_sid(userSid);
            wrAttachment.setCreate_dt(new Date());
            WRAttachment wr = wRAttachmentRepository.save(wrAttachment);
            return wr;
        } catch (Exception e) {
            log.error("save Error : " + e.toString(), e);
        }
        return null;
    }
    
    public AttachmentVO createAttachment(String wr_id, String wr_no, String fileName, Integer create_uerSid, Integer departmetSid) {
        WRAttachment create = new WRAttachment();
        create.setStatus(Activation.ACTIVE);
        create.setFile_name(fileName);
        create.setWr_sid(wr_id);
        create.setWr_no(wr_no);
        create.setDescription("");
        create.setDepartment_sid(departmetSid);
        WRAttachment wr = this.create(create, create_uerSid);
        if (wr == null) {
            return null;
        }
        return new AttachmentVO(wr.getSid(), wr.getFile_name(), wr.getDescription(), wr.getFile_name(), true, String.valueOf(wr.getCreate_usr_sid()));
    }

    /**
     * 更新工作報告與附件關聯及附件資訊
     *
     * @param wr_sid 工作報告Sid
     * @param wr_no 工作報告單號
     * @param description 附件描述
     * @param userSid 更新者
     * @param attachment_sid 附件Sid
     */
    public void updateWRMasterMappingInfo(String wr_sid, String wr_no, String description, Integer userSid, String attachment_sid) {
        try {
            wRAttachmentRepository.updateWRMasterMappingInfo(wr_sid, wr_no, description, userSid, new Date(), attachment_sid);
        } catch (Exception e) {
            log.error("updateWRMasterMappingInfo Error : " + e.toString(), e);
        }
    }

    /**
     * 更新附件
     *
     * @param wrAttachment 附件物件
     * @param userSid 更新者Sid
     * @return
     */
    public WRAttachment update(WRAttachment wrAttachment, Integer userSid) {
        try {
            wrAttachment.setUpdate_dt(new Date());
            wrAttachment.setUpdate_usr_sid(userSid);
            WRAttachment wr = wRAttachmentRepository.save(wrAttachment);
            return wr;
        } catch (Exception e) {
            log.error("save Error : " + e.toString(), e);
        }
        return null;
    }

    /**
     * 將附件物件轉換成附件界面物件
     *
     * @param wRAttachment 附件物件
     * @return
     */
    public AttachmentVO transWRAttachmentToAttachmentVO(WRAttachment wRAttachment) {
        AttachmentVO av = new AttachmentVO(wRAttachment.getSid(), wRAttachment.getFile_name(), wRAttachment.getDescription(), wRAttachment.getFile_name(), true, String.valueOf(wRAttachment.getCreate_usr_sid()));
        av.setCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wRAttachment.getCreate_dt()));
        av.setParamCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wRAttachment.getCreate_dt()));
        av.setStatus(wRAttachment.getStatus());
        try {
            User user = workSettingUserManager.findBySID(wRAttachment.getCreate_usr_sid());
            av.setCreateUserSId(String.valueOf(user.getSid()));
            Org dep = workSettingOrgManager.findBySid(user.getPrimaryOrg().getSid());
            String userInfo = workSettingOrgManager.showParentDep(dep) + "-" + user.getName();
            av.setUserInfo(userInfo);
        } catch (Exception e) {
            log.error("transWRAttachmentToAttachmentVO", e);
        }
        return av;
    }

}
