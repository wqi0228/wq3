/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.AttachmentVO;
import com.cy.work.backend.web.repository.WRAttachmentRepository;
import com.cy.work.backend.web.repository.WRMasterRepository;
import com.cy.work.backend.web.repository.WRTraceRepository;
import com.cy.work.backend.web.vo.WRAttachment;
import com.cy.work.backend.web.vo.WRMaster;
import com.cy.work.backend.web.vo.WRTag;
import com.cy.work.backend.web.vo.WRTrace;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.WREditType;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.cy.work.backend.web.vo.enums.WRStatus;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRMasterCustomLogicManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 990435613560757849L;
    @Autowired
    private WRMasterRepository wRMasterRepository;
    @Autowired
    private WRNoGenericComponents wRNoGenericComponents;
    @Autowired
    private WorkSettingOrgManager workSettingOrgManager;
    @Autowired
    private WRTraceCustomLogicManager wRTraceCustomLogicManager;
    @Autowired
    private WRTraceRepository wRTraceRepository;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WRAttachmentRepository wRAttachmentRepository;
    @Autowired
    private WRTagManager wRTagManager;
    @Autowired
    private WrReadRecordManager wrReadRecordManager;
    @PersistenceContext
    transient private EntityManager entityManager;

    /**
     * 提交工作報告
     *
     * @param sid     工作報告Sid
     * @param userSid 提交者Sid
     * @param depSid  提交者部門Sid
     * @throws InterruptedException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void summitWRMaster(String sid, Integer userSid, Integer depSid, Integer compSid) throws InterruptedException, Exception {

        // log.info("summitWRMaster : " + sid);
        WRMaster wr = wRMasterRepository.findBySid(sid);
        Preconditions.checkState(wr != null, "載入工作報告失敗！");
        Preconditions.checkState(!wr.getWr_status().equals(WRStatus.COMMIT), "單據已提交,不可提交！");
        Preconditions.checkState(!wr.getWr_status().equals(WRStatus.DELETE), "單據已刪除,不可提交！");
        Preconditions.checkState(!wr.getWr_status().equals(WRStatus.INVIALD), "單據已作廢,不可提交！");
        String tag_Sid = wr.getWr_tag_sid();
        List<WRTag> activeWRTags = wRTagManager.getWRTagActive(depSid, compSid);
        List<WRTag> filterResult = activeWRTags.stream().filter(p -> tag_Sid.equals(p.getSid()))
                .collect(Collectors.toList());
        Preconditions.checkState((filterResult != null && !filterResult.isEmpty()), "標籤已失效,請確認！若有疑問請通知系統管理員");
        wr.setDep_sid(depSid);
        wr.setWr_status(WRStatus.COMMIT);
        Org comp = workSettingOrgManager.findBySid(wr.getComp_sid());
        wr.setWr_no(comp.getId() + wRNoGenericComponents.getSequenceNum(comp.getSid()));
        wr.setSubmit_dt(new Date());
        wRMasterRepository.save(wr);
        WRTrace wt = wRTraceCustomLogicManager.getWRTrace_COMMIT(userSid, wr.getSid(), wr.getWr_no());
        this.createWRTrace(wt, userSid);
        if (wr.getEdit_type().equals(WREditType.DEP)) {
            // 若此單據編輯權限＝部門時-系統日往後加上24小時
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 1);
            this.updateLastModifyTime(wr.getSid(), c.getTime());
        } else {
            // 若此單據編輯權限＝個人時，[wr_master].[last_modify_dt]=系統日當下的時間
            this.updateLastModifyTimeToNow(wr.getSid());
        }
    }

    /**
     * 更新工作報告(主題內容備註)
     *
     * @param sid     工作報告Sid
     * @param title
     * @param content
     * @param remark
     * @param userSid 更新者Sid
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void updateContentWRMaster(String sid, String title, String content, String remark, Integer userSid) {
        // log.info("updateWRMaster : " + sid);
        WRMaster wr = wRMasterRepository.findOne(sid);
        Preconditions.checkState(wr != null, "載入工作報告失敗！");
        Preconditions.checkState(!wr.getWr_status().equals(WRStatus.INVIALD), "單據已作廢,不可編輯！");
        Preconditions.checkState(!wr.getWr_status().equals(WRStatus.DELETE), "單據已刪除,不可編輯！");

        List<WRTrace> wRTraces = Lists.newArrayList();
        // 提交才進行追蹤
        if (wr.getWr_status().equals(WRStatus.COMMIT)) {
            // 檢測標題是否有變更,若有變更需加入追蹤紀錄
            if (!wr.getTheme().equals(title)) {
                wRTraces.add(wRTraceCustomLogicManager.getWRTrace_MODIFY_THEME(wr.getTheme(),
                        title, userSid, wr.getSid(), wr.getWr_no()));
            }
            // 檢測內容是否有變更,若有變更需加入追蹤紀錄
            if (!wr.getContent_css().equals(content)) {
                wRTraces.add(wRTraceCustomLogicManager.getWRTrace_MODIFY_CONTNET(wr.getContent(),
                        content, userSid, wr.getSid(), wr.getWr_no()));
            }
            // 檢測備註是否有變更,若有變更需加入追蹤紀錄
            if (!wr.getMemo_css().equals(remark)) {
                wRTraces.add(wRTraceCustomLogicManager.getWRTrace_MODIFY_MEMO(wr.getMemo(),
                        remark, userSid, wr.getSid(), wr.getWr_no()));
            }
        }
        if (!wr.getTheme().equals(title) || !wr.getContent().equals(jsoupUtils.clearCssTag(content)) || !wr.getMemo().equals(jsoupUtils.clearCssTag(remark))) {
            wr.setUpdate_usr(userSid);
            wr.setUpdate_dt(new Date());
            for (WrReadRecord readRecord : wrReadRecordManager.findReadRecordsByWrSid(sid)) {
                if (readRecord.getCreateUser().equals(userSid.toString())) {
                    continue;
                }
                if (readRecord.getReadStatus().equals(WRReadStatus.HASREAD.name())) {
                    readRecord.setReadStatus(WRReadStatus.WAIT_READ.name());
                }
            }
        }
        wr.setTheme(title);
        wr.setContent_css(content);
        wr.setContent(jsoupUtils.clearCssTag(content));
        wr.setMemo_css(remark);
        wr.setMemo(jsoupUtils.clearCssTag(remark));
        wRMasterRepository.save(wr);
        wRTraces.forEach(item -> {
            this.createWRTrace(item, userSid);
        });
        if (!wRTraces.isEmpty()) {
            // todo 待觀察新閱讀紀錄表是否要跟之前邏輯一樣
        }

    }

    /**
     * 刪除工作報告
     *
     * @param sid     工作報告Sid
     * @param userSid 刪除者Sid
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void deleteWRMaster(String sid, Integer userSid) {
        WRMaster wr = wRMasterRepository.findOne(sid);
        Preconditions.checkState(wr != null, "載入工作報告失敗！");
        Preconditions.checkState(wr.getWr_status().equals(WRStatus.DRAFT), "單據不為草稿,不可刪除！");
        wr.setWr_status(WRStatus.DELETE);
        wr = this.updateWRMaster(wr, userSid);
        WRTrace wt = wRTraceCustomLogicManager.getWRTrace_DEL_WR(wr.getSid(), wr.getWr_no(), userSid);
        this.createWRTrace(wt, userSid);
    }

    /**
     * 作廢工作報告
     *
     * @param sid     工作報告Sid
     * @param reason  作廢理由
     * @param userSid 作廢者Sid
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void invaildWRMaster(String sid, String reason, Integer userSid) {
        WRMaster wr = wRMasterRepository.findOne(sid);
        Preconditions.checkState(wr != null, "載入工作報告失敗！");
        Preconditions.checkState(wr.getWr_status().equals(WRStatus.COMMIT), "單據不為正式發佈,不可作廢！");
        wr.setWr_status(WRStatus.INVIALD);
        wr = this.updateWRMaster(wr, userSid);
        WRTrace wt = wRTraceCustomLogicManager.getWRTrace_INVAILD(wr.getSid(), wr.getWr_no(), reason);
        this.createWRTrace(wt, userSid);
    }

    /**
     * 變更標籤
     *
     * @param sid     工作報告Sid
     * @param tagID   挑選的標籤ID
     * @param userSid 更新者Sid
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void saveTag(String sid, String tagID, Integer userSid, Integer loginUserDepSid, Integer compSid) {
        // log.info("saveTag : " + sid);
        WRMaster wr = wRMasterRepository.findOne(sid);
        Preconditions.checkState(wr != null, "載入工作報告失敗！");
        Preconditions.checkState(!(wr.getWr_status().equals(WRStatus.COMMIT) && wr.getEdit_type().equals(WREditType.PERSONAL)
                && !wr.getCreate_usr().equals(userSid)), "單據提交狀態並且允許編輯為個人,不可變更標籤！");
        Preconditions.checkState(!(wr.getWr_status().equals(WRStatus.DELETE)), "單據已刪除,不可變更變更標籤！");
        Preconditions.checkState(!(wr.getWr_status().equals(WRStatus.INVIALD)), "單據已作廢,不可變更變更標籤！");
        List<WRTag> activeWRTags = wRTagManager.getWRTagActive(loginUserDepSid, compSid);
        List<WRTag> filterResult = activeWRTags.stream().filter(p -> tagID.equals(p.getSid()))
                .collect(Collectors.toList());
        Preconditions.checkState((filterResult != null && !filterResult.isEmpty()), "挑選標籤已失效,請確認！若有疑問請通知系統管理員");
        WRTrace wt = null;
        if (!wr.getWr_tag_sid().equals(tagID)) {
            wt = wRTraceCustomLogicManager.getWRTrace_MODIFY_TAG(wr.getWr_tag_sid(), tagID, userSid, wr.getSid(), wr.getWr_no());
            if (wr.getWr_status().equals(WRStatus.DRAFT)) {
                wt.setStatus(Activation.INACTIVE);
            }
            wr.setWr_tag_sid(tagID);
            this.updateWRMaster(wr, userSid);
        }
        if (wt != null) {
            this.createWRTrace(wt, userSid);
        }
    }

    /**
     * 變更權限編輯
     *
     * @param sid      工作報告Sid
     * @param editType 變更權限編輯ID
     * @param userSid  變更者Sid
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void saveEditType(String sid, String editType, Integer userSid) {
        WRMaster wr = wRMasterRepository.findOne(sid);
        Preconditions.checkState(wr != null, "載入工作報告失敗！");
        Preconditions.checkState(!(wr.getWr_status().equals(WRStatus.COMMIT)), "單據已提交,不可變更編輯允許！");
        Preconditions.checkState(!(wr.getWr_status().equals(WRStatus.DELETE)), "單據已刪除,不可變更編輯允許！");
        Preconditions.checkState(!(wr.getWr_status().equals(WRStatus.INVIALD)), "單據已作廢,不可變更編輯允許！");
        WRTrace wt = null;
        if (!wr.getEdit_type().name().equals(editType)) {
            wt = wRTraceCustomLogicManager.getWRTrace_MODIFY_EDIT_TYPE(wr.getEdit_type().name(), editType, userSid, wr.getSid(), wr.getWr_no());
            if (wr.getWr_status().equals(WRStatus.DRAFT)) {
                wt.setStatus(Activation.INACTIVE);
            }
            wr = this.updateWRMaster(wr, userSid);
        }
        wr.setEdit_type(WREditType.valueOf(editType));
        if (wt != null) {
            this.createWRTrace(wt, userSid);
        }
    }

    /**
     * 建立工作報告
     *
     * @param wrMaster
     * @param attachmentVOs
     * @param loginUserSid
     * @return
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WRMaster createWRMaster(WRMaster wrMaster, Integer loginUserSid, List<AttachmentVO> attachmentVOs) {
        WRMaster dbWRMaster = this.createWRMaster(wrMaster, loginUserSid);
        wrReadRecordManager.saveWrReadRecord(new WrReadRecord(WRReadStatus.HASREAD.name(), loginUserSid.toString(), new Date(), dbWRMaster.getSid()));
        Preconditions.checkState(dbWRMaster != null, "新增工作報告失敗！");
        if (attachmentVOs != null && attachmentVOs.size() > 0) {
            attachmentVOs.forEach(item -> {
                wRAttachmentRepository.updateWRMasterMappingInfo(dbWRMaster.getSid(), dbWRMaster.getWr_no(), item.getAttDesc(), loginUserSid, new Date(), item.getAttSid());
            });
        }
        if (dbWRMaster.getWr_status().equals(WRStatus.COMMIT)) {
            try {
                WRTrace wt = wRTraceCustomLogicManager.getWRTrace_COMMIT(loginUserSid, dbWRMaster.getSid(), dbWRMaster.getWr_no());
                this.createWRTrace(wt, loginUserSid);
            } catch (Exception e) {
                log.error("createWRMaster ERROR", e);
            }
            if (dbWRMaster.getEdit_type().equals(WREditType.DEP)) {
                // 若此單據編輯權限＝部門時-系統日往後加上24小時
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DATE, 1);
                this.updateLastModifyTime(dbWRMaster.getSid(), c.getTime());
            } else {
                // 若此單據編輯權限＝個人時，[wr_master].[last_modify_dt]=系統日當下的時間
                this.updateLastModifyTimeToNow(dbWRMaster.getSid());
            }
        }
        return dbWRMaster;
    }

    /**
     * 回覆工作報告
     *
     * @param sid      工作報告sid
     * @param userSid  回覆者sid
     * @param replySid
     * @param content
     * @return
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WRMaster replyWRMaster(String sid, Integer userSid, String replySid, String content) {
        WRMaster wr = wRMasterRepository.findOne(sid);
        Preconditions.checkState(wr != null, "載入工作報告失敗！");
        Preconditions.checkState(!(wr.getWr_status().equals(WRStatus.DELETE)), "單據已刪除,不可回覆！");
        WRTrace wt;
        if (Strings.isNullOrEmpty(replySid)) {
            wt = wRTraceCustomLogicManager.getWRTrace_REPLY(content, userSid, wr.getSid(), wr.getWr_no());
        } else {
            wt = wRTraceRepository.findOne(replySid);
            wt.setWr_trace_content(jsoupUtils.clearCssTag(content));
            wt.setWr_trace_content_css(content);
        }

        if (Strings.isNullOrEmpty(replySid)) {
            this.createWRTrace(wt, userSid);
        } else {
            this.updateWRTrace(wt, userSid);
        }
        return wr;
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void deleteAttachment(AttachmentVO attachmentVO, Integer loginUserSid, String wr_Sid) {
        boolean result = this.deleteDBAttachment(attachmentVO.getAttSid(), loginUserSid);
        Preconditions.checkState(result, "刪除附件失敗");
        WRMaster wr = wRMasterRepository.findOne(wr_Sid);
        WRTrace deleteWRTrace = wRTraceCustomLogicManager.getWRTrace_DEL_ATTACHMENT(attachmentVO.getAttSid(), attachmentVO.getAttName(), Integer.valueOf(attachmentVO.getCreateUserSId()), loginUserSid,
                wr_Sid, wr.getWr_no());
        if (wr.getWr_status().equals(WRStatus.DRAFT)) {
            deleteWRTrace.setStatus(Activation.INACTIVE);
        }
        this.createWRTrace(deleteWRTrace, loginUserSid);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void saveReadReceipt(WRMaster wRMaster, Integer loginUserSid, boolean noChanged) {
        WRMaster wr = wRMasterRepository.save(wRMaster);
        if (!noChanged) {
            WRTrace wt = wRTraceCustomLogicManager.getWRTrace_ADD_READ_RECEIPTS(wr.getSid(), wr.getWr_no());
            if (wr.getWr_status().equals(WRStatus.DRAFT)) {
                wt.setStatus(Activation.INACTIVE);
            }
            this.createWRTrace(wt, loginUserSid);
        }
    }

    /**
     * 刪除附件
     *
     * @param att_sid        附件Sid
     * @param delete_userSid 刪除者Sid
     * @return
     */
    private boolean deleteDBAttachment(String att_sid, Integer delete_userSid) {
        WRAttachment wa = wRAttachmentRepository.findOne(att_sid);
        if (wa == null) {
            return false;
        }
        wa.setStatus(Activation.INACTIVE);
        WRAttachment wr = this.updateWRAttachment(wa, delete_userSid);
        if (wr == null) {
            return false;
        }
        return true;
    }

    /**
     * 更新附件
     *
     * @param wrAttachment 附件物件
     * @param userSid      更新者Sid
     * @return
     */
    private WRAttachment updateWRAttachment(WRAttachment wrAttachment, Integer userSid) {
        wrAttachment.setUpdate_dt(new Date());
        wrAttachment.setUpdate_usr_sid(userSid);
        WRAttachment wr = wRAttachmentRepository.save(wrAttachment);
        return wr;
    }

    /**
     * 更新追蹤資料
     *
     * @param wRTrace       追蹤資料
     * @param updateUserSid 更新者Sid
     * @return
     */
    private WRTrace updateWRTrace(WRTrace wRTrace, Integer updateUserSid) {
        wRTrace.setUpdate_usr(updateUserSid);
        wRTrace.setUpdate_dt(new Date());
        WRTrace wr = wRTraceRepository.save(wRTrace);
        return wr;
    }

    private void updateLastModifyTime(String wr_sid, Date lastModifyDate) {
        String lastModifyDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDash.getValue(), lastModifyDate);
        lastModifyDateStr = lastModifyDateStr + " 23:59:59";
        Date lastDate = ToolsDate.transStringToDate(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), lastModifyDateStr);
        wRMasterRepository.updateLastModifyTime(lastDate, wr_sid);
        entityManager.clear();
    }

    private void updateLastModifyTimeToNow(String wr_sid) {
        wRMasterRepository.updateLastModifyTime(new Date(), wr_sid);
    }

    /**
     * 更新工作報告(更新修改時間及修改者)
     *
     * @param wrMaster      工作報告物件
     * @param updateUserSid 更新者Sid
     * @return
     */
    private WRMaster updateWRMaster(WRMaster wrMaster, Integer updateUserSid) {
        wrMaster.setUpdate_usr(updateUserSid);
        wrMaster.setUpdate_dt(new Date());
        WRMaster wr = wRMasterRepository.save(wrMaster);
        return wr;
    }

    /**
     * 建立追蹤資料
     *
     * @param wRTrace       追蹤物件
     * @param createUserSid 建立者Sid
     * @return
     */
    private WRTrace createWRTrace(WRTrace wRTrace, Integer createUserSid) {
        wRTrace.setCreate_usr(createUserSid);
        Date createDate = new Date();
        wRTrace.setCreate_dt(createDate);
        wRTrace.setUpdate_dt(createDate);
        WRTrace wr = wRTraceRepository.save(wRTrace);
        return wr;
    }

    /**
     * 建立工作報告
     *
     * @param wrMaster      工作報告物件
     * @param createUserSid 建立者Sid
     * @return
     */
    private WRMaster createWRMaster(WRMaster wrMaster, Integer createUserSid) {
        wrMaster.setCreate_usr(createUserSid);
        Date createDate = new Date();
        wrMaster.setCreate_dt(createDate);
        wrMaster.setUpdate_dt(createDate);
        if (!wrMaster.getWr_status().equals(WRStatus.DRAFT)) {
            wrMaster.setSubmit_dt(createDate);
        }
        WRMaster wr = wRMasterRepository.save(wrMaster);
        return wr;
    }

}
