/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.logic.cache.WRTagCache;
import com.cy.work.backend.web.repository.WRTagRepository;
import com.cy.work.backend.web.vo.WRTag;
import com.cy.work.backend.web.vo.converter.to.UserDepTo;
import com.cy.work.backend.web.vo.enums.WRTagCategoryType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRTagManager implements Serializable {

    
    private static final long serialVersionUID = -337783020438254437L;
    @Autowired
    private WRTagRepository wrTagRepository;
    @Autowired
    private WRTagCache wRTagCache;

    public WRTag getWRTagBySid(String tagSid) {
        try {
            List<WRTag> selWRTags = wRTagCache.getWRTags().stream()
                    .filter(each -> each.getSid().equals(tagSid))
                    .collect(Collectors.toList());
            if (selWRTags != null && !selWRTags.isEmpty()) {
                return selWRTags.get(0);
            }
        } catch (Exception e) {
            log.error("getWRTagBySid Error : " + e.toString(), e);
        }
        return null;
    }

    public void createWRTag(WRTag wRTag, Integer loginUserSid,Integer compSid) {
        wRTag.setCreate_dt(new Date());
        wRTag.setCreate_usr_sid(loginUserSid);
        wRTag.setCompSid(compSid);
        wRTag = wrTagRepository.save(wRTag);
        wRTagCache.updateCacheBySid(wRTag);
    }

    public void updateWRTag(WRTag wRTag, Integer loginUserSid) {
        wRTag.setUpdate_dt(new Date());
        wRTag.setUpdate_usr_sid(loginUserSid);
        wRTag = wrTagRepository.save(wRTag);
        wRTagCache.updateCacheBySid(wRTag);
    }

    /**
     * ReadReceiptsMemberTo 比較key - reader
     */
    transient private Comparator<WRTag> wRTagComparator = new Comparator<WRTag>() {
        @Override
        public int compare(WRTag obj1, WRTag obj2) {
            final Integer seq1 = obj1.getSeq();
            final Integer seq2 = obj2.getSeq();
            return seq1 - seq2;
        }
    };

    /** 取得所有標籤資料 */
    public List<WRTag> getWRTagActive(Integer loginUserDepSid, Integer loginComp) {
        try {
            List<WRTag> lists = Lists.newArrayList();
            List<WRTag> result = wRTagCache.getWRTags();
            Collections.sort(result, wRTagComparator);
            for (WRTag item : result) {
                if (item.getStatus().equals(Activation.INACTIVE)) {
                    continue;
                }
                if (!item.getCategory().equals(WRTagCategoryType.WORKREPORT)) {
                    continue;
                }
                if (!item.getCompSid().equals(loginComp)) {
                    continue;
                }
                if (item.getUse_dep() != null && item.getUse_dep().getUserDepTos() != null
                        && !item.getUse_dep().getUserDepTos().isEmpty()) {
                    List<UserDepTo> is = item.getUse_dep().getUserDepTos().stream()
                            .filter(p -> String.valueOf(loginUserDepSid).equals(p.getDepSid()))
                            .collect(Collectors.toList());
                    if (is == null || is.isEmpty()) {
                        continue;
                    }
                }
                lists.add(item);
            }
            return lists;
        } catch (Exception e) {
            log.error("getWRTagByStatus Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }

    public List<WRTag> getWRTags(Integer compSid) {
        try {
            List<WRTag> result = wRTagCache.getWRTags();
            Collections.sort(result, wRTagComparator);
            List<WRTag> lists = Lists.newArrayList();
            result.forEach(item -> {
                if (!item.getCompSid().equals(compSid)) {
                    return;
                }
                lists.add(item);
            });
            return lists;
        } catch (Exception e) {
            log.error("getWRTagByStatus Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }

    public List<WRTag> getWRTagsSearch(String statusID, String tagName, String tagID, Integer compSid) {
        try {
            List<WRTag> tags = Lists.newArrayList();
            List<WRTag> result = wRTagCache.getWRTags();
            Collections.sort(result, wRTagComparator);
            result.forEach(item -> {
                if (!Strings.isNullOrEmpty(statusID)) {
                    Activation selStatus = Activation.valueOf(statusID);
                    if (!item.getStatus().equals(selStatus)) {
                        return;
                    }
                }
                if (!Strings.isNullOrEmpty(tagName)) {
                    if (!item.getTag_name().contains(tagName)) {
                        return;
                    }
                }
                if (!Strings.isNullOrEmpty(tagID)) {
                    if (!item.getSid().equals(tagID)) {
                        return;
                    }
                }
                if (!item.getCompSid().equals(compSid)) {
                    return;
                }
                tags.add(item);
            });
            return tags;
        } catch (Exception e) {
            log.error("getWRTags Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }

}
