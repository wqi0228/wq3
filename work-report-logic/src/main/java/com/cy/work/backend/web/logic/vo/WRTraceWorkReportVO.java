/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.vo;

import com.cy.work.backend.web.vo.enums.TraceStatus;
import com.cy.work.backend.web.vo.enums.TraceType;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;

/**
 * 追蹤報表轉出物件
 * @author brain0925_liao
 */
public class WRTraceWorkReportVO implements Serializable {

    private static final long serialVersionUID = 8825754376440520526L;

    public WRTraceWorkReportVO(boolean tofowardDep,
            boolean tofowardMember, boolean totrace,
            TraceType traceType, String wr_tag_sid,
            Date traceNotifyDate,
            Integer trace_create_usr,
            TraceStatus traceStatus,
            String trace_memo,
            String theme, Date update_dt,
            Date trace_create_dt, WRReadStatus wRReadStatus, String wr_Id, String tracesid, String trace_memo_css, String no) {
        this.wr_Id = wr_Id;
        this.tracesid = tracesid;
        this.tofowardDep = tofowardDep;
        this.tofowardMember = tofowardMember;
        this.totrace = totrace;
        this.traceType = traceType;
        this.wr_tag_sid = wr_tag_sid;
        this.traceNotifyDate = traceNotifyDate;
        this.trace_create_usr = trace_create_usr;
        this.traceStatus = traceStatus;
        this.trace_memo = trace_memo;
        this.theme = theme;
        this.update_dt = update_dt;
        this.trace_create_dt = trace_create_dt;
        this.wRReadStatus = wRReadStatus;
        this.trace_memo_css = trace_memo_css;
        this.no = no;
    }
    /**工作報告Sid*/
    @Getter
    private final String wr_Id;
    /**追蹤內容*/
    @Getter
    private final String trace_memo_css;
    /**是否有轉寄部門*/
    @Getter
    private final boolean tofowardDep;
    /**是否有轉寄個人*/
    @Getter
    private final boolean tofowardMember;
    /**是否有追蹤*/
    @Getter
    private final boolean totrace;
    /**追蹤類型*/
    @Getter
    private final TraceType traceType;
    /**工作報告標籤ID*/
    @Getter
    private final String wr_tag_sid;
    /**追蹤日期期*/
    @Getter
    private final Date traceNotifyDate;
    /**追蹤建立使用者Sid*/
    @Getter
    private final Integer trace_create_usr;
    /**追蹤狀態*/
    @Getter
    private final TraceStatus traceStatus;
    /**追蹤內容*/
    @Getter
    private final String trace_memo;
    /**主題*/
    @Getter
    private final String theme;
    /**工作報告異動時間*/
    @Getter
    private final Date update_dt;
    /**追蹤建立時間*/
    @Getter
    private final Date trace_create_dt;
    /**工作報告已未讀狀態*/
    @Getter
    private final WRReadStatus wRReadStatus;
    /**追蹤Sid*/
    @Getter
    private final String tracesid;
    /**工作報告單號*/
    @Getter
    private final String no;
}
