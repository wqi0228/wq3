
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.logic.query.WRMasterQueryService;
import com.cy.work.backend.web.logic.utils.ReqularPattenUtils;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRTraceWorkReportVO;
import com.cy.work.backend.web.repository.WorkTraceInfoRepository;
import com.cy.work.backend.web.vo.WorkTraceInfo;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.enums.ForwardType;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.TraceStatus;
import com.cy.work.backend.web.vo.enums.TraceType;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.cy.work.backend.web.vo.enums.WRTraceType;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 追蹤
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WorkTraceInfoManager implements Serializable {


    private static final long serialVersionUID = -6473265957049504405L;

    @Autowired
    private WrReadRecordManager wrReadRecordManager;
    @Autowired
    private WorkTraceInfoRepository workTraceInfoRepository;
    @Autowired
    private WRMasterQueryService wRMasterQueryService;
    @Autowired
    private ReqularPattenUtils reqularUtils;
    @Autowired
    private WkJsoupUtils jsoupUtils;

    /**
     * 取得追蹤物件 by 追蹤Sid
     *
     * @param sid 追蹤Sid
     * @return
     */
    public WorkTraceInfo getWorkTraceInfoBySid(String sid) {
        try {
            return workTraceInfoRepository.findOne(sid);
        } catch (Exception e) {
            log.error("getWorkTraceInfoBySid", e);
        }
        return null;
    }

    public List<WorkTraceInfo> getWorkTraceInfoByTraceUsrSid(Integer userSid) {
        String trace_source_type = "WR";
        return workTraceInfoRepository.getWorkTraceInfoByTraceUsrSidAndyTraceSourceType(userSid, trace_source_type);
    }
    
    public List<WorkTraceInfo> getWorkTraceInfoByTraceDepSid(Integer depSid) {
        String trace_source_type = "WR";
        return workTraceInfoRepository.getWorkTraceInfoByTraceDepSidAndyTraceSourceType(depSid, trace_source_type);
    }

    public List<WorkTraceInfo> getWorkTraceInfoByWrSid(String wcSid) {
        String trace_source_type = "WR";
        return workTraceInfoRepository.getWorkTraceInfoByTraceSourceSiidAndyTraceSourceType(wcSid, trace_source_type);
    }

    /**
     * 取得個人未完成追蹤資料 By 工作報告Sid
     *
     * @param sid 工作報告Sid
     * @return
     */
    public List<WorkTraceInfo> getWorkTraceInfoByWr_SidForPerson(String sid) {
        try {
            String trace_source_type = "WR";
            List<TraceStatus> traceStatus = Lists.newArrayList();
            traceStatus.add(TraceStatus.UN_FINISHED);
            List<TraceType> traceType = Lists.newArrayList();
            traceType.add(TraceType.PERSONAL);
            traceType.add(TraceType.OTHER_PERSON);
            return workTraceInfoRepository.getWorkTraceInfoByTraceSourceTypeAndTraceSourceSidAndTraceStatusAndTraceType(sid, trace_source_type, traceStatus, traceType);
        } catch (Exception e) {
            log.error("getWorkTraceInfoByWr_SidForPerson Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }

    /**
     * 取得追蹤(個人&部門)筆數 By 登入者Sid 及 登入者部門Sid 及 追蹤狀態ID
     *
     * @param loginUserSid 登入者Sid
     * @param depSid 登入者部門Sid
     * @param traceStausID 追蹤狀態Id
     * @return
     */
    public Integer getWorkReportListCountByTraceSearch(Integer loginUserSid, Integer depSid, String traceStausID) {
        String trace_source_type = "WR";
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT COUNT(*) ");
        builder.append(" FROM wr_trace_info wti ");
        builder.append(" WHERE wti.status = 0 ");
        if (!Strings.isNullOrEmpty(traceStausID)) {
            builder.append(" AND wti.trace_status = :trace_status");
            parameters.put("trace_status", traceStausID);
        }
        builder.append(" AND wti.trace_source_type = :trace_source_type");
        parameters.put("trace_source_type", trace_source_type);
        builder.append(" AND (wti.trace_usr = :trace_usr OR wti.trace_dep = :trace_dep )");
        parameters.put("trace_usr", loginUserSid);
        parameters.put("trace_dep", depSid);
        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        Integer count = 0;
        for (int i = 0; i < result.size(); i++) {
            try {
                count = ((BigInteger) result.get(i)).intValue();
                break;
            } catch (Exception e) {
                log.error("getWorkReportListCountByTraceSearch", e);
            }
        }
        return count;

    }

    /**
     * 取得追蹤報表
     *
     * @param tagID 介面挑選標籤ID
     * @param statusID 介面挑選工作報告狀態ID
     * @param traceTypeID 介面挑選追蹤狀態ID
     * @param onlyTraceNotifyDateNull 介面挑選是否僅搜尋空白的追蹤日期
     * @param title 標題
     * @param content 內容
     * @param traceStart 追蹤建立起始時間
     * @param traceEnd 追蹤建立結束時間
     * @param traceNotifyStart 追蹤提醒起始時間
     * @param traceNotifyEnd 追蹤提醒結束時間
     * @param searchText 模糊搜尋
     * @param loginUserSid 登入者Sid
     * @param depSid 登入者部門Sid
     * @param wr_Id 工作報告Sid
     * @param trace_Id 追蹤Sid
     * @return
     */
    public List<WRTraceWorkReportVO> getWorkReportListByTraceSearch(String tagID, String statusID,
            String traceTypeID, boolean onlyTraceNotifyDateNull,
            String title, String content, Date traceStart,
            Date traceEnd, Date traceNotifyStart, Date traceNotifyEnd,
            String searchText, Integer loginUserSid, Integer depSid, String wr_Id, String trace_Id) {
        String trace_source_type = "WR";
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        //查詢轉寄部門
        builder.append(" (SELECT COUNT(wiDep.inbox_sid) as transDepCount FROM wr_inbox wiDep WHERE wiDep.status = 0"
                + " AND wiDep.wr_sid = wr.wr_sid"
                + " AND wiDep.forward_type = '" + ForwardType.DEP.getKey() + "'"
                // + " AND wiDep.receive_dep = " + depSid
                + " )");
        builder.append(",");
        //查詢轉寄個人
        builder.append(" (SELECT COUNT(wiPerson.inbox_sid) as transPersonCount FROM wr_inbox wiPerson WHERE wiPerson.status = 0"
                + " AND wiPerson.wr_sid = wr.wr_sid"
                + " AND wiPerson.forward_type = '" + ForwardType.PERSON.getKey() + "'"
                // + " AND wiPerson.receive = " + loginUserSid
                + " )");
        builder.append(",");

        //查詢追蹤個人       
        builder.append(" (SELECT COUNT(wtp.tracesid) as personTraceCount FROM  wr_trace_info wtp WHERE wtp.status = 0 "
                + " AND wtp.trace_source_sid = wr.wr_sid "
                + " AND wtp.trace_source_type = '" + trace_source_type + "'"
                + " AND wtp.trace_status in ('" + TraceStatus.UN_FINISHED.name() + "')"
                + " AND wtp.trace_type in ('" + TraceType.PERSONAL.name() + "','" + TraceType.OTHER_PERSON.name() + "')"
                + " AND wtp.trace_usr = " + loginUserSid + ")");
        builder.append(",");
        //查詢追蹤部門
        builder.append(" (SELECT COUNT(wtp.tracesid) as depTraceCount FROM  wr_trace_info wtp WHERE wtp.status = 0 "
                + " AND wtp.trace_source_sid = wr.wr_sid "
                + " AND wtp.trace_source_type = '" + trace_source_type + "'"
                + " AND wtp.trace_status in ('" + TraceStatus.UN_FINISHED.name() + "')"
                + " AND wtp.trace_type in ('" + TraceType.DEPARTMENT.name() + "')"
                + " AND wtp.trace_dep = " + depSid + ")");
        builder.append(",");
        builder.append("  wti.trace_type,wr.wr_tag_sid,wti.trace_notice_date,wti.create_usr,"
                + "wti.trace_status,wti.trace_memo,wr.theme,wt.updateDate,wti.create_dt,"
                + "wr.wr_sid,wti.tracesid,wti.trace_memo_css,wr.wr_no FROM wr_trace_info wti");
        //關連工作報告主表
        builder.append(" INNER JOIN  wr_master wr ON wti.trace_source_sid = wr.wr_sid "
                + "AND wti.trace_source_type = '" + trace_source_type + "' AND wti.status = 0 ");
        builder.append(" INNER JOIN (SELECT wr_sid, wr_no, max(create_dt) as updateDate,min(create_dt) as createDate FROM wr_trace  GROUP BY wr_sid, wr_no) wt ON wr.wr_sid=wt.wr_sid AND wr.wr_no=wt.wr_no");
        builder.append(" WHERE 1=1  ");
        if (!Strings.isNullOrEmpty(tagID)) {
            builder.append(" AND wr.wr_tag_sid = :wr_tag_sid");
            parameters.put("wr_tag_sid", tagID);
        }
        if (!Strings.isNullOrEmpty(statusID)) {
            builder.append(" AND wr.wr_status = :wr_status");
            parameters.put("wr_status", statusID);
        }
        if (!Strings.isNullOrEmpty(traceTypeID)) {
            builder.append(" AND wti.trace_status = :trace_status");
            parameters.put("trace_status", traceTypeID);
        }
        //追蹤日期
        if (onlyTraceNotifyDateNull) {
            builder.append(" AND wti.trace_notice_date is null ");
        } else {
            if (traceNotifyStart != null) {
                String startDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), traceNotifyStart);
                builder.append(" AND wti.trace_notice_date >= :traceNotifyStart ");
                parameters.put("traceNotifyStart", startDateStr + " 00:00:00");
            }
            if (traceNotifyEnd != null) {
                String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), traceNotifyEnd);
                builder.append(" AND wti.trace_notice_date <= :traceNotifyEnd ");
                parameters.put("traceNotifyEnd", endDateStr + " 23:59:59");
            }
        }

        if (!Strings.isNullOrEmpty(title)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(title) + "%";
            builder.append(" AND wr.theme LIKE :theme");
            parameters.put("theme", textNo);
        }
        if (!Strings.isNullOrEmpty(content)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(content) + "%";
            builder.append(" AND wr.content LIKE :content");
            parameters.put("content", textNo);
        }

        if (traceStart != null) {
            String startDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), traceStart);
            builder.append(" AND wti.create_dt >= :tracecreate_dtStart ");
            parameters.put("tracecreate_dtStart", startDateStr + " 00:00:00");
        }
        if (traceEnd != null) {
            String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), traceEnd);
            builder.append(" AND wti.create_dt <= :tracecreate_dtEnd ");
            parameters.put("tracecreate_dtEnd", endDateStr + " 23:59:59");
        }

        if (!Strings.isNullOrEmpty(searchText)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchText) + "%";
            builder.append(" AND (wr.theme LIKE :searchText OR wr.content LIKE :searchText OR EXISTS"
                    + "(SELECT * FROM wr_trace wtrc "
                    + "WHERE wtrc.status = 0 AND wtrc.wr_trace_type = '" + WRTraceType.REPLY.name() + "' AND wtrc.wr_sid = wr.wr_sid AND wtrc.wr_trace_content LIKE :searchText ) )");
            parameters.put("searchText", textNo);
        }
        if (!Strings.isNullOrEmpty(wr_Id)) {
            builder.append(" AND wr.wr_sid = :wr_sid");
            parameters.put("wr_sid", wr_Id);
        }

        if (!Strings.isNullOrEmpty(trace_Id)) {
            builder.append(" AND wti.tracesid = :tracesid");
            parameters.put("tracesid", trace_Id);
        }

        builder.append(" AND (wti.trace_usr = :trace_usr OR wti.trace_dep = :trace_dep )");
        parameters.put("trace_usr", loginUserSid);
        parameters.put("trace_dep", depSid);
        builder.append(" ORDER BY  wti.trace_notice_date DESC,wti.update_dt DESC ");

        List<WRTraceWorkReportVO> ｗRTraceWorkReportVOs = Lists.newArrayList();
        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        for (int i = 0; i < result.size(); i++) {
            try {
                Object[] record = (Object[]) result.get(i);
                Integer transDepCount = ((BigInteger) record[0]).intValue();
                Integer transPersonCount = ((BigInteger) record[1]).intValue();
                Integer personTraceCount = ((BigInteger) record[2]).intValue();
                Integer depTraceCount = ((BigInteger) record[3]).intValue();
                String trace_typeStr = (String) record[4];
                TraceType trace_type = TraceType.valueOf(trace_typeStr);
                String wr_tag_sid = (String) record[5];
                Date trace_notifyDate = null;
                if (record[6] != null) {
                    trace_notifyDate = (Date) record[6];
                }
                Integer trace_createUserSid = (Integer) record[7];
                String trace_statusStr = (String) record[8];
                TraceStatus trace_status = TraceStatus.valueOf(trace_statusStr);
                String trace_memo = "";
                if (record[9] != null) {

                    trace_memo = (String) record[9];

                }
                String theme = (String) record[10];
                Date update_dt = (Date) record[11];
                Date trace_create_dt = (Date) record[12];
                String wr_sid = (String) record[13];
                String tracesid = (String) record[14];
                WrReadRecord readRecord = wrReadRecordManager.getReadRecord(loginUserSid.toString(), wr_sid);
                WRReadStatus wRReadStatus = WRReadStatus.valueOf(readRecord.getReadStatus());
                boolean toTrace = false;
                if (personTraceCount > 0 || depTraceCount > 0) {
                    toTrace = true;
                }

                String trace_memo_css = "";
                try {
                    if (record[15] != null) {
                        byte[] messageByte = (byte[]) record[15];
                        trace_memo_css = messageByte == null ? "" : new String(messageByte, "UTF-8");
                        trace_memo_css = HtmlUtils.htmlUnescape(trace_memo_css);
                        trace_memo = jsoupUtils.clearCssTag(trace_memo_css);
                    }
                } catch (Exception e) {
                    log.error("trace_memo_css ERROR", e);
                }

                String wr_no = (String) record[16];
                boolean toForwardDep = false;
                if (transDepCount > 0) {
                    toForwardDep = true;
                }
                boolean toForwardMember = false;
                if (transPersonCount > 0) {
                    toForwardMember = true;
                }
                //轉寄類功能尚未製作
                WRTraceWorkReportVO wRTraceWorkReportVO
                        = new WRTraceWorkReportVO(toForwardDep, toForwardMember, toTrace, trace_type, wr_tag_sid,
                                trace_notifyDate, trace_createUserSid, trace_status, trace_memo, theme, update_dt, trace_create_dt, wRReadStatus, wr_sid, tracesid, trace_memo_css, wr_no);
                ｗRTraceWorkReportVOs.add(wRTraceWorkReportVO);
            } catch (Exception e) {
                log.error("getWorkReportListByTraceSearch Error", e);
            }

        }
        return ｗRTraceWorkReportVOs;
    }

    /**
     * 取得個人追蹤未完成筆數 By 登入者Sid 及 工作報告Sid
     *
     * @param sid 工作報告Sid
     * @param loginUserSid 登入者Sid
     * @return
     */
    public Integer getWorkTraceInfoByWr_SidForPersonCountUnFinished(String sid, Integer loginUserSid) {
        try {
            String trace_source_type = "WR";
            List<TraceStatus> traceStatus = Lists.newArrayList();
            traceStatus.add(TraceStatus.UN_FINISHED);
            List<TraceType> traceType = Lists.newArrayList();
            traceType.add(TraceType.PERSONAL);
            traceType.add(TraceType.OTHER_PERSON);
            return workTraceInfoRepository.getWorkTraceInfoCountByTraceSourceTypeAndTraceSourceSidAndTraceStatusAndTraceTypePerson(sid, trace_source_type, traceStatus, traceType, loginUserSid);
        } catch (Exception e) {
            log.error("getWorkTraceInfoByWr_SidForPerson Error : " + e.toString(), e);
            return 0;
        }
    }

    /**
     * 取得個人追蹤(未完成&完成)筆數 By 登入者Sid 及 工作報告Sid
     *
     * @param sid 工作報告Sid
     * @param loginUserSid 登入者Sid
     * @return
     */
    public Integer getWorkTraceInfoByWr_SidForPersonCountAll(String sid, Integer loginUserSid) {
        try {
            String trace_source_type = "WR";
            List<TraceStatus> traceStatus = Lists.newArrayList();
            traceStatus.add(TraceStatus.UN_FINISHED);
            traceStatus.add(TraceStatus.FINISHED);
            List<TraceType> traceType = Lists.newArrayList();
            traceType.add(TraceType.PERSONAL);
            traceType.add(TraceType.OTHER_PERSON);
            return workTraceInfoRepository.getWorkTraceInfoCountByTraceSourceTypeAndTraceSourceSidAndTraceStatusAndTraceTypePerson(sid, trace_source_type, traceStatus, traceType, loginUserSid);
        } catch (Exception e) {
            log.error("getWorkTraceInfoByWr_SidForPerson Error : " + e.toString(), e);
            return 0;
        }
    }

    /**
     * 取得部門追蹤(未完成&完成)筆數 By 登入者部門Sid 及 工作報告Sid
     *
     * @param sid 工作報告Sid
     * @param depSid 登入者部門Sid
     * @return
     */
    public Integer getWorkTraceInfoByWr_SidForDEPCountAll(String sid, Integer depSid) {
        try {
            String trace_source_type = "WR";
            List<TraceStatus> traceStatus = Lists.newArrayList();
            traceStatus.add(TraceStatus.UN_FINISHED);
            traceStatus.add(TraceStatus.FINISHED);
            List<TraceType> traceType = Lists.newArrayList();
            traceType.add(TraceType.DEPARTMENT);
            return workTraceInfoRepository.getWorkTraceInfoCountByTraceSourceTypeAndTraceSourceSidAndTraceStatusAndTraceTypeDep(sid, trace_source_type, traceStatus, traceType, depSid);
        } catch (Exception e) {
            log.error("getWorkTraceInfoByWr_SidForPersonCount Error : " + e.toString(), e);
            return 0;
        }
    }

    /**
     * 取得部門追蹤未完成筆數 By 登入者部門Sid 及 工作報告Sid
     *
     * @param sid 工作報告Sid
     * @param depSid 登入者部門Sid
     * @return
     */
    public Integer getWorkTraceInfoByWr_SidForDEPCountUnFinished(String sid, Integer depSid) {
        try {
            String trace_source_type = "WR";
            List<TraceStatus> traceStatus = Lists.newArrayList();
            traceStatus.add(TraceStatus.UN_FINISHED);
            List<TraceType> traceType = Lists.newArrayList();
            traceType.add(TraceType.DEPARTMENT);
            return workTraceInfoRepository.getWorkTraceInfoCountByTraceSourceTypeAndTraceSourceSidAndTraceStatusAndTraceTypeDep(sid, trace_source_type, traceStatus, traceType, depSid);
        } catch (Exception e) {
            log.error("getWorkTraceInfoByWr_SidForPersonCount Error : " + e.toString(), e);
            return 0;
        }
    }

    /**
     * 取得部門追蹤未完成 By 工作報告Sid
     *
     * @param sid 工作報告Sid
     * @return
     */
    public List<WorkTraceInfo> getWorkTraceInfoByWr_SidForDEP(String sid) {
        try {
            String trace_source_type = "WR";
            List<TraceStatus> traceStatus = Lists.newArrayList();
            traceStatus.add(TraceStatus.UN_FINISHED);
            List<TraceType> traceType = Lists.newArrayList();
            traceType.add(TraceType.DEPARTMENT);
            return workTraceInfoRepository.getWorkTraceInfoByTraceSourceTypeAndTraceSourceSidAndTraceStatusAndTraceType(sid, trace_source_type, traceStatus, traceType);
        } catch (Exception e) {
            log.error("getWorkTraceInfoByWr_SidForDEP Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }

    /**
     * 建立追蹤
     *
     * @param workTraceInfo 追蹤物件
     * @param createUserSid 建立者Sid
     * @return
     */
    public WorkTraceInfo create(WorkTraceInfo workTraceInfo, Integer createUserSid) {
        workTraceInfo.setCreate_usr(createUserSid);
        Date createDate = new Date();
        workTraceInfo.setCreate_dt(createDate);
        workTraceInfo.setTrace_source_type("WR");
        workTraceInfo.setStatus(Activation.ACTIVE);
        workTraceInfo.setUpdate_dt(createDate);
        WorkTraceInfo wr = workTraceInfoRepository.save(workTraceInfo);
        return wr;
    }

    /**
     * 更新追蹤
     *
     * @param workTraceInfo 追蹤物件
     * @param updateUserSid 更新者Sid
     * @return
     */
    public WorkTraceInfo update(WorkTraceInfo workTraceInfo, Integer updateUserSid) {
        Date update = new Date();
        workTraceInfo.setUpdate_dt(update);
        workTraceInfo.setUpdate_usr(updateUserSid);
        WorkTraceInfo wr = workTraceInfoRepository.save(workTraceInfo);
        return wr;
    }

}
