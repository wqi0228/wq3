/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Component
public class WorkSettingUserManager implements Serializable {


    private static final long serialVersionUID = 1734002493958576662L;
    @Autowired
    private WkUserCache wkUserCache;

    public List<User> findAll() {
        return wkUserCache.findAll();
    }

    /**
     * Find Org By SID
     *
     * @param sid
     * @return
     */
    public User findBySID(Integer sid) {
        return wkUserCache.findBySid(sid);
    }

    public User findByID(String id) {
        return wkUserCache.findById(id);
    }

    public List<User> findUserByDepSid(Integer depSid) {
        return wkUserCache.getUsersByPrimaryOrgSid(depSid);
    }
    
    /**
     * 模糊搜尋
     * @param username
     * @return
     */
    public List<User> likeUserByUsername(String username){
        List<User> containsList = Lists.newArrayList();
        if(!Strings.isNullOrEmpty(username)) {
            List<User> userList = wkUserCache.findAll();
            for(User user : userList) {
                if(user.getName().toLowerCase().contains(username.trim().toLowerCase())) {
                    containsList.add(user);
                }
            }
        }
        return containsList;
    }

}
