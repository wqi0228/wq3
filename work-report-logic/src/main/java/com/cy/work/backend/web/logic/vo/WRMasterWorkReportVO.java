/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.vo;

import com.cy.work.backend.web.vo.enums.WREditType;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.cy.work.backend.web.vo.enums.WRStatus;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;

/**
 * 工作報告查詢轉出物件
 * @author brain0925_liao
 */
public class WRMasterWorkReportVO implements Serializable {

    private static final long serialVersionUID = 9028198063897447789L;

    public WRMasterWorkReportVO(String sid, boolean tofowardDep,
            boolean tofowardMember, boolean totrace, Date create_dt,
            String wr_tag_sid, Integer dep_sid, Integer create_usr,
            String theme, WRStatus wr_status, Date update_dt,
            WRReadStatus wRReadStatus, String wr_no, boolean isReadReceiptsMemberResult, WREditType wREditType, boolean readReceiptsMemberSended) {
        this.sid = sid;
        this.tofowardDep = tofowardDep;
        this.tofowardMember = tofowardMember;
        this.totrace = totrace;
        this.create_dt = create_dt;
        this.wr_tag_sid = wr_tag_sid;
        this.dep_sid = dep_sid;
        this.create_usr = create_usr;
        this.theme = theme;
        this.wr_status = wr_status;
        this.update_dt = update_dt;
        this.wRReadStatus = wRReadStatus;
        this.wr_no = wr_no;
        this.readReceiptsMemberResult = isReadReceiptsMemberResult;
        this.readReceiptsMemberSended = readReceiptsMemberSended;
        this.wREditType = wREditType;
    }
    /** 工作報告Sid */
    @Getter
    private final String sid;
    /** 是否有轉寄部門 */
    @Getter
    private final boolean tofowardDep;
    /** 是否有轉寄個人 */
    @Getter
    private final boolean tofowardMember;
    /** 是否有追蹤 */
    @Getter
    private final boolean totrace;
    /** 工作報告建立時間 */
    @Getter
    private final Date create_dt;
    /** 標籤ID */
    @Getter
    private final String wr_tag_sid;
    /** 建立部門Sid */
    @Getter
    private final Integer dep_sid;
    /** 建立使用者Sid */
    @Getter
    private final Integer create_usr;
    /** 標題 */
    @Getter
    private final String theme;
    /** 單據狀態 */
    @Getter
    private final WRStatus wr_status;
    /** 工作報告異動時間 */
    @Getter
    private final Date update_dt;
    /** 登入者已讀未讀狀態 */
    @Getter
    private final WRReadStatus wRReadStatus;
    /** 工作報告單號 */
    @Getter
    private final String wr_no;
    /** 登入者在該單是否為索取讀取回條人員 */
    @Getter
    private final boolean readReceiptsMemberResult;
    /** 登入者在該單是否已回傳索取讀取回條 */
    @Getter
    private final boolean readReceiptsMemberSended;
    /** 編輯權限 */
    @Getter
    private final WREditType wREditType;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRMasterWorkReportVO other = (WRMasterWorkReportVO) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }
    
    
    
}
