/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.commons.vo.Role;
import com.cy.commons.vo.special.UserWithRoles;
import com.cy.system.rest.client.RoleClient;
import com.cy.work.common.cache.WkRoleCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao Role物件管理器
 */
@Slf4j
@Component
public class WorkSettingRoleManager implements Serializable{


    private static final long serialVersionUID = -1420044484938201803L;
    /** Role物件Rest管理器 */
    @Autowired
    private RoleClient roleClient;
    /** Role物件Cache管理器 */
    @Autowired
    private WkRoleCache wkRoleCache;
    @Autowired
    private WkUserWithRolesCache wkUserWithRolesCache;

    /**
     * Find All Role
     *
     * @return
     */
    public List<Role> findAll() {
        return wkRoleCache.findAll();
    }

    /**
     * Find Role By SID
     *
     * @param sid
     * @return
     */
    public Role findBySID(Long sid) {
        return wkRoleCache.findBySid(sid);
    }

    /**
     * Find Role By SID Not From Cache
     *
     * @param sid
     * @return
     */
    public Role findBySIDNoCache(Long sid) {
        Optional<Role> o = roleClient.findBySid(sid);
        if (!o.isPresent()) {
            log.error("查無此角色　sid：" + sid);
            return null;
        }
        Role ow = o.get();
        return ow;
    }

    public List<Role> findRolesByUserSid(Integer userSid) {
        UserWithRoles userWithRole = wkUserWithRolesCache.findBySid(userSid);
        if (userWithRole == null || userWithRole.getRoles() == null) {
            return Lists.newArrayList();
        }
        List<Role> roles = Lists.newArrayList();
        userWithRole.getRoles().forEach(item -> {
            roles.add(findBySID(item.getSid()));
        });
        return roles;
    }

    public Role saveRole(Role role) {
        //Role result = workBackendWebRoleRepository.save(role);
        //wkRoleCache.updateRole(result);
        return null;
    }

}
