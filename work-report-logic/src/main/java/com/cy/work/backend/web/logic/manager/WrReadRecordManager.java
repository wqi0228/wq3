package com.cy.work.backend.web.logic.manager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.work.backend.web.repository.WrReadRecordRepository;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.vo.WrReadRecordVo;

@Service
@Slf4j
public class WrReadRecordManager implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8573885587230792625L;
    @Autowired
    private WrReadRecordRepository wrReadRecordRepository;

    /**
     * 根據主檔sid和使用者id查詢閱讀紀錄
     *
     * @param wrSid
     * @param userId
     * @return
     */
    public WrReadRecord getReadRecord(String userId, String wrSid) {
        Optional<WrReadRecord> readRecordOptional = findByUserSidAndWrSid(userId, wrSid);
        //因拆分閱讀紀錄資料表，如果沒有新的閱讀紀錄資料，則往歷史閱讀資料查找(SQL語法新增的閱讀紀錄)
        if (!readRecordOptional.isPresent()) {
            readRecordOptional = findWrMasterHistoryReadRecord(wrSid);
        }

        if(readRecordOptional.isPresent())
            return readRecordOptional.get();
        log.warn("readRecordOptional 為空");
        return null;
    }

    /**
     * 根據工作報告sid查找出有閱讀紀錄的使用者
     *
     * @param wrSid
     * @return
     */
    public List<WrReadRecord> findReadRecordsByWrSid(String wrSid) {
        return wrReadRecordRepository.findByWrSid(wrSid);
    }

    /**
     * 根據使用者sid查找出所有的閱讀紀錄
     *
     * @param userSid
     * @return
     */
    public List<WrReadRecord> findAllByUserSid(String userSid) {
        return wrReadRecordRepository.findByCreateUser(userSid);
    }

    /**
     * 根據使用者sid list和工作報告sid查找出閱讀紀錄
     *
     * @param userSids
     * @param wrSid
     * @return
     */
    public List<WrReadRecord> findAllByUserSidsAndWrSid(List<String> userSids, String wrSid) {
        return wrReadRecordRepository.findAllByWrSidAndCreateUserIn(wrSid, userSids);
    }

    /**
     * 根據使用者sid和工作報告sid查找閱讀紀錄
     *
     * @param userSid
     * @param wrSid
     * @return
     */
    public Optional<WrReadRecord> findByUserSidAndWrSid(String userSid, String wrSid) {
        return wrReadRecordRepository.findByCreateUserAndWrSid(userSid, wrSid);
    }

    /**
     * 查找工作報告因SQL語法轉檔後的歷史閱讀紀錄
     *
     * @param wrSid
     * @return
     */
    public Optional<WrReadRecord> findWrMasterHistoryReadRecord(String wrSid) {
        return wrReadRecordRepository.findByWrSidAndHistoryNotNull(wrSid);
    }

    /**
     * 儲存閱讀紀錄
     *
     * @param wrReadRecord
     * @return
     */
    public WrReadRecordVo saveWrReadRecord(WrReadRecord wrReadRecord) {
        return WrReadRecordVo.valueOf(wrReadRecordRepository.save(wrReadRecord));
    }

    /**
     * 批次儲存閱讀紀錄
     *
     * @param readRecords
     */
    public void saveAllWrReadRecord(List<WrReadRecord> readRecords) {
        wrReadRecordRepository.save(readRecords);
    }

    /**
     * 更新閱讀紀錄狀態
     *
     * @param wrSid
     * @param userSid
     * @param readStatus
     * @param updateDate
     * @return
     */
    public void updateReadStatus(String wrSid, String userSid, String readStatus, Date updateDate) {
        wrReadRecordRepository.updateReadStatus(wrSid, userSid, readStatus, updateDate);
    }
}
