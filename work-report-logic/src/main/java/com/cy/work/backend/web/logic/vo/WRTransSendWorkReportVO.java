/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.vo;

import com.cy.work.backend.web.vo.enums.ForwardType;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;

/**
 * 寄件備份轉出物件
 * @author brain0925_liao
 */
public class WRTransSendWorkReportVO implements Serializable {

    
    private static final long serialVersionUID = -7432913522760308681L;

    public WRTransSendWorkReportVO(String inbox_group_sid, String wr_Id, String no,
            boolean tofowardDep, boolean tofowardMember,
            boolean totrace, ForwardType sendType,
            String receviceUser, Date sendTime,
            String wr_tag_sid, String theme, Date modifyDate, WRReadStatus wRReadStatus) {
        this.inbox_group_sid = inbox_group_sid;
        this.wr_Id = wr_Id;
        this.no = no;
        this.tofowardDep = tofowardDep;
        this.tofowardMember = tofowardMember;
        this.totrace = totrace;
        this.sendType = sendType;
        this.receviceUser = receviceUser;
        this.sendTime = sendTime;
        this.wr_tag_sid = wr_tag_sid;
        this.theme = theme;
        this.modifyDate = modifyDate;
        this.wRReadStatus = wRReadStatus;
    }

   
    /**轉寄GroupSid*/
    @Getter
    private final String inbox_group_sid;
    /**工作報告Sid*/
    @Getter
    private final String wr_Id;
    /**工作報告單號*/
    @Getter
    private final String no;
    /**是否有轉寄部門*/
    @Getter
    private final boolean tofowardDep;
    /**是否有轉寄個人*/
    @Getter
    private final boolean tofowardMember;
    /**是否有追蹤*/
    @Getter
    private final boolean totrace;
    /**轉寄類型*/
    @Getter
    private final ForwardType sendType;
    /**接收者資訊*/
    @Getter
    private final String receviceUser;
    /**寄送時間*/
    @Getter
    private final Date sendTime;
    /**標籤ID*/
    @Getter
    private final String wr_tag_sid;
    /**主題*/
    @Getter
    private final String theme;
    /**工作報告異動時間*/
    @Getter
    private final Date modifyDate;
    /**工作報告已未讀狀態*/
    @Getter
    private final WRReadStatus wRReadStatus;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.inbox_group_sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRTransSendWorkReportVO other = (WRTransSendWorkReportVO) obj;
        if (!Objects.equals(this.inbox_group_sid, other.inbox_group_sid)) {
            return false;
        }
        return true;
    }

    

   
    
    
}
