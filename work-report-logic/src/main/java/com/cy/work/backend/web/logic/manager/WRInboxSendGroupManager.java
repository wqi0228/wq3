/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.logic.query.WRMasterQueryService;
import com.cy.work.backend.web.logic.utils.ReqularPattenUtils;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRTransSendWorkReportVO;
import com.cy.work.backend.web.repository.WRInboxSendGroupRepository;
import com.cy.work.backend.web.vo.WRInboxSendGroup;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.enums.ForwardType;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.TraceStatus;
import com.cy.work.backend.web.vo.enums.TraceType;
import com.cy.work.backend.web.vo.enums.TransReadStatus;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.cy.work.backend.web.vo.enums.WRTraceType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 轉寄Group
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRInboxSendGroupManager implements Serializable {


    private static final long serialVersionUID = 9187169811183094893L;

    @Autowired
    private WrReadRecordManager wrReadRecordManager;
    @Autowired
    private WRInboxSendGroupRepository wRInboxSendGroupRepository;
    @Autowired
    private ReqularPattenUtils reqularUtils;
    @Autowired
    private WRMasterQueryService wRMasterQueryService;

    /**
     * 取得轉寄Group By group_Sid
     *
     * @param group_Sid group_Sid
     * @return
     */
    public WRInboxSendGroup getWRInboxSendGroupBySid(String group_Sid) {
        return wRInboxSendGroupRepository.findOne(group_Sid);
    }

    public List<WRInboxSendGroup> getWRInboxSendGroupByWrSid(String wrSid) {
        List<WRInboxSendGroup> wRInboxSendGroups = wRInboxSendGroupRepository.findByWRSid(wrSid);
        if (wRInboxSendGroups == null) {
            return Lists.newArrayList();
        }
        return wRInboxSendGroups;
    }

    /**
     * 取得轉寄Group By 工作報告Sid及登入者Sid
     *
     * @param wrSid
     * @param loginUserSid
     * @return
     */
    public List<WRInboxSendGroup> getWRInboxSendGroupByWrSidAndLoginUserSid(String wrSid, Integer loginUserSid) {
        List<WRInboxSendGroup> wRInboxSendGroups = wRInboxSendGroupRepository.findByUserSidAndWRSid(wrSid, loginUserSid);
        if (wRInboxSendGroups == null) {
            return Lists.newArrayList();
        }
        return wRInboxSendGroups;
    }

    /**
     * 建立轉寄Group
     *
     * @param wRInboxSendGroup 轉寄Group物件
     * @param loginUserSid     建立者
     * @return
     */
    public WRInboxSendGroup createWRInboxSendGroup(WRInboxSendGroup wRInboxSendGroup, Integer loginUserSid) {
        wRInboxSendGroup.setStatus(Activation.ACTIVE);
        wRInboxSendGroup.setCreate_usr(loginUserSid);
        wRInboxSendGroup.setCreate_dt(new Date());
        wRInboxSendGroup.setUpdate_dt(new Date());
        return wRInboxSendGroupRepository.save(wRInboxSendGroup);
    }

    /**
     * 更新轉寄Group
     *
     * @param wRInboxSendGroup 轉寄Group物件
     * @param loginUserSid     更新者
     * @return
     */
    public WRInboxSendGroup updateWRInboxSendGroup(WRInboxSendGroup wRInboxSendGroup, Integer loginUserSid) {
        wRInboxSendGroup.setUpdate_usr(loginUserSid);
        wRInboxSendGroup.setUpdate_dt(new Date());
        return wRInboxSendGroupRepository.save(wRInboxSendGroup);
    }

    /**
     * 取得轉寄報表 - 寄件備份
     *
     * @param tagID           介面挑選標籤ID
     * @param transReadStatus 介面挑選閱讀狀態ID
     * @param receiveName    收件者名稱
     * @param receiveOrgs    寄發至 挑選的部門
     * @param title           主題
     * @param content         內容
     * @param searchText      模糊搜尋
     * @param startCreateTime 異動區間起始時間
     * @param endCreateTime   異動區間結束時間
     * @param loginUserSid    登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     * @param wr_Id           工作報告Sid
     * @param transType       寄發類型
     * @return
     */
    public List<WRTransSendWorkReportVO> getSendTrans(String tagID, String transReadStatus,
                                                      String receiveName, List<Integer> receiveOrgs,
                                                      String title, String content, String searchText, Date startCreateTime, Date endCreateTime,
                                                      Integer loginUserSid, Integer loginUserDepSid, String wr_Id, String transType) {
        String trace_source_type = "WR";
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT DISTINCT");
        //查詢轉寄部門
        builder.append(" (SELECT COUNT(wiDep.inbox_sid) as transDepCount FROM wr_inbox wiDep WHERE wiDep.status = 0"
                + " AND wiDep.wr_sid = wr.wr_sid"
                + " AND wiDep.forward_type = '" + ForwardType.DEP.getKey() + "'"
                //+ " AND wiDep.receive_dep = " + loginUserDepSid 
                + " )");
        builder.append(",");
        //查詢轉寄個人
        builder.append(" (SELECT COUNT(wiPerson.inbox_sid) as transPersonCount FROM wr_inbox wiPerson WHERE wiPerson.status = 0"
                + " AND wiPerson.wr_sid = wr.wr_sid"
                + " AND wiPerson.forward_type = '" + ForwardType.PERSON.getKey() + "'"
                //+ " AND wiPerson.receive = " + loginUserSid 
                + " )");
        builder.append(",");
        //查詢追蹤個人       
        builder.append(" (SELECT COUNT(wtp.tracesid) as personTraceCount FROM  wr_trace_info wtp WHERE wtp.status = 0 "
                + " AND wtp.trace_source_sid = wr.wr_sid "
                + " AND wtp.trace_source_type = '" + trace_source_type + "'"
                + " AND wtp.trace_status in ('" + TraceStatus.UN_FINISHED.name() + "')"
                + " AND wtp.trace_type in ('" + TraceType.PERSONAL.name() + "','" + TraceType.OTHER_PERSON.name() + "')"
                + " AND wtp.trace_usr = " + loginUserSid + ")");
        builder.append(",");
        //查詢追蹤部門
        builder.append(" (SELECT COUNT(wtp.tracesid) as depTraceCount FROM  wr_trace_info wtp WHERE wtp.status = 0 "
                + " AND wtp.trace_source_sid = wr.wr_sid "
                + " AND wtp.trace_source_type = '" + trace_source_type + "'"
                + " AND wtp.trace_status in ('" + TraceStatus.UN_FINISHED.name() + "')"
                + " AND wtp.trace_type in ('" + TraceType.DEPARTMENT.name() + "')"
                + " AND wtp.trace_dep = " + loginUserDepSid + ")");
        builder.append(",");
        builder.append("  wig.inbox_group_sid,wig.forward_type,wig.send_to,wig.create_dt as senDate,"
                + "wr.wr_tag_sid,wr.theme,wt.updateDate,"
                + "wr.wr_sid,wr.wr_no FROM wr_inbox_send_group wig ");
        //關連工作報告主表
        builder.append(" INNER JOIN  wr_master wr ON wig.wr_sid = wr.wr_sid  AND wig.status = 0 ");
        builder.append(" INNER JOIN  wr_read_record wrr ON wr.wr_sid = wrr.wr_sid");
        builder.append(" INNER JOIN (SELECT wr_sid, wr_no, max(create_dt) as updateDate,min(create_dt) as createDate FROM wr_trace  GROUP BY wr_sid, wr_no) wt ON wr.wr_sid=wt.wr_sid AND wr.wr_no=wt.wr_no");
        builder.append(" WHERE 1=1  ");
        builder.append(" AND wig.create_usr = '" + loginUserSid + "' ");
        if (!Strings.isNullOrEmpty(tagID)) {
            builder.append(" AND wr.wr_tag_sid = :wr_tag_sid");
            parameters.put("wr_tag_sid", tagID);
        }
        if (!Strings.isNullOrEmpty(transReadStatus)) {
            if (TransReadStatus.NEED_READ.name().equals(transReadStatus)) {
                builder.append(" AND wrr.create_usr = " + "'" + loginUserSid + "'");
                builder.append(" AND wrr.read_status != " + "'" + WRReadStatus.HASREAD.name() + "'");
            } else if (TransReadStatus.WAIT_READ.name().equals(transReadStatus)) {
                builder.append(" AND wrr.create_usr = " + "'" + loginUserSid + "'");
                builder.append(" AND wrr.read_status = " + "'" + WRReadStatus.WAIT_READ.name() + "'");
            } else if (TransReadStatus.NO_READ.name().equals(transReadStatus)) {
                builder.append(" AND wrr.create_usr = " + "'" + loginUserSid + "'");
                builder.append(" AND wrr.read_status != " + "'" + WRReadStatus.HASREAD.name() + "'");
                builder.append(" AND wrr.read_status != " + "'" + WRReadStatus.WAIT_READ.name() + "'");
            } else if (TransReadStatus.READED.name().equals(transReadStatus)) {
                builder.append(" AND wrr.read_status = " + "'" + WRReadStatus.HASREAD.name() + "'");
            } else {
                log.error("傳入參數有問題,transReadStatus[" + transReadStatus + "]比對不到列舉[TransReadStatus]");
            }
        }
        if (!Strings.isNullOrEmpty(receiveName)) {
            String text = "%" + reqularUtils.replaceIllegalSqlLikeStr(receiveName) + "%";
            builder.append(" AND wig.send_to LIKE :receiveName");
            parameters.put("receiveName", text);
        }

        if (receiveOrgs != null && !receiveOrgs.isEmpty()) {
            builder.append(" AND EXISTS(SELECT * FROM wr_inbox wi WHERE wi.status = 0 AND wi.receive_dep in (:receiveOrgs)) ");
            parameters.put("receiveOrgs", receiveOrgs);
        }

        if (!Strings.isNullOrEmpty(transType)) {
            try {
                ForwardType forwardType = ForwardType.valueOf(transType);
                builder.append(" AND wig.forward_type = :forward_type");
                parameters.put("forward_type", forwardType.getKey());
            } catch (Exception e) {
                log.error("ForwardType.valueOf(transType)", e);
            }
        }

        if (!Strings.isNullOrEmpty(title)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(title) + "%";
            builder.append(" AND wr.theme LIKE :theme");
            parameters.put("theme", textNo);
        }
        if (!Strings.isNullOrEmpty(content)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(content) + "%";
            builder.append(" AND wr.content LIKE :content");
            parameters.put("content", textNo);
        }
        if (startCreateTime != null) {
            String startDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), startCreateTime);
            builder.append(" AND wig.create_dt >= :startCreateTime ");
            parameters.put("startCreateTime", startDateStr + " 00:00:00");
        }
        if (endCreateTime != null) {
            String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), endCreateTime);
            builder.append(" AND wig.create_dt <= :endCreateTime ");
            parameters.put("endCreateTime", endDateStr + " 23:59:59");
        }
        if (!Strings.isNullOrEmpty(searchText)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchText) + "%";
            builder.append(" AND (wr.theme LIKE :searchText OR wr.content LIKE :searchText OR wr.memo LIKE :searchText OR EXISTS"
                    + "(SELECT * FROM wr_trace wtrc "
                    + "WHERE wtrc.status = 0 AND wtrc.wr_trace_type = '" + WRTraceType.REPLY.name() + "' AND wtrc.wr_sid = wr.wr_sid AND wtrc.wr_trace_content LIKE :searchText ) )");
            parameters.put("searchText", textNo);
        }
        if (!Strings.isNullOrEmpty(wr_Id)) {
            builder.append(" AND wr.wr_sid = :wr_sid");
            parameters.put("wr_sid", wr_Id);
        }
        builder.append(" ORDER BY wig.create_dt DESC");
        List<WRTransSendWorkReportVO> wRTransSendWorkReportVOs = Lists.newArrayList();
        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        for (int i = 0; i < result.size(); i++) {
            try {
                Object[] record = (Object[]) result.get(i);
                Integer transDepCount = ((BigInteger) record[0]).intValue();
                Integer transPersonCount = ((BigInteger) record[1]).intValue();
                Integer personTraceCount = ((BigInteger) record[2]).intValue();
                Integer depTraceCount = ((BigInteger) record[3]).intValue();
                String inbox_group_sid = (String) record[4];
                Integer forward_typeKey = (Integer) record[5];
                ForwardType forwardType = null;
                for (ForwardType value : ForwardType.values()) {
                    if (value.getKey().equals(forward_typeKey)) {
                        forwardType = value;
                        break;
                    }
                }
                String send_to = (String) record[6];
                Date create_dt = (Date) record[7];
                String wr_tag_sid = (String) record[8];
                String theme = (String) record[9];
                Date update_dt = (Date) record[10];
                String wr_sid = (String) record[11];
                String wr_no = (String) record[12];
                WrReadRecord readRecord = wrReadRecordManager.getReadRecord(loginUserSid.toString(), wr_sid);
                WRReadStatus wRReadStatus = WRReadStatus.valueOf(readRecord.getReadStatus());

                boolean toTrace = false;
                if (personTraceCount > 0 || depTraceCount > 0) {
                    toTrace = true;
                }
                boolean toForwardDep = false;
                if (transDepCount > 0) {
                    toForwardDep = true;
                }
                boolean toForwardMember = false;
                if (transPersonCount > 0) {
                    toForwardMember = true;
                }
                WRTransSendWorkReportVO wRTransSendWorkReportVO
                        = new WRTransSendWorkReportVO(inbox_group_sid,
                        wr_sid, wr_no, toForwardDep,
                        toForwardMember, toTrace,
                        forwardType, send_to,
                        create_dt, wr_tag_sid,
                        theme, update_dt, wRReadStatus);
                wRTransSendWorkReportVOs.add(wRTransSendWorkReportVO);
            } catch (Exception e) {
                log.error("getSendTrans Error", e);
            }
        }
        return wRTransSendWorkReportVOs;
    }

}
