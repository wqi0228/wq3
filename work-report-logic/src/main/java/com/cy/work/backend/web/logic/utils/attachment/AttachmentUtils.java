/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.utils.attachment;

import com.cy.work.backend.web.logic.vo.AttachmentVO;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class AttachmentUtils {

    public static final String MULTIFILES_ZIP_FILENAME = "multi-files.zip";

    public static final String DEFAULT_MIME_TYPE = "application/octet-stream";

    /**
     * 讓檔案在本類別的 close() 被呼叫之後自動刪除檔案.
     *
     * @author thomas
     *
     */
    private static class AutoDeletedFileInputStream extends FileInputStream {

        private File file = null;

        public AutoDeletedFileInputStream(File file) throws FileNotFoundException {
            super(file);
            this.file = file;
        }

        @Override
        public void close() throws IOException {
            super.close();
            if(!file.delete())
                log.warn("刪檔失敗");
        }

    }

    /**
     * server side 檔案名稱.
     *
     * @param attachmentVO 附加檔案的資料庫實例.
     * @return 實際存放在 server 上的檔案名稱.
     */
    public static String getServerSideFileName(AttachmentVO attachmentVO) {
        return attachmentVO.getAttSid() + "-" + attachmentVO.getAttName();
    }

    /**
     * 取得檔案在伺服器端的實際路徑.
     *
     * @param attachmentVO 附加檔案 entity.
     * @param directoryName 依照單據種類而異的目錄名稱.
     * @return 附加檔案的 <code>File</code> 物件.
     */
    public static File getServerSideFile(AttachmentVO attachmentVO, String directoryName) {
        File serverSideDirectory = new File(getUploadRoot(), directoryName);
        File fullPathFile
                = new File(serverSideDirectory, AttachmentUtils.getServerSideFileName(attachmentVO));
        return fullPathFile;
    }

    /**
     * 伺服器端檔案上傳路徑的根目錄.
     *
     * @return 附加檔案根目錄.
     */
    public static File getUploadRoot() {
        String dir = Env.getInstance().getAttachmentRoot();
        return new File(dir);
    }

    /**
     * 用來建立各單據種類的子目錄.
     *
     * @param directoryName 子目錄名稱，相對於 uploadRoot.
     * @return 代表該子目錄的 File 物件.
     */
    public static File createDirectoryIfNotExist(String directoryName) {
        File directory = new File(getUploadRoot(), directoryName);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        return directory;
    }

    /**
     * 將輸入資料流轉存為暫存檔，然後關閉輸入資料流.
     *
     * @param in 輸入資料流.
     * @param filenameExtension 附檔名（不包含 "." ）.
     * @return 一個暫存檔案，該檔案放置於附加檔案最終所應該放置的目錄.
     * @throws IOException
     */
    public static File saveToTemporaryFile(InputStream in, String directoryName,
            String filenameExtension) throws IOException {
        File tempFile
                = File.createTempFile("temp", "." + filenameExtension, new File(getUploadRoot(),
                        directoryName));
        BufferedOutputStream out = null;
        FileOutputStream stream = null;
        //經過測試Sonar寫法執行有問題
        try {
            stream = new FileOutputStream(tempFile);
            out = new BufferedOutputStream(stream);
            IOUtils.copy(in, out);
        } catch (Exception e) {
            if (!tempFile.delete())
                log.warn("刪檔失敗");
            throw e;
        } finally {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
            if (stream!=null)
                stream.close();
        }
        return tempFile;
    }

    /**
     * 取得 PrimeFaces 的 fileDownload 標籤所需要的 StreamedContent 物件實體.
     *
     * @param attachmentVO
     * @return
     * @throws FileNotFoundException
     */
    public static StreamedContent createStreamedContent(AttachmentVO attachmentVO, String directoryName) throws FileNotFoundException {
        String mimeType = getNoneNullMimeType(FacesContext.getCurrentInstance(), attachmentVO.getAttName());
        FileInputStream stream = new FileInputStream(getServerSideFile(attachmentVO, directoryName));
        return new DefaultStreamedContent(stream, mimeType);
    }

    /**
     * 判斷 file 的 MIME 類型，若無法判斷類型，則回傳預設類型.
     *
     * @param fileName 檔案名稱.
     * @return 檔案的 MIME 類型或預設的 MIME 類型.
     */
    public static String getNoneNullMimeType(FacesContext ctx, String fileName) {
        String mimeType = ctx.getExternalContext().getMimeType(fileName);
        return (mimeType == null) ? DEFAULT_MIME_TYPE : mimeType;
    }

    public static String getNoneNullMimeType(ServletContext ctx, String fileName) {
        String mimeType = ctx.getMimeType(fileName);
        return (mimeType == null) ? DEFAULT_MIME_TYPE : mimeType;
    }

    /**
     * 將多個附加檔案的檔案內容壓縮成一個 zip 檔案再建立 StreamedContent.<br>
     * 下載完成後該檔案會自動刪除.
     *
     * @param attachments
     * @param directoryName
     * @return
     * @throws IOException
     */
    public static StreamedContent createWrappedStreamedContent(List<AttachmentVO> attachments,
            String directoryName) throws IOException {
        BufferedInputStream in = null;
        File tempFile = null;
        ZipOutputStream out = null;
        HashMap<String, Integer> frequencies = new HashMap<>();
        boolean exceptionHappened = false;

        try {
            tempFile = File.createTempFile("wrapped-", ".zip");
            out = new ZipOutputStream(new FileOutputStream(tempFile));
            for (AttachmentVO attachment : attachments) {
                // 為了替相同檔名的檔案加上編號，在讀取檔案的過程中用 Map 統計每個檔名的出現次數
                String fileName = attachment.getAttName();
                if (frequencies.containsKey(fileName)) {
                    frequencies.put(fileName, frequencies.get(fileName) + 1);
                } else {
                    frequencies.put(fileName, 1);
                }

                Integer frequency = frequencies.get(fileName);
                String outputFileName
                        = (frequency.equals(1))
                        ? fileName
                        : insertFileNumber(fileName, frequencies.get(fileName));
                File file = getServerSideFile(attachment, directoryName);
                FileInputStream stream = null;
                try {
                    stream = new FileInputStream(file);
                    in = new BufferedInputStream(stream);
                    out.putNextEntry(new ZipEntry(outputFileName));
                    IOUtils.copy(in, out);
                } catch (Exception e) {
                    log.error("Error", e);
                    exceptionHappened = true;
                    throw e;
                } finally {
                    out.closeEntry();
                    in.close();
                    if(stream!=null)
                        stream.close();
                }
            }
        } catch (IOException e) {
            // out 關閉以後才能刪除 tempFile, 但是關閉 out 的責任屬於 finally 區塊
            // 故使用 exceptionHappened 將發生 Exception 的事實傳遞到 finally 區塊
            exceptionHappened = true;
            throw e;
        } finally {
            frequencies.clear();
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
            if (exceptionHappened) {
                FileUtils.deleteQuietly(tempFile);
            }
        }

        return new DefaultStreamedContent(new AutoDeletedFileInputStream(tempFile), "application/zip");
    }

    /**
     * 在檔名中插入序號. 在壓縮多檔案的時候若遇到原始檔名相同的情況就會需要編號.
     *
     * @param fileName 原始檔名.
     * @param number 序號.
     * @return 將序號插入副檔名之前的結果.
     */
    private static String insertFileNumber(String fileName, Integer number) {
        int dotIndex = fileName.lastIndexOf(".");
        String beforeDot = (dotIndex == -1) ? fileName : fileName.substring(0, dotIndex);
        String afterDot = (dotIndex == -1) ? "" : fileName.substring(dotIndex);
        return beforeDot + "(" + number + ")" + afterDot;
    }

    public static String encodeFileName(String fileName) {
        String encodedFileName = fileName;
        try {
            URI uri = new URI(null, null, fileName, null);
            encodedFileName = uri.toASCIIString();
        } catch (URISyntaxException e) {
            log.error("encodeFileName Error", e);
            System.err.println(e.getMessage());
        }
        return encodedFileName;
    }

    private static String getUserAgent(HttpServletRequest request) {
        return request.getHeader("User-Agent");
    }

    public static boolean isMSIE(HttpServletRequest request) {
        String userAgent = getUserAgent(request);
        boolean msie = userAgent.contains("MSIE");
        boolean trident = userAgent.contains("Trident/");
        return msie || trident;
    }
    
    

}
