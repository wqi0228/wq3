/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.work.backend.web.repository.WRTraceRepository;
import com.cy.work.backend.web.vo.WRTrace;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRTraceManager implements Serializable{


    private static final long serialVersionUID = 7309747494092899403L;
    @Autowired
    private WRTraceRepository wRTraceRepository;

    /**
     * 取得追蹤資料 By Sid
     * @param sid
     * @return 
     */
    public WRTrace getWRTraceBySid(String sid) {
        try {
            return wRTraceRepository.findOne(sid);
        } catch (Exception e) {
            log.error("getWRTraceBySid Error : " + e.toString(),e);
        }
        return null;
    }

    /**
     * 建立追蹤資料
     * @param wRTrace 追蹤物件
     * @param createUserSid 建立者Sid
     * @return 
     */
    public WRTrace create(WRTrace wRTrace, Integer createUserSid) {
        try {
            wRTrace.setCreate_usr(createUserSid);
            Date createDate = new Date();
            wRTrace.setCreate_dt(createDate);
            wRTrace.setUpdate_dt(createDate);
            WRTrace wr = wRTraceRepository.save(wRTrace);
            return wr;
        } catch (Exception e) {
            log.error("save Error : " + e.toString(),e);
        }
        return null;
    }

    /**
     * 取得追蹤資料 By 工作報告Sid
     * @param wr_id 工作報告Sid
     * @return 
     */
    public List<WRTrace> getWRTracesByWRSid(String wr_id) {
        try {
            return wRTraceRepository.getWRTraceByWrSid(wr_id);
        } catch (Exception e) {
            log.error("getWRTracesByWRSid Error : ", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 更新追蹤資料 
     * @param wRTrace 追蹤資料
     * @param updateUserSid 更新者Sid
     * @return 
     */
    public WRTrace update(WRTrace wRTrace, Integer updateUserSid) {
        try {
            wRTrace.setUpdate_usr(updateUserSid);
            wRTrace.setUpdate_dt(new Date());
            WRTrace wr = wRTraceRepository.save(wRTrace);
            return wr;
        } catch (Exception e) {
            log.error("save Error : " + e.toString(),e);
        }
        return null;
    }

}
