/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.work.backend.web.vo.WRGroupSetupPrivate;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.cy.work.backend.web.repository.WRGroupSetupPrivateRepository;

/**
 * 群組
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRGroupSetupPrivateManager implements Serializable {


    private static final long serialVersionUID = 7576944486225961723L;
    @Autowired
    private WRGroupSetupPrivateRepository wrGroupSetupPrivateRepository;
   
    /**
     * 取得群組 By 群組Sid
     * @param sid 群組Sid
     * @return 
     */
    public WRGroupSetupPrivate getWRGroupSetupPrivateBySid(String sid) {
        try {
            return wrGroupSetupPrivateRepository.findOne(sid);
        } catch (Exception e) {
            log.error("getWRGroupSetupPrivateBySid Error : " + e.toString(), e);
            return null;
        }
    }

    /**
     * 取得群組List By 登入者Sid
     * @param create_usr 登入者Sid
     * @return 
     */
    public List<WRGroupSetupPrivate> getWRFunItemByCategoryModel(Integer create_usr) {
        try {
            String type = "WR";
            return wrGroupSetupPrivateRepository.getWRGroupSetupPrivateByCreateUsr(create_usr, type);
        } catch (Exception e) {
            log.error("getWRFunItemByCategoryModel Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }
  
    /**
     * 建立群組
     * @param wrGroupSetupPrivate 群組物件
     * @param create_usr 建立者Sid
     * @return 
     */
    public WRGroupSetupPrivate create(WRGroupSetupPrivate wrGroupSetupPrivate, Integer create_usr) {
        try {
            String type = "WR";
            wrGroupSetupPrivate.setType(type);
            wrGroupSetupPrivate.setCreate_dt(new Date());
            wrGroupSetupPrivate.setCreate_usr(create_usr);
            return wrGroupSetupPrivateRepository.save(wrGroupSetupPrivate);
        } catch (Exception e) {
            log.error("create Error : " + e.toString(), e);
            return null;
        }
    }

    /**
     * 刪除群組 By 群組Sid
     * @param sid 群組Sid
     */
    public void delete(String sid) {
        wrGroupSetupPrivateRepository.delete(sid);
    }

    /**
     * 更新群組 
     * @param wrGroupSetupPrivate 群組物件
     * @param update_usr 更新者Sid
     * @return 
     */
    public WRGroupSetupPrivate update(WRGroupSetupPrivate wrGroupSetupPrivate, Integer update_usr) {
        try {
            wrGroupSetupPrivate.setUpdate_dt(new Date());
            wrGroupSetupPrivate.setUpdate_usr(update_usr);
            return wrGroupSetupPrivateRepository.save(wrGroupSetupPrivate);
        } catch (Exception e) {
            log.error("update Error : " + e.toString(), e);
            return null;
        }
    }
}
