/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.utils.attachment;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.common.ImageMetadata.ImageMetadataItem;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.commons.io.IOUtils;

@Slf4j
public class JpegExifRewriter {

    /**
     * 檢查輸入檔案的 EXIF 資訊，若有必要則用編輯過的 EXIF 資訊取代並輸出為新檔案.
     *
     * @param inputFile 輸入檔案.
     * @param outputFile 輸出目標.
     * @return true 代表輸入檔是一個 JPEG 檔案且包含 EXIF 資訊，而且 EXIF 資訊中包含 Orientation
     * 欄位，該欄位的數值非預設的「水平」，然後將該數值改為「水平」並輸出 至目標檔案；若不需要改寫 EXIF 資訊則為 false
     * ，此時沒有產生新檔案.
     * @throws ImageReadException
     * @throws IOException
     * @throws ImageWriteException
     */
    public boolean rewriteExif(File inputFile, File outputFile)
            throws ImageReadException, IOException, ImageWriteException {
        ImageMetadata imageMetadata = Imaging.getMetadata(inputFile);
        if (imageMetadata == null) {
            return false;
        }

        if (!(imageMetadata instanceof JpegImageMetadata)) {
            return false;
        }
        JpegImageMetadata jpegMetadata = (JpegImageMetadata) imageMetadata;

        TiffImageMetadata tiffMetadata = jpegMetadata.getExif();
        if (tiffMetadata == null) {
            return false;
        }

        List<Integer> interestedDirTypes = getDirectoryTypesContaingAbnormalOrientation(tiffMetadata);
        if (interestedDirTypes.size() == 0) {
            return false;
        }

        TiffOutputSet outputSet = updateOutputSet(tiffMetadata,
                interestedDirTypes);
        saveWithNewExif(inputFile, outputFile, outputSet);

        return true;
    }

    private List<Integer> getDirectoryTypesContaingAbnormalOrientation(
            TiffImageMetadata tiffImageMetadata) throws ImageReadException {
        List<? extends ImageMetadataItem> directories = tiffImageMetadata
                .getDirectories();
        ArrayList<Integer> interestedDirTypes = new ArrayList<Integer>(
                directories.size());

        for (ImageMetadataItem dirItem : directories) {
            TiffImageMetadata.Directory dir = (TiffImageMetadata.Directory) dirItem;
            TiffField orientationField = dir
                    .findField(TiffTagConstants.TIFF_TAG_ORIENTATION);
            if (orientationField == null) {
                continue;
            }
            if (orientationField
                    .getValue()
                    .equals((short) TiffTagConstants.ORIENTATION_VALUE_HORIZONTAL_NORMAL)) {
                continue;
            }
            interestedDirTypes.add(dir.type);
        }
        return interestedDirTypes;
    }

    private TiffOutputSet updateOutputSet(TiffImageMetadata tiffImageMetadata,
            List<Integer> interestedDirTypes) throws ImageWriteException {
        TiffOutputSet outputSet = tiffImageMetadata.getOutputSet();
        for (Integer dirType : interestedDirTypes) {
            TiffOutputDirectory outputDir = outputSet.findDirectory(dirType);
            outputDir.removeField(TiffTagConstants.TIFF_TAG_ORIENTATION);
            outputDir
                    .add(TiffTagConstants.TIFF_TAG_ORIENTATION,
                            (short) TiffTagConstants.ORIENTATION_VALUE_HORIZONTAL_NORMAL);
            outputDir.sortFields();
        }
        return outputSet;
    }

    private void saveWithNewExif(File inputFile, File outputFile,
            TiffOutputSet outputSet) throws ImageReadException,
            ImageWriteException, IOException {
        BufferedOutputStream out = null;
        FileOutputStream stream = null;
        ExifRewriter exifRewriter = new ExifRewriter();
        try {
            stream = new FileOutputStream(outputFile);
            out = new BufferedOutputStream(stream);
            exifRewriter.updateExifMetadataLossless(inputFile, out, outputSet);
        } catch (Exception e) {
            log.error("saveWithNewExif", e);
        } finally {
            IOUtils.closeQuietly(out);
            if(stream!=null)
                stream.close();
        }
    }

}
