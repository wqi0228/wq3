/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.query.WRMasterQueryService;
import com.cy.work.backend.web.logic.utils.ReqularPattenUtils;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRTransIncomeWorkReportVO;
import com.cy.work.backend.web.logic.vo.WRTransReceviceDepWorkReportVO;
import com.cy.work.backend.web.repository.WRInboxRepository;
import com.cy.work.backend.web.vo.WRInbox;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.enums.*;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 轉寄細項
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRInboxManager implements Serializable {

    private static final long serialVersionUID = -8606424969933139338L;
    @Autowired
    private WRInboxRepository workInboxRepository;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WRMasterQueryService wRMasterQueryService;
    @Autowired
    private WrReadRecordManager wrReadRecordManager;
    @Autowired
    private ReqularPattenUtils reqularUtils;

    /**
     * 取得轉寄細項List By 工作報告Sid 及 轉寄類型
     *
     * @param wrSid        工作報告Sid
     * @param forwardType  轉寄類型
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<WRInbox> getWorkInboxByWrSidAndForwardTypeAndLoginUserSid(String wrSid, ForwardType forwardType, Integer loginUserSid) {
        return workInboxRepository.findByInboxByWrSidAndForwardType(wrSid, forwardType);
    }

    public List<WRInbox> getByReceiveSid(Integer receiveSid) {
        return workInboxRepository.findByReceiveSid(receiveSid);
    }

    public List<WRInbox> getByReceiveDepSid(Integer receiveDepSid) {
        return workInboxRepository.findByReceiveDepSid(receiveDepSid);
    }

    /**
     * 取得轉寄細項List By 工作報告Sid
     *
     * @param wrSid 工作報告Sid
     * @return
     */
    public List<WRInbox> getWorkInboxByWrSidAndStatusActive(String wrSid) {
        return workInboxRepository.findByInboxByWrSidAndStatusActive(wrSid);
    }

    /**
     * 取得轉寄細項List By 轉寄細項Sids
     *
     * @param sids 轉寄細項Sids
     * @return
     */
    public List<WRInbox> getWorkInboxBySids(List<String> sids) {
        return workInboxRepository.findByInboxBySids(sids);
    }

    /**
     * 取得轉寄個人各類型筆數
     *
     * @param transReadStatus 轉寄閱讀類型
     * @param startCreateTime 工作報告異動起始時間
     * @param endCreateTime   工作報告異動結束時間
     * @param loginUserSid    登入者Sid
     * @param inboxType       轉寄方式
     * @return
     */
    public Integer getIncomeBoxCount(String transReadStatus, Date startCreateTime, Date endCreateTime, Integer loginUserSid, InboxType inboxType) {
        if (inboxType == null) {
            return 0;
        }
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        builder.append(" wi.inbox_sid FROM wr_inbox wi ");
        //關連工作報告主表
        builder.append(" INNER JOIN  wr_master wr ON wi.wr_sid = wr.wr_sid ");
        builder.append(" INNER JOIN  wr_read_record wrr ON wr.wr_sid = wrr.wr_sid");
        builder.append(" AND wi.status = 0");
        builder.append(" WHERE 1=1  ");
        builder.append(" AND wi.forward_type = '" + ForwardType.PERSON.getKey() + "' ");
        builder.append(" AND wi.receive = '" + loginUserSid + "' ");
        builder.append(" AND wi.inbox_type = '" + inboxType.name() + "' ");
        if (!Strings.isNullOrEmpty(transReadStatus)) {
            if (TransReadStatus.NEED_READ.name().equals(transReadStatus)) {
                builder.append(" AND wrr.create_usr = " + "'" + loginUserSid + "'");
                builder.append(" AND wrr.read_status != " + "'" + WRReadStatus.HASREAD.name() + "'");
            } else if (TransReadStatus.WAIT_READ.name().equals(transReadStatus)) {
                builder.append(" AND wrr.create_usr = " + "'" + loginUserSid + "'");
                builder.append(" AND wrr.read_status = " + "'" + WRReadStatus.WAIT_READ.name() + "'");
            } else if (TransReadStatus.NO_READ.name().equals(transReadStatus)) {
                builder.append(" AND wrr.create_usr = " + "'" + loginUserSid + "'");
                builder.append(" AND wrr.read_status != " + "'" + WRReadStatus.HASREAD.name() + "'");
                builder.append(" AND wrr.read_status != " + "'" + WRReadStatus.WAIT_READ.name() + "'");
            } else if (TransReadStatus.READED.name().equals(transReadStatus)) {
                builder.append(" AND wrr.read_status = " + "'" + WRReadStatus.HASREAD.name() + "'");
            } else {
                log.error("傳入參數有問題,transReadStatus[" + transReadStatus + "]比對不到列舉[TransReadStatus]");
            }
        }
        if (startCreateTime != null) {
            String startDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), startCreateTime);
            builder.append(" AND wi.create_dt >= :startCreateTime ");
            parameters.put("startCreateTime", startDateStr + " 00:00:00");
        }
        if (endCreateTime != null) {
            String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), endCreateTime);
            builder.append(" AND wi.create_dt <= :workreportmodifyEnd ");
            parameters.put("workreportmodifyEnd", endDateStr + " 23:59:59");
        }
        List<String> inbox_sids = Lists.newArrayList();
        try {
            @SuppressWarnings("rawtypes")
            List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);

            for (int i = 0; i < result.size(); i++) {
                String inbox_sid = (String) result.get(i);
                inbox_sids.add(inbox_sid);
            }
            return inbox_sids.size();
        } catch (Exception e) {
            log.error("getIncomeBoxCount", e);
        }
        return 0;
    }

    /**
     * 取得轉寄個人各類型報表物件
     *
     * @param tagID           介面挑選標籤ID
     * @param transReadStatus 介面挑選已未讀ID
     * @param sendOrgs        介面挑選寄送單位
     * @param sendName        寄送者名稱
     * @param title           標題
     * @param content         內容
     * @param searchText      模糊搜尋
     * @param startCreateTime 工作報告異動起始時間
     * @param endCreateTime   工作報告異動結束時間
     * @param loginUserSid    登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     * @param wr_Id           工作報告Sid
     * @param inboxType       轉寄方式
     * @return
     */
    public List<WRTransIncomeWorkReportVO> getIncomeBox(String tagID, String transReadStatus,
                                                        List<Integer> sendOrgs, String sendName,
                                                        String title, String content,
                                                        String searchText, Date startCreateTime,
                                                        Date endCreateTime, Integer loginUserSid,
                                                        Integer loginUserDepSid, String wr_Id, InboxType inboxType) {
        if (inboxType == null) {
            return Lists.newArrayList();
        }

        String trace_source_type = "WR";
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        //查詢轉寄部門
        builder.append(" (SELECT COUNT(wiDep.inbox_sid) as transDepCount FROM wr_inbox wiDep WHERE wiDep.status = 0"
                + " AND wiDep.wr_sid = wr.wr_sid"
                + " AND wiDep.forward_type = '" + ForwardType.DEP.getKey() + "'"
                //  + " AND wiDep.receive_dep = " + loginUserDepSid
                + " )");
        builder.append(",");
        //查詢轉寄個人
        builder.append(" (SELECT COUNT(wiPerson.inbox_sid) as transPersonCount FROM wr_inbox wiPerson WHERE wiPerson.status = 0"
                + " AND wiPerson.wr_sid = wr.wr_sid"
                + " AND wiPerson.forward_type = '" + ForwardType.PERSON.getKey() + "'"
                // + " AND wiPerson.receive = " + loginUserSid
                + " )");
        builder.append(",");
        //查詢追蹤個人       
        builder.append(" (SELECT COUNT(wtp.tracesid) as personTraceCount FROM  wr_trace_info wtp WHERE wtp.status = 0 "
                + " AND wtp.trace_source_sid = wr.wr_sid "
                + " AND wtp.trace_source_type = '" + trace_source_type + "'"
                + " AND wtp.trace_status in ('" + TraceStatus.UN_FINISHED.name() + "')"
                + " AND wtp.trace_type in ('" + TraceType.PERSONAL.name() + "','" + TraceType.OTHER_PERSON.name() + "')"
                + " AND wtp.trace_usr = " + loginUserSid + ")");
        builder.append(",");
        //查詢追蹤部門
        builder.append(" (SELECT COUNT(wtp.tracesid) as depTraceCount FROM  wr_trace_info wtp WHERE wtp.status = 0 "
                + " AND wtp.trace_source_sid = wr.wr_sid "
                + " AND wtp.trace_source_type = '" + trace_source_type + "'"
                + " AND wtp.trace_status in ('" + TraceStatus.UN_FINISHED.name() + "')"
                + " AND wtp.trace_type in ('" + TraceType.DEPARTMENT.name() + "')"
                + " AND wtp.trace_dep = " + loginUserDepSid + ")");
        builder.append(",");
        builder.append("  wi.sender_dep,wi.sender,wi.create_dt as senDate,"
                + "wr.wr_tag_sid,wr.theme,wt.updateDate,"
                + "wr.wr_sid,wr.wr_no,wi.inbox_sid FROM wr_inbox wi ");
        //關連工作報告主表
        builder.append(" INNER JOIN  wr_master wr ON wi.wr_sid = wr.wr_sid  AND wi.status = 0 ");
        builder.append(" INNER JOIN (SELECT wr_sid, wr_no, max(create_dt) as updateDate,min(create_dt) as createDate FROM wr_trace  GROUP BY wr_sid, wr_no) wt ON wr.wr_sid=wt.wr_sid AND wr.wr_no=wt.wr_no");
        builder.append(" WHERE 1=1  ");
        builder.append(" AND wi.forward_type = '" + ForwardType.PERSON.getKey() + "' ");
        builder.append(" AND wi.receive = '" + loginUserSid + "' ");
        builder.append(" AND wi.inbox_type = '" + inboxType.name() + "' ");
        if (!Strings.isNullOrEmpty(tagID)) {
            builder.append(" AND wr.wr_tag_sid = :wr_tag_sid");
            parameters.put("wr_tag_sid", tagID);
        }

        if (sendOrgs != null && !sendOrgs.isEmpty()) {
            builder.append(" AND wi.sender_dep in (:sendOrgs)");
            parameters.put("sendOrgs", sendOrgs);
        }
        if (!Strings.isNullOrEmpty(sendName.trim())) {
            List<User> likeUserList = workSettingUserManager.likeUserByUsername(sendName.trim());
            if (CollectionUtils.isEmpty(likeUserList)) {
                return Lists.newArrayList();
            } else {
                builder.append(" AND wr.create_usr in (:userSids)");
                parameters.put("userSids", likeUserList.stream().map(User::getSid).collect(Collectors.toList()));
            }
        }
        if (!Strings.isNullOrEmpty(title)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(title) + "%";
            builder.append(" AND wr.theme LIKE :theme");
            parameters.put("theme", textNo);
        }
        if (!Strings.isNullOrEmpty(content)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(content) + "%";
            builder.append(" AND wr.content LIKE :content");
            parameters.put("content", textNo);
        }

        if (startCreateTime != null) {
            String startDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), startCreateTime);
            builder.append(" AND wi.create_dt >= :startCreateTime ");
            parameters.put("startCreateTime", startDateStr + " 00:00:00");
        }
        if (endCreateTime != null) {
            String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), endCreateTime);
            builder.append(" AND wi.create_dt <= :endCreateTime ");
            parameters.put("endCreateTime", endDateStr + " 23:59:59");
        }

        if (!Strings.isNullOrEmpty(searchText)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchText) + "%";
            builder.append(" AND (wr.theme LIKE :searchText OR wr.content LIKE :searchText OR wr.memo LIKE :searchText OR EXISTS"
                    + "(SELECT * FROM wr_trace wtrc "
                    + "WHERE wtrc.status = 0 AND wtrc.wr_trace_type = '" + WRTraceType.REPLY.name() + "' AND wtrc.wr_sid = wr.wr_sid AND wtrc.wr_trace_content LIKE :searchText ) )");
            parameters.put("searchText", textNo);
        }
        if (!Strings.isNullOrEmpty(wr_Id)) {
            builder.append(" AND wr.wr_sid = :wr_sid");
            parameters.put("wr_sid", wr_Id);
        }
        builder.append(" ORDER BY wi.create_dt DESC");
        List<WRTransIncomeWorkReportVO> wRTransIncomeWorkReportVOs = Lists.newArrayList();

        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        for (int i = 0; i < result.size(); i++) {
            try {
                Object[] record = (Object[]) result.get(i);
                Integer transDepCount = ((BigInteger) record[0]).intValue();
                Integer transPersonCount = ((BigInteger) record[1]).intValue();
                Integer personTraceCount = ((BigInteger) record[2]).intValue();
                Integer depTraceCount = ((BigInteger) record[3]).intValue();
                Integer sender_dep = (Integer) record[4];
                Integer sender = (Integer) record[5];
                Date create_dt = (Date) record[6];
                String wr_tag_sid = (String) record[7];
                String theme = (String) record[8];
                Date update_dt = (Date) record[9];
                String wr_sid = (String) record[10];
                String wr_no = (String) record[11];
                String inbox_sid = (String) record[12];
                WrReadRecord readRecord = wrReadRecordManager.getReadRecord(loginUserSid.toString(), wr_sid);
                WRReadStatus wRReadStatus = WRReadStatus.valueOf(readRecord.getReadStatus());

                boolean toTrace = false;
                if (personTraceCount > 0 || depTraceCount > 0) {
                    toTrace = true;
                }
                boolean toForwardDep = false;
                if (transDepCount > 0) {
                    toForwardDep = true;
                }
                boolean toForwardMember = false;
                if (transPersonCount > 0) {
                    toForwardMember = true;
                }
                WRTransIncomeWorkReportVO wRTransIncomeWorkReportVO
                        = new WRTransIncomeWorkReportVO(wr_sid, wr_no, toForwardDep, toForwardMember, toTrace,
                        sender_dep, sender, create_dt, wr_tag_sid, theme, update_dt, wRReadStatus, inbox_sid);
                wRTransIncomeWorkReportVOs.add(wRTransIncomeWorkReportVO);
            } catch (Exception e) {
                log.error("getWorkReportListByTraceSearch Error", e);
            }
        }
        return wRTransIncomeWorkReportVOs;
    }

    /**
     * 取得部門轉寄筆數
     *
     * @param transReadStatus 介面挑選已未讀ID
     * @param receiveOrgs     寄發至部門Sid (若未挑選,會直接回傳0)
     * @param startCreateTime 工作報告異動起始時間
     * @param endCreateTime   工作報告異動結束時間
     * @param loginUserSid    登入者Sid
     * @return
     */
    public Integer getReceiveDepCount(String transReadStatus, List<Integer> receiveOrgs, Date startCreateTime, Date endCreateTime, Integer loginUserSid) {
        if (receiveOrgs == null || receiveOrgs.isEmpty()) {
            return 0;
        }
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        builder.append(" wr.wr_sid FROM wr_inbox wi ");
        builder.append(" INNER JOIN  wr_master wr ON wi.wr_sid = wr.wr_sid ");
        builder.append(" INNER JOIN  wr_read_record wrr ON wr.wr_sid = wrr.wr_sid");
        builder.append(" AND wi.status = 0 ");
        builder.append(" WHERE 1=1  ");
        builder.append(" AND wi.forward_type = '" + ForwardType.DEP.getKey() + "' ");
        if (!Strings.isNullOrEmpty(transReadStatus)) {
            if (TransReadStatus.NEED_READ.name().equals(transReadStatus)) {
                builder.append(" AND wrr.create_usr = " + "'" + loginUserSid + "'");
                builder.append(" AND wrr.read_status != " + "'" + WRReadStatus.HASREAD.name() + "'");
            } else if (TransReadStatus.WAIT_READ.name().equals(transReadStatus)) {
                builder.append(" AND wrr.create_usr = " + "'" + loginUserSid + "'");
                builder.append(" AND wrr.read_status = " + "'" + WRReadStatus.WAIT_READ.name() + "'");
            } else if (TransReadStatus.NO_READ.name().equals(transReadStatus)) {
                builder.append(" AND wrr.create_usr = " + "'" + loginUserSid + "'");
                builder.append(" AND wrr.read_status != " + "'" + WRReadStatus.HASREAD.name() + "'");
                builder.append(" AND wrr.read_status != " + "'" + WRReadStatus.WAIT_READ.name() + "'");
            } else if (TransReadStatus.READED.name().equals(transReadStatus)) {
                builder.append(" AND wrr.read_status = " + "'" + WRReadStatus.HASREAD.name() + "'");
            } else {
                log.error("傳入參數有問題,transReadStatus[" + transReadStatus + "]比對不到列舉[TransReadStatus]");
            }
        }
        if (receiveOrgs != null && !receiveOrgs.isEmpty()) {
            builder.append(" AND wi.receive_dep in (:receiveOrgs)");
            parameters.put("receiveOrgs", receiveOrgs);
        }
        if (startCreateTime != null) {
            String startDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), startCreateTime);
            builder.append(" AND wi.create_dt >= :startCreateTime ");
            parameters.put("startCreateTime", startDateStr + " 00:00:00");
        }
        if (endCreateTime != null) {
            String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), endCreateTime);
            builder.append(" AND wi.create_dt <= :endCreateTime ");
            parameters.put("endCreateTime", endDateStr + " 23:59:59");
        }
        List<String> wr_sids = Lists.newArrayList();
        try {
            @SuppressWarnings("rawtypes")
            List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
            for (int i = 0; i < result.size(); i++) {
                String wr_sid = (String) result.get(i);
                if (!wr_sids.contains(wr_sid)) {
                    wr_sids.add(wr_sid);
                }
            }
            return wr_sids.size();
        } catch (Exception e) {
            log.error("getReceiveDepCount", e);
        }
        return 0;
    }

    /**
     * 取得部門轉發報表List
     *
     * @param tagID           介面挑選標籤ID
     * @param transReadStatus 介面挑選已未讀ID
     * @param sendOrgs        介面挑選寄發單位Sid List
     * @param sendName        寄發者
     * @param receiveOrgs     寄發至 Sid List (若未挑選,會直接回傳空List)
     * @param title           標題
     * @param content         內容
     * @param searchText      模糊搜尋
     * @param startCreateTime 工作報告異動起始時間
     * @param endCreateTime   工作報告異動結束時間
     * @param loginUserSid    登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     * @param wr_Id           工作報告Sid
     * @return
     */
    public List<WRTransReceviceDepWorkReportVO> getReceiveDep(String tagID, String transReadStatus, List<Integer> sendOrgs, String sendName, List<Integer> receiveOrgs, String title, String content, String searchText, Date startCreateTime, Date endCreateTime, Integer loginUserSid,
                                                              Integer loginUserDepSid, String wr_Id) {
        if (receiveOrgs == null || receiveOrgs.isEmpty()) {
            return Lists.newArrayList();
        }
        String trace_source_type = "WR";
        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        //查詢轉寄部門
        builder.append(" (SELECT COUNT(wiDep.inbox_sid) as transDepCount FROM wr_inbox wiDep WHERE wiDep.status = 0"
                + " AND wiDep.wr_sid = wr.wr_sid"
                + " AND wiDep.forward_type = '" + ForwardType.DEP.getKey() + "'"
                // + " AND wiDep.receive_dep = " + loginUserDepSid 
                + " )");
        builder.append(",");
        //查詢轉寄個人
        builder.append(" (SELECT COUNT(wiPerson.inbox_sid) as transPersonCount FROM wr_inbox wiPerson WHERE wiPerson.status = 0"
                + " AND wiPerson.wr_sid = wr.wr_sid"
                + " AND wiPerson.forward_type = '" + ForwardType.PERSON.getKey() + "'"
                // + " AND wiPerson.receive = " + loginUserSid
                + " )");
        builder.append(",");
        //查詢追蹤個人       
        builder.append(" (SELECT COUNT(wtp.tracesid) as personTraceCount FROM  wr_trace_info wtp WHERE wtp.status = 0 "
                + " AND wtp.trace_source_sid = wr.wr_sid "
                + " AND wtp.trace_source_type = '" + trace_source_type + "'"
                + " AND wtp.trace_status in ('" + TraceStatus.UN_FINISHED.name() + "')"
                + " AND wtp.trace_type in ('" + TraceType.PERSONAL.name() + "','" + TraceType.OTHER_PERSON.name() + "')"
                + " AND wtp.trace_usr = " + loginUserSid + ")");
        builder.append(",");
        //查詢追蹤部門
        builder.append(" (SELECT COUNT(wtp.tracesid) as depTraceCount FROM  wr_trace_info wtp WHERE wtp.status = 0 "
                + " AND wtp.trace_source_sid = wr.wr_sid "
                + " AND wtp.trace_source_type = '" + trace_source_type + "'"
                + " AND wtp.trace_status in ('" + TraceStatus.UN_FINISHED.name() + "')"
                + " AND wtp.trace_type in ('" + TraceType.DEPARTMENT.name() + "')"
                + " AND wtp.trace_dep = " + loginUserDepSid + ")");
        builder.append(",");
        builder.append("  wi.sender_dep,wi.sender,wi.create_dt as senDate,wi.receive_dep,"
                + "wr.wr_tag_sid,wr.theme,wt.updateDate,"
                + "wr.wr_sid,wr.wr_no FROM wr_inbox wi ");
        //關連工作報告主表
        builder.append(" INNER JOIN  wr_master wr ON wi.wr_sid = wr.wr_sid  AND wi.status = 0 ");
        builder.append(" INNER JOIN (SELECT wr_sid, wr_no, max(create_dt) as updateDate,min(create_dt) as createDate FROM wr_trace  GROUP BY wr_sid, wr_no) wt ON wr.wr_sid=wt.wr_sid AND wr.wr_no=wt.wr_no");
        builder.append(" WHERE 1=1  ");
        builder.append(" AND wi.forward_type = '" + ForwardType.DEP.getKey() + "' ");
        if (!Strings.isNullOrEmpty(tagID)) {
            builder.append(" AND wr.wr_tag_sid = :wr_tag_sid");
            parameters.put("wr_tag_sid", tagID);
        }
        if (sendOrgs != null && !sendOrgs.isEmpty()) {
            builder.append(" AND wi.sender_dep in (:sendOrgs)");
            parameters.put("sendOrgs", sendOrgs);
        }
        if (!Strings.isNullOrEmpty(sendName.trim())) {
            List<User> likeUserList = workSettingUserManager.likeUserByUsername(sendName.trim());
            if (CollectionUtils.isEmpty(likeUserList)) {
                return Lists.newArrayList();
            } else {
                builder.append(" AND wr.create_usr in (:userSids)");
                parameters.put("userSids", likeUserList.stream().map(User::getSid).collect(Collectors.toList()));
            }
        }
        if (receiveOrgs != null && !receiveOrgs.isEmpty()) {
            builder.append(" AND wi.receive_dep in (:receiveOrgs)");
            parameters.put("receiveOrgs", receiveOrgs);
        }

        if (!Strings.isNullOrEmpty(title)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(title) + "%";
            builder.append(" AND wr.theme LIKE :theme");
            parameters.put("theme", textNo);
        }
        if (!Strings.isNullOrEmpty(content)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(content) + "%";
            builder.append(" AND wr.content LIKE :content");
            parameters.put("content", textNo);
        }

        if (startCreateTime != null) {
            String startDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), startCreateTime);
            builder.append(" AND wi.create_dt >= :startCreateTime ");
            parameters.put("startCreateTime", startDateStr + " 00:00:00");
        }
        if (endCreateTime != null) {
            String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), endCreateTime);
            builder.append(" AND wi.create_dt <= :endCreateTime ");
            parameters.put("endCreateTime", endDateStr + " 23:59:59");
        }

        if (!Strings.isNullOrEmpty(searchText)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(searchText) + "%";
            builder.append(" AND (wr.theme LIKE :searchText OR wr.content LIKE :searchText OR wr.memo LIKE :searchText OR EXISTS"
                    + "(SELECT * FROM wr_trace wtrc "
                    + "WHERE wtrc.status = 0 AND wtrc.wr_trace_type = '" + WRTraceType.REPLY.name() + "' AND wtrc.wr_sid = wr.wr_sid AND wtrc.wr_trace_content LIKE :searchText ) )");
            parameters.put("searchText", textNo);
        }
        if (!Strings.isNullOrEmpty(wr_Id)) {
            builder.append(" AND wr.wr_sid = :wr_sid");
            parameters.put("wr_sid", wr_Id);
        }
        builder.append(" ORDER BY wi.create_dt DESC");
        List<WRTransReceviceDepWorkReportVO> wrTransReceiveDepWorkReportVOs = Lists.newArrayList();
        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        for (int i = 0; i < result.size(); i++) {
            try {
                Object[] record = (Object[]) result.get(i);
                Integer transDepCount = ((BigInteger) record[0]).intValue();
                Integer transPersonCount = ((BigInteger) record[1]).intValue();
                Integer personTraceCount = ((BigInteger) record[2]).intValue();
                Integer depTraceCount = ((BigInteger) record[3]).intValue();
                Integer sender_dep = (Integer) record[4];
                Integer sender = (Integer) record[5];
                Date create_dt = (Date) record[6];
                Integer receive_dep = (Integer) record[7];
                String wr_tag_sid = (String) record[8];
                String theme = (String) record[9];
                Date update_dt = (Date) record[10];
                String wr_sid = (String) record[11];
                String wr_no = (String) record[12];
                WrReadRecord readRecord = wrReadRecordManager.getReadRecord(loginUserSid.toString(), wr_sid);
                WRReadStatus wRReadStatus = WRReadStatus.valueOf(readRecord.getReadStatus());
                boolean toTrace = false;
                if (personTraceCount > 0 || depTraceCount > 0) {
                    toTrace = true;
                }
                boolean toForwardDep = false;
                if (transDepCount > 0) {
                    toForwardDep = true;
                }
                boolean toForwardMember = false;
                if (transPersonCount > 0) {
                    toForwardMember = true;
                }
                //轉寄類功能尚未製作
                WRTransReceviceDepWorkReportVO wRTransReceviceDepWorkReportVO
                        = new WRTransReceviceDepWorkReportVO(wr_sid, wr_no, toForwardDep, toForwardMember, toTrace,
                        sender_dep, sender, create_dt, receive_dep, wr_tag_sid, theme, update_dt, wRReadStatus);
                if (!wrTransReceiveDepWorkReportVOs.contains(wRTransReceviceDepWorkReportVO)) {
                    wrTransReceiveDepWorkReportVOs.add(wRTransReceviceDepWorkReportVO);
                }
            } catch (Exception e) {
                log.error("getWorkReportListByTraceSearch Error", e);
            }
        }
        return wrTransReceiveDepWorkReportVOs;
    }

    /**
     * 取得轉寄細項 By 工作報告Sid 及 轉發類型及登入者Sid
     *
     * @param wrSid        工作報告Sid
     * @param forwardType  轉發類型
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<WRInbox> getWorkInboxByWrSidAndForwardTypeAndReceiveSid(String wrSid, ForwardType forwardType, Integer loginUserSid) {
        return workInboxRepository.findByInboxByWrSidAndForwardTypeAndReceiveSid(wrSid, forwardType, loginUserSid);
    }

    /**
     * 取得轉寄細項 By 轉寄GroupSid
     *
     * @param inbox_group_sid
     * @return
     */
    public List<WRInbox> getWorkInboxByInboxGroupSid(String inbox_group_sid) {
        return workInboxRepository.findByInboxByInboxGroupSid(inbox_group_sid);
    }

    /**
     * 建立轉寄細項
     *
     * @param workInbox    轉寄細項物件
     * @param loginUserSid 建立者Sid
     * @return
     */
    public WRInbox createWorkInbox(WRInbox workInbox, Integer loginUserSid) {
        workInbox.setStatus(Activation.ACTIVE);
        workInbox.setCreate_usr(loginUserSid);
        workInbox.setCreate_dt(new Date());
        workInbox.setUpdate_dt(new Date());
        return workInboxRepository.save(workInbox);
    }

    /**
     * 將轉寄細項更新為已讀 By 工作報告Sid 及 登入者Sid 及 登入者部門Sid
     *
     * @param wr_Sid          工作報告Sid
     * @param loginUserSid    登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     */
    @Transactional(rollbackFor = Exception.class)
    public void readWorkInbox(String wr_Sid, Integer loginUserSid, Integer loginUserDepSid) {
        workInboxRepository.updateReadRecordStatus(ReadRecordStatus.READ, new Date(), wr_Sid,
                ReadRecordStatus.UNREAD, loginUserSid, loginUserDepSid, ForwardType.DEP, ForwardType.PERSON);
    }

    /**
     * 更新轉寄細項
     *
     * @param workInbox    轉寄細項物件
     * @param loginUserSid 更新者Sid
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public WRInbox updateWorkInbox(WRInbox workInbox, Integer loginUserSid) {
        workInbox.setUpdate_usr(loginUserSid);
        workInbox.setUpdate_dt(new Date());
        return workInboxRepository.save(workInbox);
    }
}
