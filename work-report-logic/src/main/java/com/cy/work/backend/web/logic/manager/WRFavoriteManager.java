/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.logic.query.WRMasterQueryService;
import com.cy.work.backend.web.logic.utils.ReqularPattenUtils;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRFavoriteWorkReportVO;
import com.cy.work.backend.web.repository.WRFavoriteRepository;
import com.cy.work.backend.web.vo.WRFavorite;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 工作報告收藏
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRFavoriteManager implements Serializable {


    private static final long serialVersionUID = 1579123707106332036L;
    @Autowired
    private WRFavoriteRepository wRFavoriteRepository;
    @Autowired
    private ReqularPattenUtils reqularUtils;
    @Autowired
    private WRMasterQueryService wRMasterQueryService;

    public List<WRFavorite> findByWRSid(String wrSid) {
        return wRFavoriteRepository.findByWRSid(wrSid);
    }

    public List<WRFavorite> findByUserSid(Integer create_usr) {
        return wRFavoriteRepository.findByUserSid(create_usr);
    }

    /**
     * 取得收藏資料 By 工作報告Sid及登入者Sid
     *
     * @param wr_Id
     * @param loginUserSid
     * @return
     */
    public WRFavorite getWRFavoriteByloginUserSidAndWRSid(String wr_Id, Integer loginUserSid) {
        List<WRFavorite> wRFavorites = wRFavoriteRepository.findByUserSidAndWRSid(wr_Id, loginUserSid);
        if (wRFavorites.size() > 0) {
            return wRFavorites.get(0);
        }
        return null;
    }

    /**
     * 取得收藏資料筆數 By 收藏時間區間及登入者Sid
     *
     * @param loginUserSid 登入者Sid
     * @param start 收藏起始時間
     * @param end 收藏結束時間
     * @return
     */
    public Integer findFavoriteCountByUserSidAndUpdateDt(Integer loginUserSid, Date start, Date end) {
        try {
            String startDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDash.getValue(), start);
            startDateStr = startDateStr + " 00:00:00";
            Date searchStartDate = ToolsDate.transStringToDate(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), startDateStr);

            String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDash.getValue(), end);
            endDateStr = endDateStr + " 23:59:59";
            Date searchEndDate = ToolsDate.transStringToDate(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), endDateStr);
            return wRFavoriteRepository.findFavoriteCountByUserSidAndUpdateDt(searchStartDate, searchEndDate, loginUserSid);
        } catch (Exception e) {
            log.error("findFavoriteCountByUserSidAndUpdateDt", e);
        }
        return 0;
    }

    /**
     * 取得收藏工作報告資料 By 登入者及收藏時間區間及主題字串及工作報告Sid
     *
     * @param loginUserSid 登入者
     * @param start 收藏起始時間
     * @param end 收藏結束時間
     * @param text 主題字串
     * @param wr_Id 工作報告Sid
     * @return
     */
    public List<WRFavoriteWorkReportVO> findFavoriteByUserSidAndUpdateDtAndText(Integer loginUserSid, Date start, Date end, String text, String wr_Id) {

        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        builder.append(" wf.favorite_sid,wr.wr_sid,wr.theme,wf.update_dt,wr.wr_no");
        builder.append(" FROM wr_favorite wf INNER JOIN wr_master wr ON wr.wr_sid = wf.wr_sid");
        builder.append(" WHERE wf.status = 0 ");
        builder.append(" AND wf.create_usr = :create_usr ");
        parameters.put("create_usr", loginUserSid);
        if (start != null) {
            String startDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), start);
            builder.append(" AND wf.update_dt >= :start_dt ");
            parameters.put("start_dt", startDateStr + " 00:00:00");
        }

        if (end != null) {
            String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), end);
            builder.append(" AND wf.update_dt <= :end_dt ");
            parameters.put("end_dt", endDateStr + " 23:59:59");
        }

        if (!Strings.isNullOrEmpty(text)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(text) + "%";
            builder.append(" AND wr.theme LIKE :theme");
            parameters.put("theme", textNo);
        }
        if (!Strings.isNullOrEmpty(wr_Id)) {
            builder.append(" AND wf.wr_sid = :wr_sid ");
            parameters.put("wr_sid", wr_Id);
        }
        builder.append(" ORDER BY  wf.update_dt DESC ");
        List<WRFavoriteWorkReportVO> wRFavoriteWorkReportVOs = Lists.newArrayList();
        @SuppressWarnings("rawtypes")
        List result = wRMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        for (int i = 0; i < result.size(); i++) {
            try {
                Object[] record = (Object[]) result.get(i);
                String favorite_sid = (String) record[0];
                String wr_sid = (String) record[1];
                String theme = (String) record[2];
                Date favoriteDate = null;
                if (record[3] != null) {
                    favoriteDate = (Date) record[3];
                }
                String wr_no = (String) record[4];
                WRFavoriteWorkReportVO wv = new WRFavoriteWorkReportVO(favorite_sid, wr_sid, theme, favoriteDate, wr_no);
                wRFavoriteWorkReportVOs.add(wv);
            } catch (Exception e) {
                log.error("findFavoriteByUserSidAndUpdateDtAndText", e);
            }
        }
        return wRFavoriteWorkReportVOs;
    }

    /**
     * 建立收藏資料
     *
     * @param wRFavorite 收藏物件
     * @param loginUserSid 建立者
     */
    public WRFavorite createWRFavorite(WRFavorite wRFavorite, Integer loginUserSid) {
        wRFavorite.setStatus(Activation.ACTIVE);
        wRFavorite.setCreate_dt(new Date());
        wRFavorite.setCreate_usr(loginUserSid);
        wRFavorite.setUpdate_dt(new Date());
        return wRFavoriteRepository.save(wRFavorite);
    }

    /**
     * 更新收藏資料
     *
     * @param wRFavorite 收藏物件
     * @param loginUserSid 更新者
     */
    public WRFavorite updateWRFavorite(WRFavorite wRFavorite, Integer loginUserSid) {
        wRFavorite.setUpdate_dt(new Date());
        wRFavorite.setUpdate_usr(loginUserSid);
        return wRFavoriteRepository.save(wRFavorite);
    }
}
