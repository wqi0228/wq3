/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.cache;

import com.cy.work.backend.web.repository.WrBasePersonAccessInfoRepository;
import com.cy.work.backend.web.vo.WrBasePersonAccessInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 可閱讀部門成員權限Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WrBasePersonAccessInfoCache implements Serializable {

    
    private static final long serialVersionUID = 6494736731942528057L;
    /** WrBaseDepAccessInfoRepository */
    @Autowired
    private WrBasePersonAccessInfoRepository wrBasePersonAccessInfoRepository;
    /** 控制啟動FLAG */
    private Boolean startController;
    /** Cache 物件 */
    private Map<String, WrBasePersonAccessInfo> wrBasePersonAccessInfoMap;

    public WrBasePersonAccessInfoCache() {
        startController = Boolean.TRUE;
        wrBasePersonAccessInfoMap = Maps.newConcurrentMap();
    }

    /** 初始化資料 */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            this.updateCache();
        }
    }

    /** 排程更新Cache資料 */
    @Scheduled(cron = "0 0/30 * * * ?")
    public void updateCache() {
        log.info("建立可閱讀部門成員權限Cache");
        try {
            Map<String, WrBasePersonAccessInfo> tempWrBaseDepAccessInfoMap = Maps.newConcurrentMap();
            wrBasePersonAccessInfoRepository.findAll().forEach(item -> {
                tempWrBaseDepAccessInfoMap.put(item.getSid(), item);
            });
            wrBasePersonAccessInfoMap = tempWrBaseDepAccessInfoMap;
        } catch (Exception e) {
            log.error("updateCache", e);
        }
        log.info("建立可閱讀部門成員權限Cache結束");
    }

    /** 更新Cache資料 */
    public void updateCacheBySid(WrBasePersonAccessInfo wrBasePersonAccessInfo) {
        wrBasePersonAccessInfoMap.put(wrBasePersonAccessInfo.getSid(), wrBasePersonAccessInfo);
    }

    /**
     * 取得可閱讀部門成員權限 By Sid
     *
     * @param sid 可閱讀部門成員權限Sid
     * @return
     */
    public WrBasePersonAccessInfo findBySid(String sid) {
        return wrBasePersonAccessInfoMap.get(sid);
    }

    /**
     * 取得可閱讀部門成員權限 By 使用者Sid
     *
     * @param userSid 使用者Sid
     * @return
     */
    public WrBasePersonAccessInfo getByUserSid(Integer userSid) {
        for (WrBasePersonAccessInfo wi : Lists.newArrayList(wrBasePersonAccessInfoMap.values())) {
            if (wi.getLoginUserSid().equals(userSid)) {
                return wi;
            }
        }
        return null;
    }

}
