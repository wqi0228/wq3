/**
 * 
 */
package com.cy.work.backend.web.logic.manager.setting6.vo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */

@NoArgsConstructor
public class Setting6VO implements Serializable {


    private static final long serialVersionUID = 6650231071280933099L;
    @Getter
    @Setter
    private Integer depSid;
    @Getter
    @Setter
    private BigInteger cnt;
    @Getter
    @Setter
    private Date lastCreateDate;
    @Getter
    @Setter
    private Integer orgOrder = Integer.MAX_VALUE;

    @Getter
    @Setter
    private String compName;
    @Getter
    @Setter
    private String depName;
    @Getter
    @Setter
    private String lastCreateDateStr;
    
}
