/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.cache;

import com.cy.work.backend.web.repository.WRHomepageFavoriteRepository;
import com.cy.work.backend.web.vo.WRHomepageFavorite;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 報表快選區Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRHomepageFavoriteCache implements Serializable {


    private static final long serialVersionUID = -3262518661413654250L;
    /** WRHomepageFavoriteRepository */
    @Autowired
    private WRHomepageFavoriteRepository wRHomepageFavoriteRepository;
    /** 控制啟動FLAG */
    private Boolean startController;
    /** Cache 物件 */
    private Map<Integer, WRHomepageFavorite> wRHomepageFavoriteMap;

    public WRHomepageFavoriteCache() {
        startController = Boolean.TRUE;
        wRHomepageFavoriteMap = Maps.newConcurrentMap();
    }

    /** 初始化資料 */
    public void start() {
        if (Boolean.TRUE.equals(startController)) {
            startController = Boolean.FALSE;
            this.updateCache();
        }else{
            // false 跟 null 都會進來這邊
        }
    }

    /** 排程更新Cache資料 */
    @Scheduled(cron = "0 0/30 * * * ?")
    public void updateCache() {
        log.info("建立首頁收藏資訊Cache");
        try {
            Map<Integer, WRHomepageFavorite> tempWRHomepageFavoriteMap = Maps.newConcurrentMap();
            wRHomepageFavoriteRepository.findAll().forEach(item -> {
                tempWRHomepageFavoriteMap.put(item.getUsersid(), item);
            });
            wRHomepageFavoriteMap = tempWRHomepageFavoriteMap;
        } catch (Exception e) {
            log.error("updateCache", e);
        }
        log.info("建立首頁收藏資訊Cache結束");
    }

    /** 更新Cache資料 */
    public void updateCacheByUserSid(WRHomepageFavorite wRHomepageFavorite) {
        wRHomepageFavoriteMap.put(wRHomepageFavorite.getUsersid(), wRHomepageFavorite);
    }

    /**
     * 取得報表快選區資料 By 使用者Sid
     *
     * @param userSid 使用者Sid
     * @return
     */
    public WRHomepageFavorite findByUserSid(Integer userSid) {
        return wRHomepageFavoriteMap.get(userSid);
    }

}
