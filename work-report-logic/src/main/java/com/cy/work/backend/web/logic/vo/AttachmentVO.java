/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.vo;

import com.cy.work.common.utils.WkDateUtils;
import com.cy.commons.enums.Activation;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class AttachmentVO implements Serializable, com.cy.work.common.vo.basic.BasicAttachment<String> {


    private static final long serialVersionUID = 6895915979155169602L;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.attSid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AttachmentVO other = (AttachmentVO) obj;
        if (!Objects.equals(this.attSid, other.attSid)) {
            return false;
        }
        return true;
    }

    @Getter
    @Setter
    private String attSid;
    @Getter
    @Setter
    private String attName;
    @Getter
    @Setter
    private String attDesc;
    @Getter
    @Setter
    private String attPath;
    @Getter
    @Setter
    private boolean checked;
    /** Maintain Mode User */
    @Getter
    @Setter
    private boolean editMode = false;
     @Getter
    @Setter
    private boolean showEditBtn = false;
    /** Maintain Mode User */
    @Getter
    @Setter
    private String createTime;
    @Getter
    @Setter
    private String paramCreateTime;
    /** Maintain Mode User */
    @Getter
    @Setter
    private String userInfo;
    @Getter
    @Setter
    private String createUserSId;
    @Getter
    @Setter
    private Activation status;

    public AttachmentVO(String attSid, String attName, String attDesc, String attPath, boolean checked,String createUserSId) {
        this.attSid = attSid;
        this.attName = attName;
        this.attDesc = attDesc;
        this.attPath = attPath;
        this.checked = checked;
        this.createUserSId = createUserSId;
    }
    
    @Override
    public String getSid() {
        return attSid;
    }

    @Override
    public void setSid(String sid) {
        this.attSid = sid;
    }

    @Override
    public String getDesc() {
        return attDesc;
    }

    @Override
    public void setDesc(String desc) {
        this.attDesc = desc;
    }

    @Override
    public Boolean getKeyChecked() {
        return checked;
    }

    @Override
    public void setKeyChecked(Boolean checked) {
        this.checked = checked;
    }

    @Override
    public Boolean getKeyCheckEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setKeyCheckEdit(Boolean checkEdit) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getUploadDept() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setUploadDept(Integer uploadDept) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Date getCreatedDate() {
        try {
            return WkDateUtils.timeAnalysis(paramCreateTime);
        } catch (Exception e) {
            return new Date();
        }
    }

    @Override
    public void setCreatedDate(Date createdDate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getCreatedUser() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCreatedUser(Integer createdUser) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getUpdatedUser() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setUpdatedUser(Integer updatedUser) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Date getUpdatedDate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setUpdatedDate(Date updatedDate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getFileName() {
        return attName;
    }

    @Override
    public void setFileName(String fileName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
