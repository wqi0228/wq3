/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.beust.jcommander.internal.Sets;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.cache.WrBasePersonAccessInfoCache;
import com.cy.work.backend.web.repository.WrBasePersonAccessInfoRepository;
import com.cy.work.backend.web.vo.WrBasePersonAccessInfo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 基礎單位成員可閱設定
 *
 * @author brain0925_liao
 */
@Component
public class WrBasePersonAccessInfoManager implements Serializable {

    private static final long serialVersionUID = 6127845748702068421L;
    /** 基礎單位成員可閱設定Cache */
    @Autowired
    private WrBasePersonAccessInfoCache wrBasePersonAccessInfoCache;
    /** WrBasePersonAccessInfoRepository */
    @Autowired
    private WrBasePersonAccessInfoRepository wrBasePersonAccessInfoRepository;
    /** WorkSettingUserManager */
    @Autowired
    private WorkSettingUserManager workSettingUserManager;

    /**
     * 取得基礎單位成員可閱設定
     *
     * @param userSid 使用者Sid
     * @return
     */
    public WrBasePersonAccessInfo getByUserSid(Integer userSid) {
        return wrBasePersonAccessInfoCache.getByUserSid(userSid);
    }

    /**
     * 取得限制基礎單位成員可閱人員
     *
     * @param userSid 使用者Sid
     * @return
     */
    public List<User> getLimitBaseAccessViewUser(Integer userSid) {
        WrBasePersonAccessInfo wrBasePersonAccessInfo = getByUserSid(userSid);
        if (wrBasePersonAccessInfo == null || wrBasePersonAccessInfo.getLimitBaseAccessViewPerson() == null || wrBasePersonAccessInfo.getLimitBaseAccessViewPerson().getUserTos() == null) {
            return null;
        }
        List<User> limitBaseAccessViewUsers = Lists.newArrayList();
        wrBasePersonAccessInfo.getLimitBaseAccessViewPerson().getUserTos().forEach(item -> {
            limitBaseAccessViewUsers.add(workSettingUserManager.findBySID(item.getSid()));
        });
        return limitBaseAccessViewUsers;
    }

//    /**
//     * 取得限制基礎單位成員可閱人員
//     * @param userSid
//     * @return
//     */
//    public Set<Integer> getLimitBaseAccessViewPersonSids(Integer userSid) {
//        WrBasePersonAccessInfo wrBasePersonAccessInfo = getByUserSid(userSid);
//        if (wrBasePersonAccessInfo == null) {
//            return Sets.newHashSet();
//        }
//        return wrBasePersonAccessInfo.getLimitBaseAccessViewPersonSids();
//    }

    /**
     * 取得增加基礎單位成員可閱人員
     *
     * @param userSid 使用者Sid
     * @return
     */
    public List<User> getOtherBaseAccessViewUser(Integer userSid) {
        WrBasePersonAccessInfo wrBasePersonAccessInfo = getByUserSid(userSid);
        if (wrBasePersonAccessInfo == null || wrBasePersonAccessInfo.getOtherBaseAccessViewPerson() == null || wrBasePersonAccessInfo.getOtherBaseAccessViewPerson().getUserTos() == null) {
            return Lists.newArrayList();
        }
        List<User> otherBaseAccessViewUsers = Lists.newArrayList();
        wrBasePersonAccessInfo.getOtherBaseAccessViewPerson().getUserTos().forEach(item -> {
            otherBaseAccessViewUsers.add(workSettingUserManager.findBySID(item.getSid()));
        });
        return otherBaseAccessViewUsers;
    }

    /**
     * 建立基礎單位成員可閱設定
     *
     * @param wrBasePersonAccessInfo 基礎單位成員可閱設定物件
     * @param loginUserSid           登入者Sid
     */
    public void createWrBasePersonAccessInfo(WrBasePersonAccessInfo wrBasePersonAccessInfo, Integer loginUserSid) {
        wrBasePersonAccessInfo.setCreate_dt(new Date());
        wrBasePersonAccessInfo.setCreate_usr_sid(loginUserSid);
        wrBasePersonAccessInfo = wrBasePersonAccessInfoRepository.save(wrBasePersonAccessInfo);
        wrBasePersonAccessInfoCache.updateCacheBySid(wrBasePersonAccessInfo);
    }

    /**
     * 更新基礎單位成員可閱設定
     *
     * @param wrBasePersonAccessInfo 基礎單位成員可閱設定物件
     * @param loginUserSid           更新者Sid
     */
    public void updateWrBasePersonAccessInfo(WrBasePersonAccessInfo wrBasePersonAccessInfo, Integer loginUserSid) {
        wrBasePersonAccessInfo.setUpdate_dt(new Date());
        wrBasePersonAccessInfo.setUpdate_usr_sid(loginUserSid);
        wrBasePersonAccessInfo = wrBasePersonAccessInfoRepository.save(wrBasePersonAccessInfo);
        wrBasePersonAccessInfoCache.updateCacheBySid(wrBasePersonAccessInfo);
    }

}
