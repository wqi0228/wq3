/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.work.backend.web.vo.WRFunItem;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.cy.work.backend.web.repository.WRFunItemRepository;

/**
 * 選單細項
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRFunItemManager implements Serializable {


    private static final long serialVersionUID = -2021985368959346325L;
    @Autowired
    private WRFunItemRepository wrFunItemRepository;

    /** 取得選單細項 */
    public List<WRFunItem> getWRFunItemByCategoryModel() {
        try {
            String category_model = "WR";
            return wrFunItemRepository.getWRFunItemByCategoryModel(category_model);
        } catch (Exception e) {
            log.error("getWRFunItemByCategoryModel Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }
}
