/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.work.backend.web.vo.WRFunItemGroup;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.cy.work.backend.web.repository.WRFunItemGroupRepository;

/**
 * 選單大項
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRFunItemGroupManager implements Serializable{


    private static final long serialVersionUID = 8164937349157104801L;
    @Autowired
    private WRFunItemGroupRepository wrFunItemGroupRepository;
    /**取得選單大項*/
    public List<WRFunItemGroup> getWRFunItemGroupByCategoryModel() {
        try {
            String category_model = "WR";
            return wrFunItemGroupRepository.getWRFunItemGroupByCategoryModel(category_model);
        } catch (Exception e) {
            log.error("getWRFunItemGroupByCategoryModel Error : " + e.toString(),e);
            return Lists.newArrayList();
        }
    }
}
