/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.vo;

import com.cy.work.backend.web.vo.enums.WREditType;
import com.cy.work.backend.web.vo.enums.WRStatus;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;

/**
 * 最後可編輯時間修改報表轉出物件
 *
 * @author brain0925_liao
 */
public class WRMasterLastModifyVO implements Serializable {

    private static final long serialVersionUID = -1303480526420483527L;

    public WRMasterLastModifyVO(String wr_sid, Integer create_usr, Date create_dt,
            Integer dep_sid, String theme, String wr_no,
            Date last_modify_dt, String last_modify_memo, String last_modify_memo_css,
            WRStatus wr_status, WREditType edit_type) {
        this.wr_sid = wr_sid;
        this.create_usr = create_usr;
        this.create_dt = create_dt;
        this.dep_sid = dep_sid;
        this.theme = theme;
        this.wr_no = wr_no;
        this.last_modify_dt = last_modify_dt;
        this.last_modify_memo = last_modify_memo;
        this.last_modify_memo_css = last_modify_memo_css;
        this.wr_status = wr_status;
        this.edit_type = edit_type;
    }

    /** 工作報告Sid */
    @Getter
    private final String wr_sid;
    /** 工作報告建立者Sid */
    @Getter
    private final Integer create_usr;
    /** 工作報告建立時間 */
    @Getter
    private final Date create_dt;
    /** 工作報告提交部門Sid */
    @Getter
    private final Integer dep_sid;
    /** 主題 */
    @Getter
    private final String theme;
    /** 工作報告單號 */
    @Getter
    private final String wr_no;
    /** 最後可編輯時間 */
    @Getter
    private final Date last_modify_dt;
    @Getter
    private final String last_modify_memo;
    /** 最後可編輯時間備註 */
    @Getter
    private final String last_modify_memo_css;
    @Getter
    private final WRStatus wr_status;
    @Getter
    private final WREditType edit_type;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.wr_sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRMasterLastModifyVO other = (WRMasterLastModifyVO) obj;
        if (!Objects.equals(this.wr_sid, other.wr_sid)) {
            return false;
        }
        return true;
    }

}
