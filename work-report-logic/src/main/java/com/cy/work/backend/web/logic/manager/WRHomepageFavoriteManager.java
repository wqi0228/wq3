/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.work.backend.web.repository.WRHomepageFavoriteRepository;
import com.cy.work.backend.web.vo.WRHomepageFavorite;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 報表快選區
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRHomepageFavoriteManager implements Serializable {

    
    private static final long serialVersionUID = -3445061480735159730L;
    @Autowired
    private WRHomepageFavoriteRepository wRHomepageFavoriteRepository;

    /**
     * 取得報表快選區資料 By 登入者Sid
     *
     * @param loginUserSid 登入者Sid
     * @return
     */
    public WRHomepageFavorite getWRHomepageFavoriteByUserSid(Integer loginUserSid) {
        List<WRHomepageFavorite> wRHomepageFavorites = wRHomepageFavoriteRepository.findByUserSid(loginUserSid);
        if (wRHomepageFavorites != null && !wRHomepageFavorites.isEmpty()) {
            return wRHomepageFavorites.get(0);
        }
        return null;
    }

    /**
     * 建立報表快選區資料，已有資料就更新報表快選區資料
     * @param wRHomepageFavorite 報表快選區物件
     * @param createUserSid 建立者
     * @return 
     */
    public WRHomepageFavorite createOrUpdate(WRHomepageFavorite wRHomepageFavorite, Integer createUserSid) {
        try {
            wRHomepageFavorite.setUsersid(createUserSid);
            wRHomepageFavorite.setUpdate_dt(new Date());
            WRHomepageFavorite wr = wRHomepageFavoriteRepository.save(wRHomepageFavorite);
            return wr;
        } catch (Exception e) {
            log.error("save Error : " + e.toString(), e);
        }
        return null;
    }
}
