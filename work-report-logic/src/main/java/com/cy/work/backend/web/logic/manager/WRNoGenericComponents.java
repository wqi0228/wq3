/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.manager;

import com.cy.work.backend.web.vo.enums.WRStatus;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 流水號建立邏輯元件
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRNoGenericComponents implements Serializable {

    
    private static final long serialVersionUID = 3195820203172481087L;
    /** 流水號長度 */
    private final Integer noLength = 3;
    /** 流水號容器大小 */
    private final Integer treeSize = 1000;
    @Autowired
    private WRMasterManager wRMasterManager;
    /** 工作報告流水號存放 - 已提 */
    private final Map<Integer, TreeSet<String>> treeNoMap;

    private final Map<Integer, TreeSet<String>> tempTreeNoMap;
    /** 執行清除序號任務FLAG，並重新生成新序號 */
    private Boolean produceTime;
    /** 控制啟動FLAG */
    private Boolean startController;

    public WRNoGenericComponents() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        treeNoMap = Maps.newConcurrentMap();
        tempTreeNoMap = Maps.newConcurrentMap();
    }

    /** 啟動 */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.createNoTreeSet();
            }
        }
    }

    private TreeSet<String> createTree(Integer companySid) {
        TreeSet<String> treeNo = new TreeSet<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        StringBuilder sb = new StringBuilder();
        sb.append("WR").append(sdf.format(new Date()));
        String title = sb.toString();
        Integer beginValue = this.findBeginValue(companySid);
        for (int i = beginValue; i < treeSize; i++) {
            String num = title + Strings.padStart(String.valueOf(i), noLength, '0');
            treeNo.add(num);
        }
        log.info("本次工作報告啟始流水號碼   : " + title + Strings.padStart(String.valueOf(beginValue), noLength, '0'));
        log.info("本次工作報告啟始流水號容量 : " + treeNo.size());
        log.info("工作報告單號流beginTempValue水資料建立完成...");
        return treeNo;
    }

    private TreeSet<String> createTempTree(Integer companySid) {
        TreeSet<String> tempTreeNo = new TreeSet<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String tempTitle = sdf.format(new Date());
        Integer beginTempValue = this.findBeginTempValue(companySid);
        for (int i = beginTempValue; i < treeSize; i++) {
            String num = tempTitle + Strings.padStart(String.valueOf(i), noLength, '0');
            tempTreeNo.add(num);
        }
        log.info("本次工作報告草稿啟始流水號碼   : " + tempTitle + Strings.padStart(String.valueOf(beginTempValue), noLength, '0'));
        log.info("本次工作報告草稿啟始流水號容量 : " + tempTreeNo.size());
        log.info("工作報告單號草稿流水資料建立完成...");
        return tempTreeNo;
    }

    /**
     * 清除單號資料，並重建新水流單號
     *
     * cron解釋    <BR/>
     * 固定六個值  <BR/>
     * 秒 分 時 日 月 週
     * 日與週互斥，其中之一必須為 ? <BR/>
     *
     * http://howtodoinjava.com/2013/04/23/4-ways-to-schedule-tasks-in-spring-3-scheduled-example/
     */
    @Scheduled(cron = "0 0 0 ? * *") // 每天0點0分時觸發 秒 分 時 日 月 週
    //@Scheduled(cron = "0 0-59 0-23 * * ?") // 每分鐘觸發 
    private void createNoTreeSet() {
        produceTime = Boolean.TRUE;
        log.info("建立工作報告單號流水資料...");
        try {
            log.info("建立工作報告單號流水資料...");
            Set<Integer> treeNoKeys = treeNoMap.keySet();
            treeNoMap.clear();
            Set<Integer> tempTreeNoKeys = tempTreeNoMap.keySet();
            tempTreeNoMap.clear();
            treeNoKeys.forEach(item -> {
                treeNoMap.put(item, createTree(item));
            });
            tempTreeNoKeys.forEach(item -> {
                tempTreeNoMap.put(item, createTempTree(item));
            });
        } catch (Exception e) {
            log.error("createNoTreeSet ERROR", e);
        }
        produceTime = Boolean.FALSE;
    }

    private Integer findBeginTempValue(Integer companySid) {
        Calendar start = Calendar.getInstance();
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);

        Calendar end = Calendar.getInstance();
        end.set(Calendar.HOUR_OF_DAY, 23);
        end.set(Calendar.MINUTE, 59);
        end.set(Calendar.SECOND, 59);
        //從資料庫找出本日工作報告筆數後+1
        Integer startValue = wRMasterManager.findTodayWRMasterTotalSize(start.getTime(), end.getTime(), WRStatus.DRAFT, companySid);
        if (startValue == null || startValue == 0) {
            return 1;
        }

        String lastNo = wRMasterManager.findTodayLastWRMaster(start.getTime(), end.getTime(), WRStatus.DRAFT, companySid);
        String lastNum = lastNo.substring(lastNo.length() - noLength, lastNo.length());
        return Integer.parseInt(lastNum) + 1;
    }

    /**
     * 這個值要從資料庫中找
     *
     * @return
     */
    private Integer findBeginValue(Integer companySid) {
        Calendar start = Calendar.getInstance();
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);

        Calendar end = Calendar.getInstance();
        end.set(Calendar.HOUR_OF_DAY, 23);
        end.set(Calendar.MINUTE, 59);
        end.set(Calendar.SECOND, 59);
        //從資料庫找出本日工作報告筆數後+1
        Integer startValue = wRMasterManager.findTodayWRMasterTotalSize(start.getTime(), end.getTime(), WRStatus.COMMIT, companySid);
        if (startValue == null || startValue == 0) {
            return 1;
        }

        String lastNo = wRMasterManager.findTodayLastWRMaster(start.getTime(), end.getTime(), WRStatus.COMMIT, companySid);
        String lastNum = lastNo.substring(lastNo.length() - noLength, lastNo.length());
        return Integer.parseInt(lastNum) + 1;
    }

    /**
     * 取得日期流水號 <BR/>
     * 格式:TRyyyyMMdd###
     *
     * @return
     * @throws java.lang.InterruptedException
     */
    public synchronized String getSequenceNum(Integer companySid) throws InterruptedException {

        while (produceTime) {
            synchronized (this) {
                log.info("正在執行工作報告單號資料重建任務...");
                if (!produceTime) {
                    break;
                }
                this.wait(500);
            }
        }
        if (treeNoMap.get(companySid) == null) {
            treeNoMap.put(companySid, this.createTree(companySid));
        }
        TreeSet<String> treeNo = treeNoMap.get(companySid);
        if (treeNo.isEmpty()) {
            String uuid = UUID.randomUUID().toString();
            log.error("本日工作報告流水號已用完！！產生UUID碼取代:" + uuid);
            return uuid;
        }
        return treeNo.pollFirst();
    }

    /**
     * 取得日期流水號 <BR/>
     * 格式:TRyyyyMMdd###
     *
     * @return
     * @throws java.lang.InterruptedException
     */
    public synchronized String getTempSequenceNum(Integer companySid) throws InterruptedException {

        while (produceTime) {
            synchronized (this) {
                log.info("正在執行工作報告草稿單號資料重建任務...");
                if (!produceTime) {
                    break;
                }
                this.wait(500);
            }
        }
        if (tempTreeNoMap.get(companySid) == null) {
            tempTreeNoMap.put(companySid, this.createTempTree(companySid));
        }
        TreeSet<String> tempTreeNo = tempTreeNoMap.get(companySid);
        if (tempTreeNo.isEmpty()) {
            String uuid = UUID.randomUUID().toString();
            log.error("本日工作報告草稿流水號已用完！！產生UUID碼取代:" + uuid);
            return uuid;
        }
        return tempTreeNo.pollFirst();
    }

}
