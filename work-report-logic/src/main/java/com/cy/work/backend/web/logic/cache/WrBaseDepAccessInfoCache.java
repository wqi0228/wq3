/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.cache;

import com.cy.work.backend.web.repository.WrBaseDepAccessInfoRepository;
import com.cy.work.backend.web.vo.WrBaseDepAccessInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 可閱讀部門權限Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WrBaseDepAccessInfoCache implements Serializable {


    private static final long serialVersionUID = 559180294535791054L;
    /** WrBaseDepAccessInfoRepository */
    @Autowired
    private WrBaseDepAccessInfoRepository wrBaseDepAccessInfoRepository;
    /** 控制啟動FLAG */
    private Boolean startController;
    /** Cache 物件 */
    private Map<String, WrBaseDepAccessInfo> wrBaseDepAccessInfoMap;

    public WrBaseDepAccessInfoCache() {
        startController = Boolean.TRUE;
        wrBaseDepAccessInfoMap = Maps.newConcurrentMap();
    }

    /** 初始化資料 */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            this.updateCache();
        }
    }

    /** 排程更新Cache資料 */
    @Scheduled(cron = "0 0/30 * * * ?")
    public void updateCache() {
        log.info("建立可閱讀部門權限Cache");
        try {
            Map<String, WrBaseDepAccessInfo> tempWrBaseDepAccessInfoMap = Maps.newConcurrentMap();
            wrBaseDepAccessInfoRepository.findAll().forEach(item -> {
                tempWrBaseDepAccessInfoMap.put(item.getSid(), item);
            });
            wrBaseDepAccessInfoMap = tempWrBaseDepAccessInfoMap;
        } catch (Exception e) {
            log.error("updateCache", e);
        }
        log.info("建立可閱讀部門權限Cache結束");
    }

    /** 更新Cache資料 */
    public void updateCacheBySid(WrBaseDepAccessInfo wrBaseDepAccessInfo) {
        wrBaseDepAccessInfoMap.put(wrBaseDepAccessInfo.getSid(), wrBaseDepAccessInfo);
    }

    /**
     * 取得可閱讀部門權限 By Sid
     *
     * @param sid 可閱讀部門權限Sid
     * @return
     */
    public WrBaseDepAccessInfo findBySid(String sid) {
        return wrBaseDepAccessInfoMap.get(sid);
    }

    /**
     * 取得可閱讀部門權限 By 部門Sid
     *
     * @param depSid 部門Sid
     * @return
     */
    public WrBaseDepAccessInfo getByDepSid(Integer depSid) {
        for (WrBaseDepAccessInfo wi : Lists.newArrayList(wrBaseDepAccessInfoMap.values())) {
            if (wi.getLoginUserDepSid().equals(depSid)) {
                return wi;
            }
        }
        return null;
    }

}
