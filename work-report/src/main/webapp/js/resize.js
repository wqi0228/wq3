/**
 * 
 */

var doResizeReplyEditor;
var sourceWidth;
var sourceHeight;

function initReinstatedMuliDlg(dialog_id, edit_id, edit_two_id, otherWidth, otherHeight) {
	sourceWidth = $("#" + dialog_id).width();
	sourceHeight = $("#" + dialog_id).height();
	$("#" + dialog_id).resize(function() {
		clearTimeout(doResizeReplyEditor);
		doResizeReplyEditor = setTimeout(resizeByMuliEditor(dialog_id, edit_id, edit_two_id, otherWidth, otherHeight), 10);
	});
}

function reinstatedMuliSize(dialog_id, edit_id, edit_two_id, otherWidth, otherHeight) {
	$("#" + dialog_id).width(sourceWidth);
	$("#" + dialog_id).height(sourceHeight);
	resizeByMuliEditor(dialog_id, edit_id, edit_two_id, otherWidth, otherHeight);
}

function resizeByMuliEditor(dialog_id, edit_id, edit_two_id, otherWidth, otherHeight) {

	var unitHeight = ($("#" + dialog_id).height() - otherHeight) / 4;
	var editorWidth = $("#" + dialog_id).width() - otherWidth;
	var editorHeight = unitHeight * 3;
	var editorTwoWidth = editorWidth;
	var editorTwoHeight = unitHeight;

	$("#" + edit_id).width(editorWidth);
	$("#" + edit_id).height(editorHeight);
	$("#" + edit_id + " .ui-editor").width(editorWidth);
	$("#" + edit_id + " .ui-editor").height(editorHeight);
	$("#" + edit_id).find('iframe').each(function() {
		$(this).width(editorWidth);
		$(this).height(editorHeight - 27);
	});
	$("#" + edit_id).find('iframe').contents().find('body').each(function() {
		$(this).width(editorWidth - 30);
		$(this).height(editorHeight - 35);
	});

	$("#" + edit_two_id).width(editorTwoWidth);
	$("#" + edit_two_id).height(editorTwoHeight);
	$("#" + edit_two_id + " .ui-editor").width(editorTwoWidth);
	$("#" + edit_two_id + " .ui-editor").height(editorTwoHeight);
	$("#" + edit_two_id).find('iframe').each(function() {
		$(this).width(editorTwoWidth);
		$(this).height(editorTwoHeight - 27);
	});
	$("#" + edit_two_id).find('iframe').contents().find('body').each(function() {
		$(this).width(editorTwoWidth - 30);
		$(this).height(editorTwoHeight - 35);
	});
}

function initReinstatedDlg(dialog_id, edit_id, otherWidth, otherHeight) {
	sourceWidth = $("#" + dialog_id).width();
	sourceHeight = $("#" + dialog_id).height();
	$("#" + dialog_id).resize(function() {
		clearTimeout(doResizeReplyEditor);
		doResizeReplyEditor = setTimeout(resizeByEditor(dialog_id, edit_id, otherWidth, otherHeight), 10);
	});
}

function reinstatedSize(dialog_id, edit_id, otherWidth, otherHeight) {
	$("#" + dialog_id).width(sourceWidth);
	$("#" + dialog_id).height(sourceHeight);
	resizeByEditor(dialog_id, edit_id, otherWidth, otherHeight);
}

function resizeByEditor(dialog_id, edit_id, otherWidth, otherHeight) {
	var editorWidth = $("#" + dialog_id).width() - otherWidth;
	var editorHeight = $("#" + dialog_id).height() - otherHeight;
	$("#" + edit_id).width(editorWidth);
	$("#" + edit_id).height(editorHeight);
	$("#" + edit_id + " .ui-editor").width(editorWidth);
	$("#" + edit_id + " .ui-editor").height(editorHeight);
	$("#" + edit_id).find('iframe').each(function() {
		$(this).width(editorWidth);
		$(this).height(editorHeight - 27);
	});
	$("#" + edit_id).find('iframe').contents().find('body').each(function() {
		$(this).width(editorWidth - 30);
		$(this).height(editorHeight - 35);
	});
}