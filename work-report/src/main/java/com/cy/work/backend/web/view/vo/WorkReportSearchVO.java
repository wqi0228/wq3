/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import com.cy.work.backend.web.logic.components.WRMasterSearchWorkReportLogicComponents;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class WorkReportSearchVO implements Serializable {


    private static final long serialVersionUID = -3433582741192898110L;
    public WorkReportSearchVO(String sid) {
        this.sid = sid;
    }

    public WorkReportSearchVO(String sid, boolean tofowardDep, boolean tofowardMember, boolean totrace,
            String create_time, String tag_name, String department_name, String create_usr_name,
            String title, String status, String no, String modify_time, String readStatus,
            String readStatusColor, String readStatusTextBold, String readRecipt,
            String readReciptStyleCss, String editType, String readRecipted) {
        this.sid = sid;
        this.tofowardDep = tofowardDep;
        this.tofowardMember = tofowardMember;
        this.totrace = totrace;
        this.create_time = create_time;
        this.tag_name = tag_name;
        this.department_name = department_name;
        this.create_usr_name = create_usr_name;
        this.title = title;
        this.status = status;
        this.no = no;
        this.modify_time = modify_time;
        this.readStatus = readStatus;
        this.readStatusColor = readStatusColor;
        this.readStatusTextBold = readStatusTextBold;
        this.readRecipt = readRecipt;
        this.readReciptStyleCss = readReciptStyleCss;
        this.editType = editType;
        this.readRecipted = readRecipted;
    }

    public void replaceReadStatus(WRReadStatus wRReadStatus) {
        this.readStatus = wRReadStatus.getVal();
        this.readStatusColor = WRMasterSearchWorkReportLogicComponents.getInstance().transToColorCss(wRReadStatus);
        this.readStatusTextBold = WRMasterSearchWorkReportLogicComponents.getInstance().transToBoldCss(wRReadStatus);
    }

    public void replaceValue(WorkReportSearchVO updateObject) {
        this.tofowardDep = updateObject.tofowardDep;
        this.tofowardMember = updateObject.tofowardMember;
        this.totrace = updateObject.totrace;
        this.create_time = updateObject.create_time;
        this.tag_name = updateObject.tag_name;
        this.department_name = updateObject.department_name;
        this.create_usr_name = updateObject.create_usr_name;
        this.title = updateObject.title;
        this.status = updateObject.status;
        this.no = updateObject.no;
        this.modify_time = updateObject.modify_time;
        this.readStatus = updateObject.readStatus;
        this.readStatusColor = updateObject.readStatusColor;
        this.readStatusTextBold = updateObject.readStatusTextBold;
        this.readRecipt = updateObject.readRecipt;
        this.readReciptStyleCss = updateObject.getReadReciptStyleCss();
        this.editType = updateObject.getEditType();
        this.readRecipted = updateObject.getReadRecipted();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkReportSearchVO other = (WorkReportSearchVO) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }

    @Getter
    private String sid;
    @Getter
    private boolean tofowardDep;
    @Getter
    private boolean tofowardMember;
    @Getter
    private boolean totrace;
    @Getter
    private String create_time;
    @Getter
    private String tag_name;
    @Getter
    private String department_name;
    @Getter
    private String create_usr_name;
    @Getter
    private String title;
    @Getter
    private String status;
    @Getter
    private String no;
    @Getter
    private String modify_time;
    @Getter
    private String readStatus;
    @Getter
    private String readStatusColor;
    @Getter
    private String readStatusTextBold;
    @Getter
    private String readRecipt;
    @Getter
    private String readReciptStyleCss;
    @Getter
    private String editType;
    @Getter
    private String readRecipted;
}
