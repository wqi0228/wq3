/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class WRLastModifySearchColumnVO implements Serializable {

    
    private static final long serialVersionUID = -1116505646018313248L;
    @Setter
    @Getter
    private WRColumnDetailVO index;
    @Setter
    @Getter
    private WRColumnDetailVO createTime;
    @Setter
    @Getter
    private WRColumnDetailVO department;
    @Setter
    @Getter
    private WRColumnDetailVO createUser;
    @Setter
    @Getter
    private WRColumnDetailVO theme;

    @Setter
    @Getter
    private WRColumnDetailVO no;
    @Setter
    @Getter
    private WRColumnDetailVO lastModifyTime;
    @Setter
    @Getter
    private WRColumnDetailVO memo;
    @Setter
    @Getter
    private WRColumnDetailVO status;
    @Setter
    @Getter
    private WRColumnDetailVO editType;

    @Setter
    @Getter
    private String pageCount = "50";

}
