/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class WRColumnDetailVO implements Serializable {


    private static final long serialVersionUID = -4883128157972074021L;
    public WRColumnDetailVO(String name, boolean show, String width) {
        this.name = name;
        this.show = show;
        this.width = width;
    }
    @Getter
    private String name;
    @Getter
    private boolean show;
    @Getter
    private String width;
}
