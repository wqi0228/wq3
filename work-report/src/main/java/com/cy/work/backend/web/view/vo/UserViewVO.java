/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class UserViewVO implements Serializable {


    private static final long serialVersionUID = -6252853263351178620L;
    /** User SID */
    @Getter
    private final Integer sid;
    /** User Name */
    @Getter
    private final String name;
    /** 部門名稱 */
    @Getter
    private String departmentName;
    @Setter
    @Getter
    private boolean moveAble = true;
    @Setter
    @Getter
    private boolean modifyed = false;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserViewVO other = (UserViewVO) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }

    public UserViewVO() {
        this.sid = null;
        this.name = "";
    }

    public UserViewVO(Integer sid) {
        this.sid = sid;
        this.name = "";
    }

    public UserViewVO(Integer sid, String name) {
        this.sid = sid;
        this.name = name;
    }

    public UserViewVO(Integer sid, String name, String departmentName) {
        this.sid = sid;
        this.name = name;
        this.departmentName = departmentName;
    }
}
