/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.testng.collections.Lists;

/**
 * 特殊參數邏輯元件
 * @author brain0925_liao
 */
@Component
public class WorkParamLogicComponents implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = -4591193808422378174L;
    private static WorkParamLogicComponents instance;

    public static WorkParamLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WorkParamLogicComponents.instance = this;
    }

    /**
     * 取得特殊人員Sids
     * @return 
     */
    public List<Integer> getBigBossSids() {
        return Lists.newArrayList(72,82,146);
    }

}
