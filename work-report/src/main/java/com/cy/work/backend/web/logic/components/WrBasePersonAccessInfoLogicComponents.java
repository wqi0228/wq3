/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.manager.WrBasePersonAccessInfoManager;
import com.cy.work.backend.web.vo.WrBasePersonAccessInfo;
import com.cy.work.backend.web.vo.converter.to.LimitBaseAccessViewPerson;
import com.cy.work.backend.web.vo.converter.to.OtherBaseAccessViewPerson;
import com.cy.work.backend.web.vo.converter.to.UserTo;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 基礎單位成員可閱權限設定邏輯原件
 *
 * @author brain0925_liao
 */
@Component
public class WrBasePersonAccessInfoLogicComponents implements InitializingBean, Serializable {


    private static final long serialVersionUID = -8582829986736873412L;
    private static WrBasePersonAccessInfoLogicComponents instance;

    @Autowired
    private WrBasePersonAccessInfoManager wrBasePersonAccessInfoManager;

    public static WrBasePersonAccessInfoLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WrBasePersonAccessInfoLogicComponents.instance = this;
    }

    /**
     * 取得限制基本部門成員 By 使用者Sid
     *
     * @param userSid 使用者Sid
     * @return
     */
    public List<User> getLimitBaseAccessViewUser(Integer userSid) {
        return wrBasePersonAccessInfoManager.getLimitBaseAccessViewUser(userSid);
    }

    /**
     * 取得增加基本部門成員 By 使用者Sid
     *
     * @param userSid 使用者Sid
     * @return
     */
    public List<User> getOtherBaseAccessViewUser(Integer userSid) {
        return wrBasePersonAccessInfoManager.getOtherBaseAccessViewUser(userSid);
    }

    public void clearLimitBaseAccessViewUser(Integer userSid, Integer loginUserSid) {
        WrBasePersonAccessInfo wrBasePersonAccessInfo = wrBasePersonAccessInfoManager.getByUserSid(userSid);
        if (wrBasePersonAccessInfo == null) {
            return;
        }
        wrBasePersonAccessInfo.setLimitBaseAccessViewPerson(null);
        wrBasePersonAccessInfoManager.updateWrBasePersonAccessInfo(wrBasePersonAccessInfo, loginUserSid);
    }

    /**
     * 儲存限制基本部門成員
     *
     * @param userSid 使用者Sid
     * @param loginUserSid 登入者Sid
     * @param accessUsers 可閱讀的限制基本單位部門成員
     */
    public void saveLimitBaseAccessViewUser(Integer userSid, Integer loginUserSid, List<User> accessUsers) {
        WrBasePersonAccessInfo wrBasePersonAccessInfo = wrBasePersonAccessInfoManager.getByUserSid(userSid);
        if (wrBasePersonAccessInfo == null) {
            wrBasePersonAccessInfo = new WrBasePersonAccessInfo();
            wrBasePersonAccessInfo.setLoginUserSid(userSid);
        }
        LimitBaseAccessViewPerson limitBaseAccessViewPerson = new LimitBaseAccessViewPerson();
        accessUsers.forEach(item -> {
            UserTo dt = new UserTo();
            dt.setSid(item.getSid());
            limitBaseAccessViewPerson.getUserTos().add(dt);
        });
        wrBasePersonAccessInfo.setLimitBaseAccessViewPerson(limitBaseAccessViewPerson);
        if (!Strings.isNullOrEmpty(wrBasePersonAccessInfo.getSid())) {
            wrBasePersonAccessInfoManager.updateWrBasePersonAccessInfo(wrBasePersonAccessInfo, loginUserSid);
        } else {
            wrBasePersonAccessInfoManager.createWrBasePersonAccessInfo(wrBasePersonAccessInfo, loginUserSid);
        }
    }

    /**
     * 儲存新增基本部門成員
     *
     * @param userSid 使用者Sid
     * @param loginUserSid 登入者Sid
     * @param accessUsers 可閱讀的新增基本單位部門成員
     */
    public void saveOtherBaseAccessViewUser(Integer userSid, Integer loginUserSid, List<User> accessUsers) {
        WrBasePersonAccessInfo wrBasePersonAccessInfo = wrBasePersonAccessInfoManager.getByUserSid(userSid);
        if (wrBasePersonAccessInfo == null) {
            wrBasePersonAccessInfo = new WrBasePersonAccessInfo();
            wrBasePersonAccessInfo.setLoginUserSid(userSid);
        }
        OtherBaseAccessViewPerson otherBaseAccessViewPerson = new OtherBaseAccessViewPerson();
        accessUsers.forEach(item -> {
            UserTo dt = new UserTo();
            dt.setSid(item.getSid());
            otherBaseAccessViewPerson.getUserTos().add(dt);
        });
        wrBasePersonAccessInfo.setOtherBaseAccessViewPerson(otherBaseAccessViewPerson);
        if (!Strings.isNullOrEmpty(wrBasePersonAccessInfo.getSid())) {
            wrBasePersonAccessInfoManager.updateWrBasePersonAccessInfo(wrBasePersonAccessInfo, loginUserSid);
        } else {
            wrBasePersonAccessInfoManager.createWrBasePersonAccessInfo(wrBasePersonAccessInfo, loginUserSid);
        }
    }

}
