package com.cy.work.backend.web.controller.component.mipker;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.cy.work.backend.web.controller.component.PickerComponentHelper;
import com.cy.work.backend.web.controller.component.mipker.helper.MultItemPickerComponentHelper;
import com.cy.work.backend.web.controller.component.mipker.vo.MultItemPickerShowMode;
import com.cy.work.backend.web.controller.component.mipker.vo.MultItemPickerTreeVO;
import com.cy.work.backend.web.controller.component.mipker.vo.MultItemPickerVO;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
public class MultItemPickerComponent implements Serializable {

    
    private static final long serialVersionUID = 8917998983100258146L;
    // ========================================================================
    // 服務元件區
    // ========================================================================
    private MultItemPickerComponentHelper helper = MultItemPickerComponentHelper.getInstance();
    private PickerComponentHelper pickerHelper = PickerComponentHelper.getInstance();
    private DisplayController displayController = DisplayController.getInstance();

    // ========================================================================
    // 變數區
    // ========================================================================
    /**
     * call back 實做方法
     */
    private MultItemPickerCallback callback;
    /**
     * 設定
     */
    private MultItemPickerConfig config;

    /**
     * 所有項目
     */
    private List<WkItem> allItems;

    /**
     * 全部項目的親子關係
     */
    Map<WkItem, List<WkItem>> childItemsMapByParentItem;

    /**
     * 【待選擇清單】
     */
    @Getter
    private MultItemPickerVO srcVO;

    /**
     * 【群組待選擇清單】
     */
    @Getter
    private MultItemPickerVO srcGroupVO;

    /**
     * 【可選擇項目樹】
     */
    @Getter
    private MultItemPickerTreeVO srcTreeVO;

    /**
     * 【已選擇清單】
     */
    @Getter
    private MultItemPickerVO targetVO;

    /**
     * 顯示停用項目
     */
    @Setter
    @Getter
    private boolean showInActiveItem = false;

    /**
     * 可選清單顯示模式
     */
    @Setter
    @Getter
    private MultItemPickerShowMode srcShowMode = MultItemPickerShowMode.LIST;

    /**
     * 含以下項目
     */
    @Getter
    @Setter
    private boolean containFollowing = false;

    /**
     * 隱藏上方功能區
     */
    @Getter
    @Setter
    private boolean hideTopArea = false;

    // ----------------------------------
    // 群組相關
    // ----------------------------------
    /**
     * 項目群組的 SelectItems
     */
    @Getter
    private List<SelectItem> itemGroupSelectItems;
    
    @Getter
    @Setter
    private String selectedItemGroupSid = "";

    @Getter
    private boolean enableGroupMode = false;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * @param callback
     * @throws Exception
     */
    public MultItemPickerComponent(MultItemPickerConfig config, MultItemPickerCallback callback) throws SystemDevelopException {
        // ====================================
        //
        // ====================================
        this.config = new MultItemPickerConfig(config);
        this.callback = callback;
        this.containFollowing = config.isContainFollowing();
        this.srcShowMode = config.getDefaultShowMode();
        this.hideTopArea = config.isHideTopArea();
        this.enableGroupMode = config.isEnableGroupMode();

        // ====================================
        // 一般項目建立
        // ====================================
        // 呼叫實做方法取得所有項目
        List<WkItem> currAllItems = this.callback.prepareAllItems();
        // 呼叫實做方法
        List<WkItem> currSelectedItems = this.callback.prepareSelectedItems();
        // 重建資料
        this.rebuild(currAllItems, currSelectedItems);

        // ====================================
        // 群組功能建立
        // ====================================
        if (this.config.isEnableGroupMode()) {
            this.prepareGroupMode();
        }
        
        // ====================================
        // 清除 filter
        // ====================================
        // 有傳入時自動清除
        if (WkStringUtils.notEmpty(this.config.getComponentID())) {
            this.displayController.execute(prepareFilterScript());
        }
    }

    /**
     * 取得已選擇的項目
     * 
     * @return
     */
    public List<WkItem> getSelectedItems() {
        return this.targetVO.getAllItems();
    }

    /**
     * 重建資料
     * 
     * @throws SystemDevelopException
     * @throws Exception
     * 
     */
    private void rebuild(
            List<WkItem> newAllItems,
            List<WkItem> selectedItems) throws SystemDevelopException {

        if (selectedItems == null) {
            selectedItems = Lists.newArrayList();
        }
        if (newAllItems == null) {
            newAllItems = Lists.newArrayList();
        }

        // ====================================
        // 初始化環境變數
        // ====================================
        this.srcVO = new MultItemPickerVO();
        this.targetVO = new MultItemPickerVO();
        this.allItems = newAllItems;

        // ====================================
        // 取得所有項目
        // ====================================
        // 不顯示停用時, 過濾停用
        if (!this.showInActiveItem) {
            this.allItems = this.allItems.stream()
                    .filter(WkItem::isActive)
                    .collect(Collectors.toList());
        }

        // 建立所有項目的親子關係
        this.childItemsMapByParentItem = this.pickerHelper.prepareAllItemRelation(this.allItems);

        // 以 SID 建立 index
        Map<String, WkItem> allItemMapBySid = this.allItems.stream()
                .collect(Collectors.toMap(WkItem::getSid, item -> item));

        // ====================================
        // 取得已選擇項目
        // ====================================
        // 依據選擇可項目, 給排序序號
        for (WkItem selectedItem : selectedItems) {
            if (allItemMapBySid.containsKey(selectedItem.getSid())) {
                WkItem srcItem = allItemMapBySid.get(selectedItem.getSid());
                if (srcItem != null) {
                    selectedItem.setItemSeq(srcItem.getItemSeq());
                }
            }
        }

        // 排序
        if (this.config.getItemComparator() != null) {
            selectedItems = selectedItems.stream()
                    .sorted(this.config.getItemComparator())
                    .collect(Collectors.toList());
        }

        // 已選擇清單特殊規則排序
        final List<WkItem> finalSelectedItems = this.callback.beforTargetShow(selectedItems);

        this.targetVO.setAllItems(finalSelectedItems);
        this.targetVO.setFilteredItems(finalSelectedItems);
        // ====================================
        // 整理可選清單
        // ====================================
        List<WkItem> waitSelectItems = this.allItems.stream()
                // 過濾已選擇項目
                .filter(item -> !finalSelectedItems.contains(item))
                .collect(Collectors.toList());

        // 排序
        if (this.config.getItemComparator() != null) {
            waitSelectItems = waitSelectItems.stream()
                    .sorted(this.config.getItemComparator())
                    .collect(Collectors.toList());
        }

        this.srcVO.setAllItems(waitSelectItems);
        this.srcVO.setFilteredItems(waitSelectItems);

        // ====================================
        // 標記 disable
        // ====================================
        List<String> disableItemSids = this.callback.prepareDisableItemSids();
        if (WkStringUtils.notEmpty(disableItemSids)) {

            for (WkItem item : this.allItems) {
                if (disableItemSids.contains(item.getSid())) {
                    item.setDisable(true);
                }
            }

            for (WkItem item : this.targetVO.getAllItems()) {
                item.setDisable(disableItemSids.contains(item.getSid()));
            }
        }

        // ====================================
        // 建立樹狀清單資料
        // ====================================
        this.rebuildTreeNode();

    }
    
    /**
     * @return
     */
    public String prepareFilterScript() {
        if (WkStringUtils.isEmpty(this.config.getComponentID())) {
            throw new SystemDevelopException("config 未傳入 ComponentID");
        }
        
        return ";clearFilters" + this.config.getComponentID() + "('all');";
    }

    
    private void rebuildTreeNode() {

        this.srcTreeVO = new MultItemPickerTreeVO();

        // ====================================
        // 判斷是否需要產生資料
        // ====================================
        if (WkStringUtils.isEmpty(this.allItems)) {
            return;
        }

        // 有任何一組 item 有設定 parent
        // boolean hasAnyChildItem = this.allItems.stream().anyMatch(item ->
        // (item.getParent() != null));

        // 沒有任何一個有設定, 不需要產生樹狀模式資料
        // if (!hasAnyChildItem) {
        // return;
        // }

        // 宣告可使用樹狀模式
        this.srcTreeVO.setHasTreeMode(true);

        // ====================================
        // 建立父子關係資料結構
        // ====================================
        this.srcTreeVO.getTreeNodeMapByItem().clear();
        for (WkItem item : this.allItems) {

            if (!showInActiveItem && !item.isActive()) {
                // 不顯示停用, 且項目為停用時, 跳過
                continue;
            }

            // 建立項目樹節點物件
            DefaultTreeNode node = new DefaultTreeNode(item);
            node.setSelectable(!item.isDisable());

            this.srcTreeVO.getTreeNodeMapByItem().put(item, node);
        }

        for (WkItem item : this.srcTreeVO.getTreeNodeMapByItem().keySet()) {

            // 取得項目節點物件
            TreeNode itemTreeNode = this.srcTreeVO.getTreeNodeMapByItem().get(item);

            // 取得父項目
            WkItem parentItem = item.getParent();

            TreeNode parentTreeNode = this.srcTreeVO.getTreeNodeMapByItem().get(parentItem);

            if (parentItem == null) {
                // 無父項目, 指向 root
                if (!this.srcTreeVO.getRootNode().getChildren().contains(itemTreeNode)) {
                    this.srcTreeVO.getRootNode().getChildren().add(itemTreeNode);
                }
            } else if (parentTreeNode == null) {
                // 找不到父項目節點物件, 指向 root (應該不會發生)
                if (!this.srcTreeVO.getRootNode().getChildren().contains(itemTreeNode)) {
                    this.srcTreeVO.getRootNode().getChildren().add(itemTreeNode);
                }
            } else {
                // 將節點指向父節點
                if (!parentTreeNode.getChildren().contains(itemTreeNode)) {
                    parentTreeNode.getChildren().add(itemTreeNode);
                }
            }
        }

        // ====================================
        // 樹節點屬性處理
        // ====================================
        // 逐筆處理
        for (WkItem targetItem : this.targetVO.getAllItems()) {

            // 已選擇項目, 不在節點列表中時跳過 (可能已選項目為停用, 但可選項目不顯示停用)
            if (!this.srcTreeVO.getTreeNodeMapByItem().containsKey(targetItem)) {
                continue;
            }

            // 取得項目節點物件
            TreeNode itemTreeNode = this.srcTreeVO.getTreeNodeMapByItem().get(targetItem);
            // 已勾選
            itemTreeNode.setSelected(true);
            // 展開上層
            this.pickerHelper.changeNodeExpand(itemTreeNode, true);
        }
    }

    /**
     * 雙擊待選項目
     * 
     * @param event
     */
    public void event_SrcItemDoubleClick(SelectEvent event) {
        if (event.getObject() == null) {
            return;
        }

        // 取得選擇項目
        WkItem item = (WkItem) event.getObject();

        // 為鎖定項目 - 忽略
        if (item.isDisable()) {
            return;
        }

        // 選取
        this.helper.addSelectedItems(
                this.srcVO,
                this.targetVO,
                this.srcTreeVO,
                this.containFollowing,
                this.childItemsMapByParentItem,
                Lists.newArrayList(item),
                this.config.getItemComparator(),
                this.srcShowMode);

        // 已選擇清單特殊規則排序
        this.targetVO.setAllItems(
                this.callback.beforTargetShow(
                        this.targetVO.getAllItems()));
    }

    /**
     * 雙擊已選項目
     * 
     * @param event
     */
    public void event_TargetItemDoubleClick(SelectEvent event) {

        if (event.getObject() == null) {
            return;
        }

        // 取得選擇項目
        WkItem item = (WkItem) event.getObject();

        // 為鎖定項目 - 忽略
        if (item.isDisable()) {
            return;
        }

        // 移除
        this.helper.removeSelectedItems(
                this.srcVO,
                this.targetVO,
                this.srcTreeVO,
                Lists.newArrayList(item),
                this.config.getItemComparator(),
                this.srcShowMode);
    }

    /**
     * 功能：新增待選列表 - 選擇項目
     */
    public void btn_AddSelected() {
        // 將待選區列表中 - 被選擇項目 - 加入
        this.helper.addSelectedItems(
                this.srcVO,
                this.targetVO,
                this.srcTreeVO,
                this.containFollowing,
                this.childItemsMapByParentItem,
                this.srcVO.getSelectedItems(),
                this.config.getItemComparator(),
                this.srcShowMode);

        // 已選擇清單特殊規則排序
        this.targetVO.setAllItems(this.callback.beforTargetShow(this.targetVO.getAllItems()));
    }

    /**
     * 功能：新增全部
     */
    public void btn_AddAll() {
        // 將所有待選區項目加入
        this.helper.addSelectedItems(
                this.srcVO,
                this.targetVO,
                this.srcTreeVO,
                this.containFollowing,
                this.childItemsMapByParentItem,
                this.srcVO.getFilteredItems(),
                this.config.getItemComparator(),
                this.srcShowMode);

        // 已選擇清單特殊規則排序
        this.targetVO.setAllItems(this.callback.beforTargetShow(this.targetVO.getAllItems()));

    }

    /**
     * 功能：移除已選列表 - 選擇項目
     */
    public void btn_removeSelected() {
        // 將已選區列表中 - 被選擇項目 - 加入
        this.helper.removeSelectedItems(
                this.srcVO,
                this.targetVO,
                this.srcTreeVO,
                this.targetVO.getSelectedItems(),
                this.config.getItemComparator(),
                this.srcShowMode);
    }

    /**
     * 功能：移除全部
     */
    public void btn_removeAll() {
        // 將所有已選區項目移除
        this.helper.removeSelectedItems(
                this.srcVO,
                this.targetVO,
                this.srcTreeVO,
                this.targetVO.getFilteredItems(),
                this.config.getItemComparator(),
                this.srcShowMode);
    }

    /**
     * 事件：樹清單-勾選/反勾選
     */
    public void event_treeSelectNodeChange() {
        List<WkItem> selectedNodeItems = Lists.newArrayList();
        if (this.srcTreeVO.getSelectedNodes() != null) {
            for (TreeNode itemNode : Lists.newArrayList(this.srcTreeVO.getSelectedNodes())) {
                // 取得項目資料物件
                if (itemNode.getData() == null) {
                    continue;
                }
                selectedNodeItems.add((WkItem) itemNode.getData());
            }
        }

        // ====================================
        // 增加
        // ====================================
        for (WkItem selectedNodeItem : selectedNodeItems) {
            // 不存在於已選擇清單時, 加入
            if (!this.targetVO.getAllItems().contains(selectedNodeItem)) {
                this.helper.addSelectedItems(
                        this.srcVO,
                        this.targetVO,
                        this.srcTreeVO,
                        this.containFollowing,
                        this.childItemsMapByParentItem,
                        Lists.newArrayList(selectedNodeItem),
                        this.config.getItemComparator(),
                        this.srcShowMode);
            }
        }

        // ====================================
        // 移除
        // ====================================
        for (WkItem targetItem : Lists.newArrayList(this.targetVO.getAllItems())) {
            // 【已選項目】不在【所有可選項目清單】時, 若比對不到, 不應移除
            if (!this.srcTreeVO.getTreeNodeMapByItem().containsKey(targetItem)) {
                continue;
            }

            // 若已選清單(右邊), 不在已勾選項目中時, 從已選清單移除
            if (!selectedNodeItems.contains(targetItem)) {
                this.helper.removeSelectedItems(
                        this.srcVO,
                        this.targetVO,
                        this.srcTreeVO,
                        Lists.newArrayList(targetItem),
                        this.config.getItemComparator(),
                        this.srcShowMode);
            }
        }

        // 已選擇清單特殊規則排序
        this.targetVO.setAllItems(this.callback.beforTargetShow(this.targetVO.getAllItems()));
    }

    
    public void event_changeSrcShowMode() {
        // 初始化待選清單列表
        if (MultItemPickerShowMode.LIST.equals(this.srcShowMode)) {
            this.srcVO.initSelectedAndfiltered();
        }

        if (MultItemPickerShowMode.TREE.equals(this.srcShowMode)) {
            this.rebuildTreeNode();
        }

    }

    /**
     * 事件：樹清單 - 關鍵字查詢
     */
    public void tree_searchItemNameByKeyword() {
        // 重新轉換查詢關鍵字, 忽略大小寫差異
        String keyword = WkStringUtils.safeTrim(this.srcTreeVO.getSerachItemNameKeyword()).toUpperCase();
        // 遞迴展開名稱符合或已選的節點
        this.pickerHelper.tree_searchItemNameByKeyword_recursive(this.srcTreeVO.getRootNode(), keyword);
    }

    
    public void tree_changeAllSelected() {
    }

    /**
     * 選擇模式 - 可選項目
     * 
     * @return
     */
    public List<SelectItem> getSrcShowModeItems() {

        List<SelectItem> selectItems = Lists.newArrayList();
        for (MultItemPickerShowMode showMode : MultItemPickerShowMode.values()) {
            String label = showMode.getLabel();
            // 為樹模式, 加上前綴詞 (樹模式 -> 組織樹模式)
            if (MultItemPickerShowMode.TREE.equals(showMode)) {
                label = this.config.getTreeModePrefixName() + label;
            }

            selectItems.add(new SelectItem(showMode, label));
        }

        return selectItems;
    }

    /**
     * 取得已選擇項目 SID
     * 
     * @return
     */
    public List<String> getSelectedSid() {
        if (WkStringUtils.isEmpty(this.targetVO.getAllItems())) {
            return Lists.newArrayList();
        }
        return this.targetVO.getAllItems().stream().map(WkItem::getSid).collect(Collectors.toList());
    }

    // ========================================================================
    // group
    // ========================================================================
    
    private void prepareGroupMode() {

        this.selectedItemGroupSid = "";

        // 建立 Group 選項
        this.itemGroupSelectItems = Lists.newArrayList();

        // default 為未選擇
        this.itemGroupSelectItems.add(new SelectItem("", "--請選擇--"));

        // 呼叫實做方法, 取得 group 項目
        List<SelectItem> customItemGroups = this.callback.prepareGroupItems();
        if (WkStringUtils.notEmpty(customItemGroups)) {
            this.itemGroupSelectItems.addAll(customItemGroups);
        }
    }

    /**
     * 重新載入組選單
     */
    public void event_reloadItemGroup() {
        // 清除所選擇的群組
        this.selectedItemGroupSid = "";
        // 重新載入群組下拉選單
        this.prepareGroupMode();
        // 重新載入群組下拉選單
        this.event_changeItemGroup();
    }

    /**
     * 異動群組下拉選單
     */
    public void event_changeItemGroup() {
        // ====================================
        // 取得左邊可選項目
        // ====================================
        // 呼叫實做方法取得所有項目
        List<WkItem> currAllItems = this.callback.prepareAllItems();
        // 防呆
        if (currAllItems == null) {
            currAllItems = Lists.newArrayList();
        }
        // 建立索引
        Map<String, WkItem> currAllItemsMapBySid = currAllItems.stream()
                .collect(Collectors.toMap(
                        WkItem::getSid,
                        WkItem::getThis));

        // ====================================
        // 置換成群組項目
        // ====================================

        // 群組 sid 不為空時，將待選清單，改為群組項目
        if (WkStringUtils.notEmpty(this.selectedItemGroupSid)) {

            Set<WkItem> groupItems = Sets.newHashSet();
            // 保留異動前，已選擇的項目
            for (WkItem selectedItem : this.targetVO.getAllItems()) {
                if (currAllItemsMapBySid.containsKey(selectedItem.getSid())) {
                    groupItems.add(currAllItemsMapBySid.get(selectedItem.getSid()));
                }
            }

            // 呼叫外部實做，取得群組項目 SID
            List<String> groupItemSids = this.callback.prepareItemSidByGroupSid(this.selectedItemGroupSid);
            // 加入群組項目
            if (WkStringUtils.notEmpty(groupItemSids)) {
                for (String itemSid : groupItemSids) {
                    if (currAllItemsMapBySid.containsKey(itemSid)) {
                        groupItems.add(currAllItemsMapBySid.get(itemSid));
                    }
                }
            }
            // 置換成群組項目
            currAllItems = Lists.newArrayList(groupItems);
        }

        // ====================================
        // 重建資料
        // ====================================
        this.rebuild(currAllItems, this.targetVO.getAllItems());
        this.rebuildTreeNode();

        // ====================================
        // 群組時, 預設 tree mode
        // ====================================
        // this.srcShowMode = MultItemPickerShowMode.TREE;

    }

    /**
     * 設定選擇項目
     * 
     * @param selectedItemSids 選擇項目 sid
     */
    public void setSelectedItemsBySid(List<String> selectedItemSids) {

        // ====================================
        // 依據傳入SID 取得對應的 item
        // ====================================
        List<WkItem> selectedItems = this.allItems.stream()
                .filter(wkItem -> wkItem.isActive())
                .filter(wkItem -> selectedItemSids.contains(wkItem.getSid()))
                .collect(Collectors.toList());

        // ====================================
        // 重建
        // ====================================
        this.rebuild(this.allItems, selectedItems);

    }

}
