/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.TransUpdateCallBack;
import com.cy.work.backend.web.util.pf.DisplayController;
import java.io.Serializable;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */

public class WRTransTableComponent implements Serializable {
    
    private static final long serialVersionUID = -3019508449000180161L;

    @Getter
    private WRTransDepComponent transDepComponent;
    @Getter
    private WRTransPersonComponent transPersonComponent;

    private TransUpdateCallBack transUpdateCallBack;

    public WRTransTableComponent(Integer loginUserSid, Integer companySid, TransUpdateCallBack transUpdateCallBack, MessageCallBack messageCallBack) {
        this.transUpdateCallBack = transUpdateCallBack;
        transDepComponent = new WRTransDepComponent(loginUserSid, companySid, updateCallBack, closeCallBack, messageCallBack);
        transPersonComponent = new WRTransPersonComponent(loginUserSid, messageCallBack, updateCallBack, closeCallBack);
    }

    public final TransUpdateCallBack updateCallBack = new TransUpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -3330384009679147213L;

        @Override
        public void doUpdateData() {
            clear();
            transUpdateCallBack.doUpdateData();
            DisplayController.getInstance().hidePfWidgetVar("dlgForwardPerson");
            DisplayController.getInstance().hidePfWidgetVar("dlgForwardDep");
        }
    };

    public void clear() {
        transDepComponent.clear();
        transPersonComponent.clear();
    }

    public final TransUpdateCallBack closeCallBack = new TransUpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -687364688031616696L;

        @Override
        public void doUpdateData() {
            clear();
            DisplayController.getInstance().hidePfWidgetVar("dlgForwardPerson");
            DisplayController.getInstance().hidePfWidgetVar("dlgForwardDep");
        }
    };

    public void loadDataPerson(String wr_Sid, String wr_no) {
        transPersonComponent.loadData(wr_Sid, wr_no);
        DisplayController.getInstance().update("workReportTransPersonComponents_panelID");
    }

    public void loadDataDep(String wr_Sid, String wr_no) {
        transDepComponent.loadData(wr_Sid, wr_no);
        DisplayController.getInstance().update("workReportTransDepComponents_panelID");
    }
}
