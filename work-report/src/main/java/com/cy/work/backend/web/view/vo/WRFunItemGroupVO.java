/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class WRFunItemGroupVO implements Serializable {

    
    private static final long serialVersionUID = 2275908259212563205L;
    @Getter
    private final String groupSid;
    @Getter
    private final String name;
    @Getter
    private final Integer seq;
    @Getter
    private final String groupBaseSid;
    @Getter
    private final List<WRFunItemVO> wrFunItemVOs;

    public WRFunItemGroupVO(String groupSid, String name, Integer seq, String groupBaseSid, List<WRFunItemVO> wrFunItemVOs) {
        this.groupSid = groupSid;
        this.name = name;
        this.seq = seq;
        this.groupBaseSid = groupBaseSid;
        this.wrFunItemVOs = wrFunItemVOs;
    }

}
