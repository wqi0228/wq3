/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.callable;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRTransSearchWorkReportLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.cy.work.backend.web.vo.enums.TransReadStatus;
import com.cy.work.backend.web.vo.enums.WRFunItemComponentType;
import com.google.common.collect.Lists;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 部門轉發計算筆數Call able
 *
 * @author brain0925_liao
 */
public class IncomeDepCallable implements HomeCallable {

    /** 登入者Sid */
    private final Integer loginUserSid;
    /** 選單需計算筆數列舉 */
    private final WRFunItemComponentType wrFunItemComponentType;
    /** 選單Group物件 */
    private final WRFunItemGroupVO wrFunItemGroupVO;

    public IncomeDepCallable(WRFunItemGroupVO wrFunItemGroupVO,
            WRFunItemComponentType wrFunItemComponentType, Integer loginUserSid) {
        this.wrFunItemGroupVO = wrFunItemGroupVO;
        this.wrFunItemComponentType = wrFunItemComponentType;
        this.loginUserSid = loginUserSid;
    }

    @Override
    public HomeCallableCondition call() throws Exception {
        //計算三天內,寄送時間並且需閱讀的資料(未閱讀&待閱讀)
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -3);
        Date startDate = c.getTime();
        Date endDate = new Date();
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        //此功能僅轉寄部門使用,原本為基本單位,WORKRPT-64 已調整成向下單位
        //Org baseOrg = OrgLogicComponents.getInstance().getBaseOrg(user.getPrimaryOrg().getSid());
        Org nowOrg = OrgLogicComponents.getInstance().findOrgBySid(user.getPrimaryOrg().getSid());
        List<Org> selectBaseOrgs = OrgLogicComponents.getInstance().getAllChildOrg(nowOrg);
        selectBaseOrgs.add(nowOrg);
        List<Integer> receiveOrgs = Lists.newArrayList();
        selectBaseOrgs.forEach(item -> {
            receiveOrgs.add(item.getSid());
        });

        Integer count = WRTransSearchWorkReportLogicComponents.getInstance().getReceiveDepCount(TransReadStatus.NEED_READ.name(), receiveOrgs, startDate, endDate, loginUserSid);
        HomeCallableCondition reuslt = new HomeCallableCondition();
        if (count > 0) {
            reuslt.setConntString("(" + String.valueOf(count) + ")");
            reuslt.setCssString("color: red");
        }
        reuslt.setWrFunItemComponentType(wrFunItemComponentType);
        reuslt.setWrFunItemGroupVO(wrFunItemGroupVO);
        return reuslt;
    }

}
