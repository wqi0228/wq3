/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class WRFunItemVO implements Serializable {

    
    private static final long serialVersionUID = -2227117161377379432L;
    @Getter
    private final String function_title;
    @Getter
    private final String component_id;
    @Getter
    private final String url;
    @Getter
    private final Integer seq;
    @Getter
    private final String icon;
    @Getter
    private final String count_sql;
    @Getter
    private final String permission_role;
    @Setter
    @Getter
    private String cssStr = "";
    @Setter
    @Getter
    private String countInfo = "";

    public WRFunItemVO(String function_title, String component_id, String url,
            Integer seq, String icon, String count_sql, String permission_role) {

        this.function_title = function_title;
        this.component_id = component_id;
        this.url = url;
        this.seq = seq;
        this.icon = icon;
        this.count_sql = count_sql;
        this.permission_role = permission_role;
    }

}
