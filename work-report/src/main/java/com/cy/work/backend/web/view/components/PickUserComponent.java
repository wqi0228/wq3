/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.vo.Org;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.PickListCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.DualListModel;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class PickUserComponent implements Serializable {


    private static final long serialVersionUID = 2211885570476369156L;

    @Getter
    @Setter
    private DualListModel<UserViewVO> pickUsers;

    private List<UserViewVO> leftViewVO;



    private MessageCallBack messageCallBack;
    @Getter
    @Setter
    private String filterNameValue;

    private Integer compSid;

    private List<UserViewVO> allCompUser;

    private List<Org> allCanSelectOrgs;
    /** 挑選可調整權限部門(OrgTree元件) */
    private final DepTreeComponent selectDepTreeComponent;

    private List<Org> selectOrgs;

    private final String selectDepDialog_Var;

    private PickListCallBack pickListCallBack;

    public PickUserComponent(Integer compSid,
            MessageCallBack messageCallBack, String selectDepDialog_Var, PickListCallBack pickListCallBack) {
        this.messageCallBack = messageCallBack;
        this.pickListCallBack = pickListCallBack;
        this.selectDepDialog_Var = selectDepDialog_Var;
        this.compSid = compSid;
        this.pickUsers = new DualListModel<>();
        this.allCompUser = Lists.newArrayList();
        this.selectDepTreeComponent = new DepTreeComponent();
        initSelectDepTreeComponent();
    }

    public void savePickList() {
        pickListCallBack.saveSelectUser(pickUsers.getTarget());
    }

    /** 取得挑選部門Tree Manager
     *
     * @return */
    public MultipleDepTreeManager getSelectDepTreeManager() {
        return selectDepTreeComponent.getMultipleDepTreeManager();
    }

    /** 載入挑選部門 */
    public void loadSelectDepTree() {
        try {
            Org topOrg = new Org(compSid);
            //僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            List<Org> showOrgs = Lists.newArrayList();
            showOrgs.addAll(allCanSelectOrgs);
            allCanSelectOrgs.forEach(item -> {
                List<Org> allParents = OrgLogicComponents.getInstance().getAllParentOrgs(item);
                if (allParents == null || allParents.isEmpty()) {
                    return;
                }
                showOrgs.addAll(allParents.stream()
                        .filter(each -> !showOrgs.contains(each))
                        .collect(Collectors.toList()));
            });
            try {
                Collections.sort(showOrgs, new Comparator<Org>() {
                    @Override
                    public int compare(Org o1, Org o2) {
                        return o1.getSid() - o2.getSid();
                    }
                });
            } catch (Exception e) {
                log.error("sort Error", e);
            }

            selectDepTreeComponent.init(showOrgs, allCanSelectOrgs,
                    topOrg, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
        } catch (Exception e) {
            log.error("loadSelectDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 執行挑選部門 */
    public void doSelectDep() {
        try {
            selectOrgs = selectDepTreeComponent.getSelectOrg();
            loadSelectDepartment(selectOrgs);
            DisplayController.getInstance().hidePfWidgetVar(selectDepDialog_Var);
        } catch (Exception e) {
            log.error("doSelectDep ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 建立Empty 挑選部門Tree Manager */
    private void initSelectDepTreeComponent() {
        Org topOrg = new Org(compSid);
        //僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectDepTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                topOrg, Lists.newArrayList(), clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    public void loadUserPicker(List<UserViewVO> selUserViewVO, Integer selUserSid, List<Org> allCanSelectOrgs) {
        this.selectOrgs = Lists.newArrayList();
        this.allCanSelectOrgs = allCanSelectOrgs;
        allCompUser.clear();
        try {
            allCanSelectOrgs.forEach(item -> {
                allCompUser.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
            });
        } catch (Exception e) {
            log.error(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }
        leftViewVO = Lists.newArrayList();
        leftViewVO.addAll(allCompUser);
        List<UserViewVO> rightViewVO = Lists.newArrayList();
        rightViewVO.addAll(selUserViewVO);
        pickUsers.setSource(removeTargetUserView(leftViewVO, rightViewVO));
        pickUsers.setTarget(rightViewVO);
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source, List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(item -> {
            if (!target.contains(item)) {
                filterSource.add(item);
            }
        });
        return filterSource;
    }

    private void loadSelectDepartment(List<Org> departments) {
//        if (departments == null || departments.size() == 0) {
//            return;
//        }
        leftViewVO.clear();
        List<UserViewVO> selectDepUserView = Lists.newArrayList();
        departments.forEach(item -> {
            selectDepUserView.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
        });
        pickUsers.setSource(removeTargetUserView(selectDepUserView, pickUsers.getTarget()));
    }

    public void filterName() {
        if (Strings.isNullOrEmpty(filterNameValue)) {
            pickUsers.setSource(removeTargetUserView(allCompUser, pickUsers.getTarget()));
        } else {
            pickUsers.setSource(removeTargetUserView(allCompUser.stream()
                    .filter(each -> each.getName().toLowerCase().contains(filterNameValue.toLowerCase()))
                    .collect(Collectors.toList()), pickUsers.getTarget()));
        }
    }

}
