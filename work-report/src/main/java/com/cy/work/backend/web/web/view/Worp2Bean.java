/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.PreviousAndNextCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.WorkReportViewComponent;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Slf4j
@Scope("view")
@ManagedBean
public class Worp2Bean implements Serializable {

    
    private static final long serialVersionUID = 2425157990385699836L;

    @Getter
    @Setter
    private String wrcId;

    /** view 顯示錯誤訊息 */
    @Getter
    private String errorMessage;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Getter
    private WorkReportViewComponent workReportViewComponent;

    private boolean showBackHomeBtn = false;
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Autowired
    private SessionTimerBean sessionTimerBean;

    private String encryptParam = "";

    private String url = "";

    private PreviousAndNextCallBack previousAndNextCallBack = new PreviousAndNextCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -8756196726863571802L;

        @Override
        public void doPrevious() {
            // log.info("開始進行確認上一筆");
            initializeDataTable();
        }

        @Override
        public void doNext() {
            // log.info("開始進行確認下一筆");
            initializeDataTable();
        }
    };

    @PostConstruct
    public void init() {
        userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        depViewVo = OrgLogicComponents.getInstance().findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = OrgLogicComponents.getInstance().findById(SecurityFacade.getCompanyId());
        workReportViewComponent = new WorkReportViewComponent(userViewVO, depViewVo, compViewVo, messageCallBack, previousAndNextCallBack);
        if (!Faces.getRequestParameterMap().containsKey("wrcId")) {
            return;
        }
        wrcId = Faces.getRequestParameterMap().get("wrcId");
        if (Faces.getRequestParameterMap().containsKey("encryptParam")) {
            encryptParam = Faces.getRequestParameterMap().get("encryptParam");
        }
        workReportViewComponent.loadWrcID(wrcId, encryptParam);

    }

    public void initialize() {
        sessionTimerBean.reStartIdle();
        DisplayController.getInstance().execute("reEditor()");
        // Request scrope 僅view Load時執行
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
        if (!Faces.getRequestParameterMap().containsKey("wrcId")) {
            return;
        }

        wrcId = Faces.getRequestParameterMap().get("wrcId");
//        long startTime = System.currentTimeMillis();
//        while (!tableUpDownBean.isSessionSettingOver()) {
//            log.info("執行上下筆,Session等待執行值,最多等待兩秒");
//            try {
//                TimeUnit.MICROSECntTimeMillis() - startTime > 2000) {
//                    .clearSessionSettingOver();
//                    log.error("執行上下筆,Session並未等到執行值,最多等待兩秒");
//                    break;
//                }
//            } catch (Exception e) {
//                log.error("SleetableUpDownBeanONDS.sleep(50);
//                if (System.currep Error", e);
//            }
//        }
//         tableUpDownBean.clearSessionSettingOver();
        boolean showPreviousBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_previous_sid()));
        tableUpDownBean.setSession_previous_sid("");
        boolean showNextBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_next_sid()));
        tableUpDownBean.setSession_next_sid("");
        showBackHomeBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_show_home()));
        ;
        tableUpDownBean.setSession_show_home("");
        url = tableUpDownBean.getWorp_path();
        tableUpDownBean.setWorp_path("");
        workReportViewComponent.initialize(wrcId, showPreviousBtn, showNextBtn, showBackHomeBtn);
    }

    public void initializeDataTable() {
        long startTime = System.currentTimeMillis();
        while (!tableUpDownBean.isSessionSettingOver()) {
            log.info("執行上下筆,Session等待執行值,最多等待兩秒");
            try {
                TimeUnit.MICROSECONDS.sleep(50);
                if (System.currentTimeMillis() - startTime > 2000) {
                    tableUpDownBean.clearSessionSettingOver();
                    log.error("執行上下筆,Session並未等到執行值,最多等待兩秒");
                    break;
                }
            } catch (InterruptedException e) { // Noncompliant; logging is not enough
                log.error("Sleep Error", e);
            }
        }
        tableUpDownBean.clearSessionSettingOver();
        boolean showPreviousBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_previous_sid()));
        tableUpDownBean.setSession_previous_sid("");
        boolean showNextBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_next_sid()));
        tableUpDownBean.setSession_next_sid("");
        String wrcId = "";
        if (!Strings.isNullOrEmpty(tableUpDownBean.getSession_now_sid())) {
            wrcId = tableUpDownBean.getSession_now_sid();
            tableUpDownBean.setSession_now_sid("");
        }
        if (!Strings.isNullOrEmpty(wrcId)) {
            DisplayController.getInstance().execute("replaceUrlNotNavigation('" + url + ".xhtml?wrcId=" + wrcId + "&encryptParam=" + encryptParam + "')");
        }
        workReportViewComponent.initialize(wrcId, showPreviousBtn, showNextBtn, showBackHomeBtn);
    }

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -5868337004145910059L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    };

}
