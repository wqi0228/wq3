/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.ExportExcelComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.listener.DataTableReLoadCallBack;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.TableUpDownListener;
import com.cy.work.backend.web.listener.TraceUpdateCallBack;
import com.cy.work.backend.web.listener.TransUpdateCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRMasterSearchWorkReportLogicComponents;
import com.cy.work.backend.web.logic.components.WRReportCustomColumnLogicComponents;
import com.cy.work.backend.web.logic.components.WrBaseDepAccessInfoLogicComponents;
import com.cy.work.backend.web.logic.manager.WRReportCustomColumnHelper;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.CustomColumnComponent;
import com.cy.work.backend.web.view.components.WRTraceComponent;
import com.cy.work.backend.web.view.components.WRTransTableComponent;
import com.cy.work.backend.web.view.components.WorkReportSearchHeaderComponent;
import com.cy.work.backend.web.view.vo.WRWorkReportSearchColumnVO;
import com.cy.work.backend.web.view.vo.WorkReportSearchVO;
import com.cy.work.backend.web.view.vo.WorkReportSidTo;
import com.cy.work.backend.web.vo.converter.to.CustomClauseVO;
import com.cy.work.backend.web.vo.enums.SqlColumnType;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.cy.work.backend.web.vo.enums.WRStatus;
import com.cy.work.backend.web.vo.enums.WRWorkReportSearchColumn;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Search2Bean implements Serializable, TableUpDownListener {

    private static final long serialVersionUID = 7857141778911902312L;
    @Getter
    private WRWorkReportSearchColumnVO searchColumnVO;
    @Autowired
    private WRReportCustomColumnLogicComponents wrReportCustomColumnLogicComponents;
    @Autowired
    private OrgLogicComponents orgLogicComponents;
    @Autowired
    private WrBaseDepAccessInfoLogicComponents wrBaseDepAccessInfoLogicComponents;
    @Getter
    private WorkReportSearchHeaderComponent workReportSearchHeaderComponent;
    @Getter
    private CustomColumnComponent customColumn;
    /** view 顯示錯誤訊息 */
    @Getter
    private String errorMessage;
    @Getter
    private final String dataTableID = "dataTableWorkReport";
    @Getter
    private final String dataTableWv = "dataTableWorkReportWv";
    @Setter
    @Getter
    private List<WorkReportSearchVO> workReportSearchVOs;

    private List<WorkReportSearchVO> tempWorkReportSearchVOs;

    private String tempSelSid;
    @Setter
    @Getter
    private WorkReportSearchVO selWorkReportSearchVO;
    @Autowired
    private WRMasterSearchWorkReportLogicComponents wRMasterSearchWorkReportLogicComponents;
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Setter
    @Getter
    private boolean showFrame = false;
    @Getter
    private String iframeUrl = "";

    private List<Org> defaultOrg;

    private List<Org> selectOrgs;

    private List<Org> allOrgs;

    private List<Org> canSelectOrgs;

    private List<Org> sendReadRecipt;

    private List<Org> otherBaseAccessViewOrgs;

    /** 部門相關邏輯Component */
    @Getter
    private DepTreeComponent depTreeComponent;

    @Autowired
    private ExportExcelComponent exportExcelComponent;
    @Getter
    private boolean hasDisplay = true;
    @Getter
    @Setter
    private String itemsSize;
    @Getter
    @Setter
    private String newItemIx;

    @Getter
    private WRTraceComponent traceComponent;

    private String selWr_ID;

    @Getter
    private WRTransTableComponent transTableComponent;

    @Autowired
    private WRReportCustomColumnHelper wrReportCustomColumnHelper;

    @Getter
    @Setter
    private CustomClauseVO customClauseVO;

    @Getter
    @Setter
    @NoArgsConstructor
    private class CanViewDep implements Serializable {

        /**
         * 
         */
        private static final long serialVersionUID = -7733452409162193863L;

        /* 一般可閱部門 (登入者主要部門上升到部以下) */
        private List<Integer> normalRightDepSids = Lists.newArrayList();

        /* 額外增加的可閱單位 */
        private List<Integer> extraRightDepSids = Lists.newArrayList();

        /* 因曾經被索取回條而可查詢的部門 */
        private List<Integer> sendReadReciptDepSids = Lists.newArrayList();

    }

    @PostConstruct
    public void init() {

        Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());

        workReportSearchHeaderComponent = new WorkReportSearchHeaderComponent();
        //
        searchColumnVO = wrReportCustomColumnLogicComponents.getWRWorkReportSearchColumnSetting(SecurityFacade.getUserSid());
        //
        this.traceComponent = new WRTraceComponent(SecurityFacade.getUserSid(), messageCallBack, traceUpdateCallBack);

        this.transTableComponent = new WRTransTableComponent(
                SecurityFacade.getUserSid(),
                comp.getSid(),
                transUpdateCallBack,
                messageCallBack);

        this.customColumn = new CustomColumnComponent(
                WRReportCustomColumnUrlType.WORK_REPORT_SEARCH,
                Arrays.asList(WRWorkReportSearchColumn.values()),
                searchColumnVO.getPageCount(),
                SecurityFacade.getUserSid(),
                messageCallBack,
                dataTableReLoadCallBack);

        defaultSearchSetting();
        initOrgTree();
        doSearch();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        try {
            exportExcelComponent.exportExcel(document, workReportSearchHeaderComponent.getStartDate(),
                    workReportSearchHeaderComponent.getEndDate(), "工作報告", true);
        } catch (Exception e) {
            log.error("exportExcel", e);
        } finally {
            hasDisplay = true;
        }
    }

    public void hideColumnContent() {
        hasDisplay = false;
    }

    public void clear() {
        defaultSearchSetting();
        doSearch();

    }

    public void doSelectOrg() {
        selectOrgs = depTreeComponent.getSelectOrg();
        DisplayController.getInstance().hidePfWidgetVar("deptUnitDlg");
    }

    private void initOrgTree() {

        canSelectOrgs = Lists.newArrayList();
        canSelectOrgs.addAll(defaultOrg);
        canSelectOrgs.addAll(otherBaseAccessViewOrgs);
        canSelectOrgs.addAll(sendReadRecipt);
        // 可閱單位移除
        // canSelectOrgs.addAll(orgLogicComponents.findInquireDepByUserSid(SecurityFacade.getUserSid(),
        // compViewVo.getSid()).stream()
        // .filter(each -> !canSelectOrgs.contains(each))
        // .collect(Collectors.toList()));
        allOrgs = Lists.newArrayList();
        allOrgs.addAll(canSelectOrgs);
        canSelectOrgs.forEach(item -> {
            List<Org> allParents = orgLogicComponents.getAllParentOrgs(item);
            if (allParents == null || allParents.isEmpty()) {
                return;
            }
            allOrgs.addAll(allParents.stream()
                    .filter(each -> !allOrgs.contains(each))
                    .collect(Collectors.toList()));
        });
        
        
        
        try {
            Collections.sort(allOrgs, new Comparator<Org>() {
                @Override
                public int compare(Org o1, Org o2) {
                    return o1.getSid() - o2.getSid();
                }
            });
        } catch (Exception e) {
            log.error("sort Error", e);
        }
        depTreeComponent = new DepTreeComponent();
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(item -> {
            tempOrg.add(item);
        });

        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        boolean defaultShowDisableDep = true;
        Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());

        depTreeComponent.init(
                tempOrg, 
                canSelectOrgs,
                comp, 
                selectOrgs, 
                clearSelNodeWhenChaneDepTree, 
                selectable, 
                selectModeSingle, 
                defaultShowDisableDep);
        // Org group = new Org(compViewVo.getSid());
        // boolean clearSelNodeWhenChaneDepTree = true;
        // boolean selectable = true;
        // boolean selectModeSingle = false;
        // loadOrgs();

        // depTreeComponent.init(allOrgs, canSelectOrgs,
        // group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    public void loadOrgs() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(item -> {
            tempOrg.add(item);
        });

        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
        depTreeComponent.loadTree(tempOrg, canSelectOrgs,
                comp, selectOrgs, clearSelNodeWhenChaneDepTree, selectable);

    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() { return depTreeComponent.getMultipleDepTreeManager(); }

    // 預設搜尋條件
    private void defaultSearchSetting() {
        workReportSearchHeaderComponent.settingDefault();
        workReportSearchHeaderComponent.setStartDate(new Date());
        workReportSearchHeaderComponent.setEndDate(new Date());
        workReportSearchHeaderComponent.setSelectStatus(WRStatus.COMMIT.name());
        customClauseVO = wrReportCustomColumnHelper.loadCustomClauseVO(SecurityFacade.getUserSid());
        if (customClauseVO != null) {
            workReportSearchHeaderComponent.replaceByCustomClause(customClauseVO);
        } else {
            customClauseVO = new CustomClauseVO();
        }

        sendReadRecipt = Lists.newArrayList();
        List<Org> tempSendReadRecipts = orgLogicComponents.findSendReadReceiptsOrg(SecurityFacade.getUserSid());

        defaultOrg = wrBaseDepAccessInfoLogicComponents.getBaseDepAccessInfoDeps(
                SecurityFacade.getPrimaryOrgSid(),
                SecurityFacade.getUserSid());

        selectOrgs = Lists.newArrayList();
        selectOrgs.addAll(defaultOrg);

        otherBaseAccessViewOrgs = wrBaseDepAccessInfoLogicComponents.getOtherBaseDepAccessInfoDeps(
                SecurityFacade.getPrimaryOrgSid(),
                SecurityFacade.getUserSid());
        otherBaseAccessViewOrgs.forEach(item -> {
            if (!selectOrgs.contains(item)) {
                selectOrgs.add(item);
            }
        });

        // List<Org> inquireDeps = orgLogicComponents.findInquireDepByUserSid(SecurityFacade.getUserSid(), compViewVo.getSid());
        tempSendReadRecipts.forEach(item -> {
            if (!selectOrgs.contains(item)) {
                // if (!inquireDeps.contains(item)) {
                sendReadRecipt.add(item);
                // }
                selectOrgs.add(item);
            }
        });
    }

    private List<WorkReportSearchVO> doSearch(String sid) {
        List<Integer> depSids = Lists.newArrayList();
        List<Integer> otherBaseViewDepSids = Lists.newArrayList();
        List<Integer> sendReadReciptSids = Lists.newArrayList();
        selectOrgs.forEach(item -> {
            if (sendReadRecipt.contains(item)) {
                sendReadReciptSids.add(item.getSid());
            } else if (otherBaseAccessViewOrgs.contains(item)) {
                otherBaseViewDepSids.add(item.getSid());
            } else {
                depSids.add(item.getSid());
            }
        });

        Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());

        return wRMasterSearchWorkReportLogicComponents.getWorkReport(
                workReportSearchHeaderComponent.getSelectTag(),
                workReportSearchHeaderComponent.getSelectStatus(),
                depSids,
                workReportSearchHeaderComponent.getTitle(),
                workReportSearchHeaderComponent.getContent(),
                sid,
                SecurityFacade.getUserSid(),
                workReportSearchHeaderComponent.getStartDate(),
                workReportSearchHeaderComponent.getEndDate(),
                sendReadReciptSids,
                SecurityFacade.getPrimaryOrgSid(),
                otherBaseViewDepSids,
                workReportSearchHeaderComponent.getPersonName(),
                workReportSearchHeaderComponent.getSqlColumnType(),
                workReportSearchHeaderComponent.getOrder(),
                comp.getSid());
    }

    public void doSearch() {
        try {
            workReportSearchVOs = doSearch("");
            if (workReportSearchVOs.size() > 0) {
                DisplayController.getInstance().execute("selectDataTablePage('" + dataTableWv + "',0);");
            }
        } catch (Exception e) {
            log.error("doSearch", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void openCustomColumn() {
        try {
            customColumn.loadData();
            DisplayController.getInstance().update("dlgDefineField_view");
            DisplayController.getInstance().showPfWidgetVar("dlgDefineField");
        } catch (Exception e) {
            log.error("openCustomColumn", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenUrl(String sid) {
        try {
            settintSelWorkReportSearchVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("btnOpenUrl", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            settintSelWorkReportSearchVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp_iframe.xhtml?wrcId=" + selWorkReportSearchVO.getSid();
            tableUpDownBean.setWorp_path("worp_iframe");
        } catch (Exception e) {
            log.error("btnOpenFrame", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void settingReaded() {
        workReportSearchVOs.forEach(item -> {
            if (selWorkReportSearchVO.getSid().equals(item.getSid())) {
                selWorkReportSearchVO.replaceReadStatus(WRReadStatus.HASREAD);
                item.replaceReadStatus(WRReadStatus.HASREAD);
            }
        });
    }

    private void settintSelWorkReportSearchVO(String sid) {
        WorkReportSearchVO sel = new WorkReportSearchVO(sid);
        int index = workReportSearchVOs.indexOf(sel);
        if (index > 0) {
            selWorkReportSearchVO = workReportSearchVOs.get(index);
        } else {
            selWorkReportSearchVO = workReportSearchVOs.get(0);
        }
        settingReaded();
    }

    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.error("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -1500769477084980396L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    };

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 388275773894285099L;

        @Override
        public void reload() {
            searchColumnVO = wrReportCustomColumnLogicComponents.getWRWorkReportSearchColumnSetting(SecurityFacade.getUserSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };

    private void toUp() {
        try {
            if (tempWorkReportSearchVOs != null && !tempWorkReportSearchVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempWorkReportSearchVOs.indexOf(new WorkReportSearchVO(tempSelSid));
                if (index > 0) {
                    selWorkReportSearchVO = tempWorkReportSearchVOs.get(index - 1);
                } else {
                    selWorkReportSearchVO = workReportSearchVOs.get(0);
                }
                tempWorkReportSearchVOs = null;
                tempSelSid = "";
            } else {
                int index = workReportSearchVOs.indexOf(selWorkReportSearchVO);
                if (index > 0) {
                    selWorkReportSearchVO = workReportSearchVOs.get(index - 1);
                } else {
                    selWorkReportSearchVO = workReportSearchVOs.get(0);
                }
            }
        } catch (Exception e) {
            log.error("toUp", e);
            tempSelSid = "";
            tempWorkReportSearchVOs = null;
            if (!workReportSearchVOs.isEmpty()) {
                selWorkReportSearchVO = workReportSearchVOs.get(0);
            }
        }
    }

    private void toDown() {
        try {
            if (tempWorkReportSearchVOs != null && !tempWorkReportSearchVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempWorkReportSearchVOs.indexOf(new WorkReportSearchVO(tempSelSid));
                if (index >= 0) {
                    selWorkReportSearchVO = tempWorkReportSearchVOs.get(index + 1);
                } else {
                    selWorkReportSearchVO = workReportSearchVOs.get(workReportSearchVOs.size() - 1);
                }
                tempWorkReportSearchVOs = null;
                tempSelSid = "";
            } else {
                int index = workReportSearchVOs.indexOf(selWorkReportSearchVO);
                if (index >= 0) {
                    selWorkReportSearchVO = workReportSearchVOs.get(index + 1);
                } else {
                    selWorkReportSearchVO = workReportSearchVOs.get(workReportSearchVOs.size() - 1);
                }
            }
        } catch (Exception e) {
            log.error("toDown", e);
            tempWorkReportSearchVOs = null;
            tempSelSid = "";
            if (workReportSearchVOs.size() > 0) {
                selWorkReportSearchVO = workReportSearchVOs.get(workReportSearchVOs.size() - 1);
            }
        }
    }

    public void settingPreviousAndNext() {
        int index = workReportSearchVOs.indexOf(selWorkReportSearchVO);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(workReportSearchVOs.get(index - 1).getSid());
        }
        if (index == workReportSearchVOs.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(workReportSearchVOs.get(index + 1).getSid());
        }
    }

    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        WkJsonUtils jsonUtils = SpringContextHolder.getBean(WkJsonUtils.class);
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.error("doReloadInfo", ex);
        }
    }

    private void updateInfoToDataTable(String sid) {
        try {
            List<WorkReportSearchVO> result = doSearch(sid);
            if (result == null || result.isEmpty()) {
                tempWorkReportSearchVOs = Lists.newArrayList();
                if (workReportSearchVOs != null && !workReportSearchVOs.isEmpty()) {
                    workReportSearchVOs.forEach(item -> {
                        tempWorkReportSearchVOs.add(item);
                    });
                    workReportSearchVOs.remove(new WorkReportSearchVO(sid));
                    tempSelSid = sid;
                }
            } else {
                WorkReportSearchVO updateDeail = result.get(0);
                workReportSearchVOs.forEach(item -> {
                    if (item.getSid().equals(updateDeail.getSid())) {
                        item.replaceValue(updateDeail);
                    }
                });

            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("reCheck Error", e);
        }
    }

    public void clickTrace(String wrSid) {
        try {
            List<WorkReportSearchVO> sel = workReportSearchVOs.stream()
                    .filter(each -> each.getSid().equals(wrSid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("追蹤資料比對有誤,無法開啟");
                return;
            }
            selWr_ID = wrSid;
            selWorkReportSearchVO = sel.get(0);
            DisplayController.getInstance().update(dataTableID);
            traceComponent.loadData(selWorkReportSearchVO.getSid(), selWorkReportSearchVO.getNo(), selWorkReportSearchVO.getTitle());
            DisplayController.getInstance().showPfWidgetVar("dlgTraceAction");
            DisplayController.getInstance().update("dlgTraceAction_view");
        } catch (Exception e) {
            log.error("clickTrace", e);
        }
    }

    public void clickTransDep(String wrSid) {
        try {
            List<WorkReportSearchVO> sel = workReportSearchVOs.stream()
                    .filter(each -> each.getSid().equals(wrSid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("追蹤資料比對有誤,無法開啟");
                return;
            }
            selWr_ID = wrSid;
            selWorkReportSearchVO = sel.get(0);
            DisplayController.getInstance().update(dataTableID);
            transTableComponent.loadDataDep(selWorkReportSearchVO.getSid(), selWorkReportSearchVO.getNo());
            DisplayController.getInstance().showPfWidgetVar("dlgForwardDep");
        } catch (Exception e) {
            log.error("clickTransDep", e);
        }
    }

    public void clickTransPerson(String wrSid) {
        try {
            List<WorkReportSearchVO> sel = workReportSearchVOs.stream()
                    .filter(each -> each.getSid().equals(wrSid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("追蹤資料比對有誤,無法開啟");
                return;
            }
            selWr_ID = wrSid;
            selWorkReportSearchVO = sel.get(0);
            DisplayController.getInstance().update(dataTableID);
            transTableComponent.loadDataPerson(selWorkReportSearchVO.getSid(), selWorkReportSearchVO.getNo());
            DisplayController.getInstance().showPfWidgetVar("dlgForwardPerson");
        } catch (Exception e) {
            log.error("clickTransDep", e);
        }
    }

    @Override
    public void openerByBtnUp() {
        try {
            // log.info("工作報告查詢進行上一筆動作");
            if (!workReportSearchVOs.isEmpty()) {
                toUp();
                if (selWorkReportSearchVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selWorkReportSearchVO.getSid());
                    settingReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);

            // if (selWorkReportSearchVO != null) {
            // itemsSize = String.valueOf(workReportSearchVOs.size());
            // newItemIx = String.valueOf(workReportSearchVOs.indexOf(selWorkReportSearchVO));
            // DisplayController.getInstance().update("hiddenSizeId");
            // DisplayController.getInstance().update("hiddenIxId");
            // DisplayController.getInstance().execute("getNewItemPos('" + dataTableID + "');");
            // }
            // log.info("工作報告進行上一筆動作-結束");
        } catch (Exception e) {
            log.error("工作報告進行上一筆動作-失敗", e);
        } finally {

            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByBtnDown() {
        try {
            // log.info("工作報告查詢進行下一筆動作");
            if (!workReportSearchVOs.isEmpty()) {
                toDown();
                if (selWorkReportSearchVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selWorkReportSearchVO.getSid());
                    settingReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
            // if (selWorkReportSearchVO != null) {
            // itemsSize = String.valueOf(workReportSearchVOs.size());
            // newItemIx = String.valueOf(workReportSearchVOs.indexOf(selWorkReportSearchVO));
            // DisplayController.getInstance().update("hiddenSizeId");
            // DisplayController.getInstance().update("hiddenIxId");
            // DisplayController.getInstance().execute("getNewItemPos('" + dataTableID + "');");
            // }
        } catch (Exception e) {
            log.error("工作報告查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByDelete() {
        throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void openerByInvalid() {
        doReloadInfo();
    }

    @Override
    public void openerByCommit() {
        doReloadInfo();
    }

    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    @Override
    public void openerByEditTag() {
        doReloadInfo();
    }

    @Override
    public void openerByTrace() {
        doReloadInfo();
    }

    @Override
    public void openerByFavorite() {
    }

    @Override
    public void openerByReadRecept() {
        doReloadInfo();
    }

    public final TraceUpdateCallBack traceUpdateCallBack = new TraceUpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 7504725213291111033L;

        @Override
        public void doUpdateData() {
            updateInfoToDataTable(selWr_ID);
            selWr_ID = "";
            DisplayController.getInstance().update(dataTableID);
        }
    };

    @Override
    public void openerByTrans() {
        doReloadInfo();
    }

    public final TransUpdateCallBack transUpdateCallBack = new TransUpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 8491660500950856400L;

        @Override
        public void doUpdateData() {
            updateInfoToDataTable(selWr_ID);
            selWr_ID = "";
            DisplayController.getInstance().update(dataTableID);
        }
    };

    /**
     * 新增自訂預設搜尋條件
     */
    public void saveCustomClause() {
        wrReportCustomColumnHelper.saveCustomClause(customClauseVO, SecurityFacade.getUserSid());
        workReportSearchHeaderComponent.replaceByCustomClause(customClauseVO);
        doSearch();
    }

    /**
     * 清除日期按鈕
     */
    public void clearDateBtnIndex() {
        customClauseVO.setDateTypeIndex(-1);
    }

    /**
     * 依照日期撈取不同，排序也不同
     */
    public void dateTimeTypeOnChange(String name) {
        if ("popup".equals(name)) {
            if (SqlColumnType.WT_UPDATE_DATE.equals(customClauseVO.getSqlColumnType())) {
                customClauseVO.setOrder("DESC");
            } else {
                customClauseVO.setOrder("ASC");
            }
        } else {
            if (SqlColumnType.WT_UPDATE_DATE.equals(workReportSearchHeaderComponent.getSqlColumnType())) {
                workReportSearchHeaderComponent.setOrder("DESC");
            } else {
                workReportSearchHeaderComponent.setOrder("ASC");
            }
        }
    }
}
