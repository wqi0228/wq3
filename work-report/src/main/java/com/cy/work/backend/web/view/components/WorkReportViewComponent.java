/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.beust.jcommander.internal.Sets;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.common.attachment.AttachmentCondition;
import com.cy.work.backend.web.common.attachment.WPAttachmentComponent;
import com.cy.work.backend.web.common.setting.WorkProjectTagItemSetting;
import com.cy.work.backend.web.listener.*;
import com.cy.work.backend.web.logic.components.*;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.AttachmentVO;
import com.cy.work.backend.web.util.ToolsList;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.enumtype.TabType;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserModifyVO;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRTraceVO;
import com.cy.work.backend.web.vo.WRMaster;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMemberTo;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.WREditType;
import com.cy.work.backend.web.vo.enums.WRStatus;
import com.cy.work.backend.web.vo.enums.WRTraceType;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.omnifaces.util.Faces;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author brain0925_liao
 */
@Slf4j
public class WorkReportViewComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3677227199810297594L;
    @Getter
    @Setter
    private WorkReportDataComponent workReportDataComponent;
    @Getter
    @Setter
    private WorkReportHeaderComponent workReportHeaderComponent;
    @Getter
    @Setter
    private WorkReportBtnComponent workReportBtnComponent;
    @Getter
    private WorkReportTraceComponent workReportTraceComponent;
    @Getter
    @Setter
    private Boolean edit = false;

    private String wrcId;

    private String wrcNo;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Getter
    private WPAttachmentComponent wpAttachmentCompant;
    @Getter
    private WRTraceVO replyEditVO;
    @Getter
    private WorkReportReplyComponent workReportReplyComponent;
    @Getter
    private boolean wrStatusDeleteOrInvail = false;
    @Getter
    private WorkReportAttachMaintainCompant workReportAttachMaintainCompant;
    @Getter
    private boolean showBottomInfoTab = false;
    @Getter
    @Setter
    private int tabIndex = 0;
    @Getter
    private WorkReportReadReceiptsMemberComponent workReportReadReceiptsMemberComponent;
    /**
     * 部門相關邏輯Component
     */
    @Getter
    private DepTreeComponent depTreeComponent;

    /**
     * Org tree 挑選部門
     */
    private List<Org> selectOrgs;

    private MessageCallBack messageCallBack;

    boolean showPreviousBtn = false;

    boolean showNextBtn = false;

    private PreviousAndNextCallBack previousAndNextCallBack;

    boolean showBackListBtn = false;
    @Getter
    private UserModifyVO userModifyVO;
    @Getter
    private WRTraceComponent traceComponent;
    @Getter
    private WRTransComponent transComponent;
    @Getter
    private WorkInboxComponent workInboxComponent;

    private TabType defaultTabType = null;
    @Getter
    private LockCountDownComponent lockCountDownComponent;

    public WorkReportViewComponent(UserViewVO userViewVO, OrgViewVo depViewVo,
            OrgViewVo compViewVo, MessageCallBack messageCallBack, PreviousAndNextCallBack previousAndNextCallBack) {
        this.userViewVO = userViewVO;
        this.depViewVo = depViewVo;
        this.compViewVo = compViewVo;
        this.traceComponent = new WRTraceComponent(userViewVO.getSid(), messageCallBack, traceUpdateCallBack);
        this.transComponent = new WRTransComponent(userViewVO.getSid(), compViewVo.getSid(), transUpdateCallBack, messageCallBack);
        this.messageCallBack = messageCallBack;
        this.previousAndNextCallBack = previousAndNextCallBack;
        workReportDataComponent = new WorkReportDataComponent();
        workReportHeaderComponent = new WorkReportHeaderComponent();
        workReportBtnComponent = new WorkReportBtnComponent(workReportButtonCallBack, userViewVO.getSid());
        workReportTraceComponent = new WorkReportTraceComponent();
        workReportReplyComponent = new WorkReportReplyComponent();
        wpAttachmentCompant = new WPAttachmentComponent(userViewVO.getSid(), depViewVo.getSid());
        replyEditVO = new WRTraceVO("", "", "", "", "", false);
        workReportAttachMaintainCompant = new WorkReportAttachMaintainCompant(userViewVO.getSid(), messageCallBack, workReportTabCallBack);
        workReportReadReceiptsMemberComponent = new WorkReportReadReceiptsMemberComponent(userViewVO.getSid(), messageCallBack, workReportTabCallBack);
        depTreeComponent = new DepTreeComponent();
        Org group = new Org(compViewVo.getSid());
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectOrgs = Lists.newArrayList();
        depTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
        this.workInboxComponent = new WorkInboxComponent(userViewVO.getSid(), transCancelUpdateCallBack, messageCallBack);
        this.lockCountDownComponent = new LockCountDownComponent();
    }

    public void loadWrcID(String tempWrcId, String encryptParam) {
        if (!Strings.isNullOrEmpty(tempWrcId)) {
            this.wrcId = tempWrcId;
        }
        if (!Strings.isNullOrEmpty(encryptParam)) {
            defaultTabType = WRTransSearchWorkReportLogicComponents.getInstance().getDefaultTabType(encryptParam, userViewVO.getSid());
            if (TabType.Send.equals(defaultTabType)) {
                this.workInboxComponent.openSendTab();
            }
        }
    }

    public void initialize(String tempWrcId, boolean showPreviousBtn, boolean showNextBtn, boolean showBackListBtn) {
        if (!Strings.isNullOrEmpty(tempWrcId)) {
            this.wrcId = tempWrcId;
        }
        this.showPreviousBtn = showPreviousBtn;
        this.showNextBtn = showNextBtn;
        this.showBackListBtn = showBackListBtn;
        if (Strings.isNullOrEmpty(this.wrcId)) {
            log.error("無取得WR_SID");
            DisplayController.getInstance().showPfWidgetVar("dlgIdleSession");
            return;
        }

        WRMaster wr = WRMasterLogicComponents.getInstance().getWRMasterBySid(this.wrcId);
        if (wr == null) {
            log.error("無取得工作報告主檔,故導致錯誤連結");
            reloadToIllegalPage(illegal_ulrPath);
            return;
        }

        boolean checkPermission = checkViewWorkReportPermission(wr);
        if (!checkPermission) {
            reloadToIllegalPage(illegal_readPath);
            return;
        }
        if (wr.getWr_status().equals(WRStatus.COMMIT) || wr.getWr_status().equals(WRStatus.INVIALD)) {
            WRReadLogicComponents.getInstance().readWRMaster(wrcId, userViewVO.getSid());
            WRInboxLogicComponents.getInstance().readWorkInBox(wrcId, userViewVO.getSid(), depViewVo.getSid());
        }

        boolean reuslt = initLoadView();
        if (!reuslt) {
            return;
        }
        tabIndex = 0;
        workReportTraceComponent.loadData(wrcId, userViewVO.getSid());
        workReportReplyComponent.loadData(wrcId, userViewVO.getSid());
        workInboxComponent.loadData(wrcId);
        AttachmentCondition ac = new AttachmentCondition(wrcId, wrcNo, false);
        wpAttachmentCompant.loadAttachment(ac);
        workReportAttachMaintainCompant.loadData(wrcId);
        workReportReadReceiptsMemberComponent.loadData(wrcId, wr.getCreate_usr());
        workReportTabCallBack.reloadTabView(defaultTabType);
    }

    private boolean initLoadView() {
        try {
            workReportBtnComponent.setEditMode(false);
            this.loadData(wrcId);
            // 若發現參數失效,將進行重新載入
            // if (wr == null) {
            // reloadToIllegalPage(illegal_ulrPath);
            // return false;
            // }
        } catch (Exception e) {
            log.error("initLoadView", e);
        }

        this.edit = false;
        DisplayController.getInstance().update("workReportBtnPanel");
        DisplayController.getInstance().update("workReportHeaderPanel");
        DisplayController.getInstance().update("workReportDataPanel");
        DisplayController.getInstance().update("userModifyPanel");
        return true;
    }

    private String illegal_readPath = "../error/illegal_read.xhtml";

    private String illegal_ulrPath = "../error/illegal_url.xhtml";

    private void reloadToIllegalPage(String path) {
        try {
            Faces.getExternalContext().redirect(path);
            Faces.getContext().responseComplete();
        } catch (Exception ex) {
            log.error("導向讀取失敗頁面失敗..." + ex.getMessage(), ex);
        }
    }

    /**
     * 檢查是否有權限查看該則工作報告
     *
     * @param wRMaster
     * @return
     */
    public boolean checkViewWorkReportPermission(WRMaster wRMaster) {
        if (wRMaster.getWr_status().equals(WRStatus.DELETE) || wRMaster.getWr_status().equals(WRStatus.DRAFT)) {
            // 廢棄or草稿
            Set<Integer> orgs = Sets.newHashSet();
            Org department = OrgLogicComponents.getInstance().findOrgBySid(depViewVo.getSid());
            // 僅部門主管可看草稿,該部門其他同仁的草稿
            // 多主管 : 僅部門主管可看草稿,該部門其他同仁的草稿
            if (ToolsList.isOneOfManagers(userViewVO.getSid(), department.getManagers())) {
                orgs.add(department.getSid());
            }
            // 該部門已下的部門所開的草稿也可看
            WkOrgCache.getInstance().getChildOrgs(department.getSid()).forEach(org -> {
                orgs.add(org.getSid());
            });
            // 兼任的部門主管也可查看兼任部門跟兼任部門底下部門的草稿
            List<Org> managerOrgs = WkOrgCache.getInstance().findManagerOrgs(userViewVO.getSid());
            managerOrgs.forEach(item -> {
                if (!orgs.contains(item.getSid())) {
                    orgs.add(item.getSid());
                    WkOrgCache.getInstance().getChildOrgs(item.getSid()).forEach(childOrg -> {
                        orgs.add(childOrg.getSid());
                    });
                }
            });

            if (!orgs.contains(wRMaster.getDep_sid()) && !wRMaster.getCreate_usr().equals(userViewVO.getSid())) {
                log.error("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + department.getName() + "]"
                        + "登入者:[" + userViewVO.getName() + "] 無權限觀看此草稿");
                reloadToIllegalPage(illegal_readPath);
                return false;
            }
        } else {
            if (userViewVO.getSid().equals(wRMaster.getCreate_usr())) {
                log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                        + "登入者:[" + userViewVO.getName() + "] 建立者故可檢閱");
                return true;
            }
            List<Org> limitOrg = WrBaseDepAccessInfoLogicComponents.getInstance().getLimitBaseAccessViewDep(depViewVo.getSid());
            List<User> limitUser = WrBasePersonAccessInfoLogicComponents.getInstance().getLimitBaseAccessViewUser(userViewVO.getSid());
            // 檢查限制可閱部門與限制可閱人員是否有值
            if (CollectionUtils.isEmpty(limitOrg)) {
                List<Org> defaultOrg = Lists.newArrayList();
                // 預設可看部門(最高為OrgLevel)，目前為OrgLevel.MINISTERIAL->同部，將來OrgLevel.THE_PANEL -> 同組
                Org baseOrg = WkOrgUtils.prepareBaseDep(depViewVo.getSid(), OrgLevel.THE_PANEL);
                defaultOrg.add(baseOrg);
                defaultOrg.addAll(OrgLogicComponents.getInstance().getAllChildOrg(baseOrg));

                List<Org> result = defaultOrg.stream()
                        .filter(each -> each.getSid().equals(wRMaster.getDep_sid()))
                        .collect(Collectors.toList());

                if (result != null && !result.isEmpty()) {
                    log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                            + "登入者:[" + userViewVO.getName() + "] 基礎部門故可檢閱");
                    return true;
                }

                // 兼任的部門主管有權限查看兼任部門與兼任部門底下部門的工作報告
                List<Org> managerOrgs = WkOrgCache.getInstance().findManagerOrgs(userViewVO.getSid());
                List<Org> canSeeOrgList = Lists.newArrayList();
                managerOrgs.forEach(org -> {
                    canSeeOrgList.add(org);
                    canSeeOrgList.addAll(OrgLogicComponents.getInstance().getAllChildOrg(org));
                });
                if (CollectionUtils.isNotEmpty(managerOrgs)) {
                    boolean deputyManagerCanSee = canSeeOrgList
                            .stream()
                            .filter(org -> org.getSid().equals(wRMaster.getDep_sid()))
                            .collect(Collectors.toList()).size() != 0;
                    if (deputyManagerCanSee) {
                        log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                                + "登入者:[" + userViewVO.getName() + "] 兼任的部門主管故可檢閱");
                        return true;
                    }
                }
            } else {
                // 檢測限制權限人員
                if (CollectionUtils.isNotEmpty(limitUser)) {
                    List<User> result = limitUser.stream()
                            .filter(each -> each.getSid().equals(wRMaster.getCreate_usr()))
                            .collect(Collectors.toList());
                    if (result != null && !result.isEmpty()) {
                        log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                                + "登入者:[" + userViewVO.getName() + "] 限制權限人員有該建立者,故可檢閱");
                        return true;
                    }
                }
                // 檢測限制權限部門
                if (CollectionUtils.isNotEmpty(limitOrg)) {
                    List<Org> result = limitOrg.stream()
                            .filter(each -> each.getSid().equals(wRMaster.getDep_sid()))
                            .collect(Collectors.toList());
                    if (result != null && !result.isEmpty()) {
                        log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                                + "登入者:[" + userViewVO.getName() + "] 限制權限部門有該建立部門,故可檢閱");
                        return true;
                    }
                }
            }

            List<Org> otherOrg = WrBaseDepAccessInfoLogicComponents.getInstance().getOtherBaseAccessViewDep(depViewVo.getSid());
            List<User> otherUser = WrBasePersonAccessInfoLogicComponents.getInstance().getOtherBaseAccessViewUser(userViewVO.getSid());
            List<Org> otherOrgResult = otherOrg.stream()
                    .filter(each -> each.getSid().equals(wRMaster.getDep_sid()))
                    .collect(Collectors.toList());
            if (otherOrgResult != null && !otherOrgResult.isEmpty()) {
                log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                        + "登入者:[" + userViewVO.getName() + "] 新增權限部門有該建立部門,故可檢閱");
                return true;
            }
            List<User> otherUserResult = otherUser.stream()
                    .filter(each -> each.getSid().equals(wRMaster.getCreate_usr()))
                    .collect(Collectors.toList());
            if (otherUserResult != null && !otherUserResult.isEmpty()) {
                log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                        + "登入者:[" + userViewVO.getName() + "] 新增權限人員有該建立者,故可檢閱");
                return true;
            }

            // 移除可閱覽單位判斷
            // List<Org> inquireReuslt = OrgLogicComponents.getInstance().findInquireDepByUserSid(userViewVO.getSid(),
            // compViewVo.getSid()).stream()
            // .filter(each -> each.getSid().equals(wRMaster.getDep_sid()))
            // .collect(Collectors.toList());
            //
            // if (inquireReuslt != null && !inquireReuslt.isEmpty()) {
            // return true;
            // }

            if (wRMaster.getRead_receipts_member() != null && wRMaster.getRead_receipts_member().getReadReceiptsMemberTo() != null
                    && !wRMaster.getRead_receipts_member().getReadReceiptsMemberTo().isEmpty()) {

                List<ReadReceiptsMemberTo> listResult = wRMaster.getRead_receipts_member().getReadReceiptsMemberTo().stream()
                        .filter(each -> each.getReader().equals(String.valueOf(userViewVO.getSid())))
                        .collect(Collectors.toList());
                if (listResult != null && !listResult.isEmpty()) {
                    log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                            + "登入者:[" + userViewVO.getName() + "] 該單有索取讀取記錄,故可檢閱");
                    return true;
                }
            }

            boolean hasTrace = WorkTraceInfoLogicComponents.getInstance().isHasTracePersonAll(userViewVO.getSid(), wRMaster.getSid());
            if (hasTrace) {
                log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                        + "登入者:[" + userViewVO.getName() + "] 該單有追蹤個人,故可檢閱");
                return true;
            }
            hasTrace = WorkTraceInfoLogicComponents.getInstance().isHasTraceDepAll(depViewVo.getSid(), wRMaster.getSid());
            if (hasTrace) {
                log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                        + "登入者:[" + userViewVO.getName() + "] 該單有追蹤部門,故可檢閱");
                return true;
            }

            boolean hasTrans = WRInboxLogicComponents.getInstance().isTransPersonAndDep(wrcId, userViewVO.getSid(), depViewVo.getSid());
            if (hasTrans) {
                log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                        + "登入者:[" + userViewVO.getName() + "] 該單有轉寄,故可檢閱");
                return true;
            }

            if ("eva".equals(SecurityFacade.getUserId())) {
                Set<Integer> managerWithChildOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(
                        SecurityFacade.getUserSid());

                if (managerWithChildOrgSids.contains(wRMaster.getDep_sid())) {
                    log.info("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                            + "登入者:[" + userViewVO.getName() + "]  eva 特殊可閱");
                    return true;
                }
            }

            log.error("工作報告單號:[" + wRMaster.getWr_no() + "]" + "登入者部門:[" + depViewVo.getName() + "]"
                    + "登入者:[" + userViewVO.getName() + "] 無權限觀看此工作報告");
            reloadToIllegalPage(illegal_readPath);
            return false;
        }
        return true;
    }

    private void loadTabIndex(TabType tabType) {
        if (tabType == null) {
            return;
        }
        int traceIndex = (workReportTraceComponent.isShowTraceTab()) ? 0 : -1;
        int replyIndex = traceIndex + (workReportReplyComponent.isShowReplyTab() ? 1 : 0);
        int attachIndex = replyIndex + (workReportAttachMaintainCompant.isShowAttTab() ? 1 : 0);
        int readReceiptIndex = attachIndex + (workReportReadReceiptsMemberComponent.isShowReadReceiptTab() ? 1 : 0);
        int incomeReportIndex = readReceiptIndex + (workInboxComponent.isShowIncomeReport() ? 1 : 0);
        int incomeMemberIndex = incomeReportIndex + (workInboxComponent.isShowIncomeMember() ? 1 : 0);
        int incomeInstructionIndex = incomeMemberIndex + (workInboxComponent.isShowIncomeInstructions() ? 1 : 0);
        int sendIndex = incomeInstructionIndex + (workInboxComponent.isShowSentBackup() ? 1 : 0);
        switch (tabType) {
        case Trace:
            tabIndex = traceIndex;
            break;
        case Reply:
            tabIndex = replyIndex;
            break;
        case Attachment:
            tabIndex = attachIndex;
            break;
        case ReadReceipt:
            tabIndex = readReceiptIndex;
            break;
        case IncomeReport:
            tabIndex = incomeReportIndex;
            break;
        case IncomeMember:
            tabIndex = incomeMemberIndex;
            break;
        case IncomeInstruction:
            tabIndex = incomeInstructionIndex;
            break;
        case Send:
            tabIndex = sendIndex;
            break;
        default:
            tabIndex = 0;
            break;
        }
        if (tabIndex < 0) {
            tabIndex = 0;
        }
    }

    public void loadDepTree() {
        Org group = new Org(compViewVo.getSid());
        // 修改,故切換顯示停用部門時,需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        List<Org> allOrg = OrgLogicComponents.getInstance().findOrgsByCompanySid(group.getSid());
        depTreeComponent.init(allOrg, allOrg,
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
        DisplayController.getInstance().update("dep");
        DisplayController.getInstance().showPfWidgetVar("deptUnitDlg");
    }

    public void doSelectOrg() {
        selectOrgs = depTreeComponent.getSelectOrg();
        workReportReadReceiptsMemberComponent.loadSelectDepartment(selectOrgs);
        DisplayController.getInstance().update("readReceiptPanel");
        DisplayController.getInstance().hidePfWidgetVar("deptUnitDlg");
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() { return depTreeComponent.getMultipleDepTreeManager(); }

    private void settingEditMode(WRMaster wRMaster) {
        if (wRMaster.getWr_status().equals(WRStatus.INVIALD)) {
            Preconditions.checkState(false, "該工作報告已作廢,不可編輯");
        }
        if (wRMaster.getWr_status().equals(WRStatus.DELETE)) {
            Preconditions.checkState(false, "該工作報告已刪除,不可編輯");
        }
        if (wRMaster.getWr_status().equals(WRStatus.COMMIT)) {
            lockCountDownComponent.findEffectiveTime(wRMaster.getLock_dt());
            DisplayController.getInstance().update("userModifyPanel");
            DisplayController.getInstance().execute("countDown('screen_fullissue_content_view_lockCount','" + lockCountDownComponent.getCountDownTime() + "','closeLock();');");
            if (wRMaster.getLock_usr() != null && wRMaster.getLock_dt() != null) {
                if (!wRMaster.getLock_usr().equals(userViewVO.getSid())) {
                    if (lockCountDownComponent.remainingTime(wRMaster.getLock_dt()) > 0) {
                        User lockUser = WRUserLogicComponents.getInstance().findUserBySid(wRMaster.getLock_usr());
                        lockCountDownComponent.setEditUserName(lockUser.getName());
                        Preconditions.checkState(false, "該工作報告已被[" + lockUser.getName() + "]鎖定,"
                                + "將於" + ToolsDate.transDateToString(SimpleDateFormatEnum.SdfTime.getValue(), wRMaster.getLock_dt()) + "解除鎖定");
                    }
                }
            }
            lockCountDownComponent.loadData(wRMaster.getSid(), userViewVO.getSid(), userViewVO.getName(), new Date());
        }
    }

    private WorkReportComponent loadData(String wrcId) {
        WorkReportComponent wc = WRMasterLogicComponents.getInstance().getWorkReportComponentBySid(wrcId, userViewVO.getSid());
        if (wc == null || wc.getWRMaster() == null) {
            return null;
        }
        // 若是編輯模式,檢測參數及編輯問題
        if (workReportBtnComponent.getEditMode()) {
            settingEditMode(wc.getWRMaster());
        }
        if (wc.getWRMaster().getWr_status().equals(WRStatus.DELETE) || wc.getWRMaster().getWr_status().equals(WRStatus.INVIALD)) {
            wrStatusDeleteOrInvail = true;
        } else {
            wrStatusDeleteOrInvail = false;
        }
        workReportDataComponent = wc.getWorkReportDataComponent();
        log.info("workReportDataComponent.getTitle():" + workReportDataComponent.getTitle());
        workReportHeaderComponent = wc.getWorkReportHeaderComponent();
        workReportBtnComponent.loadButtonAble(wc.getWRMaster(), showPreviousBtn, showNextBtn, showBackListBtn);
        wrcNo = wc.getWRMaster().getWr_no();
        try {
            UserViewVO modifyUser = WRUserLogicComponents.getInstance()
                    .findBySid((wc.getWRMaster().getUpdate_usr() != null) ? wc.getWRMaster().getUpdate_usr() : wc.getWRMaster().getCreate_usr());
            String modifyTimeStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wc.getWRMaster().getUpdate_dt());
            userModifyVO = new UserModifyVO(modifyUser.getName(), modifyTimeStr);
        } catch (Exception e) {
            log.error("loadData Error", e);
        }
        return wc;
    }

    public void doReply() {
        try {
            WRMaster wr = WRMasterLogicComponents.getInstance().replyWRMaster(wrcId, userViewVO.getSid(), replyEditVO);
            if (wpAttachmentCompant.getSelectedAttachmentVOs() != null && wpAttachmentCompant.getSelectedAttachmentVOs().size() > 0) {
                wpAttachmentCompant.getSelectedAttachmentVOs().forEach(item -> {
                    WRAttachmentLogicComponents.getInstance().updateWRMasterMappingInfo(wr.getSid(), wr.getWr_no(), item.getAttDesc(), userViewVO.getSid(),
                            item.getAttSid());
                });
            }
            workReportTabCallBack.reloadTraceTab();
            workReportTabCallBack.reloadReplyTab();
            workReportTabCallBack.reloadAttTab();
            workReportTabCallBack.reloadTabView(TabType.Reply);
            DisplayController.getInstance().hidePfWidgetVar("reply_dialog");
        } catch (Exception e) {
            log.error("doReply Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 主題欄位增加預設主題
     */
    public void addSampleTitle() {
        try {
            if (Strings.isNullOrEmpty(workReportHeaderComponent.getTagID())) {
                Preconditions.checkState(false, "標籤尚未選擇，請確認！");
            }
            if (Strings.isNullOrEmpty(workReportDataComponent.getTitle())) {
                doAddSampleTitle();
            } else {
                DisplayController.getInstance().showPfWidgetVar("sampleTitleAlertDlgWv");
            }
        } catch (Exception e) {
            log.error("addSampleTitle Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void doAddSampleTitle() {
        try {
            if (Strings.isNullOrEmpty(workReportHeaderComponent.getTagID())) {
                Preconditions.checkState(false, "標籤尚未選擇，請確認！");
            }
            String tagName = WorkProjectTagItemSetting.getWorkProjectTagName(workReportHeaderComponent.getTagID(), compViewVo.getSid());
            String sampleTitle = userViewVO.getName() + "　[" + tagName + "]　" + ToolsDate.transDateToChineseString(new Date());
            workReportDataComponent.setTitle(sampleTitle);
            DisplayController.getInstance().hidePfWidgetVar("sampleTitleAlertDlgWv");
        } catch (Exception e) {
            log.error("doAddSampleTitle Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void loadReply(String traceID) {
        try {
            replyEditVO = WRTraceLogicComponents.getInstance().getWRTraceVOBySid(userViewVO.getSid(), traceID);
            int index = workReportReplyComponent.getWRTraceVO().indexOf(replyEditVO);
            DisplayController.getInstance().execute("PF('wr_replytrace_tab_wv').select('" + String.valueOf(index) + "');");
            AttachmentCondition ac = new AttachmentCondition(wrcId, wrcNo, false);
            wpAttachmentCompant.loadAttachment(ac);
            DisplayController.getInstance().showPfWidgetVar("reply_dialog");
            DisplayController.getInstance().update("reply_dialog");
            DisplayController.getInstance().execute("initReinstatedDlg('reply_dialog','reply_dialog_edit',150,170);");
        } catch (Exception e) {
            log.error("loadReply Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private final WorkReportButtonCallBack workReportButtonCallBack = new WorkReportButtonCallBack() {
        /**
         *
         */
        private static final long serialVersionUID = -2790484315191821124L;

        @Override
        public void clickEdit() {
            try {
                // 輸入框改為編輯模式
                workReportBtnComponent.setEditMode(true);
                if (Strings.isNullOrEmpty(wrcId)) {
                    wrcId = Faces.getRequestParameterMap().get("wrcId");
                }
                WorkReportComponent data = loadData(wrcId);
                Preconditions.checkState(data != null, "載入工作報告失敗！！");
                // else {
                // //將header變可編輯
                // edit = false;
                // }
                DisplayController.getInstance().update("workReportBtnPanel");
                DisplayController.getInstance().update("workReportHeaderPanel");
                DisplayController.getInstance().update("workReportDataPanel");
            } catch (Exception e) {
                log.error("clickEdit Error", e,"、","wrcId:",wrcId);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        @Override
        public void clickTrans() {
            transComponent.loadData(wrcId, wrcNo);
            DisplayController.getInstance().showPfWidgetVar("dlgForward");
        }

        @Override
        public void clickTrace() {
            traceComponent.loadData(wrcId, wrcNo, workReportDataComponent.getTitle());
            DisplayController.getInstance().showPfWidgetVar("dlgTraceAction");
            DisplayController.getInstance().update("dlgTraceAction_view");
        }

        @Override
        public void clickReply() {
            String dateTime = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date());
            String userInfo = depViewVo.getName() + "-" + userViewVO.getName();
            replyEditVO = new WRTraceVO("", WRTraceType.REPLY.getVal(), userInfo, dateTime, "", false);
            AttachmentCondition ac = new AttachmentCondition(wrcId, wrcNo, false);
            wpAttachmentCompant.loadAttachment(ac);
            DisplayController.getInstance().showPfWidgetVar("reply_dialog");
            DisplayController.getInstance().update("reply_dialog");
            DisplayController.getInstance().execute("initReinstatedDlg('reply_dialog','reply_dialog_edit',150,170);");
        }

        @Override
        public void clickTag() {
            String selectTag = workReportBtnComponent.getSelectTagID();
            try {
                WRMasterLogicComponents.getInstance().saveTag(wrcId, selectTag, userViewVO.getSid(), depViewVo.getSid(), compViewVo.getSid());
                boolean reuslt = initLoadView();
                if (!reuslt) {
                    return;
                }
                workReportTabCallBack.reloadTraceTab();
                workReportTabCallBack.reloadTabView(TabType.Trace);
                DisplayController.getInstance().execute("doEditTagToUpdate('" + wrcId + "')");
            } catch (Exception e) {
                log.error("clickTag Error", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        @Override
        public void clickSummit() {
            try {
                WRMaster wr = WRMasterLogicComponents.getInstance().getWRMasterBySid(wrcId);
                if (wr.getEdit_type().equals(WREditType.PERSONAL)) {
                    DisplayController.getInstance().showPfWidgetVar("commitAlertDlgWv");
                } else {
                    clickDoSummit();
                }
            } catch (Exception e) {
                log.error("clickSummit Error", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        @Override
        public void clickDoSummit() {
            try {
                WRMasterLogicComponents.getInstance().summitWRMaster(wrcId, userViewVO.getSid(), depViewVo.getSid(), compViewVo.getSid());
                boolean reuslt = initLoadView();
                if (!reuslt) {
                    return;
                }
                workReportTabCallBack.reloadTraceTab();
                workReportTabCallBack.reloadTabView(TabType.Trace);
                DisplayController.getInstance().execute("doCommitToUpdate('" + wrcId + "')");
            } catch (Exception e) {
                log.error("clickDoSummit Error", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        @Override
        public void clickSend() {
            try {
                WRMasterLogicComponents.getInstance().updateWRMaster(wrcId, workReportDataComponent, userViewVO.getSid());
                lockCountDownComponent.cancelLock(wrcId, userViewVO.getSid());
                boolean reuslt = initLoadView();
                if (!reuslt) {
                    return;
                }
                workReportTabCallBack.reloadTraceTab();
                workReportTabCallBack.reloadTabView(TabType.Trace);
                DisplayController.getInstance().execute("doEditContentToUpdate('" + wrcId + "')");
            } catch (Exception e) {
                log.error("clickSend Error", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        @Override
        public void clickCancel() {
            try {
                lockCountDownComponent.cancelLock(wrcId, userViewVO.getSid());
                boolean reuslt = initLoadView();
                if (!reuslt) {
                    return;
                }
            } catch (Exception e) {
                log.error("clickCancel Error", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        @Override
        public void clickAtt() {
            AttachmentCondition ac = new AttachmentCondition(wrcId, wrcNo, true);
            ac.setUploadAttCallBack(uploadAttCallBack);
            wpAttachmentCompant.loadAttachment(ac);
            DisplayController.getInstance().showPfWidgetVar("dlgUploadWv");
            DisplayController.getInstance().execute("updateAthUploadMsg()");
        }

        @Override
        public void clickPrevious() {
            previousAndNextCallBack.doPrevious();
        }

        @Override
        public void clickNext() {
            previousAndNextCallBack.doNext();
        }

        @Override
        public void clickBackList() {
            throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void clickInvalid() {
            try {
                if (Strings.isNullOrEmpty(workReportBtnComponent.getInvalidReason())) {
                    Preconditions.checkState(false, "作廢原因不可空白,請確認！");
                }

                WRMasterLogicComponents.getInstance().invaildWRMaster(wrcId, workReportBtnComponent.getInvalidReason(), userViewVO.getSid());
                workReportTabCallBack.reloadWrMasterData();
                workReportTabCallBack.reloadTraceTab();
                workReportTabCallBack.reloadAttTab();
                workReportTabCallBack.reloadReplyTab();
                workReportTabCallBack.reloadTabView(TabType.Trace);
                DisplayController.getInstance().execute("doInvalidToUpdate('" + wrcId + "')");
            } catch (Exception e) {
                log.error("clickInvalid Error", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        @Override
        public void clickDelete() {
            try {
                WRMasterLogicComponents.getInstance().deleteWRMaster(wrcId, userViewVO.getSid());
                workReportTabCallBack.reloadWrMasterData();
                workReportTabCallBack.reloadTraceTab();
                workReportTabCallBack.reloadAttTab();
                workReportTabCallBack.reloadReplyTab();
                workReportTabCallBack.reloadTabView(TabType.Trace);
                DisplayController.getInstance().execute("doDeleteToUpdate('" + wrcId + "')");
            } catch (Exception e) {
                log.error("clickDelete Error", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        @Override
        public void clickEditType() {
            String selectEditType = workReportBtnComponent.getSelectEditTypeID();
            try {
                WRMasterLogicComponents.getInstance().saveEditType(wrcId, selectEditType, userViewVO.getSid());
                boolean reuslt = initLoadView();
                if (!reuslt) {
                    return;
                }
                workReportTabCallBack.reloadTraceTab();
                workReportTabCallBack.reloadTabView(TabType.Trace);
            } catch (Exception e) {
                log.error("clickEditType Error", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        @Override
        public void clickReadReceipt() {
            selectOrgs = Lists.newArrayList();
            workReportReadReceiptsMemberComponent.loadUserPicker(wrcId);
            DisplayController.getInstance().update("readReceiptPanel");
            DisplayController.getInstance().showPfWidgetVar("readReceiptDlg");
        }

        @Override
        public void clickFavorite() {
            try {
                if (workReportBtnComponent.isFavorite()) {
                    WRFavoriteLogicComponents.getInstance().removeFavorite(wrcId, wrcNo, userViewVO.getSid());
                } else {
                    WRFavoriteLogicComponents.getInstance().addFavorite(wrcId, wrcNo, userViewVO.getSid());
                }
                workReportTabCallBack.reloadWrMasterData();
                DisplayController.getInstance().execute("doFavoriteToUpdate('" + wrcId + "')");
            } catch (Exception e) {
                log.error("clickFavorite Error", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }
    };

    @Getter
    public final UploadAttCallBack uploadAttCallBack = new UploadAttCallBack() {
        /**
         *
         */
        private static final long serialVersionUID = 4877845957123090624L;

        @Override
        public void doUploadAtt(AttachmentVO att) {
            workReportTabCallBack.reloadAttTab();
            workReportTabCallBack.reloadTabView(TabType.Attachment);
        }
    };

    public final TraceUpdateCallBack traceUpdateCallBack = new TraceUpdateCallBack() {
        /**
         *
         */
        private static final long serialVersionUID = -3307041767032850993L;

        @Override
        public void doUpdateData() {
            workReportTabCallBack.reloadWrMasterData();
            DisplayController.getInstance().execute("doTraceToUpdate('" + wrcId + "')");
        }
    };

    public final TransUpdateCallBack transUpdateCallBack = new TransUpdateCallBack() {
        /**
         *
         */
        private static final long serialVersionUID = 7193090840786196809L;

        @Override
        public void doUpdateData() {
            workInboxComponent.loadData();
            workReportTabCallBack.reloadTabView(null);
            WRInboxLogicComponents.getInstance().readWorkInBox(wrcId, userViewVO.getSid(), depViewVo.getSid());
            DisplayController.getInstance().execute("doTransToUpdate('" + wrcId + "')");
        }
    };

    private final TransCancelUpdateCallBack transCancelUpdateCallBack = new TransCancelUpdateCallBack() {
        /**
         *
         */
        private static final long serialVersionUID = -84419802541410781L;

        @Override
        public void doUpdateData() {
            workInboxComponent.loadData();
            workReportTabCallBack.reloadTabView(null);
            DisplayController.getInstance().execute("doTransToUpdate('" + wrcId + "')");
        }
    };

    public final WorkReportTabCallBack workReportTabCallBack = new WorkReportTabCallBack() {
        /**
         *
         */
        private static final long serialVersionUID = -5752079139081219474L;

        @Override
        public void reloadTraceTab() {
            workReportTraceComponent.loadData();
        }

        @Override
        public void reloadReplyTab() {
            workReportReplyComponent.loadData();
        }

        @Override
        public void reloadAttTab() {
            workReportAttachMaintainCompant.loadData();
        }

        @Override
        public void reloadTabView(TabType tabType) {
            loadTabIndex(tabType);
            showBottomInfoTab = (workReportTraceComponent.isShowTraceTab()
                    || workReportReplyComponent.isShowReplyTab()
                    || workReportAttachMaintainCompant.isShowAttTab()
                    || workReportReadReceiptsMemberComponent.isShowReadReceiptTab()
                    || workInboxComponent.isShowIncomeInstructions()
                    || workInboxComponent.isShowIncomeMember()
                    || workInboxComponent.isShowIncomeReport()
                    || workInboxComponent.isShowSentBackup());
            DisplayController.getInstance().update("viewPanelBottomInfoTabId");
        }

        @Override
        public void reloadWrMasterData() {
            boolean reuslt = initLoadView();
            if (!reuslt) {
                return;
            }
        }

    };

}
