/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.common.attachment.AttachmentComponent;
import com.cy.work.backend.web.common.attachment.AttachmentCondition;
import com.cy.work.backend.web.common.attachment.WPAttachmentComponent;
import com.cy.work.backend.web.common.setting.WorkProjectTagItemSetting;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRMasterLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.WorkReportDataComponent;
import com.cy.work.backend.web.view.components.WorkReportHeaderComponent;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.vo.WRMaster;
import com.cy.work.backend.web.vo.enums.WREditType;
import com.cy.work.backend.web.vo.enums.WRStatus;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Worp1Bean implements Serializable {

    
    private static final long serialVersionUID = 2408024711027739390L;
    @Autowired
    private WRUserLogicComponents wRUserLogicComponents;
    @Autowired
    private OrgLogicComponents orgLogicComponents;
    @Autowired
    private DisplayController displayController;
    @Autowired
    private WRMasterLogicComponents wRMasterLogicComponents;
    @Getter
    @Setter
    private WorkReportDataComponent workReportDataComponent;
    @Getter
    @Setter
    private WorkReportHeaderComponent workReportHeaderComponent;
    @Getter
    @Setter
    private List<AttachmentComponent> attachmentComponents;
    @Getter
    private WPAttachmentComponent wpAttachmentCompant;
    @Getter
    private String errorMessage;
    @Getter
    @Setter
    private Boolean edit = true;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;

    private boolean canCrreateing = true;

    //private WkJsonUtils jsonUtils;
    @PostConstruct
    public void init() {
        userViewVO = wRUserLogicComponents.findBySid(SecurityFacade.getUserSid());
        depViewVo = orgLogicComponents.findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = orgLogicComponents.findById(SecurityFacade.getCompanyId());
        workReportDataComponent = new WorkReportDataComponent();
        workReportHeaderComponent = new WorkReportHeaderComponent();
        workReportHeaderComponent.setCreateUserName(userViewVO.getName());
        workReportHeaderComponent.setDepartmentName(depViewVo.getName());
        wpAttachmentCompant = new WPAttachmentComponent(userViewVO.getSid(), depViewVo.getSid());
        AttachmentCondition ac = new AttachmentCondition("", "", false);
        wpAttachmentCompant.loadAttachment(ac);
        attachmentComponents = Lists.newArrayList();

    }

    /** 主題欄位增加預設主題 */
    public void addSampleTitle() {
        try {
            if (Strings.isNullOrEmpty(workReportHeaderComponent.getTagID())) {
                Preconditions.checkState(false, "標籤尚未選擇，請確認！");
            }
            if (Strings.isNullOrEmpty(workReportDataComponent.getTitle())) {
                doAddSampleTitle();
            } else {
                DisplayController.getInstance().showPfWidgetVar("sampleTitleAlertDlgWv");
            }
        } catch (Exception e) {
            log.error("addSampleTitle Error", e);
            showMessage(e.getMessage());
        }
    }

    public void doAddSampleTitle() {
        try {
            if (Strings.isNullOrEmpty(workReportHeaderComponent.getTagID())) {
                Preconditions.checkState(false, "標籤尚未選擇，請確認！");
            }
            String tagName = WorkProjectTagItemSetting.getWorkProjectTagName(workReportHeaderComponent.getTagID(), compViewVo.getSid());
            String sampleTitle = userViewVO.getName() + "　[" + tagName + "]　" + ToolsDate.transDateToChineseString(new Date());
            workReportDataComponent.setTitle(sampleTitle);
            DisplayController.getInstance().hidePfWidgetVar("sampleTitleAlertDlgWv");
        } catch (Exception e) {
            log.error("doAddSampleTitle Error", e);
            showMessage(e.getMessage());
        }
    }

    public void doCreateWorkReport() {
        if (!this.canCrreateing) {
            return;
        }

        try {
            this.canCrreateing = false;
            WRMaster wr = wRMasterLogicComponents.createWRMaster(WRStatus.COMMIT, workReportDataComponent,
                    workReportHeaderComponent, wpAttachmentCompant.getSelectedAttachmentVOs(), userViewVO.getSid(), depViewVo.getSid(), compViewVo.getSid());
            DisplayController.getInstance().hidePfWidgetVar("commitAlertDlgWv");
            String url = "../worp/worp_iframe.xhtml?wrcId=" + wr.getSid();
            //log.info("ReLoad View " + url);
            Faces.getExternalContext().redirect(url);
        } catch (Exception e) {
            this.canCrreateing = true;
            log.error("createWorkReport", e);
            showMessage(e.getMessage());
        }
    }

    public void createWorkReport() {
        try {
            if (WREditType.PERSONAL.name().equals(workReportHeaderComponent.getAcceptEditID())) {
                DisplayController.getInstance().showPfWidgetVar("commitAlertDlgWv");
            } else {
                doCreateWorkReport();
            }
        } catch (Exception e) {
            log.error("createWorkReport", e);
            showMessage(e.getMessage());
        }
    }

    public void cancel() {
        try {
            String url = "../worp/worp1.xhtml";
            //log.info("ReLoad View " + url);
            Faces.getExternalContext().redirect(url);
        } catch (Exception e) {
            log.error("cancel", e);
            showMessage(e.getMessage());
        }
    }

    public void createTempWorkReport() {
        if (!this.canCrreateing) {
            return;
        }
        try {

            this.canCrreateing = false;
            WRMaster wr = wRMasterLogicComponents.createWRMaster(WRStatus.DRAFT, workReportDataComponent,
                    workReportHeaderComponent, wpAttachmentCompant.getSelectedAttachmentVOs(),
                    userViewVO.getSid(), depViewVo.getSid(),
                    compViewVo.getSid());
            String url = "../worp/worp_iframe.xhtml?wrcId=" + wr.getSid();
            //log.info("ReLoad View " + url);
            Faces.getExternalContext().redirect(url);

        } catch (Exception e) {
            this.canCrreateing = true;
            log.error("createTempWorkReport", e);
            showMessage(e.getMessage());
        }
    }

    /**
     * 顯示錯誤訊息及彈出Dialog
     *
     * @param message
     */
    private void showMessage(String message) {
        this.errorMessage = message;
        displayController.update("confirmDlgTemplate");
        displayController.showPfWidgetVar("confirmDlgTemplate");
    }

}
