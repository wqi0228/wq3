/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.common.ExportExcelComponent;
import com.cy.work.backend.web.listener.*;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRReportCustomColumnLogicComponents;
import com.cy.work.backend.web.logic.components.WRTransSearchWorkReportLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.manager.WrReadRecordManager;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.CustomColumnComponent;
import com.cy.work.backend.web.view.components.WRTraceComponent;
import com.cy.work.backend.web.view.components.WRTransTableComponent;
import com.cy.work.backend.web.view.components.WorkReportTransSearchComponent;
import com.cy.work.backend.web.view.vo.*;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.enums.WRIncomeTransSearchColumn;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Search8Bean implements Serializable, TableUpDownListener {


    private static final long serialVersionUID = -2516778520231914728L;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Getter
    private WorkReportTransSearchComponent workReportTransSearchComponent;
    @Getter
    private CustomColumnComponent customColumn;
    /**
     * view 顯示錯誤訊息
     */
    @Getter
    private String errorMessage;
    @Getter
    private final String dataTableID = "dataTableIncomeMember";
    @Getter
    private final String dataTableWv = "dataTableIncomeMemberWv";
    @Setter
    @Getter
    private boolean showFrame = false;
    @Getter
    private String iframeUrl = "";
    @Autowired
    private WRReportCustomColumnLogicComponents wrReportCustomColumnLogicComponents;
    @Getter
    private WRTransIncomeSearchColumnVO searchColumnVO;
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Getter
    private List<WRTransIncomeVO> transIncomeVOs;
    @Setter
    @Getter
    private WRTransIncomeVO selWRTransIncomeVO;

    private List<WRTransIncomeVO> tempWRTransIncomeVOs;

    private String tempSelSid;
    @Autowired
    private WRTransSearchWorkReportLogicComponents wRTransSearchWorkReportLogicComponents;
    @Getter
    private WRTraceComponent traceComponent;

    private String selWr_ID;
    @Getter
    private WRTransTableComponent transTableComponent;
    @Autowired
    private ExportExcelComponent exportExcelComponent;
    @Autowired
    private WrReadRecordManager wrReadRecordManager;
    @Getter
    private boolean hasDisplay = true;

    @PostConstruct
    public void init() {
        userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        depViewVo = OrgLogicComponents.getInstance().findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = OrgLogicComponents.getInstance().findById(SecurityFacade.getCompanyId());
        this.traceComponent = new WRTraceComponent(userViewVO.getSid(), messageCallBack, traceUpdateCallBack);
        this.transTableComponent = new WRTransTableComponent(userViewVO.getSid(), compViewVo.getSid(), transUpdateCallBack, messageCallBack);
        searchColumnVO = wrReportCustomColumnLogicComponents.getWRTransIncomeMemeberSearchColumnSetting(userViewVO.getSid());
        boolean showSelectBaseOrg = false;
        String personTitleName = "寄發者";
        String selectAllOrgTitleName = "寄發單位";
        boolean showSelectTransType = false;
        workReportTransSearchComponent = new WorkReportTransSearchComponent(compViewVo.getSid(), userViewVO.getSid(), showSelectBaseOrg, personTitleName, selectAllOrgTitleName, showSelectTransType);
        settingDefault();
        customColumn = new CustomColumnComponent(WRReportCustomColumnUrlType.INCOME_MEMBER_SEARCH, Arrays.asList(WRIncomeTransSearchColumn.values()), searchColumnVO.getPageCount(), userViewVO.getSid(), messageCallBack, dataTableReLoadCallBack);
        doSearch();
    }

    public void clear() {
        settingDefault();
        doSearch();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        try {
            exportExcelComponent.exportExcel(document, null,
                    null, "收個人", false);
        } catch (Exception e) {
            log.error("exportExcel", e);
        } finally {
            hasDisplay = true;
        }
    }

    public void hideColumnContent() {
        hasDisplay = false;
    }

    public void settingDefault() {
        workReportTransSearchComponent.settingDefault();
        workReportTransSearchComponent.getWorkReportSearchHeaderComponent().changeDateIntervalToday();
    }

    public void doSearch() {
        try {
            transIncomeVOs = runSearch("");
            if (transIncomeVOs.size() > 0) {
                DisplayController.getInstance().execute("selectDataTablePage('" + dataTableWv + "',0);");
            }
        } catch (Exception e) {
            log.error("doSearch", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private List<WRTransIncomeVO> runSearch(String wr_id) {
        List<Integer> selectAllOrgSids = Lists.newArrayList();
        workReportTransSearchComponent.getSelectAllOrgs().forEach(item -> {
            selectAllOrgSids.add(item.getSid());
        });
        // return Lists.newArrayList();
        return wRTransSearchWorkReportLogicComponents.getWRTransIncomeMember(
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getSelectTag(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getReadStatus(),
                selectAllOrgSids,
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getPersonName(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getTitle(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getContent(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getAllSearchText(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getStartDate(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getEndDate(),
                userViewVO.getSid(),
                depViewVo.getSid(),
                wr_id, compViewVo.getSid()
        );
    }

    public void clickTransDep(String sid) {
        try {
            List<WRTransIncomeVO> sel = transIncomeVOs.stream()
                    .filter(each -> each.getSid().equals(sid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("轉寄部門資料比對有誤,無法開啟");
                return;
            }
            selWRTransIncomeVO = sel.get(0);
            selWr_ID = selWRTransIncomeVO.getWr_id();
            DisplayController.getInstance().update(dataTableID);
            transTableComponent.loadDataDep(selWRTransIncomeVO.getWr_id(), selWRTransIncomeVO.getNo());
            DisplayController.getInstance().showPfWidgetVar("dlgForwardDep");
        } catch (Exception e) {
            log.error("clickTransDep", e);
        }
    }

    public void clickTransPerson(String sid) {
        try {
            List<WRTransIncomeVO> sel = transIncomeVOs.stream()
                    .filter(each -> each.getSid().equals(sid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("轉寄個人資料比對有誤,無法開啟");
                return;
            }

            selWRTransIncomeVO = sel.get(0);
            selWr_ID = selWRTransIncomeVO.getWr_id();
            DisplayController.getInstance().update(dataTableID);
            transTableComponent.loadDataPerson(selWRTransIncomeVO.getWr_id(), selWRTransIncomeVO.getNo());
            DisplayController.getInstance().showPfWidgetVar("dlgForwardPerson");
        } catch (Exception e) {
            log.error("clickTransDep", e);
        }
    }

    public void clickTrace(String sid) {
        try {
            List<WRTransIncomeVO> sel = transIncomeVOs.stream()
                    .filter(each -> each.getSid().equals(sid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("轉寄個人資料比對有誤,無法開啟");
                return;
            }

            selWRTransIncomeVO = sel.get(0);
            selWr_ID = selWRTransIncomeVO.getWr_id();
            DisplayController.getInstance().update(dataTableID);
            traceComponent.loadData(selWRTransIncomeVO.getWr_id(), selWRTransIncomeVO.getNo(), selWRTransIncomeVO.getTitle());
            DisplayController.getInstance().showPfWidgetVar("dlgTraceAction");
            DisplayController.getInstance().update("dlgTraceAction_view");
        } catch (Exception e) {
            log.error("clickTrace", e);
        }
    }

    public void openCustomColumn() {
        try {
            customColumn.loadData();
            DisplayController.getInstance().update("dlgDefineField_view");
            DisplayController.getInstance().showPfWidgetVar("dlgDefineField");
        } catch (Exception e) {
            log.error("openCustomColumn", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private final MessageCallBack messageCallBack = new MessageCallBack() {

        private static final long serialVersionUID = 8059233260624825956L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    };

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {

        private static final long serialVersionUID = -866447665254492035L;

        @Override
        public void reload() {
            searchColumnVO = wrReportCustomColumnLogicComponents.getWRTransIncomeMemeberSearchColumnSetting(userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };

    private void toUp() {
        try {
            if (tempWRTransIncomeVOs != null && !tempWRTransIncomeVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempWRTransIncomeVOs.indexOf(new WRTransIncomeVO(tempSelSid));
                if (index > 0) {
                    selWRTransIncomeVO = tempWRTransIncomeVOs.get(index - 1);
                } else {
                    selWRTransIncomeVO = transIncomeVOs.get(0);
                }
                tempWRTransIncomeVOs = null;
                tempSelSid = "";
            } else {
                int index = transIncomeVOs.indexOf(selWRTransIncomeVO);
                if (index > 0) {
                    selWRTransIncomeVO = transIncomeVOs.get(index - 1);
                } else {
                    selWRTransIncomeVO = transIncomeVOs.get(0);
                }
            }
        } catch (Exception e) {
            log.error("toUp", e);
            tempSelSid = "";
            tempWRTransIncomeVOs = null;
            if (!transIncomeVOs.isEmpty()) {
                selWRTransIncomeVO = transIncomeVOs.get(0);
            }
        }
    }

    private void toDown() {
        try {
            if (tempWRTransIncomeVOs != null && !tempWRTransIncomeVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempWRTransIncomeVOs.indexOf(new WRTransIncomeVO(tempSelSid));
                if (index >= 0) {
                    selWRTransIncomeVO = tempWRTransIncomeVOs.get(index + 1);
                } else {
                    selWRTransIncomeVO = transIncomeVOs.get(transIncomeVOs.size() - 1);
                }
                tempWRTransIncomeVOs = null;
                tempSelSid = "";
            } else {
                int index = transIncomeVOs.indexOf(selWRTransIncomeVO);
                if (index >= 0) {
                    selWRTransIncomeVO = transIncomeVOs.get(index + 1);
                } else {
                    selWRTransIncomeVO = transIncomeVOs.get(transIncomeVOs.size() - 1);
                }
            }
        } catch (Exception e) {
            log.error("toDown", e);
            tempWRTransIncomeVOs = null;
            tempSelSid = "";
            if (transIncomeVOs.size() > 0) {
                selWRTransIncomeVO = transIncomeVOs.get(transIncomeVOs.size() - 1);
            }
        }
    }

    public void btnOpenUrl(String sid) {
        try {
            settingSelTransReceviceDepVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("btnOpenUrl", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            settingSelTransReceviceDepVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp_iframe.xhtml?wrcId=" + selWRTransIncomeVO.getWr_id() + "&encryptParam=" + selWRTransIncomeVO.getEncryptParam();
            tableUpDownBean.setWorp_path("worp_iframe");
        } catch (Exception e) {
            log.error("btnOpenFrame", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void settingReaded() {
        transIncomeVOs.forEach(item -> {
            if (selWRTransIncomeVO.getWr_id().equals(item.getWr_id())) {
                selWRTransIncomeVO.replaceReadStatus(WRReadStatus.HASREAD);
                item.replaceReadStatus(WRReadStatus.HASREAD);

                Optional<WrReadRecord> wrReadRecordOptional = wrReadRecordManager.findByUserSidAndWrSid(SecurityFacade.getUserSid().toString(), item.getWr_id());
                if (wrReadRecordOptional.isPresent()) {
                    WrReadRecord wrReadRecord = wrReadRecordOptional.get();
                    wrReadRecord.setReadStatus(WRReadStatus.HASREAD.name());
                    wrReadRecordManager.saveWrReadRecord(wrReadRecord);
                }
            }
        });
    }

    private void settingSelTransReceviceDepVO(String sid) {
        WRTransIncomeVO sel = new WRTransIncomeVO(sid);
        int index = transIncomeVOs.indexOf(sel);
        if (index > 0) {
            selWRTransIncomeVO = transIncomeVOs.get(index);
        } else {
            selWRTransIncomeVO = transIncomeVOs.get(0);
        }
        settingReaded();
    }

    public void settingPreviousAndNext() {
        int index = transIncomeVOs.indexOf(selWRTransIncomeVO);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(transIncomeVOs.get(index - 1).getWr_id());
        }
        if (index == transIncomeVOs.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(transIncomeVOs.get(index + 1).getWr_id());
        }
    }

    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.error("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        WkJsonUtils jsonUtils = SpringContextHolder.getBean(WkJsonUtils.class);
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.error("doReloadInfo", ex);
        }
    }

    private void updateInfoToDataTable(String sid) {
        try {
            List<WRTransIncomeVO> result = runSearch(sid);
            if (result == null || result.isEmpty()) {
                tempWRTransIncomeVOs = Lists.newArrayList();
                if (transIncomeVOs != null && !transIncomeVOs.isEmpty()) {
                    transIncomeVOs.forEach(item -> {
                        tempWRTransIncomeVOs.add(item);
                    });
                    if (selWRTransIncomeVO != null) {
                        tempSelSid = selWRTransIncomeVO.getSid();
                    }
                    List<WRTransIncomeVO> sel = transIncomeVOs.stream()
                            .filter(each -> each.getWr_id().equals(sid) && !result.contains(each))
                            .collect(Collectors.toList());
                    if (sel != null && !sel.isEmpty()) {
                        sel.forEach(item -> {
                            transIncomeVOs.remove(item);
                        });
                    }
                }
            } else {
                result.forEach(item -> {
                    int index = transIncomeVOs.indexOf(item);
                    if (index >= 0) {
                        transIncomeVOs.get(index).replaceValue(item);
                    } else {
                        transIncomeVOs.add(item);
                    }
                    if (tempWRTransIncomeVOs != null && !tempWRTransIncomeVOs.isEmpty()) {
                        int indext = tempWRTransIncomeVOs.indexOf(item);
                        if (indext >= 0) {
                            tempWRTransIncomeVOs.get(indext).replaceValue(item);
                        } else {
                            tempWRTransIncomeVOs.add(item);
                        }
                    }
                });
                List<WRTransIncomeVO> sel = transIncomeVOs.stream()
                        .filter(each -> each.getWr_id().equals(sid)
                                && !result.contains(each))
                        .collect(Collectors.toList());
                if (sel != null && !sel.isEmpty()) {
                    tempWRTransIncomeVOs = Lists.newArrayList();
                    transIncomeVOs.forEach(item -> {
                        tempWRTransIncomeVOs.add(item);
                    });
                    if (selWRTransIncomeVO != null) {
                        tempSelSid = selWRTransIncomeVO.getSid();
                    }
                    sel.forEach(item -> {
                        transIncomeVOs.remove(item);
                    });
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("reCheck Error", e);
        }
    }

    @Override
    public void openerByBtnUp() {
        try {
            //log.info("工作報告查詢進行上一筆動作");
            if (!transIncomeVOs.isEmpty()) {
                toUp();
                if (selWRTransIncomeVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selWRTransIncomeVO.getWr_id());
                    settingReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("工作報告進行上一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByBtnDown() {
        try {
            //log.info("工作報告查詢進行下一筆動作");
            if (!transIncomeVOs.isEmpty()) {
                toDown();
                if (selWRTransIncomeVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selWRTransIncomeVO.getWr_id());
                    settingReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("工作報告查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByDelete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void openerByInvalid() {
        doReloadInfo();
    }

    @Override
    public void openerByCommit() {
        doReloadInfo();
    }

    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    @Override
    public void openerByEditTag() {
        doReloadInfo();
    }

    @Override
    public void openerByTrace() {
        doReloadInfo();
    }

    @Override
    public void openerByFavorite() {
    }

    @Override
    public void openerByTrans() {
        doReloadInfo();
    }

    @Override
    public void openerByReadRecept() {

    }

    public final TraceUpdateCallBack traceUpdateCallBack = new TraceUpdateCallBack() {

        private static final long serialVersionUID = 5483815089764527601L;

        @Override
        public void doUpdateData() {
            updateInfoToDataTable(selWr_ID);
            selWr_ID = "";
            DisplayController.getInstance().update(dataTableID);
        }
    };

    public final TransUpdateCallBack transUpdateCallBack = new TransUpdateCallBack() {

        private static final long serialVersionUID = -3753731767139149240L;

        @Override
        public void doUpdateData() {
            updateInfoToDataTable(selWr_ID);
            selWr_ID = "";
            DisplayController.getInstance().update(dataTableID);
        }
    };

}
