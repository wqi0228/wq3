/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.manager.WRMasterManager;
import com.cy.work.backend.web.logic.manager.WorkSettingOrgManager;
import com.cy.work.backend.web.logic.manager.WorkSettingUserManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRMasterLastModifyVO;
import com.cy.work.backend.web.view.vo.LastModifySearchVO;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 最後修改日期調整報表邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRMasterSearchLastModifyLogicComponents implements InitializingBean, Serializable {

    private static final long serialVersionUID = -8639341228804378894L;
    private static WRMasterSearchLastModifyLogicComponents instance;

    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WorkSettingOrgManager orgManager;
    @Autowired
    private WRMasterManager wRMasterManager;
    @Autowired
    private WkJsoupUtils jsoupUtils;

    public static WRMasterSearchLastModifyLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRMasterSearchLastModifyLogicComponents.instance = this;
    }

    public List<LastModifySearchVO> getLastModifySearch(List<Integer> depSids, String no, String createUserName, String wr_id) {

        List<LastModifySearchVO> results = Lists.newArrayList();
        try {
            List<WRMasterLastModifyVO> wRMasterLastModifyVO = wRMasterManager.finaWorkReportModifyEditDte(depSids, no, createUserName, wr_id);
            wRMasterLastModifyVO.forEach(item -> {
                Org dep = orgManager.findBySid(item.getDep_sid());
                User createUser = workSettingUserManager.findBySID(item.getCreate_usr());
                LastModifySearchVO vo = new LastModifySearchVO(item.getWr_sid(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getCreate_dt()),
                        dep.getName(),
                        createUser.getName(),
                        item.getTheme(),
                        item.getWr_no(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getLast_modify_dt()),
                        item.getLast_modify_memo(),
                        item.getLast_modify_memo_css(),
                        item.getWr_status().getVal(),
                        item.getEdit_type().getValue()
                );
                results.add(vo);
            });
        } catch (Exception e) {
            log.error("getLastModifySearch", e);
        }
        return results;
    }

    public void updateLastModifyTime(String wr_sid, Date lastModifyDate) {
        String lastModifyDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDash.getValue(), lastModifyDate);
        lastModifyDateStr = lastModifyDateStr + " 23:59:59";
        Date lastDate = ToolsDate.transStringToDate(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), lastModifyDateStr);
        wRMasterManager.updateLastModifyTime(wr_sid, lastDate);
    }

    public void updateLastModifyTimeToNow(String wr_sid) {
        wRMasterManager.updateLastModifyTime(wr_sid, new Date());
    }

    public void updateLastModifyMemo(String wr_sid, String memoCss) {
        wRMasterManager.updateLastModifyMemo(wr_sid, jsoupUtils.clearCssTag(memoCss), memoCss);
    }
}
