/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.logic.manager.WorkSettingOrgManager;
import com.cy.work.backend.web.logic.manager.WrBaseDepAccessInfoManager;
import com.cy.work.backend.web.vo.WrBaseDepAccessInfo;
import com.cy.work.backend.web.vo.converter.to.DepTo;
import com.cy.work.backend.web.vo.converter.to.LimitBaseAccessViewDep;
import com.cy.work.backend.web.vo.converter.to.OtherBaseAccessViewDep;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * 基礎單位可閱權限設定邏輯原件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WrBaseDepAccessInfoLogicComponents implements InitializingBean, Serializable {

    private static final long serialVersionUID = -1860159729777276621L;
    private static WrBaseDepAccessInfoLogicComponents instance;

    @Autowired
    private WrBaseDepAccessInfoManager wrBaseDepAccessInfoManager;
    @Autowired
    private WorkSettingOrgManager orgManager;
    @Autowired
    private WrBasePersonAccessInfoLogicComponents wrBasePersonAccessInfoLogicComponents;

    public static WrBaseDepAccessInfoLogicComponents getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WrBaseDepAccessInfoLogicComponents.instance = this;
    }

    /**
     * 取得基礎單位外增加檢閱的單位(人員及部門聯集)
     *
     * @param wrBaseDepAccessInfoDepSid 登入者部門Sid
     * @param loginUserSid              登入者Sid
     * @return List
     */
    public List<Org> getOtherBaseDepAccessInfoDeps(Integer wrBaseDepAccessInfoDepSid, Integer loginUserSid) {
        WrBaseDepAccessInfo wrBaseDepAccessInfo = wrBaseDepAccessInfoManager.getByLoginDepSid(wrBaseDepAccessInfoDepSid);
        List<Org> results = Lists.newArrayList();
        if (wrBaseDepAccessInfo != null && wrBaseDepAccessInfo.getOtherBaseAccessViewDep() != null
                && CollectionUtils.isNotEmpty(wrBaseDepAccessInfo.getOtherBaseAccessViewDep().getDepTos())) {
            wrBaseDepAccessInfo.getOtherBaseAccessViewDep().getDepTos().forEach(item -> {
                results.add(orgManager.findBySid(item.getSid()));
            });
        }
        // todo 這段要重新思考下會導致後面判斷有問題
        List<User> otherUsers = wrBasePersonAccessInfoLogicComponents.getOtherBaseAccessViewUser(loginUserSid);
        otherUsers.forEach(item -> {
            Org dep = orgManager.findBySid(item.getPrimaryOrg().getSid());
            if (!results.contains(dep)) {
                results.add(dep);
            }
        });
        return results;
    }

    /**
     * 取得基礎單位內限制的單位(人員及部門交集)
     *
     * @param wrBaseDepAccessInfoDepSid 登入者部門Sid
     * @param loginUserSid              登入者Sid
     * @return List
     */
    public List<Org> getBaseDepAccessInfoDeps(Integer wrBaseDepAccessInfoDepSid, Integer loginUserSid) {
        WrBaseDepAccessInfo wrBaseDepAccessInfo = wrBaseDepAccessInfoManager.getByLoginDepSid(wrBaseDepAccessInfoDepSid);
        List<Org> results = Lists.newArrayList();

        // 優先檢測是否有限制基礎部門特定人員,若有代表基礎部門下僅能顯示該些人員
        List<User> limitUsers = wrBasePersonAccessInfoLogicComponents.getLimitBaseAccessViewUser(loginUserSid);
        List<Org> limitOrgs = getLimitBaseAccessViewDep(wrBaseDepAccessInfoDepSid);
        if (WkStringUtils.notEmpty(limitUsers)) {
            limitUsers.forEach(item -> {
                Org dep = orgManager.findBySid(item.getPrimaryOrg().getSid());
                if (limitOrgs != null && !limitOrgs.contains(dep)) {
                    return;
                }
                if (!results.contains(dep)) {
                    results.add(dep);
                }
            });
            return results;
        }

        // 若無限制基礎部門設定,預設帶入基礎部門都可搜尋
        if (wrBaseDepAccessInfo != null && wrBaseDepAccessInfo.getLimitBaseAccessViewDep() != null
                && CollectionUtils.isNotEmpty(wrBaseDepAccessInfo.getLimitBaseAccessViewDep().getDepTos())) {

            wrBaseDepAccessInfo.getLimitBaseAccessViewDep().getDepTos().forEach(item -> {
                results.add(orgManager.findBySid(item.getSid()));
            });
        } else {
            // 能看到的最高部門層級:OrgLevel.MINISTERIAL -> 同部的可以互相看到，OrgLevel.THE_PANEL -> 同組可以互相看到
            Org baseOrg = WkOrgUtils.prepareBaseDep(wrBaseDepAccessInfoDepSid, OrgLevel.THE_PANEL);
            if (baseOrg != null) {

                List<Org> allChildOrg = WkOrgCache.getInstance().findAllChild(baseOrg.getSid());
                results.add(baseOrg);
                if (WkStringUtils.notEmpty(allChildOrg)) {
                    results.addAll(allChildOrg);
                }
            }

            List<Org> managerOrgs = WkOrgCache.getInstance().findManagerOrgs(loginUserSid);
            if (WkStringUtils.notEmpty(managerOrgs)) {
                for(Org o:managerOrgs){
                    List<Org> allChildManagerOrg = WkOrgCache.getInstance().findAllChild(o.getSid());
                    results.addAll(allChildManagerOrg);
                }
                results.addAll(managerOrgs);
            }
        }

        if("eva".equals(SecurityFacade.getUserId())) {
            List<Org> managerOrgs = WkOrgCache.getInstance().findManagerOrgs(loginUserSid);
            if (WkStringUtils.notEmpty(managerOrgs)) {
                results.addAll(managerOrgs);
            }
        }
        
        
        // 部門主管可看到自己底下部門,也可看到兼任部門底下的部門
         List<Org> managerOrgs = WkOrgCache.getInstance().findManagerOrgs(loginUserSid);
         if (managerOrgs.size() != 0) {
             results.addAll(managerOrgs);
         managerOrgs.forEach(org -> {
             results.addAll(OrgLogicComponents.getInstance().getAllChildOrg(org));
         });
         }
         results.addAll(results);

        return results.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * 取得限制基本單位 By 部門Sid
     *
     * @param wrBaseDepAccessInfoDepSid 部門Sid
     * @return List
     */
    public List<Org> getLimitBaseAccessViewDep(Integer wrBaseDepAccessInfoDepSid) {
        return wrBaseDepAccessInfoManager.getLimitBaseAccessViewDep(wrBaseDepAccessInfoDepSid);
    }

    /**
     * 取得增加基本單位 By 部門Sid
     *
     * @param wrBaseDepAccessInfoDepSid 部門Sid
     * @return List
     */
    public List<Org> getOtherBaseAccessViewDep(Integer wrBaseDepAccessInfoDepSid) {
        return wrBaseDepAccessInfoManager.getOtherBaseAccessViewDep(wrBaseDepAccessInfoDepSid);
    }

    public void clearLimitBaseAccessViewDep(Integer wrBaseDepAccessInfoDepSid, Integer loginUserSid) {
        WrBaseDepAccessInfo wrBaseDepAccessInfo = wrBaseDepAccessInfoManager.getByLoginDepSid(wrBaseDepAccessInfoDepSid);
        if (wrBaseDepAccessInfo == null) {
            return;
        }
        wrBaseDepAccessInfo.setLimitBaseAccessViewDep(null);
        wrBaseDepAccessInfoManager.updateWrBaseDepAccessInfo(wrBaseDepAccessInfo, loginUserSid);
    }

    /**
     * 儲存限制基本單位
     *
     * @param wrBaseDepAccessInfoDepSid 部門Sid
     * @param loginUserSid              登入者Sid
     * @param accessDeps                可閱讀的限制基本單位部門
     */
    public void saveLimitBaseAccessViewDep(Integer wrBaseDepAccessInfoDepSid, Integer loginUserSid, List<Org> accessDeps) {
        WrBaseDepAccessInfo wrBaseDepAccessInfo = wrBaseDepAccessInfoManager.getByLoginDepSid(wrBaseDepAccessInfoDepSid);
        if (wrBaseDepAccessInfo == null) {
            wrBaseDepAccessInfo = new WrBaseDepAccessInfo();
            wrBaseDepAccessInfo.setLoginUserDepSid(wrBaseDepAccessInfoDepSid);
        }
        LimitBaseAccessViewDep limitBaseAccessViewDep = new LimitBaseAccessViewDep();
        accessDeps.forEach(item -> {
            DepTo dt = new DepTo();
            dt.setSid(item.getSid());
            limitBaseAccessViewDep.getDepTos().add(dt);
        });
        wrBaseDepAccessInfo.setLimitBaseAccessViewDep(limitBaseAccessViewDep);
        if (!Strings.isNullOrEmpty(wrBaseDepAccessInfo.getSid())) {
            wrBaseDepAccessInfoManager.updateWrBaseDepAccessInfo(wrBaseDepAccessInfo, loginUserSid);
        } else {
            wrBaseDepAccessInfoManager.createWrBaseDepAccessInfo(wrBaseDepAccessInfo, loginUserSid);
        }
    }

    /**
     * 儲存增加基本部門
     *
     * @param wrBaseDepAccessInfoDepSid 部門Sid
     * @param loginUserSid              登入者Sid
     * @param accessDeps                增加的部門
     */
    public void saveOtherBaseAccessViewDep(Integer wrBaseDepAccessInfoDepSid, Integer loginUserSid, List<Org> accessDeps) {
        WrBaseDepAccessInfo wrBaseDepAccessInfo = wrBaseDepAccessInfoManager.getByLoginDepSid(wrBaseDepAccessInfoDepSid);
        if (wrBaseDepAccessInfo == null) {
            wrBaseDepAccessInfo = new WrBaseDepAccessInfo();
            wrBaseDepAccessInfo.setLoginUserDepSid(wrBaseDepAccessInfoDepSid);
        }
        OtherBaseAccessViewDep otherBaseAccessViewDep = new OtherBaseAccessViewDep();
        accessDeps.forEach(item -> {
            DepTo dt = new DepTo();
            dt.setSid(item.getSid());
            otherBaseAccessViewDep.getDepTos().add(dt);
        });
        wrBaseDepAccessInfo.setOtherBaseAccessViewDep(otherBaseAccessViewDep);
        if (!Strings.isNullOrEmpty(wrBaseDepAccessInfo.getSid())) {
            wrBaseDepAccessInfoManager.updateWrBaseDepAccessInfo(wrBaseDepAccessInfo, loginUserSid);
        } else {
            wrBaseDepAccessInfoManager.createWrBaseDepAccessInfo(wrBaseDepAccessInfo, loginUserSid);
        }
    }

    /**
     * 找出所有額外設定可閱
     * 
     * @return
     */
    public List<WrBaseDepAccessInfo> findOtherBaseAccessViewDepNotNull() {
        return wrBaseDepAccessInfoManager.findOtherBaseAccessViewDepNotNull();
    }

    /**
     * 找出所有額外設定限制可閱
     * 
     * @return
     */
    public List<WrBaseDepAccessInfo> findLimitBaseAccessViewDepNotNull() {
        return wrBaseDepAccessInfoManager.findLimitBaseAccessViewDepNotNull();
    }
}