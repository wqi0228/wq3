/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.cy.work.backend.web.common.setting.WRStatusSetting;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@ManagedBean
public class WorkRrojectStatusItemBean implements Serializable{


    private static final long serialVersionUID = -5075911796932972813L;

    public List<SelectItem> getWorkSearchStatusItems() {
        return WRStatusSetting.getWorkSearchItems();
    }

}
