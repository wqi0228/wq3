/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class WRTransDepSearchColumnVO implements Serializable {


    private static final long serialVersionUID = 2029830336020948534L;
    /** 序 */
    @Setter
    @Getter
    private WRColumnDetailVO index;
    /** 部 */
    @Setter
    @Getter
    private WRColumnDetailVO fowardDep;
    /** 個 */
    @Setter
    @Getter
    private WRColumnDetailVO fowardMember;
    /** 追 */
    @Setter
    @Getter
    private WRColumnDetailVO trace;

    @Setter
    @Getter
    private WRColumnDetailVO sendDep;

    @Setter
    @Getter
    private WRColumnDetailVO sendUser;

    @Setter
    @Getter
    private WRColumnDetailVO sendTime;

    @Setter
    @Getter
    private WRColumnDetailVO recevieDep;

    @Setter
    @Getter
    private WRColumnDetailVO tag;

    @Setter
    @Getter
    private WRColumnDetailVO theme;

    @Setter
    @Getter
    private WRColumnDetailVO modifyTime;

    @Setter
    @Getter
    private WRColumnDetailVO readed;
    @Setter
    @Getter
    private String pageCount = "50";

}
