/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.manager.WorkSettingUserManager;
import com.cy.work.backend.web.logic.manager.WorkTraceInfoManager;
import com.cy.work.backend.web.logic.manager.WrReadRecordManager;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.vo.WorkTraceInfo;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.enums.TraceStatus;
import com.cy.work.backend.web.vo.enums.TraceType;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 追蹤邏輯元件
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WorkTraceInfoLogicComponents implements InitializingBean, Serializable {


    private static final long serialVersionUID = -4733904795578585596L;
    private static WorkTraceInfoLogicComponents instance;
    @Autowired
    private WorkTraceInfoManager workTraceInfoManager;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WrReadRecordManager wrReadRecordManager;
    @Autowired
    private OrgLogicComponents orgLogicComponents;

    @Autowired
    private WkJsoupUtils jsoupUtils;

    public static WorkTraceInfoLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WorkTraceInfoLogicComponents.instance = this;
    }

    /**
     * 取得追蹤筆數
     *
     * @param loginUserSid 登入者Sid
     * @param depSid       登入者部門Sid
     * @param traceStausID 追蹤狀態ID
     * @return
     */
    public Integer getCountByTraceSearch(Integer loginUserSid, Integer depSid, String traceStausID) {
        try {
            return workTraceInfoManager.getWorkReportListCountByTraceSearch(loginUserSid, depSid, traceStausID);

        } catch (Exception e) {
            log.error("getCountByTraceSearch", e);
        }
        return 0;
    }

    /**
     * 是否有部門追蹤(已完成&未完成)
     *
     * @param depSid 部門Sid
     * @param wr_Id  工作報告Sid
     * @return
     */
    public boolean isHasTraceDepAll(Integer depSid, String wr_Id) {
        try {
            Integer count = workTraceInfoManager.getWorkTraceInfoByWr_SidForDEPCountAll(wr_Id, depSid);
            if (count > 0) {
                return true;
            }
        } catch (Exception e) {
            log.error("isHasTraceDep", e);
        }
        return false;
    }

    /**
     * 是否有部門追蹤(未完成)
     *
     * @param depSid 部門Sid
     * @param wr_Id  工作報告Sid
     * @return
     */
    public boolean isHasTraceDepUnFinished(Integer depSid, String wr_Id) {
        try {
            Integer count = workTraceInfoManager.getWorkTraceInfoByWr_SidForDEPCountUnFinished(wr_Id, depSid);
            if (count > 0) {
                return true;
            }
        } catch (Exception e) {
            log.error("isHasTraceDep", e);
        }
        return false;
    }

    /**
     * 是否有個人追蹤(已完成&未完成)
     *
     * @param loginUserSid 登入者Sid
     * @param wr_Id        工作報告Sid
     * @return
     */
    public boolean isHasTracePersonAll(Integer loginUserSid, String wr_Id) {
        try {
            Integer count = workTraceInfoManager.getWorkTraceInfoByWr_SidForPersonCountAll(wr_Id, loginUserSid);
            if (count > 0) {
                return true;
            }
        } catch (Exception e) {
            log.error("isHasTracePerson", e);
        }
        return false;
    }

    /**
     * 是否有個人追蹤(未完成)
     *
     * @param loginUserSid 登入者Sid
     * @param wr_Id        工作報告Sid
     * @return
     */
    public boolean isHasTracePersonUnFinished(Integer loginUserSid, String wr_Id) {
        try {
            Integer count = workTraceInfoManager.getWorkTraceInfoByWr_SidForPersonCountUnFinished(wr_Id, loginUserSid);
            if (count > 0) {
                return true;
            }
        } catch (Exception e) {
            log.error("isHasTracePerson", e);
        }
        return false;
    }

    /**
     * 更新追蹤內容及追蹤日期
     *
     * @param loginUserSid 登入者Sid
     * @param traceSid     追蹤Sid
     * @param memo         內容
     * @param notifyDate   追蹤日期
     */
    public void updateTraceNote(Integer loginUserSid, String traceSid, String memo, Date notifyDate) {
        WorkTraceInfo wi = workTraceInfoManager.getWorkTraceInfoBySid(traceSid);
        if (wi == null) {
            Preconditions.checkState(false, "撈取追蹤失敗！");
        }
        wi.setTrace_memo(jsoupUtils.clearCssTag(HtmlUtils.htmlUnescape(memo)));
        wi.setTrace_memo_css(memo);
        wi.setTrace_notice_date(notifyDate);
        workTraceInfoManager.update(wi, loginUserSid);
    }

    /**
     * 取消追蹤
     *
     * @param loginUserSid 登入者Sid
     * @param traceSid     追蹤Sid
     */
    public void cancelTrace(Integer loginUserSid, String traceSid) {
        WorkTraceInfo wi = workTraceInfoManager.getWorkTraceInfoBySid(traceSid);
        if (wi == null) {
            Preconditions.checkState(false, "撈取追蹤失敗！");
        }
        wi.setStatus(Activation.INACTIVE);
        workTraceInfoManager.update(wi, loginUserSid);
    }

    /**
     * 追蹤完成
     *
     * @param loginUserSid 登入者Sid
     * @param traceSid     追蹤Sid
     */
    public void overTrace(Integer loginUserSid, String traceSid) {
        WorkTraceInfo wi = workTraceInfoManager.getWorkTraceInfoBySid(traceSid);
        if (wi == null) {
            Preconditions.checkState(false, "撈取追蹤失敗！");
        }
        wi.setTrace_finish_date(new Date());
        wi.setTrace_status(TraceStatus.FINISHED);
        workTraceInfoManager.update(wi, loginUserSid);
    }

    /**
     * 儲存部門追蹤
     *
     * @param loginUserSid 登入者Sid
     * @param wr_Id        工作報告Sid
     * @param wr_no        工作報告單號
     * @param memo         內容
     * @param noticeDate   追蹤提醒時間
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void savetWorkTraceInfoForDep(Integer loginUserSid, String wr_Id, String wr_no, String memo, Date noticeDate) {
        User user = workSettingUserManager.findBySID(loginUserSid);
        OrgViewVo o = new OrgViewVo(user.getPrimaryOrg().getSid());
        List<OrgViewVo> orgViewVos = getWorkTraceInfoByWr_SidForDep(wr_Id);
        StringBuilder sb = new StringBuilder();
        if (orgViewVos.contains(o)) {
            sb.append("該工作報告").append("部門").append("已有進行追蹤動作！！");
        }
        if (!Strings.isNullOrEmpty(sb.toString())) {
            Preconditions.checkState(false, sb.toString());
        }
        List<User> users = WkUserCache.getInstance().findUsersByPrimaryOrgSid(o.getSid());
        //用userSid查詢有關此主檔的閱讀紀錄並組成map
        List<String> userSids = users.stream().map(x -> x.getSid() + "").collect(Collectors.toList());
        Map<String, String> userMap = wrReadRecordManager.findAllByUserSidsAndWrSid(userSids, wr_Id)
                .stream()
                .collect(
                        Collectors.toMap(WrReadRecord::getCreateUser, WrReadRecord::getCreateUser));
        //如果使用者沒有該主檔閱讀紀錄，幫使用者新增一筆閱讀紀錄
        for (User u : users) {
            if (userMap.get(u.getSid() + "") == null) {
                WrReadRecord wrReadRecord = new WrReadRecord(WRReadStatus.UNREAD.name(), u.getSid().toString(), new Date(), wr_Id);
                wrReadRecordManager.saveWrReadRecord(wrReadRecord);
            }
        }

        WorkTraceInfo wti = new WorkTraceInfo();
        wti.setTrace_source_sid(wr_Id);
        wti.setTrace_source_no(wr_no);
        wti.setTrace_dep(user.getPrimaryOrg().getSid());
        wti.setTrace_usr(null);
        wti.setTrace_status(TraceStatus.UN_FINISHED);
        wti.setTrace_memo(jsoupUtils.clearCssTag(HtmlUtils.htmlUnescape(memo)));
        wti.setTrace_memo_css(memo);
        wti.setTrace_notice_date(noticeDate);
        wti.setTrace_type(TraceType.DEPARTMENT);
        workTraceInfoManager.create(wti, loginUserSid);
    }

    /**
     * 儲存個人追蹤
     *
     * @param loginUserSid 登入者Sid
     * @param wr_Id        工作報告Sid
     * @param wr_no        工作報告單號
     * @param memo         內容
     * @param noticeDate   追蹤提醒時間
     */
    public void savetWorkTraceInfoForPerson(Integer loginUserSid, String wr_Id, String wr_no, String memo, Date noticeDate) {
        List<UserViewVO> alreadyTraces = getWorkTraceInfoByWr_SidForPerson(wr_Id);
        UserViewVO addUserViewVO = new UserViewVO(loginUserSid);
        StringBuilder sb = new StringBuilder();
        if (alreadyTraces.contains(addUserViewVO)) {
            sb.append("該工作報告").append("您").append("已有進行追蹤動作！！");
        }
        if (!Strings.isNullOrEmpty(sb.toString())) {
            Preconditions.checkState(false, sb.toString());
        }
        WorkTraceInfo wti = new WorkTraceInfo();
        wti.setTrace_source_sid(wr_Id);
        wti.setTrace_source_no(wr_no);
        wti.setTrace_dep(null);
        wti.setTrace_usr(addUserViewVO.getSid());
        wti.setTrace_status(TraceStatus.UN_FINISHED);
        wti.setTrace_memo(jsoupUtils.clearCssTag(HtmlUtils.htmlUnescape(memo)));
        wti.setTrace_memo_css(memo);
        wti.setTrace_notice_date(noticeDate);
        wti.setTrace_type(TraceType.PERSONAL);
        workTraceInfoManager.create(wti, loginUserSid);
    }

    /**
     * 儲存邀請其他人追蹤
     *
     * @param loginUserSid 登入者Sid
     * @param wr_Id        工作報告Sid
     * @param wr_no        工作報告單號
     * @param memo         內容
     * @param noticeDate   追蹤日期期
     * @param selectUserVO 欲追蹤人員
     */
    public void savetWorkTraceInfoForOtherPerson(Integer loginUserSid, String wr_Id, String wr_no, String memo, Date noticeDate, List<UserViewVO> selectUserVO) {
        List<UserViewVO> alreadyTraces = getWorkTraceInfoByWr_SidForPerson(wr_Id);
        StringBuilder sb = new StringBuilder();
        selectUserVO.forEach(item -> {
            if (alreadyTraces.contains(item)) {
                if (!Strings.isNullOrEmpty(sb.toString())) {
                    sb.append("\n");
                }
                sb.append("該工作報告").append(item.getName()).append("已有進行追蹤動作！！");
            }
        });
        if (!Strings.isNullOrEmpty(sb.toString())) {
            Preconditions.checkState(false, sb.toString());
        }
        List<String> userSidList = selectUserVO.stream().map(x -> x.getSid().toString()).collect(Collectors.toList());
        Map<String, String> userReadRecordMap =
                wrReadRecordManager.findAllByUserSidsAndWrSid(userSidList, wr_Id)
                        .stream()
                        .collect(
                                Collectors.toMap(WrReadRecord::getCreateUser, WrReadRecord::getWrSid));
        for (UserViewVO item : selectUserVO) {
            WorkTraceInfo wti = new WorkTraceInfo();
            wti.setTrace_source_sid(wr_Id);
            wti.setTrace_source_no(wr_no);
            wti.setTrace_dep(null);
            wti.setTrace_usr(item.getSid());
            wti.setTrace_status(TraceStatus.UN_FINISHED);
            wti.setTrace_memo(jsoupUtils.clearCssTag(HtmlUtils.htmlUnescape(memo)));
            wti.setTrace_memo_css(memo);
            wti.setTrace_notice_date(noticeDate);
            wti.setTrace_type(TraceType.OTHER_PERSON);
            workTraceInfoManager.create(wti, loginUserSid);
            if (userReadRecordMap.get(item.getSid().toString()) == null) {
                WrReadRecord wrReadRecord = new WrReadRecord(WRReadStatus.UNREAD.name(), item.getSid().toString(), new Date(), wr_Id);
                wrReadRecordManager.saveWrReadRecord(wrReadRecord);
            }
        }

    }

    /**
     * 取得該工作報告已追蹤部門
     *
     * @param sid 工作報告Sid
     * @return
     */
    public List<OrgViewVo> getWorkTraceInfoByWr_SidForDep(String sid) {
        try {
            List<OrgViewVo> orgViewVos = Lists.newArrayList();
            List<WorkTraceInfo> workTraceInfos = workTraceInfoManager.getWorkTraceInfoByWr_SidForDEP(sid);
            workTraceInfos.forEach(item -> {
                Org department = orgLogicComponents.findOrgBySid(item.getTrace_dep());
                OrgViewVo uv = new OrgViewVo(department.getSid(), department.getOrgNameWithParentIfDuplicate());
                orgViewVos.add(uv);
            });
            return orgViewVos;
        } catch (Exception e) {
            log.error("getWorkTraceInfoByWr_SidForDep", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 取得該工作報告已追蹤人員
     *
     * @param sid 工作報告Sid
     * @return
     */
    public List<UserViewVO> getWorkTraceInfoByWr_SidForPerson(String sid) {
        try {
            List<UserViewVO> userViewVOs = Lists.newArrayList();
            List<WorkTraceInfo> workTraceInfos = workTraceInfoManager.getWorkTraceInfoByWr_SidForPerson(sid);
            workTraceInfos.forEach(item -> {
                User user = workSettingUserManager.findBySID(item.getTrace_usr());
                Org department = orgLogicComponents.findOrgBySid(user.getPrimaryOrg().getSid());
                UserViewVO uv = new UserViewVO(user.getSid(), user.getName(), orgLogicComponents.showParentDep(department));
                userViewVOs.add(uv);
            });
            return userViewVOs;
        } catch (Exception e) {
            log.error("getWorkTraceInfoByWr_SidForPerson", e);
        }
        return Lists.newArrayList();
    }

}
