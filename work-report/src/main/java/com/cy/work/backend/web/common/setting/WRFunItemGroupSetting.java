/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.setting;

import com.cy.work.backend.web.logic.components.WRFunItemGroupLogicComponents;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.google.common.collect.Lists;
import java.util.List;

/**
 * 選單大項靜態資料
 *
 * @author brain0925_liao
 */
public class WRFunItemGroupSetting {

    /** 選單大項List */
    private static List<WRFunItemGroupVO> wrFunItemGroups;

    /** 取得選單大項資料,若記憶體已有將不再行建立 */
    public static List<WRFunItemGroupVO> getWRFunItemGroups() {
        if (wrFunItemGroups != null) {
            return wrFunItemGroups;
        }
        wrFunItemGroups = Lists.newArrayList();
        WRFunItemGroupLogicComponents wrFunItemGroupLogicComponents = SpringContextHolder.getBean(WRFunItemGroupLogicComponents.class);

        wrFunItemGroups = wrFunItemGroupLogicComponents.getWRFunItemGroupVOs();
        return wrFunItemGroups;
    }

    public static void clearWRFunItemGroup() {
        wrFunItemGroups = null;
    }
}
