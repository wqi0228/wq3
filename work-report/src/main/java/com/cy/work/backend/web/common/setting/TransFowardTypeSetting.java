/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.setting;

import com.cy.work.backend.web.vo.enums.ForwardType;
import com.google.common.collect.Lists;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * 轉寄類型靜態資料
 *
 * @author brain0925_liao
 */
public class TransFowardTypeSetting {

    /** 轉寄類型選項List */
    private static List<SelectItem> transFowardTypes;

    /** 取得轉寄類型選項資料,若記憶體已有將不再行建立 */
    public static List<SelectItem> getTransFowardTypesItems() {
        if (transFowardTypes != null) {
            return transFowardTypes;
        }
        transFowardTypes = Lists.newArrayList();
        for (ForwardType item : ForwardType.values()) {
            SelectItem si1 = new SelectItem(item.name(), item.getValue());
            transFowardTypes.add(si1);
        }
        return transFowardTypes;
    }
}
