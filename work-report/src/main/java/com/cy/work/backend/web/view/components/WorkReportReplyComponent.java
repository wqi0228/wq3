/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.work.backend.web.logic.components.WRTraceLogicComponents;
import com.cy.work.backend.web.view.vo.WRTraceVO;
import com.cy.work.backend.web.vo.enums.WRTraceType;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class WorkReportReplyComponent implements Serializable {

    
    private static final long serialVersionUID = 1446718108295748900L;

    @Getter
    private List<WRTraceVO> wRTraceVO;

    private String wrcId;

    private Integer userSid;
    @Getter
    private boolean showReplyTab = false;

    public WorkReportReplyComponent() {
    }

    public void loadData(String wrcId, Integer userSid) {
        this.wrcId = wrcId;
        this.userSid = userSid;
        loadViewData(wrcId, userSid);
    }

    public void loadData() {
        loadViewData(wrcId, userSid);
    }

    private void loadViewData(String wrcId, Integer userSid) {
        wRTraceVO = WRTraceLogicComponents.getInstance().getWRTraceVOsByWrIDAndWRTraceType(userSid, wrcId, WRTraceType.REPLY);
        if (wRTraceVO != null && wRTraceVO.size() > 0) {
            showReplyTab = true;
        } else {
            showReplyTab = false;
        }
    }
}
