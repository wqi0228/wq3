/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.ReLoadCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WRGroupSetupPrivateLogicComponents;
import com.cy.work.backend.web.logic.components.WorkTraceInfoLogicComponents;
import com.cy.work.backend.web.util.pf.DataTablePickList;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRGroupVO;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class WorkReportTraceOtherPersonComponent implements Serializable {


    private static final long serialVersionUID = -323195629770342564L;

    @Getter
    private List<SelectItem> wrGroupVOs;

    private List<WRGroupVO> realWRGroupVOs;
    @Setter
    @Getter
    private String selWRGroupVOs;

    @Getter
    private WRGroupSetupPrivateComponent wrGroupSetupPrivateComponent;

    private Integer loginUserSid;

    private MessageCallBack messageCallBack;
    @Getter
    @Setter
    private String traceUserName;

    private List<UserViewVO> leftViewVO;

    /** DataTablePickList 物件 */
    private DataTablePickList<UserViewVO> dataPickList;

    private List<UserViewVO> allCompUser;

    private List<Org> selectOrgs;

    private DepTreeComponent depTreeComponent;

    private List<UserViewVO> alreadyTraceUser;

    private String wr_Id;

    private String wr_no;

    private String memo;

    private Date noticeDate;

    private ReLoadCallBack traceReloadCallBack;

    private ReLoadCallBack closeCallBack;

    public WorkReportTraceOtherPersonComponent(Integer loginUserSid, MessageCallBack messageCallBack, ReLoadCallBack traceReloadCallBack,
            ReLoadCallBack closeCallBack) {
        this.loginUserSid = loginUserSid;
        this.messageCallBack = messageCallBack;
        this.traceReloadCallBack = traceReloadCallBack;
        this.closeCallBack = closeCallBack;
        this.wrGroupSetupPrivateComponent = new WRGroupSetupPrivateComponent(
                "pt_group_setup_private_setting_dlg_wv",
                "groupMaintainComponent_pickPanel",
                "workTraceGroup_deptUnitDlg",
                "workTraceGroup_dep",
                "pt_group_setup_private_dlg_panel_table_wv",
                "pt_group_setup_private_dlg_panel_table_id",
                loginUserSid, messageCallBack, groupReloadCallBack);
        this.wrGroupVOs = Lists.newArrayList();
        this.realWRGroupVOs = Lists.newArrayList();
        initDataPickList();
        this.depTreeComponent = new DepTreeComponent();
        initOrgTree();
    }

    private void initDataPickList() {
        this.dataPickList = new DataTablePickList<>(UserViewVO.class);
    }

    private void initOrgTree() {
        Org group = new Org(1);
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectOrgs = Lists.newArrayList();
        depTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    private void clear() {
        initDataPickList();
        DisplayController.getInstance().update("dlgTraceActionUsers_view_listView");
        initOrgTree();
        DisplayController.getInstance().update("dlgTraceActionUsers_dep");
        allCompUser.clear();
        alreadyTraceUser.clear();
        realWRGroupVOs.clear();
    }

    public void close() {
        clear();
        DisplayController.getInstance().hidePfWidgetVar("dlgTraceActionUsers");
        traceReloadCallBack.onReload();
    }

    public void saveTrace() {
        if (dataPickList.getTargetData() == null || dataPickList.getTargetData().isEmpty()) {
            messageCallBack.showMessage("您尚未選擇資料");
            return;
        }
        try {
            WorkTraceInfoLogicComponents.getInstance().savetWorkTraceInfoForOtherPerson(loginUserSid,
                    wr_Id, wr_no, memo, noticeDate, dataPickList.getTargetData());
            clear();
            DisplayController.getInstance().hidePfWidgetVar("dlgTraceActionUsers");
            closeCallBack.onReload();
        } catch (Exception e) {
            log.error("saveTrace", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() {
        return depTreeComponent.getMultipleDepTreeManager();
    }

    public void loadDepTree() {
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        Org group = new Org(user.getPrimaryOrg().getCompanySid());
        // 修改,故切換顯示停用部門時,需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        List<Org> allOrg = OrgLogicComponents.getInstance().findOrgsByCompanySid(group.getSid());
        depTreeComponent.init(allOrg, allOrg,
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    public void doSelectOrg() {
        try {
            selectOrgs = depTreeComponent.getSelectOrg();
            if (selectOrgs == null || selectOrgs.isEmpty()) {
                dataPickList.setSourceData(removeTargetUserView(allCompUser, dataPickList.getTargetData()));
                return;
            } else {
                List<UserViewVO> selectDepUserView = Lists.newArrayList();
                selectOrgs.forEach(item -> {
                    selectDepUserView.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
                });
                dataPickList.setSourceData(removeTargetUserView(selectDepUserView, dataPickList.getTargetData()));
            }
        } catch (Exception e) {
            log.error("doSelectOrg", e);
            messageCallBack.showMessage(e.getMessage());
        }
        DisplayController.getInstance().update("dlgTraceActionUsers_view_listView");
        DisplayController.getInstance().hidePfWidgetVar("dlgTraceActionUsers_deptUnitDlg");
    }

    public void filterName() {
        if (Strings.isNullOrEmpty(traceUserName)) {
            dataPickList.setSourceData(removeTargetUserView(allCompUser, dataPickList.getTargetData()));
        } else {
            dataPickList.setSourceData(removeTargetUserView(allCompUser.stream()
                    .filter(each -> each.getName().toLowerCase().contains(traceUserName.toLowerCase()))
                    .collect(Collectors.toList()), dataPickList.getTargetData()));
        }
    }

    public void loadData(String wr_Id, String wr_no, String memo, Date noticDate) {
        this.wr_Id = wr_Id;
        this.wr_no = wr_no;
        this.memo = memo;
        this.noticeDate = noticDate;
        allCompUser = Lists.newArrayList();
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        Integer compSid = user.getPrimaryOrg().getCompanySid();
        try {
            OrgLogicComponents.getInstance().findOrgsByCompanySid(compSid).stream()
                    .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                    .collect(Collectors.toList()).forEach(item -> {
                        allCompUser.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
                    });
        } catch (Exception e) {
            log.error(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }
        leftViewVO = Lists.newArrayList();
        selectOrgs = Lists.newArrayList();
        Org baseOrg = WkOrgUtils.prepareBaseDep(user.getPrimaryOrg().getSid(), OrgLevel.MINISTERIAL);
        selectOrgs.add(baseOrg);
        List<Org> allChildOrg = OrgLogicComponents.getInstance().getAllChildOrg(baseOrg);
        selectOrgs.addAll(allChildOrg);
        leftViewVO.addAll(WRUserLogicComponents.getInstance().findByDepSid(baseOrg.getSid()));
        allChildOrg.forEach(item -> {
            leftViewVO.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
        });
        alreadyTraceUser = WorkTraceInfoLogicComponents.getInstance().getWorkTraceInfoByWr_SidForPerson(wr_Id);
        List<UserViewVO> right = Lists.newArrayList();
        selWRGroupVOs = "";
        traceUserName = "";
        dataPickList.setSourceData(removeTargetUserView(leftViewVO, right));
        dataPickList.setTargetData(right);
        loadGroupData();
        DisplayController.getInstance().update("dlgTraceActionUsers_view_listView");
    }

    public void changeGroupSelectItem() {
        if (!Strings.isNullOrEmpty(selWRGroupVOs)) {
            if (realWRGroupVOs != null) {
                List<WRGroupVO> selWRGroupVO = realWRGroupVOs.stream().filter(each -> each.getGroup_sid().equals(selWRGroupVOs))
                        .collect(Collectors.toList());
                if (selWRGroupVO != null && !selWRGroupVO.isEmpty()) {
                    dataPickList.setSourceData(removeTargetUserView(selWRGroupVO.get(0).getUserViewVO(), dataPickList.getTargetData()));
                }
            }
        } else {
            dataPickList.setSourceData(removeTargetUserView(leftViewVO, dataPickList.getTargetData()));
        }
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source, List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(item -> {
            if (!target.contains(item) && !alreadyTraceUser.contains(item)) {
                filterSource.add(item);
            }
        });
        return filterSource;
    }

    public void loadGroupData() {
        wrGroupVOs.clear();
        realWRGroupVOs.clear();
        for (WRGroupVO item : WRGroupSetupPrivateLogicComponents.getInstance().getWRGroupVO(loginUserSid)) {
            if (item.getStatusID().equals(Activation.ACTIVE.name())) {
                realWRGroupVOs.add(item);
                SelectItem si = new SelectItem(item.getGroup_sid(), item.getGroup_name());
                wrGroupVOs.add(si);
            }
        }
        DisplayController.getInstance().update("dlgTraceActionUsers_view_groupSetup");
    }

    public void toGroupManagerDialog() {
        this.wrGroupSetupPrivateComponent.loadData();
    }

    private final ReLoadCallBack groupReloadCallBack = new ReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 8826994869291361283L;

        @Override
        public void onReload() {
            loadGroupData();
        }
    };

    public DataTablePickList<UserViewVO> getDataPickList() {
        return dataPickList;
    }

}
