/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.primefaces.component.tabview.TabView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.LimitBaseAccessViewDepComponent;
import com.cy.work.backend.web.view.components.LimitBaseAccessViewUserComponent;
import com.cy.work.backend.web.view.components.OtherBaseAccessViewDepComponent;
import com.cy.work.backend.web.view.components.OtherBaseAccessViewUserComponent;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;

import lombok.Getter;
import lombok.Setter;

/**
 * 工作報告可閱權限調整
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@ManagedBean
public class Setting5Bean implements Serializable {


    private static final long serialVersionUID = 8378314954527692667L;
    /**
     * view TableView
     */
    @Getter
    @Setter
    private TabView permissionTabView;
    /**
     * view 錯誤訊息
     */
    @Getter
    private String errorMessage;
    /**
     * 基礎單位可閱權限增加(聯集)
     */
    @Getter
    private OtherBaseAccessViewDepComponent otherBaseAccessViewDepComponent;
    /**
     * 基礎單位可閱權限減少(交集)
     */
    @Getter
    private LimitBaseAccessViewDepComponent limitBaseAccessViewDepComponent;
    /**
     * 基礎單位成員可閱權限減少(交集)
     */
    @Getter
    private LimitBaseAccessViewUserComponent limitBaseAccessViewUserComponent;
    /**
     * 基礎單位成員可閱權限增加(聯集)
     */
    @Getter
    private OtherBaseAccessViewUserComponent otherBaseAccessViewUserComponent;
    /**
     * 登入者物件
     */
    @Getter
    private UserViewVO userViewVO;
    /**
     * 登入者部門物件
     */
    @Getter
    private OrgViewVo depViewVo;
    /**
     * 登入者公司物件
     */
    @Getter
    private OrgViewVo compViewVo;



    @PostConstruct
    public void init() {
        userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        depViewVo = OrgLogicComponents.getInstance().findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = OrgLogicComponents.getInstance().findById(SecurityFacade.getCompanyId());
        otherBaseAccessViewDepComponent = new OtherBaseAccessViewDepComponent(userViewVO.getSid(),
                compViewVo.getSid(), messageCallBack);
        limitBaseAccessViewDepComponent = new LimitBaseAccessViewDepComponent(userViewVO.getSid(),
                compViewVo.getSid(), messageCallBack);
        limitBaseAccessViewUserComponent = new LimitBaseAccessViewUserComponent(userViewVO.getSid(),
                compViewVo.getSid(), messageCallBack);
        otherBaseAccessViewUserComponent = new OtherBaseAccessViewUserComponent(userViewVO.getSid(),
                compViewVo.getSid(), messageCallBack);
    }

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         *
         */
        private static final long serialVersionUID = 8452062610064415512L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    };
}
