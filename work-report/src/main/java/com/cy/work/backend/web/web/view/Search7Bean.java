/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.common.ExportExcelComponent;
import com.cy.work.backend.web.listener.DataTableReLoadCallBack;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.TableUpDownListener;
import com.cy.work.backend.web.listener.TraceUpdateCallBack;
import com.cy.work.backend.web.listener.TransUpdateCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRTransSearchWorkReportLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WRReportCustomColumnLogicComponents;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.CustomColumnComponent;
import com.cy.work.backend.web.view.components.WRTraceComponent;
import com.cy.work.backend.web.view.components.WRTransTableComponent;
import com.cy.work.backend.web.view.components.WorkReportTransSearchComponent;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRTransSendSearchColumnVO;
import com.cy.work.backend.web.view.vo.WRTransSendVO;
import com.cy.work.backend.web.view.vo.WorkReportSidTo;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.cy.work.backend.web.vo.enums.WRSendTransSearchColumn;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Search7Bean implements Serializable, TableUpDownListener {

    
    private static final long serialVersionUID = 5874511767195917131L;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Getter
    private WorkReportTransSearchComponent workReportTransSearchComponent;
    @Getter
    private CustomColumnComponent customColumn;
    /** view 顯示錯誤訊息 */
    @Getter
    private String errorMessage;
    @Getter
    private final String dataTableID = "dataTableSend";
    @Getter
    private final String dataTableWv = "dataTableSendWv";
    @Setter
    @Getter
    private boolean showFrame = false;
    @Getter
    private String iframeUrl = "";
    @Autowired
    private WRReportCustomColumnLogicComponents wrReportCustomColumnLogicComponents;
    @Getter
    private WRTransSendSearchColumnVO searchColumnVO;
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Getter
    private List<WRTransSendVO> transSendVOs;
    @Setter
    @Getter
    private WRTransSendVO selWRTransSendVO;

    private List<WRTransSendVO> tempWRTransSendVOs;

    private String tempSelSid;
    @Autowired
    private WRTransSearchWorkReportLogicComponents wRTransSearchWorkReportLogicComponents;
    @Getter
    private WRTraceComponent traceComponent;

    private String selWr_ID;
    @Getter
    private WRTransTableComponent transTableComponent;
    @Autowired
    private ExportExcelComponent exportExcelComponent;
    @Getter
    private boolean hasDisplay = true;

    @PostConstruct
    public void init() {
        userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        depViewVo = OrgLogicComponents.getInstance().findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = OrgLogicComponents.getInstance().findById(SecurityFacade.getCompanyId());
        this.traceComponent = new WRTraceComponent(userViewVO.getSid(), messageCallBack, traceUpdateCallBack);
        this.transTableComponent = new WRTransTableComponent(userViewVO.getSid(), compViewVo.getSid(), transUpdateCallBack, messageCallBack);
        searchColumnVO = wrReportCustomColumnLogicComponents.getWRTransSendSearchColumnSetting(userViewVO.getSid());
        boolean showSelectBaseOrg = false;
        String personTitleName = "收件者";
        String selectAllOrgTitleName = "寄發至";
        boolean showSelectTransType = true;
        workReportTransSearchComponent = new WorkReportTransSearchComponent(compViewVo.getSid(), userViewVO.getSid(), showSelectBaseOrg, personTitleName, selectAllOrgTitleName, showSelectTransType);
        settingDefault();
        customColumn = new CustomColumnComponent(WRReportCustomColumnUrlType.SEND_SEARCH, Arrays.asList(WRSendTransSearchColumn.values()), searchColumnVO.getPageCount(), userViewVO.getSid(), messageCallBack, dataTableReLoadCallBack);
        doSearch();
    }

    public void clear() {
        settingDefault();
        doSearch();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        try {
            exportExcelComponent.exportExcel(document, null,
                    null, "收部門轉發", false);
        } catch (Exception e) {
            log.error("exportExcel", e);
        } finally {
            hasDisplay = true;
        }
    }

    public void hideColumnContent() {
        hasDisplay = false;
    }

    public void settingDefault() {
        workReportTransSearchComponent.settingDefault();
        workReportTransSearchComponent.getWorkReportSearchHeaderComponent().changeDateIntervalToday();
    }

    public void doSearch() {
        try {
            transSendVOs = runSearch("");
            if (transSendVOs.size() > 0) {
                DisplayController.getInstance().execute("selectDataTablePage('" + dataTableWv + "',0);");
            }
        } catch (Exception e) {
            log.error("doSearch", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private List<WRTransSendVO> runSearch(String wr_id) {
        List<Integer> selectAllOrgSids = Lists.newArrayList();
        workReportTransSearchComponent.getSelectAllOrgs().forEach(item -> {
            selectAllOrgSids.add(item.getSid());
        });
        return wRTransSearchWorkReportLogicComponents.getWRTransSend(
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getSelectTag(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getReadStatus(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getPersonName(),
                selectAllOrgSids,
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getTitle(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getContent(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getAllSearchText(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getStartDate(),
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getEndDate(),
                userViewVO.getSid(),
                depViewVo.getSid(),
                wr_id,
                workReportTransSearchComponent.getWorkReportSearchHeaderComponent().getSelectTransType(), compViewVo.getSid()
        );
    }

    public void clickTransDep(String sid) {
        try {
            List<WRTransSendVO> sel = transSendVOs.stream()
                    .filter(each -> each.getSid().equals(sid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("轉寄部門資料比對有誤,無法開啟");
                return;
            }
            selWRTransSendVO = sel.get(0);
            selWr_ID = selWRTransSendVO.getWr_id();
            DisplayController.getInstance().update(dataTableID);
            transTableComponent.loadDataDep(selWRTransSendVO.getWr_id(), selWRTransSendVO.getNo());
            DisplayController.getInstance().showPfWidgetVar("dlgForwardDep");
        } catch (Exception e) {
            log.error("clickTransDep", e);
        }
    }

    public void clickTransPerson(String sid) {
        try {
            List<WRTransSendVO> sel = transSendVOs.stream()
                    .filter(each -> each.getSid().equals(sid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("轉寄個人資料比對有誤,無法開啟");
                return;
            }

            selWRTransSendVO = sel.get(0);
            selWr_ID = selWRTransSendVO.getWr_id();
            DisplayController.getInstance().update(dataTableID);
            transTableComponent.loadDataPerson(selWRTransSendVO.getWr_id(), selWRTransSendVO.getNo());
            DisplayController.getInstance().showPfWidgetVar("dlgForwardPerson");
        } catch (Exception e) {
            log.error("clickTransDep", e);
        }
    }

    public void clickTrace(String sid) {
        try {
            List<WRTransSendVO> sel = transSendVOs.stream()
                    .filter(each -> each.getSid().equals(sid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("轉寄個人資料比對有誤,無法開啟");
                return;
            }

            selWRTransSendVO = sel.get(0);
            selWr_ID = selWRTransSendVO.getWr_id();
            DisplayController.getInstance().update(dataTableID);
            traceComponent.loadData(selWRTransSendVO.getWr_id(), selWRTransSendVO.getNo(), selWRTransSendVO.getTitle());
            DisplayController.getInstance().showPfWidgetVar("dlgTraceAction");
            DisplayController.getInstance().update("dlgTraceAction_view");
        } catch (Exception e) {
            log.error("clickTrace", e);
        }
    }

    public void openCustomColumn() {
        try {
            customColumn.loadData();
            DisplayController.getInstance().update("dlgDefineField_view");
            DisplayController.getInstance().showPfWidgetVar("dlgDefineField");
        } catch (Exception e) {
            log.error("openCustomColumn", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 94116796578470267L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    };

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -8769766690582878252L;

        @Override
        public void reload() {
            searchColumnVO = wrReportCustomColumnLogicComponents.getWRTransSendSearchColumnSetting(userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };

    private void toUp() {
        try {
            if (tempWRTransSendVOs != null && !tempWRTransSendVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempWRTransSendVOs.indexOf(new WRTransSendVO(tempSelSid));
                if (index > 0) {
                    selWRTransSendVO = tempWRTransSendVOs.get(index - 1);
                } else {
                    selWRTransSendVO = transSendVOs.get(0);
                }
                tempWRTransSendVOs = null;
                tempSelSid = "";
            } else {
                int index = transSendVOs.indexOf(selWRTransSendVO);
                if (index > 0) {
                    selWRTransSendVO = transSendVOs.get(index - 1);
                } else {
                    selWRTransSendVO = transSendVOs.get(0);
                }
            }
        } catch (Exception e) {
            log.error("toUp", e);
            tempSelSid = "";
            tempWRTransSendVOs = null;
            if (!transSendVOs.isEmpty()) {
                selWRTransSendVO = transSendVOs.get(0);
            }
        }
    }

    private void toDown() {
        try {
            if (tempWRTransSendVOs != null && !tempWRTransSendVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempWRTransSendVOs.indexOf(new WRTransSendVO(tempSelSid));
                if (index >= 0) {
                    selWRTransSendVO = tempWRTransSendVOs.get(index + 1);
                } else {
                    selWRTransSendVO = transSendVOs.get(transSendVOs.size() - 1);
                }
                tempWRTransSendVOs = null;
                tempSelSid = "";
            } else {
                int index = transSendVOs.indexOf(selWRTransSendVO);
                if (index >= 0) {
                    selWRTransSendVO = transSendVOs.get(index + 1);
                } else {
                    selWRTransSendVO = transSendVOs.get(transSendVOs.size() - 1);
                }
            }
        } catch (Exception e) {
            log.error("toDown", e);
            tempWRTransSendVOs = null;
            tempSelSid = "";
            if (transSendVOs.size() > 0) {
                selWRTransSendVO = transSendVOs.get(transSendVOs.size() - 1);
            }
        }
    }

    public void btnOpenUrl(String sid) {
        try {
            settingSelTransReceviceDepVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("btnOpenUrl", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            settingSelTransReceviceDepVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp_iframe.xhtml?wrcId=" + selWRTransSendVO.getWr_id() + "&encryptParam=" + selWRTransSendVO.getEncryptParam();
            tableUpDownBean.setWorp_path("worp_iframe");
        } catch (Exception e) {
            log.error("btnOpenFrame", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void settingReaded() {
        transSendVOs.forEach(item -> {
            if (selWRTransSendVO.getWr_id().equals(item.getWr_id())) {
                selWRTransSendVO.replaceReadStatus(WRReadStatus.HASREAD);
                item.replaceReadStatus(WRReadStatus.HASREAD);
            }
        });
    }

    private void settingSelTransReceviceDepVO(String sid) {
        WRTransSendVO sel = new WRTransSendVO(sid);
        int index = transSendVOs.indexOf(sel);
        if (index > 0) {
            selWRTransSendVO = transSendVOs.get(index);
        } else {
            selWRTransSendVO = transSendVOs.get(0);
        }
        settingReaded();
    }

    public void settingPreviousAndNext() {
        int index = transSendVOs.indexOf(selWRTransSendVO);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(transSendVOs.get(index - 1).getWr_id());
        }
        if (index == transSendVOs.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(transSendVOs.get(index + 1).getWr_id());
        }
    }

    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.error("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        WkJsonUtils jsonUtils = SpringContextHolder.getBean(WkJsonUtils.class);
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.error("doReloadInfo", ex);
        }
    }

    private void updateInfoToDataTable(String sid) {
        try {
            List<WRTransSendVO> result = runSearch(sid);
            if (result == null || result.isEmpty()) {
                tempWRTransSendVOs = Lists.newArrayList();
                if (transSendVOs != null && !transSendVOs.isEmpty()) {
                    transSendVOs.forEach(item -> {
                        tempWRTransSendVOs.add(item);
                    });
                    if (selWRTransSendVO != null) {
                        tempSelSid = selWRTransSendVO.getSid();
                    }
                    List<WRTransSendVO> sel = transSendVOs.stream()
                            .filter(each -> each.getWr_id().equals(sid)
                            && !result.contains(each))
                            .collect(Collectors.toList());
                    if (sel != null && !sel.isEmpty()) {
                        sel.forEach(item -> {
                            transSendVOs.remove(item);
                        });
                    }
                }
            } else {
                result.forEach(item -> {
                    int index = transSendVOs.indexOf(item);
                    if (index >= 0) {
                        transSendVOs.get(index).replaceValue(item);
                    } else {
                        transSendVOs.add(item);
                    }
                    if (tempWRTransSendVOs != null && !tempWRTransSendVOs.isEmpty()) {
                        int indext = tempWRTransSendVOs.indexOf(item);
                        if (indext >= 0) {
                            tempWRTransSendVOs.get(indext).replaceValue(item);
                        } else {
                            tempWRTransSendVOs.add(item);
                        }
                    }
                });
                List<WRTransSendVO> sel = transSendVOs.stream()
                        .filter(each -> each.getWr_id().equals(sid)
                        && !result.contains(each))
                        .collect(Collectors.toList());
                if (sel != null && !sel.isEmpty()) {
                    tempWRTransSendVOs = Lists.newArrayList();
                    transSendVOs.forEach(item -> {
                        tempWRTransSendVOs.add(item);
                    });
                    if (selWRTransSendVO != null) {
                        tempSelSid = selWRTransSendVO.getSid();
                    }
                    sel.forEach(item -> {
                        transSendVOs.remove(item);
                    });
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("reCheck Error", e);
        }
    }

    @Override
    public void openerByBtnUp() {
        try {
            //log.info("工作報告查詢進行上一筆動作");
            if (!transSendVOs.isEmpty()) {
                toUp();
                if (selWRTransSendVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selWRTransSendVO.getWr_id());
                    settingReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("工作報告進行上一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByBtnDown() {
        try {
            //log.info("工作報告查詢進行下一筆動作");
            if (!transSendVOs.isEmpty()) {
                toDown();
                if (selWRTransSendVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selWRTransSendVO.getWr_id());
                    settingReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("工作報告查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByDelete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void openerByInvalid() {
        doReloadInfo();
    }

    @Override
    public void openerByCommit() {
        doReloadInfo();
    }

    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    @Override
    public void openerByEditTag() {
        doReloadInfo();
    }

    @Override
    public void openerByTrace() {
        doReloadInfo();
    }

    @Override
    public void openerByFavorite() {
    }

    @Override
    public void openerByTrans() {
        doReloadInfo();
    }

    @Override
    public void openerByReadRecept() {

    }

    public final TraceUpdateCallBack traceUpdateCallBack = new TraceUpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 277989245613089293L;

        @Override
        public void doUpdateData() {
            updateInfoToDataTable(selWr_ID);
            selWr_ID = "";
            DisplayController.getInstance().update(dataTableID);
        }
    };

    public final TransUpdateCallBack transUpdateCallBack = new TransUpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -4512397072115379706L;

        @Override
        public void doUpdateData() {
            updateInfoToDataTable(selWr_ID);
            selWr_ID = "";
            DisplayController.getInstance().update(dataTableID);
        }
    };

}
