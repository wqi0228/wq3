/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.callable;

import com.cy.work.backend.web.logic.components.WRTransSearchWorkReportLogicComponents;
import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.cy.work.backend.web.vo.enums.InboxType;
import com.cy.work.backend.web.vo.enums.TransReadStatus;
import com.cy.work.backend.web.vo.enums.WRFunItemComponentType;
import java.util.Calendar;
import java.util.Date;

/**
 * 收指示計算筆數Call able
 *
 * @author brain0925_liao
 */
public class IncomeInstructionCallable implements HomeCallable {

    /** 登入者Sid */
    private final Integer loginUserSid;
    /** 選單需計算筆數列舉 */
    private final WRFunItemComponentType wrFunItemComponentType;
    /** 選單Group物件 */
    private final WRFunItemGroupVO wrFunItemGroupVO;

    public IncomeInstructionCallable(WRFunItemGroupVO wrFunItemGroupVO,
            WRFunItemComponentType wrFunItemComponentType, Integer loginUserSid) {
        this.wrFunItemGroupVO = wrFunItemGroupVO;
        this.wrFunItemComponentType = wrFunItemComponentType;
        this.loginUserSid = loginUserSid;
    }

    @Override
    public HomeCallableCondition call() throws Exception {
        //計算三天內,工作報告有異動並且需閱讀的資料(未閱讀&待閱讀)
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -3);
        Date startDate = c.getTime();
        Date endDate = new Date();
        Integer count = WRTransSearchWorkReportLogicComponents.getInstance().getIncomeBoxCount(TransReadStatus.NEED_READ.name(), startDate, endDate, loginUserSid, InboxType.INCOME_INSTRUCTION);
        HomeCallableCondition reuslt = new HomeCallableCondition();
        if (count > 0) {
            reuslt.setConntString("(" + String.valueOf(count) + ")");
            reuslt.setCssString("color: red");
        }
        reuslt.setWrFunItemComponentType(wrFunItemComponentType);
        reuslt.setWrFunItemGroupVO(wrFunItemGroupVO);
        return reuslt;
    }

}
