/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.PickListCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WrBasePersonAccessInfoLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class LimitBaseAccessViewUserComponent {

    /** 可調整權限部門 */
    @Getter
    private final List<UserViewVO> limitBaseAccessViewUsers;
    /** 挑選調整權限部門 */
    @Getter
    @Setter
    private UserViewVO selUserViewVO;
    /** 登入者Sid */
    private final Integer loginUserSid;
    /** 登入公司Sid */
    private final Integer loginCompSid;
    /** 訊息CallBack */
    private final MessageCallBack messageCallBack;
    /** 檢視其他基礎單位可閱部門(OrgTree元件) */
    @Getter
    private final List<UserViewVO> viewLimitUserViews;
    /** 挑選其他基礎單位可閱部門(OrgTree元件) */
    private final DepTreeComponent selectPermissionDepTreeComponent;
    /** 挑選可調整權限部門(OrgTree元件) */
    private final DepTreeComponent selectDepTreeComponent;
    /** 全部使用者 */
    private List<Org> allOrgs;
    /** 挑選欲調整權限部門 */
    private List<Org> selectOrgs;

    private List<User> allUsers;
    @Getter
    @Setter
    private String userName;
    @Getter
    private final PickUserComponent pickUserComponent;
    @Getter
    private final String selectDepDialog_Var = "limitBaseAccessUserpickUserSelDepDialogVar";
    @Getter
    private final String pickUserDialogVar = "sel_limitBaseAccessUser_pickUser_dlg_wav";

    public LimitBaseAccessViewUserComponent(Integer loginUserSid,
            Integer loginCompSid, MessageCallBack messageCallBack) {
        this.limitBaseAccessViewUsers = Lists.newArrayList();
        this.messageCallBack = messageCallBack;
        this.loginUserSid = loginUserSid;
        this.loginCompSid = loginCompSid;
        this.viewLimitUserViews = Lists.newArrayList();
        this.selectPermissionDepTreeComponent = new DepTreeComponent();
        this.selectDepTreeComponent = new DepTreeComponent();
        this.selectOrgs = Lists.newArrayList();
        this.allUsers = Lists.newArrayList();
        this.pickUserComponent = new PickUserComponent(loginCompSid, messageCallBack, selectDepDialog_Var, pickListCallBack);
        initOrgTree();
        loadData();
    }

    /** 挑選部門 */
    public void onRowSelect(SelectEvent event) {
        UserViewVO obj = (UserViewVO) event.getObject();
        loadViewUserList(obj);
    }

    /** 載入資料 */
    private void loadData() {
        try {
            this.limitBaseAccessViewUsers.clear();
            this.allOrgs = OrgLogicComponents.getInstance().findOrgsByCompanySid(loginCompSid);
            this.allOrgs.forEach(item -> {
                List<User> depUsers = WRUserLogicComponents.getInstance().findUserByDepSid(item.getSid());
                allUsers.addAll(depUsers);
                depUsers.forEach(userItem -> {
                    List<User> users = WrBasePersonAccessInfoLogicComponents.getInstance().getLimitBaseAccessViewUser(userItem.getSid());
                    UserViewVO uv = new UserViewVO(userItem.getSid(), userItem.getName(), OrgLogicComponents.getInstance().showParentDep(item));
                    if (users != null) {
                        uv.setModifyed(true);
                    }
                    this.limitBaseAccessViewUsers.add(uv);
                });
            });

        } catch (Exception e) {
            log.error("loadData ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void loadPickUser() {
        try {
            Preconditions.checkState(selUserViewVO != null && selUserViewVO.getSid() != null, "請挑選使用者！");
            User selUser = WRUserLogicComponents.getInstance().findUserBySid(selUserViewVO.getSid());
            Org baseOrg = WkOrgUtils.prepareBaseDep(selUser.getPrimaryOrg().getSid(), OrgLevel.MINISTERIAL);
            List<Org> allChildOrg = OrgLogicComponents.getInstance().getAllChildOrgActiveAndInactive(baseOrg);
            allChildOrg.add(baseOrg);
            List<UserViewVO> ps = Lists.newArrayList();
            List<User> viewUsers = WrBasePersonAccessInfoLogicComponents.getInstance().getLimitBaseAccessViewUser(selUser.getSid());
            if (viewUsers != null) {
                viewUsers.forEach(userItem -> {
                    Org dep = OrgLogicComponents.getInstance().findOrgBySid(userItem.getPrimaryOrg().getSid());
                    ps.add(new UserViewVO(userItem.getSid(), userItem.getName(), OrgLogicComponents.getInstance().showParentDep(dep)));
                });
            } else {
                allChildOrg.forEach(item -> {
                    List<User> depUsers = WRUserLogicComponents.getInstance().findUserByDepSid(item.getSid());
                    depUsers.forEach(userItem -> {
                        ps.add(new UserViewVO(userItem.getSid(), userItem.getName(), OrgLogicComponents.getInstance().showParentDep(item)));
                    });
                });
            }
            pickUserComponent.loadUserPicker(ps, selUserViewVO.getSid(), allChildOrg);
            DisplayController.getInstance().showPfWidgetVar("sel_limitBaseAccessUser_pickUser_dlg_wav");
        } catch (Exception e) {
            log.error("loadPickUser ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 取得挑選部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getSelectDepTreeManager() {
        return selectDepTreeComponent.getMultipleDepTreeManager();
    }

    /**
     * 取得挑選權限部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getSelectPermissionDepTreeManager() {
        return selectPermissionDepTreeComponent.getMultipleDepTreeManager();
    }

    /** 載入挑選部門 */
    public void loadSelectDepTree() {
        try {
            Org topOrg = new Org(loginCompSid);
            // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            List<Org> selectDeps = Lists.newArrayList();
            this.allOrgs.forEach(item -> {
                selectDeps.add(item);
            });
            selectDepTreeComponent.init(selectDeps, selectDeps,
                    topOrg, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
        } catch (Exception e) {
            log.error("loadSelectDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 執行挑選部門 */
    public void doSelectDep() {
        try {
            selectOrgs = selectDepTreeComponent.getSelectOrg();
            this.limitBaseAccessViewUsers.clear();
            selectOrgs.forEach(item -> {
                List<User> depUsers = WRUserLogicComponents.getInstance().findUserByDepSid(item.getSid());
                depUsers.forEach(userItem -> {
                    List<User> users = WrBasePersonAccessInfoLogicComponents.getInstance().getLimitBaseAccessViewUser(userItem.getSid());
                    UserViewVO uv = new UserViewVO(userItem.getSid(), userItem.getName(), OrgLogicComponents.getInstance().showParentDep(item));
                    if (users != null) {
                        uv.setModifyed(true);
                    }
                    this.limitBaseAccessViewUsers.add(uv);
                });
            });
            this.selUserViewVO = null;
            viewLimitUserViews.clear();
            DisplayController.getInstance().hidePfWidgetVar("sel_limitBaseAccessUser_dlg_wav");
        } catch (Exception e) {
            log.error("doSelectDep ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void doSearchUserName() {
        try {
            this.limitBaseAccessViewUsers.clear();
            allUsers.forEach(item -> {
                if (Strings.isNullOrEmpty(userName)) {
                    Org dep = OrgLogicComponents.getInstance().findOrgBySid(item.getPrimaryOrg().getSid());
                    List<User> users = WrBasePersonAccessInfoLogicComponents.getInstance().getLimitBaseAccessViewUser(item.getSid());
                    UserViewVO uv = new UserViewVO(item.getSid(), item.getName(), OrgLogicComponents.getInstance().showParentDep(dep));
                    if (users != null) {
                        uv.setModifyed(true);
                    }
                    this.limitBaseAccessViewUsers.add(uv);
                } else if (item.getName().toLowerCase().contains(userName.toLowerCase())) {
                    Org dep = OrgLogicComponents.getInstance().findOrgBySid(item.getPrimaryOrg().getSid());
                    List<User> users = WrBasePersonAccessInfoLogicComponents.getInstance().getLimitBaseAccessViewUser(item.getSid());
                    UserViewVO uv = new UserViewVO(item.getSid(), item.getName(), OrgLogicComponents.getInstance().showParentDep(dep));
                    if (users != null) {
                        uv.setModifyed(true);
                    }
                    this.limitBaseAccessViewUsers.add(uv);
                }
            });
            this.selUserViewVO = null;
            viewLimitUserViews.clear();
        } catch (Exception e) {
            log.error("doSearchUserName ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void clearViewUserList() {
        try {
            Preconditions.checkState(selUserViewVO != null && selUserViewVO.getSid() != null, "請挑選使用者！");
            WrBasePersonAccessInfoLogicComponents.getInstance().clearLimitBaseAccessViewUser(selUserViewVO.getSid(), loginUserSid);
            loadViewUserList(selUserViewVO);
            DisplayController.getInstance().update(":formTemplate:all_permission_tab:limitBaseAccessViewUserID");
            limitBaseAccessViewUsers.forEach(item -> {
                if (item.getSid().equals(selUserViewVO.getSid())) {
                    item.setModifyed(false);
                }
            });
        } catch (Exception e) {
            log.error("clearViewUserList ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 載入檢視權限部門
     *
     * @param selOrgViewVo 挑選的部門物件
     */
    private void loadViewUserList(UserViewVO selUserViewVO) {
        try {
            User selUser = WRUserLogicComponents.getInstance().findUserBySid(selUserViewVO.getSid());
            viewLimitUserViews.clear();
            List<User> viewUsers = WrBasePersonAccessInfoLogicComponents.getInstance().getLimitBaseAccessViewUser(selUser.getSid());
            if (viewUsers != null) {
                viewUsers.forEach(userItem -> {
                    Org dep = OrgLogicComponents.getInstance().findOrgBySid(userItem.getPrimaryOrg().getSid());
                    this.viewLimitUserViews.add(new UserViewVO(userItem.getSid(), userItem.getName(), OrgLogicComponents.getInstance().showParentDep(dep)));
                });
            } else {
                Org baseOrg = WkOrgUtils.prepareBaseDep(selUser.getPrimaryOrg().getSid(), OrgLevel.MINISTERIAL);
                List<Org> allChildOrg = OrgLogicComponents.getInstance().getAllChildOrgActiveAndInactive(baseOrg);
                allChildOrg.add(baseOrg);
                allChildOrg.forEach(item -> {
                    List<User> depUsers = WRUserLogicComponents.getInstance().findUserByDepSid(item.getSid());
                    depUsers.forEach(userItem -> {
                        this.viewLimitUserViews
                                .add(new UserViewVO(userItem.getSid(), userItem.getName(), OrgLogicComponents.getInstance().showParentDep(item)));
                    });
                });
            }
        } catch (Exception e) {
            log.error("loadViewDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 載入挑選權限部門 */
    public void loadPermissionDepTree() {
        loadSelectPermissionDepTree();
    }

    /** 執行挑選權限部門 */
    public void doSelectPermissionDepTree() {
        try {
//            Preconditions.checkState(selOrgViewVo != null && selOrgViewVo.getSid() != null, "請挑選部門！");
//
//            WrBaseDepAccessInfoLogicComponents.getInstance().saveLimitBaseAccessViewDep(selOrgViewVo.getSid(), loginUserSid, selectPermissionDepTreeComponent.getSelectOrg());
//            loadViewDepTree(selOrgViewVo);
//            DisplayController.getInstance().hidePfWidgetVar("sel_limitBaseAccessDep_permission_dlg_wav");
        } catch (Exception e) {
            log.error("doSelectPermissionDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 執行載入挑選權限部門 */
    private void loadSelectPermissionDepTree() {
        try {
//            Preconditions.checkState(selOrgViewVo != null && selOrgViewVo.getSid() != null, "請挑選部門！");
//            Org topOrg = new Org(loginCompSid);
//            //僅檢視,故切換顯示停用部門時,不需清除以挑選部門
//            boolean clearSelNodeWhenChaneDepTree = true;
//            boolean selectable = true;
//            boolean selectModeSingle = false;
//            Org baseOrg = OrgLogicComponents.getInstance().getBaseOrg(selOrgViewVo.getSid());
//            List<Org> allChildOrg = OrgLogicComponents.getInstance().getAllChildOrgActiveAndInactive(baseOrg);
//            allChildOrg.add(baseOrg);
//            List<Org> limitBaseAccessViewDeps = WrBaseDepAccessInfoLogicComponents.getInstance().getLimitBaseAccessViewDep(selOrgViewVo.getSid());
//            List<Org> showOrgs = Lists.newArrayList();
//            showOrgs.addAll(allChildOrg);
//            allChildOrg.forEach(item -> {
//                List<Org> allParents = OrgLogicComponents.getInstance().getAllParentOrgs(item);
//                if (allParents == null || allParents.isEmpty()) {
//                    return;
//                }
//                showOrgs.addAll(allParents.stream()
//                        .filter(each -> !showOrgs.contains(each))
//                        .collect(Collectors.toList()));
//            });
//            try {
//                Collections.sort(showOrgs, new Comparator<Org>() {
//                    @Override
//                    public int compare(Org o1, Org o2) {
//                        return o1.getSid() - o2.getSid();
//                    }
//                });
//            } catch (Exception e) {
//                log.error("sort Error", e);
//            }
////            List<Org> otherBaseAccessShowDeps = Lists.newArrayList();
////            this.allOrgs.forEach(item -> {
////                if (!allChildOrg.contains(item)) {
////                    otherBaseAccessShowDeps.add(item);
////                }
////            });
//            selectPermissionDepTreeComponent.init(showOrgs, allChildOrg,
//                    topOrg, ((limitBaseAccessViewDeps == null) ? allChildOrg : limitBaseAccessViewDeps),
//                    clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
        } catch (Exception e) {
            log.error("loadSelectPermissionDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /** 建立Empty 挑選權限部門Tree Manager */
    private void initSelectPermissionDepTreeComponent() {
        Org topOrg = new Org(loginCompSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectPermissionDepTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                topOrg, Lists.newArrayList(), clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    /** 建立Empty 挑選部門Tree Manager */
    private void initSelectDepTreeComponent() {
        Org topOrg = new Org(loginCompSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectDepTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                topOrg, Lists.newArrayList(), clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    private void initOrgTree() {
        initSelectPermissionDepTreeComponent();
        initSelectDepTreeComponent();
    }

    private final PickListCallBack pickListCallBack = new PickListCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -8223389950767431267L;

        @Override
        public void saveSelectUser(List<UserViewVO> users) {
            try {
                Preconditions.checkState(selUserViewVO != null && selUserViewVO.getSid() != null, "請挑選使用者！");
                List<User> saveUsers = Lists.newArrayList();
                users.forEach(item -> {
                    saveUsers.add(WRUserLogicComponents.getInstance().findUserBySid(item.getSid()));
                });
                WrBasePersonAccessInfoLogicComponents.getInstance().saveLimitBaseAccessViewUser(selUserViewVO.getSid(), loginUserSid, saveUsers);
                loadViewUserList(selUserViewVO);
                DisplayController.getInstance().hidePfWidgetVar(pickUserDialogVar);
                limitBaseAccessViewUsers.forEach(item -> {
                    if (item.getSid().equals(selUserViewVO.getSid())) {
                        item.setModifyed(true);
                    }
                });

            } catch (Exception e) {
                log.error("saveSelectUser ERROR", e);
                messageCallBack.showMessage(e.getMessage());
            }

        }
    };
}
