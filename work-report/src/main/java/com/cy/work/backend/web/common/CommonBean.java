/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.cy.commons.util.WebVersion;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

import javax.faces.bean.ManagedBean;

/**
 * common
 */
@Controller
@Scope(WebApplicationContext.SCOPE_APPLICATION)
@Slf4j
@ManagedBean
public class CommonBean implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = -5330936470702228266L;
    private static CommonBean instance;

    public static CommonBean getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        CommonBean.instance = this;
    }

    /**
     * for 清除 client 端 js 和 css 的快取用 (更換版本號即更新)
     * 
     * @return
     */
    public String getVersion() {

        try {
            // return PomPropertiesLoader.getProperty("version");
            // 改吃 common-util
            return WebVersion.getInstance().getWebVersion();
        } catch (Exception e) {
            log.warn("取得系統版本別失敗![" + e.getMessage() + "]", e);
            return new Date().getTime() + "";
        }
    }

    /**
     * for 清除 client 端 js 和 css 的快取用 (每次刷新頁面即更新)
     * 
     * @return
     */
    public String getSysTime() {
        return System.currentTimeMillis() + "";
    }

    /**
     * 需強制 post back ,但又不需做任何事時使用
     */
    public void doNottingPostback() {
    }
}
