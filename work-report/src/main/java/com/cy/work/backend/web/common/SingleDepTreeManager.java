/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import java.util.Arrays;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.TreeNode;

/**
 * 單選部門元件 Brain_liao
 */
@Slf4j
public class SingleDepTreeManager extends BaseDepTreeManager {

    
    private static final long serialVersionUID = 4336452896434511448L;

    public SingleDepTreeManager() {
    }

    public TreeNode getSelNode() {
        if (selNode.size() > 0) {
            return selNode.get(0);
        } else {
            return null;
        }
    }

    public void setSelNode(TreeNode selNode) {
        if (selNode == null) {
            log.error("setSelNode:selNode is null");
            return;
        }
        log.error("setSelNode:selNode" + selNode);
        this.selNode = Arrays.asList(selNode);
    }
}
