/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.vo.User;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.TransCancelUpdateCallBack;
import com.cy.work.backend.web.logic.components.WRReadLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WRInboxLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.InBoxGroupItemVO;
import com.cy.work.backend.web.view.vo.InBoxItemVO;
import com.cy.work.backend.web.view.vo.SentBackReadStatusVO;
import com.cy.work.backend.web.vo.enums.InboxType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class WorkInboxComponent implements Serializable {

    
    private static final long serialVersionUID = 3499200186795397931L;
    @Getter
    private List<InBoxItemVO> incomeReports;
    @Getter
    private List<InBoxItemVO> incomeInstructions;
    @Getter
    private List<InBoxItemVO> incomeMembers;
    @Getter
    private List<InBoxGroupItemVO> sentBackups;
    @Getter
    private List<SentBackReadStatusVO> sentBackReadStatus;
    @Setter
    @Getter
    private List<SentBackReadStatusVO> selSentBackReadStatus;

    private Integer loginUserSid;

    private String wr_id;
    @Getter
    private boolean showIncomeReport;
    @Getter
    private boolean showIncomeInstructions;
    @Getter
    private boolean showIncomeMember;
    @Getter
    private boolean showSentBackup;
    @Getter
    private String needToReadPersonNumber;
    @Getter
    private String readedPersonNumber;
    @Getter
    private String cancelPersonNumber;
    @Getter
    private boolean depSentBackReadStatus;

    private TransCancelUpdateCallBack transCancelUpdateCallBack;

    private MessageCallBack messageCallBack;

    private boolean showSendTab = false;

    public void openSendTab() {
        showSendTab = true;
    }

    public WorkInboxComponent(Integer loginUserSid, TransCancelUpdateCallBack transCancelUpdateCallBack, MessageCallBack messageCallBack) {
        this.loginUserSid = loginUserSid;
        this.messageCallBack = messageCallBack;
        this.transCancelUpdateCallBack = transCancelUpdateCallBack;
    }

    public void loadData(String wr_id) {
        this.wr_id = wr_id;

        loadData();
    }

    public void clickGroupToStatus(String inbox_group_sid) {
        this.selSentBackReadStatus = null;
        sentBackReadStatus = WRInboxLogicComponents.getInstance().getSentBackReadStatusVOByInboxGroupSid(inbox_group_sid);
        List<SentBackReadStatusVO> deps = sentBackReadStatus.stream().filter(each -> each.isTypeDep())
                .collect(Collectors.toList());
        if (deps != null && !deps.isEmpty()) {
            depSentBackReadStatus = true;
        } else {
            depSentBackReadStatus = false;
        }
        List<User> readedUser = WRReadLogicComponents.getInstance().getReadUser(wr_id);
        Integer needToReadCount = 0;
        Integer readedCount = 0;
        Integer cancelCount = 0;
        for (SentBackReadStatusVO item : sentBackReadStatus) {
            try {
                if (depSentBackReadStatus) {
                    List<User> depUsers = WRUserLogicComponents.getInstance().findUserByDepSid(item.getDepSid());
                    needToReadCount = needToReadCount + depUsers.size();
                    if (Strings.isNullOrEmpty(item.getCancelTime())) {
                        List<User> readDepUsers = readedUser.stream().filter(each -> each.getPrimaryOrg() != null && each.getPrimaryOrg().getSid().equals(item.getDepSid()))
                                .collect(Collectors.toList());
                        readedCount = readedCount + readDepUsers.size();
                    } else {
                        cancelCount = cancelCount + depUsers.size();
                    }
                } else {
                    needToReadCount = needToReadCount + 1;
                    if (Strings.isNullOrEmpty(item.getCancelTime())) {
                        if (!Strings.isNullOrEmpty(item.getReadTime())) {
                            readedCount = readedCount + 1;
                        }
//                        List<User> readDepUsers = readedUser.stream().filter(each -> each.getSid().equals(item.getUserSid()))
//                                .collect(Collectors.toList());
//                        if (readDepUsers != null && !readDepUsers.isEmpty()) {
//                            readedCount = readedCount + 1;
//                        }
                    } else {
                        cancelCount = cancelCount + 1;
                    }
                }
            } catch (Exception e) {
                log.error("clickGroupToStatus", e);
            }
        }
        needToReadCount = needToReadCount - cancelCount;
        needToReadPersonNumber = String.valueOf(needToReadCount);
        readedPersonNumber = String.valueOf(readedCount);
        cancelPersonNumber = String.valueOf(cancelCount);
        DisplayController.getInstance().update("dlgIssueSentBackup_view");
        DisplayController.getInstance().update("dlgIssueSentBackup_view_dtInbox");
        DisplayController.getInstance().showPfWidgetVar("dlgIssueSentBackup");
    }

    public void cancelWorkInBox() {
        try {
            List<String> sids = Lists.newArrayList();
            selSentBackReadStatus.forEach(item -> {
                sids.add(item.getInbox_sid());
            });
            WRInboxLogicComponents.getInstance().doCancelWorkInBox(sids, loginUserSid);
            transCancelUpdateCallBack.doUpdateData();
            DisplayController.getInstance().hidePfWidgetVar("dlgIssueSentBackup");
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
            log.error("cancelWorkInBox", e);
        }
    }

    public void loadData() {
        List<InBoxItemVO> personInBoxItems
                = WRInboxLogicComponents.getInstance().getWorkInboxByReceiveUserSidForPerson(wr_id, loginUserSid);
        if (incomeReports != null) {
            incomeReports.clear();
        }
        if (incomeInstructions != null) {
            incomeInstructions.clear();
        }
        if (incomeMembers != null) {
            incomeMembers.clear();
        }
        incomeReports = personInBoxItems.stream().filter(each -> each.getInbox_type().equals(InboxType.INCOME_REPORT))
                .collect(Collectors.toList());
        incomeInstructions = personInBoxItems.stream().filter(each -> each.getInbox_type().equals(InboxType.INCOME_INSTRUCTION))
                .collect(Collectors.toList());
        incomeMembers = personInBoxItems.stream().filter(each -> each.getInbox_type().equals(InboxType.INCOME_MEMBER))
                .collect(Collectors.toList());
        if (incomeReports == null || incomeReports.isEmpty()) {
            showIncomeReport = false;
        } else {
            showIncomeReport = true;
        }
        if (incomeInstructions == null || incomeInstructions.isEmpty()) {
            showIncomeInstructions = false;
        } else {
            showIncomeInstructions = true;
        }
        if (incomeMembers == null || incomeMembers.isEmpty()) {
            showIncomeMember = false;
        } else {
            showIncomeMember = true;
        }
        if (!showSendTab) {
            sentBackups = Lists.newArrayList();
            showSentBackup = false;
            return;
        }
        sentBackups = WRInboxLogicComponents.getInstance().getInBoxGroupItemBySendUserSid(wr_id, loginUserSid);
        if (sentBackups == null || sentBackups.isEmpty()) {
            showSentBackup = false;
        } else {
            showSentBackup = true;
        }
    }

}
