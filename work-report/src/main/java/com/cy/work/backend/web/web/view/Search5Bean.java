/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.listener.DataTableReLoadCallBack;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.TableUpDownListener;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRFavoriteLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WRReportCustomColumnLogicComponents;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.CustomColumnComponent;
import com.cy.work.backend.web.view.components.WorkReportSearchHeaderComponent;
import com.cy.work.backend.web.view.vo.FavoriteSearchVO;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRFavoriteSearchColumnVO;
import com.cy.work.backend.web.view.vo.WorkReportSidTo;
import com.cy.work.backend.web.vo.enums.WRFavoriteSearchColumn;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Search5Bean implements Serializable, TableUpDownListener {


    private static final long serialVersionUID = -1108490429020717391L;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Getter
    private CustomColumnComponent customColumn;
    /** view 顯示錯誤訊息 */
    @Getter
    private String errorMessage;
    @Getter
    private final String dataTableID = "dataTableFavorite";
    @Getter
    private final String dataTableWv = "dataTableFavoriteWv";
    @Setter
    @Getter
    private boolean showFrame = false;
    @Getter
    private WorkReportSearchHeaderComponent workReportSearchHeaderComponent;
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private boolean showCancelBtn = false;
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Getter
    private String iframeUrl = "";
    @Getter
    private WRFavoriteSearchColumnVO searchColumnVO;
    @Autowired
    private WRReportCustomColumnLogicComponents wrReportCustomColumnLogicComponents;
    @Autowired
    private WRFavoriteLogicComponents wRFavoriteLogicComponents;
    @Getter
    private List<FavoriteSearchVO> favoriteSearchVOs;
    @Setter
    @Getter
    private FavoriteSearchVO selFavoriteSearchVO;

    private List<FavoriteSearchVO> tempFavoriteSearchVOs;

    private String tempSid;

    @PostConstruct
    public void init() {
        userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        depViewVo = OrgLogicComponents.getInstance().findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = OrgLogicComponents.getInstance().findById(SecurityFacade.getCompanyId());
        searchColumnVO = wrReportCustomColumnLogicComponents.getWRFavoriteSearchColumnSetting(userViewVO.getSid());
        customColumn = new CustomColumnComponent(WRReportCustomColumnUrlType.FAVORITE_SEARCH, Arrays.asList(WRFavoriteSearchColumn.values()), searchColumnVO.getPageCount(), userViewVO.getSid(), messageCallBack, dataTableReLoadCallBack);
        workReportSearchHeaderComponent = new WorkReportSearchHeaderComponent();
        settingDefault();
        doSearch();
    }

    public void clear() {
        settingDefault();
        selFavoriteSearchVO = null;
        doSearch();
        selectCell();
    }

    public void selectCell() {
        showCancelBtn = (selFavoriteSearchVO != null);
    }

    public void cancelFavorite() {
        try {
            wRFavoriteLogicComponents.removeFavorite(selFavoriteSearchVO.getWr_sid(), selFavoriteSearchVO.getWr_no(), userViewVO.getSid());
            updateListByWrID(selFavoriteSearchVO.getWr_sid());
            selFavoriteSearchVO = null;
            DisplayController.getInstance().update(dataTableID);
            selectCell();
        } catch (Exception e) {
            log.error("cancelFavorite", e);
            messageCallBack.showMessage("收藏取消失敗,請重新點選一次");
        }
    }

    private void settingDefault() {
        workReportSearchHeaderComponent.settingDefault();
        workReportSearchHeaderComponent.changeDateIntervalThisMonth();
    }

    public void hideColumnContent() {
        hasDisplay = false;
    }

    public void openCustomColumn() {
        try {
            customColumn.loadData();
            DisplayController.getInstance().update("dlgDefineField_view");
            DisplayController.getInstance().showPfWidgetVar("dlgDefineField");
        } catch (Exception e) {
            log.error("openCustomColumn", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenUrl(String sid) {
        try {
            settintSelFavoriteSearchVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("btnOpenUrl", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.error("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void settingPreviousAndNext() {
        int index = favoriteSearchVOs.indexOf(selFavoriteSearchVO);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(favoriteSearchVOs.get(index - 1).getWr_sid());
        }
        if (index == favoriteSearchVOs.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(favoriteSearchVOs.get(index + 1).getWr_sid());
        }
    }

    private void settintSelFavoriteSearchVO(String sid) {
        FavoriteSearchVO sel = new FavoriteSearchVO(sid);
        int index = favoriteSearchVOs.indexOf(sel);
        if (index > 0) {
            selFavoriteSearchVO = favoriteSearchVOs.get(index);
        } else {
            selFavoriteSearchVO = favoriteSearchVOs.get(0);
        }
    }

    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            settintSelFavoriteSearchVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp_iframe.xhtml?wrcId=" + selFavoriteSearchVO.getWr_sid();
            tableUpDownBean.setWorp_path("worp_iframe");
        } catch (Exception e) {
            log.error("btnOpenFrame", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        WkJsonUtils jsonUtils = SpringContextHolder.getBean(WkJsonUtils.class);
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateListByWrID(workReportSidTos.get(0).getSid());
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception ex) {
            log.error("doReloadInfo", ex);
        }
    }

    private void updateListByWrID(String wrID) {
        try {
            List<FavoriteSearchVO> update = runSearch(wrID);
            if (update == null || update.isEmpty()) {
                tempFavoriteSearchVOs = Lists.newArrayList();
                if (favoriteSearchVOs != null && !favoriteSearchVOs.isEmpty()) {
                    favoriteSearchVOs.forEach(item -> {
                        tempFavoriteSearchVOs.add(item);
                    });
                    if (selFavoriteSearchVO != null) {
                        tempSid = selFavoriteSearchVO.getFavorite_sid();
                    }
                    List<FavoriteSearchVO> sel = favoriteSearchVOs.stream()
                            .filter(each -> each.getWr_sid().equals(wrID))
                            .collect(Collectors.toList());
                    if (sel != null && !sel.isEmpty()) {
                        sel.forEach(item -> {
                            favoriteSearchVOs.remove(item);
                        });
                    }
                }
            } else {
                update.forEach(newItem -> {
                    int index = favoriteSearchVOs.indexOf(newItem);
                    if (index >= 0) {
                        favoriteSearchVOs.get(index).replaceValue(newItem);
                    } else {
                        favoriteSearchVOs.add(newItem);
                    }
                    if (tempFavoriteSearchVOs != null && !tempFavoriteSearchVOs.isEmpty()) {
                        int indext = tempFavoriteSearchVOs.indexOf(newItem);
                        if (indext >= 0) {
                            tempFavoriteSearchVOs.get(indext).replaceValue(newItem);
                        } else {
                            tempFavoriteSearchVOs.add(newItem);
                        }
                    }
                });
            }
        } catch (Exception e) {
            log.error("updateList", e);
        }
    }

    public void doSearch() {
        favoriteSearchVOs = runSearch("");
        if (favoriteSearchVOs.size() > 0) {
            DisplayController.getInstance().execute("selectDataTablePage('" + dataTableWv + "',0);");
        }
    }

    private List<FavoriteSearchVO> runSearch(String wr_ID) {
        try {
            return wRFavoriteLogicComponents.findFavoriteByUserSidAndUpdateDtAndText(userViewVO.getSid(), workReportSearchHeaderComponent.getStartDate(), workReportSearchHeaderComponent.getEndDate(), workReportSearchHeaderComponent.getTitle(), wr_ID);
        } catch (Exception e) {
            log.error("runSearch", e);
        }
        return Lists.newArrayList();
    }

    @Override
    public void openerByBtnUp() {
        try {
            //log.info("工作報告收藏夾查詢進行上一筆動作");
            if (!favoriteSearchVOs.isEmpty()) {
                toUp();
                if (selFavoriteSearchVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selFavoriteSearchVO.getWr_sid());
                }
            }
            DisplayController.getInstance().update(dataTableID);
            //log.info("工作報告收藏夾查詢進行上一筆動作-結束");
        } catch (Exception e) {
            log.error("工作報告收藏夾查詢進行上一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByBtnDown() {
        try {
            //log.info("工作報告收藏夾查詢進行下一筆動作");
            if (!favoriteSearchVOs.isEmpty()) {
                toDown();
                if (favoriteSearchVOs != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selFavoriteSearchVO.getWr_sid());
                }
            }
            DisplayController.getInstance().update(dataTableID);
            //log.info("工作報告收藏夾查詢進行下一筆動作-結束");
        } catch (Exception e) {
            log.error("工作報告收藏夾查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    private void toDown() {
        try {
            if (tempFavoriteSearchVOs != null && !tempFavoriteSearchVOs.isEmpty() && !Strings.isNullOrEmpty(tempSid)) {
                int index = tempFavoriteSearchVOs.indexOf(new FavoriteSearchVO(tempSid));
                if (index >= 0) {
                    selFavoriteSearchVO = tempFavoriteSearchVOs.get(index + 1);
                } else {
                    selFavoriteSearchVO = favoriteSearchVOs.get(favoriteSearchVOs.size() - 1);
                }
                tempFavoriteSearchVOs = null;
                tempSid = "";
            } else {
                int index = favoriteSearchVOs.indexOf(selFavoriteSearchVO);
                if (index >= 0) {
                    selFavoriteSearchVO = favoriteSearchVOs.get(index + 1);
                } else {
                    selFavoriteSearchVO = favoriteSearchVOs.get(favoriteSearchVOs.size() - 1);
                }
            }
        } catch (Exception e) {
            log.error("toDown", e);
            tempFavoriteSearchVOs = null;
            tempSid = "";
            if (favoriteSearchVOs.size() > 0) {
                selFavoriteSearchVO = favoriteSearchVOs.get(favoriteSearchVOs.size() - 1);
            }
        }
    }

    private void toUp() {
        try {
            if (tempFavoriteSearchVOs != null && !tempFavoriteSearchVOs.isEmpty() && !Strings.isNullOrEmpty(tempSid)) {
                int index = tempFavoriteSearchVOs.indexOf(new FavoriteSearchVO(tempSid));
                if (index > 0) {
                    selFavoriteSearchVO = tempFavoriteSearchVOs.get(index - 1);
                } else {
                    selFavoriteSearchVO = favoriteSearchVOs.get(0);
                }
                tempFavoriteSearchVOs = null;
                tempSid = "";
            } else {
                int index = favoriteSearchVOs.indexOf(selFavoriteSearchVO);
                if (index > 0) {
                    selFavoriteSearchVO = favoriteSearchVOs.get(index - 1);
                } else {
                    selFavoriteSearchVO = favoriteSearchVOs.get(0);
                }
            }
        } catch (Exception e) {
            log.error("toUp", e);
            tempSid = "";
            tempFavoriteSearchVOs = null;
            if (!favoriteSearchVOs.isEmpty()) {
                selFavoriteSearchVO = favoriteSearchVOs.get(0);
            }
        }
    }

    @Override
    public void openerByDelete() {
    }

    @Override
    public void openerByInvalid() {
    }

    @Override
    public void openerByCommit() {
    }

    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    @Override
    public void openerByEditTag() {
    }

    @Override
    public void openerByTrace() {
    }

    @Override
    public void openerByFavorite() {
        doReloadInfo();
    }

    @Override
    public void openerByTrans() {
    }
    
    @Override
    public void openerByReadRecept() {

    }

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -6466045220240731885L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    };

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 7438749098658617258L;

        @Override
        public void reload() {
            searchColumnVO = wrReportCustomColumnLogicComponents.getWRFavoriteSearchColumnSetting(userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };
}
