/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.work.common.vo.basic.BasicAttachmentService;
import com.cy.work.backend.web.logic.manager.WRAttachmentManager;
import com.cy.work.backend.web.logic.manager.WRMasterCustomLogicManager;
import com.cy.work.backend.web.logic.utils.attachment.AttachmentUtils;
import com.cy.work.backend.web.logic.vo.AttachmentVO;
import com.cy.work.backend.web.vo.WRAttachment;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 工作報告附件邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class WRAttachmentLogicComponents implements BasicAttachmentService<AttachmentVO, WRAttachment, String>,InitializingBean, Serializable {


    private static final long serialVersionUID = -1158005423692615077L;
    private static WRAttachmentLogicComponents instance;

    public static WRAttachmentLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRAttachmentLogicComponents.instance = this;
    }
    
    @Override
    public File getServerSideFile(AttachmentVO attachment) {
        return AttachmentUtils.getServerSideFile(attachment, "wrMaster");
    }

    @Autowired
    private WRAttachmentManager wRAttachmentManager;
    @Autowired
    private WRMasterCustomLogicManager wRMasterCustomLogicManager;

    /**
     * 取得附件物件 By 附件Sid
     *
     * @param att_sid
     * @return
     */
    public WRAttachment getWRAttachmentBySid(String att_sid) {
        return wRAttachmentManager.findBySid(att_sid);
    }

    /**
     * 取得附件介面物件 By 附件Sid
     *
     * @param att_sid
     * @return
     */
    public AttachmentVO getAttachmentVOBySid(String att_sid) {
        WRAttachment wa = wRAttachmentManager.findBySid(att_sid);
        return transWRAttachmentToAttachmentVO(wa);
    }

    /**
     * 將附件物件轉換成附件界面物件
     *
     * @param wRAttachment 附件物件
     * @return
     */
    private AttachmentVO transWRAttachmentToAttachmentVO(WRAttachment wRAttachment) {
        return wRAttachmentManager.transWRAttachmentToAttachmentVO(wRAttachment);
    }

    /**
     * 取得附件界面物件By 工作報告Sid
     *
     * @param wr_sid 工作報告Sid
     * @return
     */
    public List<AttachmentVO> getAttachmentsByWRSid(String wr_sid) {
        List<WRAttachment> as = wRAttachmentManager.getWRAttachmentByWRSid(wr_sid);
        if (as == null) {
            return Lists.newArrayList();
        }
        List<AttachmentVO> avs = Lists.newArrayList();
        as.forEach(item -> {
            avs.add(transWRAttachmentToAttachmentVO(item));
        });
        return avs;
    }

    /**
     * 建立附件
     *
     * @param wr_id 工作報告Sid
     * @param wr_no 工作報告單號
     * @param fileName 附件名稱
     * @param create_uerSid 建立者Sid
     * @param departmetSid 建立者部門Sid
     * @return
     */
    public AttachmentVO createAttachment(String wr_id, String wr_no, String fileName, Integer create_uerSid, Integer departmetSid) {
        return wRAttachmentManager.createAttachment(wr_id, wr_no, fileName, create_uerSid, departmetSid);
    }

    /**
     * 更新附件資訊
     *
     * @param wr_sid 工作報告Sid
     * @param wr_no 工作報告單號
     * @param description 描述
     * @param userSid 更新者Sid
     * @param attachment_sid 附件Sid
     */
    public void updateWRMasterMappingInfo(String wr_sid, String wr_no, String description, Integer userSid, String attachment_sid) {
        wRAttachmentManager.updateWRMasterMappingInfo(wr_sid, wr_no, description, userSid, attachment_sid);
    }

    /**
     * 更新附件描述
     *
     * @param att_sid 附件Sid
     * @param desc 描述
     * @param update_userSid 更新者Sid
     * @return
     */
    public AttachmentVO updateAttachment(String att_sid, String desc, Integer update_userSid) {
        WRAttachment wa = wRAttachmentManager.findBySid(att_sid);
        if (wa == null) {
            return null;
        }
        wa.setDescription(desc);
        WRAttachment wr = wRAttachmentManager.update(wa, update_userSid);
        if (wr == null) {
            return null;
        }
        return new AttachmentVO(wr.getSid(), wr.getFile_name(), wr.getDescription(), wr.getFile_name(), true, String.valueOf(wr.getCreate_usr_sid()));
    }

    public void deleteAttachment(AttachmentVO attachmentVO, Integer loginUserSid, String wr_sid) {
        wRMasterCustomLogicManager.deleteAttachment(attachmentVO, loginUserSid, wr_sid);
    }
    
    @Override
    public AttachmentVO findBySid(String att_sid) {
        WRAttachment wa = wRAttachmentManager.findBySid(att_sid);
        return wRAttachmentManager.transWRAttachmentToAttachmentVO(wa);
    }

    @Override
    public List<AttachmentVO> findAttachsByLazy(WRAttachment e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AttachmentVO> findAttachs(WRAttachment e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDirectoryName() {
        return "WRAttachment";
    }
    
    @Override
    public String getAttachPath() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AttachmentVO createEmptyAttachment(String fileName, Integer executor, Integer dep) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AttachmentVO updateAttach(AttachmentVO attach) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveAttach(AttachmentVO attach) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveAttach(List<AttachmentVO> attachs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String depAndUserName(AttachmentVO attachment) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void linkRelation(List<AttachmentVO> attachments, WRAttachment entity, Integer login) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void linkRelation(AttachmentVO ra, WRAttachment entity, Integer login) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeStatusToInActive(List<AttachmentVO> attachments, AttachmentVO attachment, WRAttachment entity, Integer login) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
