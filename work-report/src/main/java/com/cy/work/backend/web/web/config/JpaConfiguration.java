/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.config;

import com.cy.work.common.vo.converter.InstanceStatusConverter;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author brain0925_liao
 */
@Configuration
@EnableJpaRepositories({"com.cy.work.backend.web.repository"})
@EnableTransactionManagement
public class JpaConfiguration {

    @Autowired
    private WRProperties properties;
    
    @Bean
    public JdbcTemplate reqJdbcTemplate() {
        JdbcTemplate template = new JdbcTemplate();
        template.setDataSource(this.sqlDataSource());
        return template;
    }

    @Bean
    public DataSource sqlDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(properties.getClassName());
        hikariConfig.setJdbcUrl(properties.getDbUrl());
        hikariConfig.setUsername(properties.getDbUserName());
        hikariConfig.setPassword(properties.getDbPassword());
        hikariConfig.setMaximumPoolSize(properties.getMaximumPoolSize());
        hikariConfig.setMinimumIdle(properties.getMinimumIdle());
        hikariConfig.setIdleTimeout(properties.getIdleTimeout());
        hikariConfig.setMaxLifetime(properties.getMaxLifetime());
        hikariConfig.addDataSourceProperty("dataSource.cachePrepStmts", properties.getCachePrepStmts());
        hikariConfig.addDataSourceProperty("dataSource.useServerPrepStmts", properties.getUseServerPrepStmts());
        hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSize", properties.getPrepStmtCacheSize());
        hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSqlLimit", properties.getPrepStmtCacheSqlLimit());
        hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.setPoolName("springHikariCP");
        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        return dataSource;
    }

    @Bean
    public Map<String, Object> jpaProperties() {
        Map<String, Object> props = new HashMap<>();
        props.put("hibernate.dialect", properties.getDialect());
        props.put("hibernate.show_sql", properties.isShow_sql());
        props.put("hibernate.format_sql", true);
        if(properties.isShow_sql()) {
            props.put("hibernate.format_sql", true);
        }
        return props;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
        lef.setPackagesToScan(new String[]{
              "com.cy.work.backend.web.vo",
            "com.cy.commons.vo",
            "com.cy.system.vo"});

        lef.setDataSource(sqlDataSource());
        lef.setJpaDialect(new HibernateJpaDialect());
        lef.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        lef.setJpaPropertyMap(this.jpaProperties());
        return lef;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory().getObject());
    }

    @Bean
    public InstanceStatusConverter instanceStatusConverter() {
        return new InstanceStatusConverter();
    }
}
