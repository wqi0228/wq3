/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.ReLoadCallBack;
import com.cy.work.backend.web.listener.TraceUpdateCallBack;
import com.cy.work.backend.web.logic.components.WorkTraceInfoLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 追蹤功能物件
 * @author brain0925_liao
 */
@Slf4j
public class WRTraceComponent implements Serializable {
    
    private static final long serialVersionUID = -7092360819880875508L;
    /**邀情其他人追蹤功能物件*/
    @Getter
    private WorkReportTraceOtherPersonComponent workReportTraceOtherPersonComponent;
    /**登入者Sid*/
    private Integer loginUserSid;
    /**訊息CallBack*/
    private MessageCallBack messageCallBack;
    /**工作報告Sid*/
    private String wr_Id;
    /**工作報告單號*/
    private String wr_no;
    /**追蹤內容*/
    @Getter
    @Setter
    private String memo;
    /**追蹤日期期*/
    @Getter
    @Setter
    private Date noticeDate;
    /**工作報告主題*/
    @Getter
    private String title;
    @Getter
    @Setter
    /** 追蹤方式(0：自行追蹤、1：邀請其他人追蹤、2：所屬部門追蹤) */
    private String traceRadio;
    /**該工作報告已追蹤人員*/
    @Getter
    private List<UserViewVO> traceUsers;
    /**該工作報告已追蹤部門*/
    @Getter
    private List<OrgViewVo> traceDeps;
    /**追蹤更新CallBack*/
    private TraceUpdateCallBack traceUpdateCallBack;

    public WRTraceComponent(Integer loginUserSid, MessageCallBack messageCallBack, TraceUpdateCallBack traceUpdateCallBack) {
        this.loginUserSid = loginUserSid;
        this.traceUpdateCallBack = traceUpdateCallBack;
        this.messageCallBack = messageCallBack;
        this.traceUsers = Lists.newArrayList();
        this.workReportTraceOtherPersonComponent = new WorkReportTraceOtherPersonComponent(loginUserSid, messageCallBack, traceReloadCallBack, closeCallBack);
    }
    /**
     * 載入追蹤資料
     * @param wr_Id 工作報告Sid
     * @param wr_no 工作報告單號
     * @param title 工作報告主題
     */
    public void loadData(String wr_Id, String wr_no, String title) {
        this.traceRadio = "0";
        this.wr_Id = wr_Id;
        this.noticeDate = null;
        this.memo = "";
        this.wr_no = wr_no;
        this.title = title;
        loadTraceUsers();
        loadTraceDep();
    }
    
    /**關閉追蹤Dialog*/
    public void close() {
        clear();
        DisplayController.getInstance().hidePfWidgetVar("dlgTraceAction");
    }
 
    /**清除追蹤記憶體資料*/
    private void clear() {
        traceUsers.clear();
        traceDeps.clear();
        DisplayController.getInstance().update("dlgTraceAction_view_traceRadio_panel_id");
    }

    /**載入該工作報告已追蹤人員*/
    private void loadTraceUsers() {
        traceUsers = WorkTraceInfoLogicComponents.getInstance().getWorkTraceInfoByWr_SidForPerson(wr_Id);
    }
    
    /**載入該工作報告已追蹤部門*/
    private void loadTraceDep() {
        traceDeps = WorkTraceInfoLogicComponents.getInstance().getWorkTraceInfoByWr_SidForDep(wr_Id);
    }

    /**儲存追蹤資料*/
    public void saveTrace() {
        try {
            if ("0".equals(traceRadio)) {
                WorkTraceInfoLogicComponents.getInstance().savetWorkTraceInfoForPerson(loginUserSid, wr_Id, wr_no, memo, noticeDate);
            } else if ("1".equals(traceRadio)) {
                messageCallBack.showMessage("您尚未選擇資料");
                return;
            } else if ("2".equals(traceRadio)) {
                WorkTraceInfoLogicComponents.getInstance().savetWorkTraceInfoForDep(loginUserSid, wr_Id, wr_no, memo, noticeDate);
            }
            close();
            traceUpdateCallBack.doUpdateData();
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
            log.error("saveTrace", e);
        }
    }

    /**
     * 變更追蹤方式
     */
    public void onChangeTraceRadio() {
        if ("1".equals(traceRadio)) {
            toWorkReportTraceOtherPersonComponent();
            DisplayController.getInstance().update("dlgTraceActionUsers_view");
            DisplayController.getInstance().showPfWidgetVar("dlgTraceActionUsers");
        }
    }

    /**載入邀請其他人員*/
    public void toWorkReportTraceOtherPersonComponent() {
        workReportTraceOtherPersonComponent.loadData(wr_Id, wr_no, memo, noticeDate);
    }

    private final ReLoadCallBack traceReloadCallBack = new ReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -7988107006081602052L;

        @Override
        public void onReload() {
            loadTraceUsers();
            loadTraceDep();
            DisplayController.getInstance().update("dlgTraceAction_view_traceRadio_panel_id");
        }
    };

    private final ReLoadCallBack closeCallBack = new ReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -8679302464548574231L;

        @Override
        public void onReload() {
            close();
            traceUpdateCallBack.doUpdateData();
        }
    };
}
