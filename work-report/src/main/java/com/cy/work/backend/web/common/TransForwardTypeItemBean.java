/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.cy.work.backend.web.common.setting.TransFowardTypeSetting;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 轉寄類型Item Bean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@ManagedBean
public class TransForwardTypeItemBean {

    /** 取得 轉寄類型SelectItems */
    public List<SelectItem> getTransForwardTypesItems() {
        return TransFowardTypeSetting.getTransFowardTypesItems();
    }
}
