/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.setting;

import com.cy.work.backend.web.vo.enums.WREditType;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import lombok.extern.slf4j.Slf4j;

/**
 * 編輯權限靜態資料
 *
 * @author brain0925_liao
 */
@Slf4j
public class WorkProjectAcceptEditSetting {

    /** 編輯權限選項List */
    private static List<SelectItem> workProjectAcceptEditItems;

    /** 取得編輯權限選項資料,若記憶體已有將不再行建立 */
    public static List<SelectItem> getWorkProjectAcceptEditItems() {
        if (workProjectAcceptEditItems != null) {
            return workProjectAcceptEditItems;
        }
        workProjectAcceptEditItems = Lists.newArrayList();
        for (WREditType item : WREditType.values()) {
            SelectItem si = new SelectItem(item.name(), item.getValue());
            workProjectAcceptEditItems.add(si);
        }
        return workProjectAcceptEditItems;
    }
   
    /**
     * 取得編輯權限顯示名稱 By key
     * @param key
     * @return 
     */
    public static String getWorkProjectAcceptEditName(String key) {
        try {
            List<SelectItem> wis = getWorkProjectAcceptEditItems();
            List<SelectItem> is = wis.stream()
                    .filter(p -> String.valueOf(p.getValue()).equals(key))
                    .collect(Collectors.toList());
            if (is != null && is.size() > 0) {
                return is.get(0).getLabel();
            }
        } catch (Exception e) {
            log.error("GetWorkProjectAcceptEditName Error", e);
        }
        return "";
    }
}
