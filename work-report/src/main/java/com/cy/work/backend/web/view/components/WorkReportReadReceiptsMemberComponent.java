/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.WorkReportTabCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRReadReceiptComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.enumtype.TabType;
import com.cy.work.backend.web.view.vo.ReadReceiptVO;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.vo.enums.WReadReceiptStatus;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.DualListModel;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class WorkReportReadReceiptsMemberComponent implements Serializable {

    
    private static final long serialVersionUID = 6584090467274954466L;

    @Getter
    @Setter
    private DualListModel<UserViewVO> pickUsers;

    private List<UserViewVO> leftViewVO;

    private Integer loginUserSid;

    private String wr_Id;

    private MessageCallBack messageCallBack;
    @Getter
    @Setter
    private String filterNameValue;

    private WorkReportTabCallBack workReportTabCallBack;
    @Getter
    private List<ReadReceiptVO> readReceiptVOs;
    @Getter
    private boolean showReadReceiptTab = false;

    private Integer compSid;

    private Integer createUser;

    private List<UserViewVO> allCompUser;

    public WorkReportReadReceiptsMemberComponent(Integer loginUserSid,
            MessageCallBack messageCallBack, WorkReportTabCallBack workReportTabCallBack) {
        this.messageCallBack = messageCallBack;
        this.loginUserSid = loginUserSid;
        this.workReportTabCallBack = workReportTabCallBack;
        pickUsers = new DualListModel<>();
        this.readReceiptVOs = Lists.newArrayList();
    }

    public void loadUserPicker(String wr_Id) {
        this.filterNameValue = "";
        this.wr_Id = wr_Id;
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        this.compSid = user.getPrimaryOrg().getCompanySid();
        allCompUser = Lists.newArrayList();
        try {
            OrgLogicComponents.getInstance().findOrgsByCompanySid(this.compSid).stream()
                    .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                    .collect(Collectors.toList()).forEach(item -> {
                        allCompUser.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
                    });
        } catch (Exception e) {
            log.error(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }

        leftViewVO = Lists.newArrayList();
        Org baseOrg = WkOrgUtils.prepareBaseDep(user.getPrimaryOrg().getSid(), OrgLevel.MINISTERIAL);
        List<Org> allChildOrg = OrgLogicComponents.getInstance().getAllChildOrg(baseOrg);
        leftViewVO.addAll(WRUserLogicComponents.getInstance().findByDepSid(baseOrg.getSid()));
        allChildOrg.forEach(item -> {
            leftViewVO.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
        });
        List<UserViewVO> rightViewVO = Lists.newArrayList();
        List<ReadReceiptVO> readReceiptVOs = WRReadReceiptComponents.getInstance().getReadReceiptVOByWrSid(wr_Id);
        readReceiptVOs.forEach(item -> {
            User tempUser = WRUserLogicComponents.getInstance().findUserBySid(Integer.valueOf(item.getUserSid()));
            Org dep = OrgLogicComponents.getInstance().findOrgBySid(tempUser.getPrimaryOrg().getSid());
            UserViewVO uv = new UserViewVO(tempUser.getSid(), tempUser.getName(), OrgLogicComponents.getInstance().showParentDep(dep));
            if (item.getWReadReceiptStatus().equals(WReadReceiptStatus.READ)) {
                uv.setMoveAble(false);
            }
            rightViewVO.add(uv);
        });
        pickUsers.setSource(removeTargetUserView(leftViewVO, rightViewVO));
        pickUsers.setTarget(rightViewVO);
    }

    public void loadSelectDepartment(List<Org> departments) {
        settingTargetReaded();
        if (departments == null || departments.size() == 0) {
            return;
        }
        // leftViewVO.clear();

        List<UserViewVO> selectDepUserView = Lists.newArrayList();
        departments.forEach(item -> {
            selectDepUserView.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
        });
        pickUsers.setSource(removeTargetUserView(selectDepUserView, pickUsers.getTarget()));
    }

    public void filterName() {
        settingTargetReaded();
        if (Strings.isNullOrEmpty(filterNameValue)) {
            pickUsers.setSource(removeTargetUserView(allCompUser, pickUsers.getTarget()));
        } else {
            pickUsers.setSource(removeTargetUserView(allCompUser.stream()
                    .filter(each -> each.getName().toLowerCase().contains(filterNameValue.toLowerCase()))
                    .collect(Collectors.toList()), pickUsers.getTarget()));
        }
        DisplayController.getInstance().update("readReceiptPanel");
    }

    private void settingTargetReaded() {
        List<ReadReceiptVO> readReceiptVOs = WRReadReceiptComponents.getInstance().getReadReceiptVOByWrSid(wr_Id);
        // 過濾出已讀人員
        List<ReadReceiptVO> readedReadReceipt = readReceiptVOs.stream()
                .filter(each -> each.getWReadReceiptStatus().equals(WReadReceiptStatus.READ))
                .collect(Collectors.toList());
        if (pickUsers.getTarget() != null && pickUsers.getTarget().size() > 0) {
            pickUsers.getTarget().forEach(item -> {
                List<ReadReceiptVO> sameReadReceiptsMemberTo = readedReadReceipt.stream()
                        .filter(each -> each.getUserSid().equals(String.valueOf(item.getSid())))
                        .collect(Collectors.toList());
                if (sameReadReceiptsMemberTo != null && sameReadReceiptsMemberTo.size() > 0) {
                    item.setMoveAble(false);
                }
            });
        }
    }

    public void saveReadReceipt() {
        try {
            WRReadReceiptComponents.getInstance().saveReadReceiptVO(loginUserSid, wr_Id, pickUsers.getTarget());
            workReportTabCallBack.reloadTraceTab();

            loadData();
            if (showReadReceiptTab) {
                workReportTabCallBack.reloadTabView(TabType.ReadReceipt);
            } else {
                workReportTabCallBack.reloadTabView(TabType.Trace);
            }
            DisplayController displayController = SpringContextHolder.getBean(DisplayController.class);
            displayController.hidePfWidgetVar("readReceiptDlg");

        } catch (Exception e) {
            log.error("saveReadReceipt Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void sendToReadReceopt() {
        try {
            WRReadReceiptComponents.getInstance().doReadReceipt(loginUserSid, wr_Id);
            workReportTabCallBack.reloadWrMasterData();
            loadData();
            if (showReadReceiptTab) {
                workReportTabCallBack.reloadTabView(TabType.ReadReceipt);
            } else {
                workReportTabCallBack.reloadTabView(TabType.Trace);
            }
            DisplayController.getInstance().execute("doReadReceptToUpdate('" + wr_Id + "')");
            // workReportTabCallBack.reloadTabView(TabType.ReadReceipt);
        } catch (Exception e) {
            log.error("sendToReadReceopt Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source, List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(item -> {
            // 未挑選不可以包含已挑選的人員及自己本身
            if (!target.contains(item) && !loginUserSid.equals(item.getSid())) {
                filterSource.add(item);
            }
        });
        return filterSource;
    }

    public void loadData(String wr_Id, Integer createUser) {
        this.wr_Id = wr_Id;
        this.createUser = createUser;
        loadData();
    }

    public void loadData() {
        readReceiptVOs = WRReadReceiptComponents.getInstance().getReadReceiptVOByWrSid(wr_Id);
        boolean loginUserInReadReceiptMembers = false;
        List<ReadReceiptVO> readReceiptMembers = readReceiptVOs.stream()
                .filter(each -> each.getUserSid().equals(String.valueOf(loginUserSid)))
                .collect(Collectors.toList());
        if (readReceiptMembers != null && readReceiptMembers.size() > 0) {
            loginUserInReadReceiptMembers = true;
        }

        if (readReceiptVOs != null && readReceiptVOs.size() > 0 && (createUser.equals(loginUserSid) || loginUserInReadReceiptMembers)) {
            showReadReceiptTab = true;
        } else {
            showReadReceiptTab = false;
        }
    }

}
