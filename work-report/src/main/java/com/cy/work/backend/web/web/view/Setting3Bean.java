/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.listener.DataTableReLoadCallBack;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRTagLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WRReportCustomColumnLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.CustomColumnComponent;
import com.cy.work.backend.web.view.components.WorkReportSearchHeaderComponent;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.TagSearchVO;
import com.cy.work.backend.web.view.vo.WRTagSearchColumnVO;
import com.cy.work.backend.web.vo.WRTag;
import com.cy.work.backend.web.vo.enums.WRTagSearchColumn;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Setting3Bean implements Serializable {


    private static final long serialVersionUID = 345231660039569122L;
    @Getter
    private WorkReportSearchHeaderComponent workReportSearchHeaderComponent;
    @Autowired
    private WRTagLogicComponents wRTagLogicComponents;
    @Autowired
    private WRReportCustomColumnLogicComponents wrReportCustomColumnLogicComponents;
    @Autowired
    private OrgLogicComponents orgLogicComponents;
    /** view 顯示錯誤訊息 */
    @Getter
    private String errorMessage;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Setter
    @Getter
    private List<TagSearchVO> tagSearchVO;
    @Setter
    @Getter
    private TagSearchVO selTagSearchVO;
    @Getter
    private WRTagSearchColumnVO searchColumnVO;
    @Getter
    private CustomColumnComponent customColumn;
    @Getter
    private final String dataTableID = "dataTableTagModify";
    @Getter
    private final String dataTableWv = "dataTableTagModifyWv";

    private List<Org> selectOrgs;

    private List<Org> allOrgs;

    /** 部門相關邏輯Component */
    private DepTreeComponent depTreeComponent;
    @Setter
    @Getter
    private String tagName = "";
    @Setter
    @Getter
    private String selectTagCategoryID = "";
    @Setter
    @Getter
    private String statusID = "";
    @Setter
    @Getter
    private String memo = "";
    @Setter
    @Getter
    private String seq = "";

    private String tag_sid;
    @Getter
    private boolean disableStatus = true;

    @PostConstruct
    public void init() {
        userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        depViewVo = orgLogicComponents.findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = orgLogicComponents.findById(SecurityFacade.getCompanyId());
        workReportSearchHeaderComponent = new WorkReportSearchHeaderComponent();
        searchColumnVO = wrReportCustomColumnLogicComponents.getWRTagSearchColumnSetting(userViewVO.getSid());
        customColumn = new CustomColumnComponent(WRReportCustomColumnUrlType.TAG_SEARCH, Arrays.asList(WRTagSearchColumn.values()), searchColumnVO.getPageCount(), userViewVO.getSid(), messageCallBack, dataTableReLoadCallBack);
        initOrgData();
        doSearch();
    }

    public void doSearch() {
        try {
            tagSearchVO = doSearch("");
            if (tagSearchVO.size() > 0) {
                DisplayController.getInstance().execute("selectDataTablePage('" + dataTableWv + "',0);");
            }
        } catch (Exception e) {
            log.error("doSearch Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void clear() {
        workReportSearchHeaderComponent.settingDefault();
        intOrgTree();
        DisplayController.getInstance().update("depDialogPanel");
        tagSearchVO = Lists.newArrayList();
        doSearch();
    }

    private List<TagSearchVO> doSearch(String sid) {

        return wRTagLogicComponents.getTagSearch(workReportSearchHeaderComponent.getSelectStatus(), workReportSearchHeaderComponent.getTagName(), sid, compViewVo.getSid());
    }

    private void initOrgData() {
        allOrgs = Lists.newArrayList();
        allOrgs.addAll(orgLogicComponents.findOrgsByCompanySid(compViewVo.getSid()));
        depTreeComponent = new DepTreeComponent();
        intOrgTree();
    }

    private void intOrgTree() {
        selectOrgs = Lists.newArrayList();
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        Org group = new Org(compViewVo.getSid());
        depTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    public void saveTag() {
        try {
            wRTagLogicComponents.saveTag(tag_sid, tagName, selectTagCategoryID, statusID, memo, selectOrgs, seq, userViewVO.getSid(), compViewVo.getSid());
            tagSearchVO = doSearch("");
            DisplayController.getInstance().update(dataTableID);
            DisplayController.getInstance().hidePfWidgetVar("tag_maintain_setting_dlg_wv");
        } catch (Exception e) {
            log.error("saveTag", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void addTag() {
        selTagSearchVO = null;
        loadTag();
    }

    public void loadTag() {
        try {
            if (selTagSearchVO != null && !Strings.isNullOrEmpty(selTagSearchVO.getSid())) {
                WRTag selWRTag = wRTagLogicComponents.getWRTagBySid(selTagSearchVO.getSid());
                if (selWRTag == null) {
                    messageCallBack.showMessage("主檔資料比對有誤,無法開啟");
                    return;
                }
                tag_sid = selWRTag.getSid();
                List<Org> selOrg = Lists.newArrayList();
                if (selWRTag.getUse_dep() != null && selWRTag.getUse_dep().getUserDepTos() != null) {
                    selWRTag.getUse_dep().getUserDepTos().forEach(item -> {
                        selOrg.add(orgLogicComponents.findOrgBySid(Integer.valueOf(item.getDepSid())));
                    });
                }
                selectOrgs = selOrg;
                DisplayController.getInstance().update("depDialogPanel");
                tagName = selWRTag.getTag_name();
                if (selWRTag.getStatus().equals(Activation.ACTIVE)) {
                    disableStatus = false;
                } else {
                    disableStatus = true;
                }
                selectTagCategoryID = selWRTag.getCategory().name();
                statusID = selWRTag.getStatus().name();
                memo = selWRTag.getMemo();
                seq = String.valueOf(selWRTag.getSeq());
            } else {
                disableStatus = false;
                tag_sid = "";
                selectOrgs = Lists.newArrayList();
                DisplayController.getInstance().update("depDialogPanel");
                tagName = "";
                selectTagCategoryID = "";
                statusID = "";
                memo = "";
                seq = String.valueOf((wRTagLogicComponents.getMaxTagSeq(compViewVo.getSid()) + 1));
            }
        } catch (Exception e) {
            log.error("loadTag", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void loadOrgs() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(item -> {
            tempOrg.add(item);
        });
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        Org group = new Org(compViewVo.getSid());
        depTreeComponent.init(tempOrg, tempOrg,
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);

    }

    public void doSelectOrg() {
        selectOrgs = depTreeComponent.getSelectOrg();
        DisplayController.getInstance().hidePfWidgetVar("deptUnitDlg");
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() {
        return depTreeComponent.getMultipleDepTreeManager();
    }

    public void openCustomColumn() {
        try {
            customColumn.loadData();
            DisplayController.getInstance().update("dlgDefineField_view");
            DisplayController.getInstance().showPfWidgetVar("dlgDefineField");
        } catch (Exception e) {
            log.error("openCustomColumn Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 7696755424072058721L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    };

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -6578691513255474863L;

        @Override
        public void reload() {
            searchColumnVO = wrReportCustomColumnLogicComponents.getWRTagSearchColumnSetting(userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };

}
