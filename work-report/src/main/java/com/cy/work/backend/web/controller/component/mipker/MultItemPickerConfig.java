/**
 * 
 */
package com.cy.work.backend.web.controller.component.mipker;

import java.io.Serializable;
import java.util.Comparator;

import com.cy.work.backend.web.controller.component.mipker.vo.MultItemPickerShowMode;
import com.cy.work.common.vo.WkItem;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@NoArgsConstructor
public class MultItemPickerConfig implements Serializable{
    
    
    private static final long serialVersionUID = 8363947993394804826L;

    /**
     * @param config
     */
    public MultItemPickerConfig(MultItemPickerConfig config) {
        this.treeModePrefixName = config.getTreeModePrefixName();
        this.enableGroupMode = config.isEnableGroupMode();
        this.containFollowing = config.isContainFollowing();
        this.itemComparator = config.getItemComparator();
        this.hideTopArea = config.isHideTopArea();
        this.componentID = config.getComponentID();
    }

    /**
     * 顯示模式-樹模式 顯示前綴
     */
    @Getter
    @Setter
    private String treeModePrefixName = "項目";
    
    /**
     * 是否開啟群組功能
     */
    @Getter
    @Setter
    private boolean enableGroupMode = false;

    /**
     * 含以下項目
     */
    @Getter
    @Setter
    private boolean containFollowing = false;
    
    /**
     * 隱藏上方功能區
     */
    @Getter
    @Setter
    private boolean hideTopArea = false;
    
	/**
	 * 預設顯示模式
	 */
	@Setter
	@Getter
	private MultItemPickerShowMode defaultShowMode = MultItemPickerShowMode.LIST;
    

    /**
     * 項目排序器
     */
    @Getter
    @Setter
    private Comparator<WkItem> itemComparator = Comparator.comparing(WkItem::getItemSeq);
    
    /**
     * 有傳入時, 會自動於初始化時清除 filter 
     * TODO 未來應直接限定於建構子中傳入
     */
    @Getter
    @Setter
    private String componentID;
}
