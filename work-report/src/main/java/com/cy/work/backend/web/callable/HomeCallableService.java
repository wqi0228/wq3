/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.callable;

import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.cy.work.backend.web.vo.enums.WRFunItemComponentType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;

/**
 * 選單計算筆數Service
 *
 * @author brain0925_liao
 */
@Slf4j
public class HomeCallableService implements Serializable {

    
    private static final long serialVersionUID = 6803735414876441095L;

    /**
     * 根據選單componentID取出相對應的計算筆數Callable
     *
     * @param wrFunItemGroupVO 選單Group物件
     * @param componentID componentID
     * @param loginUserSid 登入者Sid
     * @param depUserSid 登入者部門Sid
     * @return
     */
    public static HomeCallable getHomeCallableByWRFunItemComponentType(WRFunItemGroupVO wrFunItemGroupVO,
            String componentID, Integer loginUserSid, Integer depUserSid) {
        if (WRFunItemComponentType.FAVORITES.getComponentID().equals(componentID)) {
            return new FavoriteCountCallable(wrFunItemGroupVO, WRFunItemComponentType.FAVORITES, loginUserSid);
        } else if (WRFunItemComponentType.TRACE.getComponentID().equals(componentID)) {
            return new TraceCountCallable(wrFunItemGroupVO, WRFunItemComponentType.TRACE, loginUserSid, depUserSid);
        } else if (WRFunItemComponentType.INCOME_DEPT.getComponentID().equals(componentID)) {
            return new IncomeDepCallable(wrFunItemGroupVO, WRFunItemComponentType.INCOME_DEPT, loginUserSid);
        } else if (WRFunItemComponentType.INCOME_REPORT.getComponentID().equals(componentID)) {
            return new IncomeReportCallable(wrFunItemGroupVO, WRFunItemComponentType.INCOME_REPORT, loginUserSid);
        } else if (WRFunItemComponentType.INCOME_INSTRUCTION.getComponentID().equals(componentID)) {
            return new IncomeInstructionCallable(wrFunItemGroupVO, WRFunItemComponentType.INCOME_INSTRUCTION, loginUserSid);
        } else if (WRFunItemComponentType.INCOME_MEMBER.getComponentID().equals(componentID)) {
            return new IncomeMemberCallable(wrFunItemGroupVO, WRFunItemComponentType.INCOME_MEMBER, loginUserSid);
        }
        return null;
    }

    /**
     * 執行計算筆數Callable並回傳result
     *
     * @param homeCallable
     * @return
     */
    public static List<HomeCallableCondition> doHomeCallableList(List<HomeCallable> homeCallable) {
        List<HomeCallableCondition> results = Lists.newArrayList();
        if (homeCallable == null || homeCallable.isEmpty()) {
            return results;
        }
        ExecutorService pool = Executors.newFixedThreadPool(10);
        try {
            CompletionService<HomeCallableCondition> completionPool = new ExecutorCompletionService<HomeCallableCondition>(pool);
            homeCallable.forEach(item -> completionPool.submit(item));
            IntStream.range(0, homeCallable.size()).forEach(i -> {
                try {
                    Object result = completionPool.take().get();
                    if (result == null) {
                        return;
                    }
                    results.add((HomeCallableCondition) result);
                } catch (InterruptedException e) {
                    log.error("doHomeCallableList_InterruptedException", e);
                } catch (ExecutionException e){
                    log.error("doHomeCallableList_ExecutionException", e);
                }
            });
        } catch (Exception e) {
            log.error("doHomeCallableList", e);
        } finally {
            pool.shutdown();
        }
        return results;
    }
}
