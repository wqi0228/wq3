/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.attachment;

import com.cy.work.backend.web.listener.UploadAttCallBack;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class AttachmentCondition implements Serializable{

    
    private static final long serialVersionUID = -5715960159789706481L;
    /** 主檔SID */
    @Getter
    private String entity_sid;
    /** 主檔NO */
    @Getter
    private String entity_no;
    /** auto mapping entity 若為true,上傳附件將會自動一併存檔並且與主檔建立關聯 */
    @Getter
    private boolean autoMappingEntity = false;
    @Setter
    @Getter
    public UploadAttCallBack uploadAttCallBack;

    public AttachmentCondition(String entity_sid, String entity_no, boolean autoMappingEntity) {
        this.entity_sid = entity_sid;
        this.entity_no = entity_no;
        this.autoMappingEntity = autoMappingEntity;
    }

}
