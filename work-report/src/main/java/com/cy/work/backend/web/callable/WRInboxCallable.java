/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.callable;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WorkParamLogicComponents;
import com.cy.work.backend.web.logic.manager.WRInboxManager;
import com.cy.work.backend.web.util.ToolsList;
import com.cy.work.backend.web.vo.WRInbox;
import com.cy.work.backend.web.vo.enums.ForwardType;
import com.cy.work.backend.web.vo.enums.InboxType;
import com.cy.work.backend.web.vo.enums.ReadRecordStatus;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

/**
 * 轉寄個人Callable
 *
 * @author brain0925_liao
 */
@Slf4j
public class WRInboxCallable implements Callable<WRInbox> {

    /**
     * 發送者Sid
     */
    private final Integer senderUserSid;
    /**
     * 接收者Sid
     */
    private final Integer receiveUserSid;
    /**
     * 轉寄Group Sid
     */
    private final String inbox_group_sid;
    /**
     * 工作報告Sid
     */
    private final String wrSid;
    /**
     * 工作報告單號
     */
    private final String wr_no;
    /**
     * 轉寄訊息
     */
    private final String message;
    /**
     * 轉寄訊息Css
     */
    private final String messageCss;

    private final WRInboxManager workInboxManager;

    public WRInboxCallable(String wrSid, String wr_no, String message, String messageCss, Integer senderUserSid, Integer receiveUserSid, String inbox_group_sid, WRInboxManager workInboxManager) {
        this.wrSid = wrSid;
        this.wr_no = wr_no;
        this.message = message;
        this.messageCss = messageCss;
        this.senderUserSid = senderUserSid;
        this.receiveUserSid = receiveUserSid;
        this.inbox_group_sid = inbox_group_sid;
        this.workInboxManager = workInboxManager;
    }

    @Override
    public WRInbox call() throws Exception {
        User senderUser = WRUserLogicComponents.getInstance().findUserBySid(senderUserSid);
        User receiverUser = WRUserLogicComponents.getInstance().findUserBySid(receiveUserSid);
        WRInbox wr = new WRInbox();
        wr.setWr_sid(wrSid);
        wr.setWr_no(wr_no);
        wr.setForward_type(ForwardType.PERSON);
        wr.setSender(senderUserSid);
        wr.setSender_dep(senderUser.getPrimaryOrg().getSid());
        wr.setMessage(message);
        wr.setMessage_css(messageCss);
        wr.setReceive(receiveUserSid);
        wr.setReceive_dep(receiverUser.getPrimaryOrg().getSid());
        wr.setInbox_group_sid(inbox_group_sid);
        wr.setRead_record(ReadRecordStatus.UNREAD);
        //優先確認是否為bigBoss
        List<Integer> bigBossSid = WorkParamLogicComponents.getInstance().getBigBossSids();
        if (bigBossSid.contains(senderUserSid) || bigBossSid.contains(receiveUserSid)) {
            InboxType inboxType = bigBossInboxType(bigBossSid);
            wr.setInbox_type(inboxType);
            return saveWorkInboxManager(wr);
        }
        //若是自己記給自己為轉個人
        if (senderUserSid.equals(receiveUserSid)) {
            log.info("工作報告單號[" + wr_no + "]發送者[" + senderUser.getName() + "]自行轉寄,轉個人");
            wr.setInbox_type(InboxType.INCOME_MEMBER);
            return saveWorkInboxManager(wr);
        }
        // 寄信者部門
        Org sendUserDep = OrgLogicComponents.getInstance().findOrgBySid(senderUser.getPrimaryOrg().getSid());
        // 收信者部門
        Org receviceUserDep = OrgLogicComponents.getInstance().findOrgBySid(receiverUser.getPrimaryOrg().getSid());
        //已寄件者角度去確認

        //同部門狀況確認
        if (sendUserDep.getSid().equals(receviceUserDep.getSid())) {
            //發送者為部門管理者,發送給同部門其他人為轉指示
            if (ToolsList.isOneOfManagers(senderUserSid, sendUserDep.getManagers())) {
                // 多主管
                log.info("工作報告單號[" + wr_no + "]同部門[" + sendUserDep.getName() + "] 部門主管為發送者[" + senderUser.getName() + "]轉寄"
                        + "接收者[" + receiverUser.getName() + "],轉指示");
                wr.setInbox_type(InboxType.INCOME_INSTRUCTION);
                return saveWorkInboxManager(wr);
            }
            //接收者為部門管理者,為轉呈報
            else if (ToolsList.isOneOfManagers(receiveUserSid, receviceUserDep.getManagers())) {
                // 多主管
                log.info("工作報告單號[" + wr_no + "]同部門[" + sendUserDep.getName() + "] 部門主管為接收者[" + receiverUser.getName() + "]"
                        + "發送者[" + senderUser.getName() + "],轉呈報");
                wr.setInbox_type(InboxType.INCOME_REPORT);
                return saveWorkInboxManager(wr);
            } //接收者及發送者皆不為部門管理者,轉個人
            else {
                log.info("工作報告單號[" + wr_no + "]同部門[" + sendUserDep.getName() + "] 接收者[" + receiverUser.getName() + "]"
                        + "發送者[" + senderUser.getName() + "],轉個人");
                wr.setInbox_type(InboxType.INCOME_MEMBER);
                return saveWorkInboxManager(wr);
            }
        }
        //以部門去確認接收者的部門是否為發送者的上層部門,若接收者的部門為上層部門,為發呈報
        // 以部門的角度去判斷(A部為B組的上級部門，B組人發給A部人 -> 呈報)
        List<Org> parentOrgs = OrgLogicComponents.getInstance().getAllParentOrgs(sendUserDep);
        parentOrgs.add(sendUserDep);
        if (parentOrgs.contains(receviceUserDep)) {
            log.info("工作報告單號[" + wr_no + "]發送部門[" + sendUserDep.getName() + "],"
                    + "接收部門[" + receviceUserDep.getName() + "]為上層部門"
                    + "接收者[" + receiverUser.getName() + "]"
                    + "發送者[" + senderUser.getName() + "],轉呈報"
            );
            wr.setInbox_type(InboxType.INCOME_REPORT);
            return saveWorkInboxManager(wr);
        }
        // 以部門管理者確認接收者是否為發送者的上層,若為上層,為發呈報
        // 以人的角度去判斷(例子A、B同組，A為組長，B發給A -> 呈報)
        // 多主管
        List<Org> parentDepManagerUserOrgs = parentOrgs.stream().filter(each -> ToolsList.isOneOfManagers(receiveUserSid, each.getManagers()))
                .collect(Collectors.toList());
        if (WkStringUtils.notEmpty(parentDepManagerUserOrgs)) {
            log.info("特殊情境,接收者[" + receiverUser.getName() + "]所屬部門[" + receviceUserDep.getName() + "],"
                    + "不為發送者[" + sendUserDep.getName() + "]部門[" + sendUserDep.getName() + "]上層部門,"
                    + "但發送者的上層部門[" + parentDepManagerUserOrgs.get(0).getName() + "]管理者為接收者,固定義為收呈報");
            wr.setInbox_type(InboxType.INCOME_REPORT);
            return saveWorkInboxManager(wr);
        }
        //以部門去確認接收者的部門是否為發送者的下層部門,若接收者的部門為下層部門,為發指示
        List<Org> childOrgs = OrgLogicComponents.getInstance().getAllChildOrg(sendUserDep);
        if (childOrgs.contains(receviceUserDep)) {
            log.info("工作報告單號[" + wr_no + "]發送部門[" + sendUserDep.getName() + "]為上層部門,"
                    + "接收部門[" + receviceUserDep.getName() + "]"
                    + "接收者[" + receiverUser.getName() + "]"
                    + "發送者[" + senderUser.getName() + "],轉指示"
            );
            wr.setInbox_type(InboxType.INCOME_INSTRUCTION);
            return saveWorkInboxManager(wr);
        }

        // 以部門管理者確認發送者是否為接收者的上層,若為上層,為發指示
        List<Org> recevieParentOrgs = OrgLogicComponents.getInstance().getAllParentOrgs(receviceUserDep);
        recevieParentOrgs.add(receviceUserDep);
        // 多主管
        List<Org> receviceParentDepManagerUserOrgs = recevieParentOrgs.stream().filter(each -> ToolsList.isOneOfManagers(senderUserSid,each.getManagers()))
                .collect(Collectors.toList());
        if (WkStringUtils.notEmpty(receviceParentDepManagerUserOrgs)) {
            log.info("特殊情境,發送者[" + sendUserDep.getName() + "]所屬部門[" + sendUserDep.getName() + "],"
                    + "不為接收者[" + receiverUser.getName() + "]部門[" + receviceUserDep.getName() + "]上層部門,"
                    + "但接收者的上層部門[" + receviceParentDepManagerUserOrgs.get(0).getName() + "]管理者為發送者,固定義為發指示");
            wr.setInbox_type(InboxType.INCOME_INSTRUCTION);
            return saveWorkInboxManager(wr);
        }
        log.info("工作報告單號[" + wr_no + "]發送部門[" + sendUserDep.getName() + "]"
                + "接收部門[" + receviceUserDep.getName() + "]"
                + "接收者[" + receiverUser.getName() + "]"
                + "發送者[" + senderUser.getName() + "],無任何上下階層關係,轉個人"
        );
        //若不為發指示及收呈報,代表為轉個人
        wr.setInbox_type(InboxType.INCOME_MEMBER);
        return saveWorkInboxManager(wr);
    }

    private WRInbox saveWorkInboxManager(WRInbox workInbox) {
        return workInboxManager.createWorkInbox(workInbox, senderUserSid);
    }

    private InboxType bigBossInboxType(List<Integer> bigBossSid) {
        //BigBoss互寄,為轉個人
        if (bigBossSid.contains(senderUserSid) && bigBossSid.contains(receiveUserSid)) {
            log.info("bigBoss 互寄為轉個人, senderUserSid:" + senderUserSid + " to receiveUserSid:" + receiveUserSid);
            return InboxType.INCOME_MEMBER;
        } //BigBoss寄給其他人,皆為收指示
        else if (bigBossSid.contains(senderUserSid)) {
            log.info("BigBoss寄給其他人為收指示, senderUserSid:" + senderUserSid + " to receiveUserSid:" + receiveUserSid);
            return InboxType.INCOME_INSTRUCTION;
        } //其他人寄給BigBoss,皆為收呈報
        else if (bigBossSid.contains(receiveUserSid)) {
            log.info("其他人寄給BigBoss為收呈報, senderUserSid:" + senderUserSid + " to receiveUserSid:" + receiveUserSid);
            return InboxType.INCOME_REPORT;
        }
        log.error("bigBossInboxType 有問題,無找到對應InboxType");
        return InboxType.INCOME_MEMBER;
    }
}
