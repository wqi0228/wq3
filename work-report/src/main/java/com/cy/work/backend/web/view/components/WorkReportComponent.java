/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.work.backend.web.vo.WRMaster;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class WorkReportComponent implements Serializable {


    private static final long serialVersionUID = 1021604442956337225L;
    @Getter
    @Setter
    private WorkReportDataComponent workReportDataComponent;
    @Getter
    @Setter
    private WorkReportHeaderComponent workReportHeaderComponent;
    @Getter
    @Setter 
    private WRMaster wRMaster;

}
