/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class WorkReportHeaderComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5045298494704641374L;
    /** 部門 */
    @Getter
    @Setter
    private String departmentName;
    /** 建立人員 */
    @Getter
    @Setter
    private String createUserName;
    /** 建立日期 */
    @Getter
    @Setter
    private String createTime;
    @Setter
    @Getter
    private String createTimeTitle = "建立日期";
    /**提交日期*/
    @Getter
    @Setter
    private String submitTime;
    /** 標籤ID */
    @Getter
    @Setter
    private String tagID;
    /** 允許編輯ID */
    @Getter
    @Setter
    private String acceptEditID;
    /** 單號 */
    @Getter
    @Setter
    private String workProjectNo;
    /** 狀態 */
    @Getter
    @Setter
    private String status;
    /** 是否顯示回覆已讀取按鈕 */
    @Getter
    @Setter
    private boolean showReadReceiptBtn = false;
    /** 是否可點選回覆已讀取按鈕 */
    @Getter
    @Setter
    private boolean readReceiptBtnAble = false;
}
