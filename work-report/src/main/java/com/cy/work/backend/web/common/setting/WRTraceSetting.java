/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.setting;

import com.cy.work.backend.web.vo.enums.TraceStatus;
import com.google.common.collect.Lists;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * 追蹤狀態靜態資料
 *
 * @author brain0925_liao
 */
public class WRTraceSetting {

    /** 追蹤狀態選項List */
    private static List<SelectItem> wRTraceItems;

    /** 取得追蹤狀態選項資料,若記憶體已有將不再行建立 */
    public static List<SelectItem> getWRTraceItems() {
        if (wRTraceItems != null) {
            return wRTraceItems;
        }
        wRTraceItems = Lists.newArrayList();
        for (TraceStatus item : TraceStatus.values()) {
            SelectItem si1 = new SelectItem(item.name(), item.getValue());
            wRTraceItems.add(si1);
        }

        return wRTraceItems;
    }

}
