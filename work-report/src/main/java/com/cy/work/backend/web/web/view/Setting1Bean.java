/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRHomeLogicComponents;
import com.cy.work.backend.web.logic.components.WRHomepageFavoriteLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRHomepageFavoriteVO;
import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.cy.work.backend.web.vo.enums.WRFuncGroupBaseType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Setting1Bean implements Serializable {


    private static final long serialVersionUID = -8652004007819135888L;
    @Getter
    private String errorMessage;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Autowired
    private WRUserLogicComponents wRUserLogicComponents;
    @Autowired
    private OrgLogicComponents orgLogicComponents;
    @Getter
    /** 報表選項 */
    private List<SelectItem> reportItems;

    private List<WRHomepageFavoriteVO> reports;
    /** 選取的報表快選 */
    @Setter
    @Getter
    private List<String> selectReportTypes = Lists.newArrayList();

    @Autowired
    private WRHomeLogicComponents wRHomeLogicComponents;
    @Autowired
    private WRHomepageFavoriteLogicComponents wRHomepageFavoriteLogicComponents;

    @PostConstruct
    public void init() {
        userViewVO = wRUserLogicComponents.findBySid(SecurityFacade.getUserSid());
        depViewVo = orgLogicComponents.findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = orgLogicComponents.findById(SecurityFacade.getCompanyId());
        initItmes();
        initDefault();
    }

    private void initItmes() {
        reportItems = Lists.newArrayList();
        reports = Lists.newArrayList();
        List<WRFunItemGroupVO> wrFunItemGroups
                = wRHomeLogicComponents.getWRFunItemGroupByTypeAndUserPermission(WRFuncGroupBaseType.SEARCH.getKey(),
                        userViewVO.getSid(), depViewVo.getSid());
        try {

            wrFunItemGroups.get(0).getWrFunItemVOs().forEach(item -> {
                reports.add(new WRHomepageFavoriteVO(item.getUrl(), item.getFunction_title()));
                SelectItem si = new SelectItem(item.getUrl(), item.getFunction_title());
                reportItems.add(si);
            });
        } catch (Exception e) {
            log.error("initItmes", e);
        }
    }

    private void initDefault() {
        selectReportTypes = Lists.newArrayList();

        try {
            wRHomepageFavoriteLogicComponents.getWRHomepageFavoriteVOByUserSid(userViewVO.getSid()).forEach(item -> {
                selectReportTypes.add(item.getPagePath());
            });
        } catch (Exception e) {
            log.error("initDefault", e);
        }
    }

    public void saveSetting() {
        try {
            List<WRHomepageFavoriteVO> wRHomepageFavoriteVO = Lists.newArrayList();
            selectReportTypes.forEach(item -> {
                List<WRHomepageFavoriteVO> sel = reports.stream()
                        .filter(each -> each.getPagePath().equals(item))
                        .collect(Collectors.toList());
                wRHomepageFavoriteVO.add(sel.get(0));
            });
            wRHomepageFavoriteLogicComponents.saveWRHomepageFavorite(wRHomepageFavoriteVO, userViewVO.getSid());
        } catch (Exception e) {
            log.error("saveSetting", e);
            errorMessage = e.getMessage();
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    }
}
