/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.work.backend.web.logic.components.WRMasterLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class LockCountDownComponent implements Serializable {

    
    private static final long serialVersionUID = 5336747308777960992L;
    @Getter
    @Setter
    private String editUserName = "";
    @Getter
    private boolean showLock = false;
    @Getter
    private String countDownTime = "";
    /** 有效分鐘 */
    private int effectiveMinute = 10;

    public LockCountDownComponent() {

    }

    public void loadData(String wr_ID, Integer loginUserSid, String editUserName, Date startEditDate) {
        this.editUserName = editUserName;
        this.showLock = true;
        try {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.MINUTE, effectiveMinute);
            Date lockTime = c.getTime();
            WRMasterLogicComponents.getInstance().updateLockUserAndLockDate(wr_ID, loginUserSid, lockTime);
        } catch (Exception e) {
            log.error("loadData", e);
        }
    }

    public void cancelLock(String wr_ID, Integer closeUserSid) {
        try {

            WRMasterLogicComponents.getInstance().closeLockUserAndLockDate(wr_ID, closeUserSid);
        } catch (Exception e) {
            log.error("cancelLock", e);
        }
        closeLock();
    }

    public int remainingTime(Date lock_dt) {
        long nowTime = new Date().getTime();
        long lockTme = lock_dt.getTime();
        long one_sec_time = 1000;
        long diffsec = (lockTme - nowTime) / one_sec_time;
        return (int) diffsec;
    }

    public void closeLock() {
        this.showLock = false;
        this.countDownTime = "";
        this.editUserName = "";
        DisplayController.getInstance().update("userModifyPanel");
    }

    public void findEffectiveTime(Date lockEndDate) {
        this.showLock = true;
        try {
            if(null != lockEndDate){
                this.countDownTime = new DateTime(lockEndDate).toString("yyyy-MM-dd HH:mm:ss");
            }else {
                this.countDownTime = new DateTime(new Date()).plusMinutes(effectiveMinute).toString("yyyy-MM-dd HH:mm:ss");
            }
        } catch (Exception e) {
            log.error("findEffectiveTime", e);
        }
    }

}
