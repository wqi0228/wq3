/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.common.ExportExcelComponent;
import com.cy.work.backend.web.listener.DataTableReLoadCallBack;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.TableUpDownListener;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRMasterSearchDraftLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WRReportCustomColumnLogicComponents;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.CustomColumnComponent;
import com.cy.work.backend.web.view.components.WorkReportSearchHeaderComponent;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRDraftSearchColumnVO;
import com.cy.work.backend.web.view.vo.DraftSearchVO;
import com.cy.work.backend.web.view.vo.WorkReportSidTo;
import com.cy.work.backend.web.vo.enums.WRDraftSearchColumn;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Search1Bean implements Serializable, TableUpDownListener {


    private static final long serialVersionUID = -8162022513965312160L;
    @Getter
    private WorkReportSearchHeaderComponent workReportSearchHeaderComponent;
    @Autowired
    private WRMasterSearchDraftLogicComponents wRMasterSearchDraftLogicComponents;
    @Autowired
    private WRReportCustomColumnLogicComponents wrReportCustomColumnLogicComponents;
    /** view 顯示錯誤訊息 */
    @Getter
    private String errorMessage;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Setter
    @Getter
    private List<DraftSearchVO> workReportDraftSearchVO;

    private List<DraftSearchVO> tempWorkReportDraftSearchVO;

    private String tempSelSid;
    @Setter
    @Getter
    private DraftSearchVO selWorkReportDraftSearchVO;
    @Setter
    @Getter
    private boolean showFrame = false;
    @Getter
    private String iframeUrl = "";
    @Getter
    private WRDraftSearchColumnVO searchColumnVO;
    @Getter
    private CustomColumnComponent customColumn;
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Getter
    private final String dataTableID = "dataTableDraft";
    @Getter
    private final String dataTableWv = "dataTableDraftWv";

    @Autowired
    private ExportExcelComponent exportExcelComponent;
    @Getter
    private boolean hasDisplay = true;

    @PostConstruct
    public void init() {
        userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        depViewVo = OrgLogicComponents.getInstance().findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = OrgLogicComponents.getInstance().findById(SecurityFacade.getCompanyId());
        workReportSearchHeaderComponent = new WorkReportSearchHeaderComponent();
        searchColumnVO = wrReportCustomColumnLogicComponents.getWRPersonDraftSearchColumnSetting(userViewVO.getSid());
        customColumn = new CustomColumnComponent(WRReportCustomColumnUrlType.PERSON_DRAFT_SEARCH, Arrays.asList(WRDraftSearchColumn.values()), searchColumnVO.getPageCount(), userViewVO.getSid(), messageCallBack, dataTableReLoadCallBack);
        doSearch();
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        try {
            exportExcelComponent.exportExcel(document, null,
                    null, "個人草稿查詢", false);
        } catch (Exception e) {
            log.error("exportExcel", e);
        } finally {
            hasDisplay = true;
        }
    }

    public void hideColumnContent() {
        hasDisplay = false;
    }

    public void doSearch() {
        try {
            workReportDraftSearchVO = doSearch("");
            if (workReportDraftSearchVO.size() > 0) {
                DisplayController.getInstance().execute("selectDataTablePage('" + dataTableWv + "',0);");
            }
        } catch (Exception e) {
            log.error("doSearch Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void clear() {
        workReportSearchHeaderComponent.settingDefault();
        doSearch();
    }

    private List<DraftSearchVO> doSearch(String sid) {
        return wRMasterSearchDraftLogicComponents.getWorkReportDraft(workReportSearchHeaderComponent.getSelectTag(), workReportSearchHeaderComponent.getTitle(),
                workReportSearchHeaderComponent.getContent(), sid, userViewVO.getSid(), "", false, workReportSearchHeaderComponent.getCreateTime(), compViewVo.getSid());
    }

    public void openCustomColumn() {
        try {
            customColumn.loadData();
            DisplayController.getInstance().update("dlgDefineField_view");
            DisplayController.getInstance().showPfWidgetVar("dlgDefineField");
        } catch (Exception e) {
            log.error("openCustomColumn Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenUrl(String sid) {
        try {
            settingSelWorkReportDraftSearchVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("btnOpenUrl Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            settingSelWorkReportDraftSearchVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp_iframe.xhtml?wrcId=" + selWorkReportDraftSearchVO.getSid();
            tableUpDownBean.setWorp_path("worp_iframe");
        } catch (Exception e) {
            log.error("btnOpenFrame Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void settingSelWorkReportDraftSearchVO(String sid) {
        DraftSearchVO sel = new DraftSearchVO(sid);
        int index = workReportDraftSearchVO.indexOf(sel);
        if (index > 0) {
            selWorkReportDraftSearchVO = workReportDraftSearchVO.get(index);
        } else {
            selWorkReportDraftSearchVO = workReportDraftSearchVO.get(0);
        }
    }

    public void settingPreviousAndNext() {
        int index = workReportDraftSearchVO.indexOf(selWorkReportDraftSearchVO);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(workReportDraftSearchVO.get(index - 1).getSid());
        }
        if (index == workReportDraftSearchVO.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(workReportDraftSearchVO.get(index + 1).getSid());
        }
    }

    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.error("closeIframe Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void toUp() {
        try {
            if (tempWorkReportDraftSearchVO != null && tempWorkReportDraftSearchVO.size() > 0 && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempWorkReportDraftSearchVO.indexOf(new DraftSearchVO(tempSelSid));
                if (index > 0) {
                    selWorkReportDraftSearchVO = tempWorkReportDraftSearchVO.get(index - 1);
                } else {
                    selWorkReportDraftSearchVO = workReportDraftSearchVO.get(0);
                }
                tempWorkReportDraftSearchVO = null;
                tempSelSid = "";
            } else {
                int index = workReportDraftSearchVO.indexOf(selWorkReportDraftSearchVO);
                if (index > 0) {
                    selWorkReportDraftSearchVO = workReportDraftSearchVO.get(index - 1);
                } else {
                    selWorkReportDraftSearchVO = workReportDraftSearchVO.get(0);
                }
            }
        } catch (Exception e) {
            log.error("toUp Error", e);
            tempSelSid = "";
            tempWorkReportDraftSearchVO = null;
            if (workReportDraftSearchVO.size() > 0) {
                selWorkReportDraftSearchVO = workReportDraftSearchVO.get(0);
            }
        }
    }

    private void toDown() {
        try {
            if (tempWorkReportDraftSearchVO != null && tempWorkReportDraftSearchVO.size() > 0 && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempWorkReportDraftSearchVO.indexOf(new DraftSearchVO(tempSelSid));
                if (index >= 0) {
                    selWorkReportDraftSearchVO = tempWorkReportDraftSearchVO.get(index + 1);
                } else {
                    selWorkReportDraftSearchVO = workReportDraftSearchVO.get(workReportDraftSearchVO.size() - 1);
                }
                tempWorkReportDraftSearchVO = null;
                tempSelSid = "";
            } else {
                int index = workReportDraftSearchVO.indexOf(selWorkReportDraftSearchVO);
                if (index >= 0) {
                    selWorkReportDraftSearchVO = workReportDraftSearchVO.get(index + 1);
                } else {
                    selWorkReportDraftSearchVO = workReportDraftSearchVO.get(workReportDraftSearchVO.size() - 1);
                }
            }
        } catch (Exception e) {
            log.error("toDown Error", e);
            tempWorkReportDraftSearchVO = null;
            tempSelSid = "";
            if (workReportDraftSearchVO.size() > 0) {
                selWorkReportDraftSearchVO = workReportDraftSearchVO.get(workReportDraftSearchVO.size() - 1);
            }
        }
    }

    @Override
    public void openerByBtnUp() {
        try {
            //log.info("草稿查詢進行上一筆動作");
            if (workReportDraftSearchVO.size() > 0) {
                toUp();
                if (selWorkReportDraftSearchVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selWorkReportDraftSearchVO.getSid());
                }
            }
            DisplayController.getInstance().update(dataTableID);
            //log.info("草稿查詢進行上一筆動作-結束");
        } catch (Exception e) {
            log.error("草稿查詢進行上一筆動作-失敗", e);
        } finally {

            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByBtnDown() {
        try {
            //log.info("草稿查詢進行下一筆動作");
            if (workReportDraftSearchVO.size() > 0) {
                toDown();
                if (selWorkReportDraftSearchVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selWorkReportDraftSearchVO.getSid());
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("草稿查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByDelete() {
        doReloadInfo();
    }

    @Override
    public void openerByInvalid() {
        //草稿查詢-並不會受到作廢影響
    }

    @Override
    public void openerByCommit() {
        doReloadInfo();
    }

    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        WkJsonUtils jsonUtils = SpringContextHolder.getBean(WkJsonUtils.class);
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.error("doReloadInfo", ex);
        }
    }

    private void updateInfoToDataTable(String sid) {
        try {
            List<DraftSearchVO> result = doSearch(sid);
            if (result == null || result.isEmpty()) {
                tempWorkReportDraftSearchVO = Lists.newArrayList();
                if (workReportDraftSearchVO != null && workReportDraftSearchVO.size() > 0) {
                    workReportDraftSearchVO.forEach(item -> {
                        tempWorkReportDraftSearchVO.add(item);
                    });
                    workReportDraftSearchVO.remove(new DraftSearchVO(sid));
                    tempSelSid = sid;
                }
            } else {
                DraftSearchVO updateDeail = result.get(0);
                workReportDraftSearchVO.forEach(item -> {
                    if (item.getSid().equals(updateDeail.getSid())) {
                        item.replaceValue(updateDeail.getDraft_time(), updateDeail.getTag_name(),
                                updateDeail.getDepartment_name(), updateDeail.getCreate_usr_name(),
                                updateDeail.getTitle(), updateDeail.getStatus(), updateDeail.getModify_time(), updateDeail.getWr_no());
                    }
                });

            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("reCheck Error", e);
        }
    }

    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    @Override
    public void openerByEditTag() {
        doReloadInfo();
    }

    @Override
    public void openerByTrace() {

    }

    @Override
    public void openerByFavorite() {
    }

    @Override
    public void openerByTrans() {
    }

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 5877290216942352389L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    };

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 4405373492319154858L;

        @Override
        public void reload() {
            searchColumnVO = wrReportCustomColumnLogicComponents.getWRPersonDraftSearchColumnSetting(userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };

    @Override
    public void openerByReadRecept() {

    }

}
