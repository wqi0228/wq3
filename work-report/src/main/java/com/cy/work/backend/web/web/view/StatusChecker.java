/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 檢核AP 機制
 *
 * @author kasim
 */
@Controller
@Scope("view")
public class StatusChecker implements Serializable {


    private static final long serialVersionUID = -4983654144355490452L;

    public void renderCheckResult() throws IOException {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.setResponseContentType("text/plain");
        ec.setResponseCharacterEncoding("UTF-8");
        String result = "ok";
        ec.getResponseOutputWriter().write(result);
        fc.responseComplete();
    }
}
