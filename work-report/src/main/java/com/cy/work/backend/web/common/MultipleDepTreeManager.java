/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.google.common.collect.Lists;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.TreeNode;

/**
 * 多選部門元件 Brain_liao
 */
public class MultipleDepTreeManager extends BaseDepTreeManager {


    private static final long serialVersionUID = 4202275506914771759L;

    @Getter
    @Setter
    /** 是否全選 */
    private boolean checkAll = false;

    public MultipleDepTreeManager() {
    }

    public TreeNode[] getSelNode() {
        if (selNode.isEmpty()) {
            return null;
        } else {
            return selNode.stream().toArray(size -> new TreeNode[size]);
        }
    }

    public void setSelNode(TreeNode[] selNode) {
        if (selNode == null) {
            return;
        }
        this.selNode = Arrays.asList(selNode);
    }

    /**
     * 進行全選
     */
    public void selectAllTree() {
        if (!checkAll) {
            this.selectUnAllTree();
            return;
        }
        selNode = Lists.newArrayList();
        List<TreeNode> list = Lists.newArrayList();
        storeAllSubNodes(list, this.getDepRoot());
        list.forEach(each -> {
            if (each.isSelectable()) {
                this.selNode.add(each);
                each.setSelected(true);
            }
        });
        if (noShowDepHistory) {
            expandAll(depRootByActive, true, false);
        } else {
            expandAll(depRootByAll, true, false);
        }
    }

    /**
     * 進行反選
     */
    private void selectUnAllTree() {
        selNode = Lists.newArrayList();
        List<TreeNode> list = Lists.newArrayList();
        this.storeAllSubNodes(list, this.getDepRoot());
        list.forEach(each -> {
            if (each.isSelectable()) {
                each.setSelected(false);
            }
        });
        if (noShowDepHistory) {
            expandAll(depRootByActive, false, false);
        } else {
            expandAll(depRootByAll, false, false);
        }
    }

}
