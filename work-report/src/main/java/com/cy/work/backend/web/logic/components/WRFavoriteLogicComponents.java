/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.logic.manager.WRFavoriteManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRFavoriteWorkReportVO;
import com.cy.work.backend.web.view.vo.FavoriteSearchVO;
import com.cy.work.backend.web.vo.WRFavorite;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 收藏邏輯元件
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRFavoriteLogicComponents implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = -3030786638008932149L;
    private static WRFavoriteLogicComponents instance;

    @Autowired
    private WRFavoriteManager wRFavoriteManager;

    public static WRFavoriteLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRFavoriteLogicComponents.instance = this;
    }

    /**
     * 收藏筆數 By 登入者Sid 及 收藏起始時間 及 收藏結束時間
     * @param loginUserSid 登入者Sid
     * @param start 收藏起始時間
     * @param end 收藏結束時間
     * @return 
     */
    public Integer findFavoriteCountByUserSidAndUpdateDt(Integer loginUserSid, Date start, Date end) {
        try {
            return wRFavoriteManager.findFavoriteCountByUserSidAndUpdateDt(loginUserSid, start, end);
        } catch (Exception e) {
            log.error("findFavoriteCountByUserSidAndUpdateDt", e);
        }
        return 0;
    }

    /**
     * 取得收藏報表
     * @param loginUserSid 登入者Sid
     * @param start 收藏起始時間
     * @param end 收藏結束時間
     * @param text 模糊搜尋
     * @param wr_Id 工作報告Sid
     * @return 
     */
    public List<FavoriteSearchVO> findFavoriteByUserSidAndUpdateDtAndText(Integer loginUserSid, Date start, Date end, String text, String wr_Id) {
        List<FavoriteSearchVO> favoriteSearchVOs = Lists.newArrayList();
        try {
            List<WRFavoriteWorkReportVO> wRFavoriteWorkReportVO = wRFavoriteManager.findFavoriteByUserSidAndUpdateDtAndText(loginUserSid, start, end, text, wr_Id);
            wRFavoriteWorkReportVO.forEach(item -> {
                FavoriteSearchVO fs = new FavoriteSearchVO(item.getFavorite_sid(), item.getWr_sid(), item.getTheme(), ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getFavoriteDate()), item.getWr_no());
                favoriteSearchVOs.add(fs);
            });
        } catch (Exception e) {
            log.error("findFavoriteByUserSidAndUpdateDtAndText", e);
        }
        return favoriteSearchVOs;
    }

    /**
     * 該登入者在工作報告是否收藏
     * @param wr_Id 工作報告Sid
     * @param loginUserSid 登入者Sid
     * @return 
     */
    public boolean isFavorite(String wr_Id, Integer loginUserSid) {
        try {
            WRFavorite wRFavorite = wRFavoriteManager.getWRFavoriteByloginUserSidAndWRSid(wr_Id, loginUserSid);
            if (wRFavorite != null && wRFavorite.getStatus().equals(Activation.ACTIVE)) {
                return true;
            }
        } catch (Exception e) {
            log.error("isFavorite", e);
        }
        return false;
    }

    /**
     * 將該工作報告加入收藏
     * @param wr_Id 工作報告Sid
     * @param wr_no 工作報告單號
     * @param loginUserSid  登入者Sid
     */
    public void addFavorite(String wr_Id, String wr_no, Integer loginUserSid) {
        WRFavorite wRFavorite = wRFavoriteManager.getWRFavoriteByloginUserSidAndWRSid(wr_Id, loginUserSid);
        if (wRFavorite != null) {
            wRFavorite.setStatus(Activation.ACTIVE);
            wRFavoriteManager.updateWRFavorite(wRFavorite, loginUserSid);
        } else {
            wRFavorite = new WRFavorite();
            wRFavorite.setWr_no(wr_no);
            wRFavorite.setWr_sid(wr_Id);
            wRFavoriteManager.createWRFavorite(wRFavorite, loginUserSid);
        }
    }

    /**
     * 將該工作報告移除收藏
     * @param wr_Id 工作報告Sid
     * @param wr_no 工作報告單號
     * @param loginUserSid 登入者Sid 
     */ 
    public void removeFavorite(String wr_Id, String wr_no, Integer loginUserSid) {
        WRFavorite wRFavorite = wRFavoriteManager.getWRFavoriteByloginUserSidAndWRSid(wr_Id, loginUserSid);
        if (wRFavorite != null) {
            wRFavorite.setStatus(Activation.INACTIVE);
            wRFavoriteManager.updateWRFavorite(wRFavorite, loginUserSid);
        }
    }

}
