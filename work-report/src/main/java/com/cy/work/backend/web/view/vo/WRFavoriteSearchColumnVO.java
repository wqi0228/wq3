/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class WRFavoriteSearchColumnVO implements Serializable {


    private static final long serialVersionUID = 7459182449418160340L;

    /** 序 */
    @Setter
    @Getter
    private WRColumnDetailVO index;

    @Setter
    @Getter
    private WRColumnDetailVO favoriteDate;

    @Setter
    @Getter
    private WRColumnDetailVO theme;

    @Setter
    @Getter
    private String pageCount = "50";
}
