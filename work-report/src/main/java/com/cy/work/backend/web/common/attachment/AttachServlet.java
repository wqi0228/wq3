/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.attachment;

import com.cy.work.backend.web.logic.utils.attachment.AttachmentUtils;
import com.cy.work.backend.web.logic.vo.AttachmentVO;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.MessageFormat;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 附件預覽元件
 * @author brain0925_liao
 */
public interface AttachServlet {
    
    public final static String PARAM_SID_KEY = "sid";
    public final static String PARAM_TIME_KEY = "time";
    public final static String PARAM_ATTACH_SERVICE_KEY = "service";
    
    final Logger logger = LoggerFactory.getLogger(AttachServlet.class);
    
    default void doGet(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext) throws ServletException, IOException {
        Object attachmentSid = this.findAttachSid(request);
        String uploadTime = this.getUploadTimeParameter(request);
        AttachmentVO attachment = this.getAttachAttachmentVOBySid(attachmentSid);
        File attachmentFile = this.getServerSideFile(attachment);
        if (!this.validateParameters(attachment, uploadTime, attachmentFile)) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        this.sendAttachment(request, response, servletContext, attachment, attachmentFile);
    }
    
    default void sendAttachment(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
            AttachmentVO attachment, File attachmentFile) throws IOException {
        String encodedFileName = AttachmentUtils.encodeFileName(attachment.getAttName());
        String mimeType
                = AttachmentUtils.getNoneNullMimeType(servletContext, attachment.getAttName());
        String contentDispositionPattern
                = AttachmentUtils.isMSIE(request)
                ? "{0}; filename=\"{1}\""
                : "{0}; filename=\"{1}\"; filename*=UTF-8''''\"{1}\"";
        String contentDisposition
                = MessageFormat.format(contentDispositionPattern, "inline", encodedFileName);
        FileInputStream in = new FileInputStream(attachmentFile);
        OutputStream out = response.getOutputStream();
        
        response.setContentType(mimeType);
        response.setContentLength((int) attachmentFile.length());
        response.setHeader("Content-Disposition", contentDisposition);
        
        try {
            IOUtils.copy(in, out);
            out.flush();
        } finally {
            IOUtils.closeQuietly(in);
        }
    }
    
    default boolean validateParameters(AttachmentVO attachment, String uploadTime, File attachmentFile) {
        // 檢查 attachment.getCreated() 是為了預防使用者從 URL 修改 sid 即可取得其他檔案.
        return checkParam(attachment, uploadTime, attachmentFile)
                && uploadTime.equals(attachment.getParamCreateTime())
                && checkAttachmentFile(attachmentFile);
    }
    
    default boolean checkParam(AttachmentVO attachment, String uploadTime, File attachmentFile) {
        return attachment != null && uploadTime != null && attachmentFile != null;
    }
    
    default boolean checkAttachmentFile(File attachmentFile) {
        return attachmentFile.exists() && attachmentFile.isFile();
    }
    
    public AttachmentVO getAttachAttachmentVOBySid(Object attachmentSid);
    
    public File getServerSideFile(AttachmentVO attachment);
    
    default Object findAttachSid(HttpServletRequest request) {
        return this.getAttachmentSidParameter(request);
    }
    
    default String getAttachmentServiceParameter(HttpServletRequest request) {
        return request.getParameter(PARAM_ATTACH_SERVICE_KEY);
    }
    
    default String getAttachmentSidParameter(HttpServletRequest request) {
        return request.getParameter(PARAM_SID_KEY);
    }
    
    default String getUploadTimeParameter(HttpServletRequest request) {
        try {
            String timeString = request.getParameter(PARAM_TIME_KEY);
            return timeString;
        } catch (Exception e) {
            logger.error("getUploadTimeParameter Error ", e);
            System.err.println("錯誤：" + e.getMessage());
            return "";
        }
    }
}
