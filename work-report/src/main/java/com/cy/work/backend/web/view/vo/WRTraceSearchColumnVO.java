/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class WRTraceSearchColumnVO implements Serializable {


    private static final long serialVersionUID = 2965952981400013841L;
    /** 序 */
    @Setter
    @Getter
    private WRColumnDetailVO index;
    /** 部 */
    @Setter
    @Getter
    private WRColumnDetailVO fowardDep;
    /** 個 */
    @Setter
    @Getter
    private WRColumnDetailVO fowardMember;
    /** 追 */
    @Setter
    @Getter
    private WRColumnDetailVO trace;

    @Setter
    @Getter
    private WRColumnDetailVO traceType;
    /** 標籤 */
    @Setter
    @Getter
    private WRColumnDetailVO tag;

    @Setter
    @Getter
    private WRColumnDetailVO traceNotifyDate;

    @Setter
    @Getter
    private WRColumnDetailVO traceCreateUser;

    @Setter
    @Getter
    private WRColumnDetailVO traceStatus;

    @Setter
    @Getter
    private WRColumnDetailVO traceMemo;

    @Setter
    @Getter
    private WRColumnDetailVO theme;

    @Setter
    @Getter
    private WRColumnDetailVO modifyTime;

    @Setter
    @Getter
    private WRColumnDetailVO traceCreateDate;

    /** 是否閱讀 */
    @Setter
    @Getter
    private WRColumnDetailVO readed;

    @Setter
    @Getter
    private String pageCount = "50";
}
