/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.work.backend.web.logic.components.WRTraceLogicComponents;
import com.cy.work.backend.web.view.vo.WRTraceVO;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class WorkReportTraceComponent implements Serializable {


    private static final long serialVersionUID = 3981011290041189203L;

    @Getter
    private List<WRTraceVO> wRTraceVO;

    private String wrcId;

    private Integer userSid;
    @Getter
    private boolean showTraceTab = false;

    public WorkReportTraceComponent() {
    }

    public void loadData(String wrcId, Integer userSid) {
        this.userSid = userSid;
        this.wrcId = wrcId;
        loadViewData(userSid, wrcId);
    }

    public void loadData() {
        loadViewData(userSid, wrcId);
    }

    private void loadViewData(Integer userSid, String wrcId) {
        wRTraceVO = WRTraceLogicComponents.getInstance().getWRTraceVOsByWrIDAndWRTraceType(userSid, wrcId, null);
        if (wRTraceVO != null && wRTraceVO.size() > 0) {
            showTraceTab = true;
        } else {
            showTraceTab = false;
        }
    }

}
