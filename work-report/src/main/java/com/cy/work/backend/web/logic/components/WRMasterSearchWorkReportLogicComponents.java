/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.common.setting.WorkProjectTagItemSetting;
import com.cy.work.backend.web.logic.manager.WRMasterManager;
import com.cy.work.backend.web.logic.manager.WorkSettingOrgManager;
import com.cy.work.backend.web.logic.manager.WorkSettingUserManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRMasterWorkReportVO;
import com.cy.work.backend.web.view.vo.WorkReportSearchVO;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.SqlColumnType;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.google.common.collect.Lists;

/**
 * 工作報告查詢邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class WRMasterSearchWorkReportLogicComponents implements InitializingBean, Serializable {


    private static final long serialVersionUID = 1201723704931171813L;
    private static WRMasterSearchWorkReportLogicComponents instance;

    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WorkSettingOrgManager orgManager;
    @Autowired
    private WRMasterManager wRMasterManager;

    public static WRMasterSearchWorkReportLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRMasterSearchWorkReportLogicComponents.instance = this;
    }

    /**
     * 取得工作報告查詢物件List
     *
     * @param tagID 介面挑選標簽ID
     * @param status 介面挑選單據狀態ID
     * @param depSids 介面挑選建立部門Sid(基礎部門) Lis
     * @param theme 主題
     * @param content 內容
     * @param sid 工作報告Sid
     * @param searchUserSid 登入者Sid
     * @param start 建立起始時間
     * @param end 建立結束時間
     * @param sendRendReceipts 介面挑選建立部門Sid(索取讀取回條部門) Lis
     * @param searchUserDepSid 登入者部門Sid
     * @return
     */
    public List<WorkReportSearchVO> getWorkReport(String tagID, String status, List<Integer> depSids, String theme,
            String content, String sid, Integer searchUserSid,
            Date start, Date end, List<Integer> sendRendReceipts, Integer searchUserDepSid,
            List<Integer> otherBaseViewDepSids, String createUserName, SqlColumnType sqlColumnType, String order, Integer compSid) {

         List<WorkReportSearchVO> workReportSearchVOs = Lists.newArrayList();
        List<WRMasterWorkReportVO> wRMasterWorkReportVOs = wRMasterManager.findWorkReportWRMaster(depSids, tagID,
                status, theme, content, searchUserSid, start, end, sid, sendRendReceipts, searchUserDepSid, otherBaseViewDepSids, createUserName, sqlColumnType, order);
        wRMasterWorkReportVOs.forEach(item -> {
            Org departmentR = orgManager.findBySid(item.getDep_sid());
            User createUser = workSettingUserManager.findBySID(item.getCreate_usr());
            String readReceipt = (item.isReadReceiptsMemberResult()) ? "Y" : "";
            String readReceipted = "";
            if (item.isReadReceiptsMemberResult()) {
                if (item.isReadReceiptsMemberSended()) {
                    readReceipted = "Y";
                } else {
                    readReceipted = "N";
                }
            }
            workReportSearchVOs.add(new WorkReportSearchVO(item.getSid(), item.isTofowardDep(), item.isTofowardMember(), item.isTotrace(),
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getCreate_dt()), WorkProjectTagItemSetting.getWorkProjectTagName(item.getWr_tag_sid(), compSid),
                    departmentR.getOrgNameWithParentIfDuplicate(), createUser.getName(), item.getTheme(), item.getWr_status().getVal(), item.getWr_no(), ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdate_dt()),
                    item.getWRReadStatus().getVal(), transToColorCss(item.getWRReadStatus()),
                    transToBoldCss(item.getWRReadStatus()), readReceipt, tansToReadReceiptStyleCss(item.isReadReceiptsMemberResult()), item.getWREditType().getValue(), readReceipted));
        });
        return workReportSearchVOs;
    }

    /**
     * 判斷主題底線紅色或藍色(若是索取讀取回條的工作報告顯示紅色其他顯示藍色)
     *
     * @param isReadReceiptsMemberResult 是否為索取讀取回條的工作報告
     * @return
     */
    public String tansToReadReceiptStyleCss(boolean isReadReceiptsMemberResult) {
        if (isReadReceiptsMemberResult) {
            return "redAndUnderline";
        }
        return "blueAndUnderline";
    }

    /**
     * 根據已未讀狀態判斷顯示顯色(待閱讀顯示紅色其他顯示黑色)
     *
     * @param wRReadStatus 已未讀狀態
     * @return
     */
    public String transToColorCss(WRReadStatus wRReadStatus) {
        if (WRReadStatus.WAIT_READ.equals(wRReadStatus)) {
            return "color: red;";
        }
        return "";
    }

    /**
     * 根據已未讀狀態判斷顯示字體(待閱讀及未閱讀顯示粗體)
     *
     * @param wRReadStatus 已未讀狀態
     * @return
     */
    public String transToBoldCss(WRReadStatus wRReadStatus) {
        if (WRReadStatus.UNREAD.equals(wRReadStatus) || WRReadStatus.WAIT_READ.equals(wRReadStatus)) {
            return "font-weight:bold;";
        }
        return "";
    }

}
