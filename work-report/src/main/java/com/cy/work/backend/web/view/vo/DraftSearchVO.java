/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
@EqualsAndHashCode( of = {"sid"})
public class DraftSearchVO implements Serializable {

    
    private static final long serialVersionUID = -479812914924125680L;
    public DraftSearchVO(String sid) {
        this.sid = sid;
    }

    public DraftSearchVO(String draft_time, String tag_name, String department_name,
            String create_usr_name, String title, String status, String sid, String modify_time, String wr_no) {
        this.draft_time = draft_time;
        this.tag_name = tag_name;
        this.department_name = department_name;
        this.create_usr_name = create_usr_name;
        this.title = title;
        this.status = status;
        this.sid = sid;
        this.modify_time = modify_time;
        this.wr_no = wr_no;
    }

    public void replaceValue(String draft_time, String tag_name, String department_name,
            String create_usr_name, String title, String status, String modify_time, String wr_no) {
        this.draft_time = draft_time;
        this.tag_name = tag_name;
        this.department_name = department_name;
        this.create_usr_name = create_usr_name;
        this.title = title;
        this.status = status;
        this.modify_time = modify_time;
        this.wr_no = wr_no;
    }
    @Getter
    private String sid;
    @Getter
    private String draft_time;
    @Getter
    private String tag_name;
    @Getter
    private String department_name;
    @Getter
    private String create_usr_name;
    @Getter
    private String title;
    @Getter
    private String status;
    @Getter
    private String modify_time;
    @Getter
    private String wr_no;

}
