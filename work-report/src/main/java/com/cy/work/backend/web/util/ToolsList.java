
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.util;

import com.cy.commons.vo.simple.SimpleUser;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;

/**
 *
 * @author brain0925_liao
 */
public class ToolsList {

    public static <T> boolean areEqualIgnoringOrder(List<T> list1, List<T> list2, Comparator<? super T> comparator) {
        // if not the same size, lists are not equal
        if (list1.size() != list2.size()) {
            return false;
        }
        List<T> copy1 = new ArrayList<>(list1);
        List<T> copy2 = new ArrayList<>(list2);

        Collections.sort(copy1, comparator);
        Collections.sort(copy2, comparator);

        // iterate through the elements and compare them one by one using
        // the provided comparator.
        Iterator<T> it1 = copy1.iterator();
        Iterator<T> it2 = copy2.iterator();
        while (it1.hasNext()) {
            T t1 = it1.next();
            T t2 = it2.next();
            if (comparator.compare(t1, t2) != 0) {
                // as soon as a difference is found, stop looping
                return false;
            }
        }
        return true;
    }

    // 判斷該部門是否有主管們 -> 無主管:false
    // 與登入者是否為多主管之一 -> 是主管之一:true
    public static boolean isOneOfManagers(Integer userSid,List<SimpleUser> managers){
        if(CollectionUtils.isEmpty(managers))
            return false;
        for(SimpleUser manager:managers){
            if(WkCommonUtils.compareByStr(userSid, manager.getSid())){
                return true;
            }
        }
        return false;
    }
}
