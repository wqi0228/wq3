/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.cy.work.backend.web.common.setting.WorkProjectAcceptEditSetting;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 編輯權限Item Bean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@ManagedBean
public class WorkProjectAcceptEditItemBean implements Serializable {

    
    private static final long serialVersionUID = 1579330284769456699L;

    /** 取得 編輯權限SelectItems */
    public List<SelectItem> getWorkProjectAcceptEditItems() {
        return WorkProjectAcceptEditSetting.getWorkProjectAcceptEditItems();
    }

    /** 編輯權限名稱 By key */
    public String getWorkProjectAcceptEditName(String key) {
        return WorkProjectAcceptEditSetting.getWorkProjectAcceptEditName(key);
    }
}
