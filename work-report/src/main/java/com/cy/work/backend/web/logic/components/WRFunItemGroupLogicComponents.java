/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.work.backend.web.logic.manager.WRFunItemGroupManager;
import com.cy.work.backend.web.logic.manager.WRFunItemManager;
import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.cy.work.backend.web.view.vo.WRFunItemVO;
import com.cy.work.backend.web.vo.WRFunItem;
import com.cy.work.backend.web.vo.WRFunItemGroup;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 選單Group邏輯元件
 * @author brain0925_liao
 */
@Component
public class WRFunItemGroupLogicComponents implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = 612733684009751661L;

    @Autowired
    private WRFunItemGroupManager wrFunItemGroupManager;
    @Autowired
    private WRFunItemManager wrFunItemManager;

    private static WRFunItemGroupLogicComponents instance;
    @Getter
    private final Integer report_favorite = 5;

    public static WRFunItemGroupLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRFunItemGroupLogicComponents.instance = this;
    }
    /**取得選單Group*/
    public List<WRFunItemGroupVO> getWRFunItemGroupVOs() {
        List<WRFunItemGroup> wrFunItemGroups = wrFunItemGroupManager.getWRFunItemGroupByCategoryModel();
        List<WRFunItem> wrFunItems = wrFunItemManager.getWRFunItemByCategoryModel();
        List<WRFunItemGroupVO> wrFunItemGroupVOs = Lists.newArrayList();
        wrFunItemGroups.forEach(itemGroup -> {
            List<WRFunItem> groupFunItems = wrFunItems.stream()
                    .filter(p -> p.getFun_item_group_sid().equals(itemGroup.getSid()))
                    .collect(Collectors.toList());

            List<WRFunItemVO> wrFunItemVOs = Lists.newArrayList();
            groupFunItems.forEach(item -> {
                if (Strings.isNullOrEmpty(item.getUrl())) {
                    return;
                }
                WRFunItemVO wv = new WRFunItemVO(item.getFunction_title(), item.getComponent_id(), item.getUrl(),
                        item.getSeq(), item.getIcon(), item.getCount_sql(), item.getPermission_role());
                wrFunItemVOs.add(wv);
            });
            //sid = report_favorite 代表是報表快選區,預設永遠會出現
            if (wrFunItemVOs.isEmpty() && !itemGroup.getSid().equals(report_favorite)) {
                return;
            }

            WRFunItemGroupVO wfg = new WRFunItemGroupVO(String.valueOf(itemGroup.getSid()), itemGroup.getName(), itemGroup.getSeq(), itemGroup.getGroup_base_sid(), wrFunItemVOs);

            wrFunItemGroupVOs.add(wfg);
        });
        return wrFunItemGroupVOs;
    }

}
