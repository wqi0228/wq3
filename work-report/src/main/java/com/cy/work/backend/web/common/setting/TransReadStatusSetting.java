/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.setting;

import com.cy.work.backend.web.vo.enums.TransReadStatus;
import com.google.common.collect.Lists;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * 轉寄已未閱靜態資料
 *
 * @author brain0925_liao
 */
public class TransReadStatusSetting {

    /** 轉寄已未閱選項List */
    private static List<SelectItem> transReadStatus;

    /** 取得轉寄已未閱選項資料,若記憶體已有將不再行建立 */
    public static List<SelectItem> getTransReadStatusItems() {
        if (transReadStatus != null) {
            return transReadStatus;
        }
        transReadStatus = Lists.newArrayList();
        for (TransReadStatus item : TransReadStatus.values()) {
            SelectItem si1 = new SelectItem(item.name(), item.getValue());
            transReadStatus.add(si1);
        }
        return transReadStatus;
    }
}
