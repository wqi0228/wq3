/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.manager.WRGroupSetupPrivateManager;
import com.cy.work.backend.web.logic.manager.WorkSettingUserManager;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRGroupVO;
import com.cy.work.backend.web.vo.WRGroupSetupPrivate;
import com.cy.work.backend.web.vo.converter.to.GroupInfo;
import com.cy.work.backend.web.vo.converter.to.GroupInfoTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 群組邏輯元件
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRGroupSetupPrivateLogicComponents implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = 6406606751471970266L;

    @Autowired
    private WRGroupSetupPrivateManager wrGroupSetupPrivateManager;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private OrgLogicComponents orgLogicComponents;

    private static WRGroupSetupPrivateLogicComponents instance;

    public static WRGroupSetupPrivateLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRGroupSetupPrivateLogicComponents.instance = this;
    }

    /**
     * 刪除群組
     *
     * @param sid 群組Sid
     */
    public void deleteWRGroupVO(String sid) {
        if (Strings.isNullOrEmpty(sid)) {
            Preconditions.checkState(false, "無傳入刪除群組ID！");
        }
        WRGroupSetupPrivate wrGroupSetupPrivate
                = wrGroupSetupPrivateManager.getWRGroupSetupPrivateBySid(sid);
        if (wrGroupSetupPrivate == null) {
            Preconditions.checkState(false, "該群組已被刪除！");
        }

        try {
            wrGroupSetupPrivateManager.delete(sid);
        } catch (Exception e) {
            log.error("deleteWRGroupVO", e);
            Preconditions.checkState(false, "刪除群組失敗！");
        }
    }

    /**
     * 儲存群組
     * @param wrGroupVO 群組物件
     * @param loginUserSid 登入者Sid
     */
    public void saveWrGroupVO(WRGroupVO wrGroupVO, Integer loginUserSid) {
        if (wrGroupVO == null) {
            Preconditions.checkState(false, "欲存檔物件為空！");
        }

        if (Strings.isNullOrEmpty(wrGroupVO.getGroup_name())) {
            Preconditions.checkState(false, "群組名稱為必要輸入欄位！！");
        }
        if (Strings.isNullOrEmpty(wrGroupVO.getStatusID())) {
            Preconditions.checkState(false, "狀態為必要輸入欄位！！");
        }

        WRGroupSetupPrivate wrGroupSetupPrivate = null;
        if (Strings.isNullOrEmpty(wrGroupVO.getGroup_sid())) {
            wrGroupSetupPrivate = new WRGroupSetupPrivate();
        } else {
            wrGroupSetupPrivate = wrGroupSetupPrivateManager.getWRGroupSetupPrivateBySid(wrGroupVO.getGroup_sid());
            //資料取得失敗
            if (wrGroupSetupPrivate == null) {
                log.error("資料取得失敗,無法進行更新,WrGroupSetupPrivate ID : " + wrGroupVO.getGroup_sid());
                Preconditions.checkState(false, "資料取得失敗,無法進行更新,WrGroupSetupPrivate ID : " + wrGroupVO.getGroup_sid());
            }
        }
        wrGroupSetupPrivate.setGroup_name(wrGroupVO.getGroup_name());
        wrGroupSetupPrivate.setNote(wrGroupVO.getNote());
        wrGroupSetupPrivate.setStatus(Activation.valueOf(wrGroupVO.getStatusID()));
        GroupInfo groupInfo = new GroupInfo();

        wrGroupVO.getUserViewVO().forEach(item -> {
            GroupInfoTo git = new GroupInfoTo();
            git.setUser(String.valueOf(item.getSid()));
            groupInfo.getGroupInfoTos().add(git);
        });
        wrGroupSetupPrivate.setGroup_info(groupInfo);
        if (Strings.isNullOrEmpty(wrGroupVO.getGroup_sid())) {
            wrGroupSetupPrivateManager.create(wrGroupSetupPrivate, loginUserSid);
        } else {
            wrGroupSetupPrivateManager.update(wrGroupSetupPrivate, loginUserSid);
        }
    }

    /**
     * 取得群組List
     * @param loginUserSid 登入者Sid
     * @return 
     */
    public List<WRGroupVO> getWRGroupVO(Integer loginUserSid) {
        try {
            List<WRGroupVO> wrGroups = Lists.newArrayList();
            List<WRGroupSetupPrivate> wrGroupSetupPrivates = wrGroupSetupPrivateManager.getWRFunItemByCategoryModel(loginUserSid);

            wrGroupSetupPrivates.forEach(item -> {
                List<UserViewVO> userViewVOs = Lists.newArrayList();
                StringBuilder sb = new StringBuilder();
                if (item.getGroup_info() != null && item.getGroup_info().getGroupInfoTos() != null
                        && !item.getGroup_info().getGroupInfoTos().isEmpty()) {
                    item.getGroup_info().getGroupInfoTos().forEach(userSid -> {
                        User user = workSettingUserManager.findBySID(Integer.valueOf(userSid.getUser()));
                        Org dep = orgLogicComponents.findOrgBySid(user.getPrimaryOrg().getSid());
                        sb.append(orgLogicComponents.showParentDep(dep)).append("-" + user.getName()).append("\r\n");
                        userViewVOs.add(new UserViewVO(user.getSid(), user.getName(), orgLogicComponents.showParentDep(dep)));
                    });
                }
                String statusStr = "";
                if (item.getStatus().equals(Activation.ACTIVE)) {
                    statusStr = "正常";
                } else {
                    statusStr = "停用";
                }
                WRGroupVO wg = new WRGroupVO(item.getSid(), item.getGroup_name(),
                        userViewVOs, statusStr, item.getStatus().name(), item.getNote(), sb.toString());
                wrGroups.add(wg);
            });
            return wrGroups;
        } catch (Exception e) {
            log.error("getWRGroupVO", e);
        }
        return Lists.newArrayList();
    }

}
