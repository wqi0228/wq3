/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.callable;

import java.util.concurrent.Callable;

/**
 * 選單計算筆數Callable Interface
 * @author brain0925_liao
 */
public interface HomeCallable extends Callable<HomeCallableCondition> {

    @Override
    HomeCallableCondition call() throws Exception;
}
