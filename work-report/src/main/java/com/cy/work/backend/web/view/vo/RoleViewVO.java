/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 * 角色介面物件
 */
public class RoleViewVO implements Serializable {

    private static final long serialVersionUID = -5146620492729044630L;
    /**角色SID*/
    @Getter
    private final Long sid;    
    /**角色類型name*/
    @Getter
    private final String roleGroupName;
    /**角色類型SID*/
    @Getter
    private final Long roleGroupSid;   
    /**角色name*/
    @Getter
    private final String name;
    /**該角色在ad_role 所屬部門*/
    @Getter
    @Setter
    private OrgViewVo adRoleOrgViewVo;
    /**該角色在work_dep_role_mapping 所屬部門*/
    @Getter
    @Setter
    private OrgViewVo orgViewVo;
    @Getter
    private final RoleGroupViewVO roleGroupViewVO;
    @Getter
    private final Integer companySid;
    
    public RoleViewVO(Long sid,String name,Integer companySid,RoleGroupViewVO roleGroupViewVO,OrgViewVo orgViewVo,OrgViewVo adRoleOrgViewVo)
    {
        this.sid = sid;
        this.name = name;
        this.orgViewVo = orgViewVo;
        this.adRoleOrgViewVo = adRoleOrgViewVo;
        this.roleGroupName = "";
        this.roleGroupSid = null;
        this.roleGroupViewVO = roleGroupViewVO;
        this.companySid = companySid;
    }
    
    
    public RoleViewVO(Long sid,String roleGroupName,String name,Long roleGroupSid,Integer companySid)
    {
         this.sid = sid;
         this.roleGroupName = roleGroupName;
         this.name = name;
         this.roleGroupSid = roleGroupSid;
         this.roleGroupViewVO = null;
         this.companySid = companySid;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RoleViewVO other = (RoleViewVO) obj;
         if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }
}
