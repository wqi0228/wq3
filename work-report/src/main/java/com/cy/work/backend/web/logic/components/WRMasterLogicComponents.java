/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.work.backend.web.logic.manager.WRMasterCustomLogicManager;
import com.cy.work.backend.web.logic.manager.WRMasterManager;
import com.cy.work.backend.web.logic.manager.WRNoGenericComponents;
import com.cy.work.backend.web.logic.manager.WorkSettingOrgManager;
import com.cy.work.backend.web.logic.manager.WorkSettingUserManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.AttachmentVO;
import com.cy.work.backend.web.view.components.WorkReportComponent;
import com.cy.work.backend.web.view.components.WorkReportDataComponent;
import com.cy.work.backend.web.view.components.WorkReportHeaderComponent;
import com.cy.work.backend.web.view.vo.WRTraceVO;
import com.cy.work.backend.web.vo.WRMaster;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMember;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMemberTo;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.WREditType;
import com.cy.work.backend.web.vo.enums.WRStatus;
import com.cy.work.backend.web.vo.enums.WReadReceiptStatus;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import lombok.extern.slf4j.Slf4j;

/**
 * 工作報告邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRMasterLogicComponents implements InitializingBean, Serializable {


    private static final long serialVersionUID = -6765486319811839344L;

    @Autowired
    private WRMasterManager wRMasterManager;
    @Autowired
    private WorkSettingOrgManager orgManager;
    @Autowired
    private WRNoGenericComponents wRNoGenericComponents;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WRMasterCustomLogicManager wRMasterCustomLogicManager;

    private static WRMasterLogicComponents instance;

    @Autowired
    private WkJsoupUtils jsoupUtils;

    public static WRMasterLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRMasterLogicComponents.instance = this;
    }

    /**
     * 取得工作報告物件 By 工作報告Sid
     *
     * @param sid 工作報告Sid
     * @return
     */
    public WRMaster getWRMasterBySid(String sid) {
        try {
            return wRMasterManager.findBySid(sid);
        } catch (Exception e) {
            log.error("getWRMasterBySid Error", e);
        }
        return null;
    }

    /**
     * 取得工作報告介面物件 By 工作報告Sid 及 登入者Sid
     *
     * @param sid 工作報告Sid
     * @param loginUserSid 登入者Sid
     * @return
     */
    public WorkReportComponent getWorkReportComponentBySid(String sid, Integer loginUserSid) {
        try {
            WRMaster wr = wRMasterManager.findBySid(sid);
            WorkReportComponent wrc = new WorkReportComponent();
            wrc.setWorkReportHeaderComponent(transToWorkReportHeaderComponent(wr, loginUserSid));
            wrc.setWorkReportDataComponent(transToWorkReportDataComponent(wr));
            wrc.setWRMaster(wr);
            return wrc;
        } catch (Exception e) {
            log.error("GetWorkReportComponentBySid Error", e,"主單sid:",sid,"、","登入者:",loginUserSid);
        }
        return null;
    }

    /**
     * 將工作報告物件轉換成工作報告資料介面物件
     *
     * @param wr 工作報告物件
     * @return
     */
    private WorkReportDataComponent transToWorkReportDataComponent(WRMaster wr) {
        WorkReportDataComponent wdc = new WorkReportDataComponent();
        wdc.setTitle(wr.getTheme());
        wdc.setContent(wr.getContent_css());
        wdc.setRemark(wr.getMemo_css());
        return wdc;
    }

    /**
     * 將工作報告物件轉換成工作報告介面Header物件
     *
     * @param wr 工作報告物件
     * @param loginUserSid 登入者Sid
     * @return
     */
    private WorkReportHeaderComponent transToWorkReportHeaderComponent(WRMaster wr, Integer loginUserSid) {
        WorkReportHeaderComponent whc = new WorkReportHeaderComponent();
        try {
            whc.setDepartmentName(orgManager.findBySid(wr.getDep_sid()).getOrgNameWithParentIfDuplicate());
        } catch (Exception e) {
            log.error("orgManager.findBySID(wr.getDep_sid()).getName() Error", e,"、","wr 是否存在:",wr!=null);
        }
        try {
            whc.setCreateUserName(workSettingUserManager.findBySID(wr.getCreate_usr()).getName());
        } catch (Exception e) {
            log.error("workSettingUserManager.findBySID(wr.getCreate_usr()).getName() Error", e,"、","wr 是否存在:",wr!=null);
        }
        if(null!=wr.getSubmit_dt()){
            whc.setSubmitTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wr.getSubmit_dt()));
        }else {
            whc.setSubmitTime(null);
        }
        try {
            whc.setAcceptEditID(wr.getEdit_type().name());
        } catch (Exception e) {
            log.error("wr.getEdit_type().name() Error", e,"、","wr 是否存在:",wr!=null);
        }
        try {
            whc.setStatus(wr.getWr_status().getVal());
        } catch (Exception e) {
            log.error("wr.getWr_status().getVal() Error", e,"、","wr 是否存在:",wr!=null);
        }
        whc.setWorkProjectNo(wr.getWr_no());
        whc.setTagID(wr.getWr_tag_sid());
        whc.setCreateTimeTitle("建立日期");
        whc.setCreateTime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wr.getCreate_dt()));
        if (!wr.getWr_status().equals(WRStatus.COMMIT)) {
            return whc;
        }
        if (wr.getRead_receipts_member() != null && wr.getRead_receipts_member().getReadReceiptsMemberTo() != null
                && wr.getRead_receipts_member().getReadReceiptsMemberTo().size() > 0) {
            List<ReadReceiptsMemberTo> needToReadUser = wr.getRead_receipts_member().getReadReceiptsMemberTo().stream()
                    .filter(each -> each.getReader().equals(String.valueOf(loginUserSid)))
                    .collect(Collectors.toList());
            if (needToReadUser != null && needToReadUser.size() > 0) {
                whc.setShowReadReceiptBtn(true);
                ReadReceiptsMemberTo readReceiptsMemberTo = needToReadUser.get(0);
                if (readReceiptsMemberTo.getReadreceipt().equals(WReadReceiptStatus.UNREAD.name())) {
                    whc.setReadReceiptBtnAble(true);
                }
            }
        }

        return whc;
    }

    /**
     * 變更標籤
     *
     * @param sid 工作報告Sid
     * @param tagID 挑選的標籤ID
     * @param userSid 更新者Sid
     */
    public void saveTag(String sid, String tagID, Integer userSid, Integer loginUserDepSid, Integer compSid) {
        wRMasterCustomLogicManager.saveTag(sid, tagID, userSid, loginUserDepSid, compSid);
    }

    /**
     * 變更權限編輯
     *
     * @param sid 工作報告Sid
     * @param editType 變更權限編輯ID
     * @param userSid 變更者Sid
     */
    public void saveEditType(String sid, String editType, Integer userSid) {
        wRMasterCustomLogicManager.saveEditType(sid, editType, userSid);
    }

    /**
     * 提交工作報告
     *
     * @param sid 工作報告Sid
     * @param userSid 提交者Sid
     * @param depSid 提交者部門Sid
     * @throws InterruptedException
     */
    public void summitWRMaster(String sid, Integer userSid, Integer depSid, Integer compSid) throws InterruptedException, Exception {
        wRMasterCustomLogicManager.summitWRMaster(sid, userSid, depSid, compSid);
    }

    /**
     * 回復工作報告
     *
     * @param sid 工作報告Sid
     * @param userSid 回覆者Sid
     * @param replyEditVO 回覆內容物件
     * @return
     */
    public WRMaster replyWRMaster(String sid, Integer userSid, WRTraceVO replyEditVO) {
        Preconditions.checkState(!Strings.isNullOrEmpty(replyEditVO.getContent()), "請輸入回覆內容！");
        return wRMasterCustomLogicManager.replyWRMaster(sid, userSid, replyEditVO.getSid(), replyEditVO.getContent());
    }

    /**
     * 作廢工作報告
     *
     * @param sid 工作報告Sid
     * @param reason 作廢理由
     * @param userSid 作廢者Sid
     */
    public void invaildWRMaster(String sid, String reason, Integer userSid) {
        wRMasterCustomLogicManager.invaildWRMaster(sid, reason, userSid);
    }

    /**
     * 刪除工作報告
     *
     * @param sid 工作報告Sid
     * @param userSid 刪除者Sid
     */
    public void deleteWRMaster(String sid, Integer userSid) {
        wRMasterCustomLogicManager.deleteWRMaster(sid, userSid);
    }

    /**
     * 更新工作報告(主題內容備註)
     *
     * @param sid 工作報告Sid
     * @param workReportDataComponent 工作報告資料物件
     * @param userSid 更新者Sid
     */
    public void updateWRMaster(String sid, WorkReportDataComponent workReportDataComponent, Integer userSid) {
        Preconditions.checkState(!Strings.isNullOrEmpty(workReportDataComponent.getTitle()), "請填寫主題！！");
        Preconditions.checkState(!Strings.isNullOrEmpty(workReportDataComponent.getContent()), "請填寫內容！");
        wRMasterCustomLogicManager.updateContentWRMaster(sid, workReportDataComponent.getTitle(), workReportDataComponent.getContent(), workReportDataComponent.getRemark(), userSid);

    }

    /**
     * 建立工作報告
     *
     * @param wRStatus 工作報告單據狀態
     * @param workReportDataComponent 工作報告資料物件
     * @param workReportHeaderComponent 工作報告Header物件
     * @param attachmentVOs 附件介面物件List
     * @param userSid 建立者Sid
     * @param departmentSid 建立者部門Sid
     * @param companySid 建立者公司Sid
     * @return
     * @throws InterruptedException
     */
    public WRMaster createWRMaster(WRStatus wRStatus, WorkReportDataComponent workReportDataComponent,
            WorkReportHeaderComponent workReportHeaderComponent,
            List<AttachmentVO> attachmentVOs, Integer userSid,
            Integer departmentSid, Integer companySid) throws InterruptedException {
        Preconditions.checkState(!Strings.isNullOrEmpty(workReportHeaderComponent.getTagID()), "請挑選標籤！！");
        Preconditions.checkState(!Strings.isNullOrEmpty(workReportHeaderComponent.getAcceptEditID()), "請挑選編輯允許！！");
        Preconditions.checkState(!Strings.isNullOrEmpty(workReportDataComponent.getTitle()), "請填寫主題！！");
        Preconditions.checkState(!Strings.isNullOrEmpty(workReportDataComponent.getContent()), "請填寫內容1！");
        Org comp = orgManager.findBySid(companySid);
        WRMaster wrMaster = new WRMaster();
        if (wRStatus.equals(WRStatus.COMMIT)) {
            wrMaster.setWr_no(comp.getId() + wRNoGenericComponents.getSequenceNum(comp.getSid()));
        } else if (wRStatus.equals(WRStatus.DRAFT)) {
            wrMaster.setWr_no("TEMP" + comp.getId() + wRNoGenericComponents.getTempSequenceNum(comp.getSid()));
        }
        wrMaster.setComp_sid(companySid);
        wrMaster.setDep_sid(departmentSid);
        wrMaster.setWr_status(wRStatus);
        wrMaster.setHas_attachment((attachmentVOs != null && attachmentVOs.size() > 0));
        wrMaster.setWr_tag_sid(workReportHeaderComponent.getTagID());
        wrMaster.setEdit_type(WREditType.valueOf(workReportHeaderComponent.getAcceptEditID()));
        wrMaster.setTheme(workReportDataComponent.getTitle());
        wrMaster.setContent(jsoupUtils.clearCssTag(workReportDataComponent.getContent()));
        wrMaster.setContent_css(workReportDataComponent.getContent());
        wrMaster.setMemo(jsoupUtils.clearCssTag(workReportDataComponent.getRemark()));
        wrMaster.setMemo_css(workReportDataComponent.getRemark());
        wrMaster.setRead_receipts_member(new ReadReceiptsMember());
        return wRMasterCustomLogicManager.createWRMaster(wrMaster, userSid, attachmentVOs);
    }

    public void updateLockUserAndLockDate(String wr_sid, Integer lockUserSid, Date lockTime) {
        try {
            wRMasterManager.updateLockUserAndLockDate(wr_sid, lockTime, lockUserSid);
        } catch (Exception e) {
            log.error("updateLockUserAndLockDate", e);
        }
    }

    public void closeLockUserAndLockDate(String wr_sid, Integer closeUserSid) {
        try {
            wRMasterManager.closeLockUserAndLockDate(wr_sid, closeUserSid);
        } catch (Exception e) {
            log.error("closeLockUserAndLockDate", e);
        }
    }

}
