/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import com.cy.work.backend.web.logic.components.WRTransSearchWorkReportLogicComponents;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class WRTransReceviceDepVO implements Serializable {

    
    private static final long serialVersionUID = 2402590841413441638L;
    public WRTransReceviceDepVO(String sid) {
        this.sid = sid;
    }

    public WRTransReceviceDepVO(String sid, boolean tofowardDep, boolean tofowardMember, boolean totrace, String no,
            String sendDep, String sendUser, String sendTime, String receviceDep,
            String tag, String title, String workreport_modifyTime, String readed,
            String readStatusColor, String readStatusTextBold) {
        this.sid = sid;
        this.tofowardDep = tofowardDep;
        this.tofowardMember = tofowardMember;
        this.totrace = totrace;
        this.no = no;
        this.sendDep = sendDep;
        this.sendUser = sendUser;
        this.sendTime = sendTime;
        this.receviceDep = receviceDep;
        this.tag = tag;
        this.title = title;
        this.workreport_modifyTime = workreport_modifyTime;
        this.readed = readed;
        this.readStatusColor = readStatusColor;
        this.readStatusTextBold = readStatusTextBold;
    }

    public void replaceValue(WRTransReceviceDepVO update) {
        this.tofowardDep = update.tofowardDep;
        this.tofowardMember = update.tofowardMember;
        this.totrace = update.totrace;
        this.no = update.no;
        this.sendDep = update.sendDep;
        this.sendUser = update.sendUser;
        this.sendTime = update.sendTime;
        this.receviceDep = update.receviceDep;
        this.tag = update.tag;
        this.title = update.title;
        this.workreport_modifyTime = update.workreport_modifyTime;
        this.readed = update.readed;
        this.readStatusColor = update.readStatusColor;
        this.readStatusTextBold = update.readStatusTextBold;
    }

    public void replaceReadStatus(WRReadStatus wRReadStatus) {
        this.readed = wRReadStatus.getVal();
        this.readStatusColor = WRTransSearchWorkReportLogicComponents.getInstance().transToColorCss(wRReadStatus);
        this.readStatusTextBold = WRTransSearchWorkReportLogicComponents.getInstance().transToBoldCss(wRReadStatus);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRTransReceviceDepVO other = (WRTransReceviceDepVO) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }

    @Getter
    private String sid;
    @Getter
    private boolean tofowardDep;
    @Getter
    private boolean tofowardMember;
    @Getter
    private boolean totrace;
    @Getter
    private String no;
    @Getter
    private String sendDep;
    @Getter
    private String sendUser;
    @Getter
    private String sendTime;
    @Getter
    private String receviceDep;
    @Getter
    private String tag;
    @Getter
    private String title;
    @Getter
    private String workreport_modifyTime;
    @Getter
    private String readed;
    @Getter
    private String readStatusColor;
    @Getter
    private String readStatusTextBold;
}
