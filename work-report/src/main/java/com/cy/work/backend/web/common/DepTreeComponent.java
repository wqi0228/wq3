/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.cy.commons.vo.Org;
import java.io.Serializable;
import java.util.List;

/**
 * 部門Tree 元件
 *
 * @author brain0925_liao
 */
public class DepTreeComponent implements Serializable {


    private static final long serialVersionUID = 6786758893594559444L;

    private BaseDepTreeManager depTreeManager;

    private List<Org> allOrgs;

    public DepTreeComponent() {

    }

    /**
     * 初始化部門Tree元件
     *
     * @param tempallOrgs 所有部門List
     * @param showOrgs 可被挑選部門List
     * @param topOrg 最上層部門物件
     * @param selOrgs 已挑選部門List
     * @param clearSelNodeWhenChaneDepTree 將進行切換是否顯示提用部門CheckBox時,是否要清除已挑選的部門
     * @param selectable 是否可挑選部門
     * @param selectModeSingle 是否是單選模式
     */
    public void init(List<Org> tempallOrgs, List<Org> showOrgs, Org topOrg, List<Org> selOrgs,
            boolean clearSelNodeWhenChaneDepTree, boolean selectable, boolean selectModeSingle) {
        if (this.allOrgs != null) {
            this.allOrgs.clear();
            this.allOrgs = null;
        }
        if (depTreeManager != null) {
            depTreeManager = null;
        }
        this.allOrgs = tempallOrgs;
        if (selectModeSingle) {
            depTreeManager = new SingleDepTreeManager();
        } else {
            depTreeManager = new MultipleDepTreeManager();
        }
        depTreeManager.initTreeAllComp(allOrgs, showOrgs, topOrg, clearSelNodeWhenChaneDepTree, selectable);
        depTreeManager.createDepTree();
        depTreeManager.loadSelDepTree(selOrgs);
    }

    /**
     * 初始化部門Tree元件
     *
     * @param tempallOrgs 所有部門List
     * @param showOrgs 可被挑選部門List
     * @param topOrg 最上層部門物件
     * @param selOrgs 已挑選部門List
     * @param clearSelNodeWhenChaneDepTree 將進行切換是否顯示提用部門CheckBox時,是否要清除已挑選的部門
     * @param selectable 是否可挑選部門
     * @param selectModeSingle 是否是單選模式
     * @param defaultShowDisableDep 預設是否顯示停用部門
     */
    public void init(List<Org> tempallOrgs, List<Org> showOrgs, Org topOrg, List<Org> selOrgs,
            boolean clearSelNodeWhenChaneDepTree, boolean selectable, boolean selectModeSingle, boolean defaultShowDisableDep) {
        if (depTreeManager != null) {
            depTreeManager = null;
        }
        if (selectModeSingle) {
            depTreeManager = new SingleDepTreeManager();
        } else {
            depTreeManager = new MultipleDepTreeManager();
        }
        depTreeManager.noShowDepHistory = (!defaultShowDisableDep);
        loadTree(tempallOrgs, showOrgs, topOrg, selOrgs, clearSelNodeWhenChaneDepTree, selectable);
    }

    public void loadTree(List<Org> tempallOrgs, List<Org> showOrgs, Org topOrg, List<Org> selOrgs,
            boolean clearSelNodeWhenChaneDepTree, boolean selectable) {
        if (this.allOrgs != null) {
            this.allOrgs.clear();
            this.allOrgs = null;
        }
        this.allOrgs = tempallOrgs;
        depTreeManager.initTreeAllComp(allOrgs, showOrgs, topOrg, clearSelNodeWhenChaneDepTree, selectable);
        depTreeManager.createDepTree();
        depTreeManager.loadSelDepTree(selOrgs);
    }

    /** 取得部門Tree挑選的部門 */
    public List<Org> getSelectOrg() {
        return depTreeManager.getSelOrgs();
    }

    /** 取得多選部門元件 */
    public MultipleDepTreeManager getMultipleDepTreeManager() {
        return (MultipleDepTreeManager) depTreeManager;
    }

    /** 取得單選部門元件 */
    public SingleDepTreeManager getSingleDepTreeManager() {
        return (SingleDepTreeManager) depTreeManager;
    }

}
