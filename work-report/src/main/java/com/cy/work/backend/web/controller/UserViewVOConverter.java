/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.controller;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.view.vo.UserViewVO;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
@FacesConverter("userViewVOConverter")
public class UserViewVOConverter implements Converter {

    @Autowired
    private WRUserLogicComponents wRUserLogicComponents;
    
    @Autowired
    private OrgLogicComponents orgLogicComponents;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            if ((value != null) && !"".equals(value.trim())) {
                User user = wRUserLogicComponents.findUserBySid(Integer.valueOf(value));
                Org dep = orgLogicComponents.findOrgBySid(user.getPrimaryOrg().getSid());
                return new UserViewVO(user.getSid(), user.getName(), orgLogicComponents.showParentDep(dep));
            }
        } catch (Exception e) {
            log.error("UserViewVOConverter Error",e);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof UserViewVO) {
            return String.valueOf(((UserViewVO) value).getSid());
        }
        return "";
    }
}
