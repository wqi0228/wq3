/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRHomeLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.cy.work.backend.web.vo.enums.WRFuncGroupBaseType;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Slf4j
@Scope(WebApplicationContext.SCOPE_REQUEST)
@ManagedBean
public class HomeBean implements Serializable {

    private static final long serialVersionUID = -6634266885589100599L;

    @Autowired
    private WRHomeLogicComponents wRHomeLogicComponents;
    @Getter
    private List<WRFunItemGroupVO> wrFunItemGroups;
    @Getter
    private final WRFuncGroupBaseType homeType = WRFuncGroupBaseType.HOME;
    @Getter
    private final WRFuncGroupBaseType maintainType = WRFuncGroupBaseType.MAIN_TAIN;
    @Getter
    private final WRFuncGroupBaseType searchType = WRFuncGroupBaseType.SEARCH;
    @Getter
    private final WRFuncGroupBaseType addType = WRFuncGroupBaseType.ADD;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Getter
    private boolean showMaintainBtn = false;

    @Getter
    @Setter
    private String menuitemId;

    private String keepType = homeType.getKey();

    private String lock = "false";

    @PostConstruct
    void init() {
        parseRequestParameter();
        userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        depViewVo = OrgLogicComponents.getInstance().findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = OrgLogicComponents.getInstance().findById(SecurityFacade.getCompanyId());

        List<WRFunItemGroupVO> maintain_wrFunItemGroupVO = wRHomeLogicComponents.getWRFunItemGroupByTypeAndUserPermission(maintainType.getKey(), userViewVO.getSid(), depViewVo.getSid());
        if (maintain_wrFunItemGroupVO != null && !maintain_wrFunItemGroupVO.isEmpty()) {
            maintain_wrFunItemGroupVO.forEach(item -> {
                if (item.getWrFunItemVOs() != null && !item.getWrFunItemVOs().isEmpty()) {
                    showMaintainBtn = true;
                }
            });
        }
        this.wrFunItemGroups = wRHomeLogicComponents.getWRFunItemGroupByTypeAndUserPermission(keepType, userViewVO.getSid(), depViewVo.getSid());
        log.info("HomeBean_PostConstruct");
    }

    private void parseRequestParameter() {
        if (Faces.getRequestParameterMap().containsKey("type")) {
            keepType = Faces.getRequestParameterMap().get("type");
        }
        if (Faces.getRequestParameterMap().containsKey("menuitemId")) {
            menuitemId = Faces.getRequestParameterMap().get("menuitemId");
        }
        if (Faces.getRequestParameterMap().containsKey("lock")) {
            lock = Faces.getRequestParameterMap().get("lock");
        }
    }

    public void reloadLeftMenu() throws IOException {
        parseRequestParameter();
        String url = "../home/home.xhtml?type=" + keepType + "&menuitemId=" + menuitemId + "&lock=" + lock;
        log.info("ReLoad Menu " + url);
        Faces.getExternalContext().redirect(url);
    }

}
