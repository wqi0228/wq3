/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 * 角色類型介面物件
 */
public class RoleGroupViewVO implements Serializable {

    private static final long serialVersionUID = -4475199448083198814L;
    /**角色類型SID*/
    @Getter
    @Setter
    private Long sid;
    /**角色類型name*/
    @Getter
    @Setter
    private String name;
    
    public RoleGroupViewVO(Long sid,String name)
    {
        this.sid = sid;
        this.name = name;
    }   
    
     @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RoleGroupViewVO other = (RoleGroupViewVO) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }
    
}
