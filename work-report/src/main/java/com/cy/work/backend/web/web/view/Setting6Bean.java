package com.cy.work.backend.web.web.view;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.testng.collections.Lists;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.MessagesUtils;
import com.cy.work.backend.web.logic.manager.setting6.Setting6Service;
import com.cy.work.backend.web.logic.manager.setting6.vo.Setting6VO;
import com.cy.work.backend.web.repository.WrBaseDepAccessInfoRepository;
import com.cy.work.backend.web.view.vo.BaseDepAccessInfoVO;
import com.cy.work.backend.web.vo.WrBaseDepAccessInfo;
import com.cy.work.backend.web.vo.converter.LimitBaseAccessViewDepConverter;
import com.cy.work.backend.web.vo.converter.OtherBaseAccessViewDepConverter;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Setting6Bean implements Serializable {


    private static final long serialVersionUID = -5485312902690427258L;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private WrBaseDepAccessInfoRepository wrBaseDepAccessInfoRepository;

    @Autowired
    private Setting6Service setting6Service;

    // ========================================================================
    // 元件ID
    // ========================================================================
    @Getter
    private final String dataTableID = "Setting6Bean_dataTableID";

    // ========================================================================
    // 變數
    // ========================================================================
    @Getter
    private List<BaseDepAccessInfoVO> allData = Lists.newArrayList();
    @Getter
    @Setter
    private boolean allDataOnlyActive = true;

    /**
     * 使用記錄
     */
    @Getter
    private List<Setting6VO> usedRecords = Lists.newArrayList();
    @Getter
    @Setter
    private boolean usedRecordsOnlyActive = true;

    @Getter
    private String inactiveDepInfo;
    @Getter
    private String inActiveDepDeleteSql;
    @Getter
    private String inActiveDepRecoverySql;

    // ========================================================================
    // 方法
    // ========================================================================
    @PostConstruct
    public void init() {
        try {
            this.prepareAllData();

            // ====================================
            // 準備使用記錄
            // ====================================
            this.prepareUsedRecords();
        } catch (Exception e) {
            MessagesUtils.showError("初始化失敗! [" + e.getMessage() + "]");
            log.error("初始化失敗! [" + e.getMessage() + "]", e);
            return;
        }
    }


    public void prepareAllData() {
        // ====================================
        // 查詢所有資料
        // ====================================
        List<WrBaseDepAccessInfo> wrBaseDepAccessInfos = this.wrBaseDepAccessInfoRepository.findAll();
        if (WkStringUtils.isEmpty(wrBaseDepAccessInfos)) {
            this.allData = Lists.newArrayList();
            return;
        }

        // ====================================
        // 組排序條件
        // ====================================
        // 1.公司別
        Comparator<BaseDepAccessInfoVO> comparator = Comparator.comparing(BaseDepAccessInfoVO::getTargetDepCompInfo);
        // 2.部門排序
        comparator = comparator.thenComparing(Comparator.comparing(BaseDepAccessInfoVO::getTargetDepOrder));

        // ====================================
        // 轉 vo 並排序
        // ====================================
        List<BaseDepAccessInfoVO> tempDatas = wrBaseDepAccessInfos.stream()
                .map(entity -> this.prepareVo(entity))
                .sorted(comparator)
                .collect(Collectors.toList());

        // ====================================
        // 過濾已停用
        // ====================================
        this.allData = tempDatas.stream()
                // .filter(vo -> Activation.ACTIVE.equals((vo.getTargetDep().getStatus())))
                .collect(Collectors.toList());

        if (this.allDataOnlyActive) {
            this.allData = tempDatas.stream()
                    .filter(vo -> Activation.ACTIVE.equals((vo.getTargetDep().getStatus())))
                    .collect(Collectors.toList());
        }

        // ====================================
        // 準備已停用資料
        // ====================================
        List<Integer> inactiveDepSids = tempDatas.stream()
                .filter(vo -> Activation.INACTIVE.equals((vo.getTargetDep().getStatus())))
                .map(vo -> vo.getTargetDep().getSid())
                .collect(Collectors.toList());

        if (WkStringUtils.notEmpty(inactiveDepSids)) {

            // this.inactiveDepInfo = inactiveDeps.stream()
            // .map(depSid -> this.prepareOrgName(depSid))
            // .collect(Collectors.joining("<br/>"));

            this.inactiveDepInfo = WkOrgUtils.prepareDepsNameByTreeStyle(inactiveDepSids, 50);

            List<String> inactiveDepSidStrs = inactiveDepSids.stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());

            this.inActiveDepDeleteSql = ""
                    + "DELETE FROM work_project.wr_base_dep_access_info "
                    + "WHERE login_user_dep IN(" + String.join(", ", inactiveDepSidStrs) + ");";

            this.inActiveDepDeleteSql = new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(inActiveDepDeleteSql);

            this.inActiveDepRecoverySql = this.prepareinactiveDepRecoverySql(tempDatas);
        }
    }


    public void prepareUsedRecords() {
        // ====================================
        // 查詢使用記錄
        // ====================================
        this.usedRecords = this.setting6Service.prepareUsedCount();

        // ====================================
        // 需要時，過濾停用單位
        // ====================================
        if (this.usedRecordsOnlyActive) {
            this.usedRecords = this.usedRecords.stream()
                    .filter(vo -> WkOrgUtils.isActive(vo.getDepSid()))
                    .collect(Collectors.toList());
        }
    }

    private String prepareinactiveDepRecoverySql(List<BaseDepAccessInfoVO> vos) {

        List<BaseDepAccessInfoVO> inactiveVos = vos.stream()
                .filter(vo -> Activation.INACTIVE.equals((vo.getTargetDep().getStatus())))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(inactiveVos)) {
            return "";
        }

        OtherBaseAccessViewDepConverter otherBaseAccessViewDepConverter = new OtherBaseAccessViewDepConverter();
        LimitBaseAccessViewDepConverter limitBaseAccessViewDepConverter = new LimitBaseAccessViewDepConverter();

        StringBuffer varname1 = new StringBuffer();
        varname1.append("INSERT ");
        varname1.append("INTO ");
        varname1.append("wr_base_dep_access_info ");
        varname1.append("( ");
        varname1.append("base_dep_access_sid, ");
        varname1.append("login_user_dep, ");
        varname1.append("other_base_access_view_dep, ");
        varname1.append("limit_base_access_view_dep, ");
        varname1.append("create_usr, ");
        varname1.append("create_dt, ");
        varname1.append("update_usr, ");
        varname1.append("update_dt ");
        varname1.append(") \r\n");
        varname1.append("    VALUES \r\n");
        
        List<String> subSqls = Lists.newArrayList();
        for (BaseDepAccessInfoVO baseDepAccessInfoVO : inactiveVos) {
            
            StringBuffer subSql = new StringBuffer();
            
            subSql.append("        (");
            subSql.append("'" + baseDepAccessInfoVO.getSid() + "',");
            subSql.append("" + baseDepAccessInfoVO.getLoginUserDepSid() + ",");
            subSql.append("'" + otherBaseAccessViewDepConverter.convertToDatabaseColumn(baseDepAccessInfoVO.getOtherBaseAccessViewDep()) + "',");
            subSql.append("'" + limitBaseAccessViewDepConverter.convertToDatabaseColumn(baseDepAccessInfoVO.getLimitBaseAccessViewDep()) + "',");
            subSql.append("" + baseDepAccessInfoVO.getCreate_usr_sid() + ",");
            subSql.append("'" + WkDateUtils.formatDate(baseDepAccessInfoVO.getCreate_dt(), WkDateUtils.YYYY_MM_DD_HH24_mm_ss2) + "',");
            if (baseDepAccessInfoVO.getUpdate_usr_sid() != null) {
                subSql.append("" + baseDepAccessInfoVO.getUpdate_usr_sid() + ",");
            } else {
                subSql.append("NULL,");
            }
            if (baseDepAccessInfoVO.getUpdate_dt() != null) {
                subSql.append("'" + WkDateUtils.formatDate(baseDepAccessInfoVO.getUpdate_dt(), WkDateUtils.YYYY_MM_DD_HH24_mm_ss2) + "'");
            } else {
                subSql.append("NULL");
            }

            subSql.append(")");
            subSqls.add(subSql.toString());
        }
        
        varname1.append(String.join(",\r\n", subSqls));
        varname1.append(";");
        return varname1.toString();
    }

    private BaseDepAccessInfoVO prepareVo(WrBaseDepAccessInfo entity) {

        BaseDepAccessInfoVO baseDepAccessInfoVO = new BaseDepAccessInfoVO(entity);

        Org targetDep = WkOrgCache.getInstance().findBySid(baseDepAccessInfoVO.getLoginUserDepSid());
        if (targetDep == null) {
            return baseDepAccessInfoVO;
        }
        baseDepAccessInfoVO.setTargetDep(targetDep);

        List<Integer> depSids = Lists.newArrayList();
        depSids.add(targetDep.getSid());

        baseDepAccessInfoVO.setTargetDepInfo(
                WkOrgUtils.prepareDepsNameByTreeStyle(
                        depSids,
                        50));

        // 部門排序
        baseDepAccessInfoVO.setTargetDepOrder(WkOrgCache.getInstance().findOrgOrderSeqBySid(targetDep.getSid()));

        // 公司名稱
        if (targetDep.getCompany() != null) {
            baseDepAccessInfoVO.setTargetDepCompInfo(
                    targetDep.getCompany().getId() + "-" + targetDep.getCompany().getName());
        }

        // 增加
        if (baseDepAccessInfoVO.getOtherBaseAccessViewDep() != null && WkStringUtils.notEmpty(baseDepAccessInfoVO.getOtherBaseAccessViewDep().getDepTos())) {
            baseDepAccessInfoVO.setAddDepSids(baseDepAccessInfoVO.getOtherBaseAccessViewDep().getDepTos().stream()
                    .map(depVo -> depVo.getSid())
                    .collect(Collectors.toList()));

            baseDepAccessInfoVO.setAddInfo(WkOrgUtils.prepareDepsNameByTreeStyle(baseDepAccessInfoVO.getAddDepSids(), 50));
        }

        // 限制
        if (baseDepAccessInfoVO.getLimitBaseAccessViewDep() != null && WkStringUtils.notEmpty(baseDepAccessInfoVO.getLimitBaseAccessViewDep().getDepTos())) {
            baseDepAccessInfoVO.setLimitDepSids(baseDepAccessInfoVO.getLimitBaseAccessViewDep().getDepTos().stream()
                    .map(depVo -> depVo.getSid())
                    .collect(Collectors.toList()));

            baseDepAccessInfoVO.setLimitInfo(WkOrgUtils.prepareDepsNameByTreeStyle(baseDepAccessInfoVO.getLimitDepSids(), 50));
        }

        return baseDepAccessInfoVO;
    }

}
