/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.util.pf;

import java.io.Serializable;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Component
public class DisplayController implements InitializingBean, Serializable {


    private static final long serialVersionUID = -8554980310976671206L;
    private static DisplayController instance;

    public static DisplayController getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        DisplayController.instance = this;
    }

    /**
     * 顯現某個具有 widgetVar 的 PF 元件 (PF5.0)
     *
     * @author cosmo
     * @param widgetVar : PF 元件 widgetVar 名稱
     */
    public void showPfWidgetVar(String widgetVar) {
        RequestContext.getCurrentInstance().execute("PF('" + widgetVar + "').show()");
    }

    /**
     * 隱藏某個具有 widgetVar 的 PF 元件 (PF5.0)
     *
     * @author cosmo
     * @param widgetVar : PF 元件 widgetVar 名稱
     */
    public void hidePfWidgetVar(String widgetVar) {
        RequestContext.getCurrentInstance().execute("PF('" + widgetVar + "').hide()");
    }

    /**
     * 顯現某個具有 widgetVar 的 PF 元件
     *
     * @author steve_chen
     * @param widgetVar : PF 元件 widgetVar 名稱
     */
    public void showWidgetVar(String widgetVar) {
        RequestContext.getCurrentInstance().execute(widgetVar + ".show()");
    }

    /**
     * 隱藏某個具有 widgetVar 的 PF 元件
     *
     * @author steve_chen
     * @param widgetVar : PF 元件 widgetVar 名稱
     */
    public void hideWidgetVar(String widgetVar) {
        RequestContext.getCurrentInstance().execute(widgetVar + ".hide()");
    }

    /**
     * update javascript from beans.
     *
     * @author steve_chen
     * @param id : PF 元件 ID
     */
    public void update(String id) {
        RequestContext.getCurrentInstance().update(id);
    }

    /**
     * 清除某個具有 widgetVar 及 filter 功能的 PF 元件的 filter
     *
     * @param widgetVar
     */
    public void clearFilter(String widgetVar) {
        RequestContext.getCurrentInstance().execute("PF('" + widgetVar + "').clearFilters()");
    }

    /**
     * 執行某個具有 widgetVar 及 filter 功能的 PF 元件的 filter
     *
     * @param widgetVar
     */
    public void doFilter(String widgetVar) {
        RequestContext.getCurrentInstance().execute("PF('" + widgetVar + "').filter()");
    }

    /**
     * Execute javascript from beans.
     *
     * @param script : 要執行的 javascript
     */
    public void execute(String script) {
        RequestContext.getCurrentInstance().execute(script);
    }

}
