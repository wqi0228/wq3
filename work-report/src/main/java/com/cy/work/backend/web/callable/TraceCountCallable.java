/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.callable;

import com.cy.work.backend.web.logic.components.WorkTraceInfoLogicComponents;
import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.cy.work.backend.web.vo.enums.TraceStatus;
import com.cy.work.backend.web.vo.enums.WRFunItemComponentType;

/**
 * 追蹤計算筆數Call able
 *
 * @author brain0925_liao
 */
public class TraceCountCallable implements HomeCallable {

    /** 登入者Sid */
    private final Integer loginUserSid;
    /** 登入者部門Sid */
    private final Integer depSid;
    /** 選單需計算筆數列舉 */
    private final WRFunItemComponentType wrFunItemComponentType;
    /** 選單Group物件 */
    private final WRFunItemGroupVO wrFunItemGroupVO;

    public TraceCountCallable(WRFunItemGroupVO wrFunItemGroupVO,
            WRFunItemComponentType wrFunItemComponentType, Integer loginUserSid, Integer depSid) {
        this.wrFunItemGroupVO = wrFunItemGroupVO;
        this.wrFunItemComponentType = wrFunItemComponentType;
        this.loginUserSid = loginUserSid;
        this.depSid = depSid;
    }

    @Override
    public HomeCallableCondition call() throws Exception {
        //所有尚未完成追蹤
        Integer count = WorkTraceInfoLogicComponents.getInstance().getCountByTraceSearch(loginUserSid, depSid, TraceStatus.UN_FINISHED.name());
        HomeCallableCondition reuslt = new HomeCallableCondition();
        if (count > 0) {
            reuslt.setConntString("(" + String.valueOf(count) + ")");
            reuslt.setCssString("color: red");
        }
        reuslt.setWrFunItemComponentType(wrFunItemComponentType);
        reuslt.setWrFunItemGroupVO(wrFunItemGroupVO);
        return reuslt;
    }
}
