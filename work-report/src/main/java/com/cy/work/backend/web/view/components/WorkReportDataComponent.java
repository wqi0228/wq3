/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author brain0925_liao
 * 工作報告Data - 主題,內容,備註
 */
public class WorkReportDataComponent implements Serializable {

    private static final long serialVersionUID = -7853143863224797133L;
    /**
     * 主題
     */
    @Getter
    @Setter
    private String title;
    /**
     * 內容
     */
    @Getter
    @Setter
    private String content;
    /**
     * 備註
     */
    @Getter
    @Setter
    private String remark;

    public WorkReportDataComponent() {
        this.content= "<div/>";
    }
}
