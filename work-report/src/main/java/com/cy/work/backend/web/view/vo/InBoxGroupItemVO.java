/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class InBoxGroupItemVO  implements Serializable {

    
    private static final long serialVersionUID = 8035174125039798473L;
    public InBoxGroupItemVO(String inbox_group_sid, String message_css, boolean typeDep, String send_to, String createUserName, String time) {
        this.inbox_group_sid = inbox_group_sid;
        this.message_css = message_css;
        this.typeDep = typeDep;
        this.send_to = send_to;
        this.createUserName = createUserName;
        this.time = time;
    }    
    
    @Getter
    private String inbox_group_sid;
    @Getter
    private String message_css;
    @Getter
    private boolean typeDep;
    @Getter
    private String send_to;
    @Getter
    private String createUserName;
    @Getter
    private String time;
    
    
    
}
