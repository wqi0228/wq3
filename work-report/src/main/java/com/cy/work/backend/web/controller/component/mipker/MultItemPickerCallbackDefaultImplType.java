/**
 * 
 */
package com.cy.work.backend.web.controller.component.mipker;

import lombok.Getter;

/**
 * call back 事件,組件提供的預設流程類型
 * 
 * @author allen
 */
public enum MultItemPickerCallbackDefaultImplType {

    DEP("項目為部門, 預設提供 1.可選擇部門實作, 2.群組功能 for 單位實作"),
    ;

    @Getter
    private String descr;

    private MultItemPickerCallbackDefaultImplType(String descr) {
        this.descr = descr;
    }
}
