/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.vo.User;
import com.cy.work.backend.web.common.setting.WorkProjectTagItemSetting;
import com.cy.work.backend.web.logic.manager.WorkTraceInfoManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRTraceWorkReportVO;
import com.cy.work.backend.web.view.vo.TraceReportSearchVO;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.TraceType;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 追蹤查詢邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRTraceSearchWorkReportLogicComponents implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = 5959212212846110547L;
    private static WRTraceSearchWorkReportLogicComponents instance;

    @Autowired
    private WorkTraceInfoManager workTraceInfoManager;
    @Autowired
    private WRUserLogicComponents wRUserLogicComponents;

    public static WRTraceSearchWorkReportLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRTraceSearchWorkReportLogicComponents.instance = this;
    }

    /**
     * 取得追蹤查詢List
     *
     * @param tagID 介面挑選標籤ID
     * @param statusID 介面挑選狀態ID
     * @param traceTypeID 介面挑選追蹤狀態ID
     * @param onlyTraceNotifyDateNull 是否僅查詢空白的追蹤日期
     * @param title 主題
     * @param content 內容
     * @param traceStart 追蹤建立起始時間
     * @param traceEnd 追蹤建立結束時間
     * @param traceNotifyStart 追蹤提醒起始時間
     * @param traceNotifyEnd 追蹤提醒結束時間
     * @param searchText 模糊搜尋
     * @param loginUserSid 登入者Sid
     * @param depSid 登入者部門Sid
     * @param wr_Id 工作報告Sid
     * @param trace_Id 追蹤Sid
     * @return
     */
    public List<TraceReportSearchVO> getWorkReportTrace(String tagID, String statusID,
            String traceTypeID, boolean onlyTraceNotifyDateNull,
            String title, String content, Date traceStart,
            Date traceEnd, Date traceNotifyStart, Date traceNotifyEnd,
            String searchText, Integer loginUserSid, Integer depSid, String wr_Id, String trace_Id, Integer compSid) {
        List<TraceReportSearchVO> results = Lists.newArrayList();
        try {
            List<WRTraceWorkReportVO> wRTraceWorkReportVOs = workTraceInfoManager.getWorkReportListByTraceSearch(tagID, statusID,
                    traceTypeID, onlyTraceNotifyDateNull, title, content,
                    traceStart, traceEnd, traceNotifyStart, traceNotifyEnd, searchText, loginUserSid, depSid, wr_Id, trace_Id);

            wRTraceWorkReportVOs.forEach(item -> {
                User trace_create_usr = wRUserLogicComponents.findUserBySid(item.getTrace_create_usr());
                TraceReportSearchVO ts = new TraceReportSearchVO(item.getWr_Id(), item.getTracesid(), item.isTofowardDep(), item.isTofowardMember(), item.isTotrace(), transTransTypeString(item.getTraceType()), WorkProjectTagItemSetting.getWorkProjectTagName(item.getWr_tag_sid(), compSid), ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), item.getTraceNotifyDate()),
                        trace_create_usr.getName(), item.getTraceStatus().getValue(), item.getTrace_memo(), item.getTheme(), ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdate_dt()),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getTrace_create_dt()), item.getWRReadStatus().getVal(),
                        transToColorCss(item.getWRReadStatus()), transToBlodCss(item.getWRReadStatus()), item.getTrace_memo_css(), item.getNo());
                results.add(ts);
            });

        } catch (Exception e) {
            log.error("getWorkReportTrace", e);
        }

        return results;
    }

    /**
     * 根據追蹤類型顯示追蹤字串
     *
     * @param traceType 追蹤類行
     * @return
     */
    public String transTransTypeString(TraceType traceType) {
        if (null != traceType) {
            switch (traceType) {
                case PERSONAL:
                    return "個人追蹤";
                case OTHER_PERSON:
                    return "邀請他人";
                case DEPARTMENT:
                    return "部門追蹤";
                default:
                    break;
            }
        }
        return "";

    }

    /**
     * 根據已未讀狀態判斷顯示顯色(待閱讀顯示紅色其他顯示黑色)
     *
     * @param wRReadStatus 已未讀狀態
     * @return
     */
    public String transToColorCss(WRReadStatus wRReadStatus) {
        if (WRReadStatus.WAIT_READ.equals(wRReadStatus)) {
            return "color: red;";
        }
        return "";
    }

    /**
     * 根據已未讀狀態判斷顯示字體(待閱讀及未閱讀顯示粗體)
     *
     * @param wRReadStatus 已未讀狀態
     * @return
     */
    public String transToBlodCss(WRReadStatus wRReadStatus) {
        if (WRReadStatus.UNREAD.equals(wRReadStatus) || WRReadStatus.WAIT_READ.equals(wRReadStatus)) {
            return "font-weight:bold;";
        }
        return "";
    }
}
