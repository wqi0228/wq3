/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.common.setting.WorkProjectTagItemSetting;
import com.cy.work.backend.web.logic.manager.WRMasterManager;
import com.cy.work.backend.web.logic.manager.WorkSettingOrgManager;
import com.cy.work.backend.web.logic.manager.WorkSettingUserManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRMasterDraftVO;
import com.cy.work.backend.web.util.ToolsList;
import com.cy.work.backend.web.view.vo.DraftSearchVO;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 草稿查詢邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class WRMasterSearchDraftLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4588046784929732182L;
    private static WRMasterSearchDraftLogicComponents instance;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WorkSettingOrgManager orgManager;
    @Autowired
    private WRMasterManager wRMasterManager;

    public static WRMasterSearchDraftLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRMasterSearchDraftLogicComponents.instance = this;
    }

    /**
     * 取得部門草稿及個人草稿查詢物件List
     *
     * @param tagID         介面挑選標籤Sid
     * @param theme         主題
     * @param content       內容
     * @param sid           工作報告Sid
     * @param searchUserSid 登入者Sid
     * @param personName    建立人員Sid
     * @param isDepSearch   是否為部門草稿查詢
     * @param createTime    草稿建立日期
     * @return List
     */
    public List<DraftSearchVO> getWorkReportDraft(String tagID, String theme,
                                                  String content, String sid, Integer searchUserSid,
                                                  String personName, boolean isDepSearch, Date createTime, Integer compSid) {
        List<Integer> orgs = Lists.newArrayList();
        User user = workSettingUserManager.findBySID(searchUserSid);
        if (isDepSearch) {
            Org department = orgManager.findBySid(user.getPrimaryOrg().getSid());

            // 僅部門主管可看草稿,該部門其他同仁的草稿
            // 多主管: 僅部門主管可看草稿,該部門其他同仁的草稿
            if (ToolsList.isOneOfManagers(user.getSid(), department.getManagers())) {
                orgs.add(department.getSid());
            }

            //該部門以下的部門所開的草稿也可看
            orgManager.findChildOrgByParentSid(department.getSid()).forEach(item -> orgs.add(item.getSid()));
            List<Org> managerOrgs = WkOrgCache.getInstance().findManagerOrgs(searchUserSid);
            managerOrgs.forEach(item -> {
                if (!orgs.contains(item.getSid())) {
                    orgs.add(item.getSid());
                    orgManager.findChildOrgByParentSid(item.getSid()).forEach(childOrg -> orgs.add(childOrg.getSid()));
                }
            });
        }
        List<DraftSearchVO> workReportDraftSearchVOs = Lists.newArrayList();
        List<WRMasterDraftVO> wRMasterDraftVOs = wRMasterManager.findDraftWRMaster(orgs, tagID, sid, theme, content, searchUserSid, createTime);
        wRMasterDraftVOs.forEach(item -> {
            Org departmentR = orgManager.findBySid(item.getDep_sid());
            User createUser = workSettingUserManager.findBySID(item.getCreate_usr());
            if (!Strings.isNullOrEmpty(personName)) {
                if (!createUser.getName().toLowerCase().contains(personName.toLowerCase())) {
                    return;
                }
            }

            workReportDraftSearchVOs.add(new DraftSearchVO(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getCreate_dt()),
                    WorkProjectTagItemSetting.getWorkProjectTagName(item.getWr_tag_sid(), compSid),
                    departmentR.getOrgNameWithParentIfDuplicate(),
                    createUser.getName(),
                    item.getTheme(), item.getWr_status(), item.getSid(), ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdate_dt()), item.getWr_no()));
        });
        return workReportDraftSearchVOs;
    }

}
