/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.TransUpdateCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRInboxLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.util.pf.DataTablePickList;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.TransOrgViewVO;
import com.cy.work.backend.web.vo.enums.ForwardType;
import com.cy.work.backend.web.vo.enums.ReadRecordStatus;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author brain0925_liao
 */
@Slf4j
public class WRTransDepComponent implements Serializable {


    private static final long serialVersionUID = 8098906648026404296L;

    @Getter
    private List<TransOrgViewVO> transOrgViewVOs;

    private List<OrgViewVo> allDeps;

    private List<OrgViewVo> defaultSelDeps;

    private List<Org> allOrgs;

    private List<Org> canSelectOrgs;

    private List<Org> defaultSelectOrgs;

    private List<Org> canNotRemoveOrgs;

    private List<Org> readedOrgs;

    private MessageCallBack messageCallBack;

    private TransUpdateCallBack updateCallBack;

    private TransUpdateCallBack closeCallBack;

    private Integer loginUserSid;

    private String wr_Sid;

    private String wr_no;

    /** DataTablePickList 物件 */
    @Getter
    @Setter
    private DataTablePickList<OrgViewVo> dataPickList;

    private Integer companySid;
    @Getter
    @Setter
    private String transFilterName = "";
    @Getter
    @Setter
    private boolean addChild;

    private DepTreeComponent depTreeComponent;

    @Getter
    @Setter
    /** 部門轉寄資訊 */
    private boolean forwardDepModeByTree;

    public void changeForwardDepMode() {
        initDefaultCompData();
        if (forwardDepModeByTree) {
            loadDefaultDepTree();
        } else {
            loadDefaultPickListData();
        }
    }

    public void clear() {
        if (transOrgViewVOs != null) {
            transOrgViewVOs.clear();
        }
        if (allDeps != null) {
            allDeps.clear();
        }
        if (defaultSelDeps != null) {
            defaultSelDeps.clear();
        }
        if (allOrgs != null) {
            allOrgs.clear();
        }
        if (canSelectOrgs != null) {
            canSelectOrgs.clear();
        }
        if (defaultSelectOrgs != null) {
            defaultSelectOrgs.clear();
        }
        if (canNotRemoveOrgs != null) {
            canNotRemoveOrgs.clear();
        }
        if (readedOrgs != null) {
            readedOrgs.clear();
        }
        this.dataPickList = new DataTablePickList<>(OrgViewVo.class);
        initOrgTree();
        DisplayController.getInstance().update("spForwardDepMode");
    }

    public WRTransDepComponent(Integer loginUserSid, Integer companySid, TransUpdateCallBack updateCallBack, TransUpdateCallBack closeCallBack, MessageCallBack messageCallBack) {
        this.updateCallBack = updateCallBack;
        this.closeCallBack = closeCallBack;
        this.messageCallBack = messageCallBack;
        this.loginUserSid = loginUserSid;
        this.companySid = companySid;
        this.dataPickList = new DataTablePickList<>(OrgViewVo.class);
        this.depTreeComponent = new DepTreeComponent();
        initOrgTree();
    }

    public void closeDialog() {
        closeCallBack.doUpdateData();
    }

    private void initOrgTree() {
        Org group = new Org(1);
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        depTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                group, Lists.newArrayList(), clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    public void loadData(String wr_Sid, String wr_no) {
        this.addChild = false;
        this.forwardDepModeByTree = false;
        this.wr_Sid = wr_Sid;
        this.wr_no = wr_no;
        this.transOrgViewVOs = WRInboxLogicComponents.getInstance().getWorkInboxByWrSidAndForwardTypeAndLoginUserSid(wr_Sid,
                ForwardType.DEP, loginUserSid);
        initDefaultCompData();
        loadDefaultPickListData();
    }

    public void saveDepInBox() {
        try {
            List<Org> depOrgs = Lists.newArrayList();
            //depOrgs.addAll(canNotRemoveOrgs);
            if (forwardDepModeByTree) {
                depOrgs.addAll(depTreeComponent.getSelectOrg().stream().filter(each -> !canNotRemoveOrgs.contains(each)).collect(Collectors.toList()));
            } else {
                dataPickList.getTargetData().forEach(item -> {
                    Org dep = OrgLogicComponents.getInstance().findOrgBySid(item.getSid());
                    if (!depOrgs.contains(dep) && !canNotRemoveOrgs.contains(dep)) {
                        depOrgs.add(dep);
                    }
                    if (addChild) {
                        OrgLogicComponents.getInstance().getAllChildOrg(dep).forEach(childItem -> {
                            if (!depOrgs.contains(childItem) && !canNotRemoveOrgs.contains(dep)) {
                                depOrgs.add(childItem);
                            }
                        });
                    }
                });
            }
            depOrgs.addAll(readedOrgs);
            WRInboxLogicComponents.getInstance().saveTransForDep(wr_Sid, wr_no, loginUserSid, depOrgs, canNotRemoveOrgs);
            updateCallBack.doUpdateData();
        } catch (Exception e) {
            log.error("saveDepInBox", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void loadDefaultDepTree() {
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        Org group = new Org(user.getPrimaryOrg().getCompanySid());
        //修改,故切換顯示停用部門時,需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        depTreeComponent.init(allOrgs, canSelectOrgs,
                group, defaultSelectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    private void initDefaultCompData() {
        allDeps = Lists.newArrayList();
        defaultSelDeps = Lists.newArrayList();
        allOrgs = Lists.newArrayList();
        canSelectOrgs = Lists.newArrayList();
        defaultSelectOrgs = Lists.newArrayList();
        canNotRemoveOrgs = Lists.newArrayList();
        readedOrgs = Lists.newArrayList();
        Org companyOrg = OrgLogicComponents.getInstance().findOrgBySid(companySid);
        List<Org> allDepOrgs = OrgLogicComponents.getInstance().findOrgsByCompanySid(companyOrg.getSid());

        //若status顯示狀態為空字串,代表目前已轉寄
        List<TransOrgViewVO> selectTransOrgViewVO = transOrgViewVOs.stream().filter(each -> Strings.isNullOrEmpty(each.getStatus()))
                .collect(Collectors.toList());
        selectTransOrgViewVO.forEach(item -> {
            OrgViewVo io = new OrgViewVo(item.getTransedOrgSid(), item.getTransedOrgName(), companyOrg.getName());
            //若已讀,不可被移動
            if (!item.getTransUserSid().equals(loginUserSid)) {
                canNotRemoveOrgs.add(OrgLogicComponents.getInstance().findOrgBySid(item.getTransedOrgSid()));
            } else if (ReadRecordStatus.READ.equals(item.getReadRecordStatus())) {
                readedOrgs.add(OrgLogicComponents.getInstance().findOrgBySid(item.getTransedOrgSid()));
            } else {
                defaultSelDeps.add(io);
            }
            defaultSelectOrgs.add(OrgLogicComponents.getInstance().findOrgBySid(item.getTransedOrgSid()));
        });

        allDepOrgs.forEach(item -> {
            allOrgs.add(item);
            if (item.getStatus().equals(Activation.ACTIVE) && !readedOrgs.contains(item) && !canNotRemoveOrgs.contains(item)) {
                allDeps.add(new OrgViewVo(item.getSid(), item.getOrgNameWithParentIfDuplicate(), companyOrg.getName()));
            }
        });
        canSelectOrgs.addAll(allOrgs.stream()
                .filter(each -> !canNotRemoveOrgs.contains(each) && !readedOrgs.contains(each))
                .collect(Collectors.toList()));
    }

    private void loadDefaultPickListData() {
        dataPickList.setSourceData(removeTargetView(allDeps, defaultSelDeps));
        dataPickList.setTargetData(defaultSelDeps);
    }

    public void doTransfilterName() {
        if (Strings.isNullOrEmpty(transFilterName)) {
            dataPickList.setSourceData(removeTargetView(allDeps, dataPickList.getTargetData()));
        } else {
            dataPickList.setSourceData(removeTargetView(allDeps.stream()
                    .filter(each -> each.getName().toLowerCase().contains(transFilterName.toLowerCase()))
                    .collect(Collectors.toList()), dataPickList.getTargetData()));
        }
    }

    private List<OrgViewVo> removeTargetView(List<OrgViewVo> source, List<OrgViewVo> target) {
        List<OrgViewVo> filterSource = Lists.newArrayList();
        source.forEach(item -> {
            if (!target.contains(item)) {
                filterSource.add(item);
            }
        });
        return filterSource;
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() {
        return depTreeComponent.getMultipleDepTreeManager();
    }

}
