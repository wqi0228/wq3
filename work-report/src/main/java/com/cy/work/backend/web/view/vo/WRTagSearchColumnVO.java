/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class WRTagSearchColumnVO implements Serializable {


    private static final long serialVersionUID = -5256444490091693518L;
    @Setter
    @Getter
    private WRColumnDetailVO index;
    @Setter
    @Getter
    private WRColumnDetailVO createTime;
    @Setter
    @Getter
    private WRColumnDetailVO createUser;
    @Setter
    @Getter
    private WRColumnDetailVO categoryName;

    @Setter
    @Getter
    private WRColumnDetailVO tag;

    @Setter
    @Getter
    private WRColumnDetailVO status;
    @Setter
    @Getter
    private WRColumnDetailVO modifyTime;
    @Setter
    @Getter
    private WRColumnDetailVO seq;
    @Setter
    @Getter
    private String pageCount = "50";

}
