/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * 按鈕介面物件
 * @author brain0925_liao
 */
public class ButtonComponent implements Serializable {

    
    private static final long serialVersionUID = 2171157800426239019L;
    /**是否顯示作廢按鈕*/
    @Getter
    @Setter
    private boolean showInvalidBtn = false;
    /**是否顯示刪除按鈕*/
    @Getter
    @Setter
    private boolean showDeleteBtn = false;
    /**是否可編輯*/
    @Getter
    @Setter
    private boolean editAble = false;
    /**是否可轉寄*/
    @Getter
    @Setter
    private boolean transAble = false;
    @Getter
    @Setter
    private boolean deleteAble = false;
    @Getter
    @Setter
    private boolean editTypeAble = false;
    /**是否可回覆*/
    @Getter
    @Setter
    private boolean replyAble  = false;
    /**是否可附加檔案*/
    @Getter
    @Setter
    private boolean attachAble  = false;
    /**是否可標籤調整*/
    @Getter
    @Setter
    private boolean tagAble  = false;
    /**是否可收藏*/
    @Getter
    @Setter
    private boolean favoriteAble  = false;
    /**是否可追蹤*/
    @Getter
    @Setter
    private boolean traceAble  = false;
    /**是否可提交*/
    @Getter
    @Setter
    private boolean submmitAble  = false;
    /**是否可確定存檔*/
    @Getter
    @Setter
    private boolean sendAble  = false;
    /**是否可取消存檔*/
    @Getter
    @Setter
    private boolean cancalAble  = false;
    /**是否可上一筆*/
    @Getter
    @Setter
    private boolean previousAble  = false;
    /**是否可下一筆*/
    @Getter
    @Setter
    private boolean nextAble  = false;
    /**是否可回報表*/
    @Getter
    @Setter
    private boolean backList  = false;
    /**是否可索取讀取記錄*/
    @Getter
    @Setter
    private boolean readRcordAble  = false;
    /**是否可作廢*/
    @Getter
    @Setter
    private boolean invalidAble  = false;

}
