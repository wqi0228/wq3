/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.work.backend.web.vo.converter.to.CustomClauseVO;
import com.cy.work.backend.web.vo.enums.SqlColumnType;
import com.cy.work.backend.web.vo.enums.TraceStatus;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * @author brain0925_liao
 */
public class WorkReportSearchHeaderComponent implements Serializable {

    private static final long serialVersionUID = -2953876030328131668L;

    @Getter
    @Setter
    private String selectTag;
    @Getter
    @Setter
    private String selectStatus;
    @Getter
    @Setter
    private String selectTraceStatus;
    @Getter
    @Setter
    private String readStatus;
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private String content;
    @Getter
    @Setter
    private Date startDate;
    @Getter
    @Setter
    private Date endDate;
    @Getter
    @Setter
    private String personName;
    @Getter
    @Setter
    private String tagName;
    @Getter
    @Setter
    private Date createTime;
    @Getter
    private int dateTypeIndex = 5;

    private final int preMonth = 0;

    private final int thisMonth = 1;

    private final int nextMonth = 2;

    private final int todayIndex = 3;

    private final int none = 5;

    private final int preDay = 3;
    @Getter
    @Setter
    private Date traceDateStart;
    @Getter
    @Setter
    private Date traceDateEnd;
    @Getter
    @Setter
    private Date traceNotifyDateStart;
    @Getter
    @Setter
    private Date traceNotifyDateEnd;
    @Getter
    @Setter
    private String allSearchText;
    @Getter
    @Setter
    private String selectTransType;
    @Getter
    @Setter
    private String no;
    @Getter
    @Setter
    private SqlColumnType sqlColumnType;
    @Getter
    @Setter
    private String order;

    public void settingDefault() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        this.startDate = c.getTime();
        this.endDate = new Date();
        this.dateTypeIndex = none;
        this.selectTag = "";
        this.selectStatus = "";
        this.readStatus = "";
        this.title = "";
        this.content = "";
        this.personName = "";
        this.createTime = null;
        this.selectTraceStatus = TraceStatus.UN_FINISHED.name();
        this.traceDateStart = null;
        this.traceDateEnd = null;
        this.traceNotifyDateStart = null;
        this.traceNotifyDateEnd = null;
        this.allSearchText = "";
        this.selectTransType = "";
        this.no = "";
        this.tagName = "";
        this.sqlColumnType = SqlColumnType.WT_CREATE_DATE;
        this.order = "ASC";
    }

    public void changeDateIntervalPreMonth() {

        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusMonths(1);
        this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
        this.dateTypeIndex = preMonth;
    }

    public void changeDateIntervalThisMonth() {
        this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
        this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
        this.dateTypeIndex = thisMonth;
    }

    public void changeDateIntervalNextMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate nextDate = new LocalDate(date).plusMonths(1);
        this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
        this.dateTypeIndex = nextMonth;
    }

    public void changeDateIntervalPreDay() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusDays(1);
        this.startDate = lastDate.toDate();
        this.endDate = lastDate.toDate();
        this.dateTypeIndex = preDay;
    }

    public void changeDateIntervalToday() {
        Date today = new Date();
        this.startDate = new DateTime(today).minusDays(3).toDate();
        this.endDate = today;
        this.dateTypeIndex = todayIndex;
    }

    /**
     * 載入自訂查詢條件
     */
    public void replaceByCustomClause(CustomClauseVO vo) {
        this.dateTypeIndex = vo.getDateTypeIndex() == null ? -1 : vo.getDateTypeIndex();
        this.selectTag = vo.getSelectTag();
        this.selectStatus = vo.getSelectStatus();
        this.title = vo.getTitle();
        this.content = vo.getContent();
        this.personName = vo.getPersonName();
        this.sqlColumnType = vo.getSqlColumnType();
        this.order = vo.getOrder();

        switch (dateTypeIndex) {
            case 0:
                changeDateIntervalPreMonth();
                break;
            case 1:
                changeDateIntervalThisMonth();
                break;
            case 2:
                changeDateIntervalNextMonth();
                break;
            case 3:
                changeDateIntervalPreDay();
                break;
            default:
                startDate = new Date();
                endDate = new Date();
                break;
        }
    }

}
