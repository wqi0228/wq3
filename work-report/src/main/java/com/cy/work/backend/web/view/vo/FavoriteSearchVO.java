/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class FavoriteSearchVO implements Serializable {


    private static final long serialVersionUID = -6070514401536542530L;
    public FavoriteSearchVO(String favorite_sid) {
        this.favorite_sid = favorite_sid;
    }

    public void replaceValue(FavoriteSearchVO obj) {
        this.theme = obj.theme;
        this.favoriteDate = obj.favoriteDate;
        this.wr_no = obj.wr_no;
    }

    public FavoriteSearchVO(String favorite_sid, String wr_sid, String theme, String favoriteDate, String wr_no) {
        this.favorite_sid = favorite_sid;
        this.wr_sid = wr_sid;
        this.theme = theme;
        this.favoriteDate = favoriteDate;
        this.wr_no = wr_no;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.favorite_sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FavoriteSearchVO other = (FavoriteSearchVO) obj;
        if (!Objects.equals(this.favorite_sid, other.favorite_sid)) {
            return false;
        }
        return true;
    }
    
    

    @Getter
    private String favorite_sid;
    @Getter
    private String wr_sid;
    @Getter
    private String theme;
    @Getter
    private String favoriteDate;
    @Getter
    private String wr_no;

}
