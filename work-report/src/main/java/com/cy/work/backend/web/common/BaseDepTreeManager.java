/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.backend.web.util.ToolsUtil;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * 部門Tree 元件
 * @author brain0925_liao
 */
public class BaseDepTreeManager implements Serializable {

    private static final long serialVersionUID = -2975384497702906829L;
    /**將進行切換是否顯示提用部門CheckBox時,是否要清除已挑選的部門*/
    private boolean clearSelNodeWhenChaneDepTree;
    /**是否可挑選部門*/
    private boolean selectable;
    /** 選取的部門 */
    transient protected List<TreeNode> selNode = Lists.newArrayList();
    /** 非停用部門Tree */
    @Getter
    @Setter
    transient protected TreeNode depRootByActive;
    /** 所有部門Tree */
    @Getter
    @Setter
    transient protected TreeNode depRootByAll;
    /** 部門名稱(like 部門名稱%) */
    @Getter
    @Setter
    private String searchDepText;
    /** 是否顯示停用部門 */
    @Getter
    @Setter
    protected boolean noShowDepHistory = true;
    /** 部門樹 */
    @Getter
    @Setter
    transient protected TreeNode depRoot;
    /**預設挑選的部門List*/
    private List<Org> defaultSelOrg;
    /**可挑選的部門List*/
    private List<Org> canSelectAble;

    /**
     * @param topOrg
     * @param allOrgs
     * @param clearSelNodeWhenChaneDepTree
     * @param selectable
     */
    public void initTreeAllComp(List<Org> allOrgs, List<Org> canSelectAble, Org topOrg,
            boolean clearSelNodeWhenChaneDepTree, boolean selectable) {
        this.clearSelNodeWhenChaneDepTree = clearSelNodeWhenChaneDepTree;
        this.selectable = selectable;
        this.canSelectAble = canSelectAble;
        buildTree(allOrgs, canSelectAble, topOrg);
    }

    /**
     * 建立部門樹
     *
     * @param comp 公司
     * @param allOrgs 所有組織
     * @return
     */
    private void buildTree(List<Org> allOrgs, List<Org> canSelectAble, Org topOrg) {
        depRootByActive = new DefaultTreeNode(topOrg, null);
        depRootByAll = new DefaultTreeNode(topOrg, null);
        buildSubTree(topOrg, allOrgs, canSelectAble, depRootByActive, depRootByAll);
    }

    /**
     * 建立子部門樹
     *
     * @param parent 上層組織
     * @param allOrgs 所有組織
     * @param depRootByActive 只包含啟用
     * @param depRootByAll 只包含啟用及停用
     *
     */
    private void buildSubTree(Org parent, List<Org> allOrgs, List<Org> canSelectAble, TreeNode depRootByActive,
            TreeNode depRootByAll) {
        List<Org> children = ToolsUtil.getOrgChildren(allOrgs, parent);
        if (children == null || children.isEmpty()) {
            return;
        }
        TreeNode nodeByActive = null;
        TreeNode nodeByAll = null;
        for (Org child : children) {
            if (canSelectAble.contains(child)) {
                nodeByAll = new DefaultTreeNode(child, depRootByAll);
                nodeByAll.setSelectable(selectable);
            } else {
                nodeByAll = new DefaultTreeNode(child, depRootByAll);
                nodeByAll.setSelectable(false);
            }
            if (depRootByActive != null && child.getStatus().equals(Activation.ACTIVE)) {
                if (canSelectAble.contains(child)) {
                    nodeByActive = new DefaultTreeNode(child, depRootByActive);
                    nodeByActive.setSelectable(selectable);
                } else {
                    nodeByActive = new DefaultTreeNode(child, depRootByActive);
                    nodeByActive.setSelectable(false);
                }
            }
            buildSubTree(child, allOrgs, canSelectAble, ((nodeByActive != null) ? nodeByActive : depRootByActive), nodeByAll);
        }
    }

    /**
     * 初始化組織數將並針對defaultOrg預設選取組織數
     *
     */
    public void createDepTree() {
        if (noShowDepHistory) {
            depRoot = depRootByActive;
        } else {
            depRoot = depRootByAll;
        }
    }

    /**
     * Ajax 變更部門樹狀資料
     */
    public void changeDepTree() {

        cleanSelNode();
        if (noShowDepHistory) {
            if (clearSelNodeWhenChaneDepTree) {
                setNotSelected(depRootByActive);
            }
            expandAll(depRootByActive, false, false);
            depRoot = depRootByActive;
        } else {
            if (clearSelNodeWhenChaneDepTree) {
                setNotSelected(depRootByAll);
            }
            expandAll(depRootByAll, false, false);
            depRoot = depRootByAll;
        }
            loadSelDepTree(this.defaultSelOrg);
    }

    public void loadSelDepTree(List<Org> selOrgs) {
        this.defaultSelOrg = selOrgs;
        selNode = Lists.newArrayList();
        expandAll(depRoot, false, false);
        List<TreeNode> allNodes = Lists.newArrayList();
        storeAllSubNodes(allNodes, depRoot);
        Org o = null;
        for (TreeNode tn : allNodes) {
            o = (Org) tn.getData();
            if (selOrgs.contains(o)) {
                tn.setSelected(true);
                if (canSelectAble.contains(o)) {
                    tn.setSelectable(selectable);
                } else {
                    tn.setSelectable(false);
                }
                selNode.add(tn);
                if (tn.getChildren() != null && tn.getChildren().size() > 0) {
                    for (TreeNode trn : tn.getChildren()) {
                        expandToParent(trn);
                    }
                } else {
                    expandToParent(tn);
                }
            }
        }
    }

    /**
     * 清除已選擇的node
     */
    public void cleanSelNode() {

        searchDepText = "";
        if (clearSelNodeWhenChaneDepTree) {
            selNode = Lists.newArrayList();
        }
    }

    /**
     * 設定未選取
     *
     * @param depTree
     */
    private void setNotSelected(TreeNode depTree) {
        List<TreeNode> list = Lists.newArrayList();
        storeAllSubNodes(list, depTree);
        Org o;
        for (TreeNode tn : list) {
            o = (Org) tn.getData();
            tn.setSelected(false);
            if (canSelectAble.contains(o)) {
                tn.setSelectable(selectable);
            } else {
                tn.setSelectable(false);
                if (this.defaultSelOrg.contains(o)) {
                    tn.setSelected(true);
                }

            }
        }
    }

    /**
     * 依照部門名稱進行搜尋
     */
    public void searchDepTree() {
        expandAll(depRoot, false, false);
        List<TreeNode> allNodes = Lists.newArrayList();
        storeAllSubNodes(allNodes, depRoot);

        if (Strings.isNullOrEmpty(searchDepText)) {
            return;
        }
        Org o = null;
        for (TreeNode tn : allNodes) {
            o = (Org) tn.getData();
            if (o.getName().toUpperCase().contains(searchDepText.toUpperCase())) {
                if (tn.getChildren() != null && tn.getChildren().size() > 0) {
                    for (TreeNode trn : tn.getChildren()) {
                        expandToParent(trn);
                    }
                } else {
                    expandToParent(tn);
                }
            }
        }
    }

    /**
     * 展開該節點下的所有節點
     *
     * @param root : 根節點
     * @param isExpand : 設定為 true 則展開全部，反之則收起全部
     * @param isCheck : 設定為 true 則全部取消勾選，反之則不異動
     */
    protected void expandAll(TreeNode root, boolean isExpand, boolean isCheck) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        for (TreeNode treeNode : root.getChildren()) {
            treeNode.setExpanded(isExpand);
            if (!isExpand && isCheck) {
                treeNode.setSelected(false);
            }
            expandAll(treeNode, isExpand, isCheck);
        }
    }

    /**
     * 展開該子節點上層的父節點，若該父節點還有父節點也展開，直到根節點。
     *
     * @param children : 子節點
     */
    private void expandToParent(TreeNode children) {
        TreeNode parent = children.getParent();
        if (parent != null) {
            parent.setExpanded(true);
            expandToParent(parent);
        }
    }

    /**
     * 儲存所有子節點到list
     *
     * @param list : 存放的list
     * @param root : 根節點
     */
    public void storeAllSubNodes(List<TreeNode> list, TreeNode root) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        for (TreeNode treeNode : root.getChildren()) {
            list.add(treeNode);
            storeAllSubNodes(list, treeNode);
        }
    }

    /**
     *
     * 將已經選取部門放入預設部門
     *
     * @return
     */
    public List<Org> getSelOrgs() {
        List<Org> selOrgs = Lists.newArrayList();
        for (TreeNode tn : selNode) {
            Org o = (Org) tn.getData();
            selOrgs.add(o);
        }
        return selOrgs;
    }
}
