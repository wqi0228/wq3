/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.attachment;

import com.cy.work.backend.web.logic.components.WRAttachmentLogicComponents;
import com.cy.work.backend.web.logic.vo.AttachmentVO;
import java.util.List;

/**
 *
 * @author brain0925_liao
 */
public class WPAttachmentComponent extends AttachmentComponent {

    
    private static final long serialVersionUID = -1703902621287505254L;

    private Integer userSid;

    private Integer departmentSid;

    public WPAttachmentComponent(Integer userSid, Integer departmentSid) {
        this.userSid = userSid;
        this.departmentSid = departmentSid;
    }

    @Override
    protected List<AttachmentVO> getPersistedAttachments(String wr_Sid) {
        return WRAttachmentLogicComponents.getInstance().getAttachmentsByWRSid(wr_Sid);
    }

    @Override
    public String getDirectoryName() {
        return "wrMaster";
    }

    @Override
    protected AttachmentVO createAndPersistAttachment(String fileName, AttachmentCondition attachmentCondition) {
        if (attachmentCondition.isAutoMappingEntity()) {
            AttachmentVO result = WRAttachmentLogicComponents.getInstance().createAttachment(attachmentCondition.getEntity_sid(), attachmentCondition.getEntity_no(), fileName, userSid, departmentSid);
            if (attachmentCondition.getUploadAttCallBack() != null) {
                attachmentCondition.getUploadAttCallBack().doUploadAtt(result);
            }
            return result;
        } else {
            return WRAttachmentLogicComponents.getInstance().createAttachment("", "", fileName, userSid, departmentSid);
        }
    }

    @Override
    public Integer getFileSizeLimit() {
        return 50;
    }
}
