/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import com.cy.work.backend.web.logic.components.WRTraceSearchWorkReportLogicComponents;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class TraceReportSearchVO implements Serializable {

    
    private static final long serialVersionUID = -89328908412267902L;
    public TraceReportSearchVO(String tracesid) {
        this.tracesid = tracesid;
    }

    public TraceReportSearchVO(String wr_Id, String tracesid, boolean tofowardDep,
            boolean tofowardMember, boolean totrace, String traceType_name,
            String tag_name, String traceNotifyDate, String trace_create_usr_name,
            String traceStatus_name, String trace_memo, String theme,
            String update_dt_time, String trace_create_time,
            String readStatus, String readStatusColor, String readStatusTextBold, String trace_memo_css, String no) {
        this.wr_Id = wr_Id;
        this.tracesid = tracesid;
        this.tofowardDep = tofowardDep;
        this.tofowardMember = tofowardMember;
        this.totrace = totrace;
        this.traceType_name = traceType_name;
        this.tag_name = tag_name;
        this.traceNotifyDate = traceNotifyDate;
        this.trace_create_usr_name = trace_create_usr_name;
        this.traceStatus_name = traceStatus_name;
        this.trace_memo = trace_memo;
        this.theme = theme;
        this.update_dt_time = update_dt_time;
        this.trace_create_time = trace_create_time;
        this.readStatus = readStatus;
        this.readStatusColor = readStatusColor;
        this.readStatusTextBold = readStatusTextBold;
        this.trace_memo_css = trace_memo_css;
        this.no = no;
    }

    public void replaceReadStatus(WRReadStatus wRReadStatus) {
        this.readStatus = wRReadStatus.getVal();
        this.readStatusColor = WRTraceSearchWorkReportLogicComponents.getInstance().transToColorCss(wRReadStatus);
        this.readStatusTextBold = WRTraceSearchWorkReportLogicComponents.getInstance().transToBlodCss(wRReadStatus);
    }

    public void replaceValue(TraceReportSearchVO updateObject) {
        this.tofowardDep = updateObject.tofowardDep;
        this.tofowardMember = updateObject.tofowardMember;
        this.totrace = updateObject.totrace;
        this.traceType_name = updateObject.traceType_name;
        this.tag_name = updateObject.tag_name;
        this.traceNotifyDate = updateObject.traceNotifyDate;
        this.trace_create_usr_name = updateObject.trace_create_usr_name;
        this.traceStatus_name = updateObject.traceStatus_name;
        this.trace_memo = updateObject.trace_memo;
        this.theme = updateObject.theme;
        this.update_dt_time = updateObject.update_dt_time;
        this.trace_create_time = updateObject.trace_create_time;
        this.readStatus = updateObject.readStatus;
        this.readStatusColor = updateObject.readStatusColor;
        this.readStatusTextBold = updateObject.readStatusTextBold;
        this.trace_memo_css = updateObject.trace_memo_css;
        this.no = updateObject.no;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.tracesid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TraceReportSearchVO other = (TraceReportSearchVO) obj;
        if (!Objects.equals(this.tracesid, other.tracesid)) {
            return false;
        }
        return true;
    }

    @Getter
    private String wr_Id;
    @Getter
    private String tracesid;
    @Getter
    private boolean tofowardDep;
    @Getter
    private boolean tofowardMember;
    @Getter
    private boolean totrace;
    @Getter
    private String traceType_name;
    @Getter
    private String tag_name;
    @Getter
    private String traceNotifyDate;
    @Getter
    private String trace_create_usr_name;
    @Getter
    private String traceStatus_name;
    @Getter
    private String trace_memo;
    @Getter
    private String theme;
    @Getter
    private String update_dt_time;
    @Getter
    private String trace_create_time;
    @Getter
    private String readStatus;
    @Getter
    private String readStatusColor;
    @Getter
    private String readStatusTextBold;
    @Getter
    private String trace_memo_css;
    @Getter
    private String no;
}
