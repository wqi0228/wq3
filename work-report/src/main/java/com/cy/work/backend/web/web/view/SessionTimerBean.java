/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.work.backend.web.util.pf.DisplayController;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_SESSION)
@ManagedBean
public class SessionTimerBean implements Serializable {


    private static final long serialVersionUID = -5631984262346785552L;

    @PostConstruct
    void init() {
        log.info("SessionTimerBean - Session產生");
    }
    
    public void reStartIdle() {
        //log.info("執行動作");
        DisplayController.getInstance().execute("reStartIdleMonitor();");
    }
    
    @PreDestroy
    void destory() {
        log.info("SessionTimerBean - 銷毀");
    }
}
