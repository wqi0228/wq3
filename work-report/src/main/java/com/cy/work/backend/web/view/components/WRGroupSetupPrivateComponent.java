/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.enums.Activation;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.ReLoadCallBack;
import com.cy.work.backend.web.logic.components.WRGroupSetupPrivateLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.WRGroupVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class WRGroupSetupPrivateComponent implements Serializable {


    private static final long serialVersionUID = 1811899242837823463L;
    @Setter
    @Getter
    private String statsId;
    @Setter
    @Getter
    private String filterValue;
    @Setter
    @Getter
    private List<WRGroupVO> wrGroupVOs;
    @Getter
    @Setter
    private WRGroupVO selectItem;

    private Integer loginUserSid;
    @Getter
    private WRGroupSetupPrivateMaitainComponent maintainComponent;

    private MessageCallBack messageCallBack;
    @Getter
    private String deleteName;

    private ReLoadCallBack parentReloadCallBack;
    @Getter
    private String group_setup_private_dlg_panel_table_id;
    @Getter
    private String group_setup_private_dlg_panel_table_wv;

    public WRGroupSetupPrivateComponent(String group_setup_private_setting_dlg_wv,
            String groupMaintainComponent_pickPanel,
            String wrGroup_deptUnitDlg,
            String wrGroup_dep,
            String group_setup_private_dlg_panel_table_wv, 
            String group_setup_private_dlg_panel_table_id, Integer loginUserSid,
            MessageCallBack messageCallBack, ReLoadCallBack parentReloadCallBack) {
        this.loginUserSid = loginUserSid;
        this.messageCallBack = messageCallBack;
        this.parentReloadCallBack = parentReloadCallBack;
        this.group_setup_private_dlg_panel_table_id = group_setup_private_dlg_panel_table_id;
        this.group_setup_private_dlg_panel_table_wv = group_setup_private_dlg_panel_table_wv;
        this.maintainComponent = new WRGroupSetupPrivateMaitainComponent(group_setup_private_setting_dlg_wv,groupMaintainComponent_pickPanel,wrGroup_deptUnitDlg,wrGroup_dep,loginUserSid, messageCallBack, groupReloadCallBack);
        this.wrGroupVOs = Lists.newArrayList();
    }

    public void closeGroupManager() {
        if (this.wrGroupVOs != null) {
            this.wrGroupVOs.clear();
            DisplayController.getInstance().update(group_setup_private_dlg_panel_table_id);
        }
        parentReloadCallBack.onReload();
    }

    public void loadData() {
        this.statsId = Activation.ACTIVE.name();
        this.filterValue = "";
        callSearch();
    }

    private void doFilter() {
        List<WRGroupVO> tempWRGroupVO = Lists.newArrayList();
        if (wrGroupVOs != null) {

            wrGroupVOs.forEach(item -> {
                if (!Strings.isNullOrEmpty(statsId) && !item.getStatusID().equals(statsId)) {
                    return;
                }

                if (!Strings.isNullOrEmpty(filterValue)) {
                    String groupName = (Strings.isNullOrEmpty(item.getGroup_name())) ? "" : item.getGroup_name();
                    String note = (Strings.isNullOrEmpty(item.getNote())) ? "" : item.getNote();
                    if (!groupName.toLowerCase().contains(filterValue.toLowerCase())
                            && !note.toLowerCase().contains(filterValue.toLowerCase())) {
                        return;
                    }
                }
                tempWRGroupVO.add(item);
            });

        }
        wrGroupVOs = tempWRGroupVO;
    }

    public void callSearch() {
        doSearch();
        DisplayController.getInstance().execute("selectDataTablePage('" + group_setup_private_dlg_panel_table_wv + "',0);");
    }

    private void doSearch() {
        try {
            this.wrGroupVOs = WRGroupSetupPrivateLogicComponents.getInstance().getWRGroupVO(loginUserSid);
            doFilter();
            DisplayController.getInstance().update(group_setup_private_dlg_panel_table_id);
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
            log.error("doSearch", e);
        }
    }

    public void doDeleteWRGroup() {
        try {
            WRGroupSetupPrivateLogicComponents.getInstance().deleteWRGroupVO(selectItem.getGroup_sid());
            loadData();
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
            log.error("doDeleteWRGroup", e);
        }
    }

    public void toDeleteWRGroup(String sid) {
        if (!Strings.isNullOrEmpty(sid)) {
            List<WRGroupVO> selWRGroupVO = wrGroupVOs.stream()
                    .filter(each -> each.getGroup_sid().equals(sid))
                    .collect(Collectors.toList());
            if (selWRGroupVO != null && !selWRGroupVO.isEmpty()) {
                selectItem = selWRGroupVO.get(0);
            }
        }
        if (selectItem == null) {
            messageCallBack.showMessage("載入刪除物件失敗");
            return;
        }
        deleteName = selectItem.getGroup_name();
        DisplayController.getInstance().update(group_setup_private_dlg_panel_table_id);

    }

    public void addWRGroupSetupPrivateData() {
        maintainComponent.loadData(null);
    }

    public void loadWRGroupSetupPrivateData() {
        WRGroupVO wo = null;
        if (selectItem != null) {
            List<WRGroupVO> selWRGroupVO = wrGroupVOs.stream()
                    .filter(each -> each.getGroup_sid().equals(selectItem.getGroup_sid()))
                    .collect(Collectors.toList());
            if (selWRGroupVO != null && !selWRGroupVO.isEmpty()) {
                wo = selWRGroupVO.get(0);
            }
        }
        maintainComponent.loadData(wo);
    }

    private final ReLoadCallBack groupReloadCallBack = new ReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 5942785592619133372L;

        @Override
        public void onReload() {
            doSearch();
        }

    };

}
