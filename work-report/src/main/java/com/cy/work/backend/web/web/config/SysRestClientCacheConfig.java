package com.cy.work.backend.web.web.config;

import com.cy.system.rest.client.util.SystemInitCache;

public class SysRestClientCacheConfig implements SystemInitCache {
    
    @Override
    public boolean initOrgClientCache() {
        return true;
    }

    @Override
    public boolean initUserClientCache() {
        return true;
    }

    @Override
    public boolean initRoleClientCache() {
        return true;
    }

    @Override
    public boolean initRoleGroupClientCache() {
        return true;
    }

    @Override
    public boolean initCurrencyClientCache() {
        return true;
    }

    @Override
    public boolean initMenuItemClientCache() {
        return true;
    }

    @Override
    public boolean initPermissionClientCache() {
        return true;
    }

    @Override
    public boolean initSimpleMenuItemClientCache() {
        return true;
    }

    @Override
    public boolean initSimpleUserClientCache() {
        return true;
    }

    @Override
    public boolean initSimpleOrgClientCache() {
        return true;
    }

    @Override
    public boolean initSimpleRoleClientCache() {
        return true;
    }

    @Override
    public boolean initMenuWithRolesClientCache() {
        return true;
    }

    @Override
    public boolean initOrgWithChildrenClientCache() {
        return true;
    }

    @Override
    public boolean initRoleWithPermissionsClientCache() {
        return true;
    }

    @Override
    public boolean initUserWithRolesClientCache() {
        return true;
    }

}
