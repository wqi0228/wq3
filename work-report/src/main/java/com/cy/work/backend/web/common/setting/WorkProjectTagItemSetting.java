/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.setting;

import com.cy.work.backend.web.logic.components.WRTagLogicComponents;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.vo.WRTag;
import com.cy.work.backend.web.vo.enums.WRTagCategoryType;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import lombok.extern.slf4j.Slf4j;

/**
 * 標籤靜態資料
 *
 * @author brain0925_liao
 */
@Slf4j
public class WorkProjectTagItemSetting {

    /** 取得標籤選項資料,若記憶體已有將不再行建立
     *
     * @return */
    public static List<SelectItem> getWorkProjectTagItems(Integer compSid) {
        return loadAllData(compSid);
    }

    public static List<SelectItem> getActiveWorkProjectTagItems(Integer loginUserDepSid, Integer compSid) {
        List<SelectItem> workProjectTagItems = Lists.newArrayList();
        try {
            for (WRTag item : WRTagLogicComponents.getInstance().getActiveWRTag(loginUserDepSid, compSid)) {
                SelectItem si = new SelectItem(item.getSid(), item.getTag_name());
                workProjectTagItems.add(si);
            }
        } catch (Exception e) {
            log.error("getActiveWorkProjectTagItems", e);
        }
        return workProjectTagItems;
    }

    /** 載入標籤選項資料,若記憶體已有將不再行建立 */
    private static List<SelectItem> loadAllData(Integer compSid) {
        List<SelectItem> workProjectTagItems = Lists.newArrayList();
        WRTagLogicComponents wrTagLogicComponents = SpringContextHolder.getBean(WRTagLogicComponents.class);
        for (WRTag item : wrTagLogicComponents.getWRTags(compSid)) {
            if (!item.getCategory().equals(WRTagCategoryType.WORKREPORT)) {
                continue;
            }
            SelectItem si = new SelectItem(item.getSid(), item.getTag_name());
            workProjectTagItems.add(si);
        }
        return workProjectTagItems;
    }

    /**
     * 取得標籤顯示名稱 By tagID
     *
     * @param key
     * @return
     */
    public static String getWorkProjectTagName(String tagID,Integer compSid) {
        try {
            WRTagLogicComponents wrTagLogicComponents = SpringContextHolder.getBean(WRTagLogicComponents.class);
            List<WRTag> wis = wrTagLogicComponents.getWRTags(compSid);
            List<WRTag> is = wis.stream()
                    .filter(p -> String.valueOf(p.getSid()).equals(tagID))
                    .collect(Collectors.toList());
            if (is != null && is.size() > 0) {
                return is.get(0).getTag_name();
            }
        } catch (Exception e) {
            log.error("GetWorkProjectTagName Error", e);
        }
        return "";
    }

}
