/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WrBaseDepAccessInfoLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.vo.WrBaseDepAccessInfo;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.primefaces.event.SelectEvent;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 其他基礎單位可閱權限介面元件
 *
 * @author brain0925_liao
 */
@Slf4j
public class OtherBaseAccessViewDepComponent implements Serializable {


    private static final long serialVersionUID = -7964352517244023181L;

    /**
     * 可調整權限部門
     */
    @Getter
    private final List<OrgViewVo> otherBaseAccessDeps;
    /**
     * 挑選調整權限部門
     */
    @Getter
    @Setter
    private OrgViewVo selOrgViewVo;
    /**
     * 登入者Sid
     */
    private final Integer loginUserSid;
    /**
     * 登入公司Sid
     */
    private final Integer loginCompSid;
    /**
     * 訊息CallBack
     */
    private final MessageCallBack messageCallBack;
    /**
     * 檢視其他基礎單位可閱部門(OrgTree元件)
     */
    private final DepTreeComponent viewDepTreeComponent;
    /**
     * 挑選其他基礎單位可閱部門(OrgTree元件)
     */
    private final DepTreeComponent selectPermissionDepTreeComponent;
    /**
     * 挑選可調整權限部門(OrgTree元件)
     */
    private final DepTreeComponent selectDepTreeComponent;
    /**
     * 全部部門
     */
    private List<Org> allOrgs;
    /**
     * 挑選欲調整權限部門
     */
    private List<Org> selectOrgs;


    public OtherBaseAccessViewDepComponent(Integer loginUserSid, Integer loginCompSid,
                                           MessageCallBack messageCallBack) {
        this.otherBaseAccessDeps = Lists.newArrayList();
        this.messageCallBack = messageCallBack;
        this.loginUserSid = loginUserSid;
        this.loginCompSid = loginCompSid;
        this.viewDepTreeComponent = new DepTreeComponent();
        this.selectPermissionDepTreeComponent = new DepTreeComponent();
        this.selectDepTreeComponent = new DepTreeComponent();
        this.selectOrgs = Lists.newArrayList();
        initOrgTree();
        loadData();
    }

    /**
     * 挑選部門
     */
    public void onRowSelect(SelectEvent event) {
        OrgViewVo obj = (OrgViewVo) event.getObject();
        loadViewDepTree(obj);
    }

    /**
     * 載入資料
     */
    private void loadData() {
        List<Org> orgsByCompanySid = OrgLogicComponents.getInstance().findOrgsByCompanySid(loginCompSid);
        List<WrBaseDepAccessInfo> otherBaseAccessViewDepNotNullList = WrBaseDepAccessInfoLogicComponents.getInstance().findOtherBaseAccessViewDepNotNull();
        List<WrBaseDepAccessInfo> wrBaseDepAccessInfoList = Lists.newArrayList();
        for (Org org : orgsByCompanySid) {
            for (WrBaseDepAccessInfo otherWrBaseDepAccessInfo : otherBaseAccessViewDepNotNullList) {
                if (org.getSid().equals(otherWrBaseDepAccessInfo.getLoginUserDepSid())
                        && otherWrBaseDepAccessInfo.getOtherBaseAccessViewDep() != null
                        && CollectionUtils.isNotEmpty(otherWrBaseDepAccessInfo.getOtherBaseAccessViewDep().getDepTos())) {
                    wrBaseDepAccessInfoList.add(otherWrBaseDepAccessInfo);
                }
            }
        }

        List<Org> allOrg = OrgLogicComponents.getInstance().findAllOrg();
        allOrg.remove(0);
        Map<Integer, String> orgMap = allOrg.stream().collect(Collectors.toMap(Org::getSid, Org::getName, (o, n) -> n));
        try {
            this.otherBaseAccessDeps.clear();
            wrBaseDepAccessInfoList.forEach(item -> {
                this.otherBaseAccessDeps.add(new OrgViewVo(item.getLoginUserDepSid(), orgMap.get(item.getLoginUserDepSid())));
            });
            this.allOrgs = OrgLogicComponents.getInstance().findOrgsByCompanySid(loginCompSid);
        } catch (Exception e) {
            log.error("loadData ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 取得檢視權限部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getViewDepTreeManager() {
        return viewDepTreeComponent.getMultipleDepTreeManager();
    }

    /**
     * 取得挑選部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getSelectDepTreeManager() {
        return selectDepTreeComponent.getMultipleDepTreeManager();
    }

    /**
     * 取得挑選權限部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getSelectPermissionDepTreeManager() {
        return selectPermissionDepTreeComponent.getMultipleDepTreeManager();
    }

    /**
     * 載入挑選部門
     */
    public void loadSelectDepTree() {
        try {
            Org topOrg = new Org(loginCompSid);
            // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            List<Org> selectDeps = Lists.newArrayList();
            this.allOrgs.forEach(item -> {
                selectDeps.add(item);
            });
            selectDepTreeComponent.init(selectDeps, selectDeps,
                    topOrg, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
        } catch (Exception e) {
            log.error("loadSelectDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行挑選部門
     */
    public void doSelectDep() {
        try {
            selectOrgs = selectDepTreeComponent.getSelectOrg();
            this.otherBaseAccessDeps.clear();
            selectOrgs.forEach(item -> {
                this.otherBaseAccessDeps.add(new OrgViewVo(item.getSid(), item.getName()));
            });
            this.selOrgViewVo = null;
            initViewDepTreeComponent();
            DisplayController.getInstance().hidePfWidgetVar("sel_otherBaseAccessDep_dlg_wav");
        } catch (Exception e) {
            log.error("doSelectDep ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 載入檢視權限部門
     *
     * @param selOrgViewVo 挑選的部門物件
     */
    private void loadViewDepTree(OrgViewVo selOrgViewVo) {
        try {
            Org topOrg = new Org(loginCompSid);
            // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = false;
            boolean selectable = false;
            boolean selectModeSingle = false;
            Org baseOrg = WkOrgUtils.prepareBaseDep(selOrgViewVo.getSid(), OrgLevel.MINISTERIAL);
            List<Org> allChildOrg = OrgLogicComponents.getInstance().getAllChildOrg(baseOrg);
            allChildOrg.add(baseOrg);

            List<Org> otherBaseAccessShowDeps = Lists.newArrayList();
            this.allOrgs.forEach(item -> {
                if (!allChildOrg.contains(item)) {
                    otherBaseAccessShowDeps.add(item);
                }
            });
            viewDepTreeComponent.init(otherBaseAccessShowDeps, otherBaseAccessShowDeps,
                    topOrg, WrBaseDepAccessInfoLogicComponents.getInstance().getOtherBaseAccessViewDep(selOrgViewVo.getSid()),
                    clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
        } catch (Exception e) {
            log.error("loadViewDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 載入挑選權限部門
     */
    public void loadPermissionDepTree() {
        loadSelectPermissionDepTree();
    }

    /**
     * 執行挑選權限部門
     */
    public void doSelectPermissionDepTree() {
        try {
            Preconditions.checkState(selOrgViewVo != null && selOrgViewVo.getSid() != null, "請挑選部門！");

            WrBaseDepAccessInfoLogicComponents.getInstance().saveOtherBaseAccessViewDep(selOrgViewVo.getSid(), loginUserSid,
                    selectPermissionDepTreeComponent.getSelectOrg());
            loadViewDepTree(selOrgViewVo);
            DisplayController.getInstance().hidePfWidgetVar("sel_otherBaseAccessDep_permission_dlg_wav");
        } catch (Exception e) {
            log.error("doSelectPermissionDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行載入挑選權限部門
     */
    private void loadSelectPermissionDepTree() {
        try {
            Preconditions.checkState(selOrgViewVo != null && selOrgViewVo.getSid() != null, "請挑選部門！");
            Org topOrg = new Org(loginCompSid);
            // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org baseOrg = WkOrgUtils.prepareBaseDep(selOrgViewVo.getSid(), OrgLevel.MINISTERIAL);
            List<Org> allChildOrg = OrgLogicComponents.getInstance().getAllChildOrg(baseOrg);
            allChildOrg.add(baseOrg);

            List<Org> otherBaseAccessShowDeps = Lists.newArrayList();
            this.allOrgs.forEach(item -> {
                if (!allChildOrg.contains(item)) {
                    otherBaseAccessShowDeps.add(item);
                }
            });
            selectPermissionDepTreeComponent.init(otherBaseAccessShowDeps, otherBaseAccessShowDeps,
                    topOrg, WrBaseDepAccessInfoLogicComponents.getInstance().getOtherBaseAccessViewDep(selOrgViewVo.getSid()),
                    clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
            DisplayController.getInstance().showPfWidgetVar("sel_otherBaseAccessDep_permission_dlg_wav");
        } catch (Exception e) {
            log.error("loadSelectPermissionDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 建立Empty 檢視部門Tree Manager
     */
    private void initViewDepTreeComponent() {
        Org topOrg = new Org(loginCompSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = false;
        boolean selectable = false;
        boolean selectModeSingle = false;
        viewDepTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                topOrg, Lists.newArrayList(), clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    /**
     * 建立Empty 挑選權限部門Tree Manager
     */
    private void initSelectPermissionDepTreeComponent() {
        Org topOrg = new Org(loginCompSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectPermissionDepTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                topOrg, Lists.newArrayList(), clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    /**
     * 建立Empty 挑選部門Tree Manager
     */
    private void initSelectDepTreeComponent() {
        Org topOrg = new Org(loginCompSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectDepTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                topOrg, Lists.newArrayList(), clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    private void initOrgTree() {
        initViewDepTreeComponent();
        initSelectPermissionDepTreeComponent();
        initSelectDepTreeComponent();
    }

}
