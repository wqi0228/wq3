/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.work.MessagesUtils;
import com.cy.work.backend.web.common.ClearCacheManager;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Allen
 */
@Controller
@Scope("view")
@ManagedBean
public class Setting4Bean implements Serializable {


    private static final long serialVersionUID = -59673612167397335L;
    @Getter
    private String errorMessage;
    @Autowired
    private ClearCacheManager clearCacheManager;

    @PostConstruct
    public void init() {
    }

    public void clearAllCache() {
        String processMessage = this.clearCacheManager.clearAllCache();
        MessagesUtils.showInfo(processMessage);
    }
}
