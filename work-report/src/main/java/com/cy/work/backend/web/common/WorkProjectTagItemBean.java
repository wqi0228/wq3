/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.common.setting.WorkProjectTagItemSetting;
import com.cy.work.backend.web.logic.manager.WorkSettingOrgManager;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 標籤Item Bean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("request")
@Slf4j
@ManagedBean
public class WorkProjectTagItemBean implements Serializable {

    
    private static final long serialVersionUID = -101415460354603778L;

    @Autowired
    private WorkSettingOrgManager workSettingOrgManager;

    /** 取得 標籤SelectItems */
    public List<SelectItem> getWorkProjectTagItems() {
        try {
            return WorkProjectTagItemSetting.getWorkProjectTagItems(workSettingOrgManager.findByID(SecurityFacade.getCompanyId()).getSid());
        } catch (Exception e) {
            log.error("getWorkProjectTagItems ERROR", e);
        }
        return Lists.newArrayList();
    }

    public List<SelectItem> getActiveWorkProjectTagItems() {
        try {
            Integer loginUserDepSid = SecurityFacade.getPrimaryOrgSid();
            return WorkProjectTagItemSetting.getActiveWorkProjectTagItems(loginUserDepSid,
                    workSettingOrgManager.findByID(SecurityFacade.getCompanyId()).getSid());
        } catch (Exception e) {
            log.error("getActiveWorkProjectTagItems ERROR", e);
        }
        return Lists.newArrayList();
    }

    /** 標籤名稱 By tagID */
    public String getWorkProjectTagName(String tagID) {
        try {
        return WorkProjectTagItemSetting.getWorkProjectTagName(tagID,
                workSettingOrgManager.findByID(SecurityFacade.getCompanyId()).getSid());
        } catch (Exception e) {
            log.error("getWorkProjectTagName ERROR", e);
        }
        return "";
    }
}
