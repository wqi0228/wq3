/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.callable.WRInboxCallable;
import com.cy.work.backend.web.logic.manager.WRInboxManager;
import com.cy.work.backend.web.logic.manager.WRInboxSendGroupManager;
import com.cy.work.backend.web.logic.manager.WrReadRecordManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.view.vo.*;
import com.cy.work.backend.web.vo.WRInbox;
import com.cy.work.backend.web.vo.WRInboxSendGroup;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.enums.*;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 收件夾邏輯元件
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WRInboxLogicComponents implements InitializingBean, Serializable {


    private static final long serialVersionUID = -8696773015640067278L;
    private static WRInboxLogicComponents instance;

    @Autowired
    private WRInboxManager workInboxManager;
    @Autowired
    private WRInboxSendGroupManager wRInboxSendGroupManager;
    @Autowired
    private WrReadRecordManager wrReadRecordManager;
    @Autowired
    private WRUserLogicComponents wRUserLogicComponents;
    @Autowired
    private OrgLogicComponents orgLogicComponents;
    @Autowired
    private WkJsoupUtils jsoupUtils;

    public static WRInboxLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRInboxLogicComponents.instance = this;
    }

    /**
     * 該工作報告是否有轉寄給該使用者
     *
     * @param wr_Id           工作報告Sid
     * @param loginUserSid    登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     * @return
     */
    public boolean isTransPersonAndDep(String wr_Id, Integer loginUserSid, Integer loginUserDepSid) {

        boolean reuslt = false;
        try {
            //此功能僅轉寄部門使用,原本為基本單位,WORKRPT-64 已調整成向下單位
            Org nowOrg = OrgLogicComponents.getInstance().findOrgBySid(loginUserDepSid);
            List<Org> orgs = OrgLogicComponents.getInstance().getAllChildOrg(nowOrg);
            orgs.add(nowOrg);
            List<Integer> transOrgSids = Lists.newArrayList();
            orgs.forEach(item -> {
                transOrgSids.add(item.getSid());
            });
            List<WRInbox> workInboxs = workInboxManager.getWorkInboxByWrSidAndStatusActive(wr_Id);
            List<WRInbox> trans = workInboxs.stream().filter(each -> ((each.getForward_type().equals(ForwardType.DEP) && transOrgSids.contains(each.getReceive_dep()))
                            || (each.getForward_type().equals(ForwardType.PERSON) && each.getReceive().equals(loginUserSid))))
                    .collect(Collectors.toList());
            if (trans != null && !trans.isEmpty()) {
                reuslt = true;
            }
        } catch (Exception e) {
            log.error("isTransPersonAndDep", e);
        }

        return reuslt;
    }

    /**
     * 儲存部門轉寄
     *
     * @param wrSid            工作報告Sid
     * @param wr_no            工作報告單號
     * @param loginUserSid     登入者Sid
     * @param orgs             轉寄部門Sids
     * @param canNotModifyOrgs 不可被修改部門Sids
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void saveTransForDep(String wrSid, String wr_no, Integer loginUserSid, List<Org> orgs, List<Org> canNotModifyOrgs) {
        List<WRInbox> workInboxs = workInboxManager.getWorkInboxByWrSidAndForwardTypeAndLoginUserSid(wrSid,
                ForwardType.DEP, loginUserSid);
        //僅確認自己設定的轉發部門
        List<WRInbox> selWorkInbox;
        selWorkInbox = workInboxs.stream().filter(each -> each.getStatus().equals(Activation.ACTIVE) && each.getSender().equals(loginUserSid))
                .collect(Collectors.toList());
        //若DB無資料,且介面也無挑選資料,提示無挑選部門,若DB有資料,介面無挑選資料,代表需清除
        if ((selWorkInbox == null || selWorkInbox.isEmpty()) && (orgs == null || orgs.isEmpty())) {
            Preconditions.checkState(false, "尚未選擇部門，請確認！！");
        }
        //確認自己設定的轉發部門,是否有人已讀,若有人已讀不可被回收
        List<WRInbox> canNotRemoveDep = selWorkInbox.stream().filter(each -> ReadRecordStatus.READ.equals(each.getRead_record()))
                .collect(Collectors.toList());
        StringBuilder moveErrorMessage = new StringBuilder();
        canNotRemoveDep.forEach(item -> {
            List<Org> result = orgs.stream().filter(each -> each.getSid().equals(item.getReceive_dep()))
                    .collect(Collectors.toList());
            if (result == null || result.isEmpty()) {
                if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
                    moveErrorMessage.append("<br>");
                }
                Org canRemoveOrg = orgLogicComponents.findOrgBySid(item.getReceive_dep());
                moveErrorMessage.append("[" + canRemoveOrg.getName() + "]" + "已有人閱讀,不可回收");
            }
        });
        if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
            Preconditions.checkState(false, moveErrorMessage.toString());
        }
        List<WRInbox> existOrgs = workInboxs.stream().filter(each -> each.getStatus().equals(Activation.ACTIVE) && !each.getSender().equals(loginUserSid) && orgs.contains(new Org(each.getReceive_dep())))
                .collect(Collectors.toList());
        if (existOrgs != null && !existOrgs.isEmpty()) {
            StringBuilder existOrgMessage = new StringBuilder();
            existOrgs.forEach(item -> {
                if (!Strings.isNullOrEmpty(existOrgMessage.toString())) {
                    existOrgMessage.append("<br>");
                }
                Org existOrg = orgLogicComponents.findOrgBySid(item.getReceive_dep());
                existOrgMessage.append("[" + existOrg.getName() + "]" + "已有人轉寄,不可再次轉寄");

            });
            if (!Strings.isNullOrEmpty(existOrgMessage.toString())) {
                Preconditions.checkState(false, existOrgMessage.toString());
            }
        }
        //確認收件夾Group主檔是否DB是否存在
        List<WRInboxSendGroup> wRInboxSendGroups = wRInboxSendGroupManager.getWRInboxSendGroupByWrSidAndLoginUserSid(wrSid, loginUserSid);
        List<WRInboxSendGroup> wRInboxSendGroupDeps = wRInboxSendGroups.stream().filter(each -> each.getForward_type().equals(ForwardType.DEP))
                .collect(Collectors.toList());
        WRInboxSendGroup wRInboxSendGroup = null;
        if (wRInboxSendGroupDeps != null && !wRInboxSendGroupDeps.isEmpty()) {
            wRInboxSendGroup = wRInboxSendGroupDeps.get(0);
        }
        if (wRInboxSendGroup == null) {
            wRInboxSendGroup = new WRInboxSendGroup();
            wRInboxSendGroup.setMessage("");
            wRInboxSendGroup.setMessage_css("");
            wRInboxSendGroup.setWr_sid(wrSid);
            wRInboxSendGroup.setWr_no(wr_no);
            wRInboxSendGroup.setSend_to("");
            wRInboxSendGroup.setForward_type(ForwardType.DEP);
        }
        if (Strings.isNullOrEmpty(wRInboxSendGroup.getSid())) {
            wRInboxSendGroup = wRInboxSendGroupManager.createWRInboxSendGroup(wRInboxSendGroup, loginUserSid);
        } else {
            wRInboxSendGroup = wRInboxSendGroupManager.updateWRInboxSendGroup(wRInboxSendGroup, loginUserSid);
        }

        List<WRInbox> saveToDB = Lists.newArrayList();
        User loginUser = wRUserLogicComponents.findUserBySid(loginUserSid);
        String inbox_group_sid = wRInboxSendGroup.getSid();
        for (Org org : orgs) {
            List<WRInbox> alreadyInDB = selWorkInbox.stream().filter(each -> each.getReceive_dep().equals(org.getSid()))
                    .collect(Collectors.toList());
            if (alreadyInDB.size() == 0 && !canNotModifyOrgs.contains(org)) {
                WRInbox wr = new WRInbox();
                wr.setWr_sid(wrSid);
                wr.setWr_no(wr_no);
                wr.setForward_type(ForwardType.DEP);
                wr.setSender(loginUserSid);
                wr.setSender_dep(loginUser.getPrimaryOrg().getSid());
                wr.setMessage("");
                wr.setMessage_css("");
                wr.setReceive(null);
                wr.setReceive_dep(org.getSid());
                wr.setInbox_type(InboxType.INCOME_DEPT);
                wr.setInbox_group_sid(inbox_group_sid);
                wr.setRead_record(ReadRecordStatus.UNREAD);
                saveToDB.add(wr);
            }
        }

        //確認自行能進行變更的
        for (WRInbox workInbox : selWorkInbox) {
            List<Org> alreadyInSelect = orgs.stream().filter(each -> each.getSid().equals(workInbox.getReceive_dep()))
                    .collect(Collectors.toList());
            if (alreadyInSelect == null || alreadyInSelect.isEmpty()) {
                workInbox.setStatus(Activation.INACTIVE);
                saveToDB.add(workInbox);
            }
        }
        for (WRInbox wrInbox : saveToDB) {
            if (Strings.isNullOrEmpty(wrInbox.getSid())) {
                workInboxManager.createWorkInbox(wrInbox, loginUserSid);
                List<String> orgUsers = WkUserCache.getInstance().getUsersByPrimaryOrgSid(wrInbox.getReceive_dep()).stream().map(x -> x.getSid() + "").collect(Collectors.toList());

                //查詢使用者對於該主檔的閱讀紀錄
                Map<String, String> userReadRecordMap = new HashMap<>();
                if(orgUsers.size()!=0) {
                    userReadRecordMap =
                            wrReadRecordManager.findAllByUserSidsAndWrSid(orgUsers, wrSid)
                                    .stream()
                                    .collect(Collectors.toMap(WrReadRecord::getCreateUser, WrReadRecord::getReadStatus));
                }
                //查詢使用者對於該主檔的閱讀紀錄，沒有的話新增一筆閱讀紀錄
                for (String orgUser : orgUsers) {
                    String userReadRecord = userReadRecordMap.get(orgUser);
                    if (userReadRecord == null) {
                        WrReadRecord wrReadRecord = new WrReadRecord(WRReadStatus.UNREAD.name(), orgUser, new Date(), wrSid);
                        wrReadRecordManager.saveWrReadRecord(wrReadRecord);
                        continue;
                    }
                    if (!userReadRecord.equals(WRReadStatus.HASREAD.name())) {
                        wrReadRecordManager.updateReadStatus(wrSid, orgUser, WRReadStatus.UNREAD.name(), new Date());
                    }
                }
            } else {
                workInboxManager.updateWorkInbox(wrInbox, loginUserSid);
            }
        }

        List<WRInbox> workInboxUpdateOver = workInboxManager.getWorkInboxByWrSidAndForwardTypeAndLoginUserSid(wrSid, ForwardType.DEP, loginUserSid);
        List<WRInbox> selWorkInboxUpdateOver = workInboxUpdateOver.stream().filter(each -> each.getStatus().equals(Activation.ACTIVE) && each.getSender().equals(loginUserSid)).collect(Collectors.toList());

        StringBuilder sendTo = new StringBuilder();
        selWorkInboxUpdateOver.forEach(item -> {
            if (!Strings.isNullOrEmpty(sendTo.toString())) {
                sendTo.append(",");
            }
            Org dep = orgLogicComponents.findOrgBySid(item.getReceive_dep());
            sendTo.append(dep.getOrgNameWithParentIfDuplicate());
        });
        wRInboxSendGroup.setSend_to(sendTo.toString());
        wRInboxSendGroupManager.updateWRInboxSendGroup(wRInboxSendGroup, loginUserSid);
    }

    /**
     * 儲存個人轉寄
     *
     * @param wrSid        工作報告Sid
     * @param wr_no        工作報告單號
     * @param receiveUser  轉寄人Lis
     * @param message      轉寄訊息
     * @param loginUserSid 登入者Sid
     */
    public void saveReceviceUsers(String wrSid, String wr_no, List<UserViewVO> receiveUser, String message, Integer loginUserSid) {
        if (receiveUser == null || receiveUser.isEmpty()) {
            Preconditions.checkState(false, "尚未選擇使用者，請確認！！");
        }
        WRInboxSendGroup wRInboxSendGroup = new WRInboxSendGroup();
        StringBuilder sendTo = new StringBuilder("");
        receiveUser.forEach(item -> {
            if (!Strings.isNullOrEmpty(sendTo.toString())) {
                sendTo.append(",");
            }
            sendTo.append(item.getName());
        });
        wRInboxSendGroup.setSend_to(sendTo.toString());
        wRInboxSendGroup.setMessage(jsoupUtils.clearCssTag(message));
        wRInboxSendGroup.setMessage_css(message);
        wRInboxSendGroup.setWr_sid(wrSid);
        wRInboxSendGroup.setWr_no(wr_no);
        wRInboxSendGroup.setForward_type(ForwardType.PERSON);
        wRInboxSendGroup = wRInboxSendGroupManager.createWRInboxSendGroup(wRInboxSendGroup, loginUserSid);
        String groupSid = wRInboxSendGroup.getSid();
        List<WRInboxCallable> workInboxCallables = Lists.newArrayList();
        receiveUser.forEach(item -> {
            WRInboxCallable workInboxCallable = new WRInboxCallable(wrSid, wr_no, jsoupUtils.clearCssTag(message), message, loginUserSid,
                    item.getSid(), groupSid, workInboxManager);
            workInboxCallables.add(workInboxCallable);
        });
        ExecutorService pool = Executors.newFixedThreadPool(workInboxCallables.size());
        List<Integer> inBoxUserSids = Lists.newArrayList();
        try {
            CompletionService<WRInbox> completionPool = new ExecutorCompletionService<WRInbox>(pool);
            workInboxCallables.forEach(item -> {
                completionPool.submit(item);
            });
            IntStream.range(0, workInboxCallables.size()).forEach(i -> {
                try {
                    Object result = completionPool.take().get();
                    if (result == null) {
                        return;
                    }
                    WRInbox workInbox = (WRInbox) result;
                    inBoxUserSids.add(workInbox.getReceive());
                } catch (Exception e) {
                    log.error("workInboxCallables", e);
                }
            });
        } catch (Exception e) {
            log.error("CompletionService", e);
        } finally {
            pool.shutdown();
        }
    }

    /**
     * 確認是否該工作報告已有轉寄過的使用者
     *
     * @param wrSid        工作報告Sid
     * @param receiveUser  欲轉寄人願
     * @param loginUserSid 登入者Sid
     * @return
     */
    public String checkDuplicateReceviceUser(String wrSid, List<UserViewVO> receiveUser, Integer loginUserSid) {
        if (receiveUser == null || receiveUser.isEmpty()) {
            Preconditions.checkState(false, "尚未選擇使用者，請確認！！");
        }
        List<WRInbox> workInbox = workInboxManager.getWorkInboxByWrSidAndForwardTypeAndLoginUserSid(wrSid,
                ForwardType.PERSON, loginUserSid);
        StringBuilder duplicateErrorMessage = new StringBuilder("");

        receiveUser.forEach(item -> {
            List<WRInbox> duplicateWorkInbox = workInbox.stream().filter(each -> each.getReceive().equals(item.getSid()) && each.getStatus().equals(Activation.ACTIVE))
                    .collect(Collectors.toList());
            if (duplicateWorkInbox != null && !duplicateWorkInbox.isEmpty()) {
                if (!Strings.isNullOrEmpty(duplicateErrorMessage.toString())) {
                    duplicateErrorMessage.append("<br>");
                }
                duplicateErrorMessage.append("[" + item.getName() + "]已寄發過，請問是否繼續？");
            }
        });
        return duplicateErrorMessage.toString();
    }

    /**
     * 取得該工作報告已轉寄部門物件
     *
     * @param wrSid        工作報告Sid
     * @param forwardType  轉寄類型
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<TransOrgViewVO> getWorkInboxByWrSidAndForwardTypeAndLoginUserSid(String wrSid,
                                                                                 ForwardType forwardType, Integer loginUserSid) {
        List<TransOrgViewVO> transOrgViewVOs = Lists.newArrayList();

        try {
            User loginUser = wRUserLogicComponents.findUserBySid(loginUserSid);
            Org loginUserDep = orgLogicComponents.findOrgBySid(loginUser.getPrimaryOrg().getSid());

            List<WRInbox> workInbox = workInboxManager.getWorkInboxByWrSidAndForwardTypeAndLoginUserSid(wrSid,
                    forwardType, loginUserSid);

            workInbox.forEach(item -> {
                User createUser = wRUserLogicComponents.findUserBySid(item.getCreate_usr());
                Org createUserDep = orgLogicComponents.findOrgBySid(createUser.getPrimaryOrg().getSid());
                String time = (item.getStatus().equals(Activation.ACTIVE)) ? ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getCreate_dt()) : ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdate_dt());
                String statusStr = (item.getStatus().equals(Activation.ACTIVE)) ? "" : "(回收)";
                if (ForwardType.DEP.equals(item.getForward_type())) {
                    Org transToDep = orgLogicComponents.findOrgBySid(item.getReceive_dep());
                    TransOrgViewVO transOrgViewVO = new TransOrgViewVO(item.getSid(), createUserDep.getSid(), createUserDep.getOrgNameWithParentIfDuplicate(),
                            createUser.getSid(), createUser.getName(), transToDep.getSid(), transToDep.getOrgNameWithParentIfDuplicate(), null, "", time, statusStr, item.getRead_record());
                    transOrgViewVOs.add(transOrgViewVO);
                } else if (ForwardType.PERSON.equals(item.getForward_type())) {
                    //特殊若是收指示,僅接收者及發送者或者發送者的主管單位才可看到
                    if (item.getInbox_type().equals(InboxType.INCOME_INSTRUCTION)) {
                        if (!loginUserSid.equals(item.getReceive()) && !loginUserSid.equals(item.getSender())) {
                            Org sendDep = orgLogicComponents.findOrgBySid(item.getSender_dep());
                            List<Org> sendParentDeps = orgLogicComponents.getAllParentOrgs(sendDep);
                            if (!sendParentDeps.contains(loginUserDep)) {
                                return;
                            }
                        }
                    }
                    User transToUser = wRUserLogicComponents.findUserBySid(item.getReceive());
                    Org transToDep = orgLogicComponents.findOrgBySid(item.getReceive_dep());
                    TransOrgViewVO transOrgViewVO = new TransOrgViewVO(item.getSid(), createUserDep.getSid(), createUserDep.getName(),
                            createUser.getSid(), createUser.getName(), transToDep.getSid(), transToDep.getName(), transToUser.getSid(), transToUser.getName(), time, statusStr, item.getRead_record());
                    transOrgViewVOs.add(transOrgViewVO);
                }
            });
        } catch (Exception e) {
            log.error("getWorkInboxByWrSidAndForwardTypeAndLoginUserSid", e);
        }
        return transOrgViewVOs;
    }

    //搜尋發送給loginUserSid使用者的轉發,ForwardType = ForwardType.PERSON
    public List<InBoxItemVO> getWorkInboxByReceiveUserSidForPerson(String wrSid, Integer loginUserSid) {
        User receiveUser = wRUserLogicComponents.findUserBySid(loginUserSid);
        List<InBoxItemVO> inBoxItemVOs = Lists.newArrayList();
        try {
            workInboxManager.getWorkInboxByWrSidAndForwardTypeAndReceiveSid(wrSid, ForwardType.PERSON, loginUserSid).forEach(item -> {
                User sendUser = wRUserLogicComponents.findUserBySid(item.getSender());
                InBoxItemVO inBoxItemVO = new InBoxItemVO(sendUser.getName(), ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                        item.getCreate_dt()), receiveUser.getName(), item.getMessage_css(), item.getInbox_type());
                inBoxItemVOs.add(inBoxItemVO);
            });

        } catch (Exception e) {
            log.error("getWorkInboxByReceviceUserSidForPerson", e);
        }
        return inBoxItemVOs;
    }

    /**
     * 取得收件夾Group List By 工作報告Sid 及 登入者Sid
     *
     * @param wrSid        工作報告Sid
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<InBoxGroupItemVO> getInBoxGroupItemBySendUserSid(String wrSid, Integer loginUserSid) {
        User sendUser = wRUserLogicComponents.findUserBySid(loginUserSid);
        List<WRInboxSendGroup> wRInboxSendGroups = wRInboxSendGroupManager.getWRInboxSendGroupByWrSidAndLoginUserSid(wrSid, loginUserSid);
        List<InBoxGroupItemVO> inBoxGroupItemVOs = Lists.newArrayList();
        wRInboxSendGroups.forEach(item -> {
            InBoxGroupItemVO inboxSendGroup = new InBoxGroupItemVO(item.getSid(), item.getMessage_css(), (ForwardType.DEP.equals(item.getForward_type())), item.getSend_to(), sendUser.getName(), ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                    item.getCreate_dt()));
            inBoxGroupItemVOs.add(inboxSendGroup);
        });
        return inBoxGroupItemVOs;
    }

    /**
     * 取得轉寄狀態List By 轉寄GroupSid
     *
     * @param inbox_group_sid
     * @return
     */
    public List<SentBackReadStatusVO> getSentBackReadStatusVOByInboxGroupSid(String inbox_group_sid) {
        List<SentBackReadStatusVO> sentBackReadStatusVOs = Lists.newArrayList();
        try {
            List<WRInbox> workInboxs = workInboxManager.getWorkInboxByInboxGroupSid(inbox_group_sid);
            workInboxs.forEach(item -> {
                boolean canCancel = false;
                if (item.getRead_record().equals(ReadRecordStatus.UNREAD) && item.getStatus().equals(Activation.ACTIVE)) {
                    canCancel = true;
                }
                SentBackReadStatusVO sv = new SentBackReadStatusVO(
                        item.getSid(),
                        ((item.getReceive_dep() != null) ? orgLogicComponents.findOrgBySid(item.getReceive_dep()).getName() : ""),
                        ((item.getReceive() != null) ? wRUserLogicComponents.findUserBySid(item.getReceive()).getName() : ""),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getRead_dt()),
                        //若狀態為失效,代表最後更新時間變為回收時間
                        (item.getStatus().equals(Activation.INACTIVE)) ? ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdate_dt()) : "",
                        (item.getForward_type().equals(ForwardType.DEP)),
                        canCancel,
                        item.getReceive(),
                        item.getReceive_dep()
                );
                sentBackReadStatusVOs.add(sv);
            });
        } catch (Exception e) {
            log.error("getSentBackReadStatusVOByInboxGroupSid", e);
        }
        return sentBackReadStatusVOs;
    }

    /**
     * 將轉寄記錄設為已讀
     *
     * @param wr_Sid          工作報告Sid
     * @param loginUserSid    登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     */
    public void readWorkInBox(String wr_Sid, Integer loginUserSid, Integer loginUserDepSid) {
        try {
            workInboxManager.readWorkInbox(wr_Sid, loginUserSid, loginUserDepSid);
        } catch (Exception e) {
            log.error("readWorkInBox", e);
        }
    }

    /**
     * 回收傳寄資訊
     *
     * @param sids         轉記紀錄Sids
     * @param loginUserSid 登入者Sid
     */
    public void doCancelWorkInBox(List<String> sids, Integer loginUserSid) {
        if (sids == null || sids.isEmpty()) {
            Preconditions.checkState(false, "尚未選擇回收項目，請確認！！");
        }
        List<WRInbox> workInboxs = workInboxManager.getWorkInboxBySids(sids);
        List<WRInbox> readedWorkInBoxs = workInboxs.stream().filter(each -> each.getRead_record().equals(ReadRecordStatus.READ))
                .collect(Collectors.toList());
        if (readedWorkInBoxs != null && !readedWorkInBoxs.isEmpty()) {
            StringBuilder moveErrorMessage = new StringBuilder();
            readedWorkInBoxs.forEach(item -> {
                if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
                    moveErrorMessage.append("<br>");
                }
                if (item.getForward_type().equals(ForwardType.DEP)) {
                    Org canRemoveOrg = orgLogicComponents.findOrgBySid(item.getReceive_dep());
                    moveErrorMessage.append("[" + canRemoveOrg.getName() + "]" + "已有人閱讀,不可回收");
                } else {
                    User canRemoveUser = wRUserLogicComponents.findUserBySid(item.getReceive());
                    moveErrorMessage.append("[" + canRemoveUser.getName() + "]" + "已閱讀,不可回收");
                }
            });
            Preconditions.checkState(false, moveErrorMessage.toString());
        }

        List<WRInbox> canceledWorkInBox = workInboxs.stream().filter(each -> each.getStatus().equals(Activation.INACTIVE))
                .collect(Collectors.toList());
        if (canceledWorkInBox != null && !canceledWorkInBox.isEmpty()) {
            StringBuilder cancelErrorMessage = new StringBuilder();
            canceledWorkInBox.forEach(item -> {
                if (!Strings.isNullOrEmpty(cancelErrorMessage.toString())) {
                    cancelErrorMessage.append("<br>");
                }
                if (item.getForward_type().equals(ForwardType.DEP)) {
                    Org canRemoveOrg = orgLogicComponents.findOrgBySid(item.getReceive_dep());
                    cancelErrorMessage.append("[" + canRemoveOrg.getName() + "]" + "已回收,不可再次回收");
                } else {
                    User canRemoveUser = wRUserLogicComponents.findUserBySid(item.getReceive());
                    cancelErrorMessage.append("[" + canRemoveUser.getName() + "]" + "已回收,不可再次回收");
                }
            });
            Preconditions.checkState(false, cancelErrorMessage.toString());
        }

        workInboxs.forEach(item -> {
            item.setStatus(Activation.INACTIVE);
            workInboxManager.updateWorkInbox(item, loginUserSid);
        });

        String groupID = workInboxs.get(0).getInbox_group_sid();
        WRInboxSendGroup wRInboxSendGroup = wRInboxSendGroupManager.getWRInboxSendGroupBySid(groupID);
        List<WRInbox> workInboxsUpdateOver = workInboxManager.getWorkInboxByInboxGroupSid(groupID);
        List<WRInbox> selWorkInboxUpdateOver = workInboxsUpdateOver.stream().filter(each -> each.getStatus().equals(Activation.ACTIVE))
                .collect(Collectors.toList());

        StringBuilder sendTo = new StringBuilder("");
        selWorkInboxUpdateOver.forEach(item -> {
            if (!Strings.isNullOrEmpty(sendTo.toString())) {
                sendTo.append(",");
            }
            if (item.getForward_type().equals(ForwardType.DEP)) {
                Org dep = orgLogicComponents.findOrgBySid(item.getReceive_dep());
                sendTo.append(dep.getName());
            } else {
                User user = wRUserLogicComponents.findUserBySid(item.getReceive());
                sendTo.append(user.getName());
            }
        });
        wRInboxSendGroup.setSend_to(sendTo.toString());
        wRInboxSendGroupManager.updateWRInboxSendGroup(wRInboxSendGroup, loginUserSid);
    }

}
