/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao 部門介面物件
 */
public class OrgViewVo implements Serializable {


    private static final long serialVersionUID = -2447823557158957101L;
    /** 部門SID */
    @Getter
    private final Integer sid;
    /** 部門Name */
    @Getter
    private final String name;
    /** 公司Name */
    @Getter
    private final String companyName;
    @Getter
    @Setter
    private boolean canMove = true;
    @Getter
    @Setter
    private boolean modifyed = false;

    public OrgViewVo() {
        this.sid = null;
        this.name = "";
        this.companyName = "";
    }

    public OrgViewVo(Integer sid) {
        this.sid = sid;
        this.name = "";
        this.companyName = "";
    }

    public OrgViewVo(Integer sid, String name) {
        this.sid = sid;
        this.name = name;
        this.companyName = "";
    }

    public OrgViewVo(Integer sid, String name, String companyName) {
        this.sid = sid;
        this.name = name;
        this.companyName = companyName;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrgViewVo other = (OrgViewVo) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }
}
