/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class WRHomepageFavoriteVO implements Serializable {

    
    private static final long serialVersionUID = -6858701413372223439L;
    public WRHomepageFavoriteVO(String pagePath, String pageName) {
        this.pagePath = pagePath;
        this.pageName = pageName;
    }
    
    @Getter
    private String pagePath;
    @Getter
    private String pageName;

}
