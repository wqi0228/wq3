/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.work.backend.web.logic.cache.WRHomepageFavoriteCache;
import com.cy.work.backend.web.logic.manager.WRHomepageFavoriteManager;
import com.cy.work.backend.web.view.vo.WRHomepageFavoriteVO;
import com.cy.work.backend.web.vo.WRHomepageFavorite;
import com.cy.work.backend.web.vo.converter.to.ReportPage;
import com.cy.work.backend.web.vo.converter.to.ReportPageTo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 報表快選區邏輯元件
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRHomepageFavoriteLogicComponents implements InitializingBean, Serializable {


    private static final long serialVersionUID = 1296711661918801698L;
    private static WRHomepageFavoriteLogicComponents instance;

    @Autowired
    private WRHomepageFavoriteCache wRHomepageFavoriteCache;
    @Autowired
    private WRHomepageFavoriteManager wRHomepageFavoriteManager;

    public static WRHomepageFavoriteLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRHomepageFavoriteLogicComponents.instance = this;
    }

    /**
     * 儲存報表快選區設定
     * @param wRHomepageFavoriteVOs 挑選紀錄報表快選區List
     * @param userSid  登入者Sid
     */
    public void saveWRHomepageFavorite(List<WRHomepageFavoriteVO> wRHomepageFavoriteVOs, Integer userSid) {
        WRHomepageFavorite wRHomepageFavorite = wRHomepageFavoriteCache.findByUserSid(userSid);
        if (wRHomepageFavorite == null) {
            wRHomepageFavorite = new WRHomepageFavorite();
        }
        ReportPage reportPage = new ReportPage();
        List<ReportPageTo> reportPageTos = Lists.newArrayList();
        wRHomepageFavoriteVOs.forEach(item -> {
            ReportPageTo rpt = new ReportPageTo();
            rpt.setPageName(item.getPageName());
            rpt.setPagePath(item.getPagePath());
            reportPageTos.add(rpt);
        });
        reportPage.setReportPageTos(reportPageTos);
        wRHomepageFavorite.setReport_page(reportPage);
        wRHomepageFavorite = wRHomepageFavoriteManager.createOrUpdate(wRHomepageFavorite, userSid);
        wRHomepageFavoriteCache.updateCacheByUserSid(wRHomepageFavorite);
    }

    /**
     * 取得該登入者挑選的報表快選區
     * @param userSid 登入者Sid
     * @return 
     */
    public List<WRHomepageFavoriteVO> getWRHomepageFavoriteVOByUserSid(Integer userSid) {
        List<WRHomepageFavoriteVO> wRHomepageFavoriteVOs = Lists.newArrayList();
        try {
            WRHomepageFavorite wRHomepageFavorite = wRHomepageFavoriteCache.findByUserSid(userSid);
            if (wRHomepageFavorite != null && wRHomepageFavorite.getReport_page() != null
                    && wRHomepageFavorite.getReport_page().getReportPageTos() != null) {
                wRHomepageFavorite.getReport_page().getReportPageTos().forEach(item -> {
                    WRHomepageFavoriteVO wRHomepageFavoriteVO = new WRHomepageFavoriteVO(item.getPagePath(), item.getPageName());
                    wRHomepageFavoriteVOs.add(wRHomepageFavoriteVO);
                });
            }
        } catch (Exception e) {
            log.error("getWRHomepageFavoriteVOByUserSid", e);
        }
        return wRHomepageFavoriteVOs;
    }
}
