/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class WRTraceVO implements Serializable {


    private static final long serialVersionUID = -3506298715122009482L;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRTraceVO other = (WRTraceVO) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }

    @Getter
    private final String traceType;

    @Getter
    private final String userInfo;

    @Getter
    private final String time;

    @Getter
    @Setter
    private String content;
    @Getter
    @Setter
    private String sid;
    @Getter
    private final boolean showEditBtn;

    public WRTraceVO(String sid, String traceType, String userInfo, String time, String content,boolean showEditBtn) {
        this.sid = sid;
        this.traceType = traceType;
        this.userInfo = userInfo;
        this.time = time;
        this.content = content;
        this.showEditBtn = showEditBtn;
    }
}
