/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.listener;

/**
 * 父介面監聽子介面動作Interface
 *
 * @author brain0925_liao
 */
public interface TableUpDownListener {

    /** 監聽子介面點選上筆 */
    public void openerByBtnUp();

    /** 監聽子介面點選下筆 */
    public void openerByBtnDown();

    /** 監聽子介面點選刪除 */
    public void openerByDelete();

    /** 監聽子介面點選作廢 */
    public void openerByInvalid();

    /** 監聽子介面點選提交 */
    public void openerByCommit();

    /** 監聽子介面編輯資訊 */
    public void openerByEditContent();

    /** 監聽子介面修改標籤 */
    public void openerByEditTag();

    /** 監聽子介面執行追蹤 */
    public void openerByTrace();

    /** 監聽子介面執行收藏變更 */
    public void openerByFavorite();

    /** 監聽子介面執行轉寄 */
    public void openerByTrans();
    
    public void openerByReadRecept();

}
