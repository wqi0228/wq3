/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.listener;

import java.io.Serializable;

/**
 * 轉寄更新CallBack(轉寄功能使用)
 * @author brain0925_liao
 */
public class TransUpdateCallBack implements Serializable {


    private static final long serialVersionUID = 3206816468237598614L;

    public void doUpdateData() {
    }
}
