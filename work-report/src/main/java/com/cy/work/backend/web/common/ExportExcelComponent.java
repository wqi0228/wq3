/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Component;

/**
 * 匯出套件
 *
 * @author brain0925_liao
 */
@Component
public class ExportExcelComponent implements Serializable {


    private static final long serialVersionUID = -1183061699057117569L;

    /**
     * 匯出excel
     *
     * @param document 匯出報表物件
     * @param start 搜尋起始日期
     * @param end 搜尋結束日期
     * @param type 報表類型字串
     */
    public void exportExcel(Object document, Date start, Date end, String type, Boolean showCreateDate) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFCellStyle cellStyle = borderStyle(wb);
        int row = 5;
        int rowIndex = 0;
        if (!showCreateDate) {
            row = 4;
        }
        String[] strs = new String[row];
        strs[rowIndex++] = "報表產生日：" + new LocalDate().toString("yyyy/MM/dd");
        if (showCreateDate && start != null && end != null) {
            strs[rowIndex++] = "立單日期：" + new LocalDate(start).toString("yyyy/MM/dd") + "~"
                    + new LocalDate(end).toString("yyyy/MM/dd");
        }
        strs[rowIndex++] = "報表類型：" + type;

        for (int i = 0; i < row; i++) {
            int totalRows = sheet.getLastRowNum();
            sheet.shiftRows(i, totalRows, 1);
            HSSFRow headerRow = sheet.getRow(i);
            if (headerRow == null) {
                headerRow = sheet.createRow(i);
            }
            HSSFCell cell = headerRow.getCell(0);
            if (cell == null) {
                cell = headerRow.createCell(0);
            }
            cell.setCellValue(strs[i]);
        }

        Iterator<Row> rowIterator = sheet.iterator();
        int nowRow = 0;
        Row rowI;
        while (rowIterator.hasNext()) {
            rowI = rowIterator.next();
            if (nowRow < row) {
                nowRow++;
                continue;
            }
            nowRow++;
            Iterator<Cell> cellIterator = rowI.cellIterator();
            Cell cell;
            while (cellIterator.hasNext()) {
                cell = cellIterator.next();
                cell.setCellStyle(cellStyle);
            }
        }

    }

    /**
     * 建立邊線格式
     *
     * @param wb
     * @return
     */
    private HSSFCellStyle borderStyle(HSSFWorkbook wb) {
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        return cellStyle;
    }

}
