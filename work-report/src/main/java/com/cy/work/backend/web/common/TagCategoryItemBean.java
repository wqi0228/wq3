/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.cy.work.backend.web.common.setting.TagCategoryItemSetting;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 可使用的模組Item Bean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@ManagedBean
public class TagCategoryItemBean implements Serializable {


    private static final long serialVersionUID = 8182083603521203148L;

    /** 取得狀態SelectItems */
    public List<SelectItem> getTagCategoryItems() {
        return TagCategoryItemSetting.getTagCategoryItems();
    }
}
