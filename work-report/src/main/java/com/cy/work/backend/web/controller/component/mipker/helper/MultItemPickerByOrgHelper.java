package com.cy.work.backend.web.controller.component.mipker.helper;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
@NoArgsConstructor
@Component
public class MultItemPickerByOrgHelper implements InitializingBean, Serializable {

	
    private static final long serialVersionUID = 3640088175941593856L;
    private static MultItemPickerByOrgHelper instance;

	public static MultItemPickerByOrgHelper getInstance() {
		return instance;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		MultItemPickerByOrgHelper.instance = this;
	}

	@Autowired
	transient private WkOrgCache wkOrgCache;
	
	/**
	 * @return
	 * @throws Exception
	 */
	public List<WkItem> prepareAllOrgItems() {
		// ====================================
		// 取得所有單位
		// ====================================
		// 取得登入者公司別
		Org comp = this.wkOrgCache.findById(SecurityFacade.getCompanyId());
		if (comp == null || comp.getSid() == null) {
			return Lists.newArrayList();
		}

		// 取得公司下所有單位
		Set<Integer> allDepSid = WkOrgCache.getInstance().findAllChildSids(comp.getSid());
		if (WkStringUtils.isEmpty(allDepSid)) {
			return Lists.newArrayList();
		}

		// ====================================
		// 轉為 WkItem, 並兜組父子關係
		// ====================================
		return this.prepareWkItemByDepSids(Sets.newHashSet(allDepSid));

	}

	/**
	 * 依據傳入部門, 轉為 WkItem, 並兜組父子關係
	 * 
	 * @param depSidSet
	 * @return
	 */
	public List<WkItem> prepareWkItemByDepSids(Set<Integer> depSidSet) {

		// 防呆
		if (WkStringUtils.isEmpty(depSidSet)) {
			return Lists.newArrayList();
		}

		// 排序
		List<Integer> depSids = WkOrgUtils.sortDepSidByOrgTree(Lists.newArrayList(depSidSet));

		// ====================================
		// 轉換格式
		// ====================================
		List<WkItem> allItems = Lists.newArrayList();
		Map<Integer, WkItem> itemMapBySid = Maps.newHashMap();
		Map<Integer, Org> depMapBySid = Maps.newHashMap();
		Long index = Long.valueOf(0);
		
		
		for (Integer depSid : depSids) {

			// 取得公司資料
			Org dep = WkOrgCache.getInstance().findBySid(depSid);
			if (dep == null) {
				continue;
			}

			// 檢核項目需為部門 (非公司)
			if (!OrgType.DEPARTMENT.equals(dep.getType())) {
				continue;
			}

			// 建立項目物件
			WkItem item = createDepItem(dep);
			// 排序序號
			item.setItemSeq(index++);

			allItems.add(item);
			itemMapBySid.put(dep.getSid(), item);
			depMapBySid.put(dep.getSid(), dep);
		}

		for (WkItem item : allItems) {
			Org dep = depMapBySid.get(Integer.parseInt(item.getSid()));

			if (dep.getParent() == null) {
				continue;
			}

			WkItem parentItem = itemMapBySid.get(dep.getParent().getSid());
			if (parentItem == null) {
				continue;
			}

			item.setParent(parentItem);
		}

		return allItems;

	}

	/**
	 * @param depSids
	 * @return
	 * @throws Exception
	 */
	public List<WkItem> createDepItemByDepSids(List<Integer> depSids) {

		List<WkItem> selectedItems = Lists.newArrayList();

		// 防呆
		if (WkStringUtils.isEmpty(depSids)) {
			return selectedItems;
		}

		// 逐筆轉換
		for (Integer depSid : depSids) {
			WkItem item = createDepItemByDepSid(depSid);
			if (item == null) {
				continue;
			}
			selectedItems.add(item);
		}

		return selectedItems;
	}

	/**
	 * @param dep
	 * @return
	 * @throws Exception
	 */
	public WkItem createDepItemByDepSid(Integer depSid) {

		Org dep = WkOrgCache.getInstance().findBySid(depSid);
		if (dep == null) {
			return null;
		}

		return this.createDepItem(dep);
	}

	/**
	 * @param dep
	 * @return
	 * @throws Exception
	 */
	public WkItem createDepItem(Org dep) {

		// 建立項目物件
		WkItem item = new WkItem(
		        dep.getSid() + "",
		        WkOrgUtils.prepareBreadcrumbsByDepName(dep.getSid(), OrgLevel.MINISTERIAL, true, ">"), // 組部門名稱 (麵包屑 到部級)
		        Activation.ACTIVE.equals(dep.getStatus()));

		// 美化顯示名稱
		item.setShowName(WkOrgUtils.makeupDepName(item.getName(), ">", "font-weight:bold;"));

		// 設定樹狀列表顯示名稱
		String depName = WkOrgUtils.getOrgName(dep);
		item.setTreeName(depName);
		item.setShowTreeName(depName);
		
        //搜尋用名稱
        item.getItemNamesForSearch().add(dep.getName());

		return item;
	}

	/**
	 * 取得分派單位
	 * 
	 * @return
	 */
	public List<Integer> itemsToSids(List<WkItem> items) {

		List<Integer> deps = Lists.newArrayList();

		if (items == null) {
			return deps;
		}

		for (WkItem depItem : items) {
			try {
				deps.add(Integer.parseInt(depItem.getSid()));
			} catch (Exception e) {
				log.warn("解析失敗!", e);
			}
		}

		return deps;
	}

	/**
	 * @return
	 */
	public Comparator<WkItem> parpareComparator() {
		return Comparator.comparing(WkItem::getItemSeq);
	}
}
