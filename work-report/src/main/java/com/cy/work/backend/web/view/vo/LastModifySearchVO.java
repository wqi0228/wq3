/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class LastModifySearchVO implements Serializable {


    private static final long serialVersionUID = 6148739333633785979L;
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LastModifySearchVO other = (LastModifySearchVO) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }

    public LastModifySearchVO(String sid,
            String create_time, String department_name, String create_usr_name, String title,
            String wr_no, String last_modify_time, String memo, String memoCss,
            String status, String editType) {
        this.sid = sid;
        this.create_time = create_time;
        this.department_name = department_name;
        this.create_usr_name = create_usr_name;
        this.title = title;
        this.wr_no = wr_no;
        this.last_modify_time = last_modify_time;
        this.memo = memo;
        this.memoCss = memoCss;
        this.status = status;
        this.editType = editType;
    }

    public LastModifySearchVO(String sid) {
        this.sid = sid;
    }

    public void replaceValue(LastModifySearchVO update) {
        this.create_time = update.create_time;
        this.department_name = update.department_name;
        this.create_usr_name = update.create_usr_name;
        this.title = update.title;
        this.last_modify_time = update.last_modify_time;
        this.memo = update.memo;
        this.memoCss = update.memoCss;
        this.status = update.status;
        this.editType = update.editType;
    }
    @Getter
    private String sid;
    @Getter
    private String create_time;
    @Getter
    private String department_name;
    @Getter
    private String create_usr_name;
    @Getter
    private String title;
    @Getter
    private String wr_no;
    @Getter
    private String last_modify_time;
    @Getter
    private String memo;
    @Getter
    private String memoCss;
    @Getter
    private String status;
    @Getter
    private String editType;

}
