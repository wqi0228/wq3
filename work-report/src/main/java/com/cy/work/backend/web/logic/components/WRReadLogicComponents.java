/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.manager.WrReadRecordManager;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 已未讀邏輯元件
 *
 * @author brain0925_liao
 */

@Slf4j
@Service
public class WRReadLogicComponents implements InitializingBean, Serializable {

    private static final long serialVersionUID = -3246369991369136564L;
    private static WRReadLogicComponents instance;

    @Autowired
    private WrReadRecordManager wrReadRecordManager;
    @Autowired
    private WRUserLogicComponents wRUserLogicComponents;

    public static WRReadLogicComponents getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRReadLogicComponents.instance = this;
    }

    /**
     * 取得已閱讀使用者 By 工作報告Sid
     *
     * @param wr_sid 工作報告Sid
     * @return
     */
    public List<User> getReadUser(String wr_sid) {
        List<User> users = Lists.newArrayList();
        List<WrReadRecord> records = wrReadRecordManager.findReadRecordsByWrSid(wr_sid);
        if (records.size() > 0) {
            for (WrReadRecord readRecord : records) {
                if (readRecord.getReadStatus().equals(WRReadStatus.HASREAD.name())) {
                    try {
                        users.add(wRUserLogicComponents.findUserBySid(Integer.valueOf(readRecord.getCreateUser())));
                    } catch (Exception e) {
                        log.error("getReadUser", e);
                    }
                }
            }
        }
        return users;
    }

    /**
     * 將該使用者在該工作報告更新成已閱讀
     *
     * @param wr_sid       工作報告Sid
     * @param loginUserSid 登入者Sid
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void readWRMaster(String wr_sid, Integer loginUserSid) {
        try {
            List<WrReadRecord> readRecords = wrReadRecordManager.findReadRecordsByWrSid(wr_sid)
                    .stream().filter(r -> r.getCreateUser().equals(loginUserSid.toString())).collect(Collectors.toList());

            if (null != readRecords && readRecords.size() > 0) {
                for (WrReadRecord readRecord : readRecords) {
                    if (readRecord.getCreateUser().equals(loginUserSid.toString())) {
                        wrReadRecordManager.updateReadStatus(wr_sid, readRecord.getCreateUser(), WRReadStatus.HASREAD.name(), new Date());
                    }
                }
            } else {
                wrReadRecordManager.saveWrReadRecord(new WrReadRecord(WRReadStatus.HASREAD.name(), loginUserSid.toString(), new Date(), wr_sid));
            }
        } catch (Exception e) {
            log.error("readWRMaster Error", e);
        }
    }

}
