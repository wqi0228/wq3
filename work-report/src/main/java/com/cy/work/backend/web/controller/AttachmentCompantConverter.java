/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.controller;

import com.cy.work.backend.web.common.attachment.AttachmentComponent;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Strings;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
@FacesConverter(value = "attachmentCompantConverter")
public class AttachmentCompantConverter implements Converter {

    @Autowired
    private WkJsonUtils jsonUtils;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent component, String value) {
        if (Strings.isNullOrEmpty(value)) {
            return null;
        }
        try {
            AttachmentComponent attachmentComponent = jsonUtils.fromJson(value, AttachmentComponent.class);
            return attachmentComponent;
        } catch (Exception e) {
            log.error("AttachmentCompantConverter error ", e);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }
        try {
            String json = jsonUtils.toJson(value).replace("\r", "").replace("\n", "").replace(" ", "");
            return json;
        } catch (Exception e) {
            log.error("AttachmentCompantConverter error ", e);
        }
        return "";
    }

}
