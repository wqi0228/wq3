/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.listener;

import java.io.Serializable;

/**
 * 追蹤更新CallBack(追蹤功能使用)
 * @author brain0925_liao
 */
public class TraceUpdateCallBack implements Serializable{

    
    private static final long serialVersionUID = -1065058757483329378L;

    public void doUpdateData() {
    }
}
