/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.config;

import javax.annotation.PostConstruct;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@Data
@Component
public class WRProperties {

    @Value("${work-report.db.driverClassName}")
    private String className;
    @Value("${work-report.db.url}")
    private String dbUrl;
    @Value("${work-report.username}")
    private String dbUserName;
    @Value("${work-report.db.password}")
    private String dbPassword;
    @Value("${work-report.db.dialect.hibernate}")
    private String dialect;
    @Value("${work-report.db.hibernate.show_sql}")
    private boolean show_sql;
    @Value("${work-report.db.maximumPoolSize}")
    private int maximumPoolSize;
    @Value("${work-report.db.minimumIdle}")
    private int minimumIdle;
    @Value("${work-report.db.idleTimeout}")
    private int idleTimeout;
    @Value("${work-report.db.maxLifetime}")
    private int maxLifetime;
    @Value("${work-report.db.cachePrepStmts}")
    private String cachePrepStmts;
    @Value("${work-report.db.useServerPrepStmts}")
    private String useServerPrepStmts;
    @Value("${work-report.db.prepStmtCacheSize}")
    private int prepStmtCacheSize;
    @Value("${work-report.db.prepStmtCacheSqlLimit}")
    private int prepStmtCacheSqlLimit;

    @PostConstruct
    void init() {
        log.info("WRProperties startup , display default value ..");
        log.info("className = " + className);
        log.info("dbUrl = " + dbUrl);
//        log.info("dbUserName = " + dbUserName.substring(0, 2) + ".....");
//        log.info("dbPassword = " + dbPassword.substring(0, 2) + ".....");
        log.info("dialect = " + dialect);
        log.info("show_sql = " + show_sql);
        log.info("maximumPoolSize = " + maximumPoolSize);
        log.info("minimumIdle = " + minimumIdle);
        log.info("idleTimeout = " + idleTimeout);
        log.info("maxLifetime = " + maxLifetime);
        log.info("cachePrepStmts = " + cachePrepStmts);
        log.info("useServerPrepStmts = " + useServerPrepStmts);
        log.info("prepStmtCacheSize = " + prepStmtCacheSize);
        log.info("prepStmtCacheSqlLimit = " + prepStmtCacheSqlLimit);
    }
}
