/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.manager.WorkSettingOrgManager;
import com.cy.work.backend.web.logic.manager.WorkSettingUserManager;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 使用者邏輯元件
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRUserLogicComponents implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = -2266922354539965900L;
    private static WRUserLogicComponents instance;

    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WorkSettingOrgManager orgManager;
    @Autowired
    private OrgLogicComponents orgLogicComponents;

    public static WRUserLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRUserLogicComponents.instance = this;
    }

    /**
     * 取得User物件 By userSid
     * @param sid userSid
     * @return 
     */
    public User findUserBySid(Integer sid) {
        try {
            User u = workSettingUserManager.findBySID(sid);
            return u;
        } catch (Exception e) {
            log.error("findUserBySid error :", e);
        }
        return null;
    }

      /**
     * 取得User介面物件 By userSid
     * @param sid userSid
     * @return 
     */
    public UserViewVO findBySid(Integer sid) {
        try {
            User u = workSettingUserManager.findBySID(sid);
            UserViewVO uv = new UserViewVO(u.getSid(), u.getName());
            return uv;
        } catch (Exception e) {
            log.error("findBySid error :", e);
        }
        return new UserViewVO();
    }

    /**
     * 取得使用者 By 部門Sid
     * @param depSid 部門Sid
     * @return 
     */
    public List<User> findUserByDepSid(Integer depSid) {
        try {
            List<User> users = workSettingUserManager.findUserByDepSid(depSid);
            return users;
        } catch (Exception e) {
            log.error("findUserByDepSid error :", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 取得使用者介面物件 By 部門Sid
     * @param depSid 部門Sid
     * @return 
     */
    public List<UserViewVO> findByDepSid(Integer depSid) {
        try {
            List<User> users = workSettingUserManager.findUserByDepSid(depSid);
            List<UserViewVO> userViews = Lists.newArrayList();
            users.forEach(item -> {
                UserViewVO uv = new UserViewVO(item.getSid(), item.getName(), item.getPrimaryOrg().getOrgNameWithParentIfDuplicate());
                userViews.add(uv);
            });
            return userViews;
        } catch (Exception e) {
            log.error("findByDepSid error :", e);
        }
        return Lists.newArrayList();
    }


    // 取得多主管 UserViewVO
    public List<UserViewVO> getManagers(Integer orgSid){
        List<UserViewVO> userViews = Lists.newArrayList();
        Org org = orgManager.findBySid(orgSid);
        if(org.getManagers()!=null && org.getManagers().size()>0){
            org.getManagers().forEach(user -> {
                UserViewVO uv = new UserViewVO(user.getSid(), user.getName(), orgLogicComponents.showParentDep(orgManager.findBySid(orgSid)));
                userViews.add(uv);
            });
        }
        return userViews;
    }
}
