/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.attachment;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.vo.basic.BasicAttachmentService;
import com.cy.work.common.vo.basic.BasicAttachmentServlet;
import com.cy.work.backend.web.logic.components.WRAttachmentLogicComponents;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WRMasterLogicComponents;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.PreviousAndNextCallBack;
import com.cy.work.backend.web.view.components.WorkReportViewComponent;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.vo.WRAttachment;
import com.cy.work.backend.web.vo.WRMaster;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@WebServlet("/WPAttachServlet")
@Slf4j
public class WPAttachServlet extends HttpServlet implements BasicAttachmentServlet {

    
    private static final long serialVersionUID = -8173637133153556453L;

    //要覆寫
    public final static String SERVLET_NAME = WPAttachServlet.class.getSimpleName();
    
    private final String illegal_readPath = "/error/illegal_read.xhtml";

    @SuppressWarnings("rawtypes")
    @Override
    public BasicAttachmentService getAttachService(HttpServletRequest request) {
        return WRAttachmentLogicComponents.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (SecurityFacade.getUserSid() == null || SecurityFacade.getUserSid() <= 0) {
            response.sendRedirect(request.getContextPath() + illegal_readPath);
            log.error("無登入");
            return;
        }

        UserViewVO userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        OrgViewVo depViewVo = OrgLogicComponents.getInstance().findBySid(SecurityFacade.getPrimaryOrgSid());
        OrgViewVo compViewVo = OrgLogicComponents.getInstance().findById(SecurityFacade.getCompanyId());
        WorkReportViewComponent workReportViewComponent = new WorkReportViewComponent(userViewVO, depViewVo, compViewVo, messageCallBack, previousAndNextCallBack);
        String attachmentSid = (String) this.findAttachSid(request);
        WRAttachment attachment = WRAttachmentLogicComponents.getInstance().getWRAttachmentBySid(attachmentSid);
        WRMaster wr = WRMasterLogicComponents.getInstance().getWRMasterBySid(attachment.getWr_sid());
        boolean checkPermission = workReportViewComponent.checkViewWorkReportPermission(wr);
        if (!checkPermission) {
            response.sendRedirect(request.getContextPath() + illegal_readPath);
            log.error("無權限");
            return;
        }

        BasicAttachmentServlet.super.doGet(request, response, super.getServletContext());
    }

    @Override
    public Object findAttachSid(HttpServletRequest request) {
        return String.valueOf(this.getAttachmentSidParameter(request));
    }

    private PreviousAndNextCallBack previousAndNextCallBack = new PreviousAndNextCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 3386339989538319929L;

        @Override
        public void doPrevious() {
        }

        @Override
        public void doNext() {
        }
    };

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 6271258645761527981L;

        @Override
        public void showMessage(String m) {
        }
    };

}
