/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import com.cy.work.backend.web.logic.components.WRTransSearchWorkReportLogicComponents;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class WRTransSendVO implements Serializable {

    
    private static final long serialVersionUID = -776214219230843444L;
    public WRTransSendVO(String sid) {
        this.sid = sid;
    }

    public WRTransSendVO(String sid, String wr_id,
            boolean tofowardDep, boolean tofowardMember,
            boolean totrace, String no, String sendType,
            String receivceUser, String sendTime,
            String tag, String title,
            String workreport_modifyTime,
            String readed, String readStatusColor,
            String readStatusTextBold, String encryptParam,String urlParam) {
        this.sid = sid;
        this.wr_id = wr_id;
        this.tofowardDep = tofowardDep;
        this.tofowardMember = tofowardMember;
        this.totrace = totrace;
        this.no = no;
        this.sendType = sendType;
        this.receivceUser = receivceUser;
        this.sendTime = sendTime;
        this.tag = tag;
        this.title = title;
        this.workreport_modifyTime = workreport_modifyTime;
        this.readed = readed;
        this.readStatusColor = readStatusColor;
        this.readStatusTextBold = readStatusTextBold;
        this.encryptParam = encryptParam;
        this.urlParam = urlParam;
    }

    public void replaceValue(WRTransSendVO update) {
        this.tofowardDep = update.tofowardDep;
        this.tofowardMember = update.tofowardMember;
        this.totrace = update.totrace;
        this.no = update.no;
        this.sendType = update.sendType;
        this.receivceUser = update.receivceUser;
        this.sendTime = update.sendTime;
        this.tag = update.tag;
        this.title = update.title;
        this.workreport_modifyTime = update.workreport_modifyTime;
        this.readed = update.readed;
        this.readStatusColor = update.readStatusColor;
        this.readStatusTextBold = update.readStatusTextBold;
        this.encryptParam = update.encryptParam;
        this.urlParam = update.urlParam;
    }

    public void replaceReadStatus(WRReadStatus wRReadStatus) {
        this.readed = wRReadStatus.getVal();
        this.readStatusColor = WRTransSearchWorkReportLogicComponents.getInstance().transToColorCss(wRReadStatus);
        this.readStatusTextBold = WRTransSearchWorkReportLogicComponents.getInstance().transToBoldCss(wRReadStatus);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRTransSendVO other = (WRTransSendVO) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }

    @Getter
    private String sid;
    @Getter
    private String wr_id;
    @Getter
    private boolean tofowardDep;
    @Getter
    private boolean tofowardMember;
    @Getter
    private boolean totrace;
    @Getter
    private String no;
    @Getter
    private String sendType;
    @Getter
    private String receivceUser;
    @Getter
    private String sendTime;
    @Getter
    private String tag;
    @Getter
    private String title;
    @Getter
    private String workreport_modifyTime;
    @Getter
    private String readed;
    @Getter
    private String readStatusColor;
    @Getter
    private String readStatusTextBold;
    @Getter
    private String encryptParam;
    @Getter
    private String urlParam;
}
