/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class WorkReportTransSearchComponent implements Serializable {

    
    private static final long serialVersionUID = -5324290053497103052L;

    @Getter
    private WorkReportSearchHeaderComponent workReportSearchHeaderComponent;

    private DepTreeComponent depTreeComponent;

    private Integer companySid;
    @Getter
    private List<Org> selectAllOrgs;
    @Getter
    private List<Org> selectBaseOrgs;

    private Integer loginUserSid;

    private String selectOrgType = "";

    private final String OrgType_All = "All";

    private final String OrgType_Base = "Base";
    @Getter
    private boolean showSelectBaseOrg = false;
    @Getter
    private boolean showSelectTransType = false;
    @Getter
    private String personTitleName;
    @Getter
    private String selectAllOrgTitleName;

    public WorkReportTransSearchComponent(Integer companySid, Integer loginUserSid, boolean showSelectBaseOrg,
            String personTitleName, String selectAllOrgTitleName, boolean showSelectTransType) {
        this.loginUserSid = loginUserSid;
        this.companySid = companySid;
        this.showSelectBaseOrg = showSelectBaseOrg;
        this.personTitleName = personTitleName;
        this.selectAllOrgTitleName = selectAllOrgTitleName;
        this.showSelectTransType = showSelectTransType;
        this.depTreeComponent = new DepTreeComponent();
        this.workReportSearchHeaderComponent = new WorkReportSearchHeaderComponent();
        settingDefault();
        initOrgTree();
    }

    public void doSelectOrg() {
        try {
            if (selectOrgType.equals(OrgType_All)) {
                selectAllOrgs = depTreeComponent.getSelectOrg();
            } else {
                selectBaseOrgs = depTreeComponent.getSelectOrg();
            }
        } catch (Exception e) {
            log.error("doSelectOrg", e);
        }
        DisplayController.getInstance().hidePfWidgetVar("transDepUnitDlg");
    }

    public void loadAllDepTree() {
        this.selectOrgType = OrgType_All;
        Org group = new Org(companySid);
        //修改,故切換顯示停用部門時,需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        List<Org> allOrg = OrgLogicComponents.getInstance().findOrgsByCompanySid(group.getSid());
        depTreeComponent.init(allOrg, allOrg,
                group, selectAllOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
        DisplayController.getInstance().update("transDepDialogPanel");
        DisplayController.getInstance().showPfWidgetVar("transDepUnitDlg");

    }

    public void loadBaseDepTree() {
        this.selectOrgType = OrgType_Base;
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        //此功能僅轉寄部門使用,原本為基本單位,WORKRPT-64 已調整成向下單位
        Org nowOrg = OrgLogicComponents.getInstance().findOrgBySid(user.getPrimaryOrg().getSid());
        //Org baseOrg = OrgLogicComponents.getInstance().getBaseOrg(user.getPrimaryOrg().getSid());
        List<Org> canSelectOrgs = OrgLogicComponents.getInstance().getAllChildOrg(nowOrg);
        canSelectOrgs.add(nowOrg);
        List<Org> allOrgs = Lists.newArrayList();
        allOrgs.addAll(canSelectOrgs);
        canSelectOrgs.forEach(item -> {
            List<Org> allParents = OrgLogicComponents.getInstance().getAllParentOrgs(item);
            if (allParents == null || allParents.isEmpty()) {
                return;
            }
            allOrgs.addAll(allParents.stream()
                    .filter(each -> !allOrgs.contains(each))
                    .collect(Collectors.toList()));
        });
        try {
            Collections.sort(allOrgs, new Comparator<Org>() {
                @Override
                public int compare(Org o1, Org o2) {
                    return o1.getSid() - o2.getSid();
                }
            });
        } catch (Exception e) {
            log.error("sort Error", e);
        }

        //修改,故切換顯示停用部門時,需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        Org group = new Org(companySid);
        depTreeComponent.init(allOrgs, canSelectOrgs,
                group, selectBaseOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
        DisplayController.getInstance().update("transDepDialogPanel");
        DisplayController.getInstance().showPfWidgetVar("transDepUnitDlg");
    }

    public void settingDefault() {
        this.workReportSearchHeaderComponent.settingDefault();
        this.initOrgTree();
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() {
        return depTreeComponent.getMultipleDepTreeManager();
    }

    private void initOrgTree() {
        Org group = new Org(1);
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectAllOrgs = Lists.newArrayList();
        selectBaseOrgs = Lists.newArrayList();
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        //此功能僅轉寄部門使用,原本為基本單位,WORKRPT-64 已調整成向下單位
        //Org baseOrg = OrgLogicComponents.getInstance().getBaseOrg(user.getPrimaryOrg().getSid());
        Org nowOrg = OrgLogicComponents.getInstance().findOrgBySid(user.getPrimaryOrg().getSid());
        selectBaseOrgs = OrgLogicComponents.getInstance().getAllChildOrg(nowOrg);
        selectBaseOrgs.add(nowOrg);
        depTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                group, selectAllOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);

        DisplayController.getInstance().update("transDepDialogPanel");
    }

}
