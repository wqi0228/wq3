/**
 * 
 */
package com.cy.work.backend.web.controller.component.mipker.vo;

import java.io.Serializable;
import java.util.List;

import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
public class MultItemPickerVO implements Serializable {

    
    private static final long serialVersionUID = 845561509677332090L;

    /**
     * 所有項目
     */
    @Getter
    @Setter
    private List<WkItem> allItems = Lists.newArrayList();

    /**
     * 被選擇的項目
     */
    @Getter
    @Setter
    private List<WkItem> selectedItems = Lists.newArrayList();

    /**
     * 過濾後的項目
     */
    @Getter
    @Setter
    private List<WkItem> filteredItems;

    /**
     * 清空已選擇, 和過濾項目
     */
    public void initSelectedAndfiltered() {
        this.selectedItems = Lists.newArrayList();
        this.filteredItems = allItems;
    }

}
