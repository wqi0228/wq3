/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.listener.DataTableReLoadCallBack;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.TableUpDownListener;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRMasterSearchLastModifyLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WRReportCustomColumnLogicComponents;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.CustomColumnComponent;
import com.cy.work.backend.web.view.components.WorkReportSearchHeaderComponent;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.LastModifySearchVO;
import com.cy.work.backend.web.view.vo.WRLastModifySearchColumnVO;
import com.cy.work.backend.web.view.vo.WorkReportSidTo;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.WRLastModifySearchColumn;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import com.cy.work.common.utils.WkJsonUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Setting2Bean implements Serializable, TableUpDownListener {


    private static final long serialVersionUID = -526764598362372898L;
    @Getter
    private WorkReportSearchHeaderComponent workReportSearchHeaderComponent;
    @Autowired
    private WRMasterSearchLastModifyLogicComponents wRMasterSearchLastModifyLogicComponents;
    @Autowired
    private WRReportCustomColumnLogicComponents wrReportCustomColumnLogicComponents;
    /** view 顯示錯誤訊息 */
    @Getter
    private String errorMessage;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Setter
    @Getter
    private List<LastModifySearchVO> lastModifySearchVOs;

    private List<LastModifySearchVO> tempLastModifySearchVOs;

    private String tempSelSid;
    @Setter
    @Getter
    private LastModifySearchVO selLastModifySearchVO;
    @Setter
    @Getter
    private boolean showFrame = false;
    @Getter
    private String iframeUrl = "";
    @Getter
    private WRLastModifySearchColumnVO searchColumnVO;
    @Getter
    private CustomColumnComponent customColumn;
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Getter
    private final String dataTableID = "dataTableLastModify";
    @Getter
    private final String dataTableWv = "dataTableLastModifyWv";

    @Getter
    private boolean hasDisplay = true;

    private String selWr_ID;
    @Setter
    @Getter
    private Date lastModifyDate;
    @Setter
    @Getter
    private String memo;

    private List<Org> selectOrgs;

    private List<Org> allOrgs;

    /** 部門相關邏輯Component */
    private DepTreeComponent depTreeComponent;

    @PostConstruct
    public void init() {
        userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        depViewVo = OrgLogicComponents.getInstance().findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = OrgLogicComponents.getInstance().findById(SecurityFacade.getCompanyId());
        workReportSearchHeaderComponent = new WorkReportSearchHeaderComponent();
        searchColumnVO = wrReportCustomColumnLogicComponents.getWRLastModifySearchColumnSetting(userViewVO.getSid());
        customColumn = new CustomColumnComponent(WRReportCustomColumnUrlType.LAST_MODIFY_SEARCH, Arrays.asList(WRLastModifySearchColumn.values()), searchColumnVO.getPageCount(), userViewVO.getSid(), messageCallBack, dataTableReLoadCallBack);
        initOrgData();
        //doSearch();
    }

    public void doSearch() {
        try {
            lastModifySearchVOs = doSearch("");
            if (lastModifySearchVOs.size() > 0) {
                DisplayController.getInstance().execute("selectDataTablePage('" + dataTableWv + "',0);");
            }
        } catch (Exception e) {
            log.error("doSearch Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void clear() {
        workReportSearchHeaderComponent.settingDefault();
        intOrgTree();
        DisplayController.getInstance().update("depDialogPanel");
        lastModifySearchVOs = Lists.newArrayList();
        //doSearch();
    }

    private List<LastModifySearchVO> doSearch(String sid) {
        List<Integer> orgSids = Lists.newArrayList();
        if (selectOrgs != null && !selectOrgs.isEmpty()) {
            selectOrgs.forEach(item -> {
                orgSids.add(item.getSid());
            });
        }

        return wRMasterSearchLastModifyLogicComponents.getLastModifySearch(orgSids,
                workReportSearchHeaderComponent.getNo(),
                workReportSearchHeaderComponent.getPersonName(), sid);
    }

    private void initOrgData() {
        allOrgs = Lists.newArrayList();
        allOrgs.addAll(OrgLogicComponents.getInstance().findOrgsByCompanySid(compViewVo.getSid()));
        depTreeComponent = new DepTreeComponent();
        intOrgTree();
    }

    private void intOrgTree() {
        selectOrgs = Lists.newArrayList();
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        Org group = new Org(compViewVo.getSid());
        depTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    public void loadOrgs() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(item -> {
            tempOrg.add(item);
        });
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        Org group = new Org(compViewVo.getSid());
        depTreeComponent.init(tempOrg, tempOrg,
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);

    }

    public void doSelectOrg() {
        selectOrgs = depTreeComponent.getSelectOrg();
        DisplayController.getInstance().hidePfWidgetVar("deptUnitDlg");
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() {
        return depTreeComponent.getMultipleDepTreeManager();
    }

    public void openCustomColumn() {
        try {
            customColumn.loadData();
            DisplayController.getInstance().update("dlgDefineField_view");
            DisplayController.getInstance().showPfWidgetVar("dlgDefineField");
        } catch (Exception e) {
            log.error("openCustomColumn Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenUrl(String sid) {
        try {
            settintSelWorkReportDraftSearchVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("btnOpenUrl Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            settintSelWorkReportDraftSearchVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp_iframe.xhtml?wrcId=" + selLastModifySearchVO.getSid();
            tableUpDownBean.setWorp_path("worp_iframe");
        } catch (Exception e) {
            log.error("btnOpenFrame Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void settintSelWorkReportDraftSearchVO(String sid) {
        LastModifySearchVO sel = new LastModifySearchVO(sid);
        int index = lastModifySearchVOs.indexOf(sel);
        if (index > 0) {
            selLastModifySearchVO = lastModifySearchVOs.get(index);
        } else {
            selLastModifySearchVO = lastModifySearchVOs.get(0);
        }
    }

    public void settingPreviousAndNext() {
        int index = lastModifySearchVOs.indexOf(selLastModifySearchVO);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(lastModifySearchVOs.get(index - 1).getSid());
        }
        if (index == lastModifySearchVOs.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(lastModifySearchVOs.get(index + 1).getSid());
        }
    }

    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.error("closeIframe Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void toUp() {
        try {
            if (tempLastModifySearchVOs != null && tempLastModifySearchVOs.size() > 0 && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempLastModifySearchVOs.indexOf(new LastModifySearchVO(tempSelSid));
                if (index > 0) {
                    selLastModifySearchVO = tempLastModifySearchVOs.get(index - 1);
                } else {
                    selLastModifySearchVO = lastModifySearchVOs.get(0);
                }
                tempLastModifySearchVOs = null;
                tempSelSid = "";
            } else {
                int index = lastModifySearchVOs.indexOf(selLastModifySearchVO);
                if (index > 0) {
                    selLastModifySearchVO = lastModifySearchVOs.get(index - 1);
                } else {
                    selLastModifySearchVO = lastModifySearchVOs.get(0);
                }
            }
        } catch (Exception e) {
            log.error("toUp Error", e);
            tempSelSid = "";
            tempLastModifySearchVOs = null;
            if (lastModifySearchVOs.size() > 0) {
                selLastModifySearchVO = lastModifySearchVOs.get(0);
            }
        }
    }

    private void toDown() {
        try {
            if (tempLastModifySearchVOs != null && tempLastModifySearchVOs.size() > 0 && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempLastModifySearchVOs.indexOf(new LastModifySearchVO(tempSelSid));
                if (index >= 0) {
                    selLastModifySearchVO = tempLastModifySearchVOs.get(index + 1);
                } else {
                    selLastModifySearchVO = lastModifySearchVOs.get(lastModifySearchVOs.size() - 1);
                }
                tempLastModifySearchVOs = null;
                tempSelSid = "";
            } else {
                int index = lastModifySearchVOs.indexOf(selLastModifySearchVO);
                if (index >= 0) {
                    selLastModifySearchVO = lastModifySearchVOs.get(index + 1);
                } else {
                    selLastModifySearchVO = lastModifySearchVOs.get(lastModifySearchVOs.size() - 1);
                }
            }
        } catch (Exception e) {
            log.error("toDown Error", e);
            tempLastModifySearchVOs = null;
            tempSelSid = "";
            if (lastModifySearchVOs.size() > 0) {
                selLastModifySearchVO = lastModifySearchVOs.get(lastModifySearchVOs.size() - 1);
            }
        }
    }

    @Override
    public void openerByBtnUp() {
        try {
            //log.info("草稿查詢進行上一筆動作");
            if (lastModifySearchVOs.size() > 0) {
                toUp();
                if (selLastModifySearchVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selLastModifySearchVO.getSid());
                }
            }
            DisplayController.getInstance().update(dataTableID);
            //log.info("草稿查詢進行上一筆動作-結束");
        } catch (Exception e) {
            log.error("草稿查詢進行上一筆動作-失敗", e);
        } finally {

            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByBtnDown() {
        try {
            //log.info("草稿查詢進行下一筆動作");
            if (lastModifySearchVOs.size() > 0) {
                toDown();
                if (selLastModifySearchVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(selLastModifySearchVO.getSid());
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("草稿查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByDelete() {
        doReloadInfo();
    }

    @Override
    public void openerByInvalid() {
        //草稿查詢-並不會受到作廢影響
    }

    @Override
    public void openerByCommit() {
        doReloadInfo();
    }

    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        WkJsonUtils jsonUtils = SpringContextHolder.getBean(WkJsonUtils.class);
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.error("doReloadInfo", ex);
        }
    }

    private void updateInfoToDataTable(String sid) {
        try {
            List<LastModifySearchVO> result = doSearch(sid);
            if (result == null || result.isEmpty()) {
                tempLastModifySearchVOs = Lists.newArrayList();
                if (lastModifySearchVOs != null && lastModifySearchVOs.size() > 0) {
                    lastModifySearchVOs.forEach(item -> {
                        tempLastModifySearchVOs.add(item);
                    });
                    lastModifySearchVOs.remove(new LastModifySearchVO(sid));
                    tempSelSid = sid;
                }
            } else {
                LastModifySearchVO updateDeail = result.get(0);
                lastModifySearchVOs.forEach(item -> {
                    if (item.getSid().equals(updateDeail.getSid())) {
                        item.replaceValue(updateDeail);
                    }
                });

            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("reCheck Error", e);
        }
    }

    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    @Override
    public void openerByEditTag() {
    }

    @Override
    public void openerByTrace() {

    }

    @Override
    public void openerByFavorite() {
    }

    @Override
    public void openerByTrans() {
    }
    
    @Override
    public void openerByReadRecept() {

    }

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -7833519932546338825L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    };

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 4340019541220689402L;

        @Override
        public void reload() {
            searchColumnVO = wrReportCustomColumnLogicComponents.getWRLastModifySearchColumnSetting(userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };

    public void saveLastModifyTime() {
        try {
            if (lastModifyDate == null) {
                Preconditions.checkState(false, "請挑選最後可編輯時間！");
            }
            wRMasterSearchLastModifyLogicComponents.updateLastModifyTime(selWr_ID, lastModifyDate);
            updateInfoToDataTable(selWr_ID);
            DisplayController.getInstance().hidePfWidgetVar("dlglastModifyTimeWv");
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
            log.error("saveLastModifyTime", e);
        }
    }

    public void loadLastModifyTime(String wr_sid) {
        try {
            List<LastModifySearchVO> sel = lastModifySearchVOs.stream()
                    .filter(each -> each.getSid().equals(wr_sid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("主檔資料比對有誤,無法開啟");
                return;
            }
            selWr_ID = wr_sid;
            selLastModifySearchVO = sel.get(0);
            DisplayController.getInstance().update(dataTableID);
            lastModifyDate = ToolsDate.transStringToDate(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), selLastModifySearchVO.getLast_modify_time());
            DisplayController.getInstance().update("dlglastModifyTime_view");
            DisplayController.getInstance().showPfWidgetVar("dlglastModifyTimeWv");
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
            log.error("loadLastModifyTime", e);
        }
    }

    public void saveLastModifyTimeMemo() {
        try {
            wRMasterSearchLastModifyLogicComponents.updateLastModifyMemo(selWr_ID, memo);
            updateInfoToDataTable(selWr_ID);
            DisplayController.getInstance().hidePfWidgetVar("dlglastModifyTime_memoWv");
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
            log.error("saveLastModifyTimeMemo", e);
        }
    }

    public void loadLastModifyTimeMemo(String wr_sid) {
        try {
            List<LastModifySearchVO> sel = lastModifySearchVOs.stream()
                    .filter(each -> each.getSid().equals(wr_sid))
                    .collect(Collectors.toList());
            if (sel == null || sel.isEmpty()) {
                messageCallBack.showMessage("主檔資料比對有誤,無法開啟");
                return;
            }
            selWr_ID = wr_sid;
            selLastModifySearchVO = sel.get(0);
            DisplayController.getInstance().update(dataTableID);
            memo = selLastModifySearchVO.getMemoCss();
            DisplayController.getInstance().update("dlglastModifyTime_memo");
            DisplayController.getInstance().execute("initReinstatedDlg('dlglastModifyTime_memo','dlglastModifyTime_memo_view_wvMome_id',200,150);");
            DisplayController.getInstance().showPfWidgetVar("dlglastModifyTime_memoWv");
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
            log.error("loadLastModifyTime", e);
        }
    }

}
