package com.cy.work.backend.web.controller.component.mipker.helper;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.work.backend.web.controller.component.PickerComponentHelper;
import com.cy.work.backend.web.controller.component.mipker.vo.MultItemPickerShowMode;
import com.cy.work.backend.web.controller.component.mipker.vo.MultItemPickerTreeVO;
import com.cy.work.backend.web.controller.component.mipker.vo.MultItemPickerVO;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.NoArgsConstructor;

/**
 * @author allen1214_wu
 *
 */
@NoArgsConstructor
@Component
public class MultItemPickerComponentHelper implements InitializingBean, Serializable {


    private static final long serialVersionUID = 8080428483050538807L;
    private static MultItemPickerComponentHelper instance;

    public static MultItemPickerComponentHelper getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        MultItemPickerComponentHelper.instance = this;
    }


    @Autowired
    private PickerComponentHelper pickerHelper;;

    /**
     * 加入已選擇項目
     * 
     * @param srcVO
     * @param targetVO
     * @param srcTreeVO
     * @param containFollowing
     * @param childItemsMapByParentItem
     * @param items
     * @param itemComparator
     * @param srcShowMode
     */
    public void addSelectedItems(
            MultItemPickerVO srcVO,
            MultItemPickerVO targetVO,
            MultItemPickerTreeVO srcTreeVO,
            boolean containFollowing,
            Map<WkItem, List<WkItem>> childItemsMapByParentItem,
            List<WkItem> items,
            Comparator<WkItem> itemComparator,
            MultItemPickerShowMode srcShowMode) {

        List<WkItem> targetItems = items;

        // ====================================
        // 若有含以下時, 加入子項目
        // ====================================
        if (containFollowing) {
            targetItems = Lists.newArrayList(this.collectWithSubItem(items, childItemsMapByParentItem));
        }

        // ====================================
        // 清單列表移動
        // ====================================
        this.itemSwap(targetItems, srcVO, targetVO, itemComparator);

        // ====================================
        //
        // ====================================
        targetVO.initSelectedAndfiltered();

        // ====================================
        // 處理樹節點
        // ====================================
        if (!srcTreeVO.isHasTreeMode() || !MultItemPickerShowMode.TREE.equals(srcShowMode)) {
            return;
        }
        // 初始化所有節點
        for (TreeNode treeNode : srcTreeVO.getTreeNodeMapByItem().values()) {
            treeNode.setSelected(false);
            treeNode.setExpanded(false);
        }

        // 重建樹勾選狀態
        List<TreeNode> selectedItems = Lists.newArrayList();
        for (TreeNode treeNode : srcTreeVO.getTreeNodeMapByItem().values()) {
            if (treeNode.getData() == null) {
                return;
            }
            // 判斷是否勾選
            WkItem treeNodeItem = (WkItem) treeNode.getData();
            boolean isSelected = targetVO.getAllItems().contains(treeNodeItem);

            // 捨設定勾選狀態
            treeNode.setSelected(isSelected);
            // 有勾選時,
            if (isSelected) {
                // 展開上層
                this.pickerHelper.changeNodeExpand(treeNode, true);
                // 加入已選擇清單
                selectedItems.add(treeNode);
            }
        }

        // 更新已選擇項目
        TreeNode[] selectedNodeAry = new TreeNode[selectedItems.size()];
        if (WkStringUtils.notEmpty(selectedItems)) {
            selectedNodeAry = selectedItems.toArray(selectedNodeAry);
        }
        srcTreeVO.setSelectedNodes(selectedNodeAry);
    }

    /**
     * 移除已選擇項目
     * 
     * @param item
     */
    public void removeSelectedItems(
            MultItemPickerVO srcVO,
            MultItemPickerVO targetVO,
            MultItemPickerTreeVO srcTreeVO,
            List<WkItem> items,
            Comparator<WkItem> itemComparator,
            MultItemPickerShowMode srcShowMode) {
        // ====================================
        // 清單列表移動
        // ====================================
        this.itemSwap(items, targetVO, srcVO, itemComparator);

        // ====================================
        // 重建樹勾選狀態
        // ====================================
        if (!srcTreeVO.isHasTreeMode() || !MultItemPickerShowMode.TREE.equals(srcShowMode)) {
            return;
        }

        List<TreeNode> selectedItems = Lists.newArrayList();
        for (TreeNode treeNode : srcTreeVO.getTreeNodeMapByItem().values()) {
            if (treeNode.getData() == null) {
                return;
            }
            WkItem treeNodeItem = (WkItem) treeNode.getData();
            treeNode.setSelected(targetVO.getAllItems().contains(treeNodeItem));
            // 收集已選擇項目
            if (treeNode.isSelected()) {
                selectedItems.add(treeNode);
            }
        }

        // 更新已選擇項目
        TreeNode[] selectedNodeAry = new TreeNode[selectedItems.size()];
        if (WkStringUtils.notEmpty(selectedItems)) {
            selectedNodeAry = selectedItems.toArray(selectedNodeAry);
        }
        srcTreeVO.setSelectedNodes(selectedNodeAry);
    }

    /**
     * 項目交換
     * 
     * @param items
     * @param src
     * @param target
     */
    private void itemSwap(
            List<WkItem> items,
            MultItemPickerVO src,
            MultItemPickerVO target,
            Comparator<WkItem> itemComparator) {

        if (WkStringUtils.isEmpty(items)) {
            return;
        }

        // primefaces 給的不是標準可迭代物件 ,在此重新整理
        items = Lists.newArrayList(items);

        for (WkItem item : items) {

            // 為鎖定項目 - 忽略
            if (item.isDisable()) {
                continue;
            }

            // 從待選清單移除
            if (WkStringUtils.notEmpty(src.getAllItems())) {
                src.getAllItems().remove(item);
            }
            if (WkStringUtils.notEmpty(src.getSelectedItems())) {
                src.getSelectedItems().remove(item);
            }
            if (WkStringUtils.notEmpty(src.getFilteredItems())) {
                src.getFilteredItems().remove(item);
            }

            // 加入已選清單
            if (target.getAllItems() == null) {
                target.setAllItems(Lists.newArrayList());
            }
            if (!target.getAllItems().contains(item)) {
                target.getAllItems().add(item);
            }

        }

        // 排序
        if (itemComparator != null) {
            List<WkItem> targetItems = target.getAllItems().stream()
                    .sorted(itemComparator)
                    .collect(Collectors.toList());

            target.setAllItems(targetItems);
        }
    }

    /**
     * 取得傳入項目的所有直系子項目
     * 
     * @param items
     * @param childItemsMapByParentItem
     * @return
     */
    private Set<WkItem> collectWithSubItem(
            List<WkItem> items,
            Map<WkItem, List<WkItem>> childItemsMapByParentItem) {

        Set<WkItem> resultSet = Sets.newHashSet();

        if (WkStringUtils.isEmpty(items)) {
            return resultSet;
        }

        for (WkItem item : items) {
            // 加入自己
            resultSet.add(item);

            // 加入子項目
            resultSet.addAll(
                    collectWithSubItem(
                            childItemsMapByParentItem.get(item),
                            childItemsMapByParentItem));
        }

        return resultSet;
    }
}
