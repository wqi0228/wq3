/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao 工作報告搜尋欄位介面物件
 */
public class WRWorkReportSearchColumnVO implements Serializable {


    private static final long serialVersionUID = -8511056771686432707L;
    /** 序 */
    @Setter
    @Getter
    private WRColumnDetailVO index;
    /** 部 */
    @Setter
    @Getter
    private WRColumnDetailVO fowardDep;
    /** 個 */
    @Setter
    @Getter
    private WRColumnDetailVO fowardMember;
    /** 追 */
    @Setter
    @Getter
    private WRColumnDetailVO trace;
    /** 建立日期 */
    @Setter
    @Getter
    private WRColumnDetailVO createTime;
    /** 異動日期 */
    @Setter
    @Getter
    private WRColumnDetailVO modifyTime;
    /** 是否閱讀 */
    @Setter
    @Getter
    private WRColumnDetailVO readed;
    /** 標籤 */
    @Setter
    @Getter
    private WRColumnDetailVO tag;
    /** 部門 */
    @Setter
    @Getter
    private WRColumnDetailVO department;
    /** 建立人員 */
    @Setter
    @Getter
    private WRColumnDetailVO createUser;
    /** 主題 */
    @Setter
    @Getter
    private WRColumnDetailVO theme;
    /** 狀態 */
    @Setter
    @Getter
    private WRColumnDetailVO status;
    @Setter
    @Getter
    private WRColumnDetailVO readRecipt;
    @Setter
    @Getter
    private WRColumnDetailVO readRecipted;
    /** 單號 */
    @Setter
    @Getter
    private WRColumnDetailVO no;
    /** 編輯權限 */
    @Setter
    @Getter
    private WRColumnDetailVO editType;
    @Setter
    @Getter
    private String pageCount = "50";

}
