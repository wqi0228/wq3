/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.setting;

import com.cy.work.backend.web.vo.enums.WRStatus;
import com.google.common.collect.Lists;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * 工作報告單據狀態靜態資料
 *
 * @author brain0925_liao
 */
public class WRStatusSetting {

    /** 工作報告單據狀態選項List */
    private static List<SelectItem> wr_workSearchs;

    /** 取得工作報告單據狀態選項資料,若記憶體已有將不再行建立 */
    public static List<SelectItem> getWorkSearchItems() {
        if (wr_workSearchs != null) {
            return wr_workSearchs;
        }
        wr_workSearchs = Lists.newArrayList();
        SelectItem si1 = new SelectItem(WRStatus.COMMIT.name(), WRStatus.COMMIT.getVal());
        wr_workSearchs.add(si1);
        SelectItem si2 = new SelectItem(WRStatus.INVIALD.name(), WRStatus.INVIALD.getVal());
        wr_workSearchs.add(si2);
        return wr_workSearchs;
    }

}
