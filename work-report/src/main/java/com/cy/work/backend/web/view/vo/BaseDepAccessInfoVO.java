/**
 * 
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.testng.collections.Lists;

import com.cy.commons.vo.Org;
import com.cy.work.backend.web.vo.WrBaseDepAccessInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class BaseDepAccessInfoVO extends WrBaseDepAccessInfo implements Serializable {

    
    private static final long serialVersionUID = 5684859243970488695L;
    public BaseDepAccessInfoVO(WrBaseDepAccessInfo entity) {
        if (entity == null) {
            return;
        }
        BeanUtils.copyProperties(entity, this);
    }

    private Org targetDep;
    private Integer targetDepOrder = Integer.MAX_VALUE;
    private List<Integer> addDepSids = Lists.newArrayList();
    private List<Integer> limitDepSids = Lists.newArrayList();

    private String targetDepCompInfo;
    private String targetDepInfo;
    private String addInfo;
    private String limitInfo;
}
