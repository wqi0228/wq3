/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.util;

import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.google.common.collect.Lists;
import java.util.List;

/**
 *
 * @author brain0925_liao
 */
public class ToolsUtil {

    public static List<Org> getOrgChildren(List<Org> allOrgs, Org parentOrg) {
        List<Org> children = Lists.newArrayList();
        for (Org org : allOrgs) {
            if (org == null) {
                continue;
            }
            SimpleOrg parent = org.getParent();
            if (parent != null && parent.getSid() != null && parent.getSid().equals(parentOrg.getSid())) {
                children.add(org);
            }
        }
        return children;
    }

    public static boolean isCompany(Org org) {
        if (org != null) {
            return org.getType() == OrgType.BRANCH_OFFICE
                    || org.getType() == OrgType.HEAD_OFFICE;
        }
        return false;
    }

    public static String combineOrgName(List<OrgViewVo> list, String glue) {
        if (list == null) {
            return "";
        }
        int k = list.size();
        if (k == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        out.append(list.get(0).getName());
        for (int x = 1; x < k; ++x) {
            out.append(glue).append(list.get(x).getName());
        }
        return out.toString();
    }

}
