/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.WorkReportTabCallBack;
import com.cy.work.backend.web.logic.components.WRAttachmentLogicComponents;
import com.cy.work.backend.web.logic.utils.attachment.AttachmentUtils;
import com.cy.work.backend.web.logic.vo.AttachmentVO;
import com.cy.work.backend.web.view.enumtype.TabType;
import com.cy.work.common.vo.AttachmentService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class WorkReportAttachMaintainCompant implements Serializable {

    
    private static final long serialVersionUID = -2455231571983847413L;
    @Getter
    private List<AttachmentVO> attachmentVOs;
    @Getter
    private List<AttachmentVO> selectAttachmentVOs;

    private String wr_Sid;

    private Integer userSid;
    @Getter
    private AttachmentVO selDelAtt;
    @Getter
    private boolean showAttTab = false;

    private MessageCallBack messageCallBack;

    private WorkReportTabCallBack workReportTabCallBack;

    public WorkReportAttachMaintainCompant(Integer userSid, MessageCallBack messageCallBack, WorkReportTabCallBack workReportTabCallBack) {
        this.userSid = userSid;
        this.selDelAtt = new AttachmentVO("", "", "", "", false, "");
        this.messageCallBack = messageCallBack;
        this.workReportTabCallBack = workReportTabCallBack;
    }

    public void loadData(String wr_Sid) {
        this.wr_Sid = wr_Sid;

        selectAttachmentVOs = Lists.newArrayList();
        attachmentVOs = Lists.newArrayList();
        loadData();
    }

    public void changeToEditMode(String sid) {
        attachmentVOs.forEach(item -> {
            if (item.getAttSid().equals(sid)) {
                item.setEditMode(true);
            }
        });
    }

    public void loadData() {
        try {
            List<AttachmentVO> tempAttachmentVOs = WRAttachmentLogicComponents.getInstance().getAttachmentsByWRSid(wr_Sid);
            tempAttachmentVOs.forEach(item -> {
                if (!attachmentVOs.contains(item)) {
                    item.setChecked(false);
                    if (String.valueOf(userSid).equals(item.getCreateUserSId())) {
                        item.setShowEditBtn(true);
                    } else {
                        item.setShowEditBtn(false);
                    }
                    item.setEditMode(false);
                    attachmentVOs.add(item);
                }
            });
            List<AttachmentVO> tempReal = Lists.newArrayList();
            attachmentVOs.forEach(item -> {
                if (tempAttachmentVOs.contains(item)) {
                    tempReal.add(item);
                }
            });
            attachmentVOs.clear();
            attachmentVOs.addAll(tempReal);

            if (attachmentVOs.size() > 0) {
                showAttTab = true;
            } else {
                showAttTab = false;
            }
        } catch (Exception e) {
            log.error("loadData", e);
        }

    }

    public void loadDeleteAtt(String sid) {
        attachmentVOs.forEach(item -> {
            if (item.getAttSid().equals(sid)) {
                this.selDelAtt = item;
            }
        });
    }

    public void deleteAtt() {
        if (selDelAtt == null || Strings.isNullOrEmpty(selDelAtt.getAttSid())) {
            return;
        }
        List<AttachmentVO> tempAv = Lists.newArrayList(attachmentVOs);
        tempAv.forEach(item -> {
            if (item.getAttSid().equals(selDelAtt.getAttSid())) {
                try {
                    List<AttachmentVO> attachment = WRAttachmentLogicComponents.getInstance().getAttachmentsByWRSid(wr_Sid);
                    if (attachment.contains(selDelAtt)) {

                        WRAttachmentLogicComponents.getInstance().deleteAttachment(item, userSid, wr_Sid);
                        workReportTabCallBack.reloadTraceTab();
                        workReportTabCallBack.reloadAttTab();
                        if (showAttTab) {
                            workReportTabCallBack.reloadTabView(TabType.Attachment);
                        } else {
                            workReportTabCallBack.reloadTabView(TabType.Trace);
                        }
                    }
                    attachment = null;
                } catch (Exception e) {
                    messageCallBack.showMessage(e.getMessage());
                    log.error("deleteAtt Error :", e);
                }
            }
        });
    }

    public void saveAttDesc(String sid) {
        attachmentVOs.forEach(item -> {
            if (item.getAttSid().equals(sid)) {
                try {
                    WRAttachmentLogicComponents.getInstance().updateAttachment(sid, item.getAttDesc(), userSid);
                    item.setEditMode(false);
                } catch (Exception e) {
                    log.error("saveAttDesc Error :", e);
                }
            }
        });
    }

    public void selectAll() {
        selectAttachmentVOs.clear();
        attachmentVOs.forEach(item -> {
            selectAttachmentVOs.add(item);
            item.setChecked(true);
        });
    }

    public void changeCheckBox() {
        selectAttachmentVOs.clear();
        attachmentVOs.forEach(item -> {
            if (item.isChecked()) {
                selectAttachmentVOs.add(item);
            }
        });
    }

    public StreamedContent downloadFile() throws IOException {
        if (selectAttachmentVOs.isEmpty()) {
            return null;
        }
        // 下載單一檔案，預設檔名使用該檔案的原始檔名（但是要經過編碼）
        if (selectAttachmentVOs.size() == 1) {
            return AttachmentUtils.createStreamedContent(selectAttachmentVOs.get(0), "wrMaster");
        }
        // 將多檔案壓縮成單一 zip 檔案再輸出
        return AttachmentUtils.createWrappedStreamedContent(selectAttachmentVOs, "wrMaster");
    }

    public String downloadFileContentDisposition() {
        if (selectAttachmentVOs.isEmpty()) {
            return null;
        }
        String encodedFileName = (selectAttachmentVOs.size() == 1)
                ? AttachmentUtils.encodeFileName(selectAttachmentVOs.get(0).getAttName())
                : AttachmentService.MULTIFILES_ZIP_FILENAME;
        String contentDispositionPattern = AttachmentUtils.isMSIE(Faces.getRequest())
                ? "{0}; filename=\"{1}\""
                : "{0}; filename=\"{1}\"; filename*=UTF-8''''\"{1}\"";
        return MessageFormat.format(contentDispositionPattern, "attachment", encodedFileName);
    }

}
