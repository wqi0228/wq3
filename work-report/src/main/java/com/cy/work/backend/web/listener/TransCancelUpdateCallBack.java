/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.listener;

import java.io.Serializable;

/**
 * 追蹤取消更新CallBack(追蹤功能使用)
 * @author brain0925_liao
 */
public class TransCancelUpdateCallBack implements Serializable {

    
    private static final long serialVersionUID = -6373895738894154299L;

    public void doUpdateData() {
    }
}
