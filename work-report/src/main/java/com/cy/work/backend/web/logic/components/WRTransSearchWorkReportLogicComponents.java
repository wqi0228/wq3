/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.common.setting.WorkProjectTagItemSetting;
import com.cy.work.backend.web.logic.manager.WRInboxSendGroupManager;
import com.cy.work.backend.web.logic.manager.WRInboxManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.logic.vo.WRTransIncomeWorkReportVO;
import com.cy.work.backend.web.logic.vo.WRTransReceviceDepWorkReportVO;
import com.cy.work.backend.web.logic.vo.WRTransSendWorkReportVO;
import com.cy.work.backend.web.view.enumtype.TabType;
import com.cy.work.backend.web.view.vo.WRTransIncomeVO;
import com.cy.work.backend.web.view.vo.WRTransReceviceDepVO;
import com.cy.work.backend.web.view.vo.WRTransSendVO;
import com.cy.work.backend.web.vo.enums.InboxType;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 轉寄查詢邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRTransSearchWorkReportLogicComponents implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = -6779185927663835742L;
    private static WRTransSearchWorkReportLogicComponents instance;

    @Autowired
    private WRInboxManager wRInboxManager;
    @Autowired
    private WRInboxSendGroupManager wRInboxSendGroupManager;
    @Autowired
    private OrgLogicComponents orgLogicComponents;
    @Autowired
    private WRUserLogicComponents wRUserLogicComponents;
    @Autowired
    private EncryptLogicComponents encryptLogicComponents;
    /** 轉寄-寄件備份URL 字串 */
    private final String sendInBoxType = "[urlType:sendType]";
    /** 轉寄-轉個人URL 字串 */
    private final String memberInBoxType = "[urlType:memberType]";
    /** 轉寄-收指示URL 字串 */
    private final String InstructionInBoxType = "[urlType:instructionType]";
    /** 轉寄-收呈報URL 字串 */
    private final String reportInBoxType = "[urlType:reportType]";

    /**
     * 根據傳入加密字串判斷是否從收件夾進入,若為收件夾進入,會根據字串自動預設顯示特定頁簽
     *
     * @param encryptParam 加密字串
     * @param loginUserSid 登入者Sid
     * @return TabType
     */
    public TabType getDefaultTabType(String encryptParam, Integer loginUserSid) {
        String param = encryptLogicComponents.decrypt(encryptParam);
        if (param.contains(memberInBoxType)) {
            return TabType.IncomeMember;
        }
        if (param.contains(InstructionInBoxType)) {
            return TabType.IncomeInstruction;
        }
        if (param.contains(reportInBoxType)) {
            return TabType.IncomeReport;
        }
        //檢測該url是否為同一人,僅寄件備份有用
        if (!param.contains(getParamLoginUserSid(loginUserSid))) {
            return null;
        }
        if (param.contains(sendInBoxType)) {
            return TabType.Send;
        }
        return null;
    }

    /**
     * 取得使用者URL字串
     *
     * @param loginUserSid 登入者Sid
     * @return String
     */
    public String getParamLoginUserSid(Integer loginUserSid) {
        if (loginUserSid == null) {
            return "";
        }
        return "[userSid:" + loginUserSid + "]";
    }

    public static WRTransSearchWorkReportLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRTransSearchWorkReportLogicComponents.instance = this;
    }

    /**
     * 取得轉個人查詢
     *
     * @param tagID 介面挑選標籤ID
     * @param transReadStatus 介面挑選是否閱讀ID
     * @param sendOrgs 介面挑選寄發單位Sid List
     * @param sendName 介面輸入寄發者
     * @param title 主題
     * @param content 內容
     * @param searchText 模糊搜尋
     * @param startCreateTime 工作報告建立起始時間
     * @param endCreateTime 工作報告建立結束時間
     * @param loginUserSid 登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     * @param wr_Id 工作報告Sid
     * @param compSid 公司Sid
     * @return List
     */
    public List<WRTransIncomeVO> getWRTransIncomeMember(String tagID, String transReadStatus, List<Integer> sendOrgs,
            String sendName, String title, String content,
            String searchText, Date startCreateTime, Date endCreateTime, Integer loginUserSid,
            Integer loginUserDepSid, String wr_Id, Integer compSid) {
        List<WRTransIncomeVO> results = Lists.newArrayList();
        try {
            List<WRTransIncomeWorkReportVO> wRTransIncomeWorkReportVOs = wRInboxManager.getIncomeBox(tagID,
                    transReadStatus, sendOrgs, sendName,
                    title, content, searchText, startCreateTime, endCreateTime, loginUserSid, loginUserDepSid, wr_Id, InboxType.INCOME_MEMBER);
            wRTransIncomeWorkReportVOs.forEach(item -> {
                Org sendDep = orgLogicComponents.findOrgBySid(item.getSendUserDepSid());
                User sendUser = wRUserLogicComponents.findUserBySid(item.getSendUserSid());
                String tagName = WorkProjectTagItemSetting.getWorkProjectTagName(item.getWr_tag_sid(), compSid);
                String encryptParam = this.getParamLoginUserSid(loginUserSid) + memberInBoxType;
                encryptParam = encryptLogicComponents.encrypt(encryptParam);
                String urlParam = "wrcId=" + item.getWr_Id() + "&encryptParam=" + encryptParam;
                WRTransIncomeVO vo = new WRTransIncomeVO(
                        item.getSid(),
                        item.getWr_Id(),
                        item.isTofowardDep(), item.isTofowardMember(), item.isTotrace(),
                        item.getNo(),
                        sendDep.getOrgNameWithParentIfDuplicate(),
                        sendUser.getName(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getSendTime()),
                        tagName,
                        item.getTheme(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getModifyDate()),
                        item.getWRReadStatus().getVal(),
                        transToColorCss(item.getWRReadStatus()),
                        transToBoldCss(item.getWRReadStatus()),
                        encryptParam,
                        urlParam
                );
                results.add(vo);
            });

        } catch (Exception e) {
            log.error("getWRTransIncomeMember", e);
        }
        return results;
    }

    /**
     * 取得收呈報查詢
     *
     * @param tagID 介面挑選標籤ID
     * @param transReadStatus 介面挑選是否閱讀ID
     * @param sendOrgs 介面挑選寄發單位Sid List
     * @param sendName 介面輸入寄發者
     * @param title 主題
     * @param content 內容
     * @param searchText 模糊搜尋
     * @param startCreateTime 工作報告建立起始時間
     * @param endCreateTime 工作報告建立結束時間
     * @param loginUserSid 登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     * @param wr_Id 工作報告Sid
     * @param compSid 公司Sid
     * @return List
     */
    public List<WRTransIncomeVO> getWRTransIncomeReport(String tagID, String transReadStatus, List<Integer> sendOrgs,
            String sendName, String title, String content,
            String searchText, Date startCreateTime, Date endCreateTime, Integer loginUserSid,
            Integer loginUserDepSid, String wr_Id, Integer compSid) {
        List<WRTransIncomeVO> results = Lists.newArrayList();
        try {
            List<WRTransIncomeWorkReportVO> wRTransIncomeWorkReportVOs = wRInboxManager.getIncomeBox(tagID,
                    transReadStatus, sendOrgs, sendName,
                    title, content, searchText, startCreateTime, endCreateTime, loginUserSid, loginUserDepSid, wr_Id, InboxType.INCOME_REPORT);
            wRTransIncomeWorkReportVOs.forEach(item -> {
                Org sendDep = orgLogicComponents.findOrgBySid(item.getSendUserDepSid());
                User sendUser = wRUserLogicComponents.findUserBySid(item.getSendUserSid());
                String tagName = WorkProjectTagItemSetting.getWorkProjectTagName(item.getWr_tag_sid(), compSid);
                String encryptParam = this.getParamLoginUserSid(loginUserSid) + reportInBoxType;
                encryptParam = encryptLogicComponents.encrypt(encryptParam);
                String urlParam = "wrcId=" + item.getWr_Id() + "&encryptParam=" + encryptParam;
                WRTransIncomeVO vo = new WRTransIncomeVO(
                        item.getSid(),
                        item.getWr_Id(),
                        item.isTofowardDep(), item.isTofowardMember(), item.isTotrace(),
                        item.getNo(),
                        sendDep.getOrgNameWithParentIfDuplicate(),
                        sendUser.getName(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getSendTime()),
                        tagName,
                        item.getTheme(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getModifyDate()),
                        item.getWRReadStatus().getVal(),
                        transToColorCss(item.getWRReadStatus()),
                        transToBoldCss(item.getWRReadStatus()),
                        encryptParam,
                        urlParam
                );
                results.add(vo);
            });

        } catch (Exception e) {
            log.error("getWRTransIncomeReport", e);
        }
        return results;
    }

    /**
     * 取得收指示查詢
     *
     * @param tagID 介面挑選標籤ID
     * @param transReadStatus 介面挑選是否閱讀ID
     * @param sendOrgs 介面挑選寄發單位Sid List
     * @param sendName 介面輸入寄發者
     * @param title 主題
     * @param content 內容
     * @param searchText 模糊搜尋
     * @param startCreateTime 工作報告建立起始時間
     * @param endCreateTime 工作報告建立結束時間
     * @param loginUserSid 登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     * @param wr_Id 工作報告Sid
     * @param compSid 公司Sid
     * @return List
     */
    public List<WRTransIncomeVO> getWRTransIncomeInstruction(String tagID, String transReadStatus, List<Integer> sendOrgs,
            String sendName, String title, String content,
            String searchText, Date startCreateTime, Date endCreateTime, Integer loginUserSid,
            Integer loginUserDepSid, String wr_Id, Integer compSid) {
        List<WRTransIncomeVO> results = Lists.newArrayList();
        try {
            List<WRTransIncomeWorkReportVO> wRTransIncomeWorkReportVOs = wRInboxManager.getIncomeBox(tagID,
                    transReadStatus, sendOrgs, sendName,
                    title, content, searchText, startCreateTime, endCreateTime, loginUserSid, loginUserDepSid, wr_Id, InboxType.INCOME_INSTRUCTION);
            wRTransIncomeWorkReportVOs.forEach(item -> {
                Org sendDep = orgLogicComponents.findOrgBySid(item.getSendUserDepSid());
                User sendUser = wRUserLogicComponents.findUserBySid(item.getSendUserSid());
                String tagName = WorkProjectTagItemSetting.getWorkProjectTagName(item.getWr_tag_sid(), compSid);
                String encryptParam = this.getParamLoginUserSid(loginUserSid) + InstructionInBoxType;
                encryptParam = encryptLogicComponents.encrypt(encryptParam);
                String urlParam = "wrcId=" + item.getWr_Id() + "&encryptParam=" + encryptParam;
                WRTransIncomeVO vo = new WRTransIncomeVO(
                        item.getSid(),
                        item.getWr_Id(),
                        item.isTofowardDep(), item.isTofowardMember(), item.isTotrace(),
                        item.getNo(),
                        sendDep.getOrgNameWithParentIfDuplicate(),
                        sendUser.getName(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getSendTime()),
                        tagName,
                        item.getTheme(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getModifyDate()),
                        item.getWRReadStatus().getVal(),
                        transToColorCss(item.getWRReadStatus()),
                        transToBoldCss(item.getWRReadStatus()),
                        encryptParam,
                        urlParam
                );
                results.add(vo);
            });

        } catch (Exception e) {
            log.error("getWRTransIncomeInstruction", e);
        }
        return results;
    }

    /**
     * 取得寄件備份查詢
     *
     * @param tagID 挑選介面標籤ID
     * @param transReadStatus 挑選介面是否閱讀ID
     * @param receiveName 收件者
     * @param receiveOrgs 寄發至
     * @param title 主題
     * @param content 內容
     * @param searchText 模糊搜尋
     * @param startCreateTime 工作報告建立起始時間
     * @param endCreateTime 工作報告建立結束時間
     * @param loginUserSid 登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     * @param wr_Id 工作報告Sid
     * @param transType 寄發類型ID
     * @param compSid 公司Sid
     * @return List
     */
    public List<WRTransSendVO> getWRTransSend(String tagID, String transReadStatus,
            String receiveName, List<Integer> receiveOrgs,
            String title, String content, String searchText, Date startCreateTime, Date endCreateTime,
            Integer loginUserSid, Integer loginUserDepSid, String wr_Id, String transType, Integer compSid) {
        List<WRTransSendVO> results = Lists.newArrayList();
        try {
            List<WRTransSendWorkReportVO> wRTransSendWorkReportVOs = wRInboxSendGroupManager.getSendTrans(tagID, transReadStatus, receiveName,
                    receiveOrgs, title, content, searchText, startCreateTime, endCreateTime,
                    loginUserSid, loginUserDepSid, wr_Id, transType);
            wRTransSendWorkReportVOs.forEach(item -> {
                String tagName = WorkProjectTagItemSetting.getWorkProjectTagName(item.getWr_tag_sid(), compSid);
                String encryptParam = this.getParamLoginUserSid(loginUserSid) + sendInBoxType;
                encryptParam = encryptLogicComponents.encrypt(encryptParam);
                String urlParam = "wrcId=" + item.getWr_Id() + "&encryptParam=" + encryptParam;
                WRTransSendVO wRTransSendVO = new WRTransSendVO(
                        item.getInbox_group_sid(),
                        item.getWr_Id(),
                        item.isTofowardDep(),
                        item.isTofowardMember(),
                        item.isTotrace(),
                        item.getNo(),
                        item.getSendType().getValue(),
                        item.getReceviceUser(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getSendTime()),
                        tagName,
                        item.getTheme(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getModifyDate()),
                        item.getWRReadStatus().getVal(),
                        transToColorCss(item.getWRReadStatus()),
                        transToBoldCss(item.getWRReadStatus()),
                        encryptParam,
                        urlParam
                );
                results.add(wRTransSendVO);
            });

        } catch (Exception e) {
            log.error("getWRTransSend", e);
        }
        return results;
    }

    /**
     * 取得收件夾筆數
     *
     * @param transReadStatus 閱讀狀態ID
     * @param startCreateTime 寄送起始時間
     * @param endCreateTime 寄送結束時間
     * @param loginUserSid 登入者Sid
     * @param inboxType 轉寄方式
     * @return Integer
     */
    public Integer getIncomeBoxCount(String transReadStatus, Date startCreateTime, Date endCreateTime, Integer loginUserSid, InboxType inboxType) {
        try {
            return wRInboxManager.getIncomeBoxCount(transReadStatus, startCreateTime, endCreateTime, loginUserSid, inboxType);
        } catch (Exception e) {
            log.error("getReceviceDepCount", e);
        }
        return 0;
    }

    /**
     * 取得部門轉發筆數
     *
     * @param transReadStatus 閱讀狀態ID
     * @param receiveOrgs 接收部門Sids
     * @param startCreateTime 工作報告建立起始時間
     * @param endCreateTime 工作報告建立結束時間
     * @param loginUserSid 登入者Sid
     * @return Integer
     */
    public Integer getReceiveDepCount(String transReadStatus, List<Integer> receiveOrgs, Date startCreateTime, Date endCreateTime, Integer loginUserSid) {
        try {
            return wRInboxManager.getReceiveDepCount(transReadStatus, receiveOrgs, startCreateTime, endCreateTime, loginUserSid);
        } catch (Exception e) {
            log.error("getReceiveDepCount", e);
        }
        return 0;
    }

    /**
     * 部門轉發查詢
     *
     * @param tagID 挑選介面標籤ID
     * @param transReadStatus 挑選介面是否閱讀ID
     * @param sendOrgs 寄發單位Sids
     * @param sendName 寄發者
     * @param receiveOrgs 寄發至Sids
     * @param title 主題
     * @param content 內容
     * @param searchText 模糊搜尋
     * @param startCreateTime 工作報告建立起始時間
     * @param endCreateTime 工作報告建立結束時間
     * @param loginUserSid 登入者Sid
     * @param loginUserDepSid 登入者部門Sid
     * @param wr_Id 工作報告Sid
     * @param compSid 公司Sid
     * @return List
     */
    public List<WRTransReceviceDepVO> getTransReceiveDep(String tagID, String transReadStatus, List<Integer> sendOrgs,
                                                         String sendName, List<Integer> receiveOrgs, String title, String content,
                                                         String searchText, Date startCreateTime, Date endCreateTime, Integer loginUserSid,
                                                         Integer loginUserDepSid, String wr_Id, Integer compSid) {
        List<WRTransReceviceDepVO> results = Lists.newArrayList();
        try {
            List<WRTransReceviceDepWorkReportVO> wRTransReceiveDepWorkReportVOs = wRInboxManager.getReceiveDep(tagID, transReadStatus, sendOrgs, sendName, receiveOrgs, title, content, searchText, startCreateTime, endCreateTime, loginUserSid, loginUserDepSid, wr_Id);
            wRTransReceiveDepWorkReportVOs.forEach(item -> {
                Org sendDep = orgLogicComponents.findOrgBySid(item.getSendUserDepSid());
                User sendUser = wRUserLogicComponents.findUserBySid(item.getSendUserSid());
                Org receiveDep = orgLogicComponents.findOrgBySid(item.getReceviceDepSid());
                String tagName = WorkProjectTagItemSetting.getWorkProjectTagName(item.getWr_tag_sid(), compSid);
                WRTransReceviceDepVO vo = new WRTransReceviceDepVO(
                        item.getWr_Id(),
                        item.isTofowardDep(), item.isTofowardMember(), item.isTotrace(),
                        item.getNo(),
                        sendDep.getOrgNameWithParentIfDuplicate(),
                        sendUser.getName(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getSendTime()),
                        receiveDep.getOrgNameWithParentIfDuplicate(),
                        tagName,
                        item.getTheme(),
                        ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getModifyDate()),
                        item.getWRReadStatus().getVal(),
                        transToColorCss(item.getWRReadStatus()),
                        transToBoldCss(item.getWRReadStatus())
                );
                results.add(vo);
            });

        } catch (Exception e) {
            log.error("getTransReceiveDep", e);
        }
        return results;
    }

    /**
     * 根據已未讀狀態判斷顯示顯色(待閱讀顯示紅色其他顯示黑色)
     *
     * @param wRReadStatus 已未讀狀態
     * @return String
     */
    public String transToColorCss(WRReadStatus wRReadStatus) {
        if (WRReadStatus.WAIT_READ.equals(wRReadStatus)) {
            return "color: red;";
        }
        return "";
    }

    /**
     * 根據已未讀狀態判斷顯示字體(待閱讀及未閱讀顯示粗體)
     *
     * @param wRReadStatus 已未讀狀態
     * @return String
     */
    public String transToBoldCss(WRReadStatus wRReadStatus) {
        if (WRReadStatus.UNREAD.equals(wRReadStatus) || WRReadStatus.WAIT_READ.equals(wRReadStatus)) {
            return "font-weight:bold;";
        }
        return "";
    }

}
