package com.cy.work.backend.web.controller.component;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@NoArgsConstructor
@Component
@Slf4j
public class PickerComponentHelper implements InitializingBean, Serializable {


    private static final long serialVersionUID = 2632142696749550472L;
    // ========================================================================
    // implement InitializingBean
    // ========================================================================
    private static PickerComponentHelper instance;
    public static PickerComponentHelper getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        PickerComponentHelper.instance = this;
    }
    
    // ========================================================================
    // 
    // ========================================================================

    public static final String SHOW_TREE_NAME_TEMPLET = "SHOW_TREE_NAME_TEMPLET";


    // ========================================================================
    // 方法區
    // ========================================================================

    public DefaultTreeNode buildSingleSelectTree(
            TreeNode rootNode,
            List<WkItem> allItems,
            String selectedItemSid) {

        // 已選擇節點 (回傳用)
        DefaultTreeNode selectedItem = null;

        // ====================================
        // 初始化根節點
        // ====================================
        if (rootNode == null) {
            log.error("傳入 root node 為空!");
            throw new SystemDevelopException("傳入 root node 為空!");
        }
        if (rootNode.getChildren() != null) {
            rootNode.getChildren().clear();
        }

        // ====================================
        // 處理 allItems
        // ====================================
        // 防呆
        if (WkStringUtils.isEmpty(allItems)) {
            return selectedItem;
        }

        // 排序
        allItems = allItems.stream()
                .sorted(Comparator.comparing(WkItem::getItemSeq))
                .collect(Collectors.toList());

        // ====================================
        // WkItem 轉 TreeNode
        // ====================================
        // 1. WkItem 轉 TreeNode
        // 2. 以 WkItem 建立 index
        Map<WkItem, TreeNode> treeNodeMapByItem = Maps.newLinkedHashMap();
        for (WkItem item : allItems) {
            // 建立項目樹節點物件
            DefaultTreeNode node = new DefaultTreeNode(item);
            // 節點屬性:是否可選擇
            node.setSelectable(item.isSelectable());
            // 節點屬性:是否展開
            node.setExpanded(item.isForceExpand());
            // 比對已選擇
            if (WkCommonUtils.compareByStr(item.getSid(), selectedItemSid)) {
                selectedItem = node;
            }
            treeNodeMapByItem.put(item, node);
        }

        // ====================================
        // 建立父子關係資料結構
        // ====================================
        for (WkItem item : allItems) {

            // 取得項目節點 TreeNode
            TreeNode itemTreeNode = treeNodeMapByItem.get(item);

            // 取得父項目 TreeNode
            WkItem parentItem = item.getParent();
            TreeNode parentTreeNode = treeNodeMapByItem.get(parentItem);

            // -------------------
            // WkItem 未設定父項目, 指向 root
            // -------------------
            if (parentItem == null) {
                this.addToChildrens(rootNode, itemTreeNode);
            }

            // -------------------
            // 找不到父項目節點物件, 指向 root (應該不會發生)
            // -------------------
            else if (parentTreeNode == null) {
                this.addToChildrens(rootNode, itemTreeNode);
            }

            // -------------------
            // 加入父節點
            // -------------------
            else {
                this.addToChildrens(parentTreeNode, itemTreeNode);
            }
        }

        // ====================================
        // 已選擇節點處理展開
        // ====================================
        // 取得項目節點物件
        // 1.this.selectedItem 為空時(還未選擇任何一筆)時不處理
        // 2.this.selectedItem 不在節點列表中時跳過 (可能已選項目為停用, 已選不在可選列表中)
        if (selectedItem != null) {
            // 已選擇
            selectedItem.setSelected(true);
            // 展開上層
            this.changeNodeExpand(selectedItem, true);
        }

        return selectedItem;
    }
    
    
    

    /**
     * @param parentNode
     * @param node
     */
    private void addToChildrens(TreeNode parentNode, TreeNode node) {
        if (parentNode == null || node == null) {
            return;
        }

        if (!parentNode.getChildren().contains(node)) {
            parentNode.getChildren().add(node);
        }
    }

    /**
     * @param node
     * @param isExpand
     */
    public void changeNodeExpand(TreeNode node, boolean isExpand) {
        if (node == null || node.getParent() == null) {
            return;
        }
        TreeNode parentNode = node.getParent();
        parentNode.setExpanded(isExpand);
        changeNodeExpand(parentNode, isExpand);
    }

    /**
     * @param allItem
     * @return
     */
    public Map<WkItem, List<WkItem>> prepareAllItemRelation(List<WkItem> allItem) {
        // ====================================
        // 建立子項目索引
        // ====================================
        Map<WkItem, List<WkItem>> childItemsMapByParentItem = Maps.newHashMap();
        for (WkItem item : allItem) {
            WkItem parentItem = item.getParent();

            if (parentItem == null) {
                continue;
            }

            List<WkItem> parentsChilds = childItemsMapByParentItem.get(parentItem);
            if (parentsChilds == null) {
                parentsChilds = Lists.newArrayList();
                childItemsMapByParentItem.put(parentItem, parentsChilds);
            }
            parentsChilds.add(item);
        }
        return childItemsMapByParentItem;
    }

    public void tree_searchItemNameByKeyword_recursive(
            TreeNode currNode,
            String keyword) {
        this.tree_searchItemNameByKeyword_recursive(
                currNode,
                keyword,
                "WS1-1-3",
                false,
                false);
    }

    /**
     * 遞迴展開名稱符合或已選的節點
     * 
     * @param currNode
     * @param keyword
     */
    public void tree_searchItemNameByKeyword_recursive(
            TreeNode currNode,
            String keyword,
            String keywordClassStyle,
            boolean isNumberKeyWord,
            boolean isShowSid) {

        // 先關起自己以下的節點
        currNode.setExpanded(false);

        if (currNode.getData() != null) {
            // 項目資料
            WkItem item = (WkItem) currNode.getData();

            // 名稱是否符合
            // item 有設定搜尋用名稱時，使用此欄位，否則使用 treeName
            boolean isNameMatch = false;
            if (WkStringUtils.notEmpty(item.getItemNamesForSearch())) {
                for (String currItemNameForSearch : item.getItemNamesForSearch()) {
                    // 去前後空白、轉大寫
                    currItemNameForSearch = WkStringUtils.safeTrim(currItemNameForSearch).toUpperCase();
                    if (currItemNameForSearch.contains(keyword)) {
                        isNameMatch = true;
                        break;
                    }
                }
            } else {
                String currItemNameForSearch = WkStringUtils.safeTrim(item.getTreeName()).toUpperCase();
                isNameMatch = currItemNameForSearch.contains(keyword);
            }

            // 名稱不符合時, 檢查SID 是否符合
            boolean isSidMatch = false;
            if (!isNameMatch && isShowSid && isNumberKeyWord) {
                String sid = item.getSid();
                if (sid.indexOf(":") > -1) {
                    sid = sid.split(":")[1];
                }
                if (sid.equals(keyword)) {
                    isSidMatch = true;
                }
            }

            // ---------------------
            // 符合字段高亮
            // ---------------------
            // 取得顯示名稱
            String showTreeName = WkStringUtils.safeTrim(item.getShowTreeName());
            // 移除上次高亮結果
            showTreeName = WkJsoupUtils.getInstance().restoreHighlight(showTreeName, "class='" + keywordClassStyle + "'");

            if (isNameMatch || isSidMatch) {
                showTreeName = WkJsoupUtils.getInstance().highlightKeyWordByClass(showTreeName, keyword, keywordClassStyle);
            }

            item.setShowTreeName(showTreeName);

            // 名稱符合, 或已被選擇時, 展開上層節點
            if (isNameMatch || currNode.isSelected()) {
                this.changeNodeExpand(currNode, true);
            }
        }

        if (currNode.getChildCount() != 0) {
            for (TreeNode childNode : currNode.getChildren()) {
                this.tree_searchItemNameByKeyword_recursive(
                        childNode,
                        keyword,
                        keywordClassStyle,
                        isNumberKeyWord,
                        isShowSid);
            }
        }
    }

    /**
     * @param currNode
     * @param searchKeyword
     */
    public void tree_searchItemNameByKeywordForUseTemplet(
            TreeNode rootNode,
            String searchKeyword) {

        // ====================================
        // 防呆
        // ====================================
        if (rootNode == null) {
            throw new SystemDevelopException("傳入 root node 為空");
        }

        if (!"root".equals(rootNode.getRowKey())) {
            throw new SystemDevelopException("傳入必須為 root node");
        }

        // ====================================
        // 前置
        // ====================================
        // 關鍵字轉大寫
        searchKeyword = WkStringUtils.safeTrim(searchKeyword).toUpperCase();

        rootNode.setExpanded(false);

        // ====================================
        // 逐筆處理子節點
        // ====================================
        if (rootNode.getChildCount() != 0) {
            for (TreeNode childNode : rootNode.getChildren()) {
                this.tree_searchItemNameByKeywordForUseTemplet_recursive(
                        childNode,
                        searchKeyword,
                        "WS1-1-3");
            }
        }
    }

    /**
     * @param currNode
     * @param searchKeyword
     * @param keywordClassStyle
     */
    private void tree_searchItemNameByKeywordForUseTemplet_recursive(
            TreeNode currNode,
            String searchKeyword,
            String keywordClassStyle) {
        // ====================================
        // 防呆
        // ====================================
        if (currNode.getData() == null) {
            return;
        }

        // ====================================
        // 初始化
        // ====================================
        // 先關起自己以下的節點
        currNode.setExpanded(false);

        // ====================================
        // 防呆
        // ====================================
        if (currNode.getData() == null) {
            return;
        }

        // ====================================
        // 處理資料
        // ====================================
        // 取得資料容器
        WkItem item = (WkItem) currNode.getData();

        // 檢查是否有關鍵字
        boolean isNameMatch = false;

        List<String> newNames = Lists.newArrayList();
        for (String currItemNameForSearch : item.getItemNamesForSearch()) {

            if (!"".equals(searchKeyword)) { // 避免 searchKeyword 為 "" 時，會無條件比對成功
                // 比對時去前後空白、轉大寫
                String compareName = WkStringUtils.safeTrim(currItemNameForSearch).toUpperCase();

                if (compareName.indexOf(searchKeyword) >= 0) {
                    // 註記命中
                    isNameMatch = true;
                    // 關鍵字 highlight
                    currItemNameForSearch = WkJsoupUtils.getInstance().highlightKeyWordByClass(
                            currItemNameForSearch, searchKeyword, keywordClassStyle);
                }
            }
            newNames.add(currItemNameForSearch);
        }

        // 取得 SHOW_TREE_NAME_TEMPLET
        String showTreeNameTemplet = item.getOtherInfo().get(SHOW_TREE_NAME_TEMPLET);

        if (WkStringUtils.isEmpty(showTreeNameTemplet)) {
            throw new SystemDevelopException("樹節點未放入參數:SHOW_TREE_NAME_TEMPLET !");
        }

        // 設定顯示
        item.setShowTreeName(String.format(showTreeNameTemplet, newNames.toArray()));

        // 名稱符合, 或已被選擇時, 展開上層節點
        if (isNameMatch || currNode.isSelected()) {
            this.changeNodeExpand(currNode, true);
        }

        // ====================================
        // 遞迴處理下層節點
        // ====================================
        if (currNode.getChildCount() != 0) {
            for (TreeNode childNode : currNode.getChildren()) {
                this.tree_searchItemNameByKeywordForUseTemplet_recursive(
                        childNode,
                        searchKeyword,
                        keywordClassStyle);
            }
        }
    }
    
    /**
     * 取得所有 root 下的 tree node
     * @param treeNodelist
     * @param root
     */
    public void tree_NodeToList(List<TreeNode> treeNodelist, TreeNode root) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        root.getChildren().forEach(each -> {
            treeNodelist.add(each);
            this.tree_NodeToList(treeNodelist, each);
        });
    }
}
