/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.listener.WorkReportButtonCallBack;
import com.cy.work.backend.web.logic.components.*;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.util.ToolsList;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.vo.WRMaster;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMemberTo;
import com.cy.work.backend.web.vo.enums.WREditType;
import com.cy.work.backend.web.vo.enums.WRStatus;
import com.cy.work.backend.web.vo.enums.WReadReceiptStatus;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author brain0925_liao
 */
public class WorkReportBtnComponent extends ButtonComponent {


    private static final long serialVersionUID = 2434571908649649482L;
    @Getter
    private WorkReportButtonCallBack workReportButtonListener;
    @Getter
    @Setter
    private Boolean editMode = false;
    @Getter
    @Setter
    private String selectTagID;
    @Getter
    @Setter
    private String selectEditTypeID;
    private String wr_ID;
    private Integer loginUserSid;
    @Getter
    @Setter
    private String invalidReason;
    @Getter
    private String invalidCannotEnterReason;
    @Getter
    private String traceBtnCss;
    @Getter
    private String favoriteBtnCss;
    @Getter
    private boolean isFavorite = false;

    public WorkReportBtnComponent(WorkReportButtonCallBack workReportButtonListener, Integer loginUserSid) {
        this.workReportButtonListener = workReportButtonListener;
        this.loginUserSid = loginUserSid;
    }

    public void loadTag() {
        WorkReportComponent data = loadWRMaster();
        selectTagID = data.getWRMaster().getWr_tag_sid();
        DisplayController displayController = SpringContextHolder.getBean(DisplayController.class);
        displayController.showPfWidgetVar("modifyTagDialog");

    }

    public void loadEditType() {
        WorkReportComponent data = loadWRMaster();
        selectEditTypeID = data.getWRMaster().getEdit_type().name();
        DisplayController displayController = SpringContextHolder.getBean(DisplayController.class);
        displayController.showPfWidgetVar("modifyEditTypeDialog");
    }

    private WorkReportComponent loadWRMaster() {
        return WRMasterLogicComponents.getInstance().getWorkReportComponentBySid(wr_ID, loginUserSid);
    }

    private void initButton() {
        this.traceBtnCss = "";
        this.favoriteBtnCss = "";
        this.setEditAble(false);
        this.setTransAble(false);
        this.setReplyAble(false);
        this.setAttachAble(false);
        this.setTagAble(false);
        this.setFavoriteAble(false);
        this.setTraceAble(false);
        this.setSubmmitAble(false);
        this.setSendAble(false);
        this.setCancalAble(false);
        this.setPreviousAble(false);
        this.setNextAble(false);
        this.setBackList(false);
        this.setReadRcordAble(false);
        this.setInvalidAble(false);
        this.setDeleteAble(false);
        this.setEditTypeAble(false);
        this.setShowDeleteBtn(false);
        this.setInvalidAble(false);
    }

    private void checkTrace(WRMaster wRMaster) {
        boolean hasTrace = WorkTraceInfoLogicComponents.getInstance().isHasTracePersonUnFinished(loginUserSid, wRMaster.getSid());
        if (!hasTrace) {
            User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
            hasTrace = WorkTraceInfoLogicComponents.getInstance().isHasTraceDepUnFinished(user.getPrimaryOrg().getSid(), wRMaster.getSid());

        }
        if (hasTrace) {
            this.traceBtnCss = "color: red";
        }
    }

    private void checkFavorite(WRMaster wRMaster) {
        isFavorite = WRFavoriteLogicComponents.getInstance().isFavorite(wRMaster.getSid(), loginUserSid);
        if (isFavorite) {
            this.favoriteBtnCss = "color: red";
        }
    }

    public void loadButtonAble(WRMaster wRMaster,
                               boolean showPreviousBtn, boolean showNextBtn, boolean showBackListBtn) {

        initButton();
        if (wRMaster == null) {
            return;
        }
        wr_ID = wRMaster.getSid();
        this.setPreviousAble(showPreviousBtn);
        this.setNextAble(showNextBtn);
        this.setBackList(showBackListBtn);
        checkTrace(wRMaster);
        checkFavorite(wRMaster);
        if (WRStatus.INVIALD.equals(wRMaster.getWr_status())) {
            this.invalidCannotEnterReason = "";
            loadINVIALDButton();
            return;
        }

        if (editMode) {
            if (wRMaster.getWr_status().equals(WRStatus.COMMIT)) {
                this.setShowInvalidBtn(true);
            } else {
                this.setShowDeleteBtn(true);
            }
            loadEDITButton();
            this.setPreviousAble(false);
            this.setNextAble(false);
            this.setBackList(false);
            return;
        }
        this.invalidCannotEnterReason = "";
        if (wRMaster.getWr_status().equals(WRStatus.COMMIT)) {
            loadCOMMITButton(wRMaster);
            return;
        }
        if (wRMaster.getWr_status().equals(WRStatus.DRAFT)) {
            loadDRAFTButton(wRMaster);
            return;
        }
    }

    private void loadCOMMITButton(WRMaster wRMaster) {
        Org depOrg = OrgLogicComponents.getInstance().findOrgBySid(wRMaster.getDep_sid());
        List<Org> parents = OrgLogicComponents.getInstance().getAllParentOrgs(depOrg);
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        List<UserViewVO> users = Lists.newArrayList();
        parents.forEach(item -> users.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid())));
        parents.forEach(item->users.addAll(WRUserLogicComponents.getInstance().getManagers(item.getSid())));
        UserViewVO loginUser = new UserViewVO(loginUserSid, "");
        if (wRMaster.getEdit_type().equals(WREditType.DEP)) {
            // 需大於最後可編輯時間才可修改
            if (wRMaster.getLast_modify_dt() != null && wRMaster.getLast_modify_dt().compareTo(new Date()) > 0) {
                Org baseOrg = WkOrgUtils.prepareBaseDep(user.getPrimaryOrg().getSid(), OrgLevel.MINISTERIAL);
                List<Org> allChilds = OrgLogicComponents.getInstance().getAllChildOrg(baseOrg);
                allChilds.add(baseOrg);
                if (allChilds.contains(depOrg) || depOrg.getSid().equals(user.getPrimaryOrg().getSid())) {
                    this.setEditAble(true);
                }
            }
            if (wRMaster.getCreate_usr().equals(loginUserSid)) {
                this.setTagAble(true);
            }
        }

        this.setTransAble(true);
        this.setTraceAble(true);
        this.setFavoriteAble(true);
        this.setAttachAble(true);
        this.setReplyAble(true);
        if (wRMaster.getCreate_usr().equals(loginUserSid)) {
            this.setReadRcordAble(true);
        }
        this.setShowInvalidBtn(true);

        // 作廢按鈕規則-尚未轉寄
        // users 上級部門的主管List
        if (users.contains(loginUser) || (ToolsList.isOneOfManagers(loginUserSid,depOrg.getManagers()))) {
            // 多主管
            if (wRMaster.getRead_receipts_member() == null || wRMaster.getRead_receipts_member().getReadReceiptsMemberTo() == null
                    || wRMaster.getRead_receipts_member().getReadReceiptsMemberTo().size() == 0) {
                this.setInvalidAble(true);
            } else {
                List<ReadReceiptsMemberTo> readUser = wRMaster.getRead_receipts_member().getReadReceiptsMemberTo().stream()
                        .filter(each -> each.getReadreceipt().equals(WReadReceiptStatus.READ.name()))
                        .collect(Collectors.toList());
                if (WkStringUtils.isEmpty(readUser)) {
                    this.setInvalidAble(true);
                } else {
                    this.invalidCannotEnterReason = "已有回傳索取記錄，故無法作廢";
                }
            }
        } else {
            this.invalidCannotEnterReason = "無權限進行作廢";
        }
    }

    private void loadDRAFTButton(WRMaster wRMaster) {
        this.setShowDeleteBtn(true);
        if (wRMaster.getCreate_usr().equals(loginUserSid)) {
            this.setEditAble(true);
            this.setAttachAble(true);
            this.setTagAble(true);
            this.setSubmmitAble(true);
            this.setReadRcordAble(false);
            this.setInvalidAble(false);
            this.setDeleteAble(true);
            this.setTagAble(true);
            this.setEditTypeAble(true);
        }
    }

    private void loadINVIALDButton() {
        this.setReplyAble(true);
        this.setTraceAble(true);
        this.setFavoriteAble(true);
    }

    private void loadEDITButton() {
        this.setSendAble(true);
        this.setCancalAble(true);
    }

}
