/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class SentBackReadStatusVO implements Serializable {

    
    private static final long serialVersionUID = -5210320305794603005L;
    public SentBackReadStatusVO(String inbox_sid, String departmentName, String userName, String readTime, String cancelTime, boolean typeDep, boolean canCancel, Integer userSid, Integer depSid) {
        this.inbox_sid = inbox_sid;
        this.departmentName = departmentName;
        this.userName = userName;
        this.readTime = readTime;
        this.cancelTime = cancelTime;
        this.typeDep = typeDep;
        this.canCancel = canCancel;
        this.userSid = userSid;
        this.depSid = depSid;
    }

    @Getter
    private String inbox_sid;
    @Getter
    private String departmentName;
    @Getter
    private String userName;
    @Getter
    private String readTime;
    @Getter
    private String cancelTime;
    @Getter
    private boolean typeDep;
    @Getter
    private boolean canCancel;
    @Getter
    private Integer userSid;
    @Getter
    private Integer depSid;

}
