/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.cy.work.backend.web.common.setting.WRTraceSetting;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 追蹤狀態Item Bean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("request")
@ManagedBean
public class WorkReportTraceStatusItemBean implements Serializable {


    private static final long serialVersionUID = -3691663329847951290L;

    /** 取得 追蹤狀態SelectItems */
    public List<SelectItem> getWorkReportTraceStatusItems() {
        return WRTraceSetting.getWRTraceItems();
    }

}
