/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.manager.WRMasterCustomLogicManager;
import com.cy.work.backend.web.logic.manager.WRMasterManager;
import com.cy.work.backend.web.logic.manager.WorkSettingUserManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.util.ToolsList;
import com.cy.work.backend.web.view.vo.ReadReceiptVO;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.vo.WRMaster;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMember;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMemberTo;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.WReadReceiptStatus;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 索取回條邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class WRReadReceiptComponents implements InitializingBean, Serializable {


    private static final long serialVersionUID = 7299221245893278480L;
    private static WRReadReceiptComponents instance;

    @Autowired
    private WRMasterManager wRMasterManager;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;
    @Autowired
    private WRMasterCustomLogicManager wRMasterCustomLogicManager;

    public static WRReadReceiptComponents getInstance() {
        return instance;
    }
    
    @Override
    public void afterPropertiesSet() throws Exception {
        WRReadReceiptComponents.instance = this;
    }

    /**
     * 取得索取讀取回條介面物件List By 工作報告Sid
     *
     * @param wr_id 工作報告Sid
     * @return
     */
    public List<ReadReceiptVO> getReadReceiptVOByWrSid(String wr_id) {
        WRMaster wr = wRMasterManager.findBySid(wr_id);
        if (wr.getRead_receipts_member() == null) {
            return Lists.newArrayList();
        }
        
        List<ReadReceiptsMemberTo> readReceiptsMemberTos = wr.getRead_receipts_member().getReadReceiptsMemberTo();
        List<ReadReceiptVO> readReceiptVOs = Lists.newArrayList();
        readReceiptsMemberTos.forEach(item -> {
            User user = workSettingUserManager.findBySID(Integer.valueOf(item.getReader()));
            readReceiptVOs.add(new ReadReceiptVO(user.getName(), user.getPrimaryOrg().getName(), item.getReader(), WReadReceiptStatus.valueOf(item.getReadreceipt()), item.getReadtime(), item.getRequiretime()));
        });
        return readReceiptVOs;
    }

    /**
     * ReadReceiptsMemberTo 比較key - reader
     */
    transient private Comparator<ReadReceiptsMemberTo> readReceiptsMemberToComparator = new Comparator<ReadReceiptsMemberTo>() {
        @Override
        public int compare(ReadReceiptsMemberTo obj1, ReadReceiptsMemberTo obj2) {
            final String reader1 = obj1.getReader();
            final String reader2 = obj2.getReader();
            return reader1.equals(reader2)
                    ? 0
                    : reader1.compareTo(reader2);
        }
    };

    /**
     * 回傳已讀取至索取讀取回條
     *
     * @param readUserSid 回傳者Sid
     * @param wr_id 工作報告Sid
     */
    public void doReadReceipt(Integer readUserSid, String wr_id) {
        WRMaster wr = wRMasterManager.findBySid(wr_id);
        if (wr.getRead_receipts_member() == null || wr.getRead_receipts_member().getReadReceiptsMemberTo() == null
                || wr.getRead_receipts_member().getReadReceiptsMemberTo().size() == 0) {
            Preconditions.checkState(false, "已不在索取讀取記錄清單內");
        }
        List<ReadReceiptsMemberTo> needToReadUser = wr.getRead_receipts_member().getReadReceiptsMemberTo().stream()
                .filter(each -> each.getReader().equals(String.valueOf(readUserSid)))
                .collect(Collectors.toList());
        if (WkStringUtils.isEmpty(needToReadUser)) {
            Preconditions.checkState(false, "已不在索取讀取記錄清單內");
        }
        ReadReceiptsMember rms = new ReadReceiptsMember();
        wr.getRead_receipts_member().getReadReceiptsMemberTo().forEach(item -> {
            if (item.getReader().equals(String.valueOf(readUserSid))
                    && item.getReadreceipt().equals(WReadReceiptStatus.UNREAD.name())) {
                item.setReadreceipt(WReadReceiptStatus.READ.name());
                item.setReadtime(ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date()));
            }
            rms.getReadReceiptsMemberTo().add(item);
        });
        wr.setRead_receipts_member(rms);        
        wRMasterManager.updateReadReceiptsMember(rms, wr_id);
    }

    /**
     * 儲存索取讀取記錄回條
     *
     * @param updateUserSid 儲存者Sid
     * @param wr_id 工作報告Sid
     * @param userViewVO 欲索取讀取記錄使用者List
     * @return
     */
    public String saveReadReceiptVO(Integer updateUserSid, String wr_id, List<UserViewVO> userViewVO) {
        WRMaster wr = wRMasterManager.findBySid(wr_id);
        if ((wr.getRead_receipts_member() == null || wr.getRead_receipts_member().getReadReceiptsMemberTo() == null
                || wr.getRead_receipts_member().getReadReceiptsMemberTo().isEmpty())
                && (userViewVO == null || userViewVO.isEmpty())) {
            Preconditions.checkState(false, "尚未挑選任何資訊,請確認！");
        }
        List<ReadReceiptsMemberTo> saveReadReceiptsMemberTo = Lists.newArrayList();
        List<ReadReceiptsMemberTo> selectReadReceiptsMemberTo = Lists.newArrayList();
        Date now = new Date();
        userViewVO.forEach(item -> {
            selectReadReceiptsMemberTo.add(new ReadReceiptsMemberTo(String.valueOf(item.getSid()), WReadReceiptStatus.UNREAD.name(),
                    "", ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), now)));
        });
        boolean noChanged = false;
        if (wr.getRead_receipts_member() != null && wr.getRead_receipts_member().getReadReceiptsMemberTo() != null) {
            //需確認該次人員是否有變更,若有變更會增加追蹤紀錄
            noChanged = ToolsList.areEqualIgnoringOrder(selectReadReceiptsMemberTo, wr.getRead_receipts_member().getReadReceiptsMemberTo(), readReceiptsMemberToComparator);
            StringBuilder moveErrorMessage = new StringBuilder();
            List<ReadReceiptsMemberTo> oldReadReceiptsMemberTo = wr.getRead_receipts_member().getReadReceiptsMemberTo();
            //資料庫內已讀人員,發現該次變更會被移除,將必須彈出Alert並且告知不可被移除
            List<ReadReceiptsMemberTo> removed = oldReadReceiptsMemberTo.stream()
                    .filter(each -> each.getReadreceipt().equals(WReadReceiptStatus.READ.name()) && !selectReadReceiptsMemberTo.contains(each))
                    .collect(Collectors.toList());
            removed.forEach(item -> {
                if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
                    moveErrorMessage.append("\n");
                }
                User movedUser = workSettingUserManager.findBySID(Integer.valueOf(item.getReader()));
                moveErrorMessage.append("成員[" + movedUser.getPrimaryOrg().getName() + "-" + movedUser.getName() + "]已讀,不可移除");
            });
            if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
                Preconditions.checkState(false, moveErrorMessage.toString());
            }
            selectReadReceiptsMemberTo.forEach(item -> {
                //若資料庫已有記錄讀取記錄,不可變更,必須以資料庫為主
                List<ReadReceiptsMemberTo> sameReadReceiptsMemberTo = oldReadReceiptsMemberTo.stream()
                        .filter(each -> each.getReader().equals(item.getReader()))
                        .collect(Collectors.toList());
                if (sameReadReceiptsMemberTo != null && sameReadReceiptsMemberTo.size() > 0) {
                    saveReadReceiptsMemberTo.add(sameReadReceiptsMemberTo.get(0));
                } else {
                    saveReadReceiptsMemberTo.add(item);
                }
            });
        } else {
            saveReadReceiptsMemberTo.addAll(selectReadReceiptsMemberTo);
        }
        ReadReceiptsMember saveReadReceiptsMember = new ReadReceiptsMember();
        saveReadReceiptsMember.setReadReceiptsMemberTo(saveReadReceiptsMemberTo);
        wr.setRead_receipts_member(saveReadReceiptsMember);
        wRMasterCustomLogicManager.saveReadReceipt(wr, updateUserSid, noChanged);
        return "";
    }
    
}
