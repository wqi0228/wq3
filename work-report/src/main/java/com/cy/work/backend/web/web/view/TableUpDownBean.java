/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope(WebApplicationContext.SCOPE_SESSION)
public class TableUpDownBean implements Serializable {


    private static final long serialVersionUID = 3235022781716172060L;
    @Setter
    @Getter
    private String session_previous_sid;
    @Setter
    @Getter
    private String session_next_sid;
    @Setter
    @Getter
    private String session_now_sid;
    @Setter
    @Getter
    private String session_show_home;

    private String session_setting_over;
    @Setter
    @Getter
    private String worp_path;

    transient private Lock lock = new ReentrantLock();

    @PostConstruct
    void init() {
        log.info("TableUpDownBean - 上下筆傳遞Session產生");
    }

    @PreDestroy
    void destory() {
        log.info("TableUpDownBean - 銷毀");
    }

    public boolean isSessionSettingOver() {
        lock.lock();// 得到锁
        try {
            return !Strings.isNullOrEmpty(session_setting_over);
        } catch (Exception e) {
            log.error("isSessionSettingOver Error", e);
        } finally {
            lock.unlock();// 释放锁
        }
        return false;
    }

    public void setSessionSettingOver() {
        lock.lock();// 得到锁
        try {
            session_setting_over = "1";
        } catch (Exception e) {
            log.error("setSessionSettingOver Error", e);
        } finally {
            lock.unlock();// 释放锁
        }
    }

    public void clearSessionSettingOver() {
        lock.lock();// 得到锁
        try {
            if (Strings.isNullOrEmpty(session_setting_over)) {
                log.error("Session 上下筆設定尚未完成,便進行清除");
            }
            session_setting_over = "";
        } catch (Exception e) {
            log.error("clearSessionSettingOver Error", e);
        } finally {
            lock.unlock();// 释放锁
        }
    }

}
