/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.callable;

import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.cy.work.backend.web.vo.enums.WRFunItemComponentType;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * 選單計算筆數Callable 傳遞物件
 * @author brain0925_liao
 */
public class HomeCallableCondition implements Serializable {


    private static final long serialVersionUID = -6379574496936328202L;
    @Setter
    @Getter
    private WRFunItemGroupVO wrFunItemGroupVO;
    @Setter
    @Getter
    private WRFunItemComponentType wrFunItemComponentType;
    @Setter
    @Getter
    private String conntString;
    @Setter
    @Getter
    private String cssString;
}
