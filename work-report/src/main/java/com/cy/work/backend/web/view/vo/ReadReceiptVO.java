/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import com.cy.work.backend.web.vo.enums.WReadReceiptStatus;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class ReadReceiptVO implements Serializable{


    private static final long serialVersionUID = -7965882586934533116L;
    public ReadReceiptVO(String name,String departmentName,  String userSid, WReadReceiptStatus wReadReceiptStatus,
            String readtime, String requiretime) {
        this.userSid = userSid;
        this.wReadReceiptStatus = wReadReceiptStatus;
        this.readtime = readtime;
        this.requiretime = requiretime;
        this.departmentName = departmentName;
        this.name = name;
    }
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String departmentName;
    @Getter
    @Setter
    private String userSid;
    @Getter
    @Setter
    private WReadReceiptStatus wReadReceiptStatus;
    @Getter
    @Setter
    private String readtime;
    @Getter
    @Setter
    private String requiretime;

}
