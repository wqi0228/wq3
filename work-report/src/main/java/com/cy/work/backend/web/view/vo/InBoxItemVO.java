/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import com.cy.work.backend.web.vo.enums.InboxType;
import java.io.Serializable;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class InBoxItemVO implements Serializable {

    
    private static final long serialVersionUID = -7303957301637996072L;
    public InBoxItemVO(String senderName, String sendTime,
            String receviceName, String messageCss, InboxType inbox_type) {
        this.senderName = senderName;
        this.sendTime = sendTime;
        this.receviceName = receviceName;
        this.messageCss = messageCss;
        this.inbox_type = inbox_type;
    }

    @Getter
    private String senderName;
    @Getter
    private String sendTime;
    @Getter
    private String receviceName;
    @Getter
    private String messageCss;
    @Getter
    private InboxType inbox_type;

}
