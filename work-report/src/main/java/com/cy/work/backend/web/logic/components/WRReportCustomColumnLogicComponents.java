
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.work.backend.web.logic.manager.WRReportCustomColumnManager;
import com.cy.work.backend.web.view.vo.CustomCulumnVO;
import com.cy.work.backend.web.view.vo.WRColumnDetailVO;
import com.cy.work.backend.web.view.vo.WRDraftSearchColumnVO;
import com.cy.work.backend.web.view.vo.WRFavoriteSearchColumnVO;
import com.cy.work.backend.web.view.vo.WRLastModifySearchColumnVO;
import com.cy.work.backend.web.view.vo.WRTagSearchColumnVO;
import com.cy.work.backend.web.view.vo.WRTraceSearchColumnVO;
import com.cy.work.backend.web.view.vo.WRTransDepSearchColumnVO;
import com.cy.work.backend.web.view.vo.WRTransIncomeSearchColumnVO;
import com.cy.work.backend.web.view.vo.WRTransSendSearchColumnVO;
import com.cy.work.backend.web.view.vo.WRWorkReportSearchColumnVO;
import com.cy.work.backend.web.vo.WRReportCustomColumn;
import com.cy.work.backend.web.vo.converter.to.CustomColumnDetailTo;
import com.cy.work.backend.web.vo.converter.to.WRReportCustomColumnInfo;
import com.cy.work.backend.web.vo.converter.to.WRReportCustomColumnInfoTo;
import com.cy.work.backend.web.vo.enums.WRColumn;
import com.cy.work.backend.web.vo.enums.WRDraftSearchColumn;
import com.cy.work.backend.web.vo.enums.WRFavoriteSearchColumn;
import com.cy.work.backend.web.vo.enums.WRIncomeTransSearchColumn;
import com.cy.work.backend.web.vo.enums.WRLastModifySearchColumn;
import com.cy.work.backend.web.vo.enums.WRReceviceDepTransSearchColumn;
import com.cy.work.backend.web.vo.enums.WRSendTransSearchColumn;
import com.cy.work.backend.web.vo.enums.WRTagSearchColumn;
import com.cy.work.backend.web.vo.enums.WRTraceSearchColumn;
import com.cy.work.backend.web.vo.enums.WRWorkReportSearchColumn;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 自訂欄位邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRReportCustomColumnLogicComponents implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = -2726372057524144545L;
    private static WRReportCustomColumnLogicComponents instance;

    @Autowired
    private WRReportCustomColumnManager wrReportCustomColumnManager;

    public static WRReportCustomColumnLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRReportCustomColumnLogicComponents.instance = this;
    }

    /**
     * 設定顯示欄位
     *
     * @param wrReportCustomColumnInfo 資料庫蘭位資料
     * @param wRColumns 該報表欄位資訊
     * @param selectCustomCulumns 挑選顯示自訂欄位
     * @param wrReportCustomColumnUrlType 報表類型
     * @return
     */
    private List<WRReportCustomColumnInfoTo> settingShowColumn(WRReportCustomColumnInfo wrReportCustomColumnInfo,
            List<WRColumn> wRColumns,
            List<CustomCulumnVO> selectCustomCulumns, WRReportCustomColumnUrlType wrReportCustomColumnUrlType) {
        List<WRReportCustomColumnInfoTo> wrReportCustomColumnInfoTos = Lists.newArrayList();
        if (wrReportCustomColumnInfo != null && wrReportCustomColumnInfo.getWrReportCustomColumnInfoTos() != null) {
            wrReportCustomColumnInfo.getWrReportCustomColumnInfoTos().forEach(item -> {
                //需檢測DB欄位是否與目前程式欄位一致
                List<WRColumn> column = wRColumns.stream()
                        .filter(each -> each.getVal().equals(item.getName()))
                        .collect(Collectors.toList());
                if (column == null || column.size() == 0) {
                    log.error("問題欄位 : WRReportCustomColumnUrlType - " + wrReportCustomColumnUrlType.getVal()
                            + " ColumnName" + item.getName());
                    return;
                }

                WRColumn itemWRColumn = column.get(0);
                List<CustomCulumnVO> selCutom = selectCustomCulumns.stream()
                        .filter(each -> each.getName().equals(itemWRColumn.getVal()))
                        .collect(Collectors.toList());
                //檢測該次挑選的欄位,是否為可移除欄位,若不可移除,依舊需加入
                if (selCutom == null || selCutom.size() == 0) {
                    if (!itemWRColumn.getCanSelectItem()) {
                        wrReportCustomColumnInfoTos.add(item);
                    }
                } else {
                    wrReportCustomColumnInfoTos.add(item);
                }
            });
        }
        wRColumns.forEach(item -> {
            List<WRReportCustomColumnInfoTo> exists = wrReportCustomColumnInfoTos.stream()
                    .filter(each -> each.getName().equals(item.getVal()))
                    .collect(Collectors.toList());
            if (exists != null && exists.size() > 0) {
                return;
            }
            if (!item.getCanSelectItem()) {
                WRReportCustomColumnInfoTo wt = new WRReportCustomColumnInfoTo();
                wt.setName(item.getVal());
                wt.setWidth(item.getDefaultWidth());
                wrReportCustomColumnInfoTos.add(wt);
            } else {
                List<CustomCulumnVO> selCutom = selectCustomCulumns.stream()
                        .filter(each -> each.getName().equals(item.getVal()))
                        .collect(Collectors.toList());
                if (selCutom != null && selCutom.size() > 0) {
                    WRReportCustomColumnInfoTo wt = new WRReportCustomColumnInfoTo();
                    wt.setName(item.getVal());
                    wt.setWidth(item.getDefaultWidth());
                    wrReportCustomColumnInfoTos.add(wt);
                }
            }
        });
        return wrReportCustomColumnInfoTos;
    }

    /**
     * 設定欄位寬度
     *
     * @param wrReportCustomColumnInfo 資料庫蘭位資料
     * @param wRColumns 該報表欄位資訊
     * @param customColumnDetailTo Li
     * @param wrReportCustomColumnUrlType 報表類型
     * @return
     */
    private List<WRReportCustomColumnInfoTo> settingWidthColumn(WRReportCustomColumnInfo wrReportCustomColumnInfo,
            List<WRColumn> wRColumns,
            List<CustomColumnDetailTo> customColumnDetailTo, WRReportCustomColumnUrlType wrReportCustomColumnUrlType) {
        List<WRReportCustomColumnInfoTo> wrReportCustomColumnInfoTos = Lists.newArrayList();
        //檢測欄位是否有變更過,若有變更過,僅變更存在的欄位寬度
        if (wrReportCustomColumnInfo != null && wrReportCustomColumnInfo.getWrReportCustomColumnInfoTos() != null) {
            wrReportCustomColumnInfo.getWrReportCustomColumnInfoTos().forEach(item -> {
                List<WRColumn> column = wRColumns.stream()
                        .filter(each -> each.getVal().equals(item.getName()))
                        .collect(Collectors.toList());
                if (column == null || column.size() == 0) {
                    log.error("問題欄位 : WRReportCustomColumnUrlType - " + wrReportCustomColumnUrlType.getVal()
                            + " ColumnName" + item.getName());
                    return;
                }
                WRColumn itemWRColumn = column.get(0);
                List<CustomColumnDetailTo> selCutom = customColumnDetailTo.stream()
                        .filter(each -> each.getName().equals(itemWRColumn.getVal()))
                        .collect(Collectors.toList());
                //若介面與DB無找到符合,不進行變動存進DB
                if (selCutom == null || selCutom.size() == 0) {
                    wrReportCustomColumnInfoTos.add(item);
                    //若發現該欄位不可變動寬度,直接存進DB
                } else if (!itemWRColumn.getCanModifyWidth()) {
                    wrReportCustomColumnInfoTos.add(item);
                } else {
                    item.setWidth(selCutom.get(0).getWidth());
                    wrReportCustomColumnInfoTos.add(item);
                }
            });
        } else {
            wRColumns.forEach(item -> {
                if (!item.getDefaultShow()) {
                    return;
                }
                if (!item.getCanModifyWidth()) {
                    WRReportCustomColumnInfoTo wt = new WRReportCustomColumnInfoTo();
                    wt.setName(item.getVal());
                    wt.setWidth(item.getDefaultWidth());
                    wrReportCustomColumnInfoTos.add(wt);
                } else {
                    List<CustomColumnDetailTo> selCutom = customColumnDetailTo.stream()
                            .filter(each -> each.getName().equals(item.getVal()))
                            .collect(Collectors.toList());
                    if (selCutom != null && selCutom.size() > 0) {
                        WRReportCustomColumnInfoTo wt = new WRReportCustomColumnInfoTo();
                        wt.setName(item.getVal());
                        wt.setWidth(selCutom.get(0).getWidth());
                        wrReportCustomColumnInfoTos.add(wt);
                    }
                }
            });
        }
        return wrReportCustomColumnInfoTos;
    }

    /**
     * 儲存欄位設定
     *
     * @param userSid 登入者Sid
     * @param wRColumns
     * @param selectCustomCulumns
     * @param pageCount
     * @param wrReportCustomColumnUrlType
     */
    public void saveWRColumnShowSetting(Integer userSid, List<WRColumn> wRColumns,
            List<CustomCulumnVO> selectCustomCulumns, String pageCount, WRReportCustomColumnUrlType wrReportCustomColumnUrlType) {
        WRReportCustomColumn columDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(wrReportCustomColumnUrlType, userSid);

        if (columDB == null) {
            columDB = new WRReportCustomColumn();
            columDB.setUrl(wrReportCustomColumnUrlType);
        }
        columDB.setPagecnt(Integer.valueOf(pageCount));
        WRReportCustomColumnInfo wci = new WRReportCustomColumnInfo();
        wci.setWrReportCustomColumnInfoTos(settingShowColumn(columDB.getInfo(), wRColumns, selectCustomCulumns, wrReportCustomColumnUrlType));
        columDB.setInfo(wci);
        wrReportCustomColumnManager.save(columDB, userSid);
    }

    /**
     * 建立欄位介面物件
     *
     * @param wRColumn
     * @return
     */
    private WRColumnDetailVO createDefaultColumnDetail(WRColumn wRColumn) {
        return new WRColumnDetailVO(wRColumn.getVal(), wRColumn.getDefaultShow(), transToStyleCss(wRColumn.getDefaultWidth()));
    }

    public WRTagSearchColumnVO getWRTagSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn columDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.TAG_SEARCH, userSid);
        WRTagSearchColumnVO vo = new WRTagSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRTagSearchColumn.INDEX));
            vo.setCreateTime(createDefaultColumnDetail(WRTagSearchColumn.CREATE_TIME));
            vo.setCreateUser(createDefaultColumnDetail(WRTagSearchColumn.CREATE_USER));
            vo.setCategoryName(createDefaultColumnDetail(WRTagSearchColumn.CATEGORY_NAME));
            vo.setTag(createDefaultColumnDetail(WRTagSearchColumn.TAG));
            vo.setStatus(createDefaultColumnDetail(WRTagSearchColumn.STATUS));
            vo.setModifyTime(createDefaultColumnDetail(WRTagSearchColumn.MODIFY_TIME));
            vo.setSeq(createDefaultColumnDetail(WRTagSearchColumn.SEQ));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRTagSearchColumn.INDEX, columDB.getInfo()));
            vo.setCreateTime(transToWRColumnDetailVO(WRTagSearchColumn.CREATE_TIME, columDB.getInfo()));
            vo.setCreateUser(transToWRColumnDetailVO(WRTagSearchColumn.CREATE_USER, columDB.getInfo()));
            vo.setCategoryName(transToWRColumnDetailVO(WRTagSearchColumn.CATEGORY_NAME, columDB.getInfo()));
            vo.setTag(transToWRColumnDetailVO(WRTagSearchColumn.TAG, columDB.getInfo()));
            vo.setStatus(transToWRColumnDetailVO(WRTagSearchColumn.STATUS, columDB.getInfo()));
            vo.setModifyTime(transToWRColumnDetailVO(WRTagSearchColumn.MODIFY_TIME, columDB.getInfo()));
            vo.setSeq(transToWRColumnDetailVO(WRTagSearchColumn.SEQ, columDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(收藏報表)
     *
     * @param userSid
     * @return
     */
    public WRFavoriteSearchColumnVO getWRFavoriteSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn favoriteColumnDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.FAVORITE_SEARCH, userSid);
        WRFavoriteSearchColumnVO vo = new WRFavoriteSearchColumnVO();
        if (favoriteColumnDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRFavoriteSearchColumn.INDEX));
            vo.setFavoriteDate(createDefaultColumnDetail(WRFavoriteSearchColumn.FAVORITE_TIME));
            vo.setTheme(createDefaultColumnDetail(WRFavoriteSearchColumn.THEME));
        } else {
            vo.setPageCount(String.valueOf(favoriteColumnDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRFavoriteSearchColumn.INDEX, favoriteColumnDB.getInfo()));
            vo.setFavoriteDate(transToWRColumnDetailVO(WRFavoriteSearchColumn.FAVORITE_TIME, favoriteColumnDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRFavoriteSearchColumn.THEME, favoriteColumnDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(部門草稿報表)
     *
     * @param userSid
     * @return
     */
    public WRDraftSearchColumnVO getWRDepDraftSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn draftColumDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.DEP_DRAFT_SEARCH, userSid);
        WRDraftSearchColumnVO vo = new WRDraftSearchColumnVO();
        if (draftColumDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRDraftSearchColumn.INDEX));
            vo.setCreateTime(createDefaultColumnDetail(WRDraftSearchColumn.DRAFT_TIME));
            vo.setTag(createDefaultColumnDetail(WRDraftSearchColumn.TAG));
            vo.setDepartment(createDefaultColumnDetail(WRDraftSearchColumn.DEPARTMENT));
            vo.setCreateUser(createDefaultColumnDetail(WRDraftSearchColumn.CREATE_USER));
            vo.setTheme(createDefaultColumnDetail(WRDraftSearchColumn.THEME));
            vo.setModifyTime(createDefaultColumnDetail(WRDraftSearchColumn.MODIFY_TIME));
        } else {
            vo.setPageCount(String.valueOf(draftColumDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRDraftSearchColumn.INDEX, draftColumDB.getInfo()));
            vo.setCreateTime(transToWRColumnDetailVO(WRDraftSearchColumn.DRAFT_TIME, draftColumDB.getInfo()));
            vo.setTag(transToWRColumnDetailVO(WRDraftSearchColumn.TAG, draftColumDB.getInfo()));
            vo.setDepartment(transToWRColumnDetailVO(WRDraftSearchColumn.DEPARTMENT, draftColumDB.getInfo()));
            vo.setCreateUser(transToWRColumnDetailVO(WRDraftSearchColumn.CREATE_USER, draftColumDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRDraftSearchColumn.THEME, draftColumDB.getInfo()));
            vo.setModifyTime(transToWRColumnDetailVO(WRDraftSearchColumn.MODIFY_TIME, draftColumDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(最後修改日期報表)
     *
     * @param userSid
     * @return
     */
    public WRLastModifySearchColumnVO getWRLastModifySearchColumnSetting(Integer userSid) {
        WRReportCustomColumn draftColumDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.LAST_MODIFY_SEARCH, userSid);
        WRLastModifySearchColumnVO vo = new WRLastModifySearchColumnVO();
        if (draftColumDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRLastModifySearchColumn.INDEX));
            vo.setCreateTime(createDefaultColumnDetail(WRLastModifySearchColumn.CREATE_TIME));
            vo.setDepartment(createDefaultColumnDetail(WRLastModifySearchColumn.DEPARTMENT));
            vo.setCreateUser(createDefaultColumnDetail(WRLastModifySearchColumn.CREATE_USER));
            vo.setTheme(createDefaultColumnDetail(WRLastModifySearchColumn.THEME));
            vo.setNo(createDefaultColumnDetail(WRLastModifySearchColumn.NO));
            vo.setLastModifyTime(createDefaultColumnDetail(WRLastModifySearchColumn.LASTCANMODIFY_DATE));
            vo.setMemo(createDefaultColumnDetail(WRLastModifySearchColumn.MEMO));
            vo.setStatus(createDefaultColumnDetail(WRLastModifySearchColumn.STATUS));
            vo.setEditType(createDefaultColumnDetail(WRLastModifySearchColumn.EDIT_TYPE));
        } else {
            vo.setPageCount(String.valueOf(draftColumDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRLastModifySearchColumn.INDEX, draftColumDB.getInfo()));
            vo.setCreateTime(transToWRColumnDetailVO(WRLastModifySearchColumn.CREATE_TIME, draftColumDB.getInfo()));
            vo.setDepartment(transToWRColumnDetailVO(WRLastModifySearchColumn.DEPARTMENT, draftColumDB.getInfo()));
            vo.setCreateUser(transToWRColumnDetailVO(WRLastModifySearchColumn.CREATE_USER, draftColumDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRLastModifySearchColumn.THEME, draftColumDB.getInfo()));
            vo.setNo(transToWRColumnDetailVO(WRLastModifySearchColumn.NO, draftColumDB.getInfo()));
            vo.setLastModifyTime(transToWRColumnDetailVO(WRLastModifySearchColumn.LASTCANMODIFY_DATE, draftColumDB.getInfo()));
            vo.setMemo(transToWRColumnDetailVO(WRLastModifySearchColumn.MEMO, draftColumDB.getInfo()));
            vo.setStatus(transToWRColumnDetailVO(WRLastModifySearchColumn.STATUS, draftColumDB.getInfo()));
            vo.setEditType(transToWRColumnDetailVO(WRLastModifySearchColumn.EDIT_TYPE, draftColumDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(個人草稿報表)
     *
     * @param userSid
     * @return
     */
    public WRDraftSearchColumnVO getWRPersonDraftSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn draftColumDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.PERSON_DRAFT_SEARCH, userSid);
        WRDraftSearchColumnVO vo = new WRDraftSearchColumnVO();
        if (draftColumDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRDraftSearchColumn.INDEX));
            vo.setCreateTime(createDefaultColumnDetail(WRDraftSearchColumn.DRAFT_TIME));
            vo.setTag(createDefaultColumnDetail(WRDraftSearchColumn.TAG));
            vo.setDepartment(createDefaultColumnDetail(WRDraftSearchColumn.DEPARTMENT));
            vo.setCreateUser(createDefaultColumnDetail(WRDraftSearchColumn.CREATE_USER));
            vo.setTheme(createDefaultColumnDetail(WRDraftSearchColumn.THEME));
            vo.setModifyTime(createDefaultColumnDetail(WRDraftSearchColumn.MODIFY_TIME));
        } else {
            vo.setPageCount(String.valueOf(draftColumDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRDraftSearchColumn.INDEX, draftColumDB.getInfo()));
            vo.setCreateTime(transToWRColumnDetailVO(WRDraftSearchColumn.DRAFT_TIME, draftColumDB.getInfo()));
            vo.setTag(transToWRColumnDetailVO(WRDraftSearchColumn.TAG, draftColumDB.getInfo()));
            vo.setDepartment(transToWRColumnDetailVO(WRDraftSearchColumn.DEPARTMENT, draftColumDB.getInfo()));
            vo.setCreateUser(transToWRColumnDetailVO(WRDraftSearchColumn.CREATE_USER, draftColumDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRDraftSearchColumn.THEME, draftColumDB.getInfo()));
            vo.setModifyTime(transToWRColumnDetailVO(WRDraftSearchColumn.MODIFY_TIME, draftColumDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(寄件備份報表)
     *
     * @param userSid
     * @return
     */
    public WRTransSendSearchColumnVO getWRTransSendSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn columDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.SEND_SEARCH, userSid);
        WRTransSendSearchColumnVO vo = new WRTransSendSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRSendTransSearchColumn.INDEX));
            vo.setFowardDep(createDefaultColumnDetail(WRSendTransSearchColumn.FOWARDDEP));
            vo.setFowardMember(createDefaultColumnDetail(WRSendTransSearchColumn.FOWARDMEMBER));
            vo.setTrace(createDefaultColumnDetail(WRSendTransSearchColumn.TRACE));
            vo.setSendType(createDefaultColumnDetail(WRSendTransSearchColumn.SEND_TYPE));
            vo.setReceviceUser(createDefaultColumnDetail(WRSendTransSearchColumn.RECEIVCE_USER));
            vo.setSendTime(createDefaultColumnDetail(WRSendTransSearchColumn.SEND_TIME));
            vo.setTag(createDefaultColumnDetail(WRSendTransSearchColumn.TAG));
            vo.setTheme(createDefaultColumnDetail(WRSendTransSearchColumn.THEME));
            vo.setModifyTime(createDefaultColumnDetail(WRSendTransSearchColumn.MODIFY_TIME));
            vo.setReaded(createDefaultColumnDetail(WRSendTransSearchColumn.READED));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRSendTransSearchColumn.INDEX, columDB.getInfo()));
            vo.setFowardDep(transToWRColumnDetailVO(WRSendTransSearchColumn.FOWARDDEP, columDB.getInfo()));
            vo.setFowardMember(transToWRColumnDetailVO(WRSendTransSearchColumn.FOWARDMEMBER, columDB.getInfo()));
            vo.setTrace(transToWRColumnDetailVO(WRSendTransSearchColumn.TRACE, columDB.getInfo()));
            vo.setSendType(transToWRColumnDetailVO(WRSendTransSearchColumn.SEND_TYPE, columDB.getInfo()));
            vo.setReceviceUser(transToWRColumnDetailVO(WRSendTransSearchColumn.RECEIVCE_USER, columDB.getInfo()));
            vo.setSendTime(transToWRColumnDetailVO(WRSendTransSearchColumn.SEND_TIME, columDB.getInfo()));
            vo.setTag(transToWRColumnDetailVO(WRSendTransSearchColumn.TAG, columDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRSendTransSearchColumn.THEME, columDB.getInfo()));
            vo.setModifyTime(transToWRColumnDetailVO(WRSendTransSearchColumn.MODIFY_TIME, columDB.getInfo()));
            vo.setReaded(transToWRColumnDetailVO(WRSendTransSearchColumn.READED, columDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(部門轉發報表)
     *
     * @param userSid
     * @return
     */
    public WRTransDepSearchColumnVO getWRTransDepSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn columDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.RECEVICE_DEP_SEARCH, userSid);
        WRTransDepSearchColumnVO vo = new WRTransDepSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.INDEX));
            vo.setFowardDep(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.FOWARDDEP));
            vo.setFowardMember(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.FOWARDMEMBER));
            vo.setTrace(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.TRACE));
            vo.setSendDep(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.SEND_DEP));
            vo.setSendUser(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.SEND_USER));
            vo.setSendTime(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.SEND_TIME));
            vo.setRecevieDep(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.RECEVICE_DEP));
            vo.setTag(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.TAG));
            vo.setTheme(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.THEME));
            vo.setModifyTime(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.MODIFY_TIME));
            vo.setReaded(createDefaultColumnDetail(WRReceviceDepTransSearchColumn.READED));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.INDEX, columDB.getInfo()));
            vo.setFowardDep(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.FOWARDDEP, columDB.getInfo()));
            vo.setFowardMember(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.FOWARDMEMBER, columDB.getInfo()));
            vo.setTrace(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.TRACE, columDB.getInfo()));
            vo.setSendDep(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.SEND_DEP, columDB.getInfo()));
            vo.setSendUser(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.SEND_USER, columDB.getInfo()));
            vo.setSendTime(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.SEND_TIME, columDB.getInfo()));
            vo.setRecevieDep(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.RECEVICE_DEP, columDB.getInfo()));
            vo.setTag(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.TAG, columDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.THEME, columDB.getInfo()));
            vo.setModifyTime(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.MODIFY_TIME, columDB.getInfo()));
            vo.setReaded(transToWRColumnDetailVO(WRReceviceDepTransSearchColumn.READED, columDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(收呈報報表)
     *
     * @param userSid
     * @return
     */
    public WRTransIncomeSearchColumnVO getWRTransIncomeReportSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn columDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.INCOME_REPORT_SEARCH, userSid);
        WRTransIncomeSearchColumnVO vo = new WRTransIncomeSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRIncomeTransSearchColumn.INDEX));
            vo.setFowardDep(createDefaultColumnDetail(WRIncomeTransSearchColumn.FOWARDDEP));
            vo.setFowardMember(createDefaultColumnDetail(WRIncomeTransSearchColumn.FOWARDMEMBER));
            vo.setTrace(createDefaultColumnDetail(WRIncomeTransSearchColumn.TRACE));
            vo.setSendDep(createDefaultColumnDetail(WRIncomeTransSearchColumn.SEND_DEP));
            vo.setSendUser(createDefaultColumnDetail(WRIncomeTransSearchColumn.SEND_USER));
            vo.setSendTime(createDefaultColumnDetail(WRIncomeTransSearchColumn.SEND_TIME));
            vo.setTag(createDefaultColumnDetail(WRIncomeTransSearchColumn.TAG));
            vo.setTheme(createDefaultColumnDetail(WRIncomeTransSearchColumn.THEME));
            vo.setModifyTime(createDefaultColumnDetail(WRIncomeTransSearchColumn.MODIFY_TIME));
            vo.setReaded(createDefaultColumnDetail(WRIncomeTransSearchColumn.READED));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRIncomeTransSearchColumn.INDEX, columDB.getInfo()));
            vo.setFowardDep(transToWRColumnDetailVO(WRIncomeTransSearchColumn.FOWARDDEP, columDB.getInfo()));
            vo.setFowardMember(transToWRColumnDetailVO(WRIncomeTransSearchColumn.FOWARDMEMBER, columDB.getInfo()));
            vo.setTrace(transToWRColumnDetailVO(WRIncomeTransSearchColumn.TRACE, columDB.getInfo()));
            vo.setSendDep(transToWRColumnDetailVO(WRIncomeTransSearchColumn.SEND_DEP, columDB.getInfo()));
            vo.setSendUser(transToWRColumnDetailVO(WRIncomeTransSearchColumn.SEND_USER, columDB.getInfo()));
            vo.setSendTime(transToWRColumnDetailVO(WRIncomeTransSearchColumn.SEND_TIME, columDB.getInfo()));
            vo.setTag(transToWRColumnDetailVO(WRIncomeTransSearchColumn.TAG, columDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRIncomeTransSearchColumn.THEME, columDB.getInfo()));
            vo.setModifyTime(transToWRColumnDetailVO(WRIncomeTransSearchColumn.MODIFY_TIME, columDB.getInfo()));
            vo.setReaded(transToWRColumnDetailVO(WRIncomeTransSearchColumn.READED, columDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(收指示報表)
     *
     * @param userSid
     * @return
     */
    public WRTransIncomeSearchColumnVO getWRTransIncomeInstructionSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn columDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.INCOME_INSTRUCTION_SEARCH, userSid);
        WRTransIncomeSearchColumnVO vo = new WRTransIncomeSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRIncomeTransSearchColumn.INDEX));
            vo.setFowardDep(createDefaultColumnDetail(WRIncomeTransSearchColumn.FOWARDDEP));
            vo.setFowardMember(createDefaultColumnDetail(WRIncomeTransSearchColumn.FOWARDMEMBER));
            vo.setTrace(createDefaultColumnDetail(WRIncomeTransSearchColumn.TRACE));
            vo.setSendDep(createDefaultColumnDetail(WRIncomeTransSearchColumn.SEND_DEP));
            vo.setSendUser(createDefaultColumnDetail(WRIncomeTransSearchColumn.SEND_USER));
            vo.setSendTime(createDefaultColumnDetail(WRIncomeTransSearchColumn.SEND_TIME));
            vo.setTag(createDefaultColumnDetail(WRIncomeTransSearchColumn.TAG));
            vo.setTheme(createDefaultColumnDetail(WRIncomeTransSearchColumn.THEME));
            vo.setModifyTime(createDefaultColumnDetail(WRIncomeTransSearchColumn.MODIFY_TIME));
            vo.setReaded(createDefaultColumnDetail(WRIncomeTransSearchColumn.READED));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRIncomeTransSearchColumn.INDEX, columDB.getInfo()));
            vo.setFowardDep(transToWRColumnDetailVO(WRIncomeTransSearchColumn.FOWARDDEP, columDB.getInfo()));
            vo.setFowardMember(transToWRColumnDetailVO(WRIncomeTransSearchColumn.FOWARDMEMBER, columDB.getInfo()));
            vo.setTrace(transToWRColumnDetailVO(WRIncomeTransSearchColumn.TRACE, columDB.getInfo()));
            vo.setSendDep(transToWRColumnDetailVO(WRIncomeTransSearchColumn.SEND_DEP, columDB.getInfo()));
            vo.setSendUser(transToWRColumnDetailVO(WRIncomeTransSearchColumn.SEND_USER, columDB.getInfo()));
            vo.setSendTime(transToWRColumnDetailVO(WRIncomeTransSearchColumn.SEND_TIME, columDB.getInfo()));
            vo.setTag(transToWRColumnDetailVO(WRIncomeTransSearchColumn.TAG, columDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRIncomeTransSearchColumn.THEME, columDB.getInfo()));
            vo.setModifyTime(transToWRColumnDetailVO(WRIncomeTransSearchColumn.MODIFY_TIME, columDB.getInfo()));
            vo.setReaded(transToWRColumnDetailVO(WRIncomeTransSearchColumn.READED, columDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(收個人報表)
     *
     * @param userSid
     * @return
     */
    public WRTransIncomeSearchColumnVO getWRTransIncomeMemeberSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn columDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.INCOME_MEMBER_SEARCH, userSid);
        WRTransIncomeSearchColumnVO vo = new WRTransIncomeSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRIncomeTransSearchColumn.INDEX));
            vo.setFowardDep(createDefaultColumnDetail(WRIncomeTransSearchColumn.FOWARDDEP));
            vo.setFowardMember(createDefaultColumnDetail(WRIncomeTransSearchColumn.FOWARDMEMBER));
            vo.setTrace(createDefaultColumnDetail(WRIncomeTransSearchColumn.TRACE));
            vo.setSendDep(createDefaultColumnDetail(WRIncomeTransSearchColumn.SEND_DEP));
            vo.setSendUser(createDefaultColumnDetail(WRIncomeTransSearchColumn.SEND_USER));
            vo.setSendTime(createDefaultColumnDetail(WRIncomeTransSearchColumn.SEND_TIME));
            vo.setTag(createDefaultColumnDetail(WRIncomeTransSearchColumn.TAG));
            vo.setTheme(createDefaultColumnDetail(WRIncomeTransSearchColumn.THEME));
            vo.setModifyTime(createDefaultColumnDetail(WRIncomeTransSearchColumn.MODIFY_TIME));
            vo.setReaded(createDefaultColumnDetail(WRIncomeTransSearchColumn.READED));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRIncomeTransSearchColumn.INDEX, columDB.getInfo()));
            vo.setFowardDep(transToWRColumnDetailVO(WRIncomeTransSearchColumn.FOWARDDEP, columDB.getInfo()));
            vo.setFowardMember(transToWRColumnDetailVO(WRIncomeTransSearchColumn.FOWARDMEMBER, columDB.getInfo()));
            vo.setTrace(transToWRColumnDetailVO(WRIncomeTransSearchColumn.TRACE, columDB.getInfo()));
            vo.setSendDep(transToWRColumnDetailVO(WRIncomeTransSearchColumn.SEND_DEP, columDB.getInfo()));
            vo.setSendUser(transToWRColumnDetailVO(WRIncomeTransSearchColumn.SEND_USER, columDB.getInfo()));
            vo.setSendTime(transToWRColumnDetailVO(WRIncomeTransSearchColumn.SEND_TIME, columDB.getInfo()));
            vo.setTag(transToWRColumnDetailVO(WRIncomeTransSearchColumn.TAG, columDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRIncomeTransSearchColumn.THEME, columDB.getInfo()));
            vo.setModifyTime(transToWRColumnDetailVO(WRIncomeTransSearchColumn.MODIFY_TIME, columDB.getInfo()));
            vo.setReaded(transToWRColumnDetailVO(WRIncomeTransSearchColumn.READED, columDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(追蹤報表)
     *
     * @param userSid
     * @return
     */
    public WRTraceSearchColumnVO getWRTraceSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn columDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.TRACE_SEARCH, userSid);
        WRTraceSearchColumnVO vo = new WRTraceSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRTraceSearchColumn.INDEX));
            vo.setFowardDep(createDefaultColumnDetail(WRTraceSearchColumn.FOWARDDEP));
            vo.setFowardMember(createDefaultColumnDetail(WRTraceSearchColumn.FOWARDMEMBER));
            vo.setTrace(createDefaultColumnDetail(WRTraceSearchColumn.TRACE));
            vo.setTraceType(createDefaultColumnDetail(WRTraceSearchColumn.TRACE_TYPE));
            vo.setTag(createDefaultColumnDetail(WRTraceSearchColumn.TAG));
            vo.setTraceNotifyDate(createDefaultColumnDetail(WRTraceSearchColumn.TRACE_NOTIFY_TIME));
            vo.setTraceCreateUser(createDefaultColumnDetail(WRTraceSearchColumn.CREATE_USER));
            vo.setTraceStatus(createDefaultColumnDetail(WRTraceSearchColumn.TRACE_STATUS));
            vo.setTraceMemo(createDefaultColumnDetail(WRTraceSearchColumn.TRACE_CONTENT));
            vo.setTheme(createDefaultColumnDetail(WRTraceSearchColumn.THEME));
            vo.setModifyTime(createDefaultColumnDetail(WRTraceSearchColumn.MODIFY_TIME));
            vo.setTraceCreateDate(createDefaultColumnDetail(WRTraceSearchColumn.TRACE_TIME));
            vo.setReaded(createDefaultColumnDetail(WRTraceSearchColumn.READED));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRTraceSearchColumn.INDEX, columDB.getInfo()));
            vo.setFowardDep(transToWRColumnDetailVO(WRTraceSearchColumn.FOWARDDEP, columDB.getInfo()));
            vo.setFowardMember(transToWRColumnDetailVO(WRTraceSearchColumn.FOWARDMEMBER, columDB.getInfo()));
            vo.setTrace(transToWRColumnDetailVO(WRTraceSearchColumn.TRACE, columDB.getInfo()));
            vo.setTraceType(transToWRColumnDetailVO(WRTraceSearchColumn.TRACE_TYPE, columDB.getInfo()));
            vo.setTag(transToWRColumnDetailVO(WRTraceSearchColumn.TAG, columDB.getInfo()));
            vo.setTraceNotifyDate(transToWRColumnDetailVO(WRTraceSearchColumn.TRACE_NOTIFY_TIME, columDB.getInfo()));
            vo.setTraceCreateUser(transToWRColumnDetailVO(WRTraceSearchColumn.CREATE_USER, columDB.getInfo()));
            vo.setTraceStatus(transToWRColumnDetailVO(WRTraceSearchColumn.TRACE_STATUS, columDB.getInfo()));
            vo.setTraceMemo(transToWRColumnDetailVO(WRTraceSearchColumn.TRACE_CONTENT, columDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRTraceSearchColumn.THEME, columDB.getInfo()));
            vo.setModifyTime(transToWRColumnDetailVO(WRTraceSearchColumn.MODIFY_TIME, columDB.getInfo()));
            vo.setTraceCreateDate(transToWRColumnDetailVO(WRTraceSearchColumn.TRACE_TIME, columDB.getInfo()));
            vo.setReaded(transToWRColumnDetailVO(WRTraceSearchColumn.READED, columDB.getInfo()));
        }
        return vo;
    }

    /** *
     * 取得欄位介面物件(工作報告報表)
     *
     * @param userSid
     * @return
     */
    public WRWorkReportSearchColumnVO getWRWorkReportSearchColumnSetting(Integer userSid) {
        WRReportCustomColumn columDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(WRReportCustomColumnUrlType.WORK_REPORT_SEARCH, userSid);
        WRWorkReportSearchColumnVO vo = new WRWorkReportSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(createDefaultColumnDetail(WRWorkReportSearchColumn.INDEX));
            vo.setFowardDep(createDefaultColumnDetail(WRWorkReportSearchColumn.FOWARDDEP));
            vo.setFowardMember(createDefaultColumnDetail(WRWorkReportSearchColumn.FOWARDMEMBER));
            vo.setTrace(createDefaultColumnDetail(WRWorkReportSearchColumn.TRACE));
            vo.setCreateTime(createDefaultColumnDetail(WRWorkReportSearchColumn.CREATE_TIME));
            vo.setTag(createDefaultColumnDetail(WRWorkReportSearchColumn.TAG));
            vo.setDepartment(createDefaultColumnDetail(WRWorkReportSearchColumn.DEPARTMENT));
            vo.setCreateUser(createDefaultColumnDetail(WRWorkReportSearchColumn.CREATE_USER));
            vo.setTheme(createDefaultColumnDetail(WRWorkReportSearchColumn.THEME));
            vo.setStatus(createDefaultColumnDetail(WRWorkReportSearchColumn.STATUS));
            vo.setNo(createDefaultColumnDetail(WRWorkReportSearchColumn.NO));
            vo.setModifyTime(createDefaultColumnDetail(WRWorkReportSearchColumn.MODIFY_TIME));
            vo.setReaded(createDefaultColumnDetail(WRWorkReportSearchColumn.READED));
            vo.setReadRecipt(createDefaultColumnDetail(WRWorkReportSearchColumn.READRECIPT));
            vo.setReadRecipted(createDefaultColumnDetail(WRWorkReportSearchColumn.READRECIPT_SENDED));
            vo.setEditType(createDefaultColumnDetail(WRWorkReportSearchColumn.EDITTYPE));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(transToWRColumnDetailVO(WRWorkReportSearchColumn.INDEX, columDB.getInfo()));

            vo.setFowardDep(transToWRColumnDetailVO(WRWorkReportSearchColumn.FOWARDDEP, columDB.getInfo()));
            vo.setFowardMember(transToWRColumnDetailVO(WRWorkReportSearchColumn.FOWARDMEMBER, columDB.getInfo()));
            vo.setTrace(transToWRColumnDetailVO(WRWorkReportSearchColumn.TRACE, columDB.getInfo()));
            vo.setCreateTime(transToWRColumnDetailVO(WRWorkReportSearchColumn.CREATE_TIME, columDB.getInfo()));

            vo.setTag(transToWRColumnDetailVO(WRWorkReportSearchColumn.TAG, columDB.getInfo()));
            vo.setDepartment(transToWRColumnDetailVO(WRWorkReportSearchColumn.DEPARTMENT, columDB.getInfo()));
            vo.setCreateUser(transToWRColumnDetailVO(WRWorkReportSearchColumn.CREATE_USER, columDB.getInfo()));
            vo.setTheme(transToWRColumnDetailVO(WRWorkReportSearchColumn.THEME, columDB.getInfo()));

            vo.setStatus(transToWRColumnDetailVO(WRWorkReportSearchColumn.STATUS, columDB.getInfo()));
            vo.setNo(transToWRColumnDetailVO(WRWorkReportSearchColumn.NO, columDB.getInfo()));

            vo.setModifyTime(transToWRColumnDetailVO(WRWorkReportSearchColumn.MODIFY_TIME, columDB.getInfo()));
            vo.setReaded(transToWRColumnDetailVO(WRWorkReportSearchColumn.READED, columDB.getInfo()));
            vo.setReadRecipt(transToWRColumnDetailVO(WRWorkReportSearchColumn.READRECIPT, columDB.getInfo()));
            vo.setEditType(transToWRColumnDetailVO(WRWorkReportSearchColumn.EDITTYPE, columDB.getInfo()));
            vo.setReadRecipted(transToWRColumnDetailVO(WRWorkReportSearchColumn.READRECIPT_SENDED, columDB.getInfo()));
        }
        return vo;
    }

    public void saveWRColumnWidthSetting(WRReportCustomColumnUrlType wrReportCustomColumnUrlType,
            Integer userSid, List<CustomColumnDetailTo> customColumnDetailTo, List<WRColumn> wRColumns, String pageCount) {
        WRReportCustomColumn columDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(wrReportCustomColumnUrlType, userSid);
        if (columDB == null) {
            columDB = new WRReportCustomColumn();
            columDB.setUrl(wrReportCustomColumnUrlType);
            columDB.setPagecnt(Integer.valueOf(pageCount));
        }
        WRReportCustomColumnInfo wci = new WRReportCustomColumnInfo();
        wci.setWrReportCustomColumnInfoTos(settingWidthColumn(columDB.getInfo(), wRColumns, customColumnDetailTo, wrReportCustomColumnUrlType));
        columDB.setInfo(wci);
        wrReportCustomColumnManager.save(columDB, userSid);
    }

    public List<CustomCulumnVO> getSelectWRColumn(WRReportCustomColumnUrlType wrReportCustomColumnUrlType, Integer userSid) {
        WRReportCustomColumn draftColumDB = wrReportCustomColumnManager.getWRReportCustomColumnByUrlAndUserSid(wrReportCustomColumnUrlType, userSid);
        if (draftColumDB == null || draftColumDB.getInfo() == null || draftColumDB.getInfo().getWrReportCustomColumnInfoTos() == null
                || draftColumDB.getInfo().getWrReportCustomColumnInfoTos().size() == 0) {
            return null;
        }
        List<CustomCulumnVO> selectColumns = Lists.newArrayList();
        draftColumDB.getInfo().getWrReportCustomColumnInfoTos().forEach(item -> {
            CustomCulumnVO cv = new CustomCulumnVO(true, item.getName(), item.getWidth());
            selectColumns.add(cv);
        });
        return selectColumns;
    }

    private String transToStyleCss(String width) {
        if (!Strings.isNullOrEmpty(width)) {
            return "width: " + width + "px";
        }
        return "";
    }

    public WRColumnDetailVO transToWRColumnDetailVO(WRColumn wRColumn, WRReportCustomColumnInfo wrReportCustomColumnInfo) {
        if (wrReportCustomColumnInfo != null && wrReportCustomColumnInfo.getWrReportCustomColumnInfoTos() != null
                && wrReportCustomColumnInfo.getWrReportCustomColumnInfoTos().size() > 0) {
            //搜尋資料庫是否有該欄位記錄長度資料
            List<WRReportCustomColumnInfoTo> wrReportCustomColumnInfoTo = wrReportCustomColumnInfo.getWrReportCustomColumnInfoTos().stream()
                    .filter(each -> each.getName().equals(wRColumn.getVal()))
                    .collect(Collectors.toList());
            if (wrReportCustomColumnInfoTo != null && wrReportCustomColumnInfoTo.size() > 0) {
                //若資料庫有值代表,有紀錄需顯示,還需確認改欄位是否可改寬度,若不可改寬度,需顯示預設
                return new WRColumnDetailVO(wRColumn.getVal(), true, (wRColumn.getCanModifyWidth()) ? transToStyleCss(wrReportCustomColumnInfoTo.get(0).getWidth()) : transToStyleCss(wRColumn.getDefaultWidth()));
            } else if (wRColumn.getCanSelectItem()) {
                //若該欄位是可被選擇是否不顯示,在資料庫欄位未發現,代表使用者有變更將其變為不顯示
                return new WRColumnDetailVO(wRColumn.getVal(), false, "");
            } else {
                //若在資料庫欄位未發現,但該欄位是不可被選擇不顯示的,代表須顯示,寬度為預設
                return new WRColumnDetailVO(wRColumn.getVal(), true, transToStyleCss(wRColumn.getDefaultWidth()));
            }
        } else {
            return new WRColumnDetailVO(wRColumn.getVal(), wRColumn.getDefaultShow(), transToStyleCss(wRColumn.getDefaultWidth()));
        }
    }

}
