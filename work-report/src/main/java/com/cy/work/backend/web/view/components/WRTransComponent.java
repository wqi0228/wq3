/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.TransUpdateCallBack;
import com.cy.work.backend.web.util.pf.DisplayController;
import java.io.Serializable;
import lombok.Getter;

/**
 * 轉寄功能物件
 *
 * @author brain0925_liao
 */
public class WRTransComponent implements Serializable {

    
    private static final long serialVersionUID = 8190236269316580759L;
    /** 轉寄部門功能物件 */
    @Getter
    private WRTransDepComponent transDepComponent;
    /** 轉寄個人功能物件 */
    @Getter
    private WRTransPersonComponent transPersonComponent;
    /** 是否挑選轉寄部門 */
    @Getter
    private boolean showDep = true;
    /** 工作報告Sid */
    private String wr_Sid;
    /** 工作報告單號 */
    private String wr_no;
    /**轉寄更新CallBack*/
    private TransUpdateCallBack transUpdateCallBack;

    public WRTransComponent(Integer loginUserSid, Integer companySid, TransUpdateCallBack transUpdateCallBack, MessageCallBack messageCallBack) {
        this.transUpdateCallBack = transUpdateCallBack;
        transDepComponent = new WRTransDepComponent(loginUserSid, companySid, updateCallBack, closeCallBack, messageCallBack);
        transPersonComponent = new WRTransPersonComponent(loginUserSid, messageCallBack, updateCallBack, closeCallBack);
    }

    public final TransUpdateCallBack updateCallBack = new TransUpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 6041229825022367485L;

        @Override
        public void doUpdateData() {
            clear();
            transUpdateCallBack.doUpdateData();
            DisplayController.getInstance().hidePfWidgetVar("dlgForward");
        }

    };

    public void clear() {
        transDepComponent.clear();
        transPersonComponent.clear();
    }

    public final TransUpdateCallBack closeCallBack = new TransUpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = -9006750248267533891L;

        @Override
        public void doUpdateData() {
            clear();
            DisplayController.getInstance().hidePfWidgetVar("dlgForward");
        }
    };

    public void loadData(String wr_Sid, String wr_no) {
        this.wr_Sid = wr_Sid;
        this.wr_no = wr_no;
        this.showDep = true;
        transDepComponent.loadData(wr_Sid, wr_no);
        DisplayController.getInstance().update("opForward");
    }

    public void changeToDep() {
        showDep = true;
        transPersonComponent.clear();
        transDepComponent.loadData(wr_Sid, wr_no);
    }

    public void changeToPerson() {
        this.showDep = false;
        transDepComponent.clear();
        transPersonComponent.loadData(wr_Sid, wr_no);
    }

}
