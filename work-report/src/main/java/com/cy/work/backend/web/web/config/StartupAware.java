/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.config;

import com.cy.work.backend.web.logic.cache.WRHomepageFavoriteCache;
import com.cy.work.backend.web.logic.cache.WRTagCache;
import com.cy.work.backend.web.logic.cache.WrBaseDepAccessInfoCache;
import com.cy.work.backend.web.logic.cache.WrBasePersonAccessInfoCache;
import com.cy.work.backend.web.logic.manager.WRNoGenericComponents;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 *
 * @author brain0925_liao
 */
@Component
public class StartupAware implements ApplicationContextAware {

    @Autowired
    private WRNoGenericComponents wRNoGenericComponents;
    @Autowired
    private WRHomepageFavoriteCache wRHomepageFavoriteCache;
    @Autowired
    private WRTagCache wRTagCache;
    @Autowired
    private WrBaseDepAccessInfoCache wrBaseDepAccessInfoCache;
    @Autowired
    private WrBasePersonAccessInfoCache wrBasePersonAccessInfoCache;

    @Override
    public void setApplicationContext(ApplicationContext actx) throws BeansException {
        wRNoGenericComponents.start();
        wRHomepageFavoriteCache.start();
        wRTagCache.start();
        wrBaseDepAccessInfoCache.start();
        wrBasePersonAccessInfoCache.start();
    }

}
