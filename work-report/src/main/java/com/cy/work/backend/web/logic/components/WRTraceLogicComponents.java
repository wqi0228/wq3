/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.manager.WRTraceManager;
import com.cy.work.backend.web.logic.manager.WorkSettingUserManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.view.vo.WRTraceVO;
import com.cy.work.backend.web.vo.WRTrace;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.WRTraceType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

/**
 * 追蹤邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRTraceLogicComponents implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = -3623136111834031199L;

    @Autowired
    private WRTraceManager wRTraceManager;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;

    private static WRTraceLogicComponents instance;

    public static WRTraceLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRTraceLogicComponents.instance = this;
    }

    /**
     * 取得追蹤By 追蹤Sid
     *
     * @param sid 追蹤Sid
     * @return
     */
    public WRTrace getWRTraceBySid(String sid) {
        return wRTraceManager.getWRTraceBySid(sid);
    }

    /**
     * 取得追蹤介面物件By 追蹤Sid
     *
     * @param sid 追蹤Sid
     * @return
     */
    public WRTraceVO getWRTraceVOBySid(Integer userSid, String sid) {
        WRTrace wr = wRTraceManager.getWRTraceBySid(sid);
        User user = null;
        if (wr.getUpdate_usr() != null) {
            user = workSettingUserManager.findBySID(wr.getUpdate_usr());
        } else {
            user = workSettingUserManager.findBySID(wr.getCreate_usr());
        }
        String dateTime = "";
        if (wr.getUpdate_dt() != null) {
            dateTime = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wr.getUpdate_dt());
        } else {
            dateTime = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wr.getCreate_dt());
        }
        String userInfo = user.getPrimaryOrg().getName() + "-" + user.getName();
        return new WRTraceVO(wr.getSid(), wr.getWr_trace_type().getVal(), userInfo, dateTime, wr.getWr_trace_content_css(), wr.getCreate_usr().equals(userSid));
    }

    /**
     * 取得追蹤介面物件List By 工作報告Sid
     *
     * @param userSid 登入者Sid
     * @param wr_id 工作報告Sid
     * @param wRTraceType 僅蒐尋特定追蹤類型(若傳入null,會全部搜尋)
     * @return
     */
    public List<WRTraceVO> getWRTraceVOsByWrIDAndWRTraceType(Integer userSid, String wr_id, WRTraceType wRTraceType) {
        try {
            List<WRTraceVO> viewTrace = Lists.newArrayList();
            List<WRTrace> results = wRTraceManager.getWRTracesByWRSid(wr_id);
            results.forEach(item -> {
                if (wRTraceType != null && !wRTraceType.equals(item.getWr_trace_type())) {
                    return;
                }
                //回覆的資訊僅顯示在回覆頁籤中即可,故若顯示全部,需額外濾掉回覆
                if (wRTraceType == null && item.getWr_trace_type().equals(WRTraceType.REPLY)) {
                    return;
                }

                User user = null;
                if (item.getUpdate_usr() != null) {
                    user = workSettingUserManager.findBySID(item.getUpdate_usr());
                } else {
                    user = workSettingUserManager.findBySID(item.getCreate_usr());
                }
                String dateTime = "";
                if (item.getUpdate_dt() != null) {
                    dateTime = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdate_dt());
                } else {
                    dateTime = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getCreate_dt());
                }
                String userInfo = user.getPrimaryOrg().getOrgNameWithParentIfDuplicate() + "-" + user.getName();
                viewTrace.add(new WRTraceVO(item.getSid(), item.getWr_trace_type().getVal(), userInfo, dateTime, HtmlUtils.htmlUnescape(item.getWr_trace_content_css()), item.getCreate_usr().equals(userSid)));

            });
            return viewTrace;
        } catch (Exception e) {
            log.error("getWRTraceVOsByWrID Error ", e);
        }
        return Lists.newArrayList();
    }

}
