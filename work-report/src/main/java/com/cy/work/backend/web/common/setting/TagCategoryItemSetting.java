/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.setting;

import com.cy.work.backend.web.vo.enums.WRTagCategoryType;
import com.google.common.collect.Lists;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * 狀態靜態資料
 *
 * @author brain0925_liao
 */
public class TagCategoryItemSetting {

    /** 狀態選項List */
    private static List<SelectItem> tagCategoryItems;

    /** 取得狀態選項List */
    public static List<SelectItem> getTagCategoryItems() {
        return loadData();
    }

    /** 取得狀態選項資料,若記憶體已有將不再行建立 */
    private static List<SelectItem> loadData() {
        if (tagCategoryItems != null) {
            return tagCategoryItems;
        }
        tagCategoryItems = Lists.newArrayList();
        for (WRTagCategoryType item : WRTagCategoryType.values()) {

            SelectItem si = new SelectItem(item.name(), item.getVal());

            tagCategoryItems.add(si);
        }
        return tagCategoryItems;
    }

}
