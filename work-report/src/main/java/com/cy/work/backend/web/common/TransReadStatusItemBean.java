/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common;

import com.cy.work.backend.web.common.setting.TransReadStatusSetting;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 轉寄已未讀狀態Item Bean
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@ManagedBean
public class TransReadStatusItemBean  implements Serializable{

    private static final long serialVersionUID = -7072123483314014425L;

    /** 取得 轉寄已未讀狀態SelectItems */
    public List<SelectItem> getReadStatusItems() {
        return TransReadStatusSetting.getTransReadStatusItems();
    }
    
}
