/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author brain0925_liao
 */
public class CustomCulumnVO implements Serializable {

    
    private static final long serialVersionUID = -5864047618810986057L;
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CustomCulumnVO other = (CustomCulumnVO) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    public CustomCulumnVO(boolean selected, String name,String width) {
        this.selected = selected;
        this.name = name;
        this.width = width;
    }

    @Setter
    @Getter
    private boolean selected;
    @Setter
    @Getter
    private String name;
    @Setter
    @Getter
    private String width;
}
