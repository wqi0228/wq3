/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.ReLoadCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WRGroupSetupPrivateLogicComponents;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRGroupVO;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.DualListModel;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public class  WRGroupSetupPrivateMaitainComponent implements Serializable {


    private static final long serialVersionUID = -5724030074838490446L;
    @Getter
    @Setter
    private String statusID;
    @Getter
    @Setter
    private String groupName;
    @Getter
    @Setter
    private String note;
    @Getter
    @Setter
    private String filterNameValue;

    private String groupID;

    @Getter
    @Setter
    private DualListModel<UserViewVO> pickUsers;

    private List<UserViewVO> leftViewVO;

    private Integer loginUserSid;

    private MessageCallBack messageCallBack;

    private ReLoadCallBack reLoadCallBack;

    private List<UserViewVO> allCompUser;

    private List<Org> selectOrgs;

    private DepTreeComponent depTreeComponent;
    @Getter
    private String wrGroup_dep;
    @Getter
    private String wrGroup_deptUnitDlg;
    @Getter
    private String groupMaintainComponent_pickPanel;
    @Getter
    private String group_setup_private_setting_dlg_wv;

    public WRGroupSetupPrivateMaitainComponent(String group_setup_private_setting_dlg_wv,
            String groupMaintainComponent_pickPanel,
            String wrGroup_deptUnitDlg,
            String wrGroup_dep,
            Integer loginUserSid,
            MessageCallBack messageCallBack, ReLoadCallBack reLoadCallBack) {
        this.loginUserSid = loginUserSid;
        this.messageCallBack = messageCallBack;
        this.reLoadCallBack = reLoadCallBack;
        this.wrGroup_dep = wrGroup_dep;
        this.groupMaintainComponent_pickPanel = groupMaintainComponent_pickPanel;
        this.group_setup_private_setting_dlg_wv = group_setup_private_setting_dlg_wv;
        this.wrGroup_deptUnitDlg = wrGroup_deptUnitDlg;
        depTreeComponent = new DepTreeComponent();
        initOrgTree();
        initPickUsers();
    }

    private void initPickUsers() {
        pickUsers = new DualListModel<>();
    }

    public void clear() {
        initPickUsers();
        initOrgTree();
        if (leftViewVO != null) {
            leftViewVO.clear();
            leftViewVO = null;
        }
        allCompUser = null;
        DisplayController.getInstance().update(wrGroup_dep);
    }

    private void initOrgTree() {
        Org group = new Org(1);
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectOrgs = Lists.newArrayList();
        depTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() {
        return depTreeComponent.getMultipleDepTreeManager();
    }

    public void loadDepTree() {
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        Org group = new Org(user.getPrimaryOrg().getCompanySid());
        // 修改,故切換顯示停用部門時,需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        List<Org> allOrg = OrgLogicComponents.getInstance().findOrgsByCompanySid(group.getSid());
        depTreeComponent.init(allOrg, allOrg,
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    public void doSelectOrg() {
        try {
            selectOrgs = depTreeComponent.getSelectOrg();
            if (selectOrgs == null || selectOrgs.isEmpty()) {
                return;
            }
            // leftViewVO.clear();

            List<UserViewVO> selectDepUserView = Lists.newArrayList();
            selectOrgs.forEach(item -> {
                selectDepUserView.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
            });
            pickUsers.setSource(removeTargetUserView(selectDepUserView, pickUsers.getTarget()));

        } catch (Exception e) {
            log.error("doSelectOrg", e);
            messageCallBack.showMessage(e.getMessage());
        }
        DisplayController.getInstance().update(groupMaintainComponent_pickPanel);
        DisplayController.getInstance().hidePfWidgetVar(wrGroup_deptUnitDlg);
    }

    public void save() {
        try {
            WRGroupVO wrGroupVO = new WRGroupVO(groupID, groupName, pickUsers.getTarget(), statusID, note);
            WRGroupSetupPrivateLogicComponents.getInstance().saveWrGroupVO(wrGroupVO, loginUserSid);
            clear();
            reLoadCallBack.onReload();
            DisplayController.getInstance().hidePfWidgetVar(group_setup_private_setting_dlg_wv);
        } catch (Exception e) {
            log.error("save", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void loadData(WRGroupVO wrGroupVO) {
        this.filterNameValue = "";
        allCompUser = Lists.newArrayList();
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        Integer compSid = user.getPrimaryOrg().getCompanySid();
        try {
            OrgLogicComponents.getInstance().findOrgsByCompanySid(compSid).stream()
                    .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                    .collect(Collectors.toList()).forEach(item -> {
                        allCompUser.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
                    });
        } catch (Exception e) {
            log.error(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }
        leftViewVO = Lists.newArrayList();
        selectOrgs = Lists.newArrayList();
        Org baseOrg = WkOrgUtils.prepareBaseDep(user.getPrimaryOrg().getSid(), OrgLevel.MINISTERIAL);
        selectOrgs.add(baseOrg);
        List<Org> allChildOrg = OrgLogicComponents.getInstance().getAllChildOrg(baseOrg);
        selectOrgs.addAll(allChildOrg);
        leftViewVO.addAll(WRUserLogicComponents.getInstance().findByDepSid(baseOrg.getSid()));
        allChildOrg.forEach(item -> {
            leftViewVO.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
        });
        List<UserViewVO> right = Lists.newArrayList();
        filterNameValue = "";
        if (wrGroupVO == null) {
            statusID = "";
            groupName = "";
            note = "";
            groupID = "";
        } else {
            statusID = wrGroupVO.getStatusID();
            groupName = wrGroupVO.getGroup_name();
            note = wrGroupVO.getNote();
            groupID = wrGroupVO.getGroup_sid();
            right = wrGroupVO.getUserViewVO();
        }
        pickUsers.setSource(removeTargetUserView(leftViewVO, right));
        pickUsers.setTarget(right);
    }

    public void filterName() {
        if (Strings.isNullOrEmpty(filterNameValue)) {
            pickUsers.setSource(removeTargetUserView(allCompUser, pickUsers.getTarget()));
        } else {
            pickUsers.setSource(removeTargetUserView(allCompUser.stream()
                    .filter(each -> each.getName().toLowerCase().contains(filterNameValue.toLowerCase()))
                    .collect(Collectors.toList()), pickUsers.getTarget()));
        }
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source, List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(item -> {
            if (!target.contains(item)) {
                filterSource.add(item);
            }
        });
        return filterSource;
    }

}
