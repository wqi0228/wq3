/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.faces.convert.ConverterException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
@Controller
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class StringFilterMBean {

    public StringFilterMBean() {
    }

    /**
     * 不區分大小寫進行開頭字串過濾 （使用方法：&lt;p:column
     * filterFunction="#{StringFilterMBean.filterStartsWithAndInsensitive}"
     * ...&gt;）
     *
     * @param value 來源資料 (由primefaces 自動帶入)
     * @param filter 過濾的字串 (由primefaces 自動帶入)
     * @param locale 地理區域 由primefaces 自動帶入
     * @return
     */
    public boolean filterStartsWithAndInsensitive(Object value, Object filter, Locale locale) {
        if (filter == null || "".equals(filter.toString().trim())) {
            return true;
        }
        if (value == null) {
            return false;
        }
        try {
            String filterText = ((String) filter).trim().toLowerCase();
            String valueText = ((String) value).trim().toLowerCase();
            return valueText.startsWith(filterText);
        } catch (ConverterException e) {
            log.error("filterStartsWithAndInsensitive", e);
        }
        return false;
    }

    /**
     * 不區分大小寫進行包含字串過濾 （使用方法：&lt;p:column
     * filterFunction="#{StringFilterMBean.filterContainsAndInsensitive}"
     * ...&gt;）
     *
     * @param value 來源資料 (由primefaces 自動帶入)
     * @param filter 過濾的字串 (由primefaces 自動帶入)
     * @param locale 地理區域 由primefaces 自動帶入
     * @return
     */
    public boolean filterContainsAndInsensitive(Object value, Object filter, Locale locale) {
        if (filter == null || "".equals(filter.toString().trim())) {
            return true;
        }
        if (value == null) {
            return false;
        }
        try {
            String filterText = ((String) filter).trim().toLowerCase();
            String valueText = ((String) value).trim().toLowerCase();
            return valueText.contains(filterText);
         } catch (ConverterException e) {
            log.error("filterContainsAndInsensitive", e);
        }
        return false;
    }

    /**
     * 不區分大小寫進行結尾字串過濾（使用方法：&lt;p:column
     * filterFunction="#{StringFilterMBean.filterEndsWithAndInsensitive}"
     * ...&gt;）
     *
     * @param value 來源資料 (由primefaces 自動帶入)
     * @param filter 過濾的字串 (由primefaces 自動帶入)
     * @param locale 地理區域 由primefaces 自動帶入
     * @return
     */
    public boolean filterEndsWithAndInsensitive(Object value, Object filter, Locale locale) {
        if (filter == null || "".equals(filter.toString().trim())) {
            return true;
        }
        if (value == null) {
            return false;
        }
        try {
            String filterText = ((String) filter).trim().toLowerCase();
            String valueText = ((String) value).trim().toLowerCase();
            return valueText.endsWith(filterText);
        } catch (ConverterException e) {
            log.error("filterEndsWithAndInsensitive", e);
        }
        return false;
    }

    /**
     * 區分大小寫進行開頭字串過濾（使用方法：&lt;p:column
     * filterFunction="#{StringFilterMBean.filterStartsWithAndSensitive}"
     * ...&gt;）
     *
     * @param value 來源資料 (由primefaces 自動帶入)
     * @param filter 過濾的字串 (由primefaces 自動帶入)
     * @param locale 地理區域 由primefaces 自動帶入
     * @return
     */
    public boolean filterStartsWithAndSensitive(Object value, Object filter, Locale locale) {
        if (filter == null || "".equals(filter.toString().trim())) {
            return true;
        }
        if (value == null) {
            return false;
        }
        try {
            String filterText = ((String) filter).trim();
            String valueText = ((String) value).trim();
            return valueText.startsWith(filterText);
        } catch (ConverterException e) {
            log.error("filterStartsWithAndSensitive", e);
        }
        return false;
    }

    /**
     * 區分大小寫進行包含字串過濾（使用方法：&lt;p:column
     * filterFunction="#{StringFilterMBean.filterContainsAndSensitive}" ...&gt;）
     *
     * @param value 來源資料 (由primefaces 自動帶入)
     * @param filter 過濾的字串 (由primefaces 自動帶入)
     * @param locale 地理區域 由primefaces 自動帶入
     * @return
     */
    public boolean filterContainsAndSensitive(Object value, Object filter, Locale locale) {
        if (filter == null || "".equals(filter.toString().trim())) {
            return true;
        }
        if (value == null) {
            return false;
        }
        try {
            String filterText = ((String) filter).trim();
            String valueText = ((String) value).trim();
            return valueText.contains(filterText);
        } catch (ConverterException e) {
            log.error("filterContainsAndSensitive", e);
        }
        return false;
    }

    /**
     * 區分大小寫進行結尾字串過濾（使用方法：&lt;p:column
     * filterFunction="#{StringFilterMBean.filterEndsWithAndSensitive}" ...&gt;）
     *
     * @param value 來源資料 (由primefaces 自動帶入)
     * @param filter 過濾的字串 (由primefaces 自動帶入)
     * @param locale 地理區域 由primefaces 自動帶入
     * @return
     */
    public boolean filterEndsWithAndSensitive(Object value, Object filter, Locale locale) {
        if (filter == null || "".equals(filter.toString().trim())) {
            return true;
        }
        if (value == null) {
            return false;
        }
        try {
            String filterText = ((String) filter).trim();
            String valueText = ((String) value).trim();
            return valueText.endsWith(filterText);
        } catch (ConverterException e) {
            log.error("filterEndsWithAndSensitive", e);
        }
        return false;
    }

    /**
     * 進行包含日期過濾 （使用方法：&lt;p:column
     * filterFunction="#{StringFilterMBean.filterContainsAndInsensitive}"
     * ...&gt;）
     *
     * @param value 來源資料 (由primefaces 自動帶入)
     * @param filter 過濾的字串 (由primefaces 自動帶入)
     * @param locale 地理區域 由primefaces 自動帶入
     * @return
     */
    public boolean filterContainsDate(Object value, Object filter, Locale locale) {
        if (filter == null || "".equals(filter.toString().trim())) {
            return true;
        }
        if (value == null) {
            return false;
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String filterText = ((String) filter).trim();
            String valueText = sdf.format((Date) value);
            return valueText.contains(filterText);
        } catch (ConverterException e) {
            log.error("filterContainsDate", e);
        }
        return false;
    }

}
