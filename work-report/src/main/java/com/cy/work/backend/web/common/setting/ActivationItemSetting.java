/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.setting;

import com.cy.commons.enums.Activation;
import com.google.common.collect.Lists;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * 狀態靜態資料
 *
 * @author brain0925_liao
 */
public class ActivationItemSetting {

    /** 狀態選項List */
    private static List<SelectItem> activationItems;

    /** 取得狀態選項List */
    public static List<SelectItem> getActivationItems() {
        return loadData();
    }

    /** 取得狀態選項資料,若記憶體已有將不再行建立 */
    private static List<SelectItem> loadData() {
        if (activationItems != null) {
            return activationItems;
        }
        activationItems = Lists.newArrayList();
        for (Activation item : Activation.values()) {

            SelectItem si = new SelectItem(item.name(), (Activation.ACTIVE.equals(item)) ? "正常" : "停用");

            activationItems.add(si);
        }
        return activationItems;
    }

}
