/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.callable;

import com.cy.work.backend.web.logic.components.WRFavoriteLogicComponents;
import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.cy.work.backend.web.vo.enums.WRFunItemComponentType;
import java.util.Date;
import org.joda.time.LocalDate;

/**
 * 收藏計算筆數Call able
 *
 * @author brain0925_liao
 */
public class FavoriteCountCallable implements HomeCallable {

    /** 登入者Sid */
    private final Integer loginUserSid;
    /** 選單需計算筆數列舉 */
    private final WRFunItemComponentType wrFunItemComponentType;
    /** 選單Group物件 */
    private final WRFunItemGroupVO wrFunItemGroupVO;

    public FavoriteCountCallable(WRFunItemGroupVO wrFunItemGroupVO,
            WRFunItemComponentType wrFunItemComponentType, Integer loginUserSid) {
        this.wrFunItemGroupVO = wrFunItemGroupVO;
        this.wrFunItemComponentType = wrFunItemComponentType;
        this.loginUserSid = loginUserSid;
    }

    @Override
    public HomeCallableCondition call() throws Exception {
        //預設月初~月底
        Date startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
        Date endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
        Integer count = WRFavoriteLogicComponents.getInstance().findFavoriteCountByUserSidAndUpdateDt(loginUserSid, startDate, endDate);
        HomeCallableCondition reuslt = new HomeCallableCondition();
        if (count > 0) {
            reuslt.setConntString("(" + count + ")");
            reuslt.setCssString("color: red");
        }
        reuslt.setWrFunItemComponentType(wrFunItemComponentType);
        reuslt.setWrFunItemGroupVO(wrFunItemGroupVO);
        return reuslt;
    }

}
