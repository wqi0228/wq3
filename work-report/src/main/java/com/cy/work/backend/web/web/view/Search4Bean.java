/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.common.ExportExcelComponent;
import com.cy.work.backend.web.listener.DataTableReLoadCallBack;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.TableUpDownListener;
import com.cy.work.backend.web.listener.TraceUpdateCallBack;
import com.cy.work.backend.web.listener.TransUpdateCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRTraceSearchWorkReportLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.components.WRReportCustomColumnLogicComponents;
import com.cy.work.backend.web.logic.components.WorkTraceInfoLogicComponents;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.util.SpringContextHolder;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.components.CustomColumnComponent;
import com.cy.work.backend.web.view.components.WRTraceComponent;
import com.cy.work.backend.web.view.components.WRTransTableComponent;
import com.cy.work.backend.web.view.components.WorkReportSearchHeaderComponent;
import com.cy.work.backend.web.view.vo.EditTraceNoteVO;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.backend.web.view.vo.TraceReportSearchVO;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRTraceSearchColumnVO;
import com.cy.work.backend.web.view.vo.WorkReportSidTo;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.TraceStatus;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.cy.work.backend.web.vo.enums.WRTraceSearchColumn;
import com.cy.work.backend.web.vo.enums.WRReportCustomColumnUrlType;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Search4Bean implements Serializable, TableUpDownListener {

    
    private static final long serialVersionUID = 2654098150923654015L;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Getter
    private CustomColumnComponent customColumn;
    @Getter
    private WRTraceSearchColumnVO searchColumnVO;
    /** view 顯示錯誤訊息 */
    @Getter
    private String errorMessage;
    @Getter
    private final String dataTableID = "dataTableTrace";
    @Getter
    private final String dataTableWv = "dataTableTraceWv";
    @Getter
    private List<TraceReportSearchVO> traceReportSearchVO;
    private List<TraceReportSearchVO> tempTraceReportSearchVOs;
    private String tempSelSid;
    @Setter
    @Getter
    private TraceReportSearchVO straceReportSearchVO;
    @Setter
    @Getter
    private boolean showFrame = false;
    @Setter
    @Getter
    private boolean isEmptyNoticeDate = false;
    @Getter
    private boolean hasDisplay = true;
    @Getter
    private boolean showCancelAndOverTraceBtn = false;
    @Getter
    private WorkReportSearchHeaderComponent workReportSearchHeaderComponent;
    @Getter
    private WRTraceComponent traceComponent;
    private String selWr_ID;
    @Getter
    private EditTraceNoteVO editTraceNoteVO;
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Getter
    private String iframeUrl = "";
    @Autowired
    private WRReportCustomColumnLogicComponents wrReportCustomColumnLogicComponents;
    @Autowired
    private WRTraceSearchWorkReportLogicComponents wRTraceSearchWorkReportLogicComponents;
    @Autowired
    private WorkTraceInfoLogicComponents workTraceInfoLogicComponents;
    @Autowired
    private ExportExcelComponent exportExcelComponent;
    @Getter
    private WRTransTableComponent transTableComponent;

    @PostConstruct
    public void init() {
        userViewVO = WRUserLogicComponents.getInstance().findBySid(SecurityFacade.getUserSid());
        depViewVo = OrgLogicComponents.getInstance().findBySid(SecurityFacade.getPrimaryOrgSid());
        compViewVo = OrgLogicComponents.getInstance().findById(SecurityFacade.getCompanyId());
        searchColumnVO = wrReportCustomColumnLogicComponents.getWRTraceSearchColumnSetting(userViewVO.getSid());
        customColumn = new CustomColumnComponent(WRReportCustomColumnUrlType.TRACE_SEARCH, Arrays.asList(WRTraceSearchColumn.values()), searchColumnVO.getPageCount(), userViewVO.getSid(), messageCallBack, dataTableReLoadCallBack);
        this.traceComponent = new WRTraceComponent(userViewVO.getSid(), messageCallBack, traceUpdateCallBack);
        this.transTableComponent = new WRTransTableComponent(userViewVO.getSid(), compViewVo.getSid(), transUpdateCallBack, messageCallBack);
        workReportSearchHeaderComponent = new WorkReportSearchHeaderComponent();
        workReportSearchHeaderComponent.settingDefault();
        editTraceNoteVO = new EditTraceNoteVO();
        doSearch();
    }

    public void cancelTrace() {
        try {
            workTraceInfoLogicComponents.cancelTrace(userViewVO.getSid(), straceReportSearchVO.getTracesid());
            updateListByTraceID(straceReportSearchVO.getTracesid());
            straceReportSearchVO = null;
            DisplayController.getInstance().update(dataTableID);
            selectCell();
        } catch (Exception e) {
            log.error("cancelTrace", e);
            messageCallBack.showMessage("追蹤取消失敗,請重新點選一次");
        }
    }

    public void overTrace() {
        try {
            workTraceInfoLogicComponents.overTrace(userViewVO.getSid(), straceReportSearchVO.getTracesid());
            updateListByTraceID(straceReportSearchVO.getTracesid());
            straceReportSearchVO = null;
            DisplayController.getInstance().update(dataTableID);
            selectCell();
        } catch (Exception e) {
            log.error("overTrace", e);
            messageCallBack.showMessage("追蹤完成失敗,請重新點選一次");
        }
    }

    public void updateTraceNote() {
        try {
            workTraceInfoLogicComponents.updateTraceNote(userViewVO.getSid(), editTraceNoteVO.getTraceId(), editTraceNoteVO.getMemo(), editTraceNoteVO.getNoticeDate());
            updateListByTraceID(editTraceNoteVO.getTraceId());
            DisplayController.getInstance().update(dataTableID);
            DisplayController.getInstance().hidePfWidgetVar("dlgTraceAction_mome");
            selectCell();
        } catch (Exception e) {
            log.error("updateTraceNote", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void updateListByTraceID(String traceID) {
        try {
            List<TraceReportSearchVO> update = runSearch("", traceID);
            if (update == null || update.isEmpty()) {
                tempTraceReportSearchVOs = Lists.newArrayList();
                if (traceReportSearchVO != null && !traceReportSearchVO.isEmpty()) {
                    tempTraceReportSearchVOs.addAll(traceReportSearchVO);
                    if (straceReportSearchVO != null) {
                        tempSelSid = straceReportSearchVO.getTracesid();
                    }
                    List<TraceReportSearchVO> sel = traceReportSearchVO.stream()
                            .filter(each -> each.getTracesid().equals(traceID))
                            .collect(Collectors.toList());
                    if (WkStringUtils.notEmpty(sel)) {
                        sel.forEach(item -> traceReportSearchVO.remove(item));
                    }
                }
            } else {
                update.forEach(newItem -> {
                    int index = traceReportSearchVO.indexOf(newItem);
                    if (index >= 0) {
                        traceReportSearchVO.get(index).replaceValue(newItem);
                    } else {
                        traceReportSearchVO.add(newItem);
                    }
                    if (tempTraceReportSearchVOs != null && !tempTraceReportSearchVOs.isEmpty()) {
                        int indext = tempTraceReportSearchVOs.indexOf(newItem);
                        if (indext >= 0) {
                            tempTraceReportSearchVOs.get(indext).replaceValue(newItem);
                        } else {
                            tempTraceReportSearchVOs.add(newItem);
                        }
                    }
                });
            }
        } catch (Exception e) {
            log.error("updateList", e);
        }
    }

    private void updateListByWrID(String wrID) {
        try {
            List<TraceReportSearchVO> update = runSearch(wrID, "");
            if (update == null || update.isEmpty()) {
                tempTraceReportSearchVOs = Lists.newArrayList();
                if (traceReportSearchVO != null && !traceReportSearchVO.isEmpty()) {
                    tempTraceReportSearchVOs.addAll(traceReportSearchVO);
                    if (straceReportSearchVO != null) {
                        tempSelSid = straceReportSearchVO.getTracesid();
                    }
                    List<TraceReportSearchVO> sel = traceReportSearchVO.stream()
                            .filter(each -> each.getWr_Id().equals(wrID))
                            .collect(Collectors.toList());
                    if (WkStringUtils.notEmpty(sel)) {
                        sel.forEach(item -> traceReportSearchVO.remove(item));
                    }
                }
            } else {
                update.forEach(newItem -> {
                    int index = traceReportSearchVO.indexOf(newItem);
                    if (index >= 0) {
                        traceReportSearchVO.get(index).replaceValue(newItem);
                    } else {
                        traceReportSearchVO.add(newItem);
                    }
                    if (tempTraceReportSearchVOs != null && !tempTraceReportSearchVOs.isEmpty()) {
                        int indext = tempTraceReportSearchVOs.indexOf(newItem);
                        if (indext >= 0) {
                            tempTraceReportSearchVOs.get(indext).replaceValue(newItem);
                        } else {
                            tempTraceReportSearchVOs.add(newItem);
                        }
                    }
                });
            }
        } catch (Exception e) {
            log.error("updateList", e);
        }
    }

    public void clear() {
        isEmptyNoticeDate = false;
        workReportSearchHeaderComponent.settingDefault();
        workReportSearchHeaderComponent.setSelectStatus("");
        doSearch();
    }

    public void doSearch() {
        straceReportSearchVO = null;
        selectCell();
        traceReportSearchVO = runSearch("", "");
        if (traceReportSearchVO.size() > 0) {
            DisplayController.getInstance().execute("selectDataTablePage('" + dataTableWv + "',0);");
        }
        DisplayController.getInstance().update(dataTableID);
    }

    /**
     * 匯出excel
     *
     * @param document excel物件
     */
    public void exportExcel(Object document) {
        try {
            exportExcelComponent.exportExcel(document, null,
                    null, "追蹤", false);
        } catch (Exception e) {
            log.error("exportExcel", e);
        } finally {
            hasDisplay = true;
        }
    }

    public void hideColumnContent() {
        hasDisplay = false;
    }

    public void selectCell() {
        showCancelAndOverTraceBtn = (straceReportSearchVO != null && workReportSearchHeaderComponent.getSelectTraceStatus().equals(TraceStatus.UN_FINISHED.name()));
    }

    public void clickTrace(String traceID) {
        try {
            List<TraceReportSearchVO> sel = traceReportSearchVO.stream()
                    .filter(each -> each.getTracesid().equals(traceID))
                    .collect(Collectors.toList());
            if (WkStringUtils.isEmpty(sel)) {
                messageCallBack.showMessage("追蹤資料比對有誤,無法開啟");
                return;
            }
            selWr_ID = sel.get(0).getWr_Id();
            straceReportSearchVO = sel.get(0);
            DisplayController.getInstance().update(dataTableID);
            traceComponent.loadData(straceReportSearchVO.getWr_Id(), straceReportSearchVO.getNo(), straceReportSearchVO.getTheme());
            DisplayController.getInstance().showPfWidgetVar("dlgTraceAction");
            DisplayController.getInstance().update("dlgTraceAction_view");
        } catch (Exception e) {
            log.error("clickTrace", e);
        }
    }

    public void clickTransDep(String traceID) {
        try {
            List<TraceReportSearchVO> sel = traceReportSearchVO.stream()
                    .filter(each -> each.getTracesid().equals(traceID))
                    .collect(Collectors.toList());
            if (WkStringUtils.isEmpty(sel)) {
                messageCallBack.showMessage("轉寄部門資料比對有誤,無法開啟");
                return;
            }

            straceReportSearchVO = sel.get(0);
            selWr_ID = straceReportSearchVO.getWr_Id();
            DisplayController.getInstance().update(dataTableID);
            transTableComponent.loadDataDep(straceReportSearchVO.getWr_Id(), straceReportSearchVO.getNo());
            DisplayController.getInstance().showPfWidgetVar("dlgForwardDep");
        } catch (Exception e) {
            log.error("clickTransDep", e);
        }
    }

    public void clickTransPerson(String traceID) {
        try {
            List<TraceReportSearchVO> sel = traceReportSearchVO.stream()
                    .filter(each -> each.getTracesid().equals(traceID))
                    .collect(Collectors.toList());
            if (WkStringUtils.isEmpty(sel)) {
                messageCallBack.showMessage("轉寄個人資料比對有誤,無法開啟");
                return;
            }
            straceReportSearchVO = sel.get(0);
            selWr_ID = straceReportSearchVO.getWr_Id();
            DisplayController.getInstance().update(dataTableID);
            transTableComponent.loadDataPerson(straceReportSearchVO.getWr_Id(), straceReportSearchVO.getNo());
            DisplayController.getInstance().showPfWidgetVar("dlgForwardPerson");
        } catch (Exception e) {
            log.error("clickTransDep", e);
        }
    }

    public void clickTraceNote(String traceID) {
        try {
            List<TraceReportSearchVO> sel = traceReportSearchVO.stream()
                    .filter(each -> each.getTracesid().equals(traceID))
                    .collect(Collectors.toList());
            if (WkStringUtils.isEmpty(sel)) {
                messageCallBack.showMessage("追蹤資料比對有誤,無法開啟");
                return;
            }
            straceReportSearchVO = sel.get(0);
            DisplayController.getInstance().update(dataTableID);
            editTraceNoteVO = new EditTraceNoteVO(sel.get(0).getTracesid(), sel.get(0).getTheme(), sel.get(0).getTrace_memo_css(), ToolsDate.transStringToDate(SimpleDateFormatEnum.SdfDate.getValue(), sel.get(0).getTraceNotifyDate()));
            DisplayController.getInstance().update("dlgTraceAction_mome");
            DisplayController.getInstance().execute("initReinstatedDlg('dlgTraceAction_mome','dlgTraceAction_mome_view_wvMome_id',200,150);");
            DisplayController.getInstance().showPfWidgetVar("dlgTraceAction_mome");
        } catch (Exception e) {
            log.error("clickTraceNote", e);
        }
    }

    public List<TraceReportSearchVO> runSearch(String wr_Id, String trace_Id) {
        return wRTraceSearchWorkReportLogicComponents.getWorkReportTrace(workReportSearchHeaderComponent.getSelectTag(),
                workReportSearchHeaderComponent.getSelectStatus(), workReportSearchHeaderComponent.getSelectTraceStatus(),
                isEmptyNoticeDate, workReportSearchHeaderComponent.getTitle(),
                workReportSearchHeaderComponent.getContent(),
                workReportSearchHeaderComponent.getTraceDateStart(),
                workReportSearchHeaderComponent.getTraceDateEnd(),
                workReportSearchHeaderComponent.getTraceNotifyDateStart(),
                workReportSearchHeaderComponent.getTraceNotifyDateEnd(),
                workReportSearchHeaderComponent.getAllSearchText(),
                userViewVO.getSid(), depViewVo.getSid(), wr_Id, trace_Id, compViewVo.getSid());

    }

    public void openCustomColumn() {
        try {
            customColumn.loadData();
            DisplayController.getInstance().update("dlgDefineField_view");
            DisplayController.getInstance().showPfWidgetVar("dlgDefineField");
        } catch (Exception e) {
            log.error("openCustomColumn", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void btnOpenUrl(String sid) {
        try {
            settingSelWorkReportSearchVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.error("btnOpenUrl", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private void settingSelWorkReportSearchVO(String sid) {
        TraceReportSearchVO sel = new TraceReportSearchVO(sid);
        int index = traceReportSearchVO.indexOf(sel);
        if (index > 0) {
            straceReportSearchVO = traceReportSearchVO.get(index);
        } else {
            straceReportSearchVO = traceReportSearchVO.get(0);
        }
        settingReaded();
    }

    public void settingPreviousAndNext() {
        int index = traceReportSearchVO.indexOf(straceReportSearchVO);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(traceReportSearchVO.get(index - 1).getWr_Id());
        }
        if (index == traceReportSearchVO.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(traceReportSearchVO.get(index + 1).getWr_Id());
        }
    }

    private void settingReaded() {
        straceReportSearchVO.replaceReadStatus(WRReadStatus.HASREAD);
        traceReportSearchVO.forEach(item -> {
            if (straceReportSearchVO.getWr_Id().equals(item.getWr_Id())) {
                item.replaceReadStatus(WRReadStatus.HASREAD);
            }
        });
    }

    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            settingSelWorkReportSearchVO(sid);
            settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp_iframe.xhtml?wrcId=" + straceReportSearchVO.getWr_Id();
            tableUpDownBean.setWorp_path("worp_iframe");
        } catch (Exception e) {
            log.error("btnOpenFrame", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.error("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    @Override
    public void openerByBtnUp() {
        try {
            //log.info("工作報告追蹤查詢進行上一筆動作");
            if (!traceReportSearchVO.isEmpty()) {
                toUp();
                if (straceReportSearchVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(straceReportSearchVO.getWr_Id());
                    settingReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
            //log.info("工作報告追蹤查詢進行上一筆動作-結束");
        } catch (Exception e) {
            log.error("工作報告追蹤查詢進行上一筆動作-失敗", e);
        } finally {

            tableUpDownBean.setSessionSettingOver();
        }
    }

    private void toUp() {
        try {
            if (tempTraceReportSearchVOs != null && !tempTraceReportSearchVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempTraceReportSearchVOs.indexOf(new TraceReportSearchVO(tempSelSid));
                if (index > 0) {
                    straceReportSearchVO = tempTraceReportSearchVOs.get(index - 1);
                } else {
                    straceReportSearchVO = traceReportSearchVO.get(0);
                }
                tempTraceReportSearchVOs = null;
                tempSelSid = "";
            } else {
                int index = traceReportSearchVO.indexOf(straceReportSearchVO);
                if (index > 0) {
                    straceReportSearchVO = traceReportSearchVO.get(index - 1);
                } else {
                    straceReportSearchVO = traceReportSearchVO.get(0);
                }
            }
        } catch (Exception e) {
            log.error("toUp", e);
            tempSelSid = "";
            tempTraceReportSearchVOs = null;
            if (!traceReportSearchVO.isEmpty()) {
                straceReportSearchVO = traceReportSearchVO.get(0);
            }
        }
    }

    private void toDown() {
        try {
            if (tempTraceReportSearchVOs != null && !tempTraceReportSearchVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid)) {
                int index = tempTraceReportSearchVOs.indexOf(new TraceReportSearchVO(tempSelSid));
                if (index >= 0) {
                    straceReportSearchVO = tempTraceReportSearchVOs.get(index + 1);
                } else {
                    straceReportSearchVO = traceReportSearchVO.get(traceReportSearchVO.size() - 1);
                }
                tempTraceReportSearchVOs = null;
                tempSelSid = "";
            } else {
                int index = traceReportSearchVO.indexOf(straceReportSearchVO);
                if (index >= 0) {
                    straceReportSearchVO = traceReportSearchVO.get(index + 1);
                } else {
                    straceReportSearchVO = traceReportSearchVO.get(traceReportSearchVO.size() - 1);
                }
            }
        } catch (Exception e) {
            log.error("toDown", e);
            tempTraceReportSearchVOs = null;
            tempSelSid = "";
            if (traceReportSearchVO.size() > 0) {
                straceReportSearchVO = traceReportSearchVO.get(traceReportSearchVO.size() - 1);
            }
        }
    }

    @Override
    public void openerByBtnDown() {
        try {
            //log.info("工作報告追蹤查詢進行下一筆動作");
            if (!traceReportSearchVO.isEmpty()) {
                toDown();
                if (straceReportSearchVO != null) {
                    settingPreviousAndNext();
                    tableUpDownBean.setSession_now_sid(straceReportSearchVO.getWr_Id());
                    settingReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
            //log.info("工作報告追蹤查詢進行下一筆動作-結束");
        } catch (Exception e) {
            log.error("工作報告追蹤查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    @Override
    public void openerByDelete() {

    }

    @Override
    public void openerByInvalid() {

    }

    @Override
    public void openerByCommit() {

    }

    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    @Override
    public void openerByEditTag() {
        doReloadInfo();
    }

    @Override
    public void openerByTrace() {
        doReloadInfo();
    }

    @Override
    public void openerByFavorite() {
    }

    @Override
    public void openerByReadRecept() {

    }

    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        WkJsonUtils jsonUtils = SpringContextHolder.getBean(WkJsonUtils.class);
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateListByWrID(workReportSidTos.get(0).getSid());
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception ex) {
            log.error("doReloadInfo", ex);
        }
    }

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 9031487754454659569L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confirmDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confirmDlgTemplate");
        }
    };

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 2879148407953779209L;

        @Override
        public void reload() {
            searchColumnVO = wrReportCustomColumnLogicComponents.getWRTraceSearchColumnSetting(userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };

    public final TraceUpdateCallBack traceUpdateCallBack = new TraceUpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 2491716952111066212L;

        @Override
        public void doUpdateData() {
            updateListByWrID(selWr_ID);
            selWr_ID = "";
            DisplayController.getInstance().update(dataTableID);
        }
    };

    public final TransUpdateCallBack transUpdateCallBack = new TransUpdateCallBack() {
        /**
         * 
         */
        private static final long serialVersionUID = 7210782554187336734L;

        @Override
        public void doUpdateData() {
            updateListByWrID(selWr_ID);
            selWr_ID = "";
            DisplayController.getInstance().update(dataTableID);
        }
    };

    @Override
    public void openerByTrans() {
        doReloadInfo();
    }
}
