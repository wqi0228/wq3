/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class TagSearchVO implements Serializable {

    
    private static final long serialVersionUID = 8193690295042010870L;
    public TagSearchVO(String sid, String createTime,
            String creatUser, String categoryName,
            String tagName, String status, String modifyTime, String seq) {
        this.sid = sid;
        this.createTime = createTime;
        this.creatUser = creatUser;
        this.categoryName = categoryName;
        this.tagName = tagName;
        this.status = status;
        this.modifyTime = modifyTime;
        this.seq = seq;
    }

    public TagSearchVO(String sid) {
        this.sid = sid;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TagSearchVO other = (TagSearchVO) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        return true;
    }

    @Getter
    private String sid;
    @Getter
    private String createTime;
    @Getter
    private String creatUser;
    @Getter
    private String categoryName;
    @Getter
    private String tagName;
    @Getter
    private String status;
    @Getter
    private String modifyTime;
    @Getter
    private String seq;
}
