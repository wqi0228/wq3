/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.util;
import com.google.common.base.Preconditions;
import java.util.Map;
import lombok.Getter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
/**
 *
 * @author brain0925_liao
 */
public class SpringContextHolder implements ApplicationContextAware {

    @Getter
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextHolder.applicationContext = applicationContext;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(String name) {
        checkApplicationContext();
        return (T) applicationContext.getBean(name);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<T> clazz) {
        checkApplicationContext();
        Map<String, T> m = applicationContext.getBeansOfType(clazz);
        return (T) m.values().toArray()[0];
    }

    private static void checkApplicationContext() {
        Preconditions.checkNotNull(
                applicationContext,
                "applicaitonContext未注入, 請在applicationContext.xml等文件中定義SpringContextHolder！"
        );
    }

}
