/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.work.backend.web.logic.manager.WRMasterManager;
import com.cy.work.backend.web.logic.manager.WorkSettingOrgManager;
import com.cy.work.backend.web.view.vo.OrgViewVo;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * 部門邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class OrgLogicComponents implements InitializingBean, Serializable {


    private static final long serialVersionUID = -7295880834008820198L;
    private static OrgLogicComponents instance;

    public static OrgLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        OrgLogicComponents.instance = this;
    }

    @Autowired
    private WorkSettingOrgManager orgManager;
    @Autowired
    private WRMasterManager wRMasterManager;

    /**
     * 取得所有部門List
     *
     * @return
     */
    public List<Org> findAllOrg() {
        return orgManager.findAll();
    }

    /**
     * 取得索取讀取回條部門 By 登入者sid
     *
     * @param userSid 登入者Sid
     * @return
     */
    public List<Org> findSendReadReceiptsOrg(Integer userSid) {
        return wRMasterManager.findSendReadReceiptsOrg(userSid);
    }

    /**
     * 如CompanyCategory為部門（DEPARTMENT）且DepartmentLevel為組層級（THE_PANEL），則向上取到非組層級
     *
     * @param orgSid
     * @return basicOrg（至少為部級單位）
     */
    public Org getBaseOrgx(Integer orgSid) {
        return WkOrgUtils.prepareBaseDep(orgSid, OrgLevel.MINISTERIAL);
    }

    /**
     * 取得該部門所有上層部門
     *
     * @param childOrg 部門
     * @return
     */
    public List<Org> getAllParentOrgs(Org childOrg) {
        if (childOrg == null) {
            return Lists.newArrayList();
        }
        if (childOrg.getParent() == null || childOrg.getParent().getSid() == null) {
            return Lists.newArrayList();
        }
        List<Org> parentOrgs = Lists.newArrayList();
        getAllParentOrgs(childOrg, parentOrgs);
        return parentOrgs;
    }

    /**
     * 取得該部門所有上層部門
     *
     * @param childOrg  部門
     * @param parentOrg 記錄上層部門List
     * @return
     */
    private List<Org> getAllParentOrgs(Org childOrg, List<Org> parentOrg) {
        if (childOrg.getParent() != null && childOrg.getParent().getSid() != null
                && orgManager.findBySid(childOrg.getParent().getSid()) != null) {
            Org parent = orgManager.findBySid(childOrg.getParent().getSid());
            parentOrg.add(parent);
            getAllParentOrgs(parent, parentOrg);
        }
        return parentOrg;
    }

    /**
     * 取得該部門所有子部門
     *
     * @param parentOrg 部門
     * @return
     */
    public List<Org> getAllChildOrg(Org parentOrg) {
        List<Org> childOrg = Lists.newArrayList();
        orgManager.findChildOrgByParentSid(parentOrg.getSid()).forEach(item -> {
            if (item.getStatus().equals(Activation.INACTIVE)) {
                return;
            }
            getAllChildOrg(item, childOrg);
        });

        return childOrg;
    }

    public List<Org> getAllChildOrgActiveAndInactive(Org parentOrg) {
        List<Org> childOrg = Lists.newArrayList();
        orgManager.findChildOrgByParentSid(parentOrg.getSid()).forEach(item -> {
            getAllChildOrgActiveAndInactive(item, childOrg);
        });
        return childOrg;
    }

    private List<Org> getAllChildOrgActiveAndInactive(Org parentOrg, List<Org> childOrg) {
        childOrg.add(parentOrg);
        orgManager.findChildOrgByParentSid(parentOrg.getSid()).forEach(item -> {
            getAllChildOrg(item, childOrg);
        });
        return childOrg;
    }

    /**
     * 取得該部門所有子部門
     *
     * @param parentOrg 部門
     * @param childOrg  記錄子部門List
     * @return
     */
    private List<Org> getAllChildOrg(Org parentOrg, List<Org> childOrg) {
        childOrg.add(parentOrg);
        orgManager.findChildOrgByParentSid(parentOrg.getSid()).forEach(item -> {
            if (item.getStatus().equals(Activation.INACTIVE)) {
                return;
            }
            getAllChildOrg(item, childOrg);
        });
        return childOrg;
    }

    /**
     * 取得部門介面物件 By 部門ID
     *
     * @param id 部門ID
     * @return
     */
    public OrgViewVo findById(String id) {
        try {
            Org o = orgManager.findByID(id);
            OrgViewVo ov = new OrgViewVo(o.getSid(), o.getName());
            return ov;
        } catch (Exception e) {
            log.error("findById error :", e);
        }
        return new OrgViewVo();
    }

    /**
     * 取得部門介面物件 By 部門Sid
     *
     * @param sid 部門Sid
     * @return
     */
    public OrgViewVo findBySid(Integer sid) {
        try {
            Org o = orgManager.findBySid(sid);
            OrgViewVo ov = new OrgViewVo(o.getSid(), o.getOrgNameWithParentIfDuplicate());
            return ov;
        } catch (Exception e) {
            log.error("findBySid error :", e);
        }
        return new OrgViewVo();
    }

    /**
     * 取得部門物件 By 部門Sid
     *
     * @param sid 部門Sid
     * @return
     */
    public Org findOrgBySid(Integer sid) {
        try {
            return orgManager.findBySid(sid);
        } catch (Exception e) {
            log.error("findBySid error :", e);
        }
        return new Org();
    }

    /**
     * 取得公司部門 By 公司Sid
     *
     * @param companySid 公司Sid
     * @return
     */
    public List<Org> findOrgsByCompanySid(Integer companySid) {
        List<Org> companyOrgs = Lists.newArrayList();
        orgManager.findAll().forEach(item -> {
            if (item.getCompany() != null && item.getCompany().getSid().equals(companySid)) {
                companyOrgs.add(item);
            }
        });
        return companyOrgs;
    }

    /**
     * 顯示所有上層組織到「處」級為止
     *
     * @param o 部門
     * @return
     */
    public String showParentDepInner(Org o) {
        if (!OrgType.DEPARTMENT.equals(o.getType())) {
            return o.getName();
        }
        if (o.getParent() == null || o.getParent().getSid() == null
                || !OrgType.DEPARTMENT.equals(o.getParent().getType())) {
            return o.getName();
        }
        Org parentOrg = orgManager.findBySid(o.getParent().getSid());
        return showParentDepInner(parentOrg) + "-" + o.getName();
    }

    public String showParentDep(Org org) {
        if (org.getLevel() != null && (org.getLevel().equals(OrgLevel.GROUPS) || org.getLevel().equals(OrgLevel.DIVISION_LEVEL))) {
            return org.getName();
        }
        if (org.getType().equals(OrgType.HEAD_OFFICE) || org.getType().equals(OrgType.BRANCH_OFFICE)) {
            return org.getName();
        }
        return this.parseDivision(this.showParentDepInner(org));
    }

    private String parseDivision(String result) {
        if (!result.contains("群")) {
            return result;
        }
        String[] array = result.split("-");
        int groupCnt = 0;
        int divCnt = 0;
        for (String each : array) {
            if (each.contains("群")) {
                groupCnt++;
            }
            if (each.contains("處")) {
                divCnt++;
            }
        }
        if (groupCnt > 0 && divCnt > 0) {
            List<String> strList = Lists.newArrayList();
            int idx = 0;
            for (String each : array) {
                if (idx++ != 0) {
                    strList.add(each);
                }
            }
            return Joiner.on(" - ").join(strList);
        }
        return result;
    }

}
