/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class UserModifyVO implements Serializable {

    
    private static final long serialVersionUID = -1377617093315958326L;
    public UserModifyVO(String modifyUserName, String lastModifyTime) {
        this.modifyUserName = modifyUserName;
        this.lastModifyTime = lastModifyTime;
    }

    @Getter
    private String modifyUserName;
    @Getter
    private String lastModifyTime;

}
