/**
 * 
 */
package com.cy.work.backend.web.controller.component.mipker.vo;

import lombok.Getter;

/**
 * @author allen1214_wu
 *
 */
public enum MultItemPickerShowMode {

    LIST("列表模式"),
    TREE("樹模式");

    /** 欄位名稱 */
    @Getter
    private final String label;

    MultItemPickerShowMode(String label) {
        this.label = label;
    }
    
}
