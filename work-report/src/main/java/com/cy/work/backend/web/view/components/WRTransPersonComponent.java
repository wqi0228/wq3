/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.apache.commons.lang3.StringUtils;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.common.DepTreeComponent;
import com.cy.work.backend.web.common.MultipleDepTreeManager;
import com.cy.work.backend.web.listener.MessageCallBack;
import com.cy.work.backend.web.listener.ReLoadCallBack;
import com.cy.work.backend.web.listener.TransUpdateCallBack;
import com.cy.work.backend.web.logic.components.OrgLogicComponents;
import com.cy.work.backend.web.logic.components.WRGroupSetupPrivateLogicComponents;
import com.cy.work.backend.web.logic.components.WRInboxLogicComponents;
import com.cy.work.backend.web.logic.components.WRUserLogicComponents;
import com.cy.work.backend.web.logic.manager.WrReadRecordManager;
import com.cy.work.backend.web.logic.utils.BeanUtils;
import com.cy.work.backend.web.util.pf.DataTablePickList;
import com.cy.work.backend.web.util.pf.DisplayController;
import com.cy.work.backend.web.view.vo.TransOrgViewVO;
import com.cy.work.backend.web.view.vo.UserViewVO;
import com.cy.work.backend.web.view.vo.WRGroupVO;
import com.cy.work.backend.web.vo.WrReadRecord;
import com.cy.work.backend.web.vo.enums.ForwardType;
import com.cy.work.backend.web.vo.enums.WRReadStatus;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
public class WRTransPersonComponent implements Serializable {


    private static final long serialVersionUID = 6617343904214249675L;

    @Getter
    private List<TransOrgViewVO> transOrgViewVOs;

    private MessageCallBack messageCallBack;

    private TransUpdateCallBack updateCallBack;

    private TransUpdateCallBack closeCallBack;
    @Getter
    private WRGroupSetupPrivateComponent wrGroupSetupPrivateComponent;
    /** DataTablePickList 物件 */
    private DataTablePickList<UserViewVO> dataPickList;

    private Integer loginUserSid;
    @Setter
    @Getter
    private String transUserName;

    private String wr_Sid;

    private String wr_no;
    @Setter
    @Getter
    private String memo;
    @Getter
    private List<SelectItem> wrGroupVOs;

    private List<UserViewVO> allCompUser;

    private List<WRGroupVO> realWRGroupVOs;

    private List<Org> selectOrgs;

    private DepTreeComponent depTreeComponent;

    private List<UserViewVO> leftViewVO;
    @Setter
    @Getter
    private String selWRGroupVOs;
    @Getter
    private String duplicateMessage;

    public WRTransPersonComponent(Integer loginUserSid,
                                  MessageCallBack messageCallBack, TransUpdateCallBack updateCallBack, TransUpdateCallBack closeCallBack) {
        this.loginUserSid = loginUserSid;
        this.messageCallBack = messageCallBack;
        this.updateCallBack = updateCallBack;
        this.closeCallBack = closeCallBack;
        this.wrGroupSetupPrivateComponent = new WRGroupSetupPrivateComponent(
                "trans_group_setup_private_setting_dlg_wv",
                "transgroupMaintainComponent_pickPanel",
                "workTransGroup_deptUnitDlg",
                "workTransGroup_dep",
                "trans_group_setup_private_dlg_panel_table_wv",
                "trans_group_setup_private_dlg_panel_table_id",
                loginUserSid, messageCallBack, groupReloadCallBack);
        this.wrGroupVOs = Lists.newArrayList();
        this.realWRGroupVOs = Lists.newArrayList();
        initDataPickList();
        this.depTreeComponent = new DepTreeComponent();
        initOrgTree();
    }

    public void closeDialog() {
        closeCallBack.doUpdateData();
    }

    public void saveTransPerson() {
        try {
            String duplicateErrorMessage = WRInboxLogicComponents.getInstance().checkDuplicateReceviceUser(wr_Sid, dataPickList.getTargetData(), loginUserSid);
            if (!Strings.isNullOrEmpty(duplicateErrorMessage)) {
                duplicateMessage = duplicateErrorMessage;
                DisplayController.getInstance().update("dlgCheckUsers_id");
                DisplayController.getInstance().showPfWidgetVar("dlgCheckUsers");
                return;
            }
        } catch (Exception e) {
            log.error("saveTransPerson", e);
            messageCallBack.showMessage(e.getMessage());
        }
        doSaveTransPerson();
    }

    public void doSaveTransPerson() {
        WrReadRecordManager wrReadRecordManager = BeanUtils.getBean(WrReadRecordManager.class);
        List<WrReadRecord> recordList = Lists.newArrayList();
        try {
            //根據重複訊息是否為空來判斷狀態為"未閱讀"或"待閱讀"
            for (UserViewVO item : dataPickList.getTargetData()) {
                Optional<WrReadRecord> recordOptional = wrReadRecordManager.findByUserSidAndWrSid(item.getSid().toString(), wr_Sid);
                //如果該主檔有該使用者的閱讀紀錄則不重複寫新一筆資料，跳過此次迴圈
                if (recordOptional.isPresent()) {
                    WRReadStatus readStatus = WRReadStatus.UNREAD;
                    if (!recordOptional.get().getCreateDate().toString().equals(recordOptional.get().getUpdateDate().toString())) {
                        readStatus = WRReadStatus.WAIT_READ;
                    }
                    recordOptional.get().setReadStatus(readStatus.name());
                    continue;
                }
                WrReadRecord wrReadRecord = new WrReadRecord("", item.getSid().toString(), new Date(), wr_Sid);
                if (StringUtils.isNotBlank(duplicateMessage)) {
                    wrReadRecord.setReadStatus(WRReadStatus.WAIT_READ.name());
                } else {
                    wrReadRecord.setReadStatus(WRReadStatus.UNREAD.name());
                }
                recordList.add(wrReadRecord);
            }
            wrReadRecordManager.saveAllWrReadRecord(recordList);
            WRInboxLogicComponents.getInstance().saveReceviceUsers(wr_Sid, wr_no, dataPickList.getTargetData(), memo, loginUserSid);
            updateCallBack.doUpdateData();
        } catch (Exception e) {
            log.error("doSaveTransPerson", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void clear() {
        this.memo = "";
        this.dataPickList = new DataTablePickList<>(UserViewVO.class);
        if (wrGroupVOs != null) {
            wrGroupVOs.clear();
        }
        if (allCompUser != null) {
            allCompUser.clear();
        }
        if (selectOrgs != null) {
            selectOrgs.clear();
        }
        initOrgTree();
        DisplayController.getInstance().update("dlgTransActionUsers_dep");

        if (leftViewVO != null) {
            leftViewVO.clear();
        }
        DisplayController.getInstance().update("spForwardPersonMode");
    }

    public void loadData(String wr_Sid, String wr_no) {
        this.wr_Sid = wr_Sid;
        this.wr_no = wr_no;
        this.transOrgViewVOs = WRInboxLogicComponents.getInstance().getWorkInboxByWrSidAndForwardTypeAndLoginUserSid(wr_Sid,
                ForwardType.PERSON, loginUserSid);

        allCompUser = Lists.newArrayList();
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        Integer compSid = user.getPrimaryOrg().getCompanySid();
        try {
            OrgLogicComponents.getInstance().findOrgsByCompanySid(compSid).stream()
                    .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                    .collect(Collectors.toList()).forEach(item -> {
                        allCompUser.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
                    });
        } catch (Exception e) {
            log.error(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }
        leftViewVO = Lists.newArrayList();
        selectOrgs = Lists.newArrayList();
        Org baseOrg = WkOrgUtils.prepareBaseDep(user.getPrimaryOrg().getSid(), OrgLevel.MINISTERIAL);
        selectOrgs.add(baseOrg);
        List<Org> allChildOrg = OrgLogicComponents.getInstance().getAllChildOrg(baseOrg);
        selectOrgs.addAll(allChildOrg);
        leftViewVO.addAll(WRUserLogicComponents.getInstance().findByDepSid(baseOrg.getSid()));
        allChildOrg.forEach(item -> {
            leftViewVO.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
        });
        selWRGroupVOs = "";
        List<UserViewVO> right = Lists.newArrayList();
        dataPickList.setSourceData(removeTargetUserView(leftViewVO, right));
        dataPickList.setTargetData(right);
        loadGroupData();
    }

    private void initDataPickList() {
        this.dataPickList = new DataTablePickList<>(UserViewVO.class);
    }

    private void initOrgTree() {
        Org group = new Org(1);
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectOrgs = Lists.newArrayList();
        depTreeComponent.init(Lists.newArrayList(), Lists.newArrayList(),
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    public void loadGroupData() {
        wrGroupVOs.clear();
        realWRGroupVOs.clear();
        for (WRGroupVO item : WRGroupSetupPrivateLogicComponents.getInstance().getWRGroupVO(loginUserSid)) {
            if (item.getStatusID().equals(Activation.ACTIVE.name())) {
                realWRGroupVOs.add(item);
                SelectItem si = new SelectItem(item.getGroup_sid(), item.getGroup_name());
                wrGroupVOs.add(si);
            }
        }
        DisplayController.getInstance().update("dlgTrance_groupSetup");
    }

    private final ReLoadCallBack groupReloadCallBack = new ReLoadCallBack() {

        private static final long serialVersionUID = -5871667402305083930L;

        @Override
        public void onReload() {
            loadGroupData();
        }
    };

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() {
        return depTreeComponent.getMultipleDepTreeManager();
    }

    public void loadDepTree() {
        User user = WRUserLogicComponents.getInstance().findUserBySid(loginUserSid);
        Org group = new Org(user.getPrimaryOrg().getCompanySid());
        // 修改,故切換顯示停用部門時,需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        List<Org> allOrg = OrgLogicComponents.getInstance().findOrgsByCompanySid(group.getSid());
        depTreeComponent.init(allOrg, allOrg,
                group, selectOrgs, clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
    }

    public void doSelectOrg() {
        try {
            selectOrgs = depTreeComponent.getSelectOrg();
            if (selectOrgs == null || selectOrgs.isEmpty()) {
                dataPickList.setSourceData(removeTargetUserView(allCompUser, dataPickList.getTargetData()));
                return;
            } else {
                List<UserViewVO> selectDepUserView = Lists.newArrayList();
                selectOrgs.forEach(item -> {
                    selectDepUserView.addAll(WRUserLogicComponents.getInstance().findByDepSid(item.getSid()));
                });
                dataPickList.setSourceData(removeTargetUserView(selectDepUserView, dataPickList.getTargetData()));
            }
        } catch (Exception e) {
            log.error("doSelectOrg", e);
            messageCallBack.showMessage(e.getMessage());
        }
        DisplayController.getInstance().update("pgMember");
        DisplayController.getInstance().hidePfWidgetVar("dlgTransActionUsers_deptUnitDlg");
    }

    public void filterName() {
        if (Strings.isNullOrEmpty(transUserName)) {
            dataPickList.setSourceData(removeTargetUserView(allCompUser, dataPickList.getTargetData()));
        } else {
            dataPickList.setSourceData(removeTargetUserView(allCompUser.stream()
                    .filter(each -> each.getName().toLowerCase().contains(transUserName.toLowerCase()))
                    .collect(Collectors.toList()), dataPickList.getTargetData()));
        }
    }

    public DataTablePickList<UserViewVO> getDataPickList() {
        return dataPickList;
    }

    public void changeGroupSelectItem() {
        if (!Strings.isNullOrEmpty(selWRGroupVOs)) {
            if (realWRGroupVOs != null) {
                List<WRGroupVO> selWRGroupVO = realWRGroupVOs.stream().filter(each -> each.getGroup_sid().equals(selWRGroupVOs))
                        .collect(Collectors.toList());
                if (selWRGroupVO != null && !selWRGroupVO.isEmpty()) {
                    dataPickList.setSourceData(removeTargetUserView(selWRGroupVO.get(0).getUserViewVO(), dataPickList.getTargetData()));
                }
            }
        } else {
            dataPickList.setSourceData(removeTargetUserView(leftViewVO, dataPickList.getTargetData()));
        }
    }

    public void toGroupManagerDialog() {
        this.wrGroupSetupPrivateComponent.loadData();
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source, List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(item -> {
            if (!target.contains(item)) {
                filterSource.add(item);
            }
        });
        return filterSource;
    }

}
