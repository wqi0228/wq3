/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.view.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import lombok.Getter;

/**
 *
 * @author brain0925_liao
 */
public class WRGroupVO implements Serializable {


    private static final long serialVersionUID = 4772827415860703004L;

    public WRGroupVO(String group_sid, String group_name,
            List<UserViewVO> userViewVO, String statusID, String note) {
        this.group_sid = group_sid;
        this.group_name = group_name;
        this.userViewVO = userViewVO;
        this.statusID = statusID;
        this.note = note;
        this.status = "";
        this.info = "";
    } 

    public WRGroupVO(String group_sid, String group_name,
            List<UserViewVO> userViewVO, String status, String statusID, String note, String info) {
        this.group_sid = group_sid;
        this.group_name = group_name;
        this.userViewVO = userViewVO;
        this.status = status;
        this.statusID = statusID;
        this.note = note;
        this.info = info;
    }
    @Getter
    private final String group_sid;
    @Getter
    private final String group_name;
    @Getter
    private final List<UserViewVO> userViewVO;
    @Getter
    private final String status;
    @Getter
    private final String statusID;
    @Getter
    private final String note;
    @Getter
    private final String info;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.group_sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WRGroupVO other = (WRGroupVO) obj;
        if (!Objects.equals(this.group_sid, other.group_sid)) {
            return false;
        }
        return true;
    }

}
