/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.common.attachment;


import com.cy.work.backend.web.logic.utils.attachment.AttachmentUtils;
import com.cy.work.backend.web.logic.utils.attachment.FileNameValidator;
import com.cy.work.backend.web.logic.utils.attachment.JpegExifRewriter;
import com.cy.work.backend.web.logic.vo.AttachmentVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author brain0925_liao
 */
@Slf4j
public abstract class AttachmentComponent implements Serializable {


    private static final long serialVersionUID = -7588782184842981661L;
    @Getter
    private List<AttachmentVO> attachmentVOs;
    @Getter
    private StringBuilder uploadMessages;
    /** 選取的附加檔案 */
    @Getter
    private List<AttachmentVO> selectedAttachmentVOs;
    /** 開啟Dialog當次上傳的檔案 */
    @Getter
    private List<AttachmentVO> oneTimeUploadAttachmentVOs;

    private AttachmentCondition attachmentCondition;

    public void loadAttachment(AttachmentCondition attachmentCondition) {
        this.attachmentCondition = attachmentCondition;
        if (uploadMessages == null) {
            uploadMessages = new StringBuilder();
        }
        clearMessages();
        if (!Strings.isNullOrEmpty(attachmentCondition.getEntity_sid())) {
            attachmentVOs = getPersistedAttachments(attachmentCondition.getEntity_sid());
        } else {
            attachmentVOs = Lists.newArrayList();
        }
        oneTimeUploadAttachmentVOs = Lists.newArrayList();
        selectedAttachmentVOs = Lists.newArrayList();
    }

    protected abstract List<AttachmentVO> getPersistedAttachments(String attachmentSid);

    /**
     * 單一檔案容量限制，由子類別決定.
     *
     * @return 檔案容量上限，單位是 MB (1MB = 1,000,000 bytes)
     */
    public abstract Integer getFileSizeLimit();

    /**
     * 清除上傳過程所產生的訊息.
     */
    private void clearMessages() {
        uploadMessages.delete(0, uploadMessages.length());
    }

    /**
     * 由頁面呼叫，處理一筆檔案上傳的細節.
     *
     * @param event
     */
    public void handleFileUpload(FileUploadEvent event) {
        UploadedFile uploadedFile = event.getFile();
        // 在此步驟先把檔名正規化， 避免某些瀏覽器 (MSIE) 在檔名欄位填寫檔案完整路徑，
        // 造成檔名必定包含特殊字元 (back slash)
        String fileName = FilenameUtils.getName(uploadedFile.getFileName());
        String fileNameExtension = FilenameUtils.getExtension(fileName);
        if (!validateByFileNameAndResponse(fileName)) {
            return;
        }
        File tempFile = null;
        try {
            AttachmentUtils.createDirectoryIfNotExist(getDirectoryName());
            tempFile
                    = AttachmentUtils.saveToTemporaryFile(uploadedFile.getInputstream(), getDirectoryName(),
                            fileNameExtension);
            AttachmentVO attachment = createAndPersistAttachment(fileName, attachmentCondition);
            File serverSideFile = AttachmentUtils.getServerSideFile(attachment, getDirectoryName());
            if (!rewrite(tempFile, serverSideFile, fileNameExtension)) {
                if (!tempFile.renameTo(serverSideFile))
                    log.warn("更名不成功");
            }
            oneTimeUploadAttachmentVOs.add(attachment);
            selectedAttachmentVOs.add(attachment);
            addMessage(fileName, "OK");
        } catch (IOException ex) {
            log.error("上傳附件並進行對應發生錯誤", ex);
            addMessage(fileName, "發生錯誤，" + ex.getLocalizedMessage());
        } catch (Exception ex) {
            log.error("上傳附件並進行對應發生錯誤", ex);
        } finally {
            FileUtils.deleteQuietly(tempFile);
        }
    }

    /**
     * 檢查檔名是否符合規範，以及是否已經有相同檔名的檔案.
     *
     * @param fileName
     * @return true 代表通過檢查；否則為 false.
     */
    private boolean validateByFileNameAndResponse(String fileName) {
        FileNameValidator fileNameValidator = new FileNameValidator(fileName);
        if (!fileNameValidator.isAcceptable()) {
            addMessage(fileName, "不接受，" + fileNameValidator.getValidateMessage());
            return false;
        }
        if (duplicateFileNameExists(fileName)) {
            addMessage(fileName, "不接受，因為檔名重覆.");
            return false;
        }
        return true;
    }

    /**
     * 單一檔案容量限制，由子類別決定.
     *
     * @return 檔案容量上限，單位是 MB (1MB = 1,000,000 bytes)
     */
    private void addMessage(String fileName, String detail) {
        if (uploadMessages.length() > 0) {
            uploadMessages.append("\n");
        }
        uploadMessages.append("\"");
        uploadMessages.append(fileName);
        uploadMessages.append("\": ");
        uploadMessages.append(detail);
    }

    /**
     * 從現有的附加檔案中檢查是否有檔案的檔名跟參數相同.
     *
     * @param fileName 檢查要件.
     * @return true 代表有附加檔案的檔名跟參數相同；否則為 false.
     */
    private boolean duplicateFileNameExists(String fileName) {
        for (AttachmentVO attachment : attachmentVOs) {
            if (attachment.getAttName().equalsIgnoreCase(fileName)) {
                return true;
            }
        }
        for (AttachmentVO attachment : oneTimeUploadAttachmentVOs) {
            if (attachment.getAttName().equalsIgnoreCase(fileName)) {
                return true;
            }
        }
        return false;
    }

    public void changeCheckbox() {
        selectedAttachmentVOs.clear();
        oneTimeUploadAttachmentVOs.stream()
                .filter(each -> each.isChecked())
                .forEach(each -> selectedAttachmentVOs.add(each));
    }

    /**
     * 每一種單據的存放位置， <code>getUploadRoot()</code> 加上此方法的回傳值就是 server side
     * 檔案的存放目錄.<br>
     * 因為每種單據的存放路徑不一樣，故交給子類別決定.
     *
     * @return 子目錄名稱.
     */
    public abstract String getDirectoryName();

    /**
     * 產生一個新的附加檔案 entity
     * 物件並儲存，要注意的是此時單據關聯欄位尚未設定.因為不同單據的產生方法不一定相同，故交給子類別針對特定單據來實作.
     *
     * @param fileName 附加檔案的檔名.
     * @return 已經存檔的附加檔案 entity 物件，該物件的 primary key 欄位已經有資料， 但是單據關聯欄位還沒有資料.
     */
    protected abstract AttachmentVO createAndPersistAttachment(String fileName, AttachmentCondition attachmentCondition);

    /**
     * 替某些檔案改寫部分檔案內容，並儲存為最終標的.<br>
     * 請注意本方法不會擲出 Exception ，若內部發生 Exception 則回傳 false.
     *
     * @param source 輸入檔案，可能需要改寫部分內容.
     * @param target 最終標的檔案.
     * @param fileNameExtension 附檔名，用來初步過濾檔案格式.
     * @return true 代表輸入檔案經過修改並儲存為最終標的檔案；否則為 false.
     */
    private boolean rewrite(File source, File target, String fileNameExtension) {
        if ("jpg".equalsIgnoreCase(fileNameExtension) || "jpeg".equalsIgnoreCase(fileNameExtension)) {
            JpegExifRewriter rewriter = new JpegExifRewriter();
            try {
                return rewriter.rewriteExif(source, target);
            } catch (ImageReadException e) {
                log.error("Failed to rewrite JPEG EXIF information.", e);
            } catch (ImageWriteException e) {
                log.error("Failed to rewrite JPEG EXIF information.", e);
            } catch (IOException e) {
                log.error("Failed to rewrite JPEG EXIF information.", e);
            }
        }
        return false;
    }

}
