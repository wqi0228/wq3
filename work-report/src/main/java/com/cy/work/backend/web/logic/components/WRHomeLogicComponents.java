/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.backend.web.callable.HomeCallable;
import com.cy.work.backend.web.callable.HomeCallableCondition;
import com.cy.work.backend.web.callable.HomeCallableService;
import com.cy.work.backend.web.common.setting.WRFunItemGroupSetting;
import com.cy.work.backend.web.view.vo.WRFunItemGroupVO;
import com.cy.work.backend.web.view.vo.WRFunItemVO;
import com.cy.work.backend.web.view.vo.WRHomepageFavoriteVO;
import com.cy.work.backend.web.vo.enums.WRFuncGroupBaseType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 選單邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRHomeLogicComponents implements InitializingBean, Serializable {


    private static final long serialVersionUID = -1012315638952152819L;
    private static WRHomeLogicComponents instance;

    @Autowired
    private WRFunItemGroupLogicComponents wrFunItemGroupLogicComponents;
    @Autowired
    private WRHomepageFavoriteLogicComponents wRHomepageFavoriteLogicComponents;

    public static WRHomeLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRHomeLogicComponents.instance = this;
    }

    /**
     * 取得選單 By 選單ID 及 登入者Sid 及 登入者部門Sid
     *
     * @param keepType     選單ID
     * @param loginUserSid 登入者Sid
     * @param depSid       登入者部門Sid
     * @return
     */
    public List<WRFunItemGroupVO> getWRFunItemGroupByTypeAndUserPermission(String keepType, Integer loginUserSid, Integer depSid) {
        List<WRFunItemGroupVO> wrFunItemGroupVOs = null;
        try {
            wrFunItemGroupVOs = WRFunItemGroupSetting.getWRFunItemGroups().stream()
                    .filter(p -> p.getGroupBaseSid().equals(keepType))
                    .collect(Collectors.toList());
            if (wrFunItemGroupVOs == null || wrFunItemGroupVOs.isEmpty()) {
                log.error("選單資訊無相同type : keepType(" + keepType + ")");
                return Lists.newArrayList();
            }
        } catch (Exception e) {
            log.error("getWRFunItemGroupByTypeAndUserPermission", e);
            return Lists.newArrayList();
        }
        List<String> userPermissions = Lists.newArrayList(SecurityFacade.getPermissions());
        List<WRFunItemGroupVO> menuGroups = Lists.newArrayList();
        List<HomeCallable> homeCallables = Lists.newArrayList();
        for (WRFunItemGroupVO wrFunItemGroupVO : wrFunItemGroupVOs) {
            List<WRFunItemVO> tempItems = Lists.newArrayList();
            if (String.valueOf(wrFunItemGroupLogicComponents.getReport_favorite()).equals(wrFunItemGroupVO.getGroupSid())) {
                // 報表快選區
                List<WRFunItemGroupVO> wrFunItemGroups
                        = getWRFunItemGroupByTypeAndUserPermission(WRFuncGroupBaseType.SEARCH.getKey(),
                        loginUserSid, depSid);
                List<WRHomepageFavoriteVO> wRHomepageFavoriteVOs
                        = wRHomepageFavoriteLogicComponents.getWRHomepageFavoriteVOByUserSid(loginUserSid);
                if (wrFunItemGroups != null && !wrFunItemGroups.isEmpty()
                        && wrFunItemGroups.get(0).getWrFunItemVOs() != null) {
                    wrFunItemGroups.get(0).getWrFunItemVOs().forEach(favorite -> {
                        List<WRHomepageFavoriteVO> sel = wRHomepageFavoriteVOs.stream().filter(p
                                        -> p.getPagePath().equals(favorite.getUrl()))
                                .collect(Collectors.toList());
                        if (sel != null && !sel.isEmpty()) {
                            tempItems.add(favorite);
                        }
                    });
                }
            } else {
                for (WRFunItemVO wrFunItemVO : wrFunItemGroupVO.getWrFunItemVOs()) {
                    //檢測權限
                    if (hasPermission(wrFunItemVO.getPermission_role(), userPermissions)) {
                        tempItems.add(wrFunItemVO);
                        //檢測是否有該進行撈取筆數的item
                        HomeCallable homeCallable = HomeCallableService.getHomeCallableByWRFunItemComponentType(wrFunItemGroupVO,
                                wrFunItemVO.getComponent_id(), loginUserSid, depSid);
                        if (homeCallable != null) {
                            homeCallables.add(homeCallable);
                        }
                    }
                }

            }

            menuGroups.add(new WRFunItemGroupVO(wrFunItemGroupVO.getGroupSid(), wrFunItemGroupVO.getName(), wrFunItemGroupVO.getSeq(), wrFunItemGroupVO.getGroupBaseSid(), tempItems));
        }

        List<HomeCallableCondition> results = HomeCallableService.doHomeCallableList(homeCallables);

        if (results != null && !results.isEmpty()) {
            menuGroups.forEach(item -> {
                List<HomeCallableCondition> selHomeCallableConditions = results.stream().filter(p
                                -> p.getWrFunItemGroupVO().getGroupSid().equals(item.getGroupSid()))
                        .collect(Collectors.toList());
                if (selHomeCallableConditions == null || selHomeCallableConditions.size() == 0) {
                    return;
                }

                item.getWrFunItemVOs().forEach(subItems -> {
                    List<HomeCallableCondition> subSelHomeCallableCondition = selHomeCallableConditions.stream().filter(p
                                    -> p.getWrFunItemComponentType().getComponentID().equals(subItems.getComponent_id()))
                            .collect(Collectors.toList());
                    if (subSelHomeCallableCondition == null || subSelHomeCallableCondition.size() == 0) {
                        return;
                    }
                    subItems.setCountInfo(subSelHomeCallableCondition.get(0).getConntString());
                    subItems.setCssStr(subSelHomeCallableCondition.get(0).getCssString());
                });

            });
        }
        return menuGroups;
    }

    /**
     * 判斷該選單在該使用者是否有權限可顯示
     *
     * @param itemPermissionRole 該選單特殊角色才可檢閱
     * @param permission         該登入者擁有角色List
     * @return
     */
    private Boolean hasPermission(String itemPermissionRole, List<String> permission) {
        if (Strings.isNullOrEmpty(itemPermissionRole)) {
            return Boolean.TRUE;
        }
        String uRole[] = itemPermissionRole.split(",");
        for (String each : uRole) {
            if (permission.contains(each)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
}
