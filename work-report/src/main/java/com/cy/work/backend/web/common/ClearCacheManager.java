package com.cy.work.backend.web.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.collections.Lists;

import com.cy.security.utils.CacheConfigUtil;
import com.cy.system.rest.client.util.SystemCacheUtils;
import com.cy.work.backend.web.common.setting.WRFunItemGroupSetting;
import com.cy.work.backend.web.logic.cache.WRHomepageFavoriteCache;
import com.cy.work.backend.web.logic.cache.WRTagCache;
import com.cy.work.backend.web.logic.cache.WrBaseDepAccessInfoCache;
import com.cy.work.backend.web.logic.cache.WrBasePersonAccessInfoCache;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 清除快取邏輯統一放置處
 * @author allen1214_wu
 */
@Component
@Slf4j
public class ClearCacheManager {

    // ====================================
    // 服務
    // ====================================
    @Autowired
    transient private CacheConfigUtil cacheConfigUtil;
    @Autowired
    private WRHomepageFavoriteCache wrHomepageFavoriteCache;
    @Autowired
    private WRTagCache wrTagCache;
    @Autowired
    private WrBaseDepAccessInfoCache wrBaseDepAccessInfoCache;
    @Autowired
    private WrBasePersonAccessInfoCache wrBasePersonAccessInfoCache;

    // ====================================
    // 方法
    // ====================================
    /**
     * 清除所有快取
     * @return
     */
    public String clearAllCache() {

        List<String> errorMessages = Lists.newArrayList();

        try {
            this.clearSystemRestCache();
        } catch (SystemOperationException e) {
            errorMessages.add(e.getMessage());
        }

        try {
            this.clearSecurityCache();
        } catch (SystemOperationException e) {
            errorMessages.add(e.getMessage());
        }

        try {
            this.clearWorkCommonCache();
        } catch (SystemOperationException e) {
            errorMessages.add(e.getMessage());
        }

        try {
            this.clearHomepageFavoriteCache();
        } catch (SystemOperationException e) {
            errorMessages.add(e.getMessage());
        }

        try {
            this.clearTagCacheCache();
        } catch (SystemOperationException e) {
            errorMessages.add(e.getMessage());
        }

        try {
            this.clearSpcAccessCache();
        } catch (SystemOperationException e) {
            errorMessages.add(e.getMessage());
        }

        try {
            this.clearFunItemGroupCache();
        } catch (SystemOperationException e) {
            errorMessages.add(e.getMessage());
        }

        if (WkStringUtils.notEmpty(errorMessages)) {
            return String.join("<br/>", errorMessages);
        }

        log.info("所有快取清除完畢");
        return "所有快取清除完畢";
    }

    /**
     * 清除 SYSTEM-REST 快取
     * 
     * @throws SystemOperationException
     */
    public void clearSystemRestCache() throws SystemOperationException {
        long startTime = System.currentTimeMillis();
        String procTitle = "清除 SYSTEM-REST 快取";
        log.info(WkCommonUtils.prepareCostMessageStart(procTitle));
        try {
            SystemCacheUtils.getInstance().clearAllCache();
            log.info(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        } catch (Exception e) {
            String errorMessage = procTitle + "失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new SystemOperationException(errorMessage);
        }
    }

    /**
     * @throws SystemOperationException
     */
    public void clearSecurityCache() throws SystemOperationException {
        long startTime = System.currentTimeMillis();
        String procTitle = "清除 Security 快取(同步登入者最新資訊)";
        log.info(WkCommonUtils.prepareCostMessageStart(procTitle));
        try {
            cacheConfigUtil.clearAllCache();
            log.info(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        } catch (Exception e) {
            String errorMessage = procTitle + "失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new SystemOperationException(errorMessage);
        }
    }

    /**
     * 清除 SYSTEM-REST 快取
     * 
     * @throws SystemOperationException
     */
    public void clearWorkCommonCache() throws SystemOperationException {
        long startTime = System.currentTimeMillis();
        String procTitle = "清除 WK-COMMON 快取";
        log.info(WkCommonUtils.prepareCostMessageStart(procTitle));
        try {
            WkCommonCache.getInstance().clearAllCache();
            log.info(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        } catch (Exception e) {
            String errorMessage = procTitle + "失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new SystemOperationException(errorMessage);
        }
    }

    /**
     * 清除 報表快選區 快取
     * 
     * @throws SystemOperationException
     */
    public void clearHomepageFavoriteCache() throws SystemOperationException {
        long startTime = System.currentTimeMillis();
        String procTitle = "清除 報表快選區 快取";
        log.info(WkCommonUtils.prepareCostMessageStart(procTitle));
        try {
            this.wrHomepageFavoriteCache.updateCache();
            log.info(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        } catch (Exception e) {
            String errorMessage = procTitle + "失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new SystemOperationException(errorMessage);
        }
    }

    /**
     * 清除標籤快取
     * 
     * @throws SystemOperationException
     */
    public void clearTagCacheCache() throws SystemOperationException {
        long startTime = System.currentTimeMillis();
        String procTitle = "清除 標籤 快取";
        log.info(WkCommonUtils.prepareCostMessageStart(procTitle));
        try {
            this.wrTagCache.updateCache();
            log.info(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        } catch (Exception e) {
            String errorMessage = procTitle + "失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new SystemOperationException(errorMessage);
        }
    }

    /**
     * 清除特殊可閱快取
     * 
     * @throws SystemOperationException
     */
    public void clearSpcAccessCache() throws SystemOperationException {
        long startTime = System.currentTimeMillis();
        String procTitle = "清除 特殊可閱 快取";
        log.info(WkCommonUtils.prepareCostMessageStart(procTitle));
        try {
            // 部門特殊可閱
            this.wrBaseDepAccessInfoCache.updateCache();
            // 人員特殊可閱
            this.wrBasePersonAccessInfoCache.updateCache();
            log.info(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        } catch (Exception e) {
            String errorMessage = procTitle + "失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new SystemOperationException(errorMessage);
        }
    }

    /**
     * 清除功能清單快取
     * 
     * @throws SystemOperationException
     */
    public void clearFunItemGroupCache() throws SystemOperationException {
        long startTime = System.currentTimeMillis();
        String procTitle = "清除 功能清單 快取";
        log.info(WkCommonUtils.prepareCostMessageStart(procTitle));
        try {
            WRFunItemGroupSetting.clearWRFunItemGroup();
            log.info(WkCommonUtils.prepareCostMessage(startTime, procTitle));
        } catch (Exception e) {
            String errorMessage = procTitle + "失敗!" + e.getMessage();
            log.error(errorMessage, e);
            throw new SystemOperationException(errorMessage);
        }
    }

}
