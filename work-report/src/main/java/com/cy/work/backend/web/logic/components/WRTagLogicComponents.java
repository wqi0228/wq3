/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.backend.web.logic.manager.WRTagManager;
import com.cy.work.backend.web.logic.manager.WorkSettingUserManager;
import com.cy.work.backend.web.logic.utils.ToolsDate;
import com.cy.work.backend.web.view.vo.TagSearchVO;
import com.cy.work.backend.web.vo.WRTag;
import com.cy.work.backend.web.vo.converter.to.UserDep;
import com.cy.work.backend.web.vo.converter.to.UserDepTo;
import com.cy.work.backend.web.vo.enums.SimpleDateFormatEnum;
import com.cy.work.backend.web.vo.enums.WRTagCategoryType;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 標籤邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WRTagLogicComponents implements InitializingBean, Serializable {

    
    private static final long serialVersionUID = -1521413967964976261L;

    @Autowired
    private WRTagManager wRTagManager;
    @Autowired
    private WorkSettingUserManager workSettingUserManager;

    private static WRTagLogicComponents instance;

    public static WRTagLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WRTagLogicComponents.instance = this;
    }

    public WRTag getWRTagBySid(String sid) {
        return wRTagManager.getWRTagBySid(sid);
    }

    /**
     * 取得標籤List
     *
     * @return
     */
    public List<WRTag> getWRTags(Integer compSid) {
        try {
            List<WRTag> result = wRTagManager.getWRTags(compSid);
            return result;
        } catch (Exception e) {
            log.error("getWRTags Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }

    /**
     * 取得標籤List
     *
     * @return
     */
    public List<WRTag> getActiveWRTag(Integer loginUserDepSid, Integer compSid) {
        try {
            List<WRTag> result = wRTagManager.getWRTagActive(loginUserDepSid, compSid);
            return result;
        } catch (Exception e) {
            log.error("getWRTagByStatus Error : " + e.toString(), e);
            return Lists.newArrayList();
        }
    }

    public List<TagSearchVO> getTagSearch(String statusID, String tagName, String tagID, Integer compSid) {
        List<TagSearchVO> tagSearchs = Lists.newArrayList();
        wRTagManager.getWRTagsSearch(statusID, tagName, tagID, compSid).forEach(item -> {
            User crateUser = workSettingUserManager.findBySID(item.getCreate_usr_sid());
            String statusStr = (Activation.ACTIVE.equals(item.getStatus())) ? "正常" : "停用";

            TagSearchVO tagSearchVO = new TagSearchVO(
                    item.getSid(),
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getCreate_dt()),
                    crateUser.getName(),
                    item.getCategory().getVal(),
                    item.getTag_name(),
                    statusStr,
                    ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdate_dt()),
                    String.valueOf(item.getSeq())
            );
            tagSearchs.add(tagSearchVO);
        });
        return tagSearchs;
    }

    public Integer getMaxTagSeq(Integer compSid) {
        Integer maxSeq = 0;
        for (WRTag wRTag : wRTagManager.getWRTags(compSid)) {
            if (wRTag.getSeq() > maxSeq) {
                maxSeq = wRTag.getSeq();
            }
        }
        return maxSeq;
    }

    public void saveTag(String sid, String tagName, String selectTagCategoryID,
            String statusID, String memo, List<Org> selectOrg, String seqStr, Integer loginUserSid, Integer compSid) {
        if (Strings.isNullOrEmpty(tagName)) {
            Preconditions.checkState(false, "標簽名稱為必要輸入欄位，請確認！！");
        }
        if (Strings.isNullOrEmpty(selectTagCategoryID)) {
            Preconditions.checkState(false, "可使用的模組為必要輸入欄位，請確認！！");
        }
        if (Strings.isNullOrEmpty(statusID)) {
            Preconditions.checkState(false, "狀態為必要輸入欄位，請確認！！");
        }
        if (Strings.isNullOrEmpty(seqStr)) {
            Preconditions.checkState(false, "排序序號為必要輸入欄位，請確認！！");
        }
        wRTagManager.getWRTags(compSid).forEach(item -> {
            if (item.getTag_name().equals(tagName)) {
                if (!item.getSid().equals(sid)) {
                    Preconditions.checkState(false, "標籤名稱有重覆，請重新輸入！！");
                }
            }
        });
        Integer seq = Integer.valueOf(seqStr);
        wRTagManager.getWRTags(compSid).forEach(item -> {
            if (item.getSeq().equals(seq)) {
                if (!item.getSid().equals(sid)) {
                    Preconditions.checkState(false, "排序序號已與[" + item.getTag_name() + "]重覆，請重新輸入,目前最大可用序號:[" + String.valueOf((getMaxTagSeq(compSid) + 1)) + "]");
                }
            }
        });
        WRTag selWRTag = wRTagManager.getWRTagBySid(sid);
        if (selWRTag == null && !Strings.isNullOrEmpty(sid)) {
            Preconditions.checkState(false, "取得標籤資料有誤！！");
        }
        if (selWRTag == null) {
            selWRTag = new WRTag();
        }
        selWRTag.setCategory(WRTagCategoryType.valueOf(selectTagCategoryID));
        selWRTag.setStatus(Activation.valueOf(statusID));
        selWRTag.setTag_name(tagName);
        selWRTag.setMemo(memo);
        selWRTag.setSeq(seq);
        UserDep userDep = new UserDep();
        List<UserDepTo> userDepTos = Lists.newArrayList();
        selectOrg.forEach(item -> {
            UserDepTo udt = new UserDepTo();
            udt.setDepSid(String.valueOf(item.getSid()));
            userDepTos.add(udt);
        });
        userDep.setUserDepTos(userDepTos);
        selWRTag.setUse_dep(userDep);
        if (Strings.isNullOrEmpty(sid)) {
            wRTagManager.createWRTag(selWRTag, loginUserSid, compSid);
        } else {
            wRTagManager.updateWRTag(selWRTag, loginUserSid);
        }
    }
}
