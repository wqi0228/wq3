package com.cy.work;

import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.omnifaces.exceptionhandler.FullAjaxExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WorkFullAjaxExceptionHandler extends FullAjaxExceptionHandler {

    /**
     * @param wrapped
     */
    public WorkFullAjaxExceptionHandler(ExceptionHandler wrapped) {
        super(wrapped);
    }

    /**
     * Overwrite log 處理方法
     */
    @Override
    protected void logException(FacesContext context, Throwable exception, String location, String message, Object... parameters) {
        // ====================================
        // 處理 ViewExpiredException 污染 log 的問題 （寫太多不必要 log）
        // ====================================
        if (exception instanceof ViewExpiredException) {
            // With exception==null, no trace will be logged.
            log.info("ViewExpiredException:" + exception.getMessage());
            super.logException(context, null, location, message, parameters);
        }
        // ====================================
        // 處理 FullAjaxException 不會在專案寫 log 的問題
        // ====================================
        else {

            if ("Cannot change buffer size after data has been written".equals(exception.getMessage())) {
                // 此狀況縮短 log
                log.error("\r\nFullAjaxExceptionHandler:" + exception.getMessage());
            } else {

                try {
                    HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                    log.error("請求網址:" + origRequest.getRequestURL());

                } catch (Exception e) {
                    log.error("嘗試取得 Request 失敗!" + e.getMessage(), e);
                }

                log.error("\r\nFullAjaxExceptionHandler:" + exception.getMessage(), exception);
            }

            super.logException(context, exception, location, message, parameters);
        }
    }

}
