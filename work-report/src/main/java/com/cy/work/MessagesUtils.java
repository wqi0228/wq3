/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Shaun
 */
@Slf4j
public class MessagesUtils {

    /**
     * 訊息視窗
     * 
     * @param msg
     */
    public static void showInfo(String msg) {
        if (isDuplicateMsg(msg)) {
            return;
        }
        // WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "\r\n info
        // -------------------" + msg);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
        RequestContext.getCurrentInstance().update("common-msg");
    }

    /**
     * 警示視窗
     * 
     * @param msg
     */
    public static void showWarn(String msg) {
        if (isDuplicateMsg(msg)) {
            return;
        }
        // WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "\r\n Warn
        // -------------------" + msg);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, msg, msg));
        RequestContext.getCurrentInstance().update("common-msg");
    }

    /**
     * 錯誤視窗
     * 
     * @param msg
     */
    public static void showError(String msg) {
        if (isDuplicateMsg(msg)) {
            return;
        }
        // WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "\r\n error
        // -------------------" + msg);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
        RequestContext.getCurrentInstance().update("common-msg");
    }

    private static boolean isDuplicateMsg(String msg) {
        return FacesContext.getCurrentInstance().getMessageList().stream()
                .anyMatch(facesMessages -> facesMessages.getDetail().equals(msg));
    }

    public static void show(UserMessageException e) {
        show(e.getMessage(), e.getExceptionMessage(), e.getLevel());
    }
    
    public static void show(SystemOperationException e) {
        show(e.getMessage(), e.getExceptionMessage(), e.getLevel());
    }

    /**
     * 顯示 UserMessageException
     * 
     * @param e
     */
    private static void show(String message, String exceptionMessage, InfomationLevel level) {

        if (isDuplicateMsg(message)) {
            return;
        }

        FacesMessage facesMessage = null;
        switch (level) {
        case INFO:
            facesMessage = new FacesMessage(message);
            break;

        case WARN:
            facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, message, message);
            if (!WkCommonUtils.compareByStr(message, exceptionMessage)) {
                log.warn(exceptionMessage);
            }
            break;

        case ERROR:
            facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
            if (!WkCommonUtils.compareByStr(message, exceptionMessage)) {
                log.warn(exceptionMessage);
            }
            break;

        default:
            log.error("無法處理 Exception 的 level !:" + level);
            facesMessage = new FacesMessage(message);
            break;
        }

        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        RequestContext.getCurrentInstance().update("common-msg");
    }
}
