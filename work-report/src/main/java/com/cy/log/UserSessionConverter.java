package com.cy.log;

import java.util.List;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * @author allen1214_wu
 *
 */
public class UserSessionConverter extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent event) {

        List<String> showContents = Lists.newArrayList();

        // user id
        String userId = WkStringUtils.safeTrim(SecurityFacade.getUserId());
        if (WkStringUtils.notEmpty(userId)) {
            showContents.add(userId);
        }

        // session 識別
        RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
        if (attrs != null) {
            String code = WkStringUtils.safeTrim(attrs.getSessionId());
            code = code.hashCode() + "";
            if (code.length() > 5) {
                code = code.substring(code.length() - 5);
            }
            if (WkStringUtils.notEmpty(code)) {
                showContents.add(code);
            }
        }

        if (showContents.size() > 0) {
            return "[" + String.join(" ", showContents) + "]";
        }

        return "";
    }
}
