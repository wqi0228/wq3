package com.cy.log;

import ch.qos.logback.classic.PatternLayout;

/**
 * @author allen1214_wu
 */
public class PatternLayoutWithUserContext extends PatternLayout {
    static {
        PatternLayout.defaultConverterMap.put(
                "user", UserConverter.class.getName());
        PatternLayout.defaultConverterMap.put(
                "session", SessionConverter.class.getName());
        PatternLayout.defaultConverterMap.put(
                "usersession", UserSessionConverter.class.getName());
    }
}