package com.cy.log;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.cy.work.common.utils.WkStringUtils;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

public class SessionConverter extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent event) {
        RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
        if (attrs != null) {
            String code = WkStringUtils.safeTrim(attrs.getSessionId());
            code = code.hashCode() + "";
            if (code.length() > 5) {
                code = code.substring(code.length() - 5);
            }
            return code;
        }
        return "LOCAL";
    }
}
