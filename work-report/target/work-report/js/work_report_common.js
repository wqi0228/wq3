/**
 * 
 */
//將	Wizard元件導回第一個
function resetWizard(wiz) {
	wiz.loadStep(wiz.cfg.steps[0], true);
}
function handleLoginRequest(xhr, status, args) {
	if (!args.validationFailed) {
		dlgAddProgramClass.hide();
	}
}
//驗證通過才關閉Dialog
function closeDialog(xhr, status, args, dlg) {
	if (!args.validationFailed) {
		dlg.hide();
	}
}
//清除 primefaces datatable filter 
function cleanFilter(datatableWidgetVar) {
	datatableWidgetVar.clearFilters();
}
function handleComplete(xhr, status, args) {
	if (args.validationFailed) {
		PrimeFaces.debug("Validation Failed");
	} else {
		PrimeFaces.debug("Save:" + args.saved);
		PrimeFaces.debug("FirstName: " + args.user.firstname + ", Lastname: " + args.user.lastname);
	}
}
PrimeFaces.locales['zh_TW'] = {
	closeText: '關閉',
	prevText: '上個月',
	nextText: '下個月',
	currentText: '今天',
	monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
	monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
	dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
	dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
	dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
	weekHeader: '周',
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: true,
	yearSuffix: '', // 年
	timeOnlyTitle: '僅時間',
	timeText: '時間',
	hourText: '時',
	minuteText: '分',
	secondText: '秒',
	ampm: false,
	month: '月',
	week: '周',
	day: '日',
	allDayText: '全天'
};

//凍結窗格
function fixedCol(tableId) {
	var oTable2 = $('#' + tableId).find('table').dataTable({
		"sScrollX": "600", //Scroll
		"sScrollY": "550",
		"bAutoWidth": false,
		"bFilter": false,
		"bScrollCollapse": true,
		"bPaginate": false,
		"bSort": false,
		"bInfo": false
	});

	var oFC = new FixedColumns(oTable2, {
		"iLeftColumns": 1, //Freezed first for columns
		"sHeightMatch": "auto"
	});
}
//選取表格變色
function selectedColor(tid, split) {
	$("#" + tid + " td").click(function() {
		$("#" + tid + " td").removeClass("ui-state-highlight");
		var col = $(this).parent().children().index($(this));
		if (col < split)
			$(this).parent().children("td:lt(" + split + ")").addClass("ui-state-highlight");
		else
			$(this).parent().children("td" + (split != 0 ? ":gt(" + (split - 1) + ")" : "")).addClass("ui-state-highlight");
	});
}
function validateNum(e, limit) {
	var pattern = "\\d+[.]?\\d{0," + limit + "}";
	var fullPattern = "^" + pattern + "$";//加上頭尾符號
	var regexp = new RegExp(fullPattern);
	var rep = e.value.replace(/\,/g, "");//避免有逗號被清空
	if (!regexp.test(rep)) {
		regexp = new RegExp(pattern);
		e.value = regexp.exec(rep);
	}
	return false;
}
var win;
function openInNewTab(url) {
	win = window.open(url, '_blank');
}
function replaceUrl(url) {
	win.location = url;
}
$(function() {
	//除了input text外，停用Backsapce，防止不慎回前一頁
	$(document).keydown(function(e) {
		if (e.which == 8 && !(e.target.type == "text" || e.target.type == "textarea" || e.target.type == "password")) {
			e.preventDefault();
		}
		//除了textarea外，停用ENTER，以防觸發button
		if (e.which == 13 && e.target.type != "textarea") {
			e.preventDefault();
		}
	});
});

//倒數計時
function countDown(obj, timeStr, callMethod) {
	let targetElm = $("#" + obj);
	if (targetElm.length == 0) {
		return;
	}

	targetElm.countdown(timeStr)
		.on('update.countdown', function(event) {
			$(this).html(event.strftime('%M:%S'));
		})
		.on('finish.countdown', function(event) {
			let jsMethod = new Function(callMethod);
			jsMethod();
		});
}

function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
}

//取得元件
function getElementId(obj) {
	return $(document.getElementById(obj));
}
//取得元件
function getElementIdAndCssName(obj, cssName) {
	return $(document.getElementById(obj).getElementsByClassName(cssName));
}

function checkCodeForInt(obj) {
	if (obj.value != "") {
		if (!/^\d+$/.test(obj.value)) {
			checkErrCode();
		}
	}
}

//選取表格變色
function selectedMenu(tid) {
	$("#" + tid + " li").click(function() {
		$("#" + tid + " li").children().removeClass("ui-state-highlight");
		$(this).children().addClass("ui-state-highlight");
	});
}

function removeSticky() {
	$("body").children("div").each(function() {
		if ($(this).hasClass("ui-datatable-sticky")) {
			$(this).remove();
		}
	});
}
function moveScrollTop() {
	$(window).scrollTop("0");
}
jQuery(window).load(function() {
	reEditor();
});
function reEditor() {
	$("form").find(".ui-editor").each(function() {
		var widthLenght = $(this).width() - 30;
		var heightLenght = $(this).height() - 35;
		$(this).find('iframe').contents().find('body').css({
			'height': '200px',
			'word-wrap': 'break-word',
			'display': 'block',
			'white-space': 'pre-wrap',
			'width': widthLenght,
			'height': heightLenght,
			'word-break': 'break-word'
		});
	});
}

//選取dataTable 分頁位置
function selectDataTablePage(obj, page) {
	if (checkWidgets(obj)) {
		PF(obj).getPaginator().setPage(page);
	}
}

//editor元件存入內容
function saveHTML(obj) {
	if (checkWidgets(obj)) {
		PF(obj).saveHTML();
	}
}
//檢查widgets是否存在
function checkWidgets(obj) {
	if (PrimeFaces.widgets[obj]) {
		return true;
	}
	console.error("查無 widgetVarl：" + obj)
	return false;
}

function openAllTab(viewPanelBottomInfoTabId, widgetVar, widgetId) {

	$('#' + viewPanelBottomInfoTabId + '\\:' + widgetId).children(".ui-accordion-header").each(function(index) {
		PF(widgetVar).select(index);
	});
}

function closeAllTab(viewPanelBottomInfoTabId, widgetVar, widgetId) {
	$('#' + viewPanelBottomInfoTabId + '\\:' + widgetId).children(".ui-accordion-header").each(function(index) {
		PF(widgetVar).unselect(index);
	});
}