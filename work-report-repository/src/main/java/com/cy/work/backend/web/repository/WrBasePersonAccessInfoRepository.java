/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WrBasePersonAccessInfo;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author brain0925_liao
 */
public interface WrBasePersonAccessInfoRepository extends JpaRepository<WrBasePersonAccessInfo, String>, Serializable{
    
}
