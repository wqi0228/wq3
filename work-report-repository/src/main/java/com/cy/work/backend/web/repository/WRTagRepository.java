/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRTag;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * 工作報告標籤
 *
 * @author brain0925_liao
 */
public interface WRTagRepository extends JpaRepository<WRTag, String>, Serializable {

    /** 搜尋標籤資料 */
    @Query("SELECT o FROM #{#entityName} o WHERE o.status=0 ")
    public List<WRTag> getWRTagByStatus();
}
