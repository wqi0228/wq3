/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRTrace;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 工作報告追蹤
 * @author brain0925_liao
 */
public interface WRTraceRepository  extends JpaRepository<WRTrace, String> ,Serializable{
    /** 搜尋工作報告追蹤 BY 工作報告Sid*/
    @Query("SELECT o FROM #{#entityName} o WHERE o.status=0 and o.wr_sid = :wr_sid ORDER BY o.update_dt DESC ")
    public List<WRTrace> getWRTraceByWrSid( @Param("wr_sid") String wr_sid);
    
    
}
