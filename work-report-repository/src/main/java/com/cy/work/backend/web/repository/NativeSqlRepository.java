package com.cy.work.backend.web.repository;

import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.engine.jdbc.internal.BasicFormatterImpl;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class NativeSqlRepository {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * @param <T>
     * @param sql
     * @param parameters
     * @param resultClass
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> getResultList(String sql, Map<String, Object> parameters, Class<T> resultClass) {

        try {
            // ====================================
            // 建立查詢物件
            // ====================================
            sql = new BasicFormatterImpl().format(sql);
            Query query = entityManager.createNativeQuery(sql);
            // 組裝參數
            if (parameters != null) {
                for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                    query.setParameter(entry.getKey(), entry.getValue());
                }
            }

            // ====================================
            // 查詢
            // ====================================
            return query.unwrap(org.hibernate.Query.class)
                    .setResultTransformer(Transformers.aliasToBean(resultClass))
                    .list();

        } catch (Throwable e) {
            log.error("執行 SQL 時發生錯誤!"
                    + "\r\nMessage:【" + e.getMessage() + "】"
                    + "\r\nSQL:【\r\n" + sql + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                    + "\r\n");
            throw e;
        }
    }

    /**
     * @param <conditionKey>
     * @param tableName
     * @param selectFieldNames
     * @param whereFieldName
     * @param conditionValues
     * @return
     * @throws SystemDevelopException
     */
    @SuppressWarnings("unchecked")
    public <conditionKey> Map<conditionKey, Map<String, Object>> selectMapBySingleKeyField(
            String tableName,
            List<String> selectFieldNames,
            String whereFieldName,
            Collection<conditionKey> conditionValues) {

        // ====================================
        // 檢查
        // ====================================
        if (WkStringUtils.isEmpty(selectFieldNames)) {
            throw new RuntimeException("未傳入查詢欄位 selectFieldNames ");
        }

        if (WkStringUtils.isEmpty(conditionValues)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 處理 key
        // ====================================
        conditionValues = conditionValues.stream()
                .distinct()
                .collect(Collectors.toList());

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT  ");
        for (int i = 0; i < selectFieldNames.size(); i++) {
            String selectField = selectFieldNames.get(i);
            sql.append("       " + selectField + " as sfield_" + i + ", ");
        }
        sql.append("       " + whereFieldName + " as condition_key ");
        sql.append("FROM   " + tableName + " ");
        sql.append("WHERE  " + whereFieldName + " IN (:conditionValues) ");

        // ====================================
        // 查詢
        // ====================================
        List<Object[]> results = null;
        try {
            // 建立查詢物件
            Query query = entityManager.createNativeQuery(sql.toString());
            query.setParameter("conditionValues", conditionValues);

            // 查詢
            results = query.getResultList();
        } catch (Throwable e) {
            log.error("執行 SQL 時發生錯誤!"
                    + "\r\nMessage:【" + e.getMessage() + "】"
                    + "\r\nSQL:【\r\n" + new BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + WkJsonUtils.getInstance().toPettyJson(conditionValues) + "\r\n】"
                    + "\r\n");
            throw e;
        }

        if (WkStringUtils.isEmpty(results)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 整理查詢結果
        // ====================================
        // 回傳結果的資料容器
        Map<conditionKey, Map<String, Object>> resultMap = Maps.newHashMap();

        for (Object[] fields : results) {

            // 最後一欄為 selectKey
            conditionKey condition_key = (conditionKey) fields[fields.length - 1];

            if (resultMap.containsKey(condition_key)) {
                throw new RuntimeException("查詢結果有重複的 key 值");
            }

            // 組裝 row 資料
            // Map<selectField , fieldData>
            Map<String, Object> rowDataMap = Maps.newHashMap();
            for (int i = 0; i < selectFieldNames.size(); i++) {
                String selectFieldName = selectFieldNames.get(i);
                rowDataMap.put(selectFieldName, fields[i]);
            }

            resultMap.put(condition_key, rowDataMap);
        }
        return resultMap;
    }

    /**
     * @param <T>
     * @param tableName
     * @param selectFieldNames
     * @param whereFieldName
     * @param conditionValues
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> List<Map<String, Object>> selectRowsBySingleField(
            String tableName,
            List<String> selectFieldNames,
            String whereFieldName,
            Collection<T> conditionValues) {

        // ====================================
        // 檢查
        // ====================================
        if (WkStringUtils.isEmpty(selectFieldNames)) {
            throw new RuntimeException("未傳入查詢欄位 selectFieldNames ");
        }

        if (WkStringUtils.isEmpty(conditionValues)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 處理 key
        // ====================================
        conditionValues = conditionValues.stream()
                .distinct()
                .collect(Collectors.toList());

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT  ");
        for (int i = 0; i < selectFieldNames.size(); i++) {
            String selectField = selectFieldNames.get(i);
            sql.append("       " + selectField + " as sfield_" + i + ", ");
        }
        sql.append("       " + whereFieldName + " as condition_key ");
        sql.append("FROM   " + tableName + " ");
        sql.append("WHERE  " + whereFieldName + " IN (:conditionValues) ");

        // ====================================
        // 查詢
        // ====================================
        List<Object[]> results = null;
        try {
            // 建立查詢物件
            Query query = entityManager.createNativeQuery(sql.toString());
            query.setParameter("conditionValues", conditionValues);

            // 查詢
            results = query.getResultList();
        } catch (Throwable e) {
            log.error("執行 SQL 時發生錯誤!"
                    + "\r\nMessage:【" + e.getMessage() + "】"
                    + "\r\nSQL:【\r\n" + new BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + WkJsonUtils.getInstance().toPettyJson(conditionValues) + "\r\n】"
                    + "\r\n");
            throw e;
        }

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 整理查詢結果
        // ====================================
        // 回傳結果的資料容器
        List<Map<String, Object>> dataRowMap = Lists.newArrayList();

        for (Object[] fields : results) {

            // 組裝 row 資料
            // Map<selectField , fieldData>
            Map<String, Object> rowDataMap = Maps.newHashMap();
            for (int i = 0; i < selectFieldNames.size(); i++) {
                String selectFieldName = selectFieldNames.get(i);
                rowDataMap.put(selectFieldName, fields[i]);
            }

            dataRowMap.add(rowDataMap);
        }
        return dataRowMap;
    }

    /**
     * execute Native SQL
     *
     * @param sql
     * @param parameters
     * @return
     */
    public int executeNativeSql(String sql, Map<String, Object> parameters) {
        try {
            // ====================================
            // 建立查詢物件
            // ====================================
            sql = new BasicFormatterImpl().format(sql);
            Query query = entityManager.createNativeQuery(sql);
            // 組裝參數
            if (parameters != null) {
                for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                    query.setParameter(entry.getKey(), entry.getValue());
                }
            }

            // ====================================
            // 執行
            // ====================================
            return query.executeUpdate();

        } catch (Throwable e) {
            log.error("執行 SQL 時發生錯誤!"
                    + "\r\nMessage:【" + e.getMessage() + "】"
                    + "\r\nSQL:【\r\n" + sql + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                    + "\r\n");
            throw e;
        }
    }
}
