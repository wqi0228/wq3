/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRFavorite;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 工作報告 - 收藏
 *
 * @author brain0925_liao
 */
public interface WRFavoriteRepository extends JpaRepository<WRFavorite, String>, Serializable {

    /**
     * 搜尋收藏紀錄by 使用者與工作報告Sid
     */
    @Query("SELECT r FROM #{#entityName} r WHERE r.create_usr = :create_usr  AND r.wr_sid = :wr_sid")
    List<WRFavorite> findByUserSidAndWRSid(@Param("wr_sid") String wr_sid, @Param("create_usr") Integer create_usr);

    /**
     * 搜尋收藏紀錄by 工作報告Sid
     */
    @Query("SELECT r FROM #{#entityName} r WHERE  r.wr_sid = :wr_sid AND r.status = 0 ")
    List<WRFavorite> findByWRSid(@Param("wr_sid") String wr_sid);

    @Query("SELECT r FROM #{#entityName} r WHERE r.create_usr = :create_usr  AND r.status = 0 ")
    List<WRFavorite> findByUserSid(@Param("create_usr") Integer create_usr);

    /**
     * 搜尋收藏紀錄筆數 By 收藏時間區間及使用者
     */
    @Query("SELECT COUNT(r) FROM #{#entityName} r WHERE r.create_usr = :create_usr AND r.status = 0 AND r.update_dt >= :startDate AND r.update_dt <= :endDate ")
    Integer findFavoriteCountByUserSidAndUpdateDt(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
                                                  @Param("create_usr") Integer create_usr);

}
