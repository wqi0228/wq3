/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRInboxSendGroup;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 工作報告轉寄Group
 * @author brain0925_liao
 */
public interface WRInboxSendGroupRepository extends JpaRepository<WRInboxSendGroup, String>, Serializable {
    /**搜尋工作報告轉寄Group By 工作報告Sid及使用者*/
    @Query("SELECT r FROM #{#entityName} r WHERE r.wr_sid = :wr_sid  AND r.create_usr = :create_usr  AND r.status = 0 ORDER BY r.create_dt DESC ")
    public List<WRInboxSendGroup> findByUserSidAndWRSid(@Param("wr_sid") String wr_sid,@Param("create_usr") Integer create_usr);
    
     @Query("SELECT r FROM #{#entityName} r WHERE r.wr_sid = :wr_sid AND r.status = 0 ORDER BY r.create_dt DESC ")
    public List<WRInboxSendGroup> findByWRSid(@Param("wr_sid") String wr_sid);
}
