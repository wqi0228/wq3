/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WrBaseDepAccessInfo;
import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author brain0925_liao
 */
public interface WrBaseDepAccessInfoRepository extends JpaRepository<WrBaseDepAccessInfo, String>, Serializable{

    List<WrBaseDepAccessInfo> findAllByOtherBaseAccessViewDepNotNull();

    List<WrBaseDepAccessInfo> findAllByLimitBaseAccessViewDepNotNull();
}
