package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WrReadRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface WrReadRecordRepository extends JpaRepository<WrReadRecord, String>, Serializable {

    List<WrReadRecord> findByWrSid(String wrSid);

    List<WrReadRecord> findByCreateUser(String userSid);


    List<WrReadRecord> findAllByWrSidAndCreateUserIn(String wrSid, List<String> userSids);


    @Transactional
    @Modifying
    @Query("UPDATE WrReadRecord wr SET wr.readStatus = :readStatus, wr.updateDate = :updateDate WHERE wr.wrSid = :wrSid AND wr.createUser = :userSid")
    void updateReadStatus(@Param("wrSid") String wrSid, @Param("userSid") String userSid, @Param("readStatus") String readStatus, @Param("updateDate") Date updateDate);

    Optional<WrReadRecord> findByCreateUserAndWrSid(String userSid, String wrSid);

    Optional<WrReadRecord> findByWrSidAndHistoryNotNull(String wrSid);
}
