/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WorkTraceInfo;
import com.cy.work.backend.web.vo.enums.TraceStatus;
import com.cy.work.backend.web.vo.enums.TraceType;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author brain0925_liao
 */
public interface WorkTraceInfoRepository extends JpaRepository<WorkTraceInfo, String>, Serializable {

    @Query("SELECT o FROM #{#entityName} o WHERE  o.status = 0 "
            + "AND o.trace_source_sid = :trace_source_sid AND o.trace_source_type = :trace_source_type AND o.trace_status in :trace_status AND o.trace_type in :trace_type")
    public List<WorkTraceInfo> getWorkTraceInfoByTraceSourceTypeAndTraceSourceSidAndTraceStatusAndTraceType(
            @Param("trace_source_sid") String trace_source_sid, @Param("trace_source_type") String trace_source_type, @Param("trace_status") List<TraceStatus> trace_status, @Param("trace_type") List<TraceType> trace_type);

    @Query("SELECT o FROM #{#entityName} o WHERE  o.status = 0 "
            + "AND o.trace_source_sid = :trace_source_sid AND o.trace_source_type = :trace_source_type ")
    public List<WorkTraceInfo> getWorkTraceInfoByTraceSourceSiidAndyTraceSourceType(
            @Param("trace_source_sid") String trace_source_sid, @Param("trace_source_type") String trace_source_type);
    
      @Query("SELECT o FROM #{#entityName} o WHERE  o.status = 0 "
            + "AND o.trace_usr = :trace_usr AND o.trace_source_type = :trace_source_type ")
    public List<WorkTraceInfo> getWorkTraceInfoByTraceUsrSidAndyTraceSourceType(
            @Param("trace_usr") Integer trace_usr, @Param("trace_source_type") String trace_source_type);
    
      @Query("SELECT o FROM #{#entityName} o WHERE  o.status = 0 "
            + "AND o.trace_dep = :trace_dep AND o.trace_source_type = :trace_source_type ")
    public List<WorkTraceInfo> getWorkTraceInfoByTraceDepSidAndyTraceSourceType(
            @Param("trace_dep") Integer trace_dep, @Param("trace_source_type") String trace_source_type);

    @Query("SELECT COUNT(o) FROM #{#entityName} o WHERE  o.status = 0 "
            + "AND o.trace_source_sid = :trace_source_sid AND o.trace_source_type = :trace_source_type AND o.trace_status in :trace_status AND o.trace_type in :trace_type AND o.trace_usr = :trace_usr")
    public Integer getWorkTraceInfoCountByTraceSourceTypeAndTraceSourceSidAndTraceStatusAndTraceTypePerson(@Param("trace_source_sid") String trace_source_sid, @Param("trace_source_type") String trace_source_type, @Param("trace_status") List<TraceStatus> trace_status, @Param("trace_type") List<TraceType> trace_type, @Param("trace_usr") Integer trace_usr);

    @Query("SELECT COUNT(o) FROM #{#entityName} o WHERE  o.status = 0 "
            + "AND o.trace_source_sid = :trace_source_sid AND o.trace_source_type = :trace_source_type AND o.trace_status in :trace_status AND o.trace_type in :trace_type AND o.trace_dep = :trace_dep")
    public Integer getWorkTraceInfoCountByTraceSourceTypeAndTraceSourceSidAndTraceStatusAndTraceTypeDep(@Param("trace_source_sid") String trace_source_sid, @Param("trace_source_type") String trace_source_type, @Param("trace_status") List<TraceStatus> trace_status, @Param("trace_type") List<TraceType> trace_type, @Param("trace_dep") Integer trace_dep);
}
