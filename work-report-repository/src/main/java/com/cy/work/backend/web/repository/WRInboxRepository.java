/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRInbox;
import com.cy.work.backend.web.vo.enums.ForwardType;
import com.cy.work.backend.web.vo.enums.ReadRecordStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 轉寄細項
 *
 * @author brain0925_liao
 */
public interface WRInboxRepository extends JpaRepository<WRInbox, String>, Serializable {

    /**
     * 搜尋轉寄細項 By 工作報告Sid及轉寄類型
     */
    @Query("SELECT r FROM #{#entityName} r WHERE r.wr_sid = :wr_sid AND r.forward_type = :forward_type  ORDER BY r.status ASC,r.update_dt DESC  ")
    List<WRInbox> findByInboxByWrSidAndForwardType(@Param("wr_sid") String wr_sid, @Param("forward_type") ForwardType forwardType);

    /**
     * 搜尋轉寄細項 By 工作報告Sid
     */
    @Query("SELECT r FROM #{#entityName} r WHERE r.wr_sid = :wr_sid AND r.status = 0  ")
    List<WRInbox> findByInboxByWrSidAndStatusActive(@Param("wr_sid") String wr_sid);

    /**
     * 搜尋轉寄細項 By 轉寄細項Sids
     */
    @Query("SELECT r FROM #{#entityName} r WHERE r.sid in :sids  ")
    List<WRInbox> findByInboxBySids(@Param("sids") List<String> sids);

    @Query("SELECT r FROM #{#entityName} r WHERE r.receive = :receive  ")
    List<WRInbox> findByReceiveSid(@Param("receive") Integer receviceSid);

    @Query("SELECT r FROM #{#entityName} r WHERE r.receive_dep = :receive_dep  ")
    List<WRInbox> findByReceiveDepSid(@Param("receive_dep") Integer receive_dep);

    /**
     * 搜尋轉寄細項 By 轉寄大項Sid
     */
    @Query("SELECT r FROM #{#entityName} r WHERE r.inbox_group_sid = :inbox_group_sid  ORDER BY r.status ASC,r.update_dt DESC,r.read_dt ASC")
    List<WRInbox> findByInboxByInboxGroupSid(@Param("inbox_group_sid") String inbox_group_sid);

    /**
     * 搜尋轉寄細項 By 轉寄類型 及 接收者
     */
    @Query("SELECT r FROM #{#entityName} r WHERE r.wr_sid = :wr_sid AND r.forward_type = :forward_type  AND r.receive = :receive AND r.status = 0 ORDER BY r.update_dt DESC   ")
    List<WRInbox> findByInboxByWrSidAndForwardTypeAndReceiveSid(@Param("wr_sid") String wr_sid, @Param("forward_type") ForwardType forwardType, @Param("receive") Integer receive);

    /**
     * 更新轉寄細項讀取狀態及讀取時間
     */
    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.read_record  = :readed,r.read_dt = :read_dt "
            + " WHERE r.status = 0 AND r.wr_sid = :wr_sid AND r.read_record = :read_record AND ((r.receive_dep = :receive_dep AND r.forward_type = :depforwardType) OR (r.receive = :receive AND r.forward_type = :personforwardType)) ")
    int updateReadRecordStatus(@Param("readed") ReadRecordStatus readed, @Param("read_dt") Date read_dt, @Param("wr_sid") String wr_sid,
                               @Param("read_record") ReadRecordStatus read_record, @Param("receive") Integer receive, @Param("receive_dep") Integer receive_dep, @Param("depforwardType") ForwardType depforwardType, @Param("personforwardType") ForwardType personforwardType);
}
