/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRHomepageFavorite;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *工作報告 - 報表快選區
 * @author brain0925_liao
 */
public interface WRHomepageFavoriteRepository extends JpaRepository<WRHomepageFavorite, String>, Serializable {
    /**搜尋報表快選區紀錄 By 使用
     * @param usersid者*/
    @Query("SELECT r FROM #{#entityName} r WHERE r.usersid = :usersid ")
    public List<WRHomepageFavorite> findByUserSid(@Param("usersid") Integer usersid);
}
