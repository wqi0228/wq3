/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRGroupSetupPrivate;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 群組
 * @author brain0925_liao
 */
public interface WRGroupSetupPrivateRepository extends JpaRepository<WRGroupSetupPrivate, String>, Serializable {
 
    /** 搜尋群組 By Type及使用者*/
    @Query("SELECT o FROM #{#entityName} o WHERE  o.type = :type "
            + "AND o.create_usr = :create_usr")
    public List<WRGroupSetupPrivate> getWRGroupSetupPrivateByCreateUsr(
            @Param("create_usr") Integer create_usr,@Param("type") String type);
}
