/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRMaster;
import com.cy.work.backend.web.vo.converter.to.ReadReceiptsMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 工作報告主檔
 *
 * @author brain0925_liao
 */
public interface WRMasterRepository extends JpaRepository<WRMaster, String>, Serializable {

    @Query("SELECT r FROM #{#entityName} r WHERE r.create_usr = :create_usr order by r.create_dt DESC ")
    List<WRMaster> findByCreateUserSid(@Param("create_usr") Integer create_usr);

    /**
     * 搜尋[提交或作廢]工作單號 By 建立日期區間
     */
    @Query("SELECT wr_no FROM #{#entityName} r WHERE r.create_dt BETWEEN :start AND :end AND r.comp_sid = :companySid AND r.wr_status <> 'DRAFT'  AND r.wr_status <> 'DELETE' order by r.create_dt DESC")
    List<String> findTodayLastWRMaster(@Param("start") Date start, @Param("end") Date end, @Param("companySid") Integer companySid);

    /**
     * 搜尋工作單號筆數[提交或作廢] By 建立日期區間
     */
    @Query("SELECT COUNT(r) AS size FROM #{#entityName} r WHERE r.create_dt BETWEEN :start AND :end AND r.comp_sid = :companySid AND r.wr_status <> 'DRAFT' AND r.wr_status <> 'DELETE'")
    Integer findTodayWRMasterTotalSize(@Param("start") Date start, @Param("end") Date end, @Param("companySid") Integer companySid);

    /**
     * 搜尋[草稿或刪除]工作單號 By 建立日期區間
     */
    @Query("SELECT wr_no FROM #{#entityName} r WHERE r.create_dt BETWEEN :start AND :end AND r.comp_sid = :companySid AND (r.wr_status = 'DRAFT' OR r.wr_status = 'DELETE' ) order by r.create_dt DESC")
    List<String> findTodayLastTempWRMaster(@Param("start") Date start, @Param("end") Date end, @Param("companySid") Integer companySid);

    /**
     * 搜尋工作單號筆數[草稿或刪除] By 建立日期區間
     */
    @Query("SELECT COUNT(r) AS size FROM #{#entityName} r WHERE r.create_dt BETWEEN :start AND :end AND r.comp_sid = :companySid AND (r.wr_status = 'DRAFT' OR r.wr_status = 'DELETE' )")
    Integer findTodayTempWRMasterTotalSize(@Param("start") Date start, @Param("end") Date end, @Param("companySid") Integer companySid);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.last_modify_dt  = :last_modify_dt "
            + " WHERE r.sid = :wr_sid")
    int updateLastModifyTime(@Param("last_modify_dt") Date last_modify_dt, @Param("wr_sid") String wr_sid);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.read_receipts_member  = :read_receipts_member "
            + " WHERE r.sid = :wr_sid")
    int updateReadReceiptsMember(@Param("read_receipts_member") ReadReceiptsMember readReceiptsMember, @Param("wr_sid") String wr_sid);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.last_modify_memo  = :last_modify_memo,r.last_modify_memo_css = :last_modify_memo_css "
            + " WHERE r.sid = :wr_sid")
    int updateLastModifyMemo(@Param("last_modify_memo") String last_modify_memo, @Param("last_modify_memo_css") String last_modify_memo_css, @Param("wr_sid") String wr_sid);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.lock_dt  = :lock_dt,r.lock_usr = :lock_usr "
            + " WHERE r.sid = :wr_sid")
    int updateLockUserAndLockDate(@Param("lock_dt") Date lock_dt, @Param("lock_usr") Integer lock_usr, @Param("wr_sid") String wr_sid);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.lock_dt  = :lock_dt,r.lock_usr = :lock_usr "
            + " WHERE r.sid = :wr_sid and r.lock_usr  = :closeUserSid")
    int closeLockUserAndLockDate(@Param("lock_dt") Date lock_dt, @Param("lock_usr") Integer lock_usr, @Param("wr_sid") String wr_sid, @Param("closeUserSid") Integer closeUserSid);

    WRMaster findBySid(String wrSid);
}
