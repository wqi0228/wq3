/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRAttachment;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 工作報告 - 附件
 *
 * @author brain0925_liao
 */
public interface WRAttachmentRepository extends JpaRepository<WRAttachment, String>, Serializable {

    /** 搜尋附件List By 工作報告Sid */
    @Query("SELECT o FROM #{#entityName} o WHERE o.status=0 "
            + "AND o.wr_sid = :wr_sid")
    public List<WRAttachment> getWRAttachmentByWRSid(
            @Param("wr_sid") String wr_sid);

    /** 更新附件資訊 By 附件Sid */
    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.wr_sid  = :wr_sid , r.wr_no = :wr_no ,r.description = :description , r.update_usr_sid = :update_usr_sid , r.update_dt = :update_dt"
            + " WHERE r.sid = :sid")
    public int updateWRMasterMappingInfo(@Param("wr_sid") String wr_sid, @Param("wr_no") String wr_no, @Param("description") String description,
            @Param("update_usr_sid") Integer update_usr_sid, @Param("update_dt") Date update_dt, @Param("sid") String sid);
}
