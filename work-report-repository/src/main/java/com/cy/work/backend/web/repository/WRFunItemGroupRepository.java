/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRFunItemGroup;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 選單Group
 * @author brain0925_liao
 */
public interface WRFunItemGroupRepository extends JpaRepository<WRFunItemGroup, Integer>,Serializable{
    
    /** 搜尋選單Group By category_model*/
     @Query("SELECT o FROM #{#entityName} o WHERE  "
            + " o.category_model = :category_model")
    public List<WRFunItemGroup> getWRFunItemGroupByCategoryModel(
            @Param("category_model") String category_model);
}
