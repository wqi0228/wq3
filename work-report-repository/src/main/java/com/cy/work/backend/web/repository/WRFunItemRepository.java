/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.backend.web.repository;

import com.cy.work.backend.web.vo.WRFunItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.io.Serializable;
import java.util.List;

/**
 * 選單Item
 *
 * @author brain0925_liao
 */
public interface WRFunItemRepository extends JpaRepository<WRFunItem, Integer>, Serializable {

    /**
     * 搜尋選單Item By category_model
     */
    @Query("SELECT o FROM #{#entityName} o WHERE  "
            + " o.category_model = :category_model")
    List<WRFunItem> getWRFunItemByCategoryModel(
            @Param("category_model") String category_model);

}
