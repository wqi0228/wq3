# 工作報告 RELEASE NOTE
#1.15.0
 - [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKRPT/versions/15355 )
 - [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-report+version+1.15.0 )
 - <span style="color:red;">security【1.16.0】</span>
 - <span style="color:red;">commons-util【0.0.11】</span>
 - <span style="color:red;">work-common【2.27.0】</span>
#1.14.1
 - [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKRPT/versions/15329 )
 - [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-report+version+1.14.1 )
 - <span style="color:red;">security【1.14.4】</span>
 - <span style="color:red;">work-common【2.20.0】</span>
#1.14.0
 - [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKRPT/versions/15241 )
 - [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-report+version+1.14.0 )
 - <span style="color:red;">security【1.11.8】</span>
 - <span style="color:red;">work-common【2.15.0】</span>
#1.12.0
 - WORKRPT-94 更改DB連線方式
#1.11.0
 - WORKRPT-92 [基礎單位成員可閱權限減少(交集)] 提供還原預設值的功能
#1.10.0
 - p1.0.0 更換錯誤頁圖檔
#1.9.0
 - WORKRPT-91 WERP MONITOR STATUS CHECK - WEB
#1.4.3
 - WORKRPT-82 追蹤報表的追蹤事項欄位，會有HTML Code
#1.4.1
 - WORKRPT-81 editer元件判斷tag異常

